#!/bin/sh
#
# Packages the app for the specified target.
#

cd "$(dirname "$0")/.."

# Android
package_android() {
	is_true $VERBOSE && echo
	package_app "$APP_BUILD" "$target" "$args"
}

# iOS
package_ios() {
	is_true $VERBOSE && echo
	package_app "$APP_BUILD" "$target" "$args"
}

. ./scripts/share/app.sh
setup_app

while test $# -gt 0; do
	case $1 in
		--release|--debug)
			APP_BUILD=${1#--}
		;;
		--fd)
			APP_BUILD=debug
			swf="${BIN_DIR}/${APP_SWF}.swf"
		;;
		-*)
			die "Unknown option: \"$1\""
		;;
		*)
			pkg_target=$1
		;;
	esac
	
	shift
done

IFS='-' read -r target args <<<"${pkg_target}"
[[ $(type -t "package_$target") != 'function' ]] && die "Unknown package target: \"${pkg_target}\""

setup_sdk "$target"

[[ ! $swf ]] && swf="${BIN_DIR}-${APP_BUILD}/${APP_SWF}-$target.swf"
[[ ! -f $swf ]] && die "SWF file not found: \"$swf\""
app_swf="${APP_DIR}/${APP_SWF}.swf"

if [[ $swf != $app_swf ]]; then
	is_true $VERBOSE && echo "cp \"$swf\" \"${app_swf}\""
	cp "$swf" "${app_swf}"
fi

package_$target
