#!/bin/sh
#
# Runs the app on the specified target.
#

cd "$(dirname "$0")/.."

# Android
run_android() {
	is_true $VERBOSE && echo
	package_app "$APP_BUILD" "$target" "$args"
	echo
	
	is_true $VERBOSE && echo adb devices
	adb devices
	echo "Installing ${PKG_FILE}:"$'\n'
	is_true $VERBOSE && echo adb -d install -r \"${PKG_FILE}\"
	adb -d install -r "${PKG_FILE}" || die "Failed to install app."
	
	echo $'\nStarting app on the device:\n'
	is_true $VERBOSE && echo adb shell am start -n ${APP_ID}/.AppEntry
	adb shell am start -n ${APP_ID}/.AppEntry
}

# iOS
run_ios() {
	is_true $VERBOSE && echo
	package_app "$APP_BUILD" "$target" "$args"
	echo
	
	echo "Installing ${PKG_FILE}:"$'\n'
	is_true $VERBOSE && echo adt -installApp -platform ios -package \"${PKG_FILE}\"
	adt -installApp -platform ios -package "${PKG_FILE}" || die "Failed to install app."
	
	echo $'\nStart app on the device manually.'
}

# ADL
# http://help.adobe.com/en_US/air/build/WSfffb011ac560372f-6fa6d7e0128cca93d31-8000.html
run_adl() {
	local adl_opts=
	if [[ $args = 'desktop' ]]; then
		adl_opts="${adl_opts} -profile desktop"
	elif [[ $args = 'desktop-ext' ]]; then
		adl_opts="${adl_opts} -profile extendedDesktop"
	elif [[ $args =~ ^mobile-(.*) ]]; then
		adl_opts="${adl_opts} -profile mobileDevice -screensize \"${BASH_REMATCH[1]}\""
		echo $adl_opts
	else
		die "The ADL profile must be specified."
	fi
	
	is_true $VERBOSE && echo adl ${adl_opts} \"$APP_XML\" \"$APP_DIR\"
	eval adl ${adl_opts} \"$APP_XML\" \"$APP_DIR\"
	exit_code=$?
	[[ ${exit_code} -ne 0 && ${exit_code} -ne 127 ]] && die "ADL terminated with exit code ${exit_code}."
}

# Web
run_browser() {
	local browser
	[[ $WEB_BROWSER ]] &&
		browser="$(cygpath "${WEB_BROWSER}")" ||
		browser='cygstart'
	if [[ $args = file ]]; then
		app_url="file://$(cygpath -a -m "${WEB_FILE_DIR}")"
	elif [[ $args = local ]]; then
		app_url=${WEB_LOCAL_URL}
	else
		app_url=${WEB_REMOTE_URL}
	fi
	app_url="${app_url}/${WEB_INDEX}"
	is_true $VERBOSE && echo \"$browser\" \"${app_url}\"
	"$browser" "${app_url}"
}

run_standalone() {
	local sa_player
	[[ $WEB_SA_PLAYER ]] &&
		sa_player="$(cygpath "${WEB_SA_PLAYER}")" ||
		sa_player='cygstart'
	is_true $VERBOSE && echo \"${sa_player}\" \"${app_swf}\"
	"${sa_player}" "${app_swf}"
}

. ./scripts/share/app.sh
setup_app

while test $# -gt 0; do
	case $1 in
		--release|--debug)
			APP_BUILD=${1#--}
		;;
		--fd)
			APP_BUILD=debug
			swf="${BIN_DIR}/${APP_SWF}.swf"
		;;
		-*)
			die "Unknown option: \"$1\""
		;;
		*)
			run_target=$1
		;;
	esac
	
	shift
done

IFS='-' read -r target player args <<<"${run_target}"
[[ $player = device ]] && player=$target
[[ $(type -t "run_$player") != 'function' ]] && die "Unknown run target: \"${run_target}\""

setup_sdk "$player"

[[ ! $swf ]] && swf="${BIN_DIR}-${APP_BUILD}/${APP_SWF}-$target.swf"
[[ ! -f $swf ]] && die "SWF file not found: \"$swf\""
if [[ "$target-$player" = 'web-browser' ]]; then
	if [[ $args = file || $args = local ]]; then
		eval app_swf='"$(cygpath "${WEB_'${args^^}'_DIR}")"'
	elif [[ $args = remote ]]; then
		app_swf="${WEB_REMOTE_DIR}"
		cp='./scripts/deploy_scp.sh'
	else
		die "Unknown run target: \"${run_target}\""
	fi
else
	app_swf="${APP_DIR}"
fi
app_swf="${app_swf}/${APP_SWF}.swf"

if [[ $swf != $app_swf ]]; then
	is_true $VERBOSE && echo "${cp:-cp} \"$swf\" \"${app_swf}\""
	${cp:-cp} "$swf" "${app_swf}"
fi

run_$player
