#!/bin/sh
#
# Backs up the entire project's directory.
#

cd "$(dirname "$0")/.."
[[ -f application.local.conf ]] || { echo "Error: \"application.local.conf\" not found." >&2; exit 1; }
. ./application.local.conf

[[ $BACKUP_DIR ]] || { echo "Error: BACKUP_DIR not defined. Define it in \"application.local.conf\"." >&2; exit 1; }

pwd="$(pwd)"
app_name="${pwd##*/}"
backup_name=$app_name-$(date +'%Y%m%d').7z

set -e

if [[ -f .backupignore ]]; then
	ignore_file="$(mktemp --tmpdir backupignore.XXXXXXXXXX)"
	trap 'rm -f "${ignore_file}"' EXIT HUP INT TERM
	sed -E "s,^[^\s*],${app_name}/&," .backupignore >>"${ignore_file}"
	ignore_opts="-xr0@$(cygpath -w ${ignore_file})"
	ignore_opts=${ignore_opts//\\/\\\\}
fi

cd ..
rm -f "${backup_name}"
eval 7z a ${ignore_opts} \"${backup_name}\" \"${app_name}\"
rm -f "$BACKUP_DIR/${app_name}"-*.7z
mv "${backup_name}" "$BACKUP_DIR"
echo
du -h "$BACKUP_DIR/${backup_name}"
