#!/bin/sh
#
# Deploys a file using scp.
# TODO: Add support for deploying multiple files / folders (scp's -r option)
#

cd "$(dirname "$0")/.."

[[ $1 =~ .*\.(swf|xml|json|html|js|css|txt|png|jpg)$ ]] && cygrun chmod 644 "$1"
cygrun scp -v "$1" "$2" 2>&1 | 
	grep --line-buffered -E '^(debug1:\s*)?(Connecting to|Authenticating to|Offering.*public key|Authenticated to|Sending file|Transferred|Exit)' | 
	sed -ur 's,debug[0-9]?:\s*,,'
#time pscp -v "$1" "$2" 2>&1 | 
#	grep --line-buffered -E '^(Looking up|Connecting to|Using username|Authenticating with|Access|Sending file|Server sent|Disconnect)'
