is_true() {
	[[ $# -gt 0 && ( $1 = yes || $1 = true || $1 = 1 ) ]]
}

die() {
	[[ $1 ]] && echo $'\n'"Error: $1" >&2
	[[ $2 != 0 ]] && is_true $PAUSE_ON_ERRORS && { read -rs -n 1 -p $'\nPress any key to continue... '; echo; }
	exit ${2:-1}
}

trim() {
	while [[ $# -gt 0 ]]; do
		read -rd '' $1 <<<"${!1}"
		shift
	done
}

setup_sdk() {
	[[ ! $AIR_SDK ]] && AIR_SDK=${AIR_SDK_HOME:-${AIR_HOME}}
	[[ ! -d ${AIR_SDK} || ! -d ${AIR_SDK}/bin ]] && die "Incorrect path to AIR SDK: \"${AIR_SDK}\""
	is_true $VERBOSE && echo "Using AIR SDK: \"${AIR_SDK}\""
	PATH="${AIR_SDK}/bin:$PATH"
	
	while [[ $# -gt 0 ]]; do
		if [[ $1 = 'android' ]]; then
			[[ ! -d ${ANDROID_SDK} || ! -d ${ANDROID_SDK}/platform-tools ]] && die "Incorrect path to Android SDK: \"${ANDROID_SDK}\""
			is_true $VERBOSE && echo "Using Android SDK: \"${ANDROID_SDK}\""
			PATH="$PATH:${ANDROID_SDK}/platform-tools"
		fi
		shift
	done
	
	PATH=$(cygpath -p "$PATH")
	is_true $VERBOSE && echo
}

setup_app() {
	[[ ! -f application.conf ]] && die "Missing app configuration file: \"application.conf\""
	. ./application.conf
	
	is_true $ANDROID_REMOVE_AIR_PREFIX && AIR_NOANDROIDFLAIR=true || AIR_NOANDROIDFLAIR=false
	export AIR_NOANDROIDFLAIR
	
	if [[ -f ${BUILD_DIR}/build.properties ]]; then
		local name value
		while IFS='=' read -r name value; do
			trim name value
			[[ ! $name || ${name:0:1} = '#' ]] && continue
			name=${name//./_}
			name=${name^^}
			eval $name='$value'
		done <"${BUILD_DIR}/build.properties"
	fi
	
	[[ ! -f ${APP_XML} ]] && die "Missing app descriptor file: \"${APP_XML}\""
	local line null tag content
	while read -r line; do
		IFS='<>' read -r null tag content null <<<"$line"
		if [[ $tag = 'id' ]]; then
			APP_ID=$content
			break
		fi
	done <"${APP_XML}"
	[[ ! ${APP_ID} ]] && die "Failed to parse app ID from app descriptor file: \"${APP_XML}\""
}

package_app() {
	local build=$1 target=$2 args=$3 type subtype ext arch_opts cert_file sign_opts debug_opts icons pkg_include
	
	case $target in
		android)
			type=apk
			ext=apk
			local arch_rx='(.*[^-]+|)-?(armv7|x86)$'
			if [[ $args =~ $arch_rx ]]; then
				args=${BASH_REMATCH[1]}
				arch_opts="-arch ${BASH_REMATCH[2]}"
			fi
			if [[ $build = debug ]]; then
				subtype=debug
			else
				subtype=$args
			fi
			cert_file=$ANDROID_CERT_FILE
			sign_opts=$ANDROID_SIGNING_OPTIONS
			icons=$ANDROID_ICONS
		;;
		ios)
			type=ipa
			ext=ipa
			if [[ $build = debug ]]; then
				subtype="debug${args:+-}$args"
			elif [[ $args != ad-hoc && $args != app-store ]]; then
				subtype="test${args:+-}$args"
			else
				subtype=$args
			fi
			if [[ $args = ad-hoc || $args = app-store ]]; then
				cert_file=$IOS_DIST_CERT_FILE
				sign_opts=$IOS_DIST_SIGNING_OPTIONS
			else
				cert_file=$IOS_DEV_CERT_FILE
				sign_opts=$IOS_DEV_SIGNING_OPTIONS
			fi
			icons=$IOS_ICONS
		;;
		*)
			die "Unknown package target: \"$target\""
		;;
	esac
	
	pkg_include="$PKG_INCLUDE -C \"$icons\" ."
	type="$type${subtype:+-}$subtype"
	PKG_FILE=${DIST_DIR}/${DIST_PKG}${subtype:+-}$subtype.$ext
	[[ $build = debug ]] && debug_opts=$DEBUG_OPTIONS
	
#	echo
#	echo target=$target build=$build args=$args
#	echo type=$type subtype=$subtype
#	echo cert_file=$cert_file
#	echo sign_opts=$sign_opts
#	echo arch_opts=$arch_opts
#	echo debug_opts=$debug_opts
#	echo icons=$icons
#	echo pkg_include=$pkg_include
#	echo PKG_FILE=$PKG_FILE
#	echo
	
	[[ ! -f ${cert_file} ]] && die "Missing certificate file: \"${cert_file}\""
	[[ ! -d ${DIST_DIR} ]] && mkdir -p ${DIST_DIR}
	
	echo "Using certificate: \"${cert_file}\""
	echo "Packaging \"${PKG_FILE}\":"
	[[ $type =~ ^ipa-.*-compiler$ ]] && echo $'This will take a while...'
	echo
	local cmd="adt -package -target $type${debug_opts:+ }${debug_opts}${arch_opts:+ }${arch_opts} $sign_opts \"${PKG_FILE}\" \"${APP_XML}\" ${pkg_include}"
	local cmd_rx='(.*) -storepass ("[^"]*"|[^ ]*) (.*)'
	is_true $VERBOSE && [[ $cmd =~ $cmd_rx ]] && echo "${BASH_REMATCH[1]} -storepass *REDACTED* ${BASH_REMATCH[3]}"
	eval $cmd || die "Failed to package app."
	
	is_true $VERBOSE && { echo; du -h "${PKG_FILE}"; }
}
