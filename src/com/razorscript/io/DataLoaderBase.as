package com.razorscript.io {
	import com.razorscript.utils.ObjUtil;
	import flash.utils.ByteArray;
	import starling.events.EventDispatcher;
	
	/**
	 * Base class for IDataLoader implementations.
	 *
	 * @author Gene Pavlovsky
	 */
	public class DataLoaderBase extends EventDispatcher {
		public function DataLoaderBase() {
		}
		
		protected var _dataType:String;
		/**
		 * Data type.
		 * If not set, data type is auto-detected.
		 */
		public function get dataType():String {
			return _dataType;
		}
		
		public function set dataType(value:String):void {
			_dataType = value;
		}
		
		protected var _compression:String;
		/**
		 * Compression algorithm.
		 * If not set, compression is disabled.
		 */
		public function get compression():String {
			return _compression;
		}
		
		public function set compression(value:String):void {
			_compression = value;
		}
		
		protected var _error:Error;
		
		public function get error():Error {
			return _error;
		}
		
		protected function checkDataType(data:*):String {
			if (_dataType) {
				if ((_dataType == DataType.STRING) && !(data is String))
					throw new ArgumentError('The data argument (' + data + ') is not a String.');
				else if ((_dataType == DataType.OBJECT) && !ObjUtil.isObject(data))
					throw new ArgumentError('The data argument (' + data + ') is not an Object.');
				else if ((_dataType == DataType.BYTE_ARRAY) && !(data is ByteArray))
					throw new ArgumentError('The data argument (' + data + ') is not a ByteArray.');
				return _dataType;
			}
			else {
				var type:String = (data is String) ? DataType.STRING : ObjUtil.isObject(data) ? DataType.OBJECT : (data is ByteArray) ? DataType.BYTE_ARRAY : null;
				if (!type)
					throw new ArgumentError('The data argument (' + data + ') is not a String, Object or a ByteArray.');
				return type;
			}
		}
	}
}
