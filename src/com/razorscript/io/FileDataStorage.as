package com.razorscript.io {
	
	/**
	 * A File-backed data storage.
	 *
	 * @author Gene Pavlovsky
	 */
	public class FileDataStorage extends DataLoaderBase implements IDataStorage {
		public function FileDataStorage() {
			super();
		}
		
		public function load():void {
			throw new Error('Not implemented.');
		}
		
		public function save(data:*):void {
			throw new Error('Not implemented.');
		}
		
		public function wipe():void {
			throw new Error('Not implemented.');
		}
		
		public function toString():String {
			return '[FileDataStorage]';
		}
	}
}
