package com.razorscript.io {
	import com.razorscript.events.RazorEventType;
	
	/**
	 * A data loader for wrapping embedded data.
	 *
	 * @author Gene Pavlovsky
	 */
	public class EmbedDataLoader extends DataLoaderBase implements IDataLoader {
		public function EmbedDataLoader(dataClass:Class = null) {
			super();
			this.dataClass = dataClass;
		}
		
		public var dataClass:Class;
		
		public function load():void {
			_error = null;
			try {
				var data:* = new dataClass();
				// TODO: Implement data type conversions (@see SharedObjectDataStorage's byteArrayToData() and dataToByteArray()).
				checkDataType(data);
			}
			catch (err:Error) {
				_error = err;
				dispatchEventWith(RazorEventType.LOAD_ERROR);
				return;
			}
			
			dispatchEventWith(RazorEventType.LOAD, false, data);
		}
		
		public function toString():String {
			return '[EmbedDataLoader dataClass=' + dataClass + ']';
		}
	}
}
