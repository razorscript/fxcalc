package com.razorscript.io {

	/**
	 * A data storage.
	 *
	 * @author Gene Pavlovsky
	 */
	public interface IDataStorage extends IDataLoader {
		/**
		 * Saves the specified data. Dispatches RazorEventType.SAVE event on success, or RazorEventType.SAVE_ERROR on failure.
		 *
		 * @param data The data to save.
		 */
		function save(data:*):void;

		/**
		 * Wipes the data. Dispatches RazorEventType.WIPE event on success, or RazorEventType.WIPE_ERROR on failure.
		 */
		function wipe():void;
	}
}
