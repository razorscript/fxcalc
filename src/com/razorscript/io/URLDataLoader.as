package com.razorscript.io {
	
	/**
	 * A URLLoader-backed data loader.
	 *
	 * @author Gene Pavlovsky
	 */
	public class URLDataLoader extends DataLoaderBase implements IDataLoader {
		public function URLDataLoader() {
			super();
		}
		
		public function load():void {
			throw new Error('Not implemented.');
		}
		
		public function toString():String {
			return '[URLDataLoader]';
		}
	}
}
