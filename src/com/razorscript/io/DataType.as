package com.razorscript.io {
	
	/**
	 * Defines constants for data types.
	 *
	 * @author Gene Pavlovsky
	 */
	public class DataType {
		public static const STRING:String = 'String';
		public static const OBJECT:String = 'Object';
		public static const BYTE_ARRAY:String = 'ByteArray';
	}
}
