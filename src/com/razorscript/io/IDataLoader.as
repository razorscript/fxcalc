package com.razorscript.io {
	import com.razorscript.events.IEventDispatcher;
	
	/**
	 * A data storage.
	 *
	 * @author Gene Pavlovsky
	 */
	public interface IDataLoader extends IEventDispatcher {
		/**
		 * Data type.
		 * If not set, data type is auto-detected.
		 */
		function get dataType():String;
		function set dataType(value:String):void;
		
		/**
		 * Compression algorithm.
		 * If not set, compression is disabled.
		 */
		function get compression():String;
		function set compression(value:String):void;
		
		/**
		 * If an error occurs, contains the error object, otherwise null.
		 */
		function get error():Error;
		
		/**
		 * Loads the data. Dispatches RazorEventType.LOAD event with loaded data on success, or RazorEventType.LOAD_ERROR on failure.
		 */
		function load():void;
	}
}
