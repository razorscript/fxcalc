package com.razorscript.io {
	import com.razorscript.debug.Debug;
	import com.razorscript.debug.Logger;
	import com.razorscript.events.RazorEventType;
	import com.razorscript.utils.BinUtil;
	import flash.events.NetStatusEvent;
	import flash.net.SharedObject;
	import flash.net.SharedObjectFlushStatus;
	import flash.utils.ByteArray;
	
	/**
	 * A SharedObject-backed data storage.
	 *
	 * @author Gene Pavlovsky
	 */
	public class SharedObjectDataStorage extends DataLoaderBase implements IDataStorage {
		public static const PROPERTY_NAME:String = 'data';
		
		public function SharedObjectDataStorage(name:String = null, path:String = null, propertyName:String = PROPERTY_NAME, secure:Boolean = false) {
			super();
			this.name = name;
			this.path = path;
			this.propertyName = propertyName;
			this.secure = secure;
		}
		
		public var name:String;
		public var path:String;
		public var propertyName:String;
		public var secure:Boolean;
		
		public var minDiskSpace:int = 100e3; // TODO: Set to a small value and test flush() behavior in the browser.
		
		protected static const PROPERTY_NAME_DATA_TYPE:String = '__dataType';
		protected static const PROPERTY_NAME_COMPRESSION:String = '__compression';
		
		public function load():void {
			_error = null;
			try {
				initSharedObject();
				var data:* = sharedObject.data[propertyName];
				var origDataType:String = sharedObject.data[PROPERTY_NAME_DATA_TYPE];
				var origCompression:String = sharedObject.data[PROPERTY_NAME_COMPRESSION];
				
				CONFIG::debug {
					if (Debug.instance.debugDataStorage)
						Logger.debug(this, 'load()', 'dataType=' + origDataType, 'compression=' + origCompression);
				}
				
				if (data is ByteArray)
					data = byteArrayToData(data as ByteArray, _dataType ? _dataType : origDataType, origCompression);
				else if (_dataType == DataType.BYTE_ARRAY)
					data = dataToByteArray(data, null);
				else if (_dataType && origDataType && (_dataType != origDataType))
					throw new Error('Unsupported conversion from ' + origDataType + ' to ' + _dataType + '.');
			}
			catch (err:Error) {
				_error = err;
				dispatchEventWith(RazorEventType.LOAD_ERROR);
				return;
			}
			
			dispatchEventWith(RazorEventType.LOAD, false, data);
		}
		
		public function save(data:*):void {
			_error = null;
			try {
				var origDataType:String = checkDataType(data);
				
				CONFIG::debug {
					if (Debug.instance.debugDataStorage)
						Logger.debug(this, 'save()', 'dataType=' + origDataType, 'compression=' + _compression);
				}
				
				if (_compression)
					data = dataToByteArray(data, _compression);
				
				initSharedObject();
				sharedObject.data[propertyName] = data;
				sharedObject.data[PROPERTY_NAME_DATA_TYPE] = origDataType;
				sharedObject.data[PROPERTY_NAME_COMPRESSION] = _compression;
				
				flushReasons |= FLUSH_SAVE;
				var result:String = sharedObject.flush(minDiskSpace);
			}
			catch (err:Error) {
				_error = err;
				dispatchFlushEvents(false);
				return;
			}
			
			if (result == SharedObjectFlushStatus.FLUSHED)
				dispatchFlushEvents(true);
			else
				minDiskSpace = Math.max(sharedObject.size, minDiskSpace) * 2;
		}
		
		public function wipe():void {
			_error = null;
			try {
				initSharedObject();
				delete sharedObject.data[propertyName];
				delete sharedObject.data[PROPERTY_NAME_DATA_TYPE];
				delete sharedObject.data[PROPERTY_NAME_COMPRESSION];
				
				flushReasons |= FLUSH_WIPE;
				var result:String = sharedObject.flush(minDiskSpace);
			}
			catch (err:Error) {
				_error = err;
				dispatchFlushEvents(false);
				return;
			}
			
			if (result == SharedObjectFlushStatus.FLUSHED)
				dispatchFlushEvents(true);
			else
				minDiskSpace = Math.max(sharedObject.size, minDiskSpace) * 2;
		}
		
		protected function initSharedObject():void {
			if ((name == sharedObjectName) && (path == sharedObjectPath))
				return;
			if (sharedObject)
				sharedObject.removeEventListener(NetStatusEvent.NET_STATUS, onNetStatus);
			sharedObjectName = name;
			sharedObjectPath = path;
			sharedObject = SharedObject.getLocal(name, path, secure);
			sharedObject.addEventListener(NetStatusEvent.NET_STATUS, onNetStatus);
		}
		
		protected static const FLUSH_SAVE:int = 1;
		protected static const FLUSH_WIPE:int = 2;
		
		protected var flushReasons:int = 0;
		
		protected function dispatchFlushEvents(success:Boolean):void {
			var flushSave:Boolean = flushReasons & FLUSH_SAVE;
			var flushWipe:Boolean = flushReasons & FLUSH_WIPE;
			flushReasons = 0;
			
			if (flushSave)
				dispatchEventWith(success ? RazorEventType.SAVE : RazorEventType.SAVE_ERROR, false, success ? null : _error);
			if (flushWipe)
				dispatchEventWith(success ? RazorEventType.WIPE : RazorEventType.WIPE_ERROR, false, success ? null : _error);
		}
		
		protected function onNetStatus(e:NetStatusEvent):void {
			var success:Boolean = e.info.code == 'SharedObject.Flush.Success';
			if (!success)
				_error = new Error('Permission denied.');
			dispatchFlushEvents(success);
		}
		
		// TODO: Move byteArrayToData() and dataToByteArray() to BinUtil or somewhere else?
		protected function byteArrayToData(byteArray:ByteArray, dataType:String, compression:String):* {
			if ((dataType == DataType.BYTE_ARRAY) || compression)
				byteArray = BinUtil.cloneByteArray(byteArray);
			
			if (compression)
				BinUtil.uncompressByteArray(byteArray, compression);
			
			byteArray.position = 0;
			if (dataType == DataType.STRING)
				return byteArray.toString();
			else if (dataType == DataType.OBJECT)
				return byteArray.readObject();
			else
				return byteArray;
		}
		
		protected function dataToByteArray(data:*, compression:String):ByteArray {
			var byteArray:ByteArray;
			if (data is ByteArray) {
				byteArray = BinUtil.cloneByteArray(data as ByteArray);
			}
			else {
				byteArray = new ByteArray();
				if (data is String)
					byteArray.writeUTFBytes(data as String);
				else
					byteArray.writeObject(data);
			}
			
			if (compression)
				BinUtil.compressByteArray(byteArray, compression);
			
			return byteArray;
		}
		
		public function toString():String {
			return '[SharedObjectDataStorage name=' + name + ' path=' + path + ' propertyName=' + propertyName + ']';
		}
		
		protected var sharedObjectName:String;
		protected var sharedObjectPath:String;
		protected var sharedObject:SharedObject;
	}
}
