package com.razorscript.utils {
	import com.razorscript.ds.ObjectIterator;
	import com.razorscript.utils.formatter.conversions.FormatterConversionFun;
	import com.razorscript.utils.formatter.Formatter;
	import com.razorscript.utils.formatter.handlers.FormatterHandlerFun;
	import flash.utils.ByteArray;

	/**
	 * String utility functions.
	 *
	 * @author Gene Pavlovsky
	 */
	public class StrUtil {
		/**
		 * Returns a new string containing the string str repeated count times.
		 *
		 * @see http://stackoverflow.com/a/5450113/1262753
		 *
		 * @param str The string to repeat.
		 * @param count How many times to repeat the string.
		 * @return A string containing str repeated count times.
		 *
		 * @throws RangeError - if str is empty.
		 */
		public static function repeat(str:String, count:int):String {
			if (!str.length)
				throw new RangeError('Expected a non-empty string.');
			if (count <= 0)
				return '';
			return _repeat(str, count);
		}

		[Inline]
		protected static function _repeat(str:String, count:int):String {
			var result:String = '';
			while (count > 1) {
				if (count & 1)
					result += str;
				count >>= 1;
				str += str;
			}
			return result + str;
		}

		/**
		 * Left-pads the string str with the string pad to the specified length. Prepends as many pad strings as possible without exceeding specified length.
		 *
		 * @param str The string to left-pad.
		 * @param pad The padding string.
		 * @param length The desired string length.
		 * @return Left-padded string at least length characters long.
		 *
		 * @throws RangeError - if pad is empty.
		 */
		public static function lpad(str:String, pad:String, length:int):String {
			if (!pad.length)
				throw new RangeError('Expected a non-empty pad.');
			var count:int = Math.floor((length - str.length) / pad.length);
			if (count <= 0)
				return str;
			return _repeat(pad, count) + str;
		}

		/**
		 * Right-pads the string str with the string pad to the specified length. Appends as many pad strings as possible without exceeding specified length.
		 *
		 * @param str The string to right-pad.
		 * @param pad The padding string.
		 * @param length The desired string length.
		 * @return Right-padded string at least length characters long.
		 *
		 * @throws RangeError - if pad is empty.
		 */
		public static function rpad(str:String, pad:String, length:int):String {
			if (!pad.length)
				throw new RangeError('Expected a non-empty pad.');
			var count:int = Math.floor((length - str.length) / pad.length);
			if (count <= 0)
				return str;
			return str + _repeat(pad, count);
		}

		/**
		 * Returns the string with all char characters on the left trimmed.
		 *
		 * @param str The string to left-trim.
		 * @param char The char to trim.
		 * @return Left-trimmed string.
		 */
		public static function ltrim(str:String, char:String = ' '):String {
			var length:int = str.length;
			var i:int = 0;
			while ((i < length) && (str.charAt(i) == char))
				++i;
			return i ? str.substr(i) : str;
		}

		/**
		 * Returns the string with all char characters on the right trimmed.
		 *
		 * @param str The string to right-trim.
		 * @param char The char to trim.
		 * @return Right-trimmed string.
		 */
		public static function rtrim(str:String, char:String = ' '):String {
			var lastIndex:int = str.length - 1;
			var i:int = lastIndex;
			while ((i >= 0) && (str.charAt(i) == char))
				--i;
			return (i < lastIndex) ? str.substr(0, i + 1) : str;
		}

		/**
		 * Returns the string with all char characters on the left and right trimmed.
		 *
		 * @param str The string to trim.
		 * @param char The char to trim.
		 * @return Trimmed string.
		 */
		public static function trim(str:String, char:String = ' '):String {
			var length:int = str.length;
			var i:int = 0;
			while ((i < length) && (str.charAt(i) == char))
				++i;
			var j:int = length - 1;
			while ((j > i) && (str.charAt(j) == char))
				--j;
			return (i || (j < length - 1)) ? str.substring(i, j + 1) : str;
		}

		/**
		 * Removes all trailing zeros from a string. After that, if the last character is a decimal point, removes it as well.
		 * Faster than using rtrim(rtrim(str, '0'), '.').
		 *
		 * @param str A string.
		 * @return Trimmed string.
		 */
		public static function rtrim0_dp(str:String):String {
			var lastIndex:int = str.length - 1;
			var i:int = lastIndex;
			while ((i >= 0) && (str.charAt(i) == '0'))
				--i;
			if ((i >= 0) && (str.charAt(i) == '.'))
				--i;
			return (i < lastIndex) ? str.substr(0, i + 1) : str;
		}

		/**
		 * Splits a string into substrings of specified length and returns them as an array.
		 *
		 * @param str A string to split.
		 * @param width Number of characters per substring.
		 * @param shortFirst When the string's length doesn't divide cleanly by width, if shortFirst is true [false], the first [last] substring will contain (str.length % width) characters.
		 * @return An array of strings.
		 *
		 * @throws RangeError - if width is not positive.
		 */
		public static function splitw(str:String, width:int, shortFirst:Boolean = false):Array {
			if (width <= 0)
				throw new RangeError('Expected a positive width.');
			var arr:Array = [];
			var i:int = 0;
			var length:int = str.length;
			if (shortFirst) {
				i = length - Math.floor(length / width) * width;
				if (i)
					arr.push(str.substr(0, i));
			}
			for (; i < length; i += width)
				arr.push(str.substr(i, width));
			return arr;
		}

		public static const TO_STRING_MAX_DEPTH:int = 2;

		/**
		 * Formats an array as string.
		 * The sortNames and sortOptions arguments apply to any Object items (if recursive descent is enabled).
		 *
		 * @param value The array to format as string.
		 * @param maxDepth Allowed depth of recursive descent into Array, Vector and Object items.
		 * @param sortNames Whether to sort the properties by name.
		 * @param sortOptions String compare function or sort options (@see Vector#sort()).
		 * @return The formatted string.
		 */
		public static function arrayToString(value:Array, maxDepth:int = TO_STRING_MAX_DEPTH, sortNames:Boolean = true, sortOptions:* = null):String {
			return _vectorToString(value, maxDepth, sortNames, sortOptions);
		}

		/**
		 * Formats a vector as string.
		 * The sortNames and sortOptions arguments apply to any Object items (if recursive descent is enabled).
		 *
		 * @param value The vector to format as string.
		 * @param maxDepth Allowed depth of recursive descent into Array, Vector and Object items.
		 * @param sortNames Whether to sort the properties by name.
		 * @param sortOptions String compare function or sort options (@see Vector#sort()).
		 * @return The formatted string.
		 *
		 * @throws ArgumentError - if value is not a vector.
		 */
		public static function vectorToString(value:*, maxDepth:int = TO_STRING_MAX_DEPTH, sortNames:Boolean = true, sortOptions:* = null):String {
			if (!VecUtil.isVector(value))
				throw new ArgumentError('The value argument (' + value + ') is not a Vector.');
			return _vectorToString(value, maxDepth, sortNames, sortOptions);
		}

		protected static function _vectorToString(value:*, maxDepth:int, sortNames:Boolean, sortOptions:*):String {
			if (maxDepth < 0)
				return '[...]';
			var str:String = '';
			for each (var item:* in value) {
				if (str)
					str += ', ';
				str += valueToString(item, maxDepth - 1, sortNames, sortOptions);
			}
			return '[' + str + ']';
		}

		/**
		 * Formats an object as string.
		 *
		 * @param value The object to format as string.
		 * @param maxDepth Allowed depth of recursive descent into Array, Vector and Object items.
		 * @param sortNames Whether to sort the properties by name.
		 * @param sortOptions String compare function or sort options (@see Vector#sort()).
		 * @return The formatted string.
		 */
		public static function objectToString(value:Object, maxDepth:int = TO_STRING_MAX_DEPTH, sortNames:Boolean = true, sortOptions:* = null):String {
			if (maxDepth < 0)
				return '{...}';
			var str:String = '';
			var iterator:ObjectIterator = new ObjectIterator(value, sortNames, sortOptions);
			while (iterator.hasNext) {
				var name:String = iterator.getNext();
				if (str)
					str += ', ';
				str += name + ': ' + valueToString(value[name], maxDepth - 1, sortNames, sortOptions);
			}
			return '{' + str + '}';
		}

		protected static const OBJECT_TO_STRING:Function = {}.toString;

		/**
		 * Formats a value of an arbitrary type as string. Useful in custom toString functions for formatting values of unknown (generic) types.
		 * The sortNames and sortOptions arguments apply to any Object items (if recursive descent is enabled).
		 *
		 * @param value A value of an arbitrary data type.
		 * @param maxDepth Allowed depth of recursive descent into Array and Object items.
		 * @param sortNames Whether to sort the properties by name.
		 * @param sortOptions String compare function or sort options (@see Vector#sort()).
		 * @return The formatted string.
		 */
		public static function valueToString(value:*, maxDepth:int = TO_STRING_MAX_DEPTH, sortNames:Boolean = true, sortOptions:* = null):String {
			return (value == null) ? 'null' :
				((value is Boolean) || (value is Number) || (value is XML) || (value is XMLList)) ? value.toString() :
				(value is String) ? ('"' + value + '"') :
				(value is Array) ? arrayToString(value, maxDepth, sortNames, sortOptions) :
				VecUtil.isVector(value) ? _vectorToString(value, maxDepth, sortNames, sortOptions) :
				(value.toString == OBJECT_TO_STRING) ? objectToString(value, maxDepth, sortNames, sortOptions) : value.toString();
		}

		/**
		 * Formats an integer as a string of 32 binary digits. Leading zeros are added if necessary.
		 *
		 * @param value An integer.
		 * @return The formatted string.
		 */
		public static function intToBinaryString(value:uint):String {
			return lpad(value.toString(2), '0', 32);
		}

		/**
		 * Formats an integer as a string of 8 hexadecimal digits. Leading zeros are added if necessary.
		 *
		 * @param value An integer.
		 * @return The formatted string.
		 */
		public static function intToHexString(value:uint):String {
			return lpad(value.toString(16), '0', 8);
		}

		/**
		 * Converts a value of an arbitrary type to it's binary representation and returns it as a string of binary digits.
		 * The value is written to a ByteArray then read back as integers and converted to binary.
		 *
		 * @param value A value of an arbitrary data type.
		 * @return The formatted string.
		 */
		public static function valueToBinaryString(value:*):String {
			if ((value is int) || (value is uint))
				return intToBinaryString(value);
			return _valueToRawString(value, 2);
		}

		/**
		 * Converts a value of an arbitrary type to it's binary representation and returns it as a string of hexadecimal digits.
		 * The value is written to a ByteArray then read back as integers and converted to hexadecimal.
		 *
		 * @param value A value of an arbitrary data type.
		 * @return The formatted string.
		 */
		public static function valueToHexString(value:*):String {
			if ((value is int) || (value is uint))
				return intToHexString(value);
			return _valueToRawString(value, 16);
		}

		/** The base argument must be either 2 (binary) or 16 (hexadecimal). */
		protected static function _valueToRawString(value:*, base:int):String {
			initByteArray();
			if (value is Number)
				byteArray.writeDouble(value);
			else if (value is String)
				byteArray.writeUTFBytes(value);
			else if (value is Boolean)
				byteArray.writeBoolean(value);
			else
				byteArray.writeObject(value);
			byteArray.position = 0;

			var str:String = '';
			while (byteArray.bytesAvailable >= 4)
				str += lpad(byteArray.readUnsignedInt().toString(base), '0', (base == 2) ? 32 : 8);

			if (byteArray.bytesAvailable >= 2)
				str += lpad(byteArray.readUnsignedShort().toString(base), '0', (base == 2) ? 16 : 4);
			if (byteArray.bytesAvailable)
				str += lpad(byteArray.readUnsignedByte().toString(base), '0', (base == 2) ? 8 : 2);
			byteArray.clear();
			return str;
		}

		protected static var byteArray:ByteArray;
		protected static function initByteArray():void {
			if (byteArray)
				byteArray.clear();
			else
				byteArray = new ByteArray();
		}

		protected static var _formatter:Formatter;
		/**
		 * The Formatter used by format() and sprintf().
		 *
		 * For the list of supported conversions, see FormatterConversionFun.
		 * @see com.razorscript.utils.formatter.conversions.FormatterConversionFun#extend()
		 *
		 * For the list of provided handlers, see FormatterConversionFun.
		 * @see com.razorscript.utils.formatter.handlers.FormatterHandlerFun#extend()
		 */
		public static function get formatter():Formatter {
			return _formatter ? _formatter : (_formatter = FormatterHandlerFun.extend(FormatterConversionFun.extend(new Formatter())));
		}

		public static function set formatter(value:Formatter):void {
			_formatter = value;
		}

		/**
		 * Formats a string (similar to Python's format). The specified format string can contain literal text, which is copied unchanged to the output, or replacement fields delimited by curly braces "{}".
		 * Use "{{" and "}}" to output a literal character "{" and "}", respectively.
		 *
		 * Each replacement field can start with an optional parameter that specifies the object whose value is to be formatted and inserted into the output instead of the replacement field.
		 * Parameter starts with an integer that specifies the number of the positional argument (zero-based), followed by any number of property or index expressions.
		 * An expression of the form ".name" selects the named property, while an expression of the form "[index]" does an index lookup.
		 * If any single replacement field specifies a parameter, all the rest of the replacement fields must also specify a parameter.
		 *
		 * The parameter is optionally followed by a conversion field, preceded by an exclamation mark '!'.
		 * The optional conversion field causes a value conversion before formatting. This can, for example, modify the value or convert it to another type.
		 * Supported conversions can be added or removed using addConversion() and removeConversion().
		 * The Formatter used by this method supports several conversions by default. For the list of supported conversions, see FormatterConversionFun.
		 * @see com.razorscript.utils.formatter.conversions.FormatterConversionFun#extend()
		 *
		 * The conversion field is optionally followed by a format spec, preceded by a colon ':'.
		 * The optional format spec field contains a specification of how the value should be presented, including such details as field width, alignment, padding, decimal precision etc.
		 * The format spec can include nested replacement fields within it. These nested replacement fields must contain a parameter; conversion flags and format specifications are not allowed.
		 * The replacement fields within the format spec are substituted before the format spec is interpreted. This allows the formatting of a value to be dynamically specified.
		 * @see com.razorscript.utils.formatter.supportClasses.FormatSpec
		 *
		 * Supports custom formatting for built-in and user-defined classes using handler functions.
		 * User-defined classes can also provide a toFormattedString(formatSpec:String):String method to convert an object to a string.
		 * The Formatter used by this method provides several handlers by default. For the list of provided handlers, see FormatterHandlerFun.
		 * @see com.razorscript.utils.formatter.handlers.FormatterHandlerFun#extend()
		 *
		 * repl_field    ::=  "{" [parameter] ["!" conversion] [":" format_spec] "}"
		 * parameter     ::=  arg_index ("." property_name | "[" element_index "]")*
		 * arg_index     ::=  integer
		 * property_name ::=  identifier
		 * element_index ::=  integer
		 * conversion    ::=  identifier
		 * format_spec   ::=  [[fill] align] [sign] [#] [0] [width] [group_sep] [. [precision]] [type]
		 * fill          ::=  <any character>
		 * align         ::=  "<" | ">" | "=" | "^"
		 * sign          ::=  "+" | "-" | " "
		 * group_sep     ::=  "," | "_" | "*"
		 * width         ::=  integer
		 * precision     ::=  integer
		 * type          ::=  "s" | "d" | "i" | "u" | "b" | "o" | "x" | "X" | "c" | "e" | "E" | "f" | "F" | "g" | "G" | "n" | "a" | "A" | "p"
		 * nested field  ::=  "{" [parameter] "}"
		 *
		 * @see com.razorscript.utils.formatter.Formatter#format()
		 *
		 * @param format A format string.
		 * @param ... args A parameter.
		 * @return A copy of the format string, with each replacement field replaced with the string value of the corresponding parameter, formatted according to the specified (or default) formatting options.
		 *
		 * @throws FormatterSyntaxError - if the format string is invalid.
		 * @throws FormatterArgumentError - if a parameter is invalid.
		 */
		public static function format(format:String, ... args):String {
			args.unshift(format);
			return formatter.format.apply(null, args);
		}

		/**
		 * Formats a string (similar to C's sprintf). The specified format string can contain literal text, which is copied unchanged to the output, or replacement fields denoted by a percent sign '%'.
		 * Use "%%" to output a literal character "%".
		 *
		 * Each replacement field can start with an optional parameter that specifies the object whose value is to be formatted and inserted into the output instead of the replacement field.
		 * Parameter starts with an integer that specifies the number of the positional argument (zero-based), followed by any number of property or index expressions, terminated with a dollar sign '$'.
		 * An expression of the form ".name" selects the named property, while an expression of the form "[index]" does an index lookup.
		 * If any single replacement field specifies a parameter, all the rest of the replacement fields must also specify a parameter.
		 *
		 * The parameter is optionally followed by a conversion field, terminated with an exclamation mark '!'.
		 * The optional conversion field causes a value conversion before formatting. This can, for example, modify the value or convert it to another type.
		 * Supported conversions can be added or removed using addConversion() and removeConversion().
		 * The Formatter used by this method supports several conversions by default. For the list of supported conversions, see FormatterConversionFun.
		 * @see com.razorscript.utils.formatter.conversions.FormatterConversionFun#extend()
		 *
		 * The conversion field is followed by a format spec, which must indicate the type of the field.
		 * The format spec field contains a specification of how the value should be presented, including such details as field width, alignment, padding, decimal precision etc.
		 * Unlike format(), the format spec can not include nested replacement fields within it.
		 * @see com.razorscript.utils.formatter.supportClasses.FormatSpec
		 *
		 * repl_field    ::=  "%" [parameter "$"] [conversion "!"] format_spec
		 * parameter     ::=  arg_index ("." property_name | "[" element_index "]")*
		 * arg_index     ::=  integer
		 * property_name ::=  identifier
		 * element_index ::=  integer
		 * conversion    ::=  identifier
		 * format_spec   ::=  [[fill] align] [sign] [#] [0] [width] [group_sep] [. [precision]] type
		 * fill          ::=  <any character>
		 * align         ::=  "<" | ">" | "=" | "^"
		 * sign          ::=  "+" | "-" | " "
		 * group_sep     ::=  "," | "_" | "*"
		 * width         ::=  integer
		 * precision     ::=  integer
		 * type          ::=  "s" | "d" | "i" | "u" | "b" | "o" | "x" | "X" | "c" | "e" | "E" | "f" | "F" | "g" | "G" | "n" | "a" | "A" | "p"
		 *
		 * Doesn't support custom formatting handler functions.
		 * Doesn't support custom formatting with a toFormattedString() method provided by user-defined classes.
		 *
		 * @see com.razorscript.utils.formatter.Formatter#sprintf()
		 *
		 * @param format A format string.
		 * @param ... args A parameter.
		 * @return A copy of the format string, with each replacement field replaced with the string value of the corresponding parameter, formatted according to the specified (or default) formatting options.
		 *
		 * @throws FormatterSyntaxError - if the format string is invalid.
		 * @throws FormatterArgumentError - if a parameter is invalid.
		 */
		public static function sprintf(format:String, ... args):String {
			args.unshift(format);
			return formatter.sprintf.apply(null, args);
		}

		/**
		 * Formats a string using format() and outputs the result using trace().
		 * The trace() function can be replaced with a custom one.
		 *
		 * @see com.razorscript.utils.StrUtil#format()
		 * @see com.razorscript.utils.StrUtil#trace
		 */
		public static function tformat(format:String, ... args):void {
			args.unshift(format);
			trace(formatter.format.apply(null, args));
		}

		/**
		 * Formats a string using sprintf() and outputs the result using trace().
		 * The trace() function can be replaced with a custom one.
		 *
		 * @see com.razorscript.utils.StrUtil#sprintf()
		 * @see com.razorscript.utils.StrUtil#trace
		 */
		public static function printf(format:String, ... args):void {
			args.unshift(format);
			trace(formatter.sprintf.apply(null, args));
		}

		/**
		 * A trace function that does nothing.
		 *
		 * @param ... args An argument.
		 */
		public static function noTrace(... args):void {
		}

		protected static var _trace:Function;
		/**
		 * The function used by tformat() and printf() to trace messages. If null, the global trace() is used. To disable tracing, set this property to noTrace.
		 */
		public static function get trace():Function {
			return _trace ? _trace : TRACE;
		}

		public static function set trace(value:Function):void {
			_trace = value;
		}
	}
}

const TRACE:Function = trace;
