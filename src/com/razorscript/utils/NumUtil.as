package com.razorscript.utils {
	import com.razorscript.math.Double;
	import com.razorscript.math.MathExt;
	import com.razorscript.math.supportClasses.SimpleBigDecimal;

	/**
	 * Number (double-precision floating-point) utility functions.
	 *
	 * @author Gene Pavlovsky
	 */
	public class NumUtil {
		/**
		 * Returns the number of ULPs between two numbers.
		 * ULP stands for "unit of least precision" or "unit in the last place" and is the spacing between floating-point numbers, i.e., the value the least significant digit represents if it is 1.
		 * The result is a signed integer value (stored in type Number) representing the number of distinct representations between x and y.
		 * If either number is a NaN, returns NaN.
		 * If one number is an infinity and the other number is finite, returns Infinity.
		 * If numbers are infinities of the same sign, returns NaN.
		 * If numbers are infinities of the opposite sign, returns Infinity.
		 *
		 * @param x A number of type Number or Double.
		 * @param y A number of type Number or Double.
		 * @return The number of ULPs between the two numbers.
		 *
		 * @throws ArgumentError - if any argument is not a Double or a Number.
		 */
		public static function distance(x:*, y:*):Number {
			return getDouble(x).distance(getDouble(y, true));
		}

		/**
		 * Returns the nearest representable number which is greater than x.
		 *
		 * @param x A number of type Number or Double.
		 * @return The nearest representable number which is greater than x.
		 *
		 * @throws ArgumentError - if x is not a Double or a Number.
		 */
		public static function next(x:*):Number {
			return getDouble(x, false, true).advance(1).number;
		}

		/**
		 * Returns the nearest representable number which is less than x.
		 *
		 * @param x A number of type Number or Double.
		 * @return The nearest representable number which is less than x.
		 *
		 * @throws ArgumentError - if x is not a Double or a Number.
		 */
		public static function prior(x:*):Number {
			return getDouble(x, false, true).advance(-1).number;
		}

		/**
		 * Returns the number whose distance from x is the specified number of ULPs.
		 *
		 * @param x A number of type Number or Double.
		 * @param distance A signed integer number of ULPs (stored in type Number). Must be not NaN and not Infinity.
		 * @return The number r such that distance(x, r) == distance.
		 *
		 * @throws ArgumentError - if x is not a Double or a Number.
		 */
		public static function advance(x:*, distance:Number):Number {
			return getDouble(x, false, true).advance(distance).number;
		}

		/**
		 * Returns a string representation of a number in exponential notation.
		 * The string will contain one non-zero digit before the decimal point and the specified number of digits after the decimal point.
		 * Trailing zeros are added if necessary.
		 * If fractionDigits is 0, the decimal point is not included.
		 * If rounding is necessary, the half-away from zero rounding method is used.
		 *
		 * @see com.razorscript.math.Double#toExponential()
		 *
		 * @param value A number of type Number or Double.
		 * @param fractionDigits The desired number of digits after the decimal point.
		 * @param trailingZeros Whether to include trailing zeros. If false, insignificant trailing zeros are not included, and if there are no digits after the decimal point, it is not included either.
		 * @return The string representation of the number in exponential notation.
		 *
		 * @throws ArgumentError - if value is not a Double or a Number.
		 * @throws RangeError - if fractionDigits is negative.
		 */
		public static function toExponential(value:*, fractionDigits:int = 6, trailingZeros:Boolean = true):String {
			return getDouble(value).toExponential(fractionDigits, trailingZeros);
		}

		/**
		 * Returns a string representation of a number in fixed-point notation.
		 * The string will contain the specified number of digits after the decimal point.
		 * Trailing zeros are added if necessary.
		 * If fractionDigits is 0, the decimal point is not included.
		 * If rounding is necessary, the half-away from zero rounding method is used.
		 *
		 * @see com.razorscript.math.Double#toFixed()
		 *
		 * @param value A number of type Number or Double.
		 * @param fractionDigits The desired number of digits after the decimal point.
		 * @param trailingZeros Whether to include trailing zeros. If false, insignificant trailing zeros are not included, and if there are no digits after the decimal point, it is not included either.
		 * @return The string representation of the number in fixed-point notation.
		 *
		 * @throws ArgumentError - if value is not a Double or a Number.
		 * @throws RangeError - if fractionDigits is negative.
		 */
		public static function toFixed(value:*, fractionDigits:int = 6, trailingZeros:Boolean = true):String {
			return getDouble(value).toFixed(fractionDigits, trailingZeros);
		}

		/**
		 * Returns a string representation of a number in either fixed-point or exponential notation, whichever is more appropriate for it's magnitude.
		 * If the number's decimal exponent exp is in the range (fixedMinExponent <= exp < precision), the fixed-point notation is used, otherwise exponential notation is used.
		 * The string will contain the specified number of significant digits.
		 * In both cases insignificant trailing zeros after the decimal point are not included.
		 * If there are no digits after the decimal point, the decimal point is not included.
		 * If precision is 0, precision is set to 1.
		 * If rounding is necessary, the half-away from zero rounding method is used.
		 *
		 * @see com.razorscript.math.Double#toGeneral()
		 *
		 * @param value A number of type Number or Double.
		 * @param precision The desired number of significant digits. If precision is 0, precision is set to 1.
		 * @param trailingZeros Whether to include trailing zeros. If false, insignificant trailing zeros are not included, and if there are no digits after the decimal point, it is not included either.
		 * @param fixedMinExponent The low endpoint of the decimal exponent's range where the fixed-point notation is used.
		 * @return The string representation of the number in general format.
		 *
		 * @throws ArgumentError - if value is not a Double or a Number.
		 * @throws RangeError - if precision is negative.
		 */
		public static function toGeneral(value:*, precision:int = 6, trailingZeros:Boolean = false, fixedMinExponent:int = -4):String {
			return getDouble(value).toGeneral(precision, trailingZeros, fixedMinExponent);
		}

		/**
		 * Returns a new SimpleBigDecimal representing a number.
		 * The SimpleBigDecimal will contain the specified number of significant digits.
		 * If round argument is true, one extra digit is printed and then the digits are rounded to precision.
		 * If round argument is false, no extra digits are printed and no rounding is performed.
		 *
		 * @see com.razorscript.math.Double#toBigDecimal()
		 *
		 * @param value A number of type Number or Double.
		 * @param precision The desired number of significant digits. If precision is 0, precision is set to unlimited.
		 * @param round If true, one extra digit is printed and then the digits are rounded to precision, otherwise no extra digits are printed and no rounding is performed (i.e. the number is truncated).
		 * @return A new SimpleBigDecimal representing the number.
		 *
		 * @throws ArgumentError - if value is not a Double or a Number.
		 * @throws RangeError - if precision is negative.
		 */
		public static function toBigDecimal(value:*, precision:int = MathExt.DOUBLE_UNIQ_DIGITS, round:Boolean = true):SimpleBigDecimal {
			return getDouble(value).toBigDecimal(precision, round);
		}

		/**
		 * Returns a string representation of a number as a string of binary digits in IEEE 754 double-precision floating-point format (binary64).
		 *
		 * @see com.razorscript.math.Double#toBitString()
		 *
		 * @param value A number of type Number or Double.
		 * @return The string representation of the number as a string of hexadecimal digits.
		 */
		public static function toBitString(value:*):String {
			return getDouble(value).bitString;
		}

		/**
		 * Returns a string representation of a number as a string of hexadecimal digits in IEEE 754 double-precision floating-point format (binary64).
		 *
		 * @see com.razorscript.math.Double#toHexString()
		 *
		 * @param value A number of type Number or Double.
		 * @return The string representation of the number as a string of hexadecimal digits.
		 */
		public static function toHexString(value:*):String {
			return getDouble(value).hexString;
		}

		/**
		 * Returns a string representation of a number in hexfloat format, in the form [-][01].hhhhhhhhhhhhhp[-+]dddd.
		 * The digit before the radix point is 0 if the number is subnormal, 1 otherwise.
		 * The stored bits of the significand of the number are formatted as a string of hexadecimal digits.
		 * Insignificant trailing zeros after the decimal point are not included.
		 * If there are no digits after the decimal point, the decimal point is not included.
		 * The base-2 exponent is formatted as a decimal number using at least one digit but at most as many digits as necessary to represent the value exactly.
		 *
		 * @see com.razorscript.math.Double#toHexFloat()
		 * @see http://www.gnu.org/software/libc/manual/html_node/Floating_002dPoint-Conversions.html
		 *
		 * @param value A number of type Number or Double.
		 * @param trailingZeros Whether to include trailing zeros. If false, insignificant trailing zeros are not included, and if there are no digits after the decimal point, it is not included either.
		 * @return The string representation of the number in hexfloat format.
		 */
		public static function toHexFloat(value:*, trailingZeros:Boolean = false):String {
			return getDouble(value).toHexFloat(trailingZeros);
		}

		/**
		 * Converts a string to a floating-point number. Provides more accurate results than parseFloat().
		 * Leading whitespace characters are ignored, as are trailing non-numeric characters.
		 * If the string can't be parsed, the result is NaN.
		 *
		 * The parseFloat() function is inaccurate for many inputs, with confirmed error of up to 7 ULPs for some inputs.
		 * This function relies on parseFloat() to get the first conversion candidate, accurate enough to differ from the correct conversion by at most 1 in the 15th digit.
		 * The first candidate is then compared with the real number specified by the input string (the target), to determine if there is another representable number which is a better conversion.
		 * The result is almost always the correct conversion. If the target is half-way between two representable numbers, the result is either correct, or 1 ULP away from correct.
		 *
		 * @see com.razorscript.math.Double#parseDouble()
		 *
		 * @param str The string to read and convert to a floating-point number.
		 * @return A number or NaN.
		 */
		public static function parseNumber(str:String):Number {
			try {
				var double:Double = _getDouble(null);
				double.parseDouble(str);
				return double.number;
			}
			catch (err:Error) {
				return NaN;
			}
		}

		/**
		 * Returns the number, rounded to the specified number of significant digits.
		 * If precision is 0, precision is set to 1.
		 * If rounding is necessary, the half-away from zero rounding method is used.
		 *
		 * @param value A number of type Number or Double.
		 * @param precision The desired number of significant digits. If precision is 0, precision is set to 1.
		 * @return The rounded number.
		 *
		 * @throws ArgumentError - if value is not a Double or a Number.
		 * @throws RangeError - if precision is negative.
		 */
		public static function roundToPrecision(value:*, precision:int):Number {
			return parseNumber(toExponential(value, precision - 1));
		}

		/**
		 * Converts a string of binary digits in IEEE 754 double-precision floating-point format (binary64) to a floating-point number.
		 * Leading whitespace characters are not allowed, as are trailing non-numeric characters.
		 * If the string can't be parsed, the result is NaN.
		 *
		 * @see com.razorscript.math.Double#bitString
		 *
		 * @param str The string to read and convert to a floating-point number.
		 * @return A number or NaN.
		 */
		public static function parseBitString(str:String):Number {
			try {
				var double:Double = _getDouble(null);
				double.bitString = str;
				return double.number;
			}
			catch (err:Error) {
				return NaN;
			}
		}

		/**
		 * Converts a string of hexadecimal digits in IEEE 754 double-precision floating-point format (binary64) to a floating-point number.
		 * Leading whitespace characters are not allowed, as are trailing non-numeric characters.
		 * If the string can't be parsed, the result is NaN.
		 *
		 * @see com.razorscript.math.Double#hexString
		 *
		 * @param str The string to read and convert to a floating-point number.
		 * @return A number or NaN.
		 */
		public static function parseHexString(str:String):Number {
			try {
				var double:Double = _getDouble(null);
				double.hexString = str;
				return double.number;
			}
			catch (err:Error) {
				return NaN;
			}
		}

		/**
		 * Converts a hexfloat to a floating-point number.
		 * Leading whitespace characters are ignored, as are trailing non-numeric characters.
		 * If the string can't be parsed, the result is NaN.
		 *
		 * @see com.razorscript.math.Double#hexFloat
		 *
		 * @param str The string to read and convert to a floating-point number.
		 * @return A number or NaN.
		 */
		public static function parseHexFloat(str:String):Number {
			try {
				var double:Double = _getDouble(null);
				double.hexFloat = str;
				return double.number;
			}
			catch (err:Error) {
				return NaN;
			}
		}

		/**
		 * If value is a Double and alwaysUsePool argument is false, returns the value.
		 * If value is a Number or alwaysUsePool argument is true, retrieves a Double from the pool, sets it's number property to the specified value, and returns that Double.
		 * If the next property is false, all Doubles in the pool are marked available, and the first Double from the pool is used. Any values returned from the pool by earlier calls to getDouble() become invalid.
		 * If the next property is true, the next available Double from the pool is used. This makes sure that all values returned from the pool by earlier calls to getDouble() are still valid.
		 *
		 * @param value A number of type Number or Double.
		 * @param next If true, the next available Double from the pool will be used. If false, all Doubles in the pool are marked available and the first Double from the pool is used.
		 * @param alwaysUsePool If true, a Double from the pool is always used. Useful if the returned Double will be modified. If false and value is a Double, value is returned.
		 * @return The value itself if it is a Double and alwaysUsePool argument is false, or a Double from the pool, holding the specified number.
		 */
		protected static function getDouble(value:*, next:Boolean = false, alwaysUsePool:Boolean = false):Double {
			// Check early. Should reset index even if value itself will be returned (a Double from the pool is not used).
			if (!next)
				index = 0;
			if (value is Double) {
				if (alwaysUsePool)
					value = (value as Double).number;
				else
					return value;
			}
			else if (!(value is Number)) {
				throw new ArgumentError('The value argument (' + value + ') is not a Double or a Number.');
			}

			// Using a Double from the pool, increment index.
			if (next)
				++index;
			return _getDouble(value);
		}

		protected static function _getDouble(value:*):Double {
			if (!pool) {
				pool = new Vector.<Double>(POOL_LENGTH);
				for (var i:int = 0; i < POOL_LENGTH; ++i)
					pool[i] = new Double();
			}

			if (index == pool.length)
				pool[index] = (value != null) ? new Double(value) : new Double();
			else if (value != null)
				pool[index].number = value;

			return pool[index];
		}

		protected static const POOL_LENGTH:int = 2;

		protected static var pool:Vector.<Double>;
		protected static var index:uint;
	}
}
