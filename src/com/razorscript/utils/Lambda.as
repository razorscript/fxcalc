package com.razorscript.utils {
	
	/**
	 * Function closure utility functions.
	 *
	 * @author Gene Pavlovsky
	 */
	public class Lambda {
		/**
		 * Apply to fixed arguments.
		 */
		
		/**
		 * Returns a 0-ary function that applies a specified function to a variable number of specified fixed arguments, and returns the result.
		 *
		 * Example:
		 *		function printDigit(digit:int):void {
		 *			trace(digit);
		 *		}
		 *
		 *		for (var i:int = 0; i < 10; ++i)
		 * 			actions[i] = Lambda.fix(i);
		 * 		actions[5](); // 5
		 */
		public static function fix(fun:Function, ... fixArgs):Function {
			return function ():* {
				return fun.apply(null, fixArgs);
			}
		}
		
		/***********************************************************************************************************************************************/
		
		/**
		 * Replace received arguments with fixed arguments.
		 *
		 * Each of these functions returns an anonymous function of a specific arity.
		 * That function applies a specified function to a variable number of specified fixed arguments, discarding received arguments, and returns the result.
		 * Equivalent to
		 *
		 * Example:
		 *		function addDigit(digit:int):void {
		 *			trace(digit);
		 *		}
		 *
		 *		// Create a MouseEvent handler that discards the received event object, and calls addDigit(1).
		 *		button1.addEventListener(MouseEvent.CLICK, Lambda.replace1(addDigit, 1));
		 * 		// Click button1, trace output: 1
		 */
		
		/**
		 * Returns a 1-ary function that applies a specified function to a variable number of specified fixed arguments, discarding received argument, and returns the result.
		 */
		public static function replace1(fun:Function, ... fixArgs):Function {
			return function (funArg1:*):* {
				return fun.apply(null, fixArgs);
			}
		}
		
		/**
		 * Returns a 2-ary function that applies a specified function to a variable number of specified fixed arguments, discarding received arguments, and returns the result.
		 */
		public static function replace2(fun:Function, ... fixArgs):Function {
			return function (funArg1:*, funArg2:*):* {
				return fun.apply(null, fixArgs);
			}
		}
		
		/**
		 * Returns a 3-ary function that applies a specified function to a variable number of specified fixed arguments, discarding received arguments, and returns the result.
		 */
		public static function replace3(fun:Function, ... fixArgs):Function {
			return function (funArg1:*, funArg2:*, funArg3:*):* {
				return fun.apply(null, fixArgs);
			}
		}
		
		/**
		 * Returns an N-ary function that applies a specified function to a variable number of specified fixed arguments, discarding received arguments, and returns the result.
		 */
		public static function replace(fun:Function, ... fixArgs):Function {
			return function (... funArgs):* {
				return fun.apply(null, fixArgs);
			}
		}
		
		/***********************************************************************************************************************************************/
		
		/**
		 * Prepend fixed arguments to received arguments.
		 *
		 * Each of these functions returns an anonymous function of a specific arity.
		 * That function applies a specified function to a variable number of specified fixed arguments, concatenated with received arguments, and returns the result.
		 *
		 * Example:
		 *		function divide(x:Number, y:Number):Number {
		 *			return x / y;
		 *		}
		 *
		 *		// Create a function that receives one argument x, and returns the result of calling divide(1, x);
		 *		var reciprocal:Function = Lambda.prepend1(divide, 1);
		 *		trace(reciprocal(10)); // 0.1
		 */
		
		/**
		 * Returns a 1-ary function that applies a specified function to a variable number of specified fixed arguments, concatenated with received argument, and returns the result.
		 */
		public static function prepend1(fun:Function, ... fixArgs):Function {
			return function (funArg1:*):* {
				arguments.unshift.apply(null, fixArgs);
				return fun.apply(null, arguments);
			}
		}
		
		/**
		 * Returns a 2-ary function that applies a specified function to a variable number of specified fixed arguments, concatenated with received arguments, and returns the result.
		 */
		public static function prepend2(fun:Function, ... fixArgs):Function {
			return function (funArg1:*, funArg2:*):* {
				arguments.unshift.apply(null, fixArgs);
				return fun.apply(null, arguments);
			}
		}
		
		/**
		 * Returns a 3-ary function that applies a specified function to a variable number of specified fixed arguments, concatenated with received arguments, and returns the result.
		 */
		public static function prepend3(fun:Function, ... fixArgs):Function {
			return function (funArg1:*, funArg2:*, funArg3:*):* {
				arguments.unshift.apply(null, fixArgs);
				return fun.apply(null, arguments);
			}
		}
		
		/**
		 * Returns an N-ary function that applies a specified function to a variable number of specified fixed arguments, concatenated with received arguments, and returns the result.
		 */
		public static function prepend(fun:Function, ... fixArgs):Function {
			return function (... funArgs):* {
				funArgs.unshift.apply(null, fixArgs);
				return fun.apply(null, funArgs);
			}
		}
		
		/***********************************************************************************************************************************************/
		
		/**
		 * Append fixed arguments to received arguments.
		 *
		 * Each of these functions returns an anonymous function of a specific arity.
		 * That function applies a specified function to received arguments, concatenated with a variable number of specified fixed arguments, and returns the result.
		 *
		 * Example:
		 *		function divide(x:Number, y:Number):Number {
		 *			return x / y;
		 *		}
		 *
		 *		// Create a function that receives one argument x, and returns the result of calling divide(x, 2);
		 *		var halve:Function = Lambda.append1(divide, 2);
		 *		trace(halve(10)); // 5
		 */
		
		/**
		 * Returns a 1-ary function that applies a specified function to received argument, concatenated with a variable number of specified fixed arguments, and returns the result.
		 */
		public static function append1(fun:Function, ... fixArgs):Function {
			return function (funArg1:*):* {
				arguments.push.apply(null, fixArgs);
				return fun.apply(null, arguments);
			}
		}
		
		/**
		 * Returns a 2-ary function that applies a specified function to received arguments, concatenated with a variable number of specified fixed arguments, and returns the result.
		 */
		public static function append2(fun:Function, ... fixArgs):Function {
			return function (funArg1:*, funArg2:*):* {
				arguments.push.apply(null, fixArgs);
				return fun.apply(null, arguments);
			}
		}
		
		/**
		 * Returns a 3-ary function that applies a specified function to received arguments, concatenated with a variable number of specified fixed arguments, and returns the result.
		 */
		public static function append3(fun:Function, ... fixArgs):Function {
			return function (funArg1:*, funArg2:*, funArg3:*):* {
				arguments.push.apply(null, fixArgs);
				return fun.apply(null, arguments);
			}
		}
		
		/**
		 * Returns an N-ary function that applies a specified function to received arguments, concatenated with a variable number of specified fixed arguments, and returns the result.
		 */
		public static function append(fun:Function, ... fixArgs):Function {
			return function (... funArgs):* {
				funArgs.push.apply(null, fixArgs);
				return fun.apply(null, funArgs);
			}
		}

		/***********************************************************************************************************************************************/
		
		/**
		 * Slice received arguments.
		 *
		 * Each of these functions returns an anonymous function of a specific arity.
		 * That function applies a specified function to a specified subset of received arguments, and returns the result.
		 * The subset is specified by a start index (inclusive) and an end index (exclusive).
		 *
		 * Example:
		 *		// Assuming items is an array of objects with a "name" property, create an array of item names using Array.map() which expects a function of the following call signature:
		 * 		//		function callback(item:*, index:int, array:Array):String
		 * 		// Create a 3-ary function which calls the anonymous get name function with just the first argument (default slice range is index = 0, count = 1).
		 * 		var itemNames:Array = items.map(Lambda.slice3(function (obj:Object):String { return obj.name }));
		 */
		
		/**
		 * Returns a 1-ary function that applies a specified function to a specified subset of received arguments, and returns the result.
		 * Mostly useless, only included for completeness. To discard a 1-ary function's argument, use replace1(fun) instead.
		 */
		public static function slice1(fun:Function, startIndex:int = 0, endIndex:int = 1):Function {
			return function (funArg1:*):* {
				return fun.apply(null, arguments.slice(startIndex, endIndex));
			}
		}
		
		/**
		 * Returns a 2-ary function that applies a specified function to a specified subset of received arguments, and returns the result.
		 */
		public static function slice2(fun:Function, startIndex:int = 0, endIndex:int = 1):Function {
			return function (funArg1:*, funArg2:*):* {
				return fun.apply(null, arguments.slice(startIndex, endIndex));
			}
		}
		
		/**
		 * Returns a 3-ary function that applies a specified function to a specified subset of received arguments, and returns the result.
		 */
		public static function slice3(fun:Function, startIndex:int = 0, endIndex:int = 1):Function {
			return function (funArg1:*, funArg2:*, funArg3:*):* {
				return fun.apply(null, arguments.slice(startIndex, endIndex));
			}
		}
		
		/**
		 * Returns an N-ary function that applies a specified function to a specified subset of received arguments, and returns the result.
		 */
		public static function slice(fun:Function, startIndex:int = 0, endIndex:int = 1):Function {
			return function (... args):* {
				return fun.apply(null, args.slice(startIndex, endIndex));
			}
		}
		
		/***********************************************************************************************************************************************/
		
		/**
		 * Compose two or more unary functions.
		 */
		
		/**
		 * Returns a 1-ary function that applies a chain of 2 or more functions to received argument, and returns the result.
		 * The functions are applied in right-to-left order.
		 *
		 * Example:
		 *		function square(x:Number):Number {
		 *			return x * x;
		 *		}
		 *		function add1(x:Number):Number {
		 *			return x + 1;
		 *		}
		 *
		 *		// Create a function that receives one argument x, and returns the result of calling add1(square(x)).
		 *		var sqrAdd1:Function = Lambda.compose(add1, square);
		 *		trace(sqrAdd1(3)); // 10
		 */
		public static function compose(f:Function, g:Function, ... rest):Function {
			var lastIndex:int = rest.length - 1;
			if (lastIndex >= 0) {
				return function (x:*):* {
					for (var i:int = lastIndex; i >= 0; --i)
						x = rest[i](x);
					return f(g(x));
				}
			}
			else {
				return function (x:*):* {
					return f(g(x));
				}
			}
		}
	}
}
