package com.razorscript.utils {

	/**
	 * RegExp utility functions.
	 *
	 * @author Gene Pavlovsky
	 */
	public class RXUtil {
		/**
		 * Matches a RegExp metacharacter.
		 * Useful to escape all RegExp metacharacters with backslashes, by calling str.replace(ESCAPE_REGEXP_META_CHARS_RX, '\\$&').
		 */
		public static const ESCAPE_REGEXP_META_CHARS_RX:RegExp = /[\^$\\.*+?()[\]{}|]/g;

		/**
		 * Returns a RegExp that matches any string in the specified vector.
		 * If a filter function is specified, only those items that return true for that filter function are included.
		 * If a suffix string is specified, appends it after the parenthesized RegExp.
		 * If sort is true, sorts the vector by item length in descending order, to make the RegExp greedy.
		 *
		 * @param items The vector of strings to match.
		 * @param filterFun The filter function to run on each array item.
		 * @param suffix The suffix to append after the parenthesized RegExp.
		 * @param sort Whether to sort the vector by item length in descending order.
		 * @return A RegExp that matches any string in the specified vector.
		 */
		public static function createRegExpFromVector(items:Vector.<String>, filterFun:Function = null, suffix:String = '', sort:Boolean = true):RegExp {
			if (sort)
				items.sort(VecUtil.compareStringsByLengthDesc);
			var str:String = '';
			for each (var item:String in items) {
				if (filterFun && !filterFun(item))
					continue;
				if (str)
					str += '|';
				str += item.replace(ESCAPE_REGEXP_META_CHARS_RX, '\\$&');
			}
			return new RegExp('(' + str + ')' + suffix, 'g');
		}
	}
}
