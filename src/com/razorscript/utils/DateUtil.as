package com.razorscript.utils {

	/**
	 * Date utility functions.
	 *
	 * @author Gene Pavlovsky
	 */
	public class DateUtil {
		//protected static const FORMAT_REPL_FIELD_RX:RegExp = /% ([ymdhms])/gx;

		// TODO: Add formatting options (e.g. different paddings).
		/**
		 * Formats a date (similar to UNIX date). The specified format string can contain literal text, which is copied unchanged to the output, or replacement fields denoted by a percent sign '%'.
		 * Use "%%" to output a literal character "%".
		 *
		 * Each replacement field must indicate the type of the field.
		 *
		 * Supported replacement specifiers:
		 * 		%		a literal '%'
		 * 		U		switch to universal time (UTC) mode
		 * 		L		switch to local time mode (default)
		 * 		a		abbreviated weekday name in English (e.g. Sun)
		 * 		A		full weekday name in English (e.g. Sunday)
		 * 		b		abbreviated month name in English (e.g. Aug)
		 * 		B		full month name in English (e.g. August)
		 * 		c		date and time (e.g. Mon Oct 12 2015 02:55:37 UTC+0700)
		 * 		d		day of the month (01..31)
		 * 		F		full date (same as %Y-%m-%d)
		 * 		H		hour (00..23)
		 * 		m		month (01..12)
		 * 		M		minute (00..59)
		 * 		r		date and time in RFC 2822 format (e.g. Mon, 12 Oct 2015 03:02:27 +0700)
		 * 		R		time without seconds (same as %H:%M)
		 * 		s		seconds since 1970-01-01 00:00:00 UTC
		 * 		S		second (00..59)
		 * 		T		time with seconds (same as %H:%M:%S)
		 * 		Y		year
		 * 		z		+hhmm numeric time zone
		 *
		 * @param format A format string.
		 * @param date A date.
		 * @return A copy of the format string, with each replacement field replaced with the string value of the corresponding parameter.
		 */
		public static function format(format:String, date:Date, useUTC:Boolean = false):String {
			_utc = useUTC;
			var str:String = '';
			var index:int = 0;
			var argIndex:int = 0;

			while (true) {
				// Find the next '%' character, starting at index.
				var matchIndex:int = format.indexOf('%', index);
				if (matchIndex == -1)
					break;
				if (matchIndex > index)
					str += format.substring(index, matchIndex);
				/*if (format.substr(matchIndex, 2) == '%%') {
					// '%%' found, replace with a '%', advance index beyond '%%' and continue.
					str += '%';
					index = matchIndex + 2;
					continue;
				}

				// Match the replacement field RegExp. If there's no match, or the match is at an index different from matchIndex, the replacement field is invalid.
				FORMAT_REPL_FIELD_RX.lastIndex = matchIndex;
				var rxMatch:Array = FORMAT_REPL_FIELD_RX.exec(format);
				if (!rxMatch || (rxMatch.index != matchIndex))
					throw new Error('Invalid replacement field at index ' + matchIndex + ': "' + format.substr(matchIndex) + '".');

				str += _formatDate(rxMatch[1], date);
				index = FORMAT_REPL_FIELD_RX.lastIndex;*/

				var fieldChar:String = format.substr(matchIndex + 1, 1);
				var field:String = _formatField(fieldChar, date);
				if (field == null)
					throw new Error('Invalid replacement field at index ' + matchIndex + ': "' + fieldChar + '".');

				str += field;
				index = matchIndex + 2;
			}

			if (index < format.length)
				str += format.substr(index);
			return str;
		}

		protected static const DAYS_FULL:Vector.<String> = new <String>['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
		protected static const DAYS:Vector.<String> = new <String>['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
		protected static const MONTHS_FULL:Vector.<String> = new <String>['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
		protected static const MONTHS:Vector.<String> = new <String>['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

		protected static function _formatField(type:String, date:Date):String {
			switch (type) {
				case '%': return '%';
				case 'a': return DAYS[_utc ? date.dayUTC : date.day];
				case 'A': return DAYS_FULL[_utc ? date.dayUTC : date.day];
				case 'b': return MONTHS[_utc ? date.monthUTC : date.month];
				case 'B': return MONTHS_FULL[_utc ? date.monthUTC : date.month];
				case 'c': return DAYS[_utc ? date.dayUTC : date.day] + MONTHS[_utc ? date.monthUTC : date.month] + ' ' + IntUtil.lpad0_2(_utc ? date.dateUTC : date.date) + ' ' +
					(_utc ? date.fullYearUTC : date.fullYear) + ' ' + _formatTime(date) + ' UTC' + (_utc ? '+0000' : _formatTZ(date));
				case 'd': return IntUtil.lpad0_2(_utc ? date.dateUTC : date.date);
				case 'F': return (_utc ? date.fullYearUTC : date.fullYear) + '-' + IntUtil.lpad0_2(date.month + 1) + '-' + IntUtil.lpad0_2(_utc ? date.dateUTC : date.date);
				case 'H': return IntUtil.lpad0_2(_utc ? date.hoursUTC : date.hours);
				case 'm': return IntUtil.lpad0_2((_utc ? date.monthUTC : date.month) + 1);
				case 'M': return IntUtil.lpad0_2(_utc ? date.minutesUTC : date.minutes);
				case 'r': return DAYS[_utc ? date.dayUTC : date.day] + ', ' + IntUtil.lpad0_2(_utc ? date.dateUTC : date.date) + ' ' + MONTHS[_utc ? date.monthUTC : date.month] + ' ' +
					(_utc ? date.fullYearUTC : date.fullYear) + ' ' + _formatTime(date) + ' ' + (_utc ? '+0000' : _formatTZ(date));
				case 'R': return IntUtil.lpad0_2(_utc ? date.hoursUTC : date.hours) + ':' + IntUtil.lpad0_2(_utc ? date.minutesUTC : date.minutes);
				case 's': return (_utc ? date.timeUTC : date.time).toString();
				case 'S': return IntUtil.lpad0_2(_utc ? date.secondsUTC : date.seconds);
				case 'T': return _formatTime(date);
				case 'Y': return (_utc ? date.fullYearUTC : date.fullYear).toString();
				case 'z': return _utc ? '+0000' : _formatTZ(date);
				case 'U': _utc = true; return '';
				case 'L': _utc = false; return '';
				default: return null;
			}
		}

		[Inline]
		protected static function _formatTime(date:Date):String {
			return IntUtil.lpad0_2(_utc ? date.hoursUTC : date.hours) + ':' + IntUtil.lpad0_2(_utc ? date.minutesUTC : date.minutes) + ':' + IntUtil.lpad0_2(_utc ? date.secondsUTC : date.seconds);
		}

		protected static function _formatTZ(date:Date):String {
			var tz:int = -date.timezoneOffset;
			var str:String = (tz < 0) ? '-' : '+';
			tz = Math.abs(tz);
			return str + IntUtil.lpad0_2(tz / 60) + IntUtil.lpad0_2(tz % 60);
		}

		protected static var _utc:Boolean;
	}
}
