package com.razorscript.utils {
	import com.razorscript.debug.Debug;
	import com.razorscript.debug.Logger;
	import flash.utils.ByteArray;
	import flash.utils.getTimer;

	/**
	 * Binary data utility functions.
	 *
	 * @author Gene Pavlovsky
	 */
	public class BinUtil {
		/**
		 * Returns a clone of the specified byte array.
		 *
		 * @param byteArray A byte array.
		 * @return The cloned byte array.
		 */
		public static function cloneByteArray(byteArray:ByteArray):ByteArray {
			var resultByteArray:ByteArray = new ByteArray();
			resultByteArray.writeBytes(byteArray);
			resultByteArray.position = byteArray.position;
			return resultByteArray;
		}

		/**
		 * Uncompresses a byte array using the specified compression method.
		 *
		 * @param byteArray A byte array.
		 * @param compression A compression algorithm.
		 */
		CONFIG::release
		public static function uncompressByteArray(byteArray:ByteArray, compression:String):void {
			byteArray.uncompress(compression);
		}

		/**
		 * Compresses a byte array using the specified compression method.
		 *
		 * @param byteArray A byte array.
		 * @param compression A compression algorithm.
		 */
		CONFIG::release
		public static function compressByteArray(byteArray:ByteArray, compression:String):void {
			byteArray.compress(compression);
		}

		CONFIG::debug
		public static function uncompressByteArray(byteArray:ByteArray, compression:String):void {
			if (Debug.instance.debugBinUtil) {
				var length:int = byteArray.length;
				var startTime:int = getTimer();
			}
			byteArray.uncompress(compression);
			if (Debug.instance.debugBinUtil) {
				Logger.debug(BinUtil, length + '->' + byteArray.length + ' bytes', '(' + int(100 * length / byteArray.length) + '%)',
					'time=' + (getTimer() - startTime) + ' ms');
			}
		}

		CONFIG::debug
		public static function compressByteArray(byteArray:ByteArray, compression:String):void {
			if (Debug.instance.debugBinUtil) {
				var length:int = byteArray.length;
				var startTime:int = getTimer();
			}
			byteArray.compress(compression);
			if (Debug.instance.debugBinUtil) {
				Logger.debug(BinUtil, length + '->' + byteArray.length + ' bytes', '(' + int(100 * byteArray.length / length) + '%)',
					'time=' + (getTimer() - startTime) + ' ms');
			}
		}
	}
}
