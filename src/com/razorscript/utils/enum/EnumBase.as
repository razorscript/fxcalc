package com.razorscript.utils.enum {
	
	/**
	 * Base class for enumerations.
	 *
	 * @author Gene Pavlovsky
	 */
	public class EnumBase implements IEnum {
		protected static const LOCK:Object = {};
		
		/**
		 * @private
		 */
		public function EnumBase(lock:Object) {
			if (lock != LOCK)
				throw new Error('Illegal enum constructor access.');
		}
		
		public function toString():String	{
			return EnumUtil.getName(this);
		}
		
		public function toJSON(key:String):* {
			return EnumUtil.getName(this);
		}
		
		public function valueOf():Number {
			return EnumUtil.getOrdinal(this);
		}
	}
}
