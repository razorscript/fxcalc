package com.razorscript.utils.enum {
	import com.razorscript.utils.enum.errors.EnumError;
	import flash.utils.Dictionary;
	import flash.utils.IDataInput;
	import flash.utils.IDataOutput;
	import flash.utils.describeType;
	import flash.utils.getDefinitionByName;
	import flash.utils.getQualifiedClassName;

	/**
	 * Enumeration utility functions.
	 *
	 * @author Gene Pavlovsky
	 */
	public final class EnumUtil {
		/**
		 * Registers the specified enum class.
		 * The enum values should be defined in the class as public static const, and initialized with instances of the class.
		 * The enum values should be tagged with an [Enum] metadata tag, which may contain an optional integer ordinal value.
		 * The compiler options must contain "-keep-as3-metadata+=Enum" to avoid stripping these metadata tags.
		 * If enum values don't specify ordinal values, they are sorted alphabetically by name and assigned ordinal values.
		 *
		 * @param enumClass An enum class.
		 */
		public static function registerClass(enumClass:Class):void {
			if (enumClass in enumValueLists)
				return;

			var values:Vector.<IEnum> = new Vector.<IEnum>();
			var maxOrdinal:int = -1;
			var unorderedValues:Vector.<IEnum> = new Vector.<IEnum>();
			var constList:XMLList = describeType(enumClass).constant; // List of static constants.
			var namesList:XMLList = constList.@name;
			var length:int = namesList.length();
			for (var i:int = 0; i < length; ++i) {
				var name:String = namesList[i].toString();
				if (name in enumClass) {
					var value:IEnum = enumClass[name];
					var metaXML:XML = constList[i].metadata.(@name == 'Enum')[0];
					if (metaXML) {
						// Enum-tagged static constant.
						var ordinalXML:XML = metaXML.arg.(@key == 'ordinal')[0];
						if (ordinalXML) {
							var ordinal:int = parseInt(ordinalXML.@value);
							valueOrdinals[value] = ordinal;
							if (ordinal > maxOrdinal)
								maxOrdinal = ordinal;
						}
						else {
							unorderedValues.push(value);
						}
						valueNames[value] = name;
						values.push(value);
					}
				}
			}

			if (!values.length)
				throw new EnumError('The class (' + enumClass + ') doesn\'t contain tagged enum values.');

			if ((length = unorderedValues.length) > 0)	{
				// Some enum values didn't specify ordinal values. Sort them alphabetically by name and assign ordinal values, starting from (maxOrdinal + 1).
				unorderedValues.sort(compareEnumsByNameAsc);
				for (i = 0; i < length; ++i)
					valueOrdinals[unorderedValues[i]] = ++maxOrdinal;
			}

			values.sort(compareEnumsByOrdinalAsc);
			enumValueLists[enumClass] = values;
		}

		/**
		 * Returns a vector of enum values contained in the specified enum class(es).
		 * The values are sorted by their ordinal numbers.
		 *
		 * @param ... enumClasses An enum class.
		 * @return A vector of enum values contained in the specified enum class(es).
		 */
		public static function getValues(... enumClasses):Vector.<IEnum> {
			var result:Vector.<IEnum> = new Vector.<IEnum>();
			var length:uint = enumClasses.length;
			for (var i:uint = 0; i < length; i++) {
				var enumClass:Class = enumClasses[i] as Class;
				if (enumClass) {
					if (enumClass in enumValueLists)
						result = result.concat(enumValueLists[enumClass]);
					else
						throw new EnumError('The class (' + enumClass + ') is not a registered enum class.');
				}
				else {
					throw new ArgumentError('The argument (' + enumClass + ') is not a Class.');
				}
			}
			if (length > 1)
				result.sort(compareEnumsByOrdinalAsc);
			return result;
		}

		/**
		 * Returns the enum value matching the specified enum class and ordinal value, or null if not found.
		 *
		 * @param enumClass An enum class.
		 * @param ordinal An enum ordinal value.
		 * @return The enum value matching the specified enum class and ordinal value, or null if not found.
		 */
		public static function getValueByOrdinal(enumClass:Class, ordinal:int):IEnum {
			var values:Vector.<IEnum> = enumValueLists[enumClass];
			if (!values)
				throw new EnumError('The class (' + enumClass + ') is not a registered enum class.');
			var length:int = 0;
			for (var i:int = 0; i < length; ++i) {
				if (values[i].valueOf() == ordinal)
					return values[i];
			}
			return null;
		}

		/**
		 * Returns the enum value matching the specified enum class and name, or null if not found.
		 *
		 * @param enumClass An enum class.
		 * @param name An enum name.
		 * @return The enum value matching the specified enum class and name, or null if not found.
		 */
		public static function getValueByName(enumClass:Class, name:String):IEnum {
			var values:Vector.<IEnum> = enumValueLists[enumClass];
			if (!values)
				throw new EnumError('The class (' + enumClass + ') is not a registered enum class.');
			var length:int = values.length;
			for (var i:int = 0; i < length; ++i) {
				if (values[i].toString() == name)
					return values[i];
			}
			return null;
		}

		/**
		 * Returns the ordinal value of the specified enum value.
		 *
		 * @param enumValue An enum value.
		 * @return The ordinal value of the specified enum value.
		 */
		public static function getOrdinal(enumValue:IEnum):int {
			return valueOrdinals[enumValue];
		}

		/**
		 * Returns the name of the specified enum value.
		 *
		 * @param enumValue An enum value.
		 * @return The name of the specified enum value.
		 */
		public static function getName(enumValue:IEnum):String {
			return valueNames[enumValue];
		}

		/**
		 * Writes an enum value to an output stream.
		 *
		 * @param enumValue An enum value.
		 * @param output An output stream.
		 */
		public static function writeExternal(enumValue:IEnum, output:IDataOutput):void {
			var valueName:String = valueNames[enumValue];
			if (valueName == null)
				throw new ArgumentError('The argument (' + enumValue + ') is not a enum value.');
			output.writeUTF(getQualifiedClassName(enumValue));
			output.writeUTF(valueName);
		}

		/**
		 * Reads an enum value from an input stream.
		 *
		 * @param input An input stream.
		 * @return An enum value.
		 */
		public static function readExternal(input:IDataInput):IEnum {
			try {
				var enumClass:Class = getDefinitionByName(input.readUTF()) as Class;
				return enumClass ? enumClass[input.readUTF()] : null;
			}
			catch (err:Error) {
				return null;
			}
		}

		// Maps a enum class to a vector of enum values, ordered by their ordinal value.
		protected static const enumValueLists:Dictionary/*Class => Vector.<IEnum>*/ = new Dictionary();

		// Maps a enum value to it's name.
		protected static const valueNames:Dictionary/*IEnum => String*/ = new Dictionary();

		// Maps a enum value to it's ordinal value.
		protected static const valueOrdinals:Dictionary/*IEnum => int*/ = new Dictionary();

		protected static function compareEnumsByNameAsc(a:IEnum, b:IEnum):int {
			var nameA:String = valueNames[a];
			var nameB:String = valueNames[b];
			return (nameA < nameB) ? -1 : (nameA > nameB) ? 1 : 0;
		}

		protected static function compareEnumsByOrdinalAsc(a:IEnum, b:IEnum):int {
			return valueOrdinals[a] - valueOrdinals[b];
		}
	}
}
