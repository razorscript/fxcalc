package com.razorscript.utils.enum.errors {
	
	/**
	 * Thrown when an exceptional enumeration-related condition has occurred.
	 *
	 * @author Gene Pavlovsky
	 */
	public final class EnumError extends Error {
		public function EnumError(message:String, id:int = 0) {
			super(message, id);
		}
	}
}
