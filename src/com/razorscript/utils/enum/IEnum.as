package com.razorscript.utils.enum {
	
	/**
	 * A enumeration.
	 *
	 * @author Gene Pavlovsky
	 */
	public interface IEnum {
		function toString():String;
		function toJSON(key:String):*;
		function valueOf():Number;
	}
}
