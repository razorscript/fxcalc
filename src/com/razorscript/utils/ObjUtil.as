package com.razorscript.utils {
	import com.razorscript.ds.BitVector;
	import com.razorscript.utils.objectParser.ObjectParser;
	import com.razorscript.utils.objectParser.conversions.ObjectParserConversionFun;
	import flash.utils.ByteArray;

	/**
	 * Object utility functions.
	 *
	 * @author Gene Pavlovsky
	 */
	public class ObjUtil {
		/**
		 * Returns true if a value is an object.
		 *
		 * @param value A value.
		 * @return Boolean true if the value is an object.
		 */
		[Inline]
		public static function isObject(value:*):Boolean {
			return typeof(value) == 'object';
		}

		/**
		 * Delets all properties of an object and returns that object.
		 *
		 * @param obj An object.
		 * @return The object with all properties deleted.
		 */
		public static function clear(obj:Object):Object {
			for (var name:String in obj)
				delete obj[name];
			return obj;
		}

		/**
		 * Copies all properties from one object to another, and returns the destination object.
		 * The copied object is a shallow copy of the source object.
		 * Properties whose values are objects are copied by reference.
		 *
		 * @param src The source object.
		 * @param dest The destination object.
		 * @return The desination object.
		 */
		public static function copy(src:Object, dest:Object):Object {
			for (var name:String in src)
				dest[name] = src[name];
			return dest;
		}

		/**
		 * Returns a clone of an object.
		 * The cloned object is a shallow copy of the source object.
		 *
		 * @param obj An object.
		 * @return The cloned object.
		 */
		public static function clone(obj:Object):Object {
			return copy(obj, {});
		}

		/**
		 * Copies all properties from one object to another, and returns the destination object.
		 * The copied object is a deep copy of the source object.
		 * Properties whose values are objects are recursively copied, if they exist on the destination object.
		 * Otherwise the values are cloned, or copied by reference if the useDeepClone argument is false (useful if the source object is disposable).
		 *
		 * @param src The source object.
		 * @param dest The destination object.
		 * @param useDeepClone Whether to clone object values for properties missing on the destination object. If false, object values are copied by reference.
		 * @return The desination object.
		 */
		public static function deepCopy(src:Object, dest:Object, useDeepClone:Boolean = true):Object {
			for (var name:String in src) {
				var value:* = src[name];
				if (isObject(dest[name])) {
					if (isObject(value))
						deepCopy(value, dest[name], useDeepClone);
					else
						dest[name] = value;
				}
				else {
					dest[name] = (useDeepClone && isObject(value)) ? deepCopy(value, {}) : value;
				}
			}
			return dest;
		}

		/**
		 * Returns a clone of an object.
		 * The cloned object is a deep copy of the source object.
		 *
		 * @param obj An object.
		 * @return The cloned object.
		 */
		public static function deepClone(obj:Object):Object {
			return deepCopy(obj, {});
		}

		/**
		 * Converts a value of an arbitrary type to it's binary representation and returns it as a BitVector.
		 * The value is written to a ByteArray then read back as integers and converted to binary.
		 *
		 * @param value A value of an arbitrary data type.
		 * @param bitVector If not null, the bits are stored in the specified bit vector, otherwise a new bit vector is created.
		 * @return A BitVector containing the binary representation of value.
		 */
		public static function valueToBitVector(value:*, bitVector:BitVector = null):BitVector {
			clearByteArray();
			if ((value is int) || (value is uint))
				byteArray.writeUnsignedInt(value);
			else if (value is Number)
				byteArray.writeDouble(value);
			else if (value is String)
				byteArray.writeUTFBytes(value);
			else if (value is Boolean)
				byteArray.writeBoolean(value);
			else
				byteArray.writeObject(value);
			byteArray.position = 0;

			if (bitVector)
				bitVector.length = byteArray.length * 8;
			else
				bitVector = new BitVector(byteArray.length * 8);

			var i:int = bitVector.numWords - 1;
			// The last word contains the most significant bits, could be less than 32 bits.
			var highBytes:int = byteArray.length % 4;
			if (highBytes) {
				var highWord:uint = byteArray.readUnsignedByte();
				while (--highBytes) {
					highWord <<= 8;
					highWord |= byteArray.readUnsignedByte();
				}
				bitVector.setWordAt(i--, highWord);
			}
			while (i >= 0)
				bitVector.setWordAt(i--, byteArray.readUnsignedInt());
			clearByteArray();
			return bitVector;
		}

		protected static var byteArray:ByteArray;

		protected static function clearByteArray():void {
			if (byteArray)
				byteArray.clear();
			else
				byteArray = new ByteArray();
		}

		protected static var _objectParser:ObjectParser;
		/**
		 * The ObjectParser used by parseObject() and parseJSON().
		 *
		 * For the list of supported conversions, see FormatterConversionFun.
		 * @see com.razorscript.utils.formatter.conversions.FormatterConversionFun#extend()
		 *
		 * For the list of provided handlers, see FormatterConversionFun.
		 * @see com.razorscript.utils.formatter.handlers.FormatterHandlerFun#extend()
		 */
		public static function get objectParser():ObjectParser {
			return _objectParser ? _objectParser : (_objectParser = ObjectParserConversionFun.extend(new ObjectParser()));
		}

		public static function set objectParser(value:ObjectParser):void {
			_objectParser = value;
		}

		/**
		 * Parses an object into the specified target object.
		 * Supports custom conversions using conversion functions.
		 * The ObjectParser used by this method supports several conversions by default. For the list of supported conversions, see ObjectParserConversionFun.
		 * @see com.razorscript.utils.objectParser.conversions.ObjectParserConversionFun#extend()
		 *
		 * @see com.razorscript.utils.objectParser.ObjectParser#parseObjectTo()
		 *
		 * @param obj The source object.
		 * @param target The destination object.
		 */
		public static function parseObjectTo(obj:Object, target:Object):void {
			objectParser.parseObjectTo(obj, target);
		}

		/**
		 * Parses a JSON-formatted string into the specified target object.
		 * Supports custom conversions using conversion functions.
		 * The ObjectParser used by this method supports several conversions by default. For the list of supported conversions, see ObjectParserConversionFun.
		 * @see com.razorscript.utils.objectParser.conversions.ObjectParserConversionFun#extend()
		 *
		 * @see com.razorscript.utils.objectParser.ObjectParser#parseObjectTo()
		 *
		 * @param text The JSON string.
		 * @param target The destination object.
		 * @param reviver A function that transforms each key/value pair that is parsed.
		 */
		public static function parseJSONTo(text:String, target:Object, reviver:Function = null):void {
			objectParser.parseObjectTo(JSON.parse(text, reviver), target);
		}
	}
}
