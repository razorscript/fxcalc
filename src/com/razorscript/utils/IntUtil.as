package com.razorscript.utils {

	/**
	 * Integer utility functions.
	 *
	 * @author Gene Pavlovsky
	 */
	public class IntUtil {
		/**
		 * Left-pads the string representation of an unsigned integer with zeros to 2 characters. Prepends as many zeros as possible without exceeding length.
		 *
		 * @param num An unsigned integer.
		 * @return Left-padded string representation of the number at least 2 characters long. ("00" to "99").
		 */
		[Inline]
		public static function lpad0_2(num:uint):String {
			return (num < 10) ? ('0' + num) : num.toString();
		}

		/**
		 * Left-pads the string representation of an unsigned integer with zeros to 3 characters. Prepends as many zeros as possible without exceeding length.
		 *
		 * @param num An unsigned integer.
		 * @return Left-padded string representation of the number at least 3 characters long. ("000" to "999").
		 */
		[Inline]
		public static function lpad0_3(num:uint):String {
			return (num < 10) ? ('00' + num) : (num < 100) ? ('0' + num) : num.toString();
		}

		/**
		 * Left-pads the string representation of an unsigned integer with zeros to 4 characters. Prepends as many zeros as possible without exceeding length.
		 *
		 * @param num An unsigned integer number.
		 * @return Left-padded string representation of the number at least 4 characters long. ("0000" to "9999").
		 */
		[Inline]
		public static function lpad0_4(num:uint):String {
			return (num < 10) ? ('000' + num) : (num < 100) ? ('00' + num) : (num < 1000) ? ('0' + num) : num.toString();
		}
	}
}
