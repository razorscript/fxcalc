package com.razorscript.utils.supportClasses {

	/**
	 * Property information.
	 *
	 * @author Gene Pavlovsky
	 */
	public class PropertyInfo {
		public static const READ_WRITE:int = 0;
		public static const READ_ONLY:int = 1;
		public static const WRITE_ONLY:int = 2;

		/**
		 * Constructor.
		 *
		 * @param typeName Qualified class name.
		 * @param classRef Class reference.
		 * @param access Access type.
		 */
		public function PropertyInfo(typeName:String, classRef:Class, access:int = -1) {
			this.typeName = typeName;
			this.classRef = classRef;
			this.access = access;
		}

		/** Qualified class name. */
		public var typeName:String;
		/** Class reference. */
		public var classRef:Class;
		/** Access type. */
		public var access:int;

		public function toString():String {
			return '[PropertyInfo typeName="' + typeName + '" classRef=' + classRef + ' access=' + access + ']';
		}
	}
}
