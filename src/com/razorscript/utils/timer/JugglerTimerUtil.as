package com.razorscript.utils.timer {
	import com.razorscript.utils.timer.ITimerUtil;
	import starling.animation.IAnimatable;
	import starling.animation.Juggler;
	import starling.core.Starling;
	
	/**
	 * An implementation of ITimerUtil that uses Starling's Juggler.
	 *
	 * @author Gene Pavlovsky
	 */
	public class JugglerTimerUtil implements ITimerUtil {
		public function JugglerTimerUtil(juggler:Juggler = null) {
			this.juggler = juggler ? juggler : Starling.current.juggler;
		}
		
		public function delayCall(fun:Function, delay:Number, ... args):Object {
			if (fun) {
				if (delay > 0) {
					args.unshift(fun, delay / 1000);
					return juggler.delayCall.apply(null, args);
				}
				else if (delay == 0) {
					fun.apply(null, args);
				}
				else {
					throw new ArgumentError('The delay argument (' + delay + ') must be a non-negative number');
				}
			}
			return null;
		}
		
		public function repeatCall(fun:Function, interval:Number, repeatCount:int = 0, ... args):Object {
			if (fun) {
				if (interval > 0) {
					args.unshift(fun, interval / 1000, repeatCount);
					return juggler.repeatCall.apply(null, args);
				}
				else {
					throw new ArgumentError('The interval argument (' + interval + ') must be a positive number');
				}
			}
			return null;
		}
		
		public function remove(delayedCall:Object):void {
			juggler.remove(delayedCall as IAnimatable);
		}
		
		protected var juggler:Juggler;
	}
}
