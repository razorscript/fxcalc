package com.razorscript.utils.timer.supportClasses {
	import com.razorscript.debug.Debug;
	import com.razorscript.razor_internal;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.utils.getTimer;
	import flash.utils.Timer;
	
	/**
	 * A pooled Timer-based delayed call used by TimerTimerUtil.
	 *
	 * @author Gene Pavlovsky
	 */
	public class TimerDelayedCall {
		/**
		 * TimerDelayedCall objects can only be created by static fromPool(), and can't be instantiated directly.
		 *
		 * @private
		 */
		public function TimerDelayedCall(fun:Function, args:Array, interval:Number, repeatCount:int) {
			this.fun = fun;
			this.args = args;
			timer = new Timer(interval, repeatCount);
			timer.addEventListener(TimerEvent.TIMER, onTimer);
			timer.addEventListener(TimerEvent.TIMER_COMPLETE, onTimerComplete);
			timer.start();
		}
		
		public function remove():void {
			timer.stop();
			toPool();
		}
		
		protected function onTimer(e:Event):void {
			fun.apply(null, args);
		}
		
		protected function onTimerComplete(e:Event):void {
			toPool();
		}
		
		protected var timer:Timer;
		protected var fun:Function;
		protected var args:Array;
		protected var count:int;
		
		protected static var pool:Vector.<TimerDelayedCall> = new Vector.<TimerDelayedCall>();
		
		razor_internal static function fromPool(fun:Function, args:Array, interval:Number, repeatCount:int):TimerDelayedCall {
			if (pool.length) {
				var call:TimerDelayedCall = pool.pop();
				call.fun = fun;
				call.args = args;
				call.timer.reset();
				call.timer.delay = interval;
				call.timer.repeatCount = repeatCount;
				call.timer.start();
				return call;
			}
			return new TimerDelayedCall(fun, args, interval, repeatCount);
		}
		
		protected function toPool():void {
			if (!fun)
				return;
			fun = null;
			args = null;
			pool.push(this);
			CONFIG::debug {
				Debug.instance.timerDelayedCallPoolSize = pool.length;
			}
		}
	}
}
