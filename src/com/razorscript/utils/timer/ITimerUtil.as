package com.razorscript.utils.timer {

	/**
	 * Timer utility functions.
	 *
	 * @author Gene Pavlovsky
	 */
	public interface ITimerUtil {
		/**
		 * Delays the execution of a function until the specified number of milliseconds has passed.
		 * To cancel the call, pass the returned object to remove().
		 *
		 * @param fun A function.
		 * @param delay The delay in milliseconds.
		 * @param ... args An argument.
		 * @return An object that can be passed to remove() to cancel the call.
		 */
		function delayCall(fun:Function, delay:Number, ... args):Object;

		/**
		 * Runs a function at the specified interval (in milliseconds), with an optional repeat count.
		 * If the repeat count is zero, the function will repeat indefinitely.
		 * To cancel the call, pass the returned object to remove().
		 *
		 * @param fun A function.
		 * @param interval The interval in milliseconds.
		 * @param repeatCount The number of times to run the function. If set to 0, the function will repeat indefinitely.
		 * @param ... args An argument.
		 * @return An object that can be passed to remove() to cancel the call.
		 */
		function repeatCall(fun:Function, interval:Number, repeatCount:int = 0, ... args):Object;

		/**
		 * Removes a scheduled delayed call.
		 * If the argument is null, does nothing.
		 *
		 * @param delayedCall An object.
		 */
		function remove(delayedCall:Object):void;
	}
}
