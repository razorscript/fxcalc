package com.razorscript.utils.timer {
	import com.razorscript.razor_internal;
	import com.razorscript.utils.timer.ITimerUtil;
	import com.razorscript.utils.timer.supportClasses.TimerDelayedCall;
	
	use namespace razor_internal;
	
	/**
	 * An implementation of Object that uses Timer.
	 *
	 * @author Gene Pavlovsky
	 */
	public class TimerTimerUtil implements ITimerUtil {
		public function TimerTimerUtil() {
		}
		
		public function delayCall(fun:Function, delay:Number, ... args):Object {
			if (fun) {
				if (delay > 0)
					return TimerDelayedCall.fromPool(fun, args, delay, 1);
				else if (delay == 0)
					fun.apply(null, args);
				else
					throw new ArgumentError('The delay argument (' + delay + ') must be a non-negative number');
			}
			return null;
		}
		
		public function repeatCall(fun:Function, interval:Number, repeatCount:int = 0, ... args):Object {
			if (fun) {
				if (interval > 0)
					return TimerDelayedCall.fromPool(fun, args, interval, repeatCount);
				else
					throw new ArgumentError('The interval argument (' + interval + ') must be a positive number');
			}
			return null;
		}
		
		public function remove(delayedCall:Object):void {
			if (delayedCall)
				(delayedCall as TimerDelayedCall).remove();
		}
	}
}
