package com.razorscript.utils.formatter.conversions {
	import com.razorscript.utils.formatter.Formatter;
	import com.razorscript.utils.NumUtil;

	/**
	 * Formatter conversion functions.
	 *
	 * @author Gene Pavlovsky
	 */
	public class FormatterConversionFun {
		/**
		 * Adds conversions to a formatter and returns that formatter.
		 *
		 * Provided conversions:
		 * 	Number:
		 * 		i		converts the value to an int using parseInt().
		 * 		f		converts the value to a Number using parseFloat().
		 * 		n		converts the value to a Number using NumUtil.parseNumber().
		 *
		 * 	Case:
		 * 		u		converts a string to upper case using String.toUpperCase().
		 * 		l		converts a string to lower case using String.toLowerCase().
		 *
		 * 	JSON:
		 * 		j		converts the value to a string using JSON.stringify().
		 * 		J		converts the value to a string using JSON.stringify() with a gap of two spaces.
		 *
		 * 	URI:
		 * 		eu	encodes a string into a valid URI (string) using encodeURI().
		 * 		du	decodes an encoded URI (string) into a string using decodeURI().
		 * 		ec	encodes a string into a valid URI component (string) using encodeURIComponent().
		 * 		dc	decodes an encoded URI component (string) into a string using decodeURIComponent().
		 *
		 * @param formatter A formatter.
		 * @param numberConv Whether to add number conversions.
		 * @param caseConv Whether to add case conversions.
		 * @param jsonConv Whether to add JSON conversions.
		 * @param uriConv Whether to add URI conversions.
		 * @return The formatter.
		 */
		public static function extend(formatter:Formatter, numberConv:Boolean = true, caseConv:Boolean = true, jsonConv:Boolean = true, uriConv:Boolean = true):Formatter {
			if (numberConv) {
				formatter.addConversion('i', parseInt);
				formatter.addConversion('f', parseFloat);
				formatter.addConversion('n', NumUtil.parseNumber);
			}

			if (caseConv) {
				formatter.addConversion('u', function (value:*):* { return value.toString().toUpperCase(); });
				formatter.addConversion('l', function (value:*):* { return value.toString().toLowerCase(); });
			}

			if (jsonConv) {
				formatter.addConversion('j', JSON.stringify);
				formatter.addConversion('J', function (value:*):* { return JSON.stringify(value, null, 2); });
			}

			if (uriConv) {
				formatter.addConversion('eu', encodeURI);
				formatter.addConversion('du', decodeURI);
				formatter.addConversion('ec', encodeURIComponent);
				formatter.addConversion('dc', decodeURIComponent);
			}

			return formatter;
		}
	}
}
