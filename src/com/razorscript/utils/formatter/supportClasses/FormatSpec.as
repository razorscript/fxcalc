package com.razorscript.utils.formatter.supportClasses {

	/**
	 * Format specification for com.razorscript.utils.Formatter.
	 * Similar to Python's format and C's printf.
	 *
	 * @see https://docs.python.org/3/library/string.html#format-string-syntax
	 * @see https://en.wikipedia.org/wiki/Printf_format_string
	 *
	 * @author Gene Pavlovsky
	 */
	public class FormatSpec {
		/** Forces the field to be left-aligned within the available space (this is the default for most objects). */
		public static const ALIGN_LEFT:String = '<';
		/** Forces the field to be right-aligned within the available space (this is the default for numbers). */
		public static const ALIGN_RIGHT:String = '>';
		/** Forces the padding to be placed after the sign and/or prefix (if any) but before the digits. This is used for fields such as '-0010' or '0x00ff'. Only valid for numeric types. */
		public static const ALIGN_SIGN:String = '=';
		/** Forces the field to be centered within the available space. */
		public static const ALIGN_CENTER:String = '^';

		/** Indicates that a sign should be used for both non-negative as well as negative numbers. */
		public static const SIGN_PLUS:String = '+';
		/** Indicates that a sign should be used only for negative numbers (this is the default behavior). */
		public static const SIGN_MINUS:String = '-';
		/** Indicates that a leading space should be used on non-negative numbers, and a minus sign on negative numbers. */
		public static const SIGN_SPACE:String = ' ';

		/**
		 * Use "alternate form" for the conversion. The alternate form is defined differently for different types.
		 * This option is only valid for unsigned integer (uint) and float (Number) types.
		 * For unsigned integers, when binary, octal, or hexadecimal output is used, this option adds a prefix '0b', '0o', or '0x' respectively to the output value.
		 * For floats alternate form causes the result of the conversion to always contain a decimal point, even if no digits follow it.
		 * Normally, a decimal point appears in the result of these conversions only if a digit follows it.
		 * In addition, for types 'g', 'G', 'a', 'A', trailing zeros are not removed from the result.
		 */
		public static const ALT_FORM:String = '#';

		/**
		 * Signals the use of a comma for a digit grouping separator.
		 * This option is only valid for integer (int, uint) and float (Number) types.
		 * For unsigned integers, when binary output is used, separates digits in groups of 4, corresponding to a nibble, or equivalently to a hexadecimal digit.
		 * For unsigned integers, when hexadecimal output is used, separates digits in groups of 2, corresponding to a byte.
		 * For signed or unsigned integers, when decimal output is used, separates digits in groups of 3 (groups of thousands).
		 * For floats, separates digits before the decimal point in groups of 3 (groups of thousands).
		 */
		public static const GROUP_COMMA:String = ',';
		/**
		 * Signals the use of an underscore for a digit grouping separator.
		 * @see com.razorscript.utils.formatter.supportClasses.FormatSpec:GROUP_COMMA
		 */
		public static const GROUP_UNDERSCORE:String = '_';
		/**
		 * Signals the use of a space for a digit grouping separator.
		 * @see com.razorscript.utils.formatter.supportClasses.FormatSpec:GROUP_COMMA
		 */
		public static const GROUP_SPACE:String = '*';

		/** Enables sign-aware zero-padding for numeric types. This is equivalent to a fill character of '0' with an alignment of '='. */
		public static const PAD_ZERO:String = '0';

		/** String format. This is the default type for strings and may be omitted. */
		public static const TYPE_STR_STRING:String = 's';

		/** Decimal signed integer. Outputs the number as int in base 10. */
		public static const TYPE_INT_DECIMAL:String = 'd';
		/** This is the same as 'd'. */
		public static const TYPE_INT_DECIMAL_ALIAS:String = 'i';

		/** Decimal unsigned integer. Outputs the number as uint in base 10. */
		public static const TYPE_UINT_UNSIGNED:String = 'u';
		/** Binary unsigned format. Outputs the number as uint in base 2. */
		public static const TYPE_UINT_BINARY:String = 'b';
		/** Octal unsigned format. Outputs the number as uint in base 8. */
		public static const TYPE_UINT_OCTAL:String = 'o';
		/** Hexadecimal unsigned format. Outputs the number as uint in base 16, using lowercase letters for digits above 9. */
		public static const TYPE_UINT_HEX:String = 'x';
		/** Hexadecimal unsigned format. Outputs the number as uint in base 16, using uppercase letters for digits above 9. */
		public static const TYPE_UINT_HEX_U:String = 'X';
		/** Character. Converts the integer to the corresponding unicode character. */
		public static const TYPE_UINT_CHAR:String = 'c';

		// TODO: Add exponential engineering formats (e / E), and engineering with SI prefix format - Or add them as options to EXP formats?!

		/** Exponential notation. Outputs the number in scientific notation using 'e' to indicate the exponent. The default precision is 6. */
		public static const TYPE_FLOAT_EXP:String = 'e';
		/** Exponential notation. This is the same as 'e', except that it uses 'E' to indicate the exponent, and converts 'NaN' to 'NAN' and Infinity to 'INFINITY'. */
		public static const TYPE_FLOAT_EXP_U:String = 'E';
		/** Fixed point. Outputs the number as a fixed-point number. The default precision is 6. */
		public static const TYPE_FLOAT_FIXED:String = 'f';
		/** Fixed point. This is the same as 'f', except that it converts 'NaN' to 'NAN' and Infinity to 'INFINITY'. */
		public static const TYPE_FLOAT_FIXED_U:String = 'F';
		/**
		 * General format. For a given precision p, rounds the number to p significant digits and formats the number in either fixed-point or exponential notation, whichever is more appropriate for it's magnitude.
		 * If the number's decimal exponent exp is in the range (fixedMinExponent <= exp < precision), the fixed-point notation is used, otherwise exponential notation is used.
		 * In both cases insignificant trailing zeros after the decimal point are not included. If there are no digits after the decimal point, the decimal point is not included.
		 * If precision is 0, precision is set to 1. The default precision is 6.
		 */
		public static const TYPE_FLOAT_GENERAL:String = 'g';
		/** General format. This is the same as 'g', except that it uses 'E' to indicate the exponent, and converts 'NaN' to 'NAN' and Infinity to 'INFINITY'. */
		public static const TYPE_FLOAT_GENERAL_U:String = 'G';
		/** Hexfloat format. Outputs the number as a double in hexadecimal format (in the form [-]0x[01].hhhhhhhhhhhhhp[-+]ddd), with precision as high as needed to represent the particular value. */
		public static const TYPE_FLOAT_HEXFLOAT:String = 'a';
		/** Hexfloat format. This is the same as 'a', except that it uses uppercase letters for digits above 9 and 'P' to indicate the exponent, and converts 'NaN' to 'NAN' and Infinity to 'INFINITY'. */
		public static const TYPE_FLOAT_HEXFLOAT_U:String = 'A';
		/** Percentage. Multiplies the number by 100 and displays in fixed ('f') format, followed by a percent sign '%'. */
		public static const TYPE_FLOAT_PERCENTAGE:String = 'p';
		/** Default type for float (Number) values when type is omitted in the format spec. This is the same as 'g', except that the default precision is 17. */
		public static const TYPE_FLOAT_DEFAULT:String = 'n';

		public function FormatSpec(str:String = '') {
			if (str)
				parse(str);
		}

		public var source:String;

		public var fill:String;
		public var align:String;
		public var sign:String;
		public var altForm:Boolean;
		public var upperCase:Boolean;
		public var groupSep:String;
		public var width:int;
		public var precision:int;
		public var type:String;

		protected static const INT_TYPES:String = 'di';
		public function get isIntType():Boolean {
			return INT_TYPES.indexOf(type) != -1;
		}

		protected static const UINT_TYPES:String = 'uboxXc';
		public function get isUIntType():Boolean {
			return UINT_TYPES.indexOf(type) != -1;
		}

		protected static const FLOAT_TYPES:String = 'eEfFgGnaAp';
		public function get isFloatType():Boolean {
			return FLOAT_TYPES.indexOf(type) != -1;
		}

		protected static const FORMAT_SPEC_RX:RegExp = /^ (.? [<>=^])? ([-+ ])? (\#)? (0)? (\d+)? ([_,*])? (\. \d*)? ([sdiuboxXceEfFgGnaAp])? $/x;

		/**
		 * Parses the format spec string and sets the properties of this object.
		 *
		 * format_spec   ::=  [[fill] align] [sign] [#] [0] [width] [group_sep] [. [precision]] [type]
		 * fill          ::=  <any character>
		 * align         ::=  "<" | ">" | "=" | "^"
		 * sign          ::=  "+" | "-" | " "
		 * group_sep     ::=  "," | "_" | "*"
		 * width         ::=  integer
		 * precision     ::=  integer
		 * type          ::=  "s" | "d" | "i" | "u" | "b" | "o" | "x" | "X" | "c" | "e" | "E" | "f" | "F" | "g" | "G" | "n" | "a" | "A" | "p"
		 *
		 * @param str A format spec.
		 */
		public function parse(str:String):void {
			source = str;
			var rxMatch:Array = FORMAT_SPEC_RX.exec(str);
			if (!rxMatch)
				throw new Error('Invalid format spec: "' + str + '".');

			fill = ' ';
			align = '';
			str = rxMatch[1];
			if (str) {
				if (str.length > 1) {
					fill = str.charAt(0);
					align = str.charAt(1);
				}
				else {
					align = str.charAt(0);
				}
			}

			sign = rxMatch[2] ? rxMatch[2] : SIGN_MINUS;
			altForm = rxMatch[3] == ALT_FORM;

			if (rxMatch[4]) {
				fill = '0';
				align = ALIGN_SIGN;
			}

			width = rxMatch[5] ? parseInt(rxMatch[5]) : 0;

			str = rxMatch[6];
			groupSep = str ? (str == '*') ? ' ' : str : '';

			str = rxMatch[7];
			precision = str ? (str.length > 1) ? parseInt(str.substr(1)) : 0 : -1;

			upperCase = false;
			type = rxMatch[8] ? rxMatch[8] : '';
			if (type) {
				var _type:String = type;
				type = type.toLowerCase();
				if (type != _type)
					upperCase = true;
			}
		}

		public function toString():String {
			return '[FormatSpec fill="' + fill + '" align="' + align + '" sign="' + sign + '" altForm=' + altForm + ' groupSep="' + groupSep + '" width=' + width + ' precision=' + precision + ' type="' + type +
				'" isIntType=' + isIntType + ' isUIntType=' + isUIntType + ' isFloatType=' + isFloatType + ']';
		}
	}
}
