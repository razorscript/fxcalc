package com.razorscript.utils.formatter.handlers {
	import com.razorscript.utils.DateUtil;
	import com.razorscript.utils.formatter.Formatter;

	/**
	 * Formatter handler functions.
	 *
	 * @author Gene Pavlovsky
	 */
	public class FormatterHandlerFun {
		/**
		 * Adds handlers to a formatter and returns that formatter.
		 *
		 * Provided handlers:
		 * 		Date: formats dates using DateUtil.format().
		 *
		 * @param formatter A formatter.
		 * @return The formatter.
		 */
		public static function extend(formatter:Formatter):Formatter {
			formatter.addHandler(dateHandler);

			return formatter;
		}

		protected static function dateHandler(formatSpec:String, value:*):String {
			return (value is Date) ? DateUtil.format(formatSpec, value) : null;
		}
	}
}
