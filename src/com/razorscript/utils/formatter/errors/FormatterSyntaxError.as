package com.razorscript.utils.formatter.errors {
	
	/**
	 * Thrown when an exceptional condition has occurred during formatting due to invalid format syntax.
	 * This exception indicates an invalid input, not a Formatter bug.
	 *
	 * @author Gene Pavlovsky
	 */
	public class FormatterSyntaxError extends Error {
		public function FormatterSyntaxError(message:* = '', id:* = 0) {
			super(message, id);
		}
	}
}
