package com.razorscript.utils.formatter.errors {
	
	/**
	 * Thrown when an exceptional condition has occurred during formatting.
	 * This exception indicates a Formatter bug, not an invalid input, which throws FormatterSyntaxError or FormatArgumentError.
	 *
	 * @author Gene Pavlovsky
	 */
	public class FormatterError extends Error {
		public function FormatterError(message:* = '', id:* = 0) {
			super(message, id);
		}
	}
}
