package com.razorscript.utils.formatter.errors {
	
	/**
	 * Thrown when an exceptional condition has occurred during formatting due to a missing or invalid argument.
	 * This exception indicates an invalid input, not a Formatter bug.
	 *
	 * @author Gene Pavlovsky
	 */
	public class FormatterArgumentError extends Error {
		public function FormatterArgumentError(message:* = '', id:* = 0) {
			super(message, id);
		}
	}
}
