package com.razorscript.utils.formatter {
	import com.razorscript.math.MathExt;
	import com.razorscript.utils.formatter.errors.FormatterArgumentError;
	import com.razorscript.utils.formatter.errors.FormatterError;
	import com.razorscript.utils.formatter.errors.FormatterSyntaxError;
	import com.razorscript.utils.formatter.supportClasses.FormatSpec;
	import com.razorscript.utils.NumUtil;
	import com.razorscript.utils.StrUtil;

	/**
	 * Prints formatted strings. Provides two formatting functions: format(), similar to Python's format, and sprintf(), similar to C's sprintf.
	 *
	 * Supports custom conversions using conversion functions.
	 * Supports custom formatting for built-in and user-defined classes using handler functions (format() only).
	 * User-defined classes can also provide a toFormattedString(formatSpec:String):String method to convert an object to a string (format() only).
	 *
	 * @see https://docs.python.org/3/library/string.html#format-string-syntax
	 * @see https://en.wikipedia.org/wiki/Printf_format_string
	 *
	 * @author Gene Pavlovsky
	 */
	public class Formatter {
		public function Formatter(exponentWidth:int = 0) {
			this.exponentWidth = exponentWidth;
			_conversions = {};
			_handlers = new Vector.<Function>();
		}

		/** When printing floats in exponential notation, the minimum number of characters to print for the exponent digits. The exponent is left-padded with zeros if necessary. */
		public var exponentWidth:int = 0;

		/** When printing floats in general format, the low endpoint of the decimal exponent's range where the fixed-point notation is used. */
		public var fixedMinExponent:int = -4;

		protected static const FORMAT_REPL_FIELD_RX:RegExp = /{ (\d+ [^{}!:]*)? (! [$_a-zA-Z] [$_a-zA-Z0-9]*)? (: (?: ({ [^{}]+ }) | [^{}]+)*)? }/gx;
		protected static const FORMAT_NESTED_RX:RegExp = /{ ([^{}]+) }/gx;

		/**
		 * Formats a string (similar to Python's format). The specified format string can contain literal text, which is copied unchanged to the output, or replacement fields delimited by curly braces "{}".
		 * Use "{{" and "}}" to output a literal character "{" and "}", respectively.
		 *
		 * Each replacement field can start with an optional parameter that specifies the object whose value is to be formatted and inserted into the output instead of the replacement field.
		 * Parameter starts with an integer that specifies the number of the positional argument (zero-based), followed by any number of property or index expressions.
		 * An expression of the form ".name" selects the named property, while an expression of the form "[index]" does an index lookup.
		 * If any single replacement field specifies a parameter, all the rest of the replacement fields must also specify a parameter.
		 *
		 * The parameter is optionally followed by a conversion field, preceded by an exclamation mark '!'.
		 * The optional conversion field causes a value conversion before formatting. This can, for example, modify the value or convert it to another type.
		 * Supported conversions can be added or removed using addConversion() and removeConversion().
		 *
		 * The conversion field is optionally followed by a format spec, preceded by a colon ':'.
		 * The optional format spec field contains a specification of how the value should be presented, including such details as field width, alignment, padding, decimal precision etc.
		 * The format spec can include nested replacement fields within it. These nested replacement fields must contain a parameter; conversion flags and format specifications are not allowed.
		 * The replacement fields within the format spec are substituted before the format spec is interpreted. This allows the formatting of a value to be dynamically specified.
		 * @see com.razorscript.utils.formatter.supportClasses.FormatSpec
		 *
		 * Supports custom formatting for built-in and user-defined classes using handler functions.
		 * User-defined classes can also provide a toFormattedString(formatSpec:String):String method to convert an object to a string.
		 *
		 * repl_field    ::=  "{" [parameter] ["!" conversion] [":" format_spec] "}"
		 * parameter     ::=  arg_index ("." property_name | "[" element_index "]")*
		 * arg_index     ::=  integer
		 * property_name ::=  identifier
		 * element_index ::=  integer
		 * conversion    ::=  identifier
		 * format_spec   ::=  [[fill] align] [sign] [#] [0] [width] [group_sep] [. [precision]] [type]
		 * fill          ::=  <any character>
		 * align         ::=  "<" | ">" | "=" | "^"
		 * sign          ::=  "+" | "-" | " "
		 * group_sep     ::=  "," | "_" | "*"
		 * width         ::=  integer
		 * precision     ::=  integer
		 * type          ::=  "s" | "d" | "i" | "u" | "b" | "o" | "x" | "X" | "c" | "e" | "E" | "f" | "F" | "g" | "G" | "n" | "a" | "A" | "p"
		 * nested field  ::=  "{" [parameter] "}"
		 *
		 * @param format A format string.
		 * @param ... args A parameter.
		 * @return A copy of the format string, with each replacement field replaced with the string value of the corresponding parameter, formatted according to the specified (or default) formatting options.
		 *
		 * @throws FormatterSyntaxError - if the format string is invalid.
		 * @throws FormatterArgumentError - if a parameter is invalid.
		 */
		public function format(format:String, ... args):String {
			hasArgIndexOmitted = hasArgIndexSpecified = false;
			var str:String = '';
			var index:int = 0;
			var argIndex:int = 0;

			while (true) {
				// Find the next '{' character, starting at index.
				var matchIndex:int = format.indexOf('{', index);
				if (matchIndex == -1)
					break;
				if (matchIndex > index)
					str += format.substring(index, matchIndex);
				if (format.charAt(matchIndex + 1) == '{') {
					// '{{' found, replace with a '{', advance index beyond '{{' and continue.
					str += '{';
					index = matchIndex + 2;
					continue;
				}

				// Match the replacement field RegExp. If there's no match, or the match is at an index different from matchIndex, the replacement field is invalid.
				FORMAT_REPL_FIELD_RX.lastIndex = matchIndex;
				var rxMatch:Array = FORMAT_REPL_FIELD_RX.exec(format);
				if (!rxMatch || (rxMatch.index != matchIndex))
					throw new FormatterSyntaxError('Invalid replacement field at index ' + matchIndex + ': "' + format.substr(matchIndex) + '".');

				var param:String = rxMatch[1] ? rxMatch[1] : '';
				var conv:String = rxMatch[2] ? rxMatch[2].substr(1) : '';
				var formatSpec:String = rxMatch[3] ? rxMatch[3].substr(1) : '';
				if (rxMatch[4]) {
					// Matched at least one nested replacement field. Nested fields always specify a parameter.
					_checkArgIndex(true);
					// Replace all nested replacement fields with their values.
					formatSpec = formatSpec.replace(FORMAT_NESTED_RX, function (matchStr:String, matchParam:String, ... rest):String {
						var value:* = _getFormatArg(matchParam, args);
						if (value == null)
							throw new FormatterArgumentError('Missing value for nested replacement field: "' + matchStr + '".');
						return value;
					});
				}

				_checkArgIndex(param != '');
				var value:* = param ? _getFormatArg(param, args) : args[argIndex++];
				if (value == null)
					throw new FormatterArgumentError('Missing value for replacement field at index ' + matchIndex + ': "' + format.substr(matchIndex) + '".');
				if (conv)
					value = _convertValue(conv, value);
				str += _formatValue(formatSpec, value, true);
				index = FORMAT_REPL_FIELD_RX.lastIndex;
			}

			if (index < format.length)
				str += format.substr(index);
			// '{{' are already replaced with '{' by the main loop, replace '}}' with '}'.
			return str.replace(/}}/g, '}');
		}

		protected static const PRINTF_REPL_FIELD_RX:RegExp = /% (\d+ [^%$!]* \$)? ([$_a-zA-Z] [$_a-zA-Z0-9]* !)? ([^%$!sdiuboxXceEfFgGnaAp]* [sdiuboxXceEfFgGnaAp])/gx;

		/**
		 * Formats a string (similar to C's sprintf). The specified format string can contain literal text, which is copied unchanged to the output, or replacement fields denoted by a percent sign '%'.
		 * Use "%%" to output a literal character "%".
		 *
		 * Each replacement field can start with an optional parameter that specifies the object whose value is to be formatted and inserted into the output instead of the replacement field.
		 * Parameter starts with an integer that specifies the number of the positional argument (zero-based), followed by any number of property or index expressions, terminated with a dollar sign '$'.
		 * An expression of the form ".name" selects the named property, while an expression of the form "[index]" does an index lookup.
		 * If any single replacement field specifies a parameter, all the rest of the replacement fields must also specify a parameter.
		 *
		 * The parameter is optionally followed by a conversion field, terminated with an exclamation mark '!'.
		 * The optional conversion field causes a value conversion before formatting. This can, for example, modify the value or convert it to another type.
		 * Supported conversions can be added or removed using addConversion() and removeConversion().
		 *
		 * The conversion field is followed by a format spec, which must indicate the type of the field.
		 * The format spec field contains a specification of how the value should be presented, including such details as field width, alignment, padding, decimal precision etc.
		 * Unlike format(), the format spec can not include nested replacement fields within it.
		 * @see com.razorscript.utils.formatter.supportClasses.FormatSpec
		 *
		 * Doesn't support custom formatting handler functions.
		 * Doesn't support custom formatting with a toFormattedString() method provided by user-defined classes.
		 *
		 * repl_field    ::=  "%" [parameter "$"] [conversion "!"] format_spec
		 * parameter     ::=  arg_index ("." property_name | "[" element_index "]")*
		 * arg_index     ::=  integer
		 * property_name ::=  identifier
		 * element_index ::=  integer
		 * conversion    ::=  identifier
		 * format_spec   ::=  [[fill] align] [sign] [#] [0] [width] [group_sep] [. [precision]] type
		 * fill          ::=  <any character>
		 * align         ::=  "<" | ">" | "=" | "^"
		 * sign          ::=  "+" | "-" | " "
		 * group_sep     ::=  "," | "_" | "*"
		 * width         ::=  integer
		 * precision     ::=  integer
		 * type          ::=  "s" | "d" | "i" | "u" | "b" | "o" | "x" | "X" | "c" | "e" | "E" | "f" | "F" | "g" | "G" | "n" | "a" | "A" | "p"
		 *
		 * @param format A format string.
		 * @param ... args A parameter.
		 * @return A copy of the format string, with each replacement field replaced with the string value of the corresponding parameter, formatted according to the specified (or default) formatting options.
		 *
		 * @throws FormatterSyntaxError - if the format string is invalid.
		 * @throws FormatterArgumentError - if a parameter is invalid.
		 */
		public function sprintf(format:String, ... args):String {
			hasArgIndexOmitted = hasArgIndexSpecified = false;
			var str:String = '';
			var index:int = 0;
			var argIndex:int = 0;

			while (true) {
				// Find the next '%' character, starting at index.
				var matchIndex:int = format.indexOf('%', index);
				if (matchIndex == -1)
					break;
				if (matchIndex > index)
					str += format.substring(index, matchIndex);
				if (format.charAt(matchIndex + 1) == '%') {
					// '%%' found, replace with a '%', advance index beyond '%%' and continue.
					str += '%';
					index = matchIndex + 2;
					continue;
				}

				// Match the replacement field RegExp. If there's no match, or the match is at an index different from matchIndex, the replacement field is invalid.
				PRINTF_REPL_FIELD_RX.lastIndex = matchIndex;
				var rxMatch:Array = PRINTF_REPL_FIELD_RX.exec(format);
				if (!rxMatch || (rxMatch.index != matchIndex))
					throw new FormatterSyntaxError('Invalid replacement field at index ' + matchIndex + ': "' + format.substr(matchIndex) + '".');

				var param:String = rxMatch[1] ? rxMatch[1].substr(0, -1) : '';
				var conv:String = rxMatch[2] ? rxMatch[2].substr(0, -1) : '';
				var formatSpec:String = rxMatch[3];

				_checkArgIndex(param != '');
				var value:* = param ? _getFormatArg(param, args) : args[argIndex++];
				if (value == null)
					throw new FormatterArgumentError('Missing value for replacement field at index ' + matchIndex + ': "' + format.substr(matchIndex) + '".');
				if (conv)
					value = _convertValue(conv, value);
				str += _formatValue(formatSpec, value, false);
				index = PRINTF_REPL_FIELD_RX.lastIndex;
			}

			if (index < format.length)
				str += format.substr(index);
			return str;
		}

		protected var hasArgIndexOmitted:Boolean;
		protected var hasArgIndexSpecified:Boolean;

		/**
		 * Keeps track of whether all replacement fields specify a parameter.
		 * A format method must set hasArgIndexOmitted and hasArgIndexSpecified to false, and call _checkArgIndex(isSpecified) for every replacement field.
		 * The isSpecified argument should be true or false if the replacement field respectively specifies or omits a parameter.
		 * If a replacement field have a parameter specified, and another replacement field have a parameter ommited, throws an exception.
		 *
		 * @param isSpecified True if a replacement field specifies a parameter, otherwise false.
		 *
		 * @throws FormatterSyntaxError - if a replacement field have a parameter specified, and another replacement field have a parameter ommited.
		 */
		protected function _checkArgIndex(isSpecified:Boolean):void {
			if (isSpecified)
				hasArgIndexSpecified = true;
			else
				hasArgIndexOmitted = true;
			if (hasArgIndexSpecified && hasArgIndexOmitted)
				throw new FormatterSyntaxError('Parameter must be either specified for all replacement fields, or omitted for all replacement fields.');
		}

		protected static const PARAMETER_RX:RegExp = /\. ([$_a-zA-Z] [$_a-zA-Z0-9]*) | \[ (\d+) \]/gx;

		/**
		 * Parses a parameter spec and returns the specified value.
		 *
		 * parameter     ::=  arg_index ("." property_name | "[" element_index "]")*
		 * arg_index     ::=  integer
		 * property_name ::=  identifier
		 * element_index ::=  integer
		 *
		 * @param param A parameter spec.
		 * @param args The format arguments array.
		 * @return The specified parameter.
		 *
		 * @throws FormatterSyntaxError - if the parameter is invalid.
		 */
		protected function _getFormatArg(param:String, args:Array):* {
			var i:int = 0;
			var length:int = param.length;
			while ((i < length) && (uint(param.charCodeAt(i) - 48) < 10))
				++i;
			var value:* = args[param.substr(0, i)];
			if (value == null)
				return null; // Value is missing, handled by caller.
			PARAMETER_RX.lastIndex = i;
			while (PARAMETER_RX.lastIndex < length) {
				var rxMatch:Array = PARAMETER_RX.exec(param);
				if (!rxMatch)
					break;
				value = value[rxMatch[1] ? rxMatch[1] : rxMatch[2]];
				if (value == null)
					break;
			}
			if ((PARAMETER_RX.lastIndex != length) || (value == null))
				throw new FormatterSyntaxError('Invalid parameter: "' + param + '".');
			return value;
		}

		/**
		 * Applies a conversion to a value.
		 *
		 * @param conv A conversion id.
		 * @param value A value.
		 * @return The converted value.
		 *
		 * @throws FormatterSyntaxError - if the conversion id is not in the supported conversions list.
		 * @throws FormatterArgumentError - if value is an invalid argument for the conversion.
		 */
		protected function _convertValue(conv:String, value:*):* {
			var fun:Function = _conversions[conv];
			if (!fun)
				throw new FormatterSyntaxError('Unknown conversion: "' + conv + '".');
			var result:* = fun(value);
			if (result == null)
				throw new FormatterArgumentError('Invalid argument "' + value + '" for conversion: "' + conv + '".');
			return result;
		}

		protected var _conversions:Object;

		/**
		 * Adds a conversion to the supported conversions list.
		 *
		 * @param id A conversion id.
		 * @param fun The conversion function, is invoked with one argument (value) and should return the converted value:
		 *		function conversion(value:*):*;
		 */
		public function addConversion(id:String, fun:Function):void {
			if (!(id in _conversions))
				_conversions[id] = fun;
		}

		/**
		 * Returns if the specified conversion is registered on the supported conversions list.
		 *
		 * @param id A conversion id.
		 * @return Boolean true if the conversion is registered.
		 */
		public function hasConversion(id:String):Boolean {
			return id in _conversions;
		}

		/**
		 * Removes a conversion from the supported conversions list.
		 *
		 * @param id A conversion id.
		 */
		public function removeConversion(id:String):void {
			delete _conversions[id];
		}

		// TODO: Refactor to use a Dictionary mapping value class reference to a handler function? (like ObjectParser's conversions).

		protected var _handlers:Vector.<Function>;

		/**
		 * Adds a handler function to the handlers list, that may convert a value to a string before formatting, bypassing the normal formatting logic.
		 * Before formatting a value, the handlers are invoked one by one, with format spec and value as arguments, until a handler returns a non-empty string.
		 * If a handler returns a non-empty string, that string is inserted into the output instead of the replacement field, bypassing the normal formatting logic.
		 * Otherwise the value is formatted according to the normal formatting logic.
		 *
		 * @param fun The function to run on each value, is invoked with two arguments (format spec, value) and should return either a formatted string or null:
		 *		function handler(formatSpec:String, value:*):String;
		 */
		public function addHandler(fun:Function):void {
			if (_handlers.indexOf(fun) == -1)
				_handlers.push(fun);
		}

		/**
		 * Returns if the specified handler function is registered on the handlers list.
		 *
		 * @param fun A function.
		 * @return Boolean true if the handler is registered.
		 */
		public function hasHandler(fun:Function):Boolean {
			return _handlers.indexOf(fun) != -1;
		}

		/**
		 * Removes a handler function from the handlers list.
		 *
		 * @param fun A function.
		 */
		public function removeHandler(fun:Function):void {
			var index:int = _handlers.indexOf(fun);
			if (index >= 0)
				_handlers.splice(index, 1);
		}

		/**
		 * Formats a value to a string according to a format spec.
		 *
		 * @param formatSpec A format spec.
		 * @param value A value.
		 * @param useCustomHandlers Whether custom handlers, if they exist, should be used.
		 * @return The formatted string.
		 *
		 * @throws FormatterArgumentError - if value's type doesn't match the format spec type.
		 */
		protected function _formatValue(formatSpec:String, value:*, useCustomHandlers:Boolean):String {
			if (useCustomHandlers) {
				// Call handlers first.
				var length:int = _handlers.length;
				for (var i:int = 0; i < length; ++i) {
					var str:String = _handlers[i](formatSpec, value);
					if (str != null)
						return str;
				}

				// If value has a toFormattedString() method, use it.
				if ('toFormattedString' in value)
					return value.toFormattedString(formatSpec);
			}

			fmt.parse(formatSpec);
			var checkNumeric:Boolean = true;
			if (!fmt.type) {
				// Format spec doesn't specify the type, select it based on value's actual data type.
				if ((value is int) || (value is uint))
					fmt.type = (value >= 0) ? FormatSpec.TYPE_UINT_UNSIGNED : FormatSpec.TYPE_INT_DECIMAL;
				else if (value is Number)
					fmt.type = FormatSpec.TYPE_FLOAT_DEFAULT;
				else
					fmt.type = FormatSpec.TYPE_STR_STRING;
				checkNumeric = false; // No need to check since _fmt.type matches value's actual data type.
			}

			if (fmt.isUIntType) {
				if (checkNumeric && !(value is uint))
					throw new FormatterArgumentError('Expected an unsigned integer value for replacement field with format spec "' + formatSpec + '", but got "' + value + '".');
				return _formatUInt(value);
			}
			else if (fmt.isIntType) {
				if (checkNumeric && !(value is int))
					throw new FormatterArgumentError('Expected a signed integer value for replacement field with format spec "' + formatSpec + '", but got "' + value + '".');
				return _formatInt(value);
			}
			else if (fmt.isFloatType) {
				if (checkNumeric && !(value is Number))
					throw new FormatterArgumentError('Expected a numeric value for replacement field with format spec "' + formatSpec + '", but got "' + value + '".');
				return _formatFloat(value);
			}
			else {
				return _formatString(value);
			}
		}

		/**
		 * Formats an unsigned integer (uint) value.
		 *
		 * @param value An unsigned integer (uint).
		 * @return The formatted string.
		 */
		protected function _formatUInt(value:uint):String {
			if (!fmt.align)
				fmt.align = FormatSpec.ALIGN_RIGHT;

			if (fmt.type == FormatSpec.TYPE_UINT_CHAR)
				return _formatString(String.fromCharCode(value));

			var prefix:String;
			var groupWidth:int;
			var base:int;

			if (fmt.type == FormatSpec.TYPE_UINT_UNSIGNED) {
				prefix = '';
				groupWidth = 3;
				base = 10;
				if (fmt.align == FormatSpec.ALIGN_SIGN)
					fmt.align = FormatSpec.ALIGN_RIGHT;
			}
			else if (fmt.type == FormatSpec.TYPE_UINT_BINARY) {
				prefix = '0b';
				groupWidth = 4;
				base = 2;
			}
			else if (fmt.type == FormatSpec.TYPE_UINT_OCTAL) {
				prefix = '0o';
				groupWidth = 0;
				base = 8;
			}
			else if (fmt.type == FormatSpec.TYPE_UINT_HEX) {
				prefix = fmt.upperCase ? '0X' : '0x';
				groupWidth = 2;
				base = 16;
			}
			else {
				throw new FormatterError('Invalid type "' + fmt.type + '" in format spec "' + fmt.source + '".');
			}

			var str:String = fmt.upperCase ? value.toString(base).toUpperCase() : value.toString(base);
			if (fmt.groupSep && groupWidth)
				str = _groupDigits(str, groupWidth, fmt.groupSep);
			return (fmt.width && (fmt.align == FormatSpec.ALIGN_SIGN)) ? _alignSign(prefix, str, '', groupWidth) : _formatString(prefix + str);
		}

		/**
		 * Formats an integer (int) value.
		 *
		 * @param value A signed integer (int).
		 * @return The formatted string.
		 */
		protected function _formatInt(value:int):String {
			if (!fmt.align)
				fmt.align = FormatSpec.ALIGN_RIGHT;

			var prefix:String = (value < 0) ? '-' : (fmt.sign == FormatSpec.SIGN_MINUS) ? '' : fmt.sign;
			var str:String = Math.abs(value).toString();
			if (fmt.groupSep)
				str = _groupDigits(str, 3, fmt.groupSep);
			return (fmt.width && (fmt.align == FormatSpec.ALIGN_SIGN)) ? _alignSign(prefix, str, '', 3) : _formatString(prefix + str);
		}

		/**
		 * Formats a number (Number, int or uint) as a floating-point value.
		 *
		 * @param value A number (Number, int or uint).
		 * @return The formatted string.
		 */
		protected function _formatFloat(value:Number):String {
			if (!fmt.align)
				fmt.align = FormatSpec.ALIGN_RIGHT;
			if (fmt.precision < 0)
				fmt.precision = (fmt.type == FormatSpec.TYPE_FLOAT_DEFAULT) ? MathExt.DOUBLE_UNIQ_DIGITS : 6;
			if (fmt.type == FormatSpec.TYPE_FLOAT_DEFAULT)
				fmt.type = FormatSpec.TYPE_FLOAT_GENERAL;

			var prefix:String = (value < 0) ? '-' : (fmt.sign == FormatSpec.SIGN_MINUS) ? '' : fmt.sign;
			if (fmt.type == FormatSpec.TYPE_FLOAT_HEXFLOAT)
				prefix += fmt.upperCase ? '0X' : '0x';
			value = Math.abs(value);

			var str:String;
			if (fmt.type == FormatSpec.TYPE_FLOAT_EXP) {
				str = NumUtil.toExponential(value, fmt.precision);
				if (fmt.altForm && (str.indexOf('.') == -1))
					str = _addDecimalPoint(str, 'e');
			}
			else if (fmt.type == FormatSpec.TYPE_FLOAT_FIXED) {
				str = NumUtil.toFixed(value, fmt.precision, true);
				if (fmt.altForm && !fmt.precision)
					str += '.';
			}
			else if (fmt.type == FormatSpec.TYPE_FLOAT_GENERAL) {
				str = NumUtil.toGeneral(value, fmt.precision, fmt.altForm, fixedMinExponent);
				if (fmt.altForm && (str.indexOf('.') == -1))
					str = _addDecimalPoint(str, 'e');
			}
			else if (fmt.type == FormatSpec.TYPE_FLOAT_HEXFLOAT) {
				str = NumUtil.toHexFloat(value, fmt.altForm);
				if (fmt.altForm && (str.indexOf('.') == -1))
					str = _addDecimalPoint(str, 'p');
			}
			else if (fmt.type == FormatSpec.TYPE_FLOAT_PERCENTAGE) {
				str = NumUtil.toFixed(value * 100, fmt.precision, true) + ((fmt.altForm && !fmt.precision) ? '.%' : '%');
			}
			else {
				throw new FormatterError('Invalid type "' + fmt.type + '" in format spec "' + fmt.source + '".');
			}

			if ((exponentWidth > 1) && (fmt.type != FormatSpec.TYPE_FLOAT_FIXED) && (fmt.type != FormatSpec.TYPE_FLOAT_PERCENTAGE))
				str = _padExponent(str);

			if (fmt.upperCase)
				str = str.toUpperCase();

			var suffix:String = '';
			if (fmt.groupSep) {
				var index:int = str.indexOf('.');
				if ((index == -1) && (fmt.type != FormatSpec.TYPE_FLOAT_FIXED) && (fmt.type != FormatSpec.TYPE_FLOAT_PERCENTAGE))
					index = str.indexOf((fmt.type == FormatSpec.TYPE_FLOAT_HEXFLOAT) ? 'p' : 'e');
				if (index != -1) {
					suffix = str.substr(index);
					str = str.substr(0, index);
				}
				str = _groupDigits(str, 3, fmt.groupSep);
			}
			return (fmt.width && (fmt.align == FormatSpec.ALIGN_SIGN)) ? _alignSign(prefix, str, suffix, 3) : _formatString(prefix + str + suffix);
		}

		/**
		 * Formats a string value.
		 *
		 * @param value A string.
		 * @return The formatted string.
		 *
		 * @throws FormatterSyntaxError - if the format spec align is invalid.
		 */
		protected function _formatString(value:String):String {
			if (!fmt.align)
				fmt.align = FormatSpec.ALIGN_LEFT;

			if (!fmt.width)
				return value;

			if (fmt.align == FormatSpec.ALIGN_LEFT) {
				return StrUtil.rpad(value, fmt.fill, fmt.width);
			}
			else if (fmt.align == FormatSpec.ALIGN_RIGHT) {
				return StrUtil.lpad(value, fmt.fill, fmt.width);
			}
			else if (fmt.align == FormatSpec.ALIGN_CENTER) {
				var numFill:int = fmt.width - value.length;
				if (numFill <= 0)
					return value;
				var halfFill:String = StrUtil.repeat(fmt.fill, numFill >> 1);
				return halfFill + value + halfFill + ((numFill & 1) ? fmt.fill : '');
			}
			else {
				throw new FormatterSyntaxError('Invalid align "' + fmt.align + '" in format spec "' + fmt.source + '".');
			}
		}

		/**
		 * Inserts a decimal point before the specified exponent character, or at the end of the string if the exponent character is not found.
		 *
		 * @param str A string.
		 * @param expChar The character used to indicate the exponent.
		 * @return The string with a decimal point added after the digits.
		 */
		protected function _addDecimalPoint(str:String, expChar:String):String {
			var index:int = str.indexOf(expChar);
			return (index == -1) ? (str + '.') : (str.substr(0, index) + '.' + str.substr(index));
		}

		protected static const PAD_EXP_RX:RegExp = /^ (.* [ep][-+]) (\d+) $/x;

		/**
		 * If a string contains an exponent, left-pads the exponent with zeros to exponentWidth characters, otherwise returns the original string.
		 *
		 * @param str A string.
		 * @return The string with exponent, if found, left-padded with zeros to exponentWidth characters.
		 */
		protected function _padExponent(str:String):String {
			var rxMatch:Array = PAD_EXP_RX.exec(str);
			return (rxMatch && (rxMatch[2].length < exponentWidth)) ? (rxMatch[1] + StrUtil.lpad(rxMatch[2], '0', exponentWidth)) : str;
		}

		/**
		 * Divides the string into groupWidth-sized substrings, then joins them together using the specified group separator character. The first substring may contain less than groupWidth characters.
		 *
		 * @param str A string.
		 * @param groupWidth Group width.
		 * @param groupSep Group separator character.
		 * @return The grouped string.
		 */
		protected function _groupDigits(str:String, groupWidth:int, groupSep:String):String {
			return StrUtil.splitw(str, groupWidth, true).join(groupSep);
		}

		/**
		 * Pads the string to width, placing the padding between the prefix string and the str string, respecting the digits grouping options.
		 *
		 * @param prefix A string. Padding is placed between prefix and str.
		 * @param str A string. Padding is placed between prefix and str.
		 * @param suffix A string.
		 * @return The padded string.
		 */
		protected function _alignSign(prefix:String, str:String, suffix:String, groupWidth:int):String {
			if (fmt.groupSep && groupWidth && (fmt.fill == '0')) {
				var minLength:int = fmt.width - prefix.length - suffix.length; // Desired digits string length. Resulting length should be equal to or greater than minLength.
				var length:int = str.length;
				if (length < minLength) {
					// If the first digits group is not complete, left-pad it with the fill character until the group is complete or length equals minLength, whichever happens first.
					var remainder:int = (length % (groupWidth + 1));
					if (remainder && (remainder < groupWidth))
						str = StrUtil.repeat(fmt.fill, Math.min(groupWidth - remainder, minLength - length)) + str;
					length = str.length;
					if (length < minLength) {
						// Left-pad the string with as many fill groups (fill character repeated groupWidth times, plus the group separator character) as possible without length exceeding minLength.
						str = StrUtil.lpad(str, StrUtil.repeat(fmt.fill, groupWidth) + fmt.groupSep, minLength);
						length = str.length;
						if (length < minLength) {
							// Left-pad the string with an incomplete fill group (fill character repeated at most as many times as required to make length equal to minLength, but at least once, plus the group separator character).
							var numFill:int = minLength - length - 1;
							str = StrUtil.repeat(fmt.fill, (numFill > 0) ? numFill : 1) + fmt.groupSep + str; // Resulting length is either equal to or 1 greater than minLength.
						}
					}
				}
			}
			else {
				numFill = fmt.width - prefix.length - str.length - suffix.length;
				if (numFill > 0)
					str = StrUtil.repeat(fmt.fill, numFill) + str;
			}
			return prefix + str + suffix;
		}

		protected static var fmt:FormatSpec = new FormatSpec();
	}
}
