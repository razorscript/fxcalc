package com.razorscript.utils {
	import com.razorscript.utils.timer.ITimerUtil;
	import com.razorscript.utils.timer.TimerTimerUtil;

	/**
	 * Timer utility functions.
	 *
	 * @author Gene Pavlovsky
	 */
	public class TimerUtil {
		protected static var _timerUtil:ITimerUtil;
		/**
		 * An instance of ITimerUtil used by static delayCall(), repeatCall() and remove(). If null, a static instance of TimerTimerUtil, created on first request, is used.
		 */
		public static function get timerUtil():ITimerUtil {
			return _timerUtil ? _timerUtil : timerTimerUtil ? timerTimerUtil : (timerTimerUtil = new TimerTimerUtil());
		}

		public static function set timerUtil(value:ITimerUtil):void {
			_timerUtil = value;
		}

		protected static var timerTimerUtil:ITimerUtil;

		/**
		 * Delays the execution of a function until the specified number of milliseconds has passed.
		 * To cancel the call, pass the returned object to remove().
		 *
		 * @param fun A function.
		 * @param delay The delay in milliseconds.
		 * @param ... args An argument.
		 * @return An object that can be passed to remove() to cancel the call.
		 */
		public static function delayCall(fun:Function, delay:Number, ... args):Object {
			args.unshift(fun, delay);
			return timerUtil.delayCall.apply(null, args);
		}

		/**
		 * Runs a function at the specified interval (in milliseconds), with an optional repeat count.
		 * If the repeat count is zero, the function will repeat indefinitely.
		 * To cancel the call, pass the returned object to remove().
		 *
		 * @param fun A function.
		 * @param interval The interval in milliseconds.
		 * @param repeatCount The number of times to run the function. If set to 0, the function will repeat indefinitely.
		 * @param ... args An argument.
		 * @return An object that can be passed to remove() to cancel the call.
		 */
		public static function repeatCall(fun:Function, interval:Number, repeatCount:int = 0, ... args):Object {
			args.unshift(fun, interval, repeatCount);
			return timerUtil.repeatCall.apply(null, args);
		}

		/**
		 * Removes a scheduled delayed call.
		 * If the argument is null, does nothing.
		 *
		 * @param delayedCall An object.
		 */
		public static function remove(delayedCall:Object):void {
			timerUtil.remove(delayedCall);
		}
	}
}
