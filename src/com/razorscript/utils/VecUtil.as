package com.razorscript.utils {

	/**
	 * TODO: Add search or binarySearch function which accepts a vector, a search value, a comparison function, and a desired result (< | <= | == | >= | >). Returns a match index or -1 if not found.
	 * Put as a separate class in the com.razorscript.ds package?
	 * Choose an iterator corresponding to a Vector's base type. 4 versions should be implemented...
	 * Internally, AS3 uses 4 low-level vector classes:
	 *		Vector.<Number> - use VectorNumberIterator.
	 *		Vector.<int> - use VectorIntIterator.
	 *		Vector.<uint> - use VectorUIntIterator.
	 *		Use VectorIterator for any other Vector (Vectors of non-numeric base types are all represented by the Vector.<*> class, supported by VectorIterator).
	 * @see com.razorscript.ds.VectorIterator
	 */

	/**
	 * Vector and array utility functions.
	 *
	 * @author Gene Pavlovsky
	 */
	public class VecUtil {
		/**
		 * Returns true if a value is a Vector.
		 *
		 * Internally, AS3 uses 4 low-level vector classes: Vector.<Number>, Vector.<int>, Vector.<uint> and Vector.<*> for all other base types.
		 *
		 * @param value A value.
		 * @return Boolean true if the value is a Vector.
		 */
		[Inline]
		public static function isVector(value:*):Boolean {
			return (value is Vector.<*>) || (value is Vector.<Number>) || (value is Vector.<int>) || (value is Vector.<uint>);
		}

		/**
		 * Executes the specified function on each element of an array or a vector, and pushes it's results into either the specified target, or a new array.
		 *
		 * @param elements An array or a vector to map.
		 * @param callback The function to run on each element in the collection, is invoked with three arguments:
		 *		function callback(element:*, index:int, elements:*):*;
		 * @param thisObject An object to use as this for the function.
		 * @param target A collection that supports push(), to store the resulting elements. If not specified, a new array is created.
		 * @return The collection containing the results of calling the specified function on each source element.
		 */
		public static function genericMap(elements:*, callback:Function, thisObject:* = null, target:* = null):* {
			if (!target)
				target = [];
			var length:int = elements.length;
			for (var i:int = 0; i < length; ++i)
				target.push(callback.apply(thisObject, [elements[i], i, elements]));
			return target;
		}

		/**
		 * Joins the elements of an array or a vector into a string.
		 * Elements are grouped and concatenated, with a specified separator and group separator.
		 *
		 * @param elements An array or a vector to join.
		 * @param groupLength Number of elements per group.
		 * @param elementSep Separator between elements within a group.
		 * @param groupSep Separator between groups of N elements.
		 * @return The elements grouped into a String.
		 */
		public static function joinGrouped(elements:*, groupLength:int, elementSep:String = ', ', groupSep:String = '; '):String {
			var str:String = '';
			var length:int = elements.length;
			for (var i:int = 0; i < length;) {
				if (i)
					str += groupSep;
				for (var j:int = 0; (j < groupLength) && (i < length); ++j, ++i) {
					if (j)
						str += elementSep;
					str += elements[i];
				}
			}
			return str;
		}

		/**
		 * Joins the elements of an array or a vector into a string.
		 * Elements are grouped and concatenated, with a specified separator, group separator and a label for each element in a group.
		 *
		 * @param elements An array or a vector to join.
		 * @param labels Labels of each element in the group. The length of this array defines the size of the group.
		 * @param subSep Separator between elements within a group.
		 * @param sep Separator between groups of N elements.
		 * @param labelSep Separator between labels and elements.
		 * @return The elements grouped into a String.
		 */
		public static function joinGroupedWithLabels(elements:*, labels:Array, elementSep:String = ', ', groupSep:String = '; ', labelSep:String = ':'):String {
			var str:String = '';
			var numLabels:uint = labels.length;
			var length:int = elements.length;
			for (var i:int = 0; i < length;) {
				if (i)
					str += groupSep;
				for (var j:int = 0; (j < numLabels) && (i < length); ++j, ++i) {
					if (j)
						str += elementSep;
					str += labels[j] + labelSep + elements[i];
				}
			}
			return str;
		}

		/**
		 * A compare function for sorting arrays or vectors of strings (@see Vector#sort()).
		 * Orders items by string length, in ascending order.
		 */
		public static function compareStringsByLengthAsc(a:String, b:String):int {
			return a.length - b.length;
		}

		/**
		 * A compare function for sorting arrays or vectors of strings (@see Vector#sort()).
		 * Orders items by string length, in descending order.
		 */
		public static function compareStringsByLengthDesc(a:String, b:String):int {
			return b.length - a.length;
		}
	}
}
