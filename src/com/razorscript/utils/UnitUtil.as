package com.razorscript.utils {
	import com.razorscript.math.MathExt;

	/**
	 * Units utility functions.
	 *
	 * @author Gene Pavlovsky
	 */
	public class UnitUtil {
		protected static const SI_PREFIX_SYMBOLS:Vector.<String> = new <String>['y', 'z', 'a', 'f', 'p', 'n', 'μ', 'm', '', 'k', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y'];
		protected static const SI_PREFIX_SYMBOL_INDEX_BIAS:int = 8;

		/**
		 * Returns a SI unit prefix (metric prefix) symbol, corresponding to the specified decimal exponent, which must be a multiple of 3.
		 * If the exponent is zero, returns an empty string.
		 * If the exponent is out of metric prefix range, returns null.
		 *
		 * @param exponent A decimal exponent which must be a multiple of 3.
		 * @return The SI unit prefix corresponding to the specified exponent.
		 */
		public static function getSIPrefix(exponent:int):String {
			if (exponent % 3)
				throw new RangeError('The exponent argument (' + exponent + ') is not a multiple of 3.');
			var index:int = (exponent / 3) + SI_PREFIX_SYMBOL_INDEX_BIAS;
			return (index in SI_PREFIX_SYMBOLS) ? SI_PREFIX_SYMBOLS[index] : null;
		}

		/**
		 * Returns a SI unit prefix (metric prefix) symbol, corresponding to the specified zero-based index.
		 * If the index is zero, returns an empty string.
		 * If the index is out of metric prefix range, returns null.
		 *
		 * @param index A zero-based SI unit prefix index.
		 * @return The SI unit prefix corresponding to the specified index.
		 */
		public static function getSIPrefixByIndex(index:int):String {
			index += SI_PREFIX_SYMBOL_INDEX_BIAS;
			return (index in SI_PREFIX_SYMBOLS) ? SI_PREFIX_SYMBOLS[index] : null;
		}

		/**
		 * Returns a string representation of a number in engineering notation, using a SI unit prefix instead of an exponent, if possible.
		 * This function divides or multiplies the number by 1000 repeatedly to get it in the range [1; 1000), losing some precision.
		 *
		 * @param x A number.
		 * @param unit A unit name.
		 * @param fractionDigits The desired number of digits after the decimal point.
		 * @param numWidth The width to pad the number (but not unit prefix or unit) to.
		 * @param ifNaN A string to return if the number is NaN (not-a-number).
		 * @param ifInfinity A string to return if the number is a positive infinity.
		 * @param ifNegInfinity A string to return if the number is a negative infinity.
		 * @return The string representation of the number in engineering notation, with a SI unit prefix instead of an exponent, if possible.
		 */
		public static function formatSI(x:Number, unit:String = '', fractionDigits:int = 1, numWidth:int = 0, ifNaN:String = 'NaN', ifInfinity:String = 'Infinity', ifNegInfinity:String = '-Infinity'):String {
			return _formatWithPrefix(getSIPrefixByIndex, 1000, 3, 'e', x, unit, fractionDigits, numWidth, ifNaN, ifInfinity, ifNegInfinity);
		}

		protected static const IEC_BINARY_PREFIX_SYMBOLS:Vector.<String> = new <String>['', 'Ki', 'Mi', 'Gi', 'Ti', 'Pi', 'Ei', 'Zi', 'Yi'];

		/**
		 * Returns a IEC binary unit prefix symbol, corresponding to the specified binary exponent, which must be a multiple of 10.
		 * If the exponent is zero, returns an empty string.
		 * If the exponent is out of binary prefix range, returns null.
		 *
		 * @param exponent A binary exponent which must be a multiple of 10.
		 * @return The IEC binary unit prefix corresponding to the specified exponent.
		 */
		public static function getBinaryPrefix(exponent:int):String {
			if (exponent % 10)
				throw new RangeError('The exponent argument (' + exponent + ') is not a multiple of 10.');
			var index:int = exponent / 10;
			return (index in IEC_BINARY_PREFIX_SYMBOLS) ? IEC_BINARY_PREFIX_SYMBOLS[index] : null;
		}

		/**
		 * Returns a IEC binary unit prefix symbol, corresponding to the specified zero-based index.
		 * If the index is zero, returns an empty string.
		 * If the index is out of binary prefix range, returns null.
		 *
		 * @param index A zero-based binary unit prefix index.
		 * @return The IEC binary unit prefix corresponding to the specified index.
		 */
		public static function getBinaryPrefixByIndex(index:int):String {
			return (index in IEC_BINARY_PREFIX_SYMBOLS) ? IEC_BINARY_PREFIX_SYMBOLS[index] : null;
		}

		/**
		 * Returns a string representation of a number in engineering notation, using a IEC binary unit prefix instead of an exponent, if possible.
		 * This function divides the number by 1024 repeatedly to get it in the range [1; 1024), losing some precision.
		 *
		 * @param x A number.
		 * @param unit A unit name.
		 * @param fractionDigits The desired number of digits after the decimal point.
		 * @param numWidth The width to pad the number (but not unit prefix or unit) to.
		 * @param ifNaN A string to return if the number is NaN (not-a-number).
		 * @param ifInfinity A string to return if the number is a positive infinity.
		 * @param ifNegInfinity A string to return if the number is a negative infinity.
		 * @return The string representation of the number in engineering notation, with a IEC binary unit prefix instead of an exponent, if possible.
		 */
		public static function formatBinary(x:Number, unit:String = '', fractionDigits:int = 1, numWidth:int = 0, ifNaN:String = 'NaN', ifInfinity:String = 'Infinity', ifNegInfinity:String = '-Infinity'):String {
			return _formatWithPrefix(getBinaryPrefixByIndex, 1024, 10, 'p', x, unit, fractionDigits, numWidth, ifNaN, ifInfinity, ifNegInfinity);
		}

		protected static function _formatWithPrefix(prefixFun:Function, rangeMax:int, expMultiple:int, expChar:String, x:Number, unit:String, fractionDigits:int, numWidth:int, ifNaN:String, ifInfinity:String, ifNegInfinity:String):String {
			if (fractionDigits < 0)
				throw new RangeError('The fractionDigits argument (' + fractionDigits + ') must be a non-negative number.');
			if (numWidth < 0)
				throw new RangeError('The numWidth argument (' + numWidth + ') must be a non-negative number.');

			var prefixLength:int = prefixFun(1).length;
			var width:int = numWidth ? (numWidth + 1 + prefixLength + unit.length) : 0;

			var str:String;
			if (MathExt.isNaN(x))
				str = ifNaN;
			else if (x == Infinity)
				str = ifInfinity;
			else if (x == -Infinity)
				str = ifNegInfinity;

			if (str != null) {
				// The number is NaN or Infinity.
				if (str == '')
					return width ? StrUtil.repeat(' ', width) : '';
				if (unit)
					str += ' ' + ((width && (str.length <= numWidth + 1 + prefixLength)) ? StrUtil.repeat(' ', prefixLength) : '') + unit;
				return width ? StrUtil.lpad(str, ' ', width) : str;
			}

			var sign:int = MathExt.sign(x);
			x = Math.abs(x);

			// TODO: Use base-10 or base-2 logarithm to get the initial exp and minimize calculations.
			var index:int = 0;
			// Get the number in the range [1; rangeMax).
			while (x >= rangeMax) {
				x /= rangeMax;
				++index;
			}
			if (x) {
				while (x < 1) {
					x *= rangeMax;
					--index;
				}
			}
			str = x.toFixed(fractionDigits);
			var rangeStr:String = rangeMax.toString();
			if (str.substr(0, rangeStr.length) == rangeStr) {
				// The number was rounded up to the next prefix.
				str = '1' + str.substr(rangeStr.length);
				++index;
			}
			if (sign < 0)
				str = '-' + str;

			var prefix:String = prefixFun(index);
			if (prefix == null) {
				str += 'p' + (index * expMultiple).toString();
				if (width && (str.length <= numWidth + 1 + prefixLength))
					str += ' ';
			}
			else {
				unit = ((prefix != '') ? prefix : width ? StrUtil.repeat(' ', prefixLength) : '') + unit;
			}
			if (unit)
				str += ' ' + unit;
			return width ? StrUtil.lpad(str, ' ', width) : str;
		}
	}
}
