package com.razorscript.utils.objectParser.errors {
	
	/**
	 * Thrown when an exceptional condition has occurred during parsing an object due to an unknown property or an invalid value.
	 * This exception indicates an invalid input, not an ObjectParser bug.
	 *
	 * @author Gene Pavlovsky
	 */
	public class ObjectParserError extends Error {
		public function ObjectParserError(message:* = '', target:Object = null, property:String = '', id:* = 0) {
			super(message, id);
			this.target = target;
			this.property = property;
		}
		
		/** The target object. */
		public var target:Object;
		/** The property name. */
		public var property:String;
	}
}
