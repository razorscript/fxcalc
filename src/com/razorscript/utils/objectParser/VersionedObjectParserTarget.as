package com.razorscript.utils.objectParser {
	import com.razorscript.utils.objectParser.IObjectParserCustomTarget;

	/**
	 * Base class for ObjectParser targets that use a versioned object format.
	 *
	 * @author Gene Pavlovsky
	 */
	public class VersionedObjectParserTarget implements IObjectParserCustomTarget {
		public function VersionedObjectParserTarget(currentFormatVersion:int) {
			this.currentFormatVersion = currentFormatVersion;
		}

		protected var currentFormatVersion:int;

		public function get formatVersion():int {
			return currentFormatVersion;
		}

		/**
		 * Parses the specified object into this object.
		 * If the object's format version differs from the current version, attempts to upgrade or downgrade the object.
		 *
		 * @param obj An object to parse into this object.
		 * @return Boolean false to tell ObjectParser it should call the default parseObjectTo() method.
		 */
		public function parseObject(obj:Object):Boolean {
			var objectFormatVersion:int = obj.formatVersion;
			delete obj.formatVersion;

			if (objectFormatVersion < currentFormatVersion)
				upgradeObject(obj, objectFormatVersion);
			else if (objectFormatVersion > currentFormatVersion)
				downgradeObject(obj, objectFormatVersion);

			return false;
		}

		/**
		 * Upgrades an object from an older format version to the current one.
		 * Subclasses should override this function to implement object format upgrading.
		 *
		 * @param obj An object to upgrade.
		 * @param objectFormatVersion Object's format version.
		 */
		protected function upgradeObject(obj:Object, objectFormatVersion:int):void {
		}

		/**
		 * Downgrades an object from a newer format version to the current one.
		 * Subclasses should override this function to implement object format downgrading.
		 *
		 * @param obj An object to downgrade.
		 * @param objectFormatVersion Object's format version.
		 */
		protected function downgradeObject(obj:Object, objectFormatVersion:int):void {
			throw new Error('Downgrading object format (' + objectFormatVersion + '->' + currentFormatVersion + ') is not implemented.');
		}
	}
}
