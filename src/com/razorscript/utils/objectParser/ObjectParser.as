package com.razorscript.utils.objectParser {
	import com.razorscript.debug.Logger;
	import com.razorscript.utils.ClassUtil;
	import com.razorscript.utils.objectParser.errors.ObjectParserError;
	import com.razorscript.utils.supportClasses.PropertyInfo;
	import flash.utils.Dictionary;

	/**
	 * Parses objects or JSON-formatted strings into another object.
	 *
	 * @author Gene Pavlovsky
	 */
	public class ObjectParser {
		public function ObjectParser(strict:Boolean = false) {
			this.strict = strict;
			_conversions = new Dictionary();
		}

		/** If true, throws an ObjectParserError on error. Otherwise, logs a warning message. */
		public var strict:Boolean;

		/**
		 * Parses an object into the specified target object.
		 * Supports custom conversions using conversion functions.
		 *
		 * @param obj The source object.
		 * @param target The destination object.
		 */
		public function parseObjectTo(obj:Object, target:Object):void {
			if ((target is IObjectParserCustomTarget) && (target as IObjectParserCustomTarget).parseObject(obj))
				return;
			var classPropertyInfo:Object = ClassUtil.getClassPropertyInfo(target.constructor);
			for (var property:String in obj) {
				var propertyInfo:PropertyInfo = classPropertyInfo[property];
				if (propertyInfo) {
					try {
						// TODO: Add support for handlers, allowing to add custom parseObject() functionality without modifying target property classes.
						var targetValue:Object = target[property];
						if (targetValue is IObjectParserTarget) {
							parseObjectTo(obj[property], targetValue);
						}
						else if (propertyInfo.access != PropertyInfo.READ_ONLY) {
							var fun:Function = _conversions[propertyInfo.classRef];
							target[property] = fun ? fun(obj[property]) : obj[property];
						}
						else {
							// TODO: Add option to ignore read-only properties?
							_error('Property {} is read-only.', target, property);
						}
					}
					catch (err:ObjectParserError) {
						throw err;
					}
					catch (err:Error) {
						_error('Failed to parse property {}: ' + err.message, target, property);
					}
				}
				else {
					_error('Property {} doesn\'t exist.', target, property);
				}
			}
		}

		protected function _error(message:String, target:Object, property:String):void {
			if (strict)
				throw new ObjectParserError(message.replace(' {}', ''), target, property);
			else
				Logger.warn(this, message.replace('{}', '"' + ClassUtil.getClassName(target) + '.' + property + '"'));
		}

		/**
		 * Parses a JSON-formatted string into the specified target object.
		 * Supports custom conversions using conversion functions.
		 *
		 * @param text The JSON string.
		 * @param target The destination object.
		 * @param reviver A function that transforms each key/value pair that is parsed.
		 */
		public function parseJSONTo(text:String, target:Object, reviver:Function = null):void {
			parseObjectTo(JSON.parse(text, reviver), target);
		}

		protected var _conversions:Dictionary/*Class => Function*/;

		/**
		 * Adds a conversion to the supported conversions list.
		 *
		 * @param toClass The conversion output class.
		 * @param fun The conversion function, is invoked with one argument (value) and should return the converted value:
		 *		function conversion(value:*):toClass;
		 */
		public function addConversion(toClass:Class, fun:Function):void {
			if (!(toClass in _conversions))
				_conversions[toClass] = fun;
		}

		/**
		 * Returns if the specified conversion is registered on the supported conversions list.
		 *
		 * @param toClass The conversion output class.
		 * @return Boolean true if the conversion is registered.
		 */
		public function hasConversion(toClass:Class):Boolean {
			return toClass in _conversions;
		}

		/**
		 * Removes a conversion from the supported conversions list.
		 *
		 * @param toClass The conversion output class.
		 */
		public function removeConversion(toClass:Class):void {
			delete _conversions[toClass];
		}
	}
}
