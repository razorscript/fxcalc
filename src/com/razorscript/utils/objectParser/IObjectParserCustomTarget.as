package com.razorscript.utils.objectParser {

	/**
	 * An ObjectParser target that uses a custom parseObject() method.
	 * @see com.razorscript.utils.objectParser.ObjectParser#parseObjectTo()
	 *
	 * @author Gene Pavlovsky
	 */
	public interface IObjectParserCustomTarget extends IObjectParserTarget {
		/**
		 * Parses the specified object into this object.
		 *
		 * @param obj An object to parse into this object.
		 * @return Boolean true if no further action is required, false if ObjectParser should call the default parseObjectTo() method.
		 */
		function parseObject(obj:Object):Boolean;
	}
}
