package com.razorscript.utils.objectParser.conversions {
	import com.razorscript.utils.Lambda;
	import com.razorscript.utils.enum.EnumUtil;
	import com.razorscript.utils.objectParser.ObjectParser;

	/**
	 * ObjectParser conversion functions.
	 *
	 * @author Gene Pavlovsky
	 */
	public class ObjectParserConversionFun {
		/**
		 * Adds conversions to a formatter and returns that formatter.
		 *
		 * Provided conversions:
		 * 		Enum	converts a string value to IEnum.
		 *
		 * @param objectParser An object parser.
		 * @param enumClasses A vector of enum classes to add enum conversions for.
		 * @return The object parser.
		 */
		public static function extend(objectParser:ObjectParser, enumClasses:Vector.<Class> = null):ObjectParser {
			if (enumClasses) {
				for each (var enumClass:Class in enumClasses)
					objectParser.addConversion(enumClass, Lambda.prepend1(EnumUtil.getValueByName, enumClass));
			}
			return objectParser;
		}
	}
}
