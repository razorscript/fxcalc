package com.razorscript.utils.objectParser {
	
	/**
	 * An ObjectParser target that uses the default parseObjectTo() method provided by the ObjectParser.
	 * @see com.razorscript.utils.objectParser.ObjectParser#parseObjectTo()
	 *
	 * @author Gene Pavlovsky
	 */
	public interface IObjectParserTarget {
	}
}
