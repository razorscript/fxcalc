package com.razorscript.utils {
	import com.razorscript.utils.supportClasses.PropertyInfo;
	import flash.utils.Dictionary;
	import flash.utils.describeType;
	import flash.utils.getDefinitionByName;
	import flash.utils.getQualifiedClassName;
	import flash.utils.getQualifiedSuperclassName;

	/**
	 * Class utility functions.
	 *
	 * @author Gene Pavlovsky
	 */
	public class ClassUtil {
		/**
		 * Returns a reference to the class object of an object.
		 *
		 * @param value The object for which a class object is desired.
		 * @return A reference to the class object of an object.
		 */
		public static function getClass(value:*):Class {
			return (value != null) ? value.constructor : null;
		}

		/**
		 * Returns the class name of an object.
		 *
		 * @param value The object for which a class name is desired.
		 * @return The class name.
		 */
		public static function getClassName(value:*):String {
			var name:String = getQualifiedClassName(value);
			var i:int = name.lastIndexOf('::');
			return (i != -1) ? name.substr(i + 2) : name;
		}

		/**
		 * Returns the class name of an object's base class.
		 *
		 * @param value The object for which a base class name is desired.
		 * @return The base class name.
		 */
		public static function getSuperclassName(value:*):String {
			var name:String = getQualifiedSuperclassName(value);
			var i:int = name.lastIndexOf('::');
			return (i != -1) ? name.substr(i + 2) : name;
		}

		/**
		 * Returns a reference to the class object of the class specified by the name parameter.
		 *
		 * @param qualifiedClassName The class name.
		 * @return A reference to the class object of the class specified by the name parameter.
		 */
		public static function getClassReference(qualifiedClassName:String):Class {
			try {
				return getDefinitionByName(qualifiedClassName) as Class;
			}
			catch (err:ReferenceError) {
			}
			return null;
		}

		/**
		 * Returns an object mapping property names to property information objects.
		 * Describes all public non-static properties defined by a class.
		 *
		 * @param classRef A class object.
		 * @return An object mapping property names to property information objects.
		 */
		public static function getClassPropertyInfo(classRef:Class):Object {
			var map:Object = classPropertyInfo[classRef];
			if (map)
				return map;
			map = classPropertyInfo[classRef] = {};
			var factoryXML:XML = describeType(classRef).factory[0];
			var constList:XMLList = factoryXML.constant;
			var varList:XMLList = factoryXML.variable;
			var setterList:XMLList = factoryXML.accessor;
			var varXML:XML;
			for each (varXML in constList)
				map[varXML.@name] = new PropertyInfo(varXML.@type, getClassReference(varXML.@type), PropertyInfo.READ_ONLY);
			for each (varXML in varList)
				map[varXML.@name] = new PropertyInfo(varXML.@type, getClassReference(varXML.@type), PropertyInfo.READ_WRITE);
			for each (varXML in setterList)
				map[varXML.@name] = new PropertyInfo(varXML.@type, getClassReference(varXML.@type), _getPropertyAccess(varXML.@access));
			return map;
		}

		[Inline]
		protected static function _getPropertyAccess(access:String):int {
			return (access == 'readwrite') ? PropertyInfo.READ_WRITE : (access == 'readonly') ? PropertyInfo.READ_ONLY : PropertyInfo.WRITE_ONLY;
		}

		protected static var classPropertyInfo:Dictionary/*Class => Object*/ = new Dictionary();
	}
}
