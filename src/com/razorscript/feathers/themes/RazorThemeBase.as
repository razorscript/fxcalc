package com.razorscript.feathers.themes {
	import com.razorscript.feathers.themes.ThemeBase;
	import feathers.controls.Alert;
	import feathers.controls.Button;
	import feathers.controls.ButtonGroup;
	import feathers.controls.ButtonState;
	import feathers.controls.Callout;
	import feathers.controls.Check;
	import feathers.controls.Header;
	import feathers.controls.ImageLoader;
	import feathers.controls.Label;
	import feathers.controls.Panel;
	import feathers.controls.Radio;
	import feathers.controls.ScrollContainer;
	import feathers.controls.Scroller;
	import feathers.controls.SimpleScrollBar;
	import feathers.controls.Slider;
	import feathers.controls.TextCallout;
	import feathers.controls.ToggleButton;
	import feathers.controls.TrackInteractionMode;
	import feathers.controls.TrackLayoutMode;
	import feathers.controls.text.StageTextTextEditor;
	import feathers.controls.text.TextBlockTextEditor;
	import feathers.controls.text.TextBlockTextRenderer;
	import feathers.core.FeathersControl;
	import feathers.core.PopUpManager;
	import feathers.layout.Direction;
	import feathers.layout.HorizontalAlign;
	import feathers.layout.VerticalAlign;
	import feathers.skins.ImageSkin;
	import feathers.system.DeviceCapabilities;
	import flash.geom.Rectangle;
	import flash.text.engine.CFFHinting;
	import flash.text.engine.FontDescription;
	import flash.text.engine.FontLookup;
	import flash.text.engine.FontPosture;
	import flash.text.engine.FontWeight;
	import flash.text.engine.RenderingMode;
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.text.TextFormat;
	import starling.textures.Texture;

	// TODO: Integrate changes in Feathers 3 default theme.

	/**
	 * Base class for the Razor theme for mobile apps.
	 * Handles everything except asset loading, which is left to subclasses.
	 *
	 * @see com.razorscript.feathers.themes.RazorTheme
	 *
	 * @author Gene Pavlovsky
	 */
	public class RazorThemeBase extends ThemeBase {
		protected static const LONG_PRESS_DURATION:Number = 0.3;

		/** The screen density of an iPhone with Retina display. The textures used by this theme are designed for this density and scale for other densities. */
		protected static const ORIGINAL_DPI_IPHONE_RETINA:int = 326;
		/** The screen density of an iPad with Retina display. The textures used by this theme are designed for this density and scale for other densities. */
		protected static const ORIGINAL_DPI_IPAD_RETINA:int = 264;

		/** The name of the embedded font used by controls in this theme. Available in normal, bold and italic. */
		protected static const FONT_NAME:String = 'DejaVuSansMono';

		// TODO: Auto-generate RazorThemeColor, RazorThemeStyle and RazorThemeTexture/RazorThemeSkin from JSON.
		protected static const COLOR_STAGE_BACKGROUND:String = 'stage-background';
		protected static const COLOR_BACKGROUND:String = 'background';
		protected static const COLOR_INVERTED_BACKGROUND:String = 'inverted-background';
		protected static const COLOR_TEXT:String = 'text';
		protected static const COLOR_DIM_TEXT:String = 'text-dim';
		protected static const COLOR_DISABLED_TEXT:String = 'text-disabled';
		protected static const COLOR_SELECTED_TEXT:String = 'text-selected';
		protected static const COLOR_INVERTED_TEXT:String = 'inverted-text';
		protected static const COLOR_INVERTED_DIM_TEXT:String = 'inverted-text-dim';
		protected static const COLOR_INVERTED_DISABLED_TEXT:String = 'inverted-text-disabled';
		protected static const COLOR_INVERTED_SELECTED_TEXT:String = 'inverted-text-selected';
		protected static const COLOR_LIST_BACKGROUND:String = 'list-background';
		protected static const COLOR_TAB_BACKGROUND:String = 'tab-background';
		protected static const COLOR_TAB_DISABLED_BACKGROUND:String = 'tab-background-disabled';
		protected static const COLOR_GROUPED_LIST_HEADER_BACKGROUND:String = 'grouped-list-header-background';
		protected static const COLOR_GROUPED_LIST_FOOTER_BACKGROUND:String = 'grouped-list-footer-background';
		protected static const COLOR_POPUP_BACKGROUND:String = 'popup-background';

		protected static const COLOR_BUTTON_STATES:String = 'button-states';
		protected static const COLOR_BUTTON_GROUP_BUTTON_STATES:String = 'button-group-button-states';
		protected static const COLOR_SIMPLE_BUTTON_STATES:String = 'simple-button-states';
		protected static const COLOR_ALERT_BUTTON_STATES:String = 'alert-button-states';

		protected static const COLOR_MODAL_OVERLAY:String = 'modal-overlay';
		protected static const COLOR_MODAL_OVERLAY_ALPHA:String = 'modal-overlay-alpha';
		protected static const COLOR_CALLOUT_ARROW:String = 'callout-arrow';
		protected static const COLOR_CALLOUT_OVERLAY:String = 'callout-overlay';
		protected static const COLOR_CALLOUT_OVERLAY_ALPHA:String = 'callout-overlay-alpha';
		protected static const COLOR_DRAWER_OVERLAY:String = 'drawer-overlay';
		protected static const COLOR_DRAWER_OVERLAY_ALPHA:String = 'drawer-overlay-alpha';

		// TODO: To be deprecated. Refactor FXCalcRazorThemeBase.setKeypadFunButtonStyles().
		protected static const _BTN_BASE02_SKIN:String = '_button-base02';
		protected static const _BTN_BASE01_SKIN:String = '_button-base01';
		protected static const _BTN_BASE1_SKIN:String = '_button-base1';
		protected static const _BTN_BASE2_SKIN:String = '_button-base2';
		protected static const _BTN_BASE02_SEL_SKIN:String = '_button-base02-selected';
		protected static const _BTN_BASE01_SEL_SKIN:String = '_button-base01-selected';
		protected static const _BTN_BASE1_SEL_SKIN:String = '_button-base1-selected';
		protected static const _BTN_BASE2_SEL_SKIN:String = '_button-base2-selected';

		protected static const BTN_WHITE_SKIN:String = 'button-white';

		protected static const BG_POPUP_SKIN:String = 'background-popup';
		protected static const HEADER_BG_SKIN:String = 'header-background';

		protected static const CALLOUT_ARROW_TOP_SKIN:String = 'callout-arrow-top';
		protected static const CALLOUT_ARROW_BOTTOM_SKIN:String = 'callout-arrow-bottom';
		protected static const CALLOUT_ARROW_LEFT_SKIN:String = 'callout-arrow-left';
		protected static const CALLOUT_ARROW_RIGHT_SKIN:String = 'callout-arrow-right';

		/** Style name for item renderers in a PickerList. */
		protected static const THEME_STYLE_NAME_PICKER_LIST_ITEM_RENDERER:String = 'solarized-picker-list-item-renderer';
		/** Style name for item renderers in a SpinnerList. */
		protected static const THEME_STYLE_NAME_SPINNER_LIST_ITEM_RENDERER:String = 'solarized-spinner-list-item-renderer';
		/** Style name for buttons in an Alert's button group. */
		protected static const THEME_STYLE_NAME_ALERT_BUTTON_GROUP_BUTTON:String = 'solarized-alert-button-group-button';
		/** Style name for the thumb of a horizontal SimpleScrollBar. */
		protected static const THEME_STYLE_NAME_HORIZONTAL_SIMPLE_SCROLL_BAR_THUMB:String = 'solarized-horizontal-simple-scroll-bar-thumb';
		/** Style name for the thumb of a vertical SimpleScrollBar. */
		protected static const THEME_STYLE_NAME_VERTICAL_SIMPLE_SCROLL_BAR_THUMB:String = 'solarized-vertical-simple-scroll-bar-thumb';
		/** Style name for the minimum track of a horizontal slider. */
		protected static const THEME_STYLE_NAME_HORIZONTAL_SLIDER_MINIMUM_TRACK:String = 'solarized-horizontal-slider-minimum-track';
		/** Style name for the maximum track of a horizontal slider. */
		protected static const THEME_STYLE_NAME_HORIZONTAL_SLIDER_MAXIMUM_TRACK:String = 'solarized-horizontal-slider-maximum-track';
		/** Style name for the minimum track of a vertical slider. */
		protected static const THEME_STYLE_NAME_VERTICAL_SLIDER_MINIMUM_TRACK:String = 'solarized-vertical-slider-minimum-track';
		/** Style name for the maximum track of a vertical slider. */
		protected static const THEME_STYLE_NAME_VERTICAL_SLIDER_MAXIMUM_TRACK:String = 'solarized-vertical-slider-maximum-track';

		/**
		 * The default global text renderer factory for this theme creates a TextBlockTextRenderer.
		 */
		protected function textRendererFactory():TextBlockTextRenderer {
			var renderer:TextBlockTextRenderer = new TextBlockTextRenderer();
			renderer.truncateToFit = false;
			return renderer;
		}

		/**
		 * The default global text editor factory for this theme creates a StageTextTextEditor.
		 */
		protected function textEditorFactory():StageTextTextEditor {
			return new StageTextTextEditor();
		}

		/**
		 * The text editor factory for a NumericStepper creates a TextBlockTextEditor.
		 */
		protected function stepperTextEditorFactory():TextBlockTextEditor {
			// We're only using this text editor in the NumericStepper because isEditable is false on the TextInput.
			// This text editor is not suitable for mobile use if the TextInput needs to be editable because it can't use the soft keyboard or other mobile-friendly UI.
			return new TextBlockTextEditor();
		}

		/**
		 * This theme's scroll bar type is SimpleScrollBar.
		 */
		protected function scrollBarFactory():SimpleScrollBar {
			return new SimpleScrollBar();
		}

		protected function popUpOverlayFactory():DisplayObject {
			var quad:Quad = new Quad(10, 10, getColor(COLOR_MODAL_OVERLAY));
			quad.alpha = getColor(COLOR_MODAL_OVERLAY_ALPHA);
			return quad;
		}

		protected function calloutOverlayFactory():DisplayObject {
			var quad:Quad = new Quad(10, 10, getColor(COLOR_CALLOUT_OVERLAY));
			quad.alpha = getColor(COLOR_CALLOUT_OVERLAY_ALPHA);
			return quad;
		}

		/**
		 * SmartDisplayObjectValueSelectors will use ImageLoader instead of Image so that we can use extra features like pixel snapping.
		 */
		protected function textureValueTypeHandler(value:Texture, oldDisplayObject:DisplayObject = null):DisplayObject {
			var displayObject:ImageLoader = oldDisplayObject as ImageLoader;
			if (!displayObject)
				displayObject = new ImageLoader();
			displayObject.source = value;
			displayObject.pixelSnapping = true;
			return displayObject;
		}

		/**
		 * Constructor.
		 *
		 * @param styleName Style name.
		 * @param colorSchemeName Color scheme name.
		 * @param scaleToDPI Whether the theme's skins should be scaled based on the screen density and content scale factor.
		 * @param designDPI The screen density that the textures used by this theme are designed for.
		 */
		public function RazorThemeBase(styleName:String = null, colorSchemeName:String = null, scaleToDPI:Boolean = true, designDPI:int = 0) {
			super(styleName, colorSchemeName, scaleToDPI, designDPI ? designDPI : DeviceCapabilities.isTablet(Starling.current.nativeStage) ? ORIGINAL_DPI_IPAD_RETINA : ORIGINAL_DPI_IPHONE_RETINA);
		}

		/** A smaller font size for details. */
		protected var tinyFontSize:int;
		/** A small font size. */
		protected var smallFontSize:int;
		/** A normal font size. */
		protected var regularFontSize:int;
		/** A larger font size for headers. */
		protected var largeFontSize:int;
		/** An extra large font size. */
		protected var extraLargeFontSize:int;

		/** The size, in pixels, of major regions in the grid. Used for sizing containers and larger UI controls. */
		protected var gridSize:int;
		/** The size, in pixels, of minor regions in the grid. Used for larger padding and gaps. */
		protected var gutterSize:int;
		/** The size, in pixels, of smaller padding and gaps within the major regions in the grid. */
		protected var smallGutterSize:int;
		/** The width, in pixels, of UI controls that span across multiple grid regions. */
		protected var wideControlSize:int;
		/** The size, in pixels, of a typical UI control. */
		protected var controlSize:int;
		/** The size, in pixels, of smaller UI controls. */
		protected var smallControlSize:int;

		protected var popUpFillSize:int;
		protected var calloutBackgroundMinSize:int;
		protected var calloutStagePadding:int;
		protected var calloutArrowGap:int;
		protected var scrollBarGutterSize:int;

		/** The FTE FontDescription used for text of normal style and bold weight. */
		protected var boldFontDescription:FontDescription;
		/** The FTE FontDescription used for text of italic style and normal weight. */
		protected var italicFontDescription:FontDescription;
		/** ScrollText uses TextField instead of FTE, so it has a separate TextFormat. */
		protected var scrollTextTextFormat:TextFormat;
		/** ScrollText uses TextField instead of FTE, so it has a separate disabled TextFormat. */
		protected var scrollTextDisabledTextFormat:TextFormat;

		/**
		 * Initializes the theme. Expected to be called by subclasses after the assets have been loaded and the skin texture atlas has been created.
		 */
		override protected function initialize():void {
			super.initialize();

			// TODO: Refactor initialize() and validateTheme(). Move most of the init code to validateTheme (and rename to validate)?
			initDimensions();
			initFonts();
			initTextures();
			initGlobals();
			initStage();
			initStyleProviders();
		}

		override protected function validateTheme():void {
			initDimensions();
			initFonts();
			initGlobals();
			initStage();
		}

		/**
		 * Initializes common values used for setting the dimensions of components.
		 */
		protected function initDimensions():void {
			gridSize = scaleValue(88);
			smallGutterSize = scaleValue(11);
			gutterSize = scaleValue(22);
			controlSize = scaleValue(58);
			smallControlSize = scaleValue(22);
			popUpFillSize = scaleValue(552);
			calloutBackgroundMinSize = scaleValue(11);
			calloutStagePadding = scaleValue(4);
			calloutArrowGap = -scaleValue(4);
			scrollBarGutterSize = scaleValue(4);
			wideControlSize = gridSize * 3 + gutterSize * 2;
		}

		/**
		 * Initializes font sizes and formats.
		 */
		protected function initFonts():void {
			tinyFontSize = getFontSize('font-size-tiny'); // scaleValue(18);
			smallFontSize = getFontSize('font-size-small'); // scaleValue(20);
			regularFontSize = getFontSize('font-size-regular'); // scaleValue(24);
			largeFontSize = getFontSize('font-size-large'); // scaleValue(28);
			extraLargeFontSize = getFontSize('font-size-extra-large'); // scaleValue(36);

			// These are for components that don't use FTE.
			scrollTextTextFormat = new TextFormat('_sans', regularFontSize, getColor(COLOR_TEXT));
			scrollTextDisabledTextFormat = new TextFormat('_sans', regularFontSize, getColor(COLOR_DISABLED_TEXT));

			regularFontDescription = new FontDescription(FONT_NAME, FontWeight.NORMAL, FontPosture.NORMAL, FontLookup.EMBEDDED_CFF, RenderingMode.CFF, CFFHinting.HORIZONTAL_STEM);
			boldFontDescription = new FontDescription(FONT_NAME, FontWeight.BOLD, FontPosture.NORMAL, FontLookup.EMBEDDED_CFF, RenderingMode.CFF, CFFHinting.HORIZONTAL_STEM);
			italicFontDescription = new FontDescription(FONT_NAME, FontWeight.NORMAL, FontPosture.ITALIC, FontLookup.EMBEDDED_CFF, RenderingMode.CFF, CFFHinting.HORIZONTAL_STEM);
		}

		/**
		 * Initializes the textures by extracting them from the atlas and setting up any scaling grids that are needed.
		 */
		protected function initTextures():void {
			createTextures([
				_BTN_BASE02_SKIN, _BTN_BASE01_SKIN, _BTN_BASE1_SKIN, _BTN_BASE2_SKIN,	_BTN_BASE02_SEL_SKIN, _BTN_BASE01_SEL_SKIN, _BTN_BASE1_SEL_SKIN, _BTN_BASE2_SEL_SKIN,
				BTN_WHITE_SKIN]);

			addTextureAliases(BTN_WHITE_SKIN, BG_POPUP_SKIN);

			createTextures([HEADER_BG_SKIN]);
			createTextures([CALLOUT_ARROW_TOP_SKIN, CALLOUT_ARROW_BOTTOM_SKIN, CALLOUT_ARROW_LEFT_SKIN, CALLOUT_ARROW_RIGHT_SKIN]);
		}

		/**
		 * Initializes global variables (not including global style providers).
		 */
		protected function initGlobals():void {
			FeathersControl.defaultTextRendererFactory = textRendererFactory;
			FeathersControl.defaultTextEditorFactory = textEditorFactory;

			PopUpManager.overlayFactory = popUpOverlayFactory;
			Callout.calloutOverlayFactory = calloutOverlayFactory;
			Callout.stagePaddingTop = Callout.stagePaddingBottom = calloutStagePadding;
		}

		/**
		 * Sets the stage background color.
		 */
		protected function initStage():void {
			Starling.current.nativeStage.color = Starling.current.stage.color = getColor(COLOR_STAGE_BACKGROUND);
		}

		/**
		 * Sets global style providers for all components.
		 */
		protected function initStyleProviders():void {
			//alert
			getStyleProviderForClass(Alert).defaultStyleFunction = setAlertStyles;
			getStyleProviderForClass(ButtonGroup).setFunctionForStyleName(Alert.DEFAULT_CHILD_STYLE_NAME_BUTTON_GROUP, setAlertButtonGroupStyles);
			getStyleProviderForClass(Button).setFunctionForStyleName(THEME_STYLE_NAME_ALERT_BUTTON_GROUP_BUTTON, setAlertButtonGroupButtonStyles);
			getStyleProviderForClass(Header).setFunctionForStyleName(Alert.DEFAULT_CHILD_STYLE_NAME_HEADER, setHeaderWithoutBackgroundStyles);

			//button
			getStyleProviderForClass(Button).defaultStyleFunction = setButtonStyles;

			//button group
			getStyleProviderForClass(ButtonGroup).defaultStyleFunction = setButtonGroupStyles;
			getStyleProviderForClass(Button).setFunctionForStyleName(ButtonGroup.DEFAULT_CHILD_STYLE_NAME_BUTTON, setButtonGroupButtonStyles);
			getStyleProviderForClass(ToggleButton).setFunctionForStyleName(ButtonGroup.DEFAULT_CHILD_STYLE_NAME_BUTTON, setButtonGroupButtonStyles);

			//callout
			getStyleProviderForClass(Callout).defaultStyleFunction = setCalloutStyles;

			//check
			getStyleProviderForClass(Check).defaultStyleFunction = setCheckStyles;

			//header
			getStyleProviderForClass(Header).defaultStyleFunction = setHeaderStyles;

			//labels
			getStyleProviderForClass(Label).defaultStyleFunction = setLabelStyles;

			//radio
			getStyleProviderForClass(Radio).defaultStyleFunction = setRadioStyles;

			//scroll container
			getStyleProviderForClass(ScrollContainer).defaultStyleFunction = setScrollContainerStyles;
			//getStyleProviderForClass(ScrollContainer).setFunctionForStyleName(ScrollContainer.ALTERNATE_STYLE_NAME_TOOLBAR, setToolbarScrollContainerStyles);

			//simple scroll bar
			getStyleProviderForClass(SimpleScrollBar).defaultStyleFunction = setSimpleScrollBarStyles;
			//getStyleProviderForClass(Button).setFunctionForStyleName(THEME_STYLE_NAME_HORIZONTAL_SIMPLE_SCROLL_BAR_THUMB, setHorizontalSimpleScrollBarThumbStyles);
			//getStyleProviderForClass(Button).setFunctionForStyleName(THEME_STYLE_NAME_VERTICAL_SIMPLE_SCROLL_BAR_THUMB, setVerticalSimpleScrollBarThumbStyles);

			//slider
			getStyleProviderForClass(Slider).defaultStyleFunction = setSliderStyles;
			getStyleProviderForClass(Button).setFunctionForStyleName(Slider.DEFAULT_CHILD_STYLE_NAME_THUMB, setSimpleButtonStyles);
			getStyleProviderForClass(Button).setFunctionForStyleName(THEME_STYLE_NAME_HORIZONTAL_SLIDER_MINIMUM_TRACK, setHorizontalSliderMinimumTrackStyles);
			getStyleProviderForClass(Button).setFunctionForStyleName(THEME_STYLE_NAME_HORIZONTAL_SLIDER_MAXIMUM_TRACK, setHorizontalSliderMaximumTrackStyles);
			getStyleProviderForClass(Button).setFunctionForStyleName(THEME_STYLE_NAME_VERTICAL_SLIDER_MINIMUM_TRACK, setVerticalSliderMinimumTrackStyles);
			getStyleProviderForClass(Button).setFunctionForStyleName(THEME_STYLE_NAME_VERTICAL_SLIDER_MAXIMUM_TRACK, setVerticalSliderMaximumTrackStyles);

			//text callout
			getStyleProviderForClass(TextCallout).defaultStyleFunction = setTextCalloutStyles;

			//toggle button
			getStyleProviderForClass(ToggleButton).defaultStyleFunction = setButtonStyles;
		}

		/***********************************************************************************************************************************************/

		/**
		 * Shared
		 */

		protected function setScrollerStyles(scroller:Scroller):void {
			scroller.horizontalScrollBarFactory = scrollBarFactory;
			scroller.verticalScrollBarFactory = scrollBarFactory;
		}

		protected function setSimpleButtonStyles(button:Button):void {
			setColoredButtonStates(button, {up: getTexture(BTN_WHITE_SKIN)}, getColor(COLOR_SIMPLE_BUTTON_STATES));
			button.hasLabelTextRenderer = false;
			button.minWidth = button.minHeight = controlSize - scaleValue(10);
			button.minTouchWidth = button.minTouchHeight = gridSize;
		}

		/***********************************************************************************************************************************************/

		/**
		 * Alert
		 */

		protected function setAlertStyles(alert:Alert):void {
			setScrollerStyles(alert);

			var backgroundSkin:Image = new Image(getTexture(BG_POPUP_SKIN));
			backgroundSkin.color = getColor(COLOR_POPUP_BACKGROUND);
			alert.backgroundSkin = backgroundSkin;

			alert.fontStyles = getTextFormat(smallFontSize, COLOR_TEXT);
			alert.messageProperties.wordWrap = true;

			alert.paddingTop = 0;
			alert.paddingRight = gutterSize;
			alert.paddingBottom = smallGutterSize;
			alert.paddingLeft = gutterSize;
			alert.gap = smallGutterSize;
			alert.maxWidth = popUpFillSize;
			alert.maxHeight = popUpFillSize;
		}

		//see Panel section for Header styles

		protected function setAlertButtonGroupStyles(group:ButtonGroup):void {
			group.direction = Direction.HORIZONTAL;
			group.horizontalAlign = HorizontalAlign.CENTER;
			group.verticalAlign = VerticalAlign.JUSTIFY;
			group.distributeButtonSizes = false;
			group.gap = smallGutterSize;
			group.padding = smallGutterSize;
			group.customButtonStyleName = THEME_STYLE_NAME_ALERT_BUTTON_GROUP_BUTTON;
		}

		protected function setAlertButtonGroupButtonStyles(button:Button):void {
			setBaseButtonStyles(button);
			setColoredButtonStates(button, {up: getTexture(BTN_WHITE_SKIN)}, getColor(COLOR_ALERT_BUTTON_STATES));
			button.minWidth = 2 * controlSize;
		}

		/***********************************************************************************************************************************************/

		/**
		 * Button
		 */

		protected function setBaseButtonStyles(button:Button):void {
			button.fontStyles = getTextFormat(regularFontSize, COLOR_TEXT, boldFontDescription);
			button.disabledFontStyles = getTextFormat(regularFontSize, COLOR_DISABLED_TEXT, boldFontDescription);
			if (button is ToggleButton)
				(button as ToggleButton).selectedFontStyles = getTextFormat(regularFontSize, COLOR_SELECTED_TEXT, boldFontDescription);
			button.longPressDuration = LONG_PRESS_DURATION;
			button.verticalAlign = VerticalAlign.MIDDLE;
			button.paddingTop = smallGutterSize;
			button.paddingBottom = smallGutterSize;
			button.paddingLeft = gutterSize;
			button.paddingRight = gutterSize;
			button.gap = smallGutterSize;
			button.minGap = smallGutterSize;
			button.minWidth = controlSize;
			button.minHeight = controlSize;
			button.minTouchWidth = gridSize;
			button.minTouchHeight = gridSize;
		}

		protected function setColoredButtonStates(button:Button, skins:Object, colors:Object, size:int = 0):void {
			if (!size)
				size = controlSize;

			var skin:ImageSkin = new ImageSkin(skins.up);
			if (colors.up)
				skin.defaultColor = colors.up;
			if (skins.hover)
				skin.setTextureForState(ButtonState.HOVER, skins.hover);
			if (colors.hover)
				skin.setColorForState(ButtonState.HOVER, colors.hover);
			if (skins.down)
				skin.setTextureForState(ButtonState.HOVER, skins.down);
			if (colors.down)
				skin.setColorForState(ButtonState.DOWN, colors.down);
			if (skins.disabled)
				skin.setTextureForState(ButtonState.DISABLED, skins.disabled);
			if (colors.disabled)
				skin.setColorForState(ButtonState.DISABLED, colors.disabled);
			if (button is ToggleButton) {
				if (skins.selectedUp)
					skin.selectedTexture = skins.selectedUp;
				if (colors.selectedUp)
					skin.selectedColor = colors.selectedUp;
				if (skins.selectedHover)
					skin.setTextureForState(ButtonState.HOVER_AND_SELECTED, skins.selectedHover);
				if (colors.selectedHover)
					skin.setColorForState(ButtonState.HOVER_AND_SELECTED, colors.selectedHover);
				if (skins.selectedDown)
					skin.setTextureForState(ButtonState.HOVER_AND_SELECTED, skins.selectedDown);
				if (colors.selectedDown)
					skin.setColorForState(ButtonState.DOWN_AND_SELECTED, colors.selectedDown);
				if (skins.selectedDisabled)
					skin.setTextureForState(ButtonState.DISABLED_AND_SELECTED, skins.selectedDisabled);
				if (colors.selectedDisabled)
					skin.setColorForState(ButtonState.DISABLED_AND_SELECTED, colors.selectedDisabled);
			}
			skin.width = skin.height = size;
			button.defaultSkin = skin;
		}

		protected function setButtonSkins(button:Button, skins:Object, size:int = 0):void {
			if (!size)
				size = controlSize;

			var skin:ImageSkin = new ImageSkin(skins.up);
			if (skins.hover)
				skin.setTextureForState(ButtonState.HOVER, skins.hover);
			if (skins.down)
				skin.setTextureForState(ButtonState.DOWN, skins.down);
			if (skins.disabled)
				skin.setTextureForState(ButtonState.DISABLED, skins.disabled);
			if (button is ToggleButton) {
				if (skins.selectedUp)
					skin.selectedTexture = skins.selectedUp;
				if (skins.selectedHover)
					skin.setTextureForState(ButtonState.HOVER_AND_SELECTED, skins.selectedHover);
				if (skins.selectedDown)
					skin.setTextureForState(ButtonState.DOWN_AND_SELECTED, skins.selectedDown);
				if (skins.selectedDisabled)
					skin.setTextureForState(ButtonState.DISABLED_AND_SELECTED, skins.selectedDisabled);
			}
			skin.width = skin.height = size;
			button.defaultSkin = skin;
		}

		protected function setButtonStyles(button:Button):void {
			setBaseButtonStyles(button);
			setColoredButtonStates(button, {up: getTexture(BTN_WHITE_SKIN)}, getColor(COLOR_BUTTON_STATES));
		}

		/***********************************************************************************************************************************************/

		/**
		 * ButtonGroup
		 */

		protected function setButtonGroupStyles(group:ButtonGroup):void {
			group.minWidth = popUpFillSize;
			group.gap = smallGutterSize;
		}

		protected function setButtonGroupButtonStyles(button:Button):void {
			setColoredButtonStates(button, {up: getTexture(BTN_WHITE_SKIN)}, getColor(COLOR_BUTTON_GROUP_BUTTON_STATES));

			button.paddingTop = smallGutterSize;
			button.paddingBottom = smallGutterSize;
			button.paddingLeft = gutterSize;
			button.paddingRight = gutterSize;
			button.gap = smallGutterSize;
			button.minGap = smallGutterSize;
			button.minWidth = gridSize;
			button.minHeight = gridSize;
			button.minTouchWidth = gridSize;
			button.minTouchHeight = gridSize;
		}

		/***********************************************************************************************************************************************/

		/**
		 * Callout
		 */

		protected function setCalloutStyles(callout:Callout):void {
			var backgroundSkin:Image = new Image(getTexture(BG_POPUP_SKIN));
			backgroundSkin.color = getColor(COLOR_POPUP_BACKGROUND);
			backgroundSkin.width = calloutBackgroundMinSize;
			backgroundSkin.height = calloutBackgroundMinSize;
			callout.backgroundSkin = backgroundSkin;
			var arrowColor:int = getColor(COLOR_CALLOUT_ARROW);

			var topArrowSkin:Image = new Image(getTexture(CALLOUT_ARROW_TOP_SKIN));
			topArrowSkin.color = arrowColor;
			callout.topArrowSkin = topArrowSkin;

			var rightArrowSkin:Image = new Image(getTexture(CALLOUT_ARROW_RIGHT_SKIN));
			rightArrowSkin.color = arrowColor;
			callout.rightArrowSkin = rightArrowSkin;

			var bottomArrowSkin:Image = new Image(getTexture(CALLOUT_ARROW_BOTTOM_SKIN));
			bottomArrowSkin.color = arrowColor;
			callout.bottomArrowSkin = bottomArrowSkin;

			var leftArrowSkin:Image = new Image(getTexture(CALLOUT_ARROW_LEFT_SKIN));
			leftArrowSkin.color = arrowColor;
			callout.leftArrowSkin = leftArrowSkin;

			callout.padding = smallGutterSize;
			callout.topArrowGap = callout.bottomArrowGap = callout.leftArrowGap = callout.rightArrowGap = calloutArrowGap;
		}

		protected function setTextCalloutStyles(callout:TextCallout):void {
			setCalloutStyles(callout);

			callout.fontStyles = getTextFormat(regularFontSize, COLOR_TEXT);
			callout.disabledFontStyles = getTextFormat(regularFontSize, COLOR_DISABLED_TEXT);
		}

		/***********************************************************************************************************************************************/

		/**
		 * Check
		 */

		protected function setCheckStyles(check:Check):void {
			check.fontStyles = getTextFormat(regularFontSize, COLOR_TEXT, boldFontDescription);
			check.disabledFontStyles = getTextFormat(regularFontSize, COLOR_DISABLED_TEXT, boldFontDescription);
			check.selectedFontStyles = getTextFormat(regularFontSize, COLOR_SELECTED_TEXT, boldFontDescription);

			check.gap = smallGutterSize;
			check.minWidth = controlSize;
			check.minHeight = controlSize;
			check.minTouchWidth = gridSize;
			check.minTouchHeight = gridSize;
		}

		/***********************************************************************************************************************************************/

		/**
		 * Header
		 */

		protected function setHeaderStyles(header:Header):void {
			header.minWidth = gridSize;
			header.minHeight = gridSize;
			header.padding = smallGutterSize;
			header.gap = smallGutterSize;
			header.titleGap = smallGutterSize;

			header.fontStyles = getTextFormat(extraLargeFontSize, COLOR_TEXT, boldFontDescription);
			header.disabledFontStyles = getTextFormat(extraLargeFontSize, COLOR_DISABLED_TEXT, boldFontDescription);

			var backgroundSkin:Image = new Image(getTexture(HEADER_BG_SKIN));
			backgroundSkin.tileGrid = new Rectangle();
			backgroundSkin.width = gridSize;
			backgroundSkin.height = gridSize;
			header.backgroundSkin = backgroundSkin;
		}

		/***********************************************************************************************************************************************/

		/**
		 * Label
		 */

		protected function setLabelStyles(label:Label):void {
			label.fontStyles = getTextFormat(regularFontSize, COLOR_TEXT);
			label.disabledFontStyles = getTextFormat(regularFontSize, COLOR_DISABLED_TEXT);
		}

		protected function setHeadingLabelStyles(label:Label):void {
			label.fontStyles = getTextFormat(largeFontSize, COLOR_TEXT);
			label.disabledFontStyles = getTextFormat(largeFontSize, COLOR_DISABLED_TEXT);
		}

		protected function setDetailLabelStyles(label:Label):void {
			label.fontStyles = getTextFormat(tinyFontSize, COLOR_TEXT);
			label.disabledFontStyles = getTextFormat(tinyFontSize, COLOR_DISABLED_TEXT);
		}

		/***********************************************************************************************************************************************/

		/**
		 * Panel
		 */

		protected function setPanelStyles(panel:Panel):void {
			setScrollerStyles(panel);

			var backgroundSkin:Image = new Image(getTexture(BG_POPUP_SKIN));
			backgroundSkin.color = getColor(COLOR_POPUP_BACKGROUND);
			panel.backgroundSkin = backgroundSkin;

			panel.paddingTop = 0;
			panel.paddingRight = smallGutterSize;
			panel.paddingBottom = smallGutterSize;
			panel.paddingLeft = smallGutterSize;
		}

		protected function setHeaderWithoutBackgroundStyles(header:Header):void {
			header.minWidth = gridSize;
			header.minHeight = gridSize;
			header.padding = smallGutterSize;
			header.gap = smallGutterSize;
			header.titleGap = smallGutterSize;

			header.fontStyles = getTextFormat(extraLargeFontSize, COLOR_TEXT, boldFontDescription);
			header.disabledFontStyles = getTextFormat(extraLargeFontSize, COLOR_DISABLED_TEXT, boldFontDescription);
		}

		/***********************************************************************************************************************************************/

		/**
		 * Radio
		 */

		protected function setRadioStyles(radio:Radio):void {
			radio.fontStyles = getTextFormat(regularFontSize, COLOR_TEXT, boldFontDescription);
			radio.disabledFontStyles = getTextFormat(regularFontSize, COLOR_DISABLED_TEXT, boldFontDescription);
			radio.selectedFontStyles = getTextFormat(regularFontSize, COLOR_SELECTED_TEXT, boldFontDescription);

			radio.gap = smallGutterSize;
			radio.minWidth = controlSize;
			radio.minHeight = controlSize;
			radio.minTouchWidth = gridSize;
			radio.minTouchHeight = gridSize;
		}

		/***********************************************************************************************************************************************/

		/**
		 * ScrollContainer
		 */

		protected function setScrollContainerStyles(container:ScrollContainer):void {
			setScrollerStyles(container);
		}

		/***********************************************************************************************************************************************/

		/**
		 * SimpleScrollBar
		 */

		protected function setSimpleScrollBarStyles(scrollBar:SimpleScrollBar):void {
			if (scrollBar.direction == Direction.HORIZONTAL) {
				scrollBar.paddingRight = scrollBarGutterSize;
				scrollBar.paddingBottom = scrollBarGutterSize;
				scrollBar.paddingLeft = scrollBarGutterSize;
			}
			else {
				scrollBar.paddingTop = scrollBarGutterSize;
				scrollBar.paddingRight = scrollBarGutterSize;
				scrollBar.paddingBottom = scrollBarGutterSize;
			}
		}

		/***********************************************************************************************************************************************/

		/**
		 * Slider
		 */

		protected function setSliderStyles(slider:Slider):void {
			slider.trackInteractionMode = TrackInteractionMode.TO_VALUE;
			slider.trackLayoutMode = TrackLayoutMode.SPLIT;
			if (slider.direction == Direction.VERTICAL) {
				slider.customMinimumTrackStyleName = THEME_STYLE_NAME_VERTICAL_SLIDER_MINIMUM_TRACK;
				slider.customMaximumTrackStyleName = THEME_STYLE_NAME_VERTICAL_SLIDER_MAXIMUM_TRACK;
			}
			else {
				slider.customMinimumTrackStyleName = THEME_STYLE_NAME_HORIZONTAL_SLIDER_MINIMUM_TRACK;
				slider.customMaximumTrackStyleName = THEME_STYLE_NAME_HORIZONTAL_SLIDER_MAXIMUM_TRACK;
			}
		}

		protected function setHorizontalSliderMinimumTrackStyles(track:Button):void {
			setSliderTrackStyles(track, wideControlSize, controlSize);
		}

		protected function setHorizontalSliderMaximumTrackStyles(track:Button):void {
			setSliderTrackStyles(track, wideControlSize, controlSize);
		}

		protected function setVerticalSliderMinimumTrackStyles(track:Button):void {
			setSliderTrackStyles(track, controlSize, wideControlSize);
		}

		protected function setVerticalSliderMaximumTrackStyles(track:Button):void {
			setSliderTrackStyles(track, controlSize, wideControlSize);
		}

		protected function setSliderTrackStyles(track:Button, width:int, height:int):void {
			var skin:ImageSkin = new ImageSkin();

			// TODO: Refactor. @see http://forum.starling-framework.org/topic/how-to-overlay-textures-using-imageskin-with-togglebutton
			var defaultSkinName:String, disabledSkinName:String;
			if (_colorSchemeName == 'solarized-dark') {
				defaultSkinName = _BTN_BASE02_SEL_SKIN;
				disabledSkinName = _BTN_BASE01_SKIN;
			}
			else if (_colorSchemeName == 'solarized-light') {
				defaultSkinName = _BTN_BASE2_SEL_SKIN;
				disabledSkinName = _BTN_BASE1_SKIN;
			}
			else {
				defaultSkinName = disabledSkinName = BTN_WHITE_SKIN; // TODO: Support custom color scheme.
			}
			skin.defaultTexture = getTexture(defaultSkinName);
			skin.disabledTexture = getTexture(disabledSkinName);
			skin.width = width;
			skin.height = height;
			track.defaultSkin = skin;
			track.hasLabelTextRenderer = false;
		}
	}
}
