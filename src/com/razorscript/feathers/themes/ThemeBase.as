package com.razorscript.feathers.themes {
	import com.razorscript.debug.Logger;
	import com.razorscript.events.RazorEventType;
	import com.razorscript.utils.ObjUtil;
	import com.razorscript.utils.TimerUtil;
	import feathers.layout.HorizontalAlign;
	import feathers.layout.VerticalAlign;
	import feathers.system.DeviceCapabilities;
	import feathers.themes.StyleNameFunctionTheme;
	import flash.geom.Rectangle;
	import flash.text.engine.FontDescription;
	import flash.text.engine.FontPosture;
	import flash.text.engine.FontWeight;
	import starling.core.Starling;
	import starling.display.Image;
	import starling.text.TextFormat;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;

	// TODO: Add a function to re-apply styles to all controls (call when validating theme styles).
	// TODO: Create a color manager for easily switching colors in theme styles.
	// TODO: Create a better texture manager to avoid unnecessary code.

	/**
	 * Base class for themes.
	 *
	 * @author Gene Pavlovsky
	 */

	public class ThemeBase extends StyleNameFunctionTheme implements ITheme {
		/**
		 * Constructor.
		 *
		 * @param styleName Style name.
		 * @param colorSchemeName Color scheme name.
		 * @param scaleToDPI Whether the theme's skins should be scaled based on the screen density and content scale factor.
		 * @param designDPI The screen density that the textures used by this theme are designed for.
		 */
		public function ThemeBase(styleName:String = null, colorSchemeName:String = null, scaleToDPI:Boolean = true, designDPI:int = 0) {
			super();
			_styleName = styleName;
			_colorSchemeName = colorSchemeName;
			invalidationFlags = INVALIDATION_FLAG_STYLE | INVALIDATION_FLAG_COLOR_SCHEME;
			_scaleToDPI = scaleToDPI;
			_designDPI = designDPI;
		}

		protected var _styleName:String;

		public function get styleName():String {
			return _styleName;
		}

		public function set styleName(value:String):void {
			if (_styleName == value)
				return;
			_styleName = value;
			invalidate(INVALIDATION_FLAG_STYLE);
		}

		protected var _colorSchemeName:String;

		public function get colorSchemeName():String {
			return _colorSchemeName;
		}

		public function set colorSchemeName(value:String):void {
			if (_colorSchemeName == value)
				return;
			_colorSchemeName = value;
			invalidate(INVALIDATION_FLAG_COLOR_SCHEME);
		}

		public static const INVALIDATION_FLAG_STYLE:uint = 1 << uidIF++;
		public static const INVALIDATION_FLAG_COLOR_SCHEME:uint = 1 << uidIF++;

		protected static var uidIF:int = 0;

		public function invalidate(flag:uint):void {
			invalidationFlags |= flag;
			if (!validateDelayedCall)
				validateDelayedCall = TimerUtil.delayCall(validate, 20);
		}

		protected var invalidationFlags:uint;

		public function validate():void {
			TimerUtil.remove(validateDelayedCall);
			validateDelayedCall = null;

			var isStyleInvalid:Boolean = invalidationFlags & INVALIDATION_FLAG_STYLE;
			var isColorSchemeInvalid:Boolean = invalidationFlags & INVALIDATION_FLAG_COLOR_SCHEME;
			var isThemeInvalid:Boolean = invalidationFlags;
			invalidationFlags = 0;

			if (isStyleInvalid)
				validateStyle();
			if (isColorSchemeInvalid)
				validateColorScheme();

			if (!invalidationFlags && _isLoaded) {
				if (isThemeInvalid)
					validateTheme();
				dispatchEventWith(RazorEventType.VALIDATE);
			}
		}

		protected var validateDelayedCall:Object;

		protected function validateStyle():void {
		}

		protected function validateColorScheme():void {
		}

		protected function validateTheme():void {
		}

		protected var _isLoaded:Boolean;

		public function get isLoaded():Boolean {
			return _isLoaded;
		}

		/**
		 * Loads the theme. Subclasses are expected to load the assets, call initialize(), then dispatch RazorEventType.LOAD.
		 */
		public function load():void {
			throw new Error('Illegal abstract method call.');
		}

		/** The screen density that the textures used by this theme are designed for. */
		protected var _designDPI:int;

		/** The original screen density used for scaling. */
		protected var _originalDPI:int;

		/** Whether the theme's skins should be scaled based on the screen density and content scale factor. */
		protected var _scaleToDPI:Boolean;

		protected var _scale:Number = 1;
		/**
		 * Skins are scaled by this value, based on the screen density and content scale factor.
		 */
		public function get scale():Number {
			return _scale;
		}

		/**
		 * Multiplies a number by scale and rounds the result to the nearest integer.
		 *
		 * @param size A number.
		 * @return The specified number, multiplied by scale and rounded to the nearest integer.
		 */
		public function scaleValue(size:Number):int {
			return Math.round(size * _scale); // TODO: Is it preferable to have non-rounded scaled value in some cases?
		}

		/**
		 * StageText is scaled by this value, based on the screen density and content scale factor.
		 */
		protected var stageTextScale:Number = 1;

		/**
		 * Initializes the theme. Expected to be called by subclasses after the assets have been loaded and the skin texture atlas has been created.
		 */
		protected function initialize():void {
			validate();
			initScale();
		}

		/**
		 * Initializes the scale value based on the screen density and content scale factor.
		 */
		protected function initScale():void {
			if (_originalDPI != 0) return;

			var starling:Starling = Starling.current;
			var nativeScaleFactor:Number = starling.supportHighResolutions ? starling.nativeStage.contentsScaleFactor : 1;
			var scaledDPI:int = DeviceCapabilities.dpi / (starling.contentScaleFactor / nativeScaleFactor);
			_originalDPI = (_scaleToDPI && _designDPI) ? _designDPI : scaledDPI;
			_scale = scaledDPI / _originalDPI;
			stageTextScale = _scale / nativeScaleFactor;
		}

		/**
		 * Disposes the texture atlas before calling super.dispose().
		 */
		override public function dispose():void {
			if (atlas) {
				atlas.dispose();
				atlas = null;
			}
			super.dispose();
		}

		/**
		 * The texture atlas that contains skins for this theme. Subclasses are expected to load the assets and set this property before calling initialize().
		 */
		protected var atlas:TextureAtlas;

		/**
		 * Stores atlas textures metadata.
		 */
		protected var textureMetadata:Object;

		protected function parseTextureMetadataJSON(text:String):void {
			textureMetadata = JSON.parse(text);
			for (var name:String in textureMetadata) {
				var metadata:Object = textureMetadata[name];
				if (metadata is String)
					textureMetadata[name] = _resolveRef(metadata as String, textureMetadata);
				else
					_parseTextureMetadata(metadata);
			}
		}

		CONFIG::release
		private function _resolveRef(ref:String, obj:Object):Object {
			return (obj[ref] is String) ? (obj[ref] = _resolveRef(obj[ref], obj)) : obj[ref];
		}

		CONFIG::debug
		private function _resolveRef(ref:String, obj:Object):Object {
			var value:* = obj[ref];
			if (value == null)
				Logger.error(this, 'Texture metadata reference "' + ref + '" doesn\'t exist.');
			return (value is String) ? (obj[ref] = _resolveRef(value, obj)) : value;
		}

		private function _parseTextureMetadata(metadata:Object):void {
			if ('scale9Grid' in metadata) {
				var xywh:Array = metadata.scale9Grid;
				metadata.scale9Grid = new Rectangle(xywh[0] * _scale, xywh[1] * _scale, xywh[2] * _scale, xywh[3] * _scale);
			}
			if ('pivotX' in metadata)
				metadata.pivotX = metadata.pivotX * _scale;
			if ('pivotY' in metadata)
				metadata.pivotY = metadata.pivotY * _scale;
		}

		/**
		 * Stores textures.
		 */
		private const textures:Object = {};

		protected function addTextureAliases(name:String, ... aliases):void {
			for each (var alias:String in aliases)
				textures[alias] = textures[name];
		}

		protected function createTextures(names:Array):void {
			for each (var name:String in names) {
				var texture:Texture = textures[name] = atlas.getTexture(name);
				var metadata:Object = textureMetadata[name];
				if (metadata) {
					if (('scale9Grid' in metadata) && ('pivotX' in metadata) && ('pivotY' in metadata))
						_automateSetupForTexture(texture, metadata.scale9Grid, metadata.pivotX, metadata.pivotY);
					else if ('scale9Grid' in metadata)
						Image.bindScale9GridToTexture(texture, metadata.scale9Grid);
					else if (('pivotX' in metadata) && ('pivotY' in metadata))
						Image.bindPivotPointToTexture(texture, metadata.pivotX, metadata.pivotY);
				}
			}
		}

		private function _automateSetupForTexture(texture:Texture, scale9Grid:Rectangle, pivotX:Number, pivotY:Number):void {
			Image.automateSetupForTexture(texture,
				function (image:Image):void {
					image.scale9Grid = scale9Grid;
					image.pivotX = pivotX;
					image.pivotY = pivotY;
				},
				function (image:Image):void {
					image.scale9Grid = null;
					image.pivotX = image.pivotY = 0;
				});
		}

		CONFIG::release
		[Inline]
		protected final function getTexture(name:String):* {
			return textures[name];
		}

		CONFIG::debug
		protected final function getTexture(name:String):* {
			var texture:* = textures[name];
			if (texture == null)
				Logger.error(this, 'Texture "' + name + '" doesn\'t exist.');
			return texture;
		}

		/**
		 * The FTE FontDescription used for text of normal style and normal weight. Used by default if a font description is not specified.
		 */
		protected var regularFontDescription:FontDescription;

		/**
		 * Stores cached TextFormat objects.
		 */
		private const textFormatCache:Object = {};

		/**
		 * Returns a TextFormat with the specified font size, color and font description (if null, regularFontDescription is used). If possible, reuses a previously created object.
		 *
		 * @param fontSize A font size.
		 * @param fontColor A font color (name or numeric value).
		 * @param fontDescription A font description. If null, regularFontDescription is used.
		 * @return A TextFormat with the specified font size, color and font description.
		 */
		protected function getTextFormat(fontSize:int, fontColor:*, fontDescription:FontDescription = null):TextFormat {
			// TODO: Is it better to have floating-point fontSize, as in TextFormat?
			if (!(fontColor is uint))
				fontColor = getColor(fontColor);
			if (!fontDescription)
				fontDescription = regularFontDescription;
			var key:String = fontDescription.fontName + fontSize + fontDescription.fontWeight + fontDescription.fontPosture + fontColor;
			return (key in textFormatCache) ? textFormatCache[key] : (textFormatCache[key] = _createTextFormat(fontDescription, fontSize, fontColor));
		}

		private function _createTextFormat(fontDescription:FontDescription, fontSize:int, fontColor:uint):TextFormat {
			var textFormat:TextFormat = new TextFormat(fontDescription.fontName, fontSize, fontColor, HorizontalAlign.LEFT, VerticalAlign.TOP);
			textFormat.bold = fontDescription.fontWeight == FontWeight.BOLD;
			textFormat.italic = fontDescription.fontPosture == FontPosture.ITALIC;
			return textFormat;
		}

		/**
		 * Stores style properties.
		 */
		private var _style:Object;

		protected function parseStyleJSON(text:String):void {
			_style = ObjUtil.deepCopy(JSON.parse(text), ObjUtil.deepClone(_baseStyle), false);
		}

		protected var _baseStyle:Object;

		protected function get baseStyle():Object {
			return _baseStyle;
		}

		protected function isStyleInitialized():Boolean {
			return _style != null;
		}

		CONFIG::release
		[Inline]
		protected final function getStyle(name:String):* {
			return _style[name];
		}

		CONFIG::debug
		protected final function getStyle(name:String):* {
			var style:* = _style[name];
			if (style == null)
				Logger.error(this, 'Style property "' + name + '" doesn\'t exist.');
			return style;
		}

		protected function getFontSize(name:String):int {
			return scaleValue(getStyle(name));
		}

		/**
		 * Stores color values.
		 */
		private var _colorScheme:Object;

		protected function isColorSchemeInitialized():Boolean {
			return _colorScheme != null;
		}

		protected function parseColorSchemeJSON(text:String):void {
			_colorScheme = JSON.parse(text);
			_parseColorValues(_colorScheme);
		}

		protected function _parseColorValues(obj:Object):void {
			for (var name:String in obj) {
				var value:* = obj[name];
				if (value is String)
					obj[name] = parseInt(value);
				else if (ObjUtil.isObject(value))
					_parseColorValues(value);
			}
		}

		CONFIG::release
		[Inline]
		protected final function getColor(name:String):* {
			return _colorScheme[name];
		}

		CONFIG::debug
		protected final function getColor(name:String):* {
			var color:* = _colorScheme[name];
			if (color == null)
				Logger.error(this, 'Color "' + name + '" doesn\'t exist.');
			return color;
		}
	}
}
