package com.razorscript.feathers.themes {
	import com.razorscript.events.RazorEventType;
	import starling.events.Event;
	import starling.events.EventDispatcher;
	
	/**
	 * Stores the active theme and dispatches it's events.
	 *
	 * @author Gene Pavlovsky
	 */
	public class ThemeManager extends EventDispatcher {
		protected static var _instance:ThemeManager;
		
		public static function get instance():ThemeManager {
			return _instance ? _instance : (_instance = new ThemeManager());
		}
		
		public static function set instance(value:ThemeManager):void {
			_instance = value;
		}
		
		public function ThemeManager() {
		}
		
		/***********************************************************************************************************************************************/
		
		protected var _theme:ITheme;
		
		public function get theme():ITheme {
			return _theme;
		}
		
		public function set theme(value:ITheme):void {
			if (_theme) {
				_theme.removeEventListener(RazorEventType.LOAD, onThemeEvent);
				_theme.removeEventListener(RazorEventType.VALIDATE, onThemeEvent);
				_theme.dispose();
			}
			
			_theme = value;
			_theme.addEventListener(RazorEventType.LOAD, onThemeEvent);
			_theme.addEventListener(RazorEventType.VALIDATE, onThemeEvent);
			_theme.load();
		}
		
		protected function onThemeEvent(e:Event):void {
			dispatchEvent(e);
		}
	}
}
