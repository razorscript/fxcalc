package com.razorscript.feathers.themes {
	
	/**
	 * Defines color value constants for the Solarized color scheme.
	 *
	 * @author Gene Pavlovsky
	 */
	public class SolarizedColors {
		// Base colors
		public static const BASE03:uint = 0x002b36;
		public static const BASE02:uint = 0x073642;
		public static const BASE01:uint = 0x586e75;
		public static const BASE00:uint = 0x657b83;
		public static const BASE0:uint =  0x839496;
		public static const BASE1:uint =  0x93a1a1;
		public static const BASE2:uint =  0xeee8d5;
		public static const BASE3:uint =  0xfdf6e3;
		
		// Accent colors (minimum brightness)
		public static const YELLOW_MIN:uint =   0x5c4500;
		public static const ORANGE_MIN:uint =   0x66260b;
		public static const RED_MIN:uint =      0x6e1817;
		public static const MAGENTA_MIN:uint =  0x6b1c42;
		public static const VIOLET_MIN:uint =   0x373963;
		public static const BLUE_MIN:uint =     0x134569;
		public static const CYAN_MIN:uint =     0x15524d;
		public static const GREEN_MIN:uint =    0x4d5900;
		
		// Accent colors (low brightness)
		public static const YELLOW_LOW:uint =   0x876500;
		public static const ORANGE_LOW:uint =   0x993a11;
		public static const RED_LOW:uint =      0xa32422;
		public static const MAGENTA_LOW:uint =  0x9e2962;
		public static const VIOLET_LOW:uint =   0x515594;
		public static const BLUE_LOW:uint =     0x1c669c;
		public static const CYAN_LOW:uint =     0x1f7870;
		public static const GREEN_LOW:uint =    0x637300;
		
		// Accent colors (default brightness)
		public static const YELLOW:uint =       0xb58900;
		public static const ORANGE:uint =       0xcb4b16;
		public static const RED:uint =          0xdc322f;
		public static const MAGENTA:uint =      0xd33682;
		public static const VIOLET:uint =       0x6c71c4;
		public static const BLUE:uint =         0x268bd2;
		public static const CYAN:uint =         0x2aa198;
		public static const GREEN:uint =        0x859900;
		
		// Accent colors (high brightness)
		public static const YELLOW_HIGH:uint =  0xdba400;
		public static const ORANGE_HIGH:uint =  0xe55719;
		public static const RED_HIGH:uint =     0xed3532;
		public static const MAGENTA_HIGH:uint = 0xeb3d91;
		public static const VIOLET_HIGH:uint =  0x7d82e3;
		public static const BLUE_HIGH:uint =    0x2a99e8;
		public static const CYAN_HIGH:uint =    0x36d1c4;
		public static const GREEN_HIGH:uint =   0xb1cc00;
		
		// Accent colors (maximum brightness)
		public static const YELLOW_MAX:uint =   0xffbf00;
		public static const ORANGE_MAX:uint =   0xff601c;
		public static const RED_MAX:uint =      0xff3936;
		public static const MAGENTA_MAX:uint =  0xff429d;
		public static const VIOLET_MAX:uint =   0x8c92ff;
		public static const BLUE_MAX:uint =     0x2ea8ff;
		public static const CYAN_MAX:uint =     0x42ffef;
		public static const GREEN_MAX:uint =    0xddff00;
	}
}
