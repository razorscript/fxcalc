package com.razorscript.feathers.themes {
	import com.razorscript.events.IEventDispatcher;
	
	/**
	 * A theme.
	 *
	 * @author Gene Pavlovsky
	 */
	public interface ITheme extends IEventDispatcher {
		function get isLoaded():Boolean;
		function get scale():Number;
		function get styleName():String;
		function set styleName(value:String):void;
		function get colorSchemeName():String;
		function set colorSchemeName(value:String):void;
		
		function load():void;
		function dispose():void;
	}
}
