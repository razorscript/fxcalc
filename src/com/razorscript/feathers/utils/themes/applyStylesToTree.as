package com.razorscript.feathers.utils.themes {
	import feathers.core.IFeathersControl;
	import feathers.skins.IStyleProvider;
	import starling.display.DisplayObjectContainer;
	
	// TODO: Consider a non-recursive implementation of tree traversal.
	
	public function applyStylesToTree(target:IFeathersControl):void {
		var tree:DisplayObjectContainer = target as DisplayObjectContainer;
		var length:int = tree.numChildren;
		for (var i:int = 0; i < length; ++i) {
			var child:IFeathersControl = tree.getChildAt(i) as IFeathersControl;
			if (child)
				applyStylesToTree(child);
		}
		
		// TODO: Reset properties applied by the previous style provider (which might not be set by the new style provider).
		var styleProvider:IStyleProvider = target.styleProvider;
		if (styleProvider)
			styleProvider.applyStyles(target);
	}
}
