package com.razorscript.app {
	
	/**
	 * Version number, as in Semantic Versioning.
	 * @see http://semver.org
	 *
	 * @author Gene Pavlovsky
	 */
	public class Version {
		/** The smallest representable version. */
		public static const MIN_VALUE:Version = new Version(0);
		/** The largest representable version. */
		public static const MAX_VALUE:Version = new Version(int.MAX_VALUE);
		
		/**
		 * A compare function for sorting arrays or vectors of versions (@see Vector#sort()).
		 * Orders items by version code, in ascending order.
		 */
		public static function compareAsc(a:Version, b:Version):int {
			return a._versionCode - b._versionCode;
		}
		
		/**
		 * A compare function for sorting arrays or vectors of versions (@see Vector#sort()).
		 * Orders items by version code, in descending order.
		 */
		public static function compareDesc(a:Version, b:Version):int {
			return b._versionCode - a._versionCode;
		}
		
		public function Version(... args) {
			if (args.length == 1) {
				var arg:* = args[0];
				if (arg is String)
					parseVersionNumber(arg);
				else if (arg is int)
					parseVersionCode(arg);
			}
			else {
				setTo.apply(this, args);
			}
		}
		
		protected var _major:int;
		
		public function get major():int {
			return _major;
		}
		
		protected var _minor:int;
		
		public function get minor():int {
			return _minor;
		}
		
		protected var _patch:int;
		
		public function get patch():int {
			return _patch;
		}
		
		protected var _build:int;
		
		public function get build():int {
			return _build;
		}
		
		protected var _versionCode:int;
		
		public function get versionCode():int {
			return _versionCode;
		}
		
		protected var _versionNumber:String;
		
		public function get versionNumber():String {
			return _versionNumber;
		}
		
		public function toString():String {
			return _versionNumber;
		}
		
		protected function setTo(major:int = 0, minor:int = 0, patch:int = 0, build:int = 0):Version {
			_major = major;
			_minor = minor;
			_patch = patch;
			_build = build;
			_versionCode = major * 1e6 + minor * 1e3 + patch;
			_versionNumber = major + '.' + minor + '.' + patch + (build ? ('.' + build) : '');
			return this;
		}
		
		protected function parseVersionNumber(versionNumber:String):void {
			setTo.apply(this, versionNumber.split('.'));
			_versionNumber = versionNumber;
		}
		
		protected function parseVersionCode(versionCode:int):void {
			var code:int = _versionCode = versionCode;
			_major = code / 1e6;
			code -= _major * 1e6;
			_minor = code / 1e3;
			_patch = code - _minor * 1e3;
			_build = 0;
			_versionNumber = major + '.' + minor + '.' + patch;
		}
	}
}
