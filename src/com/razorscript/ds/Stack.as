package com.razorscript.ds {
	import com.razorscript.razor_internal;
	import com.razorscript.utils.StrUtil;
	import flash.utils.IDataInput;
	import flash.utils.IDataOutput;
	import flash.utils.IExternalizable;

	use namespace razor_internal;

	/**
	 * Generic stack (LIFO) data structure.
	 * Elements are stored in a Deque.
	 *
	 * @author Gene Pavlovsky
	 */
	public class Stack implements IIterable, IExternalizable {
		/**
		 * Constructor.
		 *
		 * If elements are provided as arguments, they are pushed on the stack.
		 *
		 * @param ... args A value.
		 */
		public function Stack(... args) {
			deque = new Deque();
			if (args.length)
				deque.addLast.apply(null, args);
		}

		/**
		 * The number of elements in this stack.
		 */
		public function get length():int {
			return deque.length;
		}

		/**
		 * Returns true if this stack contains no elements.
		 *
		 * @return Boolean true if this stack contains no elements.
		 */
		public function get isEmpty():Boolean {
			return !deque.length;
		}

		/**
		 * Empties the stack.
		 */
		public function clear():void {
			deque.clear();
		}

		/**
		 * Returns true if this stack contains the specified element.
		 *
		 * @param value A value.
		 * @return Boolean true if this stack contains the specified element.
		 */
		public function contains(value:*):Boolean {
			return deque.contains(value);
		}

		/**
		 * Returns a new iterator over the elements in this stack.
		 *
		 * @return An iterator over the elements in this stack.
		 */
		public function get iterator():IIterator {
			return deque.reverseIterator;
		}

		/**
		 * Adds one or more values to the top of the stack.
		 *
		 * @param value A value.
		 * @return The new length of the stack.
		 */
		public function push(... args):int {
			return deque.addLast.apply(null, args);
		}

		/**
		 * Retrieves and removes the top element of the stack, or throws a RangeError if this stack is empty.
		 *
		 * @return The top element of the stack.
		 *
		 * @throws RangeError - if this stack is empty.
		 */
		public function pop():* {
			return deque.removeLast();
		}

		/**
		 * Retrieves and removes the top element of the stack, or returns null if this stack is empty.
		 *
		 * @return The top element of the stack.
		 */
		public function poll():* {
			return deque.pollLast();
		}

		/**
		 * Retrieves, but does not remove, the top element of the stack, or throws a RangeError if this stack is empty.
		 *
		 * @return The top element of the stack.
		 *
		 * @throws RangeError - if this stack is empty.
		 */
		public function top():* {
			return deque.getLast();
		}

		/**
		 * Retrieves, but does not remove, the top element of the stack, or returns null if this stack is empty.
		 *
		 * @return The top element of the stack.
		 */
		public function peek():* {
			return deque.peekLast();
		}

		/**
		 * Returns a shallow copy of this stack.
		 *
		 * @return A shallow copy of this stack.
		 */
		public function clone():Stack {
			var stack:Stack = new Stack();
			stack.deque.addLast.apply(null, deque.items);
			return stack;
		}

		public function toString():String {
			return '[Stack length=' + length + ' items=' + StrUtil.arrayToString(deque.items) + ']';
		}

		public function writeExternal(output:IDataOutput):void {
			output.writeObject(deque);
		}

		public function readExternal(input:IDataInput):void {
			deque = input.readObject();
		}

		protected var deque:Deque;
	}
}
