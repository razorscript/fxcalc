package com.razorscript.ds {

	/**
	 * Implements an iterator over a Vector.<*>'s elements.
	 *
	 * Choose an iterator corresponding to a Vector's base type.
	 * Internally, AS3 uses 4 low-level vector classes:
	 *		Vector.<Number> - use VectorNumberIterator.
	 *		Vector.<int> - use VectorIntIterator.
	 *		Vector.<uint> - use VectorUIntIterator.
	 *		Use VectorIterator for any other Vector (Vectors of non-numeric base types are all represented by the Vector.<*> class, supported by VectorIterator).
	 *
	 * The behavior of an iterator is unspecified if the underlying collection is modified while the iteration is in progress in any way other than by calling remove().
	 *
	 * @author Gene Pavlovsky
	 */
	public class VectorIterator extends IndexedIteratorBase {
		public static const FORWARD:int = IndexedIteratorBase.FORWARD;
		public static const REVERSE:int = IndexedIteratorBase.REVERSE;

		/**
		 * Constructor.
		 *
		 * Creates an iterator over the specified Vector.<*>'s elements.
		 * The value argument is untyped to avoid manual type casts, however a type mismatch can only be detected at run-time (throws ArgumentError in case of type mismatch).
		 * Default order of iteration is forward (from the first to the last element).
		 *
		 * @param value The vector to iterate over.
		 * @param direction The order of the iteration (VectorIterator.FORWARD or VectorIterator.REVERSE).
		 *
		 * @throws ArgumentError - if value is not a Vector.<*>.
		 */
		public function VectorIterator(value:*, direction:int = FORWARD) {
			super(direction);
			data = value as Vector.<*>;
			if (!data)
				throw new ArgumentError('The value argument (' + value + ') is not a Vector.<*>.');
			initialize(data.length);
		}

		/**
		 * Returns from the underlying collection the element at the specified index pos.
		 *
		 * @param pos Index of the element to get.
		 * @return The element at the specified index.
		 */
		override protected function getElementAt(pos:int):* {
			return data[pos];
		}

		/**
		 * Removes from the underlying collection the element at the specified index pos.
		 *
		 * @param pos Index of the element to remove.
		 */
		override protected function removeElementAt(pos:int):void {
			data.splice(pos, 1);
		}

		protected var data:Vector.<*>;
	}
}
