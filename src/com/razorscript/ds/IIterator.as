package com.razorscript.ds {

	/**
	 * An iterator over collections.
	 *
	 * @author Gene Pavlovsky
	 */
	public interface IIterator {
		/**
		 * Performs the specified function for each remaining element until all elements have been processed or the action throws an exception.
		 * Actions are performed in the order of iteration, if that order is specified.
		 *
		 * @param callback The function to run on each item in the collection, should accept one argument (item:*).
		 * @param thisObject An object to use as this for the function.
		 */
		function forEachRemaining(callback:Function, thisObject:* = null):void;

		/**
		 * Whether the iteration has more elements.
		 */
		function get hasNext():Boolean;

		/**
		 * Returns the next element in the iteration.
		 *
		 * @return The next element in the iteration.
		 *
		 * @throws RangeError - if the iteration has no more elements.
		 */
		function getNext():*;

		/**
		 * Removes from the underlying collection the last element returned by this iterator (optional operation).
		 * This method can be called only once per call to getNext().
		 * The behavior of an iterator is unspecified if the underlying collection is modified while the iteration is in progress in any way other than by calling this method.
		 *
		 * @throws Error - if the remove operation is not supported by this iterator.
		 * @throws Error - if the next method has not yet been called, or the remove method has already been called after the last call to the next method.
		 */
		function remove():void;
	}
}
