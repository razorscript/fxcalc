package com.razorscript.ds {
	import com.razorscript.razor_internal;
	import com.razorscript.utils.VecUtil;

	use namespace razor_internal;

	/**
	 * Implements an iterator over Object's properties.
	 * The behavior of an iterator is unspecified if the underlying collection is modified while the iteration is in progress in any way other than by calling remove().
	 *
	 * @author Gene Pavlovsky
	 */
	public class ObjectIterator implements IIterator {
		/**
		 * Constructor.
		 *
		 * Creates an iterator over the specified Object's properties.
		 * Default order of iteration is unspecified (same as for.in operator).
		 * If the sortNames option is specified, the properties are sorted by name according to the specified (optional) sort options and iterated in that order.
		 *
		 * @param value The object to iterate over.
		 * @param sortNames Whether to sort the properties by name.
		 * @param sortOptions String compare function or sort options (@see Vector#sort()).
		 */
		public function ObjectIterator(value:Object, sortNames:Boolean = false, sortOptions:* = null) {
			data = value;
			items = new Vector.<String>();
			for (var name:String in data)
				items.push(name);
			if (sortNames)
				items.sort(sortOptions ? sortOptions : 0);
			iterator = new VectorIterator(items);
		}

		/**
		 * Performs the specified function for each remaining element until all elements have been processed or the action throws an exception.
		 * Actions are performed in the order of iteration, if that order is specified.
		 *
		 * @param callback The function to run on each item in the collection, should accept one argument (item:*).
		 * @param thisObject An object to use as this for the function.
		 */
		public function forEachRemaining(callback:Function, thisObject:* = null):void {
			return iterator.forEachRemaining(callback, thisObject);
		}

		/**
		 * Returns true if the iteration has more elements.
		 *
		 * @return Boolean true if the iteration has more elements.
		 */
		public function get hasNext():Boolean {
			return iterator.hasNext;
		}

		/**
		 * Returns the next element in the iteration.
		 *
		 * @return The next element in the iteration.
		 *
		 * @throws RangeError - if the iteration has no more elements.
		 */
		public function getNext():* {
			return lastItem = iterator.getNext();
		}

		/**
		 * Removes from the underlying collection the last element returned by this iterator (optional operation).
		 * This method can be called only once per call to getNext().
		 * The behavior of an iterator is unspecified if the underlying collection is modified while the iteration is in progress in any way other than by calling this method.
		 *
		 * @throws Error - if the next method has not yet been called, or the remove method has already been called after the last call to the next method.
		 */
		public function remove():void {
			if (!iterator.isRemovable)
				throw new Error('No element to remove.');
			delete data[lastItem];
			iterator.isRemovable = false;
		}

		protected var data:Object;
		protected var items:Vector.<String>;
		protected var lastItem:String;
		protected var iterator:VectorIterator;
	}
}
