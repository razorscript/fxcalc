package com.razorscript.ds {

	/**
	 * Implements an iterator over a String's characters.
	 * Doesn't support the remove() operation since it would be highly inefficient, throws an Error if remove() is called.
	 *
	 * The behavior of an iterator is unspecified if the underlying collection is modified while the iteration is in progress in any way other than by calling remove().
	 *
	 * @author Gene Pavlovsky
	 */
	public class StringIterator extends IndexedIteratorBase {
		public static const FORWARD:int = IndexedIteratorBase.FORWARD;
		public static const REVERSE:int = IndexedIteratorBase.REVERSE;

		/**
		 * Constructor.
		 *
		 * Creates an iterator over the specified String's characters.
		 * Default order of iteration is forward (from the first to the last character).
		 *
		 * @param value The string to iterate over.
		 * @param direction The order of the iteration (StringIterator.FORWARD or StringIterator.REVERSE).
		 */
		public function StringIterator(value:String, direction:int = FORWARD) {
			super(direction);
			data = value;
			initialize(data.length);
		}

		/**
		 * @throws Error - remove operation is not supported by this iterator since it would be highly inefficient.
		 */
		override public function remove():void {
			throw new Error('Operation not supported.');
		}

		/**
		 * Returns from the underlying collection the element at the specified index pos.
		 *
		 * @param pos Index of the element to get.
		 * @return The element at the specified index.
		 */
		override protected function getElementAt(pos:int):* {
			return data.charAt(pos);
		}

		protected var data:String;
	}
}
