package com.razorscript.ds {
	import com.razorscript.razor_internal;
	import com.razorscript.utils.StrUtil;
	import flash.utils.IDataInput;
	import flash.utils.IDataOutput;
	import flash.utils.IExternalizable;

	use namespace razor_internal;

	/**
	 * Generic queue (FIFO) data structure.
	 * Elements are stored in a Deque.
	 *
	 * @author Gene Pavlovsky
	 */
	public class Queue implements IIterable, IExternalizable {
		/**
		 * Constructor.
		 *
		 * If elements are provided as arguments, they are added to the queue.
		 *
		 * @param ... args A value.
		 */
		public function Queue(... args) {
			deque = new Deque();
			if (args.length)
				deque.addLast.apply(null, args);
		}

		/**
		 * The number of elements in this queue.
		 */
		public function get length():int {
			return deque.length;
		}

		/**
		 * Returns true if this queue contains no elements.
		 *
		 * @return Boolean true if this queue contains no elements.
		 */
		public function get isEmpty():Boolean {
			return !deque.length;
		}

		/**
		 * Empties the queue.
		 */
		public function clear():void {
			deque.clear();
		}

		/**
		 * Returns true if this queue contains the specified element.
		 *
		 * @param value A value.
		 * @return Boolean true if this queue contains the specified element.
		 */
		public function contains(value:*):Boolean {
			return deque.contains(value);
		}

		/**
		 * Returns a new iterator over the elements in this queue.
		 *
		 * @return An iterator over the elements in this queue.
		 */
		public function get iterator():IIterator {
			return deque.iterator;
		}

		/**
		 * Adds one or more values to the end of the queue.
		 *
		 * @param value A value.
		 * @return The new length of the deque.
		 */
		public function add(... args):int {
			return deque.addLast.apply(null, args);
		}

		/**
		 * Retrieves and removes the head element of the queue, or throws a RangeError if this queue is empty.
		 *
		 * @return The head element of the queue.
		 *
		 * @throws RangeError - if this queue is empty.
		 */
		public function remove():* {
			return deque.removeFirst();
		}

		/**
		 * Retrieves and removes the head element of the queue, or returns null if this queue is empty.
		 *
		 * @return The head element of the queue.
		 */
		public function poll():* {
			return deque.pollFirst();
		}

		/**
		 * Retrieves, but does not remove, the head element of the queue, or throws a RangeError if this queue is empty.
		 *
		 * @return The head element of the queue.
		 *
		 * @throws RangeError - if this queue is empty.
		 */
		public function head():* {
			return deque.getFirst();
		}

		/**
		 * Retrieves, but does not remove, the head element of the queue, or returns null if this queue is empty.
		 *
		 * @return The head element of the queue.
		 */
		public function peek():* {
			return deque.peekFirst();
		}

		/**
		 * Returns a shallow copy of this queue.
		 *
		 * @return A shallow copy of this queue.
		 */
		public function clone():Queue {
			var queue:Queue = new Queue();
			queue.deque.addLast.apply(null, deque.items);
			return queue;
		}

		public function toString():String {
			return '[Queue length=' + length + ' items=' + StrUtil.arrayToString(deque.items) + ']';
		}

		public function writeExternal(output:IDataOutput):void {
			output.writeObject(deque);
		}

		public function readExternal(input:IDataInput):void {
			deque = input.readObject();
		}

		protected var deque:Deque;
	}
}
