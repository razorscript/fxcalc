package com.razorscript.ds {

	/**
	 * Implements an iterator over a BitVector's bits.
	 * Doesn't support the remove() operation, throws an Error if remove() is called.
	 *
	 * The behavior of an iterator is unspecified if the underlying collection is modified while the iteration is in progress in any way other than by calling remove().
	 *
	 * @author Gene Pavlovsky
	 */
	public class BitVectorIterator implements IIterator {
		public static const FORWARD:int = 1;
		public static const REVERSE:int = -1;

		/**
		 * Constructor.
		 *
		 * Creates an iterator over the specified BitVector's bits.
		 * Default order of iteration is forward (from the least significant to the most significant bit).
		 *
		 * @param value The bit vector to iterate over.
		 * @param direction The order of the iteration (BitVectorIterator.FORWARD or BitVectorIterator.REVERSE).
		 */
		public function BitVectorIterator(value:BitVector, direction:int = FORWARD) {
			if ((direction != FORWARD) && (direction != REVERSE))
				throw new RangeError('The direction argument must be either FORWARD or REVERSE');
			this.direction = direction;
			data = value;
			numBitsLeft = data.length;
			wordIndex = (direction == FORWARD) ? 0 : (data.numWords - 1);
			word = data.getWordAt(wordIndex);
			bitMask = (direction == FORWARD) ? FIRST_BIT_MASK : LAST_BIT_MASK;
		}

		/**
		 * Performs the specified function for each remaining element until all elements have been processed or the action throws an exception.
		 * Actions are performed in the order of iteration, if that order is specified.
		 *
		 * @param callback The function to run on each element in the collection, is invoked with one argument (the value of the element):
		 *		function callback(value:*):void;
		 * @param thisObject An object to use as this for the function.
		 */
		public function forEachRemaining(callback:Function, thisObject:* = null):void {
			if (!hasNext)
				return;
			var _numBitsLeft:int = numBitsLeft;
			var _wordIndex:uint = wordIndex;
			var _word:uint = word;
			var _bitMask:uint = bitMask;
			while (hasNext)
				callback(getNext());
			numBitsLeft = _numBitsLeft;
			wordIndex = _wordIndex;
			word = _word;
			bitMask = _bitMask;
		}

		/**
		 * Whether the iteration has more elements.
		 */
		[Inline]
		public final function get hasNext():Boolean {
			return numBitsLeft > 0;
		}

		/**
		 * Returns the next element in the iteration.
		 *
		 * @return The next element in the iteration.
		 *
		 * @throws RangeError - if the iteration has no more elements.
		 */
		public function getNext():* {
			if (!hasNext)
				throw new RangeError('No more elements.');
			var bit:int = (word & bitMask) ? 1 : 0;
			if (--numBitsLeft) {
				if (direction == FORWARD) {
					if (bitMask == LAST_BIT_MASK) {
						bitMask = FIRST_BIT_MASK;
						word = data.getWordAt(++wordIndex);
					}
					else {
						bitMask <<= 1;
					}
				}
				else {
					if (bitMask == FIRST_BIT_MASK) {
						bitMask = LAST_BIT_MASK;
						word = data.getWordAt(--wordIndex);
					}
					else {
						bitMask >>>= 1;
					}
				}
			}
			return bit;
		}

		/**
		 * @throws Error - remove operation is not supported by this iterator since it would be highly inefficient.
		 */
		public function remove():void {
			throw new Error('Operation not supported.');
		}

		protected static const LAST_BIT_MASK:uint = uint(1 << 31);
		protected static const FIRST_BIT_MASK:uint = 1;

		protected var data:BitVector;
		protected var direction:int;

		protected var numBitsLeft:int;

		/** Current word (word at index wordIndex). */
		protected var word:uint;
		/** Current word index. */
		protected var wordIndex:uint;
		/** Current bit mask. */
		protected var bitMask:uint;
	}
}
