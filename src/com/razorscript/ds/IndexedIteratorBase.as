package com.razorscript.ds {
	import com.razorscript.razor_internal;

	use namespace razor_internal;

	/**
	 * Abstract base class for iterators over dense indexed data structures, such as a dense Array, a Vector or a String.
	 * Subclasses must override getElementAt() and removeElementAt(), and call initialize() at the end of it's constructor.
	 *
	 * The behavior of an iterator is unspecified if the underlying collection is modified while the iteration is in progress in any way other than by calling remove().
	 *
	 * @author Gene Pavlovsky
	 */
	public class IndexedIteratorBase implements IIterator {
		public static const FORWARD:int = 1;
		public static const REVERSE:int = -1;

		/**
		 * Constructor.
		 *
		 * @param value The array to iterate over.
		 * @param direction The order of the iteration (ArrayIteratorBase.FORWARD or ArrayIteratorBase.REVERSE).
		 */
		public function IndexedIteratorBase(direction:int) {
			if ((direction != FORWARD) && (direction != REVERSE))
				throw new RangeError('The direction argument must be either FORWARD or REVERSE');
			this.direction = direction;
		}

		/**
		 * Performs the specified function for each remaining element until all elements have been processed or the action throws an exception.
		 * Actions are performed in the order of iteration, if that order is specified.
		 *
		 * @param callback The function to run on each element in the collection, is invoked with one argument (the value of the element):
		 *		function callback(value:*):void;
		 * @param thisObject An object to use as this for the function.
		 */
		public function forEachRemaining(callback:Function, thisObject:* = null):void {
			if (!hasNext)
				return;
			var i:int = index;
			if (direction == FORWARD) {
				for (; i < length; ++i)
					callback(getElementAt(i));
			}
			else {
				for (; i >= 0; --i)
					callback(getElementAt(i));
			}
		}

		/**
		 * Whether the iteration has more elements.
		 */
		[Inline]
		public final function get hasNext():Boolean {
			return (direction == FORWARD) ? (index < length) : (index >= 0);
		}

		/**
		 * Returns the next element in the iteration.
		 *
		 * @return The next element in the iteration.
		 *
		 * @throws RangeError - if the iteration has no more elements.
		 */
		public function getNext():* {
			if (!hasNext)
				throw new RangeError('No more elements.');
			var element:* = getElementAt(index);
			index += direction;
			isRemovable = true;
			return element;
		}

		/**
		 * Removes from the underlying collection the last element returned by this iterator (optional operation).
		 * This method can be called only once per call to getNext().
		 * The behavior of an iterator is unspecified if the underlying collection is modified while the iteration is in progress in any way other than by calling this method.
		 *
		 * @throws Error - if the remove operation is not supported by this iterator.
		 * @throws Error - if the next method has not yet been called, or the remove method has already been called after the last call to the next method.
		 */
		public function remove():void {
			if (!isRemovable)
				throw new Error('No element to remove.');
			removeElementAt(index - direction);
			--length;
			if (direction == FORWARD)
				--index;
			isRemovable = false;
		}

		/**
		 * Initializes the iteration.
		 * Subclasses must call this method at the end of it's constructor.
		 *
		 * @param length The number of elements in the iteration.
		 */
		protected function initialize(length:int):void {
			this.length = length;
			index = (direction == FORWARD) ? 0 : (length - 1);
		}

		/**
		 * Returns from the underlying collection the element at the specified index pos.
		 * Subclasses must override this method.
		 *
		 * @param pos Index of the element to get.
		 * @return The element at the specified index.
		 */
		protected function getElementAt(pos:int):* {
			throw new Error('Illegal abstract method call.');
		}

		/**
		 * Removes from the underlying collection the element at the specified index pos.
		 * Subclasses must override this method.
		 *
		 * @param pos Index of the element to remove.
		 */
		protected function removeElementAt(pos:int):void {
			throw new Error('Illegal abstract method call.');
		}

		protected var direction:int;
		protected var index:int;
		protected var length:int;

		razor_internal var isRemovable:Boolean;
	}
}
