package com.razorscript.ds {
	import com.razorscript.razor_internal;
	import com.razorscript.utils.StrUtil;
	import flash.utils.IDataInput;
	import flash.utils.IDataOutput;
	import flash.utils.IExternalizable;

	use namespace razor_internal;

	// TODO: Improve performance by using the items array as a circular array.

	/**
	 * Generic double-ended queue (deque) data structure.
	 * Elements are stored in an array.
	 *
	 * @author Gene Pavlovsky
	 */
	public class Deque implements IIterable, IExternalizable {
		/**
		 * Constructor.
		 *
		 * If elements are provided as arguments, they are added to the deque.
		 *
		 * @param ... args A value.
		 */
		public function Deque(... args) {
			items = [];
			if (args.length)
				addLast.apply(null, args);
		}

		protected var _length:int;
		/**
		 * The number of elements in this deque.
		 */
		public function get length():int {
			return _length;
		}

		/**
		 * Returns true if this deque contains no elements.
		 *
		 * @return Boolean true if this deque contains no elements.
		 */
		public function get isEmpty():Boolean {
			return !_length;
		}

		/**
		 * Empties the deque.
		 */
		public function clear():void {
			_length = items.length = 0;
		}

		/**
		 * Returns true if this deque contains the specified element.
		 *
		 * @param value A value.
		 * @return Boolean true if this deque contains the specified element.
		 */
		public function contains(value:*):Boolean {
			return items.indexOf(value) != -1;
		}

		/**
		 * Returns a new iterator over the elements in this deque.
		 *
		 * @return An iterator over the elements in this deque.
		 */
		public function get iterator():IIterator {
			return new ArrayIterator(items);
		}

		/**
		 * Returns a new iterator over the elements in this deque in reverse sequential order.
		 *
		 * @return An iterator over the elements in this deque in reverse sequential order.
		 */
		public function get reverseIterator():IIterator {
			return new ArrayIterator(items, ArrayIterator.REVERSE);
		}

		/**
		 * Adds one or more values at the front of the deque and returns the new length of the deque.
		 * Values are added one by one. The last argument will be at the front of the deque.
		 *
		 * @param value A value.
		 * @return The new length of the deque.
		 */
		public function addFirst(... args):int {
			_length += args.length;
			return items.unshift.apply(null, args.reverse());
		}

		/**
		 * Adds one or more values at the end the deque and returns the new length of the deque.
		 *
		 * @param value A value.
		 * @return The new length of the deque.
		 */
		public function addLast(... args):int {
			_length += args.length;
			return items.push.apply(null, args);
		}

		/**
		 * Retrieves and removes the first element of the deque, or throws a RangeError if this deque is empty.
		 *
		 * @return The first element of the deque.
		 *
		 * @throws RangeError - if this deque is empty.
		 */
		public function removeFirst():* {
			if (!_length)
				throw new RangeError('No more elements.');
			--_length;
			return items.shift();
		}

		/**
		 * Retrieves and removes the last element of the deque, or throws a RangeError if this deque is empty.
		 *
		 * @return The first element of the deque.
		 *
		 * @throws RangeError - if this deque is empty.
		 */
		public function removeLast():* {
			if (!_length)
				throw new RangeError('No more elements.');
			--_length;
			return items.pop();
		}

		/**
		 * Retrieves and removes the first element of the deque, or returns null if this deque is empty.
		 *
		 * @return The first element of the deque.
		 */
		public function pollFirst():* {
			if (_length) {
				--_length;
				return items.shift();
			}
			return null;
		}

		/**
		 * Retrieves and removes the last element of the deque, or returns null if this deque is empty.
		 *
		 * @return The first element of the deque.
		 */
		public function pollLast():* {
			if (_length) {
				--_length;
				return items.pop();
			}
			return null;
		}

		/**
		 * Retrieves, but does not remove, the first element of the deque, or throws a RangeError if this deque is empty.
		 *
		 * @return The first element of the deque.
		 *
		 * @throws RangeError - if this deque is empty.
		 */
		public function getFirst():* {
			if (!_length)
				throw new RangeError('No more elements.');
			return items[0];
		}

		/**
		 * Retrieves, but does not remove, the last element of the deque, or throws a RangeError if this deque is empty.
		 *
		 * @return The last element of the deque.
		 *
		 * @throws RangeError - if this deque is empty.
		 */
		public function getLast():* {
			if (!_length)
				throw new RangeError('No more elements.');
			return items[_length - 1];
		}

		/**
		 * Retrieves, but does not remove, the first element of the deque, or returns null if this deque is empty.
		 *
		 * @return The first element of the deque.
		 */
		public function peekFirst():* {
			return _length ? items[0] : null;
		}

		/**
		 * Retrieves, but does not remove, the last element of the deque, or returns null if this deque is empty.
		 *
		 * @return The last element of the deque.
		 */
		public function peekLast():* {
			return _length ? items[_length - 1] : null;
		}

		/**
		 * Returns a shallow copy of this deque.
		 *
		 * @return A shallow copy of this deque.
		 */
		public function clone():Deque {
			var deque:Deque = new Deque();
			deque.addLast.apply(null, items);
			return deque;
		}

		public function toString():String {
			return '[Deque length=' + _length + ' items=' + StrUtil.arrayToString(items) + ']';
		}

		public function writeExternal(output:IDataOutput):void {
			output.writeObject(items);
		}

		public function readExternal(input:IDataInput):void {
			items = input.readObject();
			_length = items.length;
		}

		razor_internal var items:Array;
	}
}
