package com.razorscript.ds {
	import com.razorscript.utils.StrUtil;
	import com.razorscript.utils.VecUtil;
	import flash.utils.IDataInput;
	import flash.utils.IDataOutput;
	import flash.utils.IExternalizable;

	/**
	 * Bit vector data structure.
	 * Bits are compactly stored in a Vector.<uint>.
	 * Performs runtime range checking (getBitAt, setBitAt, toggleBitAt, getWordAt, setWordAt).
	 *
	 * @author Gene Pavlovsky
	 */
	public class BitVector implements IIterable, IExternalizable {
		/**
		 * Constructor.
		 *
		 * If an int value is provided, creates a bit vector containing value bits (all the bits are set to 0).
		 * If a String, Array or a Vector.<int> value is provided, creates a bit vector containing the specified values.
		 *
		 * @param value Bit vector's length as an int, or bits to store in the bit vector as a String, an Array or a Vector.<int>.
		 */
		public function BitVector(value:* = null) {
			if (value is String)
				bitString = value;
			else if (value is Array)
				bitArray = value;
			else if (value is Vector.<int>)
				bitVector = value;
			else if (value is int)
				length = value;
		}

		protected var _numBits:int;
		/**
		 * The number of bits in the bit vector.
		 */
		public function get length():int {
			return _numBits;
		}

		public function set length(value:int):void {
			_numBits = value;
			highBitsMask = (value & 0x1f) ? (0xffffffff >>> (32 - (value & 0x1f))) : 0xffffffff;
			_numWords = value >> 5;
			if (value & 0x1f)
				++_numWords;
			if (words)
				words.length = _numWords;
			else if (!words)
				words = new Vector.<uint>(_numWords);
		}

		protected var _numWords:int;
		/**
		 * The number of 32-bit words in the bit vector.
		 */
		public function get numWords():int {
			return _numWords;
		}

		public function set numWords(value:int):void {
			length = value * 32;
		}

		/**
		 * The bit vector as a string of binary digits.
		 *
		 * @throws RangeError - if value contains invalid digits.
		 */
		public function get bitString():String {
			return getDigits(2);
		}

		public function set bitString(value:String):void {
			parseDigits(value, 2);
		}

		/**
		 * The bit vector as a string of hexadecimal digits.
		 *
		 * @throws RangeError - if value contains invalid digits.
		 */
		public function get hexString():String {
			return getDigits(16);
		}

		public function set hexString(value:String):void {
			parseDigits(value, 16);
		}

		/** The base argument must be either 2 (binary) or 16 (hexadecimal). */
		protected function getDigits(base:int):String {
			if (!_numWords)
				return '';
			const wordDigits:int = (base == 2) ? 32 : 8;
			var i:int = _numWords - 1;
			// Going from the most significant bits to the least significant bits in the words vector (right to left, little-endian order), append the digits.
			var str:String = StrUtil.lpad(uint(words[i] & highBitsMask).toString(base), '0', wordDigits);
			for (--i; i >= 0; --i)
				str += StrUtil.lpad(words[i].toString(base), '0', wordDigits);
			return str;
		}

		/** The base argument must be either 2 (binary) or 16 (hexadecimal). */
		protected function parseDigits(value:String, base:int):void {
			const wordDigits:int = (base == 2) ? 32 : 8;
			length = value.length;
			var i:int = 0, endIndex:int = _numBits;
			// Going from the least significant to most significant bits in the string, extract blocks of at most wordLength bits, and store them in the words vector from left to right (little-endian order).
			while (endIndex >= 0) {
				var bits:Number = parseInt(value.substring((endIndex > wordDigits) ? (endIndex - wordDigits) : 0, endIndex), base);
				if (bits != bits)
					throw new RangeError('Value (' + value + ') contains invalid digit(s) (only ' + ((base == 2) ? 'binary' : 'hexadecimal') + ' digits are allowed).');
				words[i] = bits;
				endIndex -= wordDigits;
				++i;
			}
		}

		/**
		 * The bit vector as an array of int values, each value representing one binary digit.
		 */
		public function get bitArray():Array/*int*/ {
			var arr:Array = new Array(_numBits);
			for (var i:int = 0, j:int = _numBits - 1; i < _numBits; ++i, --j)
				arr[j] = getBitAt(i);
			return arr;
		}

		public function set bitArray(value:Array/*int*/):void {
			length = value.length;
			for (var i:int = 0, j:int = _numBits - 1; i < _numBits; ++i, --j)
				setBitAt(i, value[j]);
		}

		/**
		 * The bit vector as a vector of int values, each value representing one binary digit.
		 */
		public function get bitVector():Vector.<int> {
			var vec:Vector.<int> = new Vector.<int>(_numBits);
			for (var i:int = 0, j:int = _numBits - 1; i < _numBits; ++i, --j)
				vec[j] = getBitAt(i);
			return vec;
		}

		public function set bitVector(value:Vector.<int>):void {
			length = value.length;
			for (var i:int = 0, j:int = _numBits - 1; i < _numBits; ++i, --j)
				setBitAt(i, value[j]);
		}

		/**
		 * Whether all the bits in this bit vector are zero. If the bit vector's length is 0, returns true.
		 */
		public function get isZero():Boolean {
			if (!_numWords)
				return true;
			for (var i:int = 0; i < _numWords - 1; ++i) {
				if (words[i])
					return false;
			}
			return words[i] & highBitsMask;
		}

		/**
		 * Returns the value of the word at the specified index, 0 being the index of the least significant word.
		 *
		 * @param index Index of the word to get (0 being the least significant word).
		 * @return The value of the word as an uint.
		 */
		public function getWordAt(index:uint):uint {
			return words[index];
		}

		/**
		 * Sets the value of the word at the specified index to the specified value, 0 being the index of the least significant word.
		 *
		 * @param index Index of the word to set (0 being the least significant word).
		 * @param value The value of the word as an int.
		 */
		public function setWordAt(index:uint, value:uint):void {
			words[index] = value;
		}

		/**
		 * Returns the value of the bit at the specified index, 0 being the index of the least significant bit.
		 *
		 * @param index Index of the bit to get (0 being the least significant bit).
		 * @return The value of the bit as an int (1 or 0).
		 *
		 * @throws RangeError - if index is out of range.
		 */
		public function getBitAt(index:uint):int	{
			if (index >= _numBits)
				throw new RangeError('The index argument (' + index + ') is outside the range [0, ' + (_numBits - 1) + '].');

			var wordIndex:uint = index >> 5;
			index &= 0x1f;
			return (words[wordIndex] & (1 << index)) ? 1 : 0;
		}

		/**
		 * Sets the value of the bit at the specified index to the specified value, 0 being the index of the least significant bit.
		 *
		 * @param index Index of the bit to set (0 being the least significant bit).
		 * @param value The value of the bit as an int.
		 *
		 * @throws RangeError - if index is out of range.
		 */
		public function setBitAt(index:uint, value:int):void	{
			if (index >= _numBits)
				throw new RangeError('The index argument (' + index + ') is outside the range [0, ' + (_numBits - 1) + '].');

			var wordIndex:uint = index >> 5;
			index &= 0x1f;
			if (value)
				words[wordIndex] |= 1 << index;
			else
				words[wordIndex] &= ~(1 << index);
		}

		/**
		 * Toggles the value of the bit at the specified index.
		 *
		 * @param index Index of the bit to toggle.
		 *
		 * @throws RangeError - if index is out of range.
		 */
		public function toggleBitAt(index:uint):int	{
			if (index >= _numBits)
				throw new RangeError('The index argument (' + index + ') is outside the range [0, ' + (_numBits - 1) + '].');

			var wordIndex:uint = index >> 5;
			index &= 0x1f;
			return words[wordIndex] ^= 1 << index;
		}

		/**
		 * Sets all the bits to 0.
		 */
		public function clearAllBits():void {
			for (var i:int = 0; i < _numWords; ++i)
				words[i] = 0;
		}

		/**
		 * Sets all the bits to 1.
		 */
		public function setAllBits():void {
			if (!_numWords)
				return;
			var i:int = _numWords - 1;
			words[i] = highBitsMask;
			for (--i; i >= 0; --i)
				words[i] = 0xffffffff;
		}

		/**
		 * Toggles all the bits.
		 */
		public function toggleAllBits():void {
			if (!_numWords)
				return;
			var i:int = _numWords - 1;
			words[i] ^= highBitsMask;
			for (--i; i >= 0; --i)
				words[i] ^= 0xffffffff;
		}

		/**
		 * Insert the bits from value into this bit vector, shifted in from the least significant end by the specified number of bits.
		 *
		 * A shift of zero or less means that none of the bits will be shifted in.
		 * A shift of one means that the high bit of value will be in the least significant bit of this bit vector.
		 * A shift of 32 means that all value's bits will be in the least significant word of this bit vector. Etc.
		 *
		 * If the replace argument is true, the corresponding bits in the words vector will be cleared before insertion.
		 *
		 * @param value An integer containing the bits to insert.
		 * @param shiftDepth The depth to shift the bits to.
		 * @param replace If true, the corresponding bits in this bit vector will be cleared before insertion.
		 */
		public function insertLowBits(value:uint, shiftDepth:int, replace:Boolean = false):void {
			if (shiftDepth <= 0)
				return;

			// Get the index of the low word.
			var wordIndex:uint = (shiftDepth >> 5) - 1;
			shiftDepth &= 0x1f;

			var word:uint;
			// If the index of the low word is within range, copy the low word bits.
			if (wordIndex < _numWords) {
				word = replace ? (words[wordIndex] & ~(0xffffffff << shiftDepth)) : words[wordIndex];
				words[wordIndex] = word | ((wordIndex == _numWords - 1) ? ((value << shiftDepth) & highBitsMask) : (value << shiftDepth));
			}

			++wordIndex;
			// If there are bits to insert into the high word, and the index of the high word is within range, copy the high word bits.
			if (shiftDepth && (wordIndex < _numWords)) {
				word = replace ? (words[wordIndex] & ~(0xffffffff >>> (32 - shiftDepth))) : words[wordIndex];
				words[wordIndex] = word | ((wordIndex == _numWords - 1) ? ((value >>> (32 - shiftDepth)) & highBitsMask) : (value >>> (32 - shiftDepth)));
			}
		}

		/**
		 * Insert the bits from value into this bit vector, shifted in from the most significant end by the specified number of bits.
		 *
		 * A shift of zero or less means that none of the bits will be shifted in.
		 * A shift of one means that the high bit of value will be in the most significant bit of this bit vector.
		 * A shift of 32 means that all value's bits will be in the most significant word of this bit vector.
		 *
		 * If the replace argument is true, the corresponding bits in this bit vector will be cleared before insertion.
		 *
		 * @param value An integer containing the bits to insert.
		 * @param shiftDepth The depth to shift the bits to.
		 * @param replace If true, the corresponding bits in the words vector will be cleared before insertion.
		 */
		public function insertHighBits(value:uint, shiftDepth:int, replace:Boolean = false):void {
			insertLowBits(value, _numBits + 32 - shiftDepth, replace);
		}

		/**
		 * Returns a copy of this bit vector.
		 *
		 * @return A copy of this bit vector.
		 */
		public function clone():BitVector {
			var bitVector:BitVector = new BitVector(_numBits);
			for (var i:int = 0; i < _numWords; ++i)
				bitVector.words[i] = words[i];
			return bitVector;
		}

		/**
		 * Returns a new iterator over the bits in this bit vector.
		 *
		 * @return An iterator over the bits in this bit vector.
		 */
		public function get iterator():IIterator {
			return new BitVectorIterator(this);
		}

		/**
		 * Returns a new iterator over the bits in this bit vector in reverse sequential order.
		 *
		 * @return An iterator over the bits in this bit vector in reverse sequential order.
		 */
		public function get reverseIterator():IIterator {
			return new BitVectorIterator(this, BitVectorIterator.REVERSE);
		}

		/**
		 * Returns a new iterator over the words in this bit vector.
		 *
		 * @return An iterator over the words in this bit vector.
		 */
		public function get wordIterator():IIterator {
			return new VectorUIntIterator(words);
		}

		/**
		 * Returns a new iterator over the words in this bit vector in reverse sequential order.
		 *
		 * @return An iterator over the words in this bit vector in reverse sequential order.
		 */
		public function get reverseWordIterator():IIterator {
			return new VectorUIntIterator(words, VectorUIntIterator.REVERSE);
		}

		public function toString(includeBits:Boolean = false):String {
			var str:String = '[BitVector length=' + length;
			if (includeBits)
				str += ' bits=' + VecUtil.joinGrouped(StrUtil.splitw(bitString, 8), 4, ',', '__');
			return str + ']';
		}

		public function writeExternal(output:IDataOutput):void {
			output.writeObject(words);
			output.writeInt(length);
		}

		public function readExternal(input:IDataInput):void {
			words = input.readObject();
			length = input.readInt();
		}

		/** Bit storage vector holding words in little-endian order (0 is the index of the least significant word). */
		protected var words:Vector.<uint>;
		/** Bit mask for the bits stored in the the last word. */
		protected var highBitsMask:uint;
	}
}
