package com.razorscript.ds {
	
	/**
	 * A collection supporting iteration.
	 *
	 * @author Gene Pavlovsky
	 */
	public interface IIterable {
		/**
		 * Returns a new iterator over the elements in this collection.
		 *
		 * @return An iterator over the elements in this collection.
		 */
		function get iterator():IIterator;
	}
}
