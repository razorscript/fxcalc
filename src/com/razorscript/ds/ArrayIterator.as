package com.razorscript.ds {

	/**
	 * Implements an iterator over a dense Array's elements. Sparse arrays are not supported.
	 *
	 * The behavior of an iterator is unspecified if the underlying collection is modified while the iteration is in progress in any way other than by calling remove().
	 *
	 * @author Gene Pavlovsky
	 */
	public class ArrayIterator extends IndexedIteratorBase {
		public static const FORWARD:int = IndexedIteratorBase.FORWARD;
		public static const REVERSE:int = IndexedIteratorBase.REVERSE;

		/**
		 * Constructor.
		 *
		 * Returns an iterator over the specified Array's elements.
		 * Default order of iteration is forward (from the first to the last element).
		 *
		 * @param value The array to iterate over.
		 * @param direction The order of the iteration (ArrayIterator.FORWARD or ArrayIterator.REVERSE).
		 */
		public function ArrayIterator(value:Array, direction:int = FORWARD) {
			super(direction);
			data = value;
			initialize(data.length);
		}

		/**
		 * Returns from the underlying collection the element at the specified index pos.
		 *
		 * @param pos Index of the element to get.
		 * @return The element at the specified index.
		 */
		override protected function getElementAt(pos:int):* {
			return data[pos];
		}

		/**
		 * Removes from the underlying collection the element at the specified index pos.
		 *
		 * @param pos Index of the element to remove.
		 */
		override protected function removeElementAt(pos:int):void {
			data.splice(pos, 1);
		}

		protected var data:Array;
	}
}
