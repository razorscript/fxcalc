package com.razorscript.bench.formatters {
	import com.razorscript.bench.results.BenchmarkResultEntry;
	import com.razorscript.bench.results.BenchmarkResults;
	
	/**
	 * Benchmark output format options.
	 *
	 * If a property is null (negative for numeric properties), it might be replaced from another format options instance by copyFrom().
	 * This allows a mechanism to fall back to a property from another set of options, if an instance's property is null.
	 * To explicitly set a property to an empty value, use the empty* static functions for the function properties, an empty string for string properties and a zero for numeric properties.
	 *
	 * @author Gene Pavlovsky
	 */
	public class BenchmarkFormatOptions {
		public static function emptyEntryFormatFun(entry:BenchmarkResultEntry):String {
			return '';
		}
		
		public static function emptyValueFormatFun(value:*):String {
			return '';
		}
		
		public static function toStringValueFormatFun(value:*):String {
			return value ? value.toString() : '';
		}
		
		public static function emptyResultsFormatFun(results:BenchmarkResults):String {
			return '';
		}
		
		public function BenchmarkFormatOptions(entryFormatFun:Function = null, valueFormatFun:Function = null, headerFactory:* = null, footerFactory:* = null, resultsFormatFun:Function = null, resultsGap:int = -1) {
			this.entryFormatFun = entryFormatFun;
			this.valueFormatFun = valueFormatFun;
			this.headerFactory = headerFactory;
			this.footerFactory = footerFactory;
			this.resultsFormatFun = resultsFormatFun;
			this.resultsGap = resultsGap;
		}
		
		public function clone():BenchmarkFormatOptions {
			return new BenchmarkFormatOptions(entryFormatFun, valueFormatFun, headerFactory, footerFactory, resultsFormatFun, resultsGap);
		}
		
		public function copyMissingFrom(formatOptions:BenchmarkFormatOptions):BenchmarkFormatOptions {
			if (!entryFormatFun)
				entryFormatFun = formatOptions.entryFormatFun;
			if (!valueFormatFun)
				valueFormatFun = formatOptions.valueFormatFun;
			if (headerFactory == null)
				headerFactory = formatOptions.headerFactory;
			if (footerFactory == null)
				footerFactory = formatOptions.footerFactory;
			if (!resultsFormatFun)
				resultsFormatFun = formatOptions.resultsFormatFun;
			if (resultsGap < 0)
				resultsGap = formatOptions.resultsGap;
			return this;
		}
		
		public function copyFrom(formatOptions:BenchmarkFormatOptions):BenchmarkFormatOptions {
			entryFormatFun = formatOptions.entryFormatFun;
			valueFormatFun = formatOptions.valueFormatFun;
			headerFactory = formatOptions.headerFactory;
			footerFactory = formatOptions.footerFactory;
			resultsFormatFun = formatOptions.resultsFormatFun;
			resultsGap = formatOptions.resultsGap;
			return this;
		}
		
		public function clear():BenchmarkFormatOptions {
			entryFormatFun = null;
			valueFormatFun = null;
			headerFactory = null;
			footerFactory = null;
			resultsFormatFun = null;
			resultsGap = -1;
			return this;
		}
		
		public var entryFormatFun:Function;
		public var valueFormatFun:Function;
		public var headerFactory:*;
		public var footerFactory:*;
		public var resultsFormatFun:Function;
		public var resultsGap:int;
	}
}
