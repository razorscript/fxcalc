package com.razorscript.bench.formatters {
	import com.razorscript.bench.formatters.BenchmarkFormatOptions;
	import com.razorscript.bench.results.BenchmarkResultEntry;
	import com.razorscript.bench.results.BenchmarkResults;
	import com.razorscript.utils.DateUtil;
	import com.razorscript.utils.UnitUtil;
	import com.razorscript.utils.StrUtil;
	
	/**
	 * Provides basic formatting for benchmark results.
	 *
	 * @author Gene Pavlovsky
	 */
	public class BenchmarkFormatter {
		public function BenchmarkFormatter(timeWidth:int = 6, wantCount:Boolean = true, wantsOPS:Boolean = true, wantValue:Boolean = true, wantDate:Boolean = true, wantStats:Boolean = true, headerPadding:int = 0, footerPadding:int = 0, resultsGap:int = 1) {
			_timeWidth = timeWidth;
			this.wantCount = wantCount;
			this.wantOPS = wantsOPS;
			this.wantValue = wantValue;
			this.wantDate = wantDate;
			this.wantStats = wantStats;
			this.headerPadding = headerPadding;
			this.footerPadding = footerPadding;
			this.resultsGap = resultsGap;
			initFormatOptions();
		}
		
		protected function initFormatOptions():void {
			_formatOptions = new BenchmarkFormatOptions(entryFormatFun, valueFormatFun, headerFactory, footerFactory, resultsFormatFun, resultsGap);
		}
		
		protected var _formatOptions:BenchmarkFormatOptions;
		
		public function get formatOptions():BenchmarkFormatOptions {
			return _formatOptions;
		}
		
		protected var _timeWidth:int;
		
		public function get timeWidth():int {
			return _timeWidth;
		}
		
		public function set timeWidth(value:int):void {
			_timeWidth = value;
		}
		
		public var wantCount:Boolean;
		public var wantOPS:Boolean;
		public var wantValue:Boolean;
		public var wantDate:Boolean;
		public var wantStats:Boolean;
		
		protected var _headerPadStr:String;
		
		public function get headerPadding():int {
			return _headerPadStr.length;
		}
		
		public function set headerPadding(value:int):void {
			_headerPadStr = StrUtil.repeat('\n', value);
		}
		
		protected var _footerPadStr:String;
		
		public function get footerPadding():int {
			return _footerPadStr.length;
		}
		
		public function set footerPadding(value:int):void {
			_footerPadStr = StrUtil.repeat('\n', value);
		}
		
		public var resultsGap:int;
		
		protected static const GAP:String = '   ';
		
		protected var lastEntry:BenchmarkResultEntry;
		
		public function entryFormatFun(entry:BenchmarkResultEntry):String {
			if (!entry.description) {
				lastEntry = null;
				return '';
			}
			
			var str:String = (lastEntry && (lastEntry.isCompound || (entry.isFirstSlice && lastEntry.isRegular))) ? '\n' : '';
			
			if (_timeWidth) {
				if (entry.count) {
					var timeStr:String = entry.elapsedTime.toString();
					str += ((timeStr.length <= _timeWidth) ? (StrUtil.lpad(timeStr, ' ', _timeWidth) + ' ms') : (StrUtil.lpad(Math.round(entry.elapsedTime / 1000).toFixed(0), ' ', _timeWidth) + '  s')) + GAP;
				}
				else {
					str += StrUtil.repeat(' ', _timeWidth + 3) + GAP;
				}
			}
			if (wantCount)
				str += ((entry.count >= 1000) ? UnitUtil.formatSI(entry.count, 'Op', 1, 5) : entry.count ? (StrUtil.lpad(entry.count.toString(), ' ', 5) + '  Op') : '         ') + GAP;
			if (wantOPS)
				str += (entry.count ? entry.elapsedTime ? UnitUtil.formatSI(1000 * (entry.count / entry.elapsedTime), 'Op/s', 1, 5) : '    ?  Op/s' : '           ') + GAP;
			
			if (entry.isFirstSlice)
				str = str.replace(/  $/, ' ▾') + entry.description;
			else if (entry.isSlice)
				str += '· ' + uint(entry.data / 10).toString() + ' %';
			else
				str += entry.description;
			
			if (wantValue && entry.value && (entry.isRegular || entry.isFirstSlice || entry.isCompound))
				str += ' = ' + entry.value;
			
			lastEntry = entry;
			return str;
		}
		
		public function valueFormatFun(value:*):String {
			return value ? StrUtil.valueToString(value) : '';
		}
		
		public function headerFactory(results:BenchmarkResults):String {
			// Have to include the padding here for BenchmarkRunner live tracing to work correctly.
			return (wantDate ? (formatDate() + ' ') : '') + results.name + _headerPadStr +
				((wantStats && (results.initTime >= 0)) ? ('\n' + formatTime(results, 'initialize()', results.initTime)) : '');
		}
		
		public function footerFactory(results:BenchmarkResults):String {
			// Have to include the padding here for BenchmarkRunner live tracing to work correctly.
			return _footerPadStr + (wantStats ? (
				formatTime(results, 'real time', results.realTime) + '\n' +
				formatTime(results, 'user time', results.userTime, results.totalCount) + '\n' +
				formatTime(results, 'sys  time', results.systemTime) + (results.realTime ? StrUtil.format(' ({:5.1p})', results.systemTime / results.realTime) : '')) : '');
		}
		
		public function resultsFormatFun(results:BenchmarkResults):String {
			var header:String = results.header;
			var footer:String = results.footer;
			return (header ? (header + '\n') : '') + results.formatEntries() + (footer ? ('\n' + footer) : '');
		}
		
		protected function formatDate():String {
			return DateUtil.format('[%F %T %z]', new Date());
		}
		
		protected function formatTime(results:BenchmarkResults, description:String, time:int, count:int = 0):String {
			return results.formatOptions.entryFormatFun(new BenchmarkResultEntry(description, time, count));
		}
	}
}
