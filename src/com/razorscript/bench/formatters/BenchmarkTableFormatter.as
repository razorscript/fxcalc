package com.razorscript.bench.formatters {
	import com.razorscript.bench.results.BenchmarkResults;
	import com.razorscript.utils.StrUtil;

	/**
	 * Provides table-like formatting for benchmark results.
	 *
	 * @author Gene Pavlovsky
	 */
	public class BenchmarkTableFormatter extends BenchmarkFormatter {
		/**
		 *
		 * @param timeWidth
		 * @param wantCount
		 * @param wantOPS
		 * @param wantValue
		 * @param wantDate
		 * @param wantStats
		 * @param titlePadding The number of empty lines to insert between the name line of the header and the table header line of the header.
		 * @param headerPadding The number of empty lines to insert after the table header line of the header.
		 * @param footerPadding
		 */
		public function BenchmarkTableFormatter(timeWidth:int = 6, wantCount:Boolean = true, wantOPS:Boolean = true, wantValue:Boolean = true, wantDate:Boolean = true, wantStats:Boolean = true, titlePadding:int = 1, headerPadding:int = 1, footerPadding:int = 1, resultsGap:int = 1) {
			super(timeWidth, wantCount, wantOPS, wantValue, wantDate, wantStats, headerPadding, footerPadding, resultsGap);
			this.titlePadding = titlePadding;
			updateTableHeader();
		}

		override public function set timeWidth(value:int):void {
			super.timeWidth = value;
			updateTableHeader();
		}

		protected var _titlePadStr:String;

		public function get titlePadding():int {
			return _titlePadStr.length;
		}

		public function set titlePadding(value:int):void {
			_titlePadStr = StrUtil.repeat('\n', value);
		}

		override public function headerFactory(results:BenchmarkResults):String {
			// Have to include the padding here for BenchmarkRunner live tracing to work correctly.
			return (wantDate ? (formatDate() + ' ') : '') + results.name + _titlePadStr + '\n' + _tableHeader + _headerPadStr +
				((wantStats && (results.initTime >= 0)) ? ('\n' + formatTime(results, 'initialize()', results.initTime)) : '');
		}

		protected static const HEADER_GAP:String = GAP;

		protected function updateTableHeader():void {
			/**
			 * |   Time       Count       Speed       Description
			 * |  2500 ms   328.0 kOp   131.2 kOp/s   parse(PI).eval() = 3.1415926535897931
			 */
			_tableHeader = StrUtil.repeat(' ', _timeWidth - 3) + 'Time  ' + HEADER_GAP + '  Count  ' + HEADER_GAP + '  Speed    ' + HEADER_GAP + 'Description';
		}

		protected var _tableHeader:String;
	}
}
