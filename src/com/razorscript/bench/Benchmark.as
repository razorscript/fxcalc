package com.razorscript.bench {
	import com.razorscript.bench.formatters.BenchmarkFormatOptions;
	import com.razorscript.bench.runners.BenchmarkRunner;
	import com.razorscript.razor_internal;
	import flash.utils.getQualifiedClassName;

	use namespace razor_internal;

	/**
	 * Base class for benchmarks.
	 * Can also be instantiated and filled with benchmarkable items.
	 *
	 * @author Gene Pavlovsky
	 */
	public class Benchmark implements IBenchmarkable {
		public function Benchmark(name:String = null, cases:Vector.<BenchmarkCase> = null) {
			if (name)
				_name = name;
			_cases = cases ? cases : new Vector.<BenchmarkCase>();
			_formatOptions = new BenchmarkFormatOptions();
		}

		protected static const CLASS_NAME:String = getQualifiedClassName(Benchmark);

		protected var _name:String;

		public function get name():String {
			if (_name)
				return _name;
			var className:String = getQualifiedClassName(this);
			if (className == CLASS_NAME)
				return '';
			var i:int = className.lastIndexOf('::');
			return (i != -1) ? className.substr(i + 2) : className;
		}

		protected var _cases:Vector.<BenchmarkCase>;

		public function get cases():Vector.<BenchmarkCase> {
			return _cases;
		}

		public function set cases(value:Vector.<BenchmarkCase>):void {
			_cases = value;
		}

		protected var _formatOptions:BenchmarkFormatOptions;
		/**
		 * The benchmark format options. Any unset options fall back to the benchmark runner format options.
		 * Used to set default formatting options for the whole benchmark.
		 */
		public function get formatOptions():BenchmarkFormatOptions {
			return _formatOptions;
		}

		public function set formatOptions(value:BenchmarkFormatOptions):void {
			_formatOptions = value;
		}

		protected var _itemDuration:int;
		/**
		 * The default item duration for items that don't specify a duration. If not zero, overrides the benchmark runner default item duration.
		 */
		public function get itemDuration():int {
			return _itemDuration;
		}

		public function set itemDuration(value:int):void {
			_itemDuration = value;
		}

		/**
		 * If the argument is a benchmark case, adds it to the cases list.
		 * If the argument is a benchmark case item, adds a new benchmark case with this item to the cases list.
		 *
		 * @param benchable A benchmark case or a benchmark case item.
		 */
		public function add(benchable:IBenchmarkable):void {
			if (benchable is BenchmarkCase)
				_cases.push(benchable as BenchmarkCase);
			else if (benchable is BenchmarkCaseItem)
				_cases.push(new BenchmarkCase(null, new <BenchmarkCaseItem>[benchable as BenchmarkCaseItem]));
			else
				throw new Error('The benchable argument is ' + (benchable ? 'not a BenchmarkCase or a BenchmarkCaseItem.' : 'null.'));
		}
	}
}
