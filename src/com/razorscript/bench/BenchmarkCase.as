package com.razorscript.bench {
	import com.razorscript.bench.formatters.BenchmarkFormatOptions;
	import com.razorscript.bench.runners.BenchmarkRunner;
	import flash.utils.getQualifiedClassName;

	/**
	 * Base class for benchmark cases.
	 * Can also be instantiated and filled with benchmark case items.
	 *
	 * @author Gene Pavlovsky
	 */
	public class BenchmarkCase implements IBenchmarkable {
		public function BenchmarkCase(name:String = null, items:Vector.<BenchmarkCaseItem> = null) {
			if (name)
				_name = name;
			_items = items ? items : new Vector.<BenchmarkCaseItem>();
			_formatOptions = new BenchmarkFormatOptions();
		}

		protected static const CLASS_NAME:String = getQualifiedClassName(BenchmarkCase);

		protected var _name:String;

		public function get name():String {
			if (_name)
				return _name;
			var className:String = getQualifiedClassName(this);
			return (className != CLASS_NAME) ? className : '';
		}

		public function set name(value:String):void {
			_name = value;
		}

		protected var _items:Vector.<BenchmarkCaseItem>;

		public function get items():Vector.<BenchmarkCaseItem> {
			return _items;
		}

		public function set items(value:Vector.<BenchmarkCaseItem>):void {
			_items = value;
		}

		protected var _formatOptions:BenchmarkFormatOptions;
		/**
		 * The benchmark case format options. Any unset options fall back to the benchmark format options.
		 * Useful if a particular case needs different formatting.
		 * Commonly used to set a custom value format function.
		 */
		public function get formatOptions():BenchmarkFormatOptions {
			return _formatOptions;
		}

		public function set formatOptions(value:BenchmarkFormatOptions):void {
			_formatOptions = value;
		}

		protected var _itemDuration:int;
		/**
		 * The default item duration for items that don't specify a duration. If not zero, overrides the benchmark default item duration.
		 */
		public function get itemDuration():int {
			return _itemDuration;
		}

		public function set itemDuration(value:int):void {
			_itemDuration = value;
		}

		public function addFun(description:String, fun:Function, ... args):void {
			var benchItem:BenchmarkCaseItem = new BenchmarkCaseItem(description, fun);
			benchItem.args = args;
			_items.push(benchItem);
		}

		public function addFunD(duration:int, description:String, fun:Function, ... args):void {
			var benchItem:BenchmarkCaseItem = new BenchmarkCaseItem(description, fun);
			benchItem.args = args;
			benchItem.duration = duration;
			_items.push(benchItem);
		}

		public function addFunC(count:int, description:String, fun:Function, ... args):void {
			var benchItem:BenchmarkCaseItem = new BenchmarkCaseItem(description, fun);
			benchItem.args = args;
			benchItem.count = count;
			_items.push(benchItem);
		}

		public function addFunV(valueFormatFun:Function, description:String, fun:Function, ... args):void {
			var benchItem:BenchmarkCaseItem = new BenchmarkCaseItem(description, fun);
			benchItem.args = args;
			benchItem.valueFormatFun = valueFormatFun;
			_items.push(benchItem);
		}

		public function addFunDV(duration:int, valueFormatFun:Function, description:String, fun:Function, ... args):void {
			var benchItem:BenchmarkCaseItem = new BenchmarkCaseItem(description, fun);
			benchItem.args = args;
			benchItem.duration = duration;
			benchItem.valueFormatFun = valueFormatFun;
			_items.push(benchItem);
		}

		public function addFunCV(count:int, valueFormatFun:Function, description:String, fun:Function, ... args):void {
			var benchItem:BenchmarkCaseItem = new BenchmarkCaseItem(description, fun);
			benchItem.args = args;
			benchItem.count = count;
			benchItem.valueFormatFun = valueFormatFun;
			_items.push(benchItem);
		}

		public function addComment(description:String):void {
			_items.push(new BenchmarkCaseItem(description, null));
		}

		/**
		 * Adds a benchmark case item to the items list.
		 *
		 * @param benchItem A benchmark case item.
		 */
		public function addItem(benchItem:BenchmarkCaseItem):void {
			_items.push(benchItem);
		}

		public function initialize():void {
		}

		public function dispose():void {
			_items.length = 0;
			_formatOptions = null;
		}
	}
}
