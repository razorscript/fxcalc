package com.razorscript.bench {
	
	/**
	 * A benchmarkable item. BenchRunner.run() accepts values of this type.
	 *
	 * @author Gene Pavlovsky
	 */
	public interface IBenchmarkable {
	}
}
