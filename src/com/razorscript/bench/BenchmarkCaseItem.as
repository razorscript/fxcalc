package com.razorscript.bench {
	
	/**
	 * Contains properties describing a benchmark case item.
	 *
	 * @author Gene Pavlovsky
	 */
	public class BenchmarkCaseItem implements IBenchmarkable {
		public function BenchmarkCaseItem(description:String, fun:Function, ... args) {
			this.description = description;
			this.fun = fun;
			this.args = args;
		}
		
		public var description:String;
		
		public var fun:Function;
		public var args:Array;
		
		/**
		 * The duration to run for. If both count and duration are set, count is used.
		 */
		public var duration:int;
		
		/**
		 * The number of iterations to run. If both count and duration are set, count is used.
		 */
		public var count:int;
		
		public var valueFormatFun:Function;
		
		public function toString():String {
			return '[BenchmarkCaseItem description="' + description + '" args=[' + args.join(', ') + '] duration=' + duration + ' count=' + count + ']';
		}
	}
}
