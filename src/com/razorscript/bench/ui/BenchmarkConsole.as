package com.razorscript.bench.ui {
	import com.razorscript.bench.events.BenchmarkEvent;
	import com.razorscript.bench.runners.BenchmarkRunner;
	import com.razorscript.debug.LogEntry;
	import com.razorscript.debug.Logger;
	import com.razorscript.debug.LogLevel;
	import com.razorscript.feathers.themes.SolarizedColors;
	import com.razorscript.math.MathExt;
	import com.razorscript.utils.ClassUtil;
	import com.razorscript.utils.Lambda;
	import com.razorscript.utils.StrUtil;
	import com.razorscript.utils.UnitUtil;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.system.System;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.utils.getTimer;
	
	/**
	 * A Benchmark UI with a text console display.
	 *
	 * @author Gene Pavlovsky
	 */
	
	[Event(name="init", type="flash.events.Event")]
	public class BenchmarkConsole extends Sprite {
		public function BenchmarkConsole() {
			setConsoleOptions();
			if (stage) {
				stage.scaleMode = StageScaleMode.NO_SCALE;
				stage.align = StageAlign.TOP_LEFT;
				stage.addEventListener(Event.RESIZE, onStageResize);
				init();
			}
			else {
				addEventListener(Event.ADDED_TO_STAGE, init);
			}
		}
		
		protected function onStageResize(e:Event):void {
			resizeConsoleUI();
		}
		
		protected function setConsoleOptions():void {
		}
		
		protected function init(e:Event = null):void {
			removeEventListener(Event.ADDED_TO_STAGE, init);
			initGlobals();
			createConsoleUI();
			initBenchRunner();
		}
		
		protected function initGlobals():void {
			Logger.severityLevels = LogLevel.upTo(LogLevel.WARN);
			CONFIG::debug {
				Logger.traceFun = public::trace;
			}
			CONFIG::release {
				Logger.logFun = Logger.noLogger;
				StrUtil.trace = StrUtil.noTrace;
			}
		}
		
		protected var isPausable:Boolean = true;
		
		protected var fontName:String = 'Consolas';
		protected var fontSize:Number = 14;
		protected var consoleTextColor:Number = SolarizedColors.BASE1;
		protected var statusTextColor:Number = SolarizedColors.CYAN;
		protected var togglePauseTextColor:Number = SolarizedColors.BLUE;
		
		protected var _consoleText:TextField;
		protected var _statusText:TextField;
		protected var _togglePauseText:TextField;
		
		protected function createConsoleUI():void {
			_consoleText = new TextField();
			_consoleText.defaultTextFormat = new TextFormat(fontName, fontSize, consoleTextColor);
			_consoleText.wordWrap = true;
			addChild(_consoleText);
			
			_statusText = new TextField();
			_statusText.defaultTextFormat = new TextFormat(fontName, fontSize, statusTextColor);
			addChild(_statusText);
			
			if (isPausable)
				createTogglePauseUI();
			
			resizeConsoleUI();
		}
		
		protected function createTogglePauseUI():void {
			_togglePauseText = new TextField();
			_togglePauseText.defaultTextFormat = new TextFormat(fontName, fontSize, togglePauseTextColor);
			_togglePauseText.border = true;
			_togglePauseText.borderColor = togglePauseTextColor;
			_togglePauseText.selectable = false;
			_togglePauseText.autoSize = TextFieldAutoSize.RIGHT;
			_togglePauseText.visible = false;
			_togglePauseText.addEventListener(MouseEvent.CLICK, Lambda.replace1(togglePause));
			addChild(_togglePauseText);
		}
		
		protected function updateTogglePauseUI():void {
			if (!_togglePauseText)
				return;
			var state:int = benchRunner.state;
			_togglePauseText.visible = isPausable && ((state == BenchmarkRunner.STATE_RUNNING) || (state == BenchmarkRunner.STATE_PAUSED));
			if (_togglePauseText.visible)
				_togglePauseText.text = (state == BenchmarkRunner.STATE_RUNNING) ? 'Pause' : 'Resume';
		}
		
		protected function resizeConsoleUI():void {
			if (!_consoleText)
				return;
			
			var textHeight:Number = _statusText.getLineMetrics(0).height;
			_consoleText.width = _statusText.width = stage.stageWidth;
			_consoleText.height = stage.stageHeight - textHeight - 5;
			_statusText.height = textHeight + 3;
			_statusText.y = stage.stageHeight - _statusText.height;
			
			if (_togglePauseText) {
				_togglePauseText.y = 2;
				_togglePauseText.x = stage.stageWidth - _togglePauseText.textWidth - 7;
			}
		}
		
		protected function trace(... args):void {
			public::trace.apply(null, args);
			var followTail:Boolean = _consoleText.scrollV == _consoleText.maxScrollV;
			_consoleText.appendText(args.join(' ') + '\n');
			if (followTail)
				_consoleText.scrollV = _consoleText.maxScrollV;
		}
		
		protected function clear():void {
			_consoleText.text = '';
		}
		
		protected function get status():String {
			return _statusText.text;
		}
		
		protected function set status(value:String):void {
			_statusText.text = value;
		}
		
		protected function initBenchRunner():void {
			createBenchRunner();
			setupBenchRunner();
			addBenchRunnerEventListeners();
			dispatchEvent(new Event(Event.INIT));
			run();
		}
		
		protected function createBenchRunner():void {
			benchRunner = new BenchmarkRunner();
		}
		
		protected function setupBenchRunner():void {
			benchRunner.anonymousCaseName = benchRunner.anonymousBenchmarkName = ClassUtil.getClassName(this);
			benchRunner.asyncMode = BenchmarkRunner.RUN_ASYNC;
			benchRunner.gcMode = BenchmarkRunner.GC_ALWAYS;
			benchRunner.traceFun = trace;
		}
		
		protected function addBenchRunnerEventListeners():void {
			var updateUIEventFun:Function = function (e:Event):void {
				updateTogglePauseUI();
				updateBenchStatus();
			}
			var updateStatusEventFun:Function = Lambda.replace1(updateBenchStatus);
			benchRunner.addEventListener(BenchmarkEvent.START, onBenchStart);
			benchRunner.addEventListener(BenchmarkEvent.START, updateUIEventFun);
			benchRunner.addEventListener(BenchmarkEvent.STOP, updateUIEventFun);
			benchRunner.addEventListener(BenchmarkEvent.PAUSE, updateUIEventFun);
			benchRunner.addEventListener(BenchmarkEvent.RESUME, updateUIEventFun);
			benchRunner.addEventListener(BenchmarkEvent.CASE_START, onBenchCaseStart);
			benchRunner.addEventListener(BenchmarkEvent.CASE_START, updateStatusEventFun);
			benchRunner.addEventListener(BenchmarkEvent.ITEM_START, updateStatusEventFun);
			benchRunner.addEventListener(BenchmarkEvent.ITEM_PROGRESS, updateStatusEventFun);
			benchRunner.addEventListener(BenchmarkEvent.COMPLETE, onBenchComplete);
			benchRunner.addEventListener(BenchmarkEvent.COMPLETE, updateUIEventFun);
		}
		
		/**
		 * Called after the benchmark console is fully initialized.
		 * Subclasses should override this function to use as an entry point, or listen to the Event.INIT event.
		 */
		protected function run():void {
		}
		
		protected function togglePause():void {
			if (benchRunner.state == BenchmarkRunner.STATE_RUNNING)
				benchRunner.pause();
			else if (benchRunner.state == BenchmarkRunner.STATE_PAUSED)
				benchRunner.resume();
		}
		
		protected const BR_STATE_NAMES:Array = createBenchRunnerStateNames();
		
		protected function createBenchRunnerStateNames():Array {
			var names:Array = [];
			names[BenchmarkRunner.STATE_IDLE] = 		'stopped';
			names[BenchmarkRunner.STATE_RUNNING] = 	'running';
			names[BenchmarkRunner.STATE_PAUSED] = 	' paused';
			names[BenchmarkRunner.STATE_COMPLETED] = 'completed';
			return names;
		}
		
		protected var _benchStartTime:int;
		protected var _benchCaseWidth:int;
		protected var _benchItemWidth:int;
		
		protected function updateBenchStatus():void {
			var state:int = benchRunner.state;
			var caseIndex:int = MathExt.clamp(benchRunner.caseIndex + 1, 0, benchRunner.caseCount);
			var itemIndex:int = MathExt.clamp(benchRunner.itemIndex + 1, 0, benchRunner.itemCount);
			_statusText.text = StrUtil.format('{0:6.1f} s [free {1}] [case {3:{4}} / {5}] ' + ((state != BenchmarkRunner.STATE_COMPLETED) ? '[{6:{7}} / {8}] [{11}] {9}' : '[{10} {11}]'),
				(currentTime - _benchStartTime) / 1000, UnitUtil.formatBinary(System.freeMemory, 'B', 1, 6), UnitUtil.formatBinary(System.totalMemory, 'B', 1, 6),
				caseIndex, _benchCaseWidth, benchRunner.caseCount, itemIndex, _benchItemWidth, benchRunner.itemCount, benchRunner.caseName, benchRunner.benchmarkName, BR_STATE_NAMES[state]);
		}
		
		protected function onBenchStart(e:BenchmarkEvent):void {
			_benchStartTime = currentTime;
			_benchCaseWidth = benchRunner.caseCount.toString().length;
		}
		
		protected function onBenchCaseStart(e:BenchmarkEvent):void {
			_benchItemWidth = Math.max(_benchItemWidth, benchRunner.itemCount.toString().length);
		}
		
		protected var _benchErrorCount:int;
		
		protected function onBenchComplete(e:BenchmarkEvent):void {
			var errorLogs:Vector.<LogEntry> = Logger.filterLogs(LogLevel.upTo(LogLevel.WARN));
			if ((_benchErrorCount = errorLogs.length) > 0)
				trace('\n\n\nError log:\n\n' + Logger.formatLogs(errorLogs).join('\n'));
		}
		
		[Inline]
		protected final function get currentTime():int {
			return getTimer();
		}
		
		protected var benchRunner:BenchmarkRunner;
	}
}
