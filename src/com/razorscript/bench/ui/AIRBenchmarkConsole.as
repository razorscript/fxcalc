package com.razorscript.bench.ui {
	import com.razorscript.bench.events.BenchmarkEvent;
	import com.razorscript.bench.runners.AIRBenchmarkRunner;
	import flash.desktop.NativeApplication;
	import flash.display.NativeWindow;
	import flash.events.Event;
	import flash.events.InvokeEvent;
	import flash.events.NativeWindowBoundsEvent;
	import flash.events.NativeWindowDisplayStateEvent;
	import flash.filesystem.File;
	
	/**
	 * An extension of BenchmarkConsole that saves each benchmark case's formatted results to a separate file.
	 * The results directory is the first non-option command-line argument.
	 * Supports unattended operation (using the "--unattend" command-line argument).
	 *
	 * @author Gene Pavlovsky
	 */
	public class AIRBenchmarkConsole extends BenchmarkConsole {
		public function AIRBenchmarkConsole() {
			super();
		}
		
		protected var maximizeWindow:Boolean = true;
		
		override protected function init(e:Event = null):void {
			nativeApp = NativeApplication.nativeApplication;
			if (maximizeWindow)
				nativeApp.addEventListener(Event.ACTIVATE, onAppActivate);
			else
				nativeApp.addEventListener(InvokeEvent.INVOKE, onAppInvoke);
		}
		
		protected function onAppActivate(e:Event):void {
			nativeApp.removeEventListener(Event.ACTIVATE, onAppActivate);
			nativeWin = nativeApp.activeWindow;
			nativeWin.addEventListener(NativeWindowDisplayStateEvent.DISPLAY_STATE_CHANGE, onWinMaximize);
			nativeWin.maximize();
		}
		
		protected function onWinMaximize(e:Event):void {
			nativeWin.removeEventListener(NativeWindowDisplayStateEvent.DISPLAY_STATE_CHANGE, onWinMaximize);
			nativeApp.addEventListener(InvokeEvent.INVOKE, onAppInvoke);
		}
		
		protected function onAppInvoke(e:InvokeEvent):void {
			nativeApp.removeEventListener(InvokeEvent.INVOKE, onAppInvoke);
			parseOptions(e.arguments);
			super.init();
		}
		
		protected function parseOptions(options:Array):void {
			// TODO: Add extra options allowing to specify e.g. itemDuration, etc.
			for each (var option:String in options) {
				if (option == '--unattend') {
					unattendedMode = true;
				}
				else if (option.charAt(0) != '-') {
					resultsDir = option;
					break;
				}
			}
		}
		
		protected var unattendedMode:Boolean;
		protected var saveDebugResults:Boolean;
		protected var resultsDir:String = '../results';
		
		override protected function createBenchRunner():void {
			resultsDir = new File(File.applicationDirectory.nativePath + '/' + resultsDir).nativePath;
			CONFIG::debug {
				if (!saveDebugResults)
					resultsDir = null;
			}
			benchRunner = new AIRBenchmarkRunner(resultsDir, '.');
		}
		
		override protected function setupBenchRunner():void {
			super.setupBenchRunner();
			if (unattendedMode)
				benchRunner.traceFun = null;
		}
		
		override protected function onBenchComplete(e:BenchmarkEvent):void {
			super.onBenchComplete(e);
			// TODO: If benchmark is closed before finishing, return a non-zero exit code.
			if (unattendedMode && !_benchErrorCount)
				nativeApp.exit();
		}
		
		protected var nativeApp:NativeApplication;
		protected var nativeWin:NativeWindow;
	}
}
