package com.razorscript.bench {
	
	/**
	 * Benchmark utility functions.
	 * Useful for feeding benchmark case items straight to BenchmarkRunner.run(), e.g. for quick testing.
	 *
	 * @author Gene Pavlovsky
	 */
	public class BenchUtil {
		public static function fun(description:String, fun:Function, ... args):BenchmarkCaseItem {
			var benchItem:BenchmarkCaseItem = new BenchmarkCaseItem(description, fun);
			benchItem.args = args;
			return benchItem;
		}
		
		public static function funD(duration:int, description:String, fun:Function, ... args):BenchmarkCaseItem {
			var benchItem:BenchmarkCaseItem = new BenchmarkCaseItem(description, fun);
			benchItem.args = args;
			benchItem.duration = duration;
			return benchItem;
		}
		
		public static function funC(count:int, description:String, fun:Function, ... args):BenchmarkCaseItem {
			var benchItem:BenchmarkCaseItem = new BenchmarkCaseItem(description, fun);
			benchItem.args = args;
			benchItem.count = count;
			return benchItem;
		}
		
		public static function funV(valueFormatFun:Function, description:String, fun:Function, ... args):BenchmarkCaseItem {
			var benchItem:BenchmarkCaseItem = new BenchmarkCaseItem(description, fun);
			benchItem.args = args;
			benchItem.valueFormatFun = valueFormatFun;
			return benchItem;
		}
		
		public static function funDV(duration:int, valueFormatFun:Function, description:String, fun:Function, ... args):BenchmarkCaseItem {
			var benchItem:BenchmarkCaseItem = new BenchmarkCaseItem(description, fun);
			benchItem.args = args;
			benchItem.duration = duration;
			benchItem.valueFormatFun = valueFormatFun;
			return benchItem;
		}
		
		public static function funCV(count:int, valueFormatFun:Function, description:String, fun:Function, ... args):BenchmarkCaseItem {
			var benchItem:BenchmarkCaseItem = new BenchmarkCaseItem(description, fun);
			benchItem.args = args;
			benchItem.count = count;
			benchItem.valueFormatFun = valueFormatFun;
			return benchItem;
		}
	}
}
