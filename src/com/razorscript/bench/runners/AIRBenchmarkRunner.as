package com.razorscript.bench.runners {
	import com.razorscript.bench.formatters.BenchmarkFormatOptions;
	import com.razorscript.bench.runners.BenchmarkRunner;
	import com.razorscript.bench.runners.supportClasses.BenchmarkRunnerAsyncMode;
	import com.razorscript.bench.runners.supportClasses.BenchmarkRunnerGCMode;
	import com.razorscript.debug.Logger;
	import com.razorscript.utils.StrUtil;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	
	/**
	 * An extension of BenchmarkRunner that saves each benchmark case's formatted results to a separate file.
	 *
	 * @author Gene Pavlovsky
	 */
	public class AIRBenchmarkRunner extends BenchmarkRunner {
		public function AIRBenchmarkRunner(resultsDir:String, nameSep:String, asyncMode:BenchmarkRunnerAsyncMode = null, gcMode:BenchmarkRunnerGCMode = null, formatOptions:BenchmarkFormatOptions = null, traceFun:Function = null) {
			super(asyncMode, gcMode, formatOptions, traceFun);
			this.resultsDir = resultsDir;
			this.nameSep = nameSep;
		}
		
		public var resultsDir:String;
		public var nameSep:String;
		
		override protected function outputCaseResults():void {
			super.outputCaseResults();
			
			if (!resultsDir)
				return;
			var file:File = new File(resultsDir + '/' + _caseResults.name.replace(/(\.|::)/g, nameSep) + '.txt');
			try {
				var stream:FileStream = new FileStream();
				var prefix:String = '';
				if (file.nativePath in filePaths) {
					stream.open(file, FileMode.APPEND);
					prefix = StrUtil.repeat('\n', caseFormatOptions.resultsGap);
				}
				else {
					stream.open(file, FileMode.WRITE);
					filePaths[file.nativePath] = true;
					CONFIG::debug {
						prefix = 'This is a debug build. It is recommended to use a release build for benchmarking.\n\n';
					}
				}
				stream.writeUTFBytes((prefix + caseFormatOptions.resultsFormatFun(_caseResults) + '\n').replace(/\n/g, File.lineEnding));
				stream.close();
			}
			catch (err:Error) {
				Logger.error(this, 'Failed to write case results to "' + file.nativePath + '": ' + err.message);
			}
		}
		
		protected var filePaths:Object = {};
	}
}
