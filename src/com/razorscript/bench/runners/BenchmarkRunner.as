package com.razorscript.bench.runners {
	import com.razorscript.bench.Benchmark;
	import com.razorscript.bench.BenchmarkCase;
	import com.razorscript.bench.BenchmarkCaseItem;
	import com.razorscript.bench.IBenchmarkable;
	import com.razorscript.bench.events.BenchmarkEvent;
	import com.razorscript.bench.formatters.BenchmarkFormatOptions;
	import com.razorscript.bench.formatters.BenchmarkFormatter;
	import com.razorscript.bench.results.BenchmarkResultEntry;
	import com.razorscript.bench.results.BenchmarkResults;
	import com.razorscript.bench.runners.supportClasses.BenchmarkRunnerAsyncMode;
	import com.razorscript.bench.runners.supportClasses.BenchmarkRunnerGCMode;
	import com.razorscript.debug.Logger;
	import com.razorscript.utils.ClassUtil;
	import com.razorscript.utils.StrUtil;
	import com.razorscript.utils.TimerUtil;
	import flash.events.EventDispatcher;
	import flash.system.System;
	import flash.utils.getTimer;

	/**
	 * Runs a benchmark.
	 * Outputs the results using traceFun().
	 *
	 * @author Gene Pavlovsky
	 */

	[Event(name="start", type="com.razorscript.bench.events.BenchmarkEvent")]
	[Event(name="stop", type="com.razorscript.bench.events.BenchmarkEvent")]
	[Event(name="pause", type="com.razorscript.bench.events.BenchmarkEvent")]
	[Event(name="resume", type="com.razorscript.bench.events.BenchmarkEvent")]
	[Event(name="complete", type="com.razorscript.bench.events.BenchmarkEvent")]
	[Event(name="caseStart", type="com.razorscript.bench.events.BenchmarkEvent")]
	[Event(name="caseComplete", type="com.razorscript.bench.events.BenchmarkEvent")]
	[Event(name="itemStart", type="com.razorscript.bench.events.BenchmarkEvent")]
	[Event(name="itemProgress", type="com.razorscript.bench.events.BenchmarkEvent")]
	[Event(name="itemComplete", type="com.razorscript.bench.events.BenchmarkEvent")]
	public class BenchmarkRunner extends EventDispatcher {
		public static const RUN_SYNC:BenchmarkRunnerAsyncMode = BenchmarkRunnerAsyncMode.SYNC;
		public static const RUN_ASYNC:BenchmarkRunnerAsyncMode = BenchmarkRunnerAsyncMode.ASYNC;
		public static const RUN_AUTO:BenchmarkRunnerAsyncMode = BenchmarkRunnerAsyncMode.AUTO;

		public static const GC_ALWAYS:BenchmarkRunnerGCMode = BenchmarkRunnerGCMode.ALWAYS;
		public static const GC_IF_IMMINENT:BenchmarkRunnerGCMode = BenchmarkRunnerGCMode.IF_IMMINENT;
		public static const GC_SYSTEM:BenchmarkRunnerGCMode = BenchmarkRunnerGCMode.SYSTEM;

		public static const STATE_IDLE:int = 0;
		public static const STATE_RUNNING:int = 1;
		public static const STATE_PAUSED:int = 2;
		public static const STATE_COMPLETED:int = 3;

		public static const DEFAULT_ASYNC_TIME_SLICE:int = 1000;
		public static const DEFAULT_GC_IMMINENCE:Number = 0.5;

		public static const MIN_ASYNC_TIME_SLICE:int = 20;
		public static const MIN_ITEM_DURATION:int = 50;

		public function BenchmarkRunner(asyncMode:BenchmarkRunnerAsyncMode = null, gcMode:BenchmarkRunnerGCMode = null, formatOptions:BenchmarkFormatOptions = null, traceFun:Function = null) {
			this.asyncMode = asyncMode ? asyncMode : RUN_ASYNC;
			this.gcMode = gcMode ? gcMode : GC_IF_IMMINENT;
			this.formatOptions = formatOptions ? formatOptions : new BenchmarkFormatOptions();
			this.traceFun = traceFun ? traceFun : StrUtil.trace;
			_caseResults = new BenchmarkResults();
			_benchmarkResults = new BenchmarkResults();
		}

		/** The benchmark runner async mode. */
		public var asyncMode:BenchmarkRunnerAsyncMode;

		protected var _asyncTimeSlice:int = DEFAULT_ASYNC_TIME_SLICE;
		/** The ideal duration of synchronous (blocking) execution when running in async mode. Items with durations greater than this value have their duration subdivided into slices. */
		public function get asyncTimeSlice():int {
			return _asyncTimeSlice;
		}

		public function set asyncTimeSlice(value:int):void {
			_asyncTimeSlice = Math.max(MIN_ASYNC_TIME_SLICE, value);
			_runMinCycleTime = Math.max(RUN_MIN_CYCLE_MIN_TIME, RUN_MIN_CYCLE_TIMESLICE_RATIO * value);
		}

		/** If an item's duration is subdivided into slices, whether to add separate benchmark result entries for each slice (in addition to the aggregated item benchmark result entry). */
		public var reportTimeSlices:Boolean;

		/** The benchmark runner garbage collection mode. */
		public var gcMode:BenchmarkRunnerGCMode;

		/**
		 * The imminence value for GC_IF_IMMINENT garbage collection mode.
		 * @see flash.system.System#pauseForGCIfCollectionImminent()
		 */
		public var gcImminence:Number = DEFAULT_GC_IMMINENCE;

		/** The benchmark runner format options. Any unset options fall back to the either single-item or multi-item format options, depending on the number of case items in a benchmark. */
		public var formatOptions:BenchmarkFormatOptions;

		/** The function used to trace benchmark results. */
		public var traceFun:Function;

		protected var _itemDuration:int;
		/** The benchmark runner default item duration for items that don't specify a duration. */
		public function get itemDuration():int {
			return _itemDuration;
		}

		public function set itemDuration(value:int):void {
			_itemDuration = (value > 0) ? Math.max(MIN_ITEM_DURATION, value) : 0;
		}

		/** Whether to dispose benchmark cases and remove cases from  cases vector on completion. */
		public var disposeOnComplete:Boolean = true;

		/** The benchmark name used if a benchmark doesn't specify a name, e.g. the benchmark created by run() if the benchable argument is a benchmark case or a benchmark case item. */
		public var anonymousBenchmarkName:String = ClassUtil.getClassName(this);

		protected var _benchmarkName:String = '';
		/** The benchmark name. */
		public function get benchmarkName():String {
			return _benchmarkName;
		}

		protected var _isWrapperBenchmark:Boolean;
		/** Whether this benchmark was created by run() which happens when run() receives a benchmark case or a benchmark case item as the benchable argument. */
		public function get isWrapperBenchmark():Boolean {
			return _isWrapperBenchmark;
		}

		/** The benchmark async options. */
		protected var benchmarkAsyncMode:BenchmarkRunnerAsyncMode;

		/** The benchmark garbage collection options. */
		protected var benchmarkGCMode:BenchmarkRunnerGCMode;

		/** The benchmark format options. Any unset options fall back to the benchmark runner format options. */
		protected var benchmarkFormatOptions:BenchmarkFormatOptions;

		/** The benchmark default item duration for items that don't specify a duration. If not zero, overrides the benchmark runner default item duration. */
		protected var benchmarkItemDuration:int;

		protected var _multiItemFormatOptions:BenchmarkFormatOptions;
		/**
		 * The default format options for multi-item benchmarks.
		 */
		public function get multiItemFormatOptions():BenchmarkFormatOptions {
			if (!_multiItemFormatOptions) {
				var formatter:BenchmarkFormatter = new BenchmarkFormatter();
				formatter.wantStats = false;
				_multiItemFormatOptions = formatter.formatOptions;
				_multiItemFormatOptions.footerFactory = '';
			}
			return _multiItemFormatOptions;
		}

		public function set multiItemFormatOptions(value:BenchmarkFormatOptions):void {
			_multiItemFormatOptions = value;
		}

		protected var _singleItemFormatOptions:BenchmarkFormatOptions;
		/**
		 * The default format options for single-item benchmarks.
		 */
		public function get singleItemFormatOptions():BenchmarkFormatOptions {
			if (!_singleItemFormatOptions) {
				_singleItemFormatOptions = new BenchmarkFormatter().formatOptions;
				_singleItemFormatOptions.headerFactory = '';
				_singleItemFormatOptions.footerFactory = '';
				_singleItemFormatOptions.resultsGap = 0;
			}
			return _singleItemFormatOptions;
		}

		public function set singleItemFormatOptions(value:BenchmarkFormatOptions):void {
			_singleItemFormatOptions = value;
		}

		protected var caseQueue:Vector.<BenchmarkCase>;
		protected var benchCase:BenchmarkCase;

		/** The benchmark case format options. Any unset options fall back to the benchmark format options.*/
		protected var caseFormatOptions:BenchmarkFormatOptions;

		/** The benchmark case name used if a benchmark case doesn't specify a name, e.g. the benchmark case created by run() if the benchable argument is a benchmark case item. */
		public var anonymousCaseName:String = anonymousBenchmarkName;

		protected var _caseName:String = '';
		/** The benchmark case name. */
		public function get caseName():String {
			return _caseName;
		}

		protected var _isWrapperCase:Boolean;
		/** Whether this case was created by run(), which happens when run() receives a benchmark case item as the benchable argument. */
		public function get isWrapperCase():Boolean {
			return _isWrapperCase;
		}

		protected var _caseCount:int;

		public function get caseCount():int {
			return _caseCount;
		}

		protected var _caseIndex:int;

		public function get caseIndex():int {
			return _caseIndex;
		}

		protected var itemQueue:Vector.<BenchmarkCaseItem>;
		protected var benchItem:BenchmarkCaseItem;

		protected var _itemCount:int;

		public function get itemCount():int {
			return _itemCount;
		}

		protected var _itemIndex:int;

		public function get itemIndex():int {
			return _itemIndex;
		}

		protected var _benchmarkResults:BenchmarkResults;

		public function get benchmarkResults():BenchmarkResults {
			return _benchmarkResults;
		}

		protected var _caseResults:BenchmarkResults;

		public function get caseResults():BenchmarkResults {
			return _caseResults;
		}

		protected var _state:int = STATE_IDLE;

		public function get state():int {
			return _state;
		}

		protected var _anonCase:BenchmarkCase;

		protected function initAnonymousCase(benchItem:BenchmarkCaseItem):BenchmarkCase {
			if (_anonCase)
				_anonCase.items.length = 0;
			else
				_anonCase = new BenchmarkCase(anonymousCaseName);
			_anonCase.addItem(benchItem);
			return _anonCase;
		}

		protected var nameFilterFun:Function;
		protected var timerDelay:int;
		protected var delayedCall:Object;

		[Inline]
		protected final function get isAsync():Boolean {
			return timerDelay;
		}

		/**
		 * Runs a benchmark.
		 *
		 * @param benchable A benchmarkable object of type Benchmark, BenchmarkCase or BenchmarkCaseItem.
		 * @param nameFilterFun A test function to filter cases by name. If specified, the case is only benchmarked if the function returns true for it's name:
		 * 		function nameFilterFun(caseName:String):Boolean;
		 * @param asyncMode The async mode to use for this benchmark. If specified, overrides the asyncMode property.
		 * @param gcMode The garbage collection mode to use for this benchmark. If specified, overrides the gcMode property.
		 */
		public function run(benchable:IBenchmarkable, nameFilterFun:Function = null, asyncMode:BenchmarkRunnerAsyncMode = null, gcMode:BenchmarkRunnerGCMode = null):void {
			if (caseQueue) {
				Logger.warn(this, 'run(): Running a benchmark already. Intended to run in synchronous mode? Use stop() before run() to remove this warning.');
				clear();
			}

			if (benchable is Benchmark) {
				CONFIG::debug {
					Logger.warn(this, 'run(): This is a debug build. It is recommended to use a release build for benchmarking.');
				}
				_isWrapperBenchmark = _isWrapperCase = false;
				var benchmark:Benchmark = benchable as Benchmark;
				_benchmarkName = benchmark.name ? benchmark.name : anonymousBenchmarkName;
				caseQueue = benchmark.cases;
			}
			else if (benchable is BenchmarkCase) {
				_isWrapperBenchmark = true;
				_isWrapperCase = false;
				_benchmarkName = anonymousBenchmarkName;
				caseQueue = new <BenchmarkCase>[benchable as BenchmarkCase];
			}
			else if (benchable is BenchmarkCaseItem) {
				_isWrapperBenchmark = _isWrapperCase = true;
				_benchmarkName = anonymousBenchmarkName;
				caseQueue = new <BenchmarkCase>[initAnonymousCase(benchable as BenchmarkCaseItem)];
			}
			else {
				throw new Error('The benchable argument is ' + (benchable ? 'not a Benchmark, a BenchmarkCase or a BenchmarkCaseItem.' : 'null.'));
			}

			_caseCount = caseQueue.length;
			_caseIndex = -1;

			itemQueue = null;
			_itemCount = 0;
			_itemIndex = -1;

			if (!_caseCount) {
				finish();
				return;
			}

			this.nameFilterFun = nameFilterFun;
			benchmarkAsyncMode = asyncMode ? asyncMode : this.asyncMode;
			benchmarkGCMode = gcMode ? gcMode : this.gcMode;
			benchmarkFormatOptions = benchmark ? benchmark.formatOptions.clone() : null; // Without an item queue, can't determine if it's a single-item or a multi-item benchmark.
			benchmarkItemDuration = (benchmark && benchmark.itemDuration) ? benchmark.itemDuration : itemDuration;
			timerDelay = -1;

			_state = STATE_RUNNING;
			dispatchEvent(new BenchmarkEvent(BenchmarkEvent.START));
			nextCase();
		}

		/**
		 * Stops the running benchmark, and clears the benchmark-related properties.
		 */
		public function stop():void {
			clear();
			dispatchEvent(new BenchmarkEvent(BenchmarkEvent.STOP));
		}

		protected var _pausedDuringDelayedCall:Boolean;

		/**
		 * Pauses a running benchmark. The benchmark can be unpaused with resume().
		 */
		public function pause():void {
			if (_state != STATE_RUNNING)
				return;
			_state = STATE_PAUSED;

			_pausedDuringDelayedCall = delayedCall != null;
			if (delayedCall) {
				TimerUtil.remove(delayedCall);
				delayedCall = null;
			}

			_benchmarkResults.pause(currentTime);

			dispatchEvent(new BenchmarkEvent(BenchmarkEvent.PAUSE));
		}

		/**
		 * Resumes a benchmark paused with pause().
		 */
		public function resume():void {
			if (_state != STATE_PAUSED)
				return;
			_state = STATE_RUNNING;

			dispatchEvent(new BenchmarkEvent(BenchmarkEvent.RESUME));
			// An event listener might have stopped or paused the benchmark runner.
			if (_state != STATE_RUNNING)
				return;

			_benchmarkResults.resume(currentTime);

			if (_pausedDuringDelayedCall)
				delayedCall = TimerUtil.delayCall(_runItemDuration ? runDurationSlice : _runItemCount ? runCountSlice : benchItem ? runItem : nextCase, timerDelay);
			else if (benchCase)
				nextItem();
			else
				nextCase();
		}

		protected function finish():void {
			_benchmarkResults.finish(currentTime);
			traceBenchmarkStats();
			outputBenchmarkResults();
			clear();
			_state = STATE_COMPLETED;
			dispatchEvent(new BenchmarkEvent(BenchmarkEvent.COMPLETE));
		}

		protected function outputBenchmarkResults():void {
		}

		protected function clear():void {
			_state = STATE_IDLE;
			if (delayedCall) {
				TimerUtil.remove(delayedCall);
				delayedCall = null;
			}
			_runItemCount = _runItemDuration = 0;
			_benchmarkName = _caseName = '';
			benchmarkFormatOptions = caseFormatOptions = null;
			benchmarkAsyncMode = null;
			benchmarkGCMode = null;
			caseQueue = null;
			benchCase = null;
			itemQueue = null;
			benchItem = null;
			_benchmarkResults.clear();
			_caseResults.clear();
		}

		protected static const ASYNC_DELAY:int = 20;

		protected function nextCase():void {
			delayedCall = null;
			if (_state != STATE_RUNNING)
				return;

			++_caseIndex;
			if (nameFilterFun) {
				for (; _caseIndex < _caseCount; ++_caseIndex) {
					if (nameFilterFun(caseQueue[_caseIndex].name))
						break;
				}
			}
			if (_caseIndex >= _caseCount) {
				finish();
				return;
			}

			benchCase = caseQueue[_caseIndex];
			_caseName = benchCase.name ? benchCase.name : anonymousCaseName;
			runCase();
		}

		protected function runCase():void {
			var startTime:int = currentTime;
			benchCase.initialize();
			var initTime:int = currentTime - startTime;

			itemQueue = benchCase.items;
			_itemCount = itemQueue.length;
			_itemIndex = -1;

			if (timerDelay < 0) {
				// The first case has been initialized, set benchmark format options and effective async mode.
				var isSingleItem:Boolean = (_caseCount == 1) && (_itemCount == 1);
				var defaultFormatOptions:BenchmarkFormatOptions = formatOptions.clone().copyMissingFrom(isSingleItem ? singleItemFormatOptions : multiItemFormatOptions);
				benchmarkFormatOptions = benchmarkFormatOptions ? benchmarkFormatOptions.copyMissingFrom(defaultFormatOptions) : defaultFormatOptions;
				timerDelay = ((benchmarkAsyncMode == RUN_SYNC) || ((benchmarkAsyncMode == RUN_AUTO) && isSingleItem)) ? 0 : ASYNC_DELAY;
				_benchmarkResults.formatOptions = benchmarkFormatOptions;
				_benchmarkResults.reset(_benchmarkName, currentTime);
			}

			_caseResults.formatOptions = caseFormatOptions = benchCase.formatOptions.clone().copyMissingFrom(benchmarkFormatOptions);
			_caseResults.reset(caseName, currentTime, initTime);
			traceHeader();

			dispatchEvent(new BenchmarkEvent(BenchmarkEvent.CASE_START));
			nextItem();
		}

		protected function finishCase():void {
			_caseResults.finish(currentTime);
			_benchmarkResults.addEntry(_caseName, _caseResults.userTime, _caseResults.totalCount);
			traceFooter();
			outputCaseResults();
			if (disposeOnComplete && (benchCase != _anonCase)) {
				benchCase.dispose();
				caseQueue[_caseIndex] = null;
			}
			benchCase = null;
			dispatchEvent(new BenchmarkEvent(BenchmarkEvent.CASE_COMPLETE));
			if (benchmarkGCMode == GC_ALWAYS)
				System.gc();
			else if (benchmarkGCMode == GC_IF_IMMINENT)
				System.pauseForGCIfCollectionImminent(gcImminence);
		}

		protected function outputCaseResults():void {
		}

		protected function nextItem():void {
			if (_state != STATE_RUNNING)
				return;

			if (++_itemIndex >= _itemCount) {
				finishCase();
				delayedCall = TimerUtil.delayCall(nextCase, timerDelay);
				return;
			}

			benchItem = itemQueue[_itemIndex];

			dispatchEvent(new BenchmarkEvent(BenchmarkEvent.ITEM_START));
			// An event listener might have stopped or paused the benchmark runner.
			if (_state != STATE_RUNNING)
				return;

			if (!benchItem.fun) {
				_caseResults.addEntry(benchItem.description, 0, 0, null, caseFormatOptions.entryFormatFun);
				finishItem();
				nextItem();
				return;
			}

			delayedCall = TimerUtil.delayCall(runItem, timerDelay);
		}

		protected function runItem():void {
			delayedCall = null;

			if (benchItem.count) {
				_runItemCount = benchItem.count;
				_runItemDuration = 0;
				runCount();
			}
			else {
				_runItemCount = 0;
				_runItemDuration = benchItem.duration ? benchItem.duration : benchCase.itemDuration ? benchCase.itemDuration : benchmarkItemDuration;
				if (!_runItemDuration)
					throw new Error('Default item duration is not set, item must specify count or duration: caseName="' + caseName + '" itemIndex=' + _itemIndex + '.');
				if (_runItemDuration < MIN_ITEM_DURATION)
					_runItemDuration = MIN_ITEM_DURATION;
				runDuration();
			}
		}

		protected static const RUN_SAFETY_RATIO:Number = 0.5;
		protected static const RUN_EXCEED_TIMESLICE_THRESHOLD:Number = 0.33;
		protected static const RUN_MIN_CYCLE_MIN_TIME:int = 5;
		protected static const RUN_MIN_CYCLE_TIMESLICE_RATIO:Number = 0.02;

		protected var _runMinCycleTime:int = RUN_MIN_CYCLE_MIN_TIME;

		protected var _runItemCount:int;
		protected var _runItemDuration:int;
		protected var _runItemValue:*;

		protected var _runCount:int;
		protected var _runDuration:int;
		protected var _runCycleCount:int;
		protected var _runSpeed:Number;

		protected function runCount():void {
			var fun:Function = benchItem.fun;
			var args:Array = benchItem.args;

			_runSpeedLogIndex = _runSpeedLogCount = 0;
			var startTime:int = currentTime;
			_runItemValue = fun.apply(null, args);
			_runCount = 1;
			_runCycleCount = (_runItemCount > 1) ? 1 : 0;
			// Use time slices only if the benchmark is running in asynchronous mode.
			var finishTime:int = isAsync ? (startTime + (asyncTimeSlice >>> 1)) : int.MAX_VALUE;
			// Run the function in cycles as long as more than half of the time slice remains.
			while ((_runCycleCount > 0) && (currentTime < finishTime)) {
				var i:int = _runCycleCount;
				do {
					fun.apply(null, args);
				} while (--i);
				_runCount += _runCycleCount;
				// Double the number of iterations for the next cycle. This reduces the measuring code overhead to a minimum.
				_runCycleCount = Math.min(_runItemCount - _runCount, _runCycleCount << 1);
			}
			_runDuration = currentTime - startTime;
			// Calculate an estimate of the function's speed.
			_runSpeed = _runGetAverageSpeed(_runCount, _runDuration);

			runCountSlice(true);
		}

		protected function runCountSlice(firstSlice:Boolean = false):void {
			delayedCall = null;
			if (_state != STATE_RUNNING)
				return;

			var fun:Function = benchItem.fun;
			var args:Array = benchItem.args;
			// If this is the first slice, set the slice start count and duration to 0, to account for the work done by runCount().
			var sliceStartCount:int = firstSlice ? 0 : _runCount;
			var sliceStartDuration:int = firstSlice ? 0 : _runDuration;

			// Use time slices only if the benchmark is running in asynchronous mode. If this is the first slice, reduce the slice duration to account for the time used by runCount().
			var timeSlice:int = isAsync ? (asyncTimeSlice - (firstSlice ? _runDuration : 0)) : int.MAX_VALUE;
			// Run the function in cycles while the remaining time slice is longer than the minimum cycle time. This avoids excessive short-cycling when the time slice is almost used up.
			while ((_runCount < _runItemCount) && (timeSlice >= _runMinCycleTime)) {
				// Calculate the number of iterations for the next cycle. Multiply by a safety ratio to reduce the chance of exceeding the time slice.
				_runCycleCount = Math.min(_runItemCount - _runCount, RUN_SAFETY_RATIO * _runSpeed * timeSlice);
				if (!_runCycleCount) {
					// If the time slice is completely unused, this means that the benchmarked function is so slow that it can't be executed even once per time slice. Run one iteration anyway.
					_runCycleCount = (timeSlice == asyncTimeSlice) ? 1 : Math.min(1, _runSpeed * timeSlice + RUN_EXCEED_TIMESLICE_THRESHOLD);
					if (!_runCycleCount)
						break;
				}

				var i:int = _runCycleCount;
				var startTime:int = currentTime;
				do {
					fun.apply(null, args);
				} while (--i);
				_runCount += _runCycleCount;
				var elapsedTime:int = currentTime - startTime;
				_runDuration += elapsedTime;

				// Recalculate the function's speed, but only if this is the first cycle - longest and therefore most accurate.
				if (timeSlice == asyncTimeSlice)
					_runSpeed = _runGetAverageSpeed(_runCycleCount, elapsedTime);
				timeSlice -= elapsedTime;
			}

			_runFinishSlice(firstSlice, sliceStartCount, sliceStartDuration);
		}

		protected function runDuration():void {
			var fun:Function = benchItem.fun;
			var args:Array = benchItem.args;

			_runSpeedLogIndex = _runSpeedLogCount = 0;
			var startTime:int = currentTime;
			_runItemValue = fun.apply(null, args);
			_runCount = 1;
			_runCycleCount = 1;
			// Use time slices only if the benchmark is running in asynchronous mode.
			var finishTime:int = startTime + (isAsync ? (Math.min(_runItemDuration >>> 2, asyncTimeSlice >>> 1)) : (_runItemDuration >>> 2));
			// Run the function in cycles as long as more than half of the time slice remains.
			while (currentTime < finishTime) {
				var i:int = _runCycleCount;
				do {
					fun.apply(null, args);
				} while (--i);
				_runCount += _runCycleCount;
				// Double the number of iterations for the next cycle. This reduces the measuring code overhead to a minimum.
				_runCycleCount <<= 1;
			}
			_runDuration = currentTime - startTime;
			// Calculate an estimate of the function's speed.
			_runSpeed = _runGetAverageSpeed(_runCount, _runDuration);

			runDurationSlice(true);
		}

		protected function runDurationSlice(firstSlice:Boolean = false):void {
			delayedCall = null;
			if (_state != STATE_RUNNING)
				return;

			var fun:Function = benchItem.fun;
			var args:Array = benchItem.args;
			// If this is the first slice, set the slice start count and duration to 0, to account for the work done by runDuration().
			var sliceStartCount:int = firstSlice ? 0 : _runCount;
			var sliceStartDuration:int = firstSlice ? 0 : _runDuration;

			var durationLeft:int;
			// Use time slices only if the benchmark is running in asynchronous mode. If this is the first slice, reduce the slice duration to account for the time used by runDuration().
			var timeSlice:int = isAsync ? (asyncTimeSlice - (firstSlice ? _runDuration : 0)) : _runItemDuration;
			// Run the function in cycles while the remaining time slice is longer than the minimum cycle time. This avoids excessive short-cycling when the time slice is almost used up.
			while (((durationLeft = _runItemDuration - _runDuration) > 0) && (timeSlice >= Math.min(durationLeft, _runMinCycleTime))) {
				// Calculate the number of iterations for the next cycle. Multiply by a safety ratio to reduce the chance of exceeding the time slice.
				_runCycleCount = RUN_SAFETY_RATIO * _runSpeed * Math.min(durationLeft, timeSlice);
				if (!_runCycleCount) {
					// If the time slice is completely unused, this means that the benchmarked function is so slow that it can't be executed even once per time slice. Run one iteration anyway.
					_runCycleCount = (timeSlice == asyncTimeSlice) ? 1 : Math.min(1, _runSpeed * timeSlice + RUN_EXCEED_TIMESLICE_THRESHOLD);
					if (!_runCycleCount)
						break;
				}

				var i:int = _runCycleCount;
				var startTime:int = currentTime;
				do {
					fun.apply(null, args);
				} while (--i);
				_runCount += _runCycleCount;
				var elapsedTime:int = currentTime - startTime;
				_runDuration += elapsedTime;

				// Recalculate the function's speed, but only if this is the first cycle - longest and therefore most accurate.
				if (timeSlice == asyncTimeSlice)
					_runSpeed = _runGetAverageSpeed(_runCycleCount, elapsedTime);
				timeSlice -= elapsedTime;
			}

			_runFinishSlice(firstSlice, sliceStartCount, sliceStartDuration);
		}

		protected static const RUN_SPEED_LOG_SIZE:int = 5;

		protected var _runSpeedLog:Vector.<Number> = new Vector.<Number>(RUN_SPEED_LOG_SIZE, true);
		protected var _runSpeedLogIndex:int;
		protected var _runSpeedLogCount:int;

		protected function _runGetAverageSpeed(count:int, elapsedTime:int):Number {
			if (elapsedTime) {
				if (_runSpeedLogCount < RUN_SPEED_LOG_SIZE)
					_runSpeedLogIndex = _runSpeedLogCount++;
				else if (++_runSpeedLogIndex >= _runSpeedLogCount)
					_runSpeedLogIndex = 0;
				_runSpeedLog[_runSpeedLogIndex] = count / elapsedTime;
			}

			var sum:Number = 0;
			for (var i:int = _runSpeedLogCount - 1; i >= 0; --i)
				sum += _runSpeedLog[i];
			return sum / _runSpeedLogCount;
		}

		protected function _runFinishSlice(firstSlice:Boolean, sliceStartCount:int, sliceStartDuration:int):void {
			var amountCompleted:int = _runItemCount ? _runCount : _runDuration;
			var amountTotal:int = _runItemCount ? _runItemCount : _runItemDuration;
			var nextSliceFun:Function = _runItemCount ? runCountSlice : runDurationSlice;

			if (firstSlice) {
				var valueFormatFun:Function = benchItem.valueFormatFun ? benchItem.valueFormatFun : caseFormatOptions.valueFormatFun;
				_runItemValue = valueFormatFun(_runItemValue);
			}

			if (reportTimeSlices && ((amountCompleted < amountTotal) || !firstSlice)) {
				var flags:uint = firstSlice ? (BenchmarkResultEntry.SLICE | BenchmarkResultEntry.FIRST_SLICE) : BenchmarkResultEntry.SLICE;
				var progress:uint = 1000 * amountCompleted / amountTotal;
				_caseResults.addEntry(benchItem.description, _runDuration - sliceStartDuration, _runCount - sliceStartCount, _runItemValue, caseFormatOptions.entryFormatFun, flags, progress);
				traceEntry();
			}

			if (amountCompleted < amountTotal) {
				delayedCall = TimerUtil.delayCall(nextSliceFun, timerDelay);
				dispatchEvent(new BenchmarkEvent(BenchmarkEvent.ITEM_PROGRESS));
			}
			else {
				flags = (reportTimeSlices && !firstSlice && isAsync) ? BenchmarkResultEntry.COMPOUND : 0;
				_caseResults.addEntry(benchItem.description, _runDuration, _runCount, _runItemValue, caseFormatOptions.entryFormatFun, flags);
				finishItem();
				nextItem();
			}
		}

		protected function finishItem():void {
			_runItemCount = _runItemDuration = 0;
			traceEntry();
			outputItemResults();
			benchItem = null;
			dispatchEvent(new BenchmarkEvent(BenchmarkEvent.ITEM_COMPLETE));
		}

		protected function outputItemResults():void {
		}

		protected function traceHeader():void {
			if (!traceFun)
				return;
			var header:String = _caseResults.header;
			if (header)
				traceFun(header);
		}

		protected function traceFooter():void {
			if (!traceFun)
				return;
			var footer:String = _caseResults.footer;
			if (footer)
				traceFun(footer + StrUtil.repeat('\n', caseFormatOptions.resultsGap));
			else if (caseFormatOptions.resultsGap)
				traceFun(StrUtil.repeat('\n', caseFormatOptions.resultsGap - 1));
		}

		protected function traceEntry():void {
			if (!traceFun)
				return;
			var entry:BenchmarkResultEntry = _caseResults.entries[_caseResults.entries.length - 1];
			var entryFormatFun:Function = entry.entryFormatFun ? entry.entryFormatFun : caseFormatOptions.entryFormatFun;
			traceFun(entryFormatFun(entry));
		}

		protected function traceBenchmarkStats():void {
			if (!traceFun)
				return;
			var header:String = _benchmarkResults.header;
			var footer:String = _benchmarkResults.footer;
			if (header || footer)
				traceFun('\n\n' + (header ? header.replace(/\n.*/s, ' benchmark complete' + (footer ? '\n\n' : '')) : '') + footer.replace(/^\n*/, ''));
		}

		[Inline]
		protected final function get currentTime():int {
			return getTimer();
		}
	}
}
