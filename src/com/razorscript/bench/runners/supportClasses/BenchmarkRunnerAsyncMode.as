package com.razorscript.bench.runners.supportClasses {
	import com.razorscript.utils.enum.EnumBase;
	import com.razorscript.utils.enum.EnumUtil;
	
	/**
	 * Defines constants for benchmark runner async mode.
	 *
	 * @author Gene Pavlovsky
	 */
	public class BenchmarkRunnerAsyncMode extends EnumBase {
		/**
		 * Asynchronous (non-blocking) mode. Runs the benchmark with a short pause between every case and item.
		 * While the benchmark is running, the app remains responsive.
		 * Recommended for dedicated benchmark apps and general use.
		 */
		[Enum]
		public static const ASYNC:BenchmarkRunnerAsyncMode = factory();
		
		/**
		 * Synchronous (blocking) mode. Runs the benchmark without pausing.
		 * While the benchmark is running, the app will be non-responsive.
		 * Useful for testing, but only practical for short benchmarks.
		 */
		[Enum]
		public static const SYNC:BenchmarkRunnerAsyncMode = factory();
		
		/**
		 * Runs single-item benchmarks in sync mode and multi-item benchmarks in async mode.
		 * Recommended for general use.
		 */
		[Enum]
		public static const AUTO:BenchmarkRunnerAsyncMode = factory();
		
		protected static function factory():BenchmarkRunnerAsyncMode {
			return new BenchmarkRunnerAsyncMode(LOCK);
		}
		
		/**
		 * @private
		 */
		public function BenchmarkRunnerAsyncMode(lock:Object) {
			super(lock);
		}
		
		EnumUtil.registerClass(BenchmarkRunnerAsyncMode);
	}
}
