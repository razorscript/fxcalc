package com.razorscript.bench.runners.supportClasses {
	import com.razorscript.utils.enum.EnumBase;
	import com.razorscript.utils.enum.EnumUtil;
	
	/**
	 * Defines constants for benchmark runner garbage collection mode.
	 *
	 * @author Gene Pavlovsky
	 */
	public class BenchmarkRunnerGCMode extends EnumBase {
		/**
		 * After each benchmark case is finished, runs the garbage collector.
		 * This is the most reliable way to prevent garbage collection from triggering during benchmarking.
		 * Recommended for dedicated benchmark apps.
		 */
		[Enum]
		public static const ALWAYS:BenchmarkRunnerGCMode = factory();
		
		/**
		 * After each benchmark case is finished, advises the garbage collector to run if the garbage collection is imminent.
		 * This should in most cases prevent garbage collection from triggering during benchmarking.
		 * Recommended for general use.
		 */
		[Enum]
		public static const IF_IMMINENT:BenchmarkRunnerGCMode = factory();
		
		/**
		 * Lets the garbage collector decide when to run.
		 * If garbage collection triggers when a case is running, the benchmark results can be affected.
		 * Recommended only for testing.
		 */
		[Enum]
		public static const SYSTEM:BenchmarkRunnerGCMode = factory();
		
		protected static function factory():BenchmarkRunnerGCMode {
			return new BenchmarkRunnerGCMode(LOCK);
		}
		
		/**
		 * @private
		 */
		public function BenchmarkRunnerGCMode(lock:Object) {
			super(lock);
		}
		
		EnumUtil.registerClass(BenchmarkRunnerGCMode);
	}
}
