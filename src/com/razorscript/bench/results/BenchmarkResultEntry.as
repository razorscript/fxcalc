package com.razorscript.bench.results {
	
	/**
	 * Contains properties describing a benchmark result entry.
	 *
	 * @author Gene Pavlovsky
	 */
	public class BenchmarkResultEntry {
		public static const COMPOUND:uint = 1 << uid++;
		public static const SLICE:uint = 1 << uid++;
		public static const FIRST_SLICE:uint = 1 << uid++;
		
		/** Reserve 8 bits for flags, with 24 bits left for data. */
		protected static const MAX_UID:int = 8;
		
		private static var _uid:int = 0;
		
		protected static function get uid():int {
			return _uid;
		}
		
		protected static function set uid(value:int):void {
			if (value > MAX_UID)
				throw new RangeError('Value (' + value + ') for the uid property is outside the range [0, ' + MAX_UID + '].');
			_uid = value;
		}
		
		public function BenchmarkResultEntry(description:String, elapsedTime:int, count:int, value:String = null, entryFormatFun:Function = null, flags:uint = 0, data:uint = 0) {
			_description = description;
			_elapsedTime = elapsedTime;
			_count = count;
			_value = value;
			_entryFormatFun = entryFormatFun;
			_flags = data ? (flags | (data << MAX_UID)) : flags;
		}
		
		protected var _description:String;
		
		public function get description():String {
			return _description;
		}
		
		protected var _elapsedTime:int;
		
		public function get elapsedTime():int {
			return _elapsedTime;
		}
		
		protected var _count:int;
		
		public function get count():int {
			return _count;
		}
		
		protected var _value:String;
		
		public function get value():String {
			return _value;
		}
		
		protected var _entryFormatFun:Function;
		
		public function get entryFormatFun():Function {
			return _entryFormatFun;
		}
		
		public static const FLAGS_MASK:uint = 0xffffffff >>> (32 - MAX_UID);
		
		protected var _flags:uint;
		
		public function get flags():uint {
			return _flags & FLAGS_MASK;
		}
		
		public function get data():uint {
			return _flags >>> MAX_UID;
		}
		
		/**
		 * Whether this entry represents a partial slice of a benchmark case item's total duration.
		 *
		 * Slice entries are reported if the following conditions are met:
		 * 		- The benchmark is running in asynchronous mode
		 * 		- Item's duration is greater than the benchmark runner's asyncTimeSlice property
		 * 		- The benchmark runner's reportTimeSlices property is true
		 * After all slices of a benchmark case item are finished, a compound entry is added, with a summary of all slice entries.
		 * When analyzing the benchmark results to compute statistics, care should be taken to count duration and count values of either a summary entry or the corresponding slice entries.
		 */
		public function get isSlice():Boolean {
			return _flags & SLICE;
		}
		
		public function get isFirstSlice():Boolean {
			return _flags & FIRST_SLICE;
		}
		
		public function get isCompound():Boolean {
			return _flags & COMPOUND;
		}
		
		public function get isRegular():Boolean {
			return !(_flags & (SLICE | COMPOUND));
		}
		
		public function toString():String {
			return '[BenchmarkResultEntry description="' + _description + '" elapsedTime=' + _elapsedTime + ' count=' + _count + ' value=' + _value +
				' isSlice=' + isSlice + ' isFirstSlice=' + isFirstSlice + ' isCompound=' + isCompound + ']';
		}
	}
}
