package com.razorscript.bench.results {
	import com.razorscript.bench.formatters.BenchmarkFormatOptions;
	
	/**
	 * Contains properties containing a benchmark results.
	 *
	 * @author Gene Pavlovsky
	 */
	public class BenchmarkResults {
		public function BenchmarkResults(name:String = null, formatOptions:BenchmarkFormatOptions = null) {
			_name = name;
			this.formatOptions = formatOptions;
			_entries = new Vector.<BenchmarkResultEntry>();
		}
		
		/**
		 * Benchmark format options. The entryFormatFun property must be not null.
		 */
		public var formatOptions:BenchmarkFormatOptions;
		
		public function get header():String {
			var factory:* = formatOptions.headerFactory;
			return (factory is Function) ? factory(this) : factory;
		}
		
		public function get footer():String {
			var factory:* = formatOptions.footerFactory;
			return (factory is Function) ? factory(this) : factory;
		}
		
		protected var _name:String;
		
		public function get name():String {
			return _name;
		}
		
		protected var _initTime:int;
		
		public function get initTime():int {
			return _initTime;
		}
		
		protected var _realTime:int;
		
		public function get realTime():int {
			return _realTime;
		}
		
		protected var _userTime:int;
		
		public function get userTime():int {
			return _userTime;
		}
		
		protected var _systemTime:int;
		
		public function get systemTime():int {
			return _systemTime;
		}
		
		protected var _totalCount:int;
		
		public function get totalCount():int {
			return _totalCount;
		}
		
		protected var _entries:Vector.<BenchmarkResultEntry>;
		
		public function get entries():Vector.<BenchmarkResultEntry> {
			return _entries;
		}
		
		public function addEntry(description:String, elapsedTime:int, count:int = 1, value:String = null, entryFormatFun:Function = null, flags:uint = 0, data:uint = 0):void {
			if (!(flags & BenchmarkResultEntry.SLICE)) {
				_userTime += elapsedTime;
				_totalCount += count;
			}
			_entries.push(new BenchmarkResultEntry(description, elapsedTime, count, value, entryFormatFun, flags, data));
		}
		
		protected static const emptyEntry:BenchmarkResultEntry = new BenchmarkResultEntry(null, 0, 0);
		
		public function clear():void {
			_entries.length = 0;
			if (formatOptions)
				formatOptions.entryFormatFun(emptyEntry);
		}
		
		public function reset(name:String, currentTime:int = 0, initTime:int = -1):void {
			_name = name;
			startTime = currentTime;
			_initTime = initTime;
			_realTime = _userTime = _systemTime = 0;
			_totalCount = 0;
			_entries.length = 0;
			if (formatOptions)
				formatOptions.entryFormatFun(emptyEntry);
		}
		
		public function pause(currentTime:int):void {
			_realTime += currentTime - startTime;
		}
		
		public function resume(currentTime:int):void {
			startTime = currentTime;
		}
		
		public function finish(currentTime:int = 0):void {
			_realTime = currentTime - startTime;
			_systemTime = _realTime - _userTime;
			if (formatOptions)
				formatOptions.entryFormatFun(emptyEntry);
		}
		
		public function formatEntries():String {
			var length:int = _entries.length;
			var vec:Vector.<String> = new Vector.<String>();
			for (var i:int = 0; i < length; ++i) {
				var entry:BenchmarkResultEntry = _entries[i];
				var entryFormatFun:Function = entry.entryFormatFun ? entry.entryFormatFun : formatOptions.entryFormatFun;
				vec.push(entryFormatFun(entry));
			}
			return vec.join('\n');
		}
		
		protected var startTime:int;
	}
}
