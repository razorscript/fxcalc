package com.razorscript.bench.events {
	import com.razorscript.bench.BenchmarkCase;
	import com.razorscript.bench.BenchmarkCaseItem;
	import flash.events.Event;
	
	/**
	 * Dispatched by BenchmarkRunner.
	 *
	 * @author Gene Pavlovsky
	 */
	public class BenchmarkEvent extends Event {
		public static const START:String = 'start';
		public static const STOP:String = 'stop';
		public static const PAUSE:String = 'pause';
		public static const RESUME:String = 'resume';
		public static const COMPLETE:String = 'complete';
		public static const CASE_START:String = 'caseStart';
		public static const CASE_COMPLETE:String = 'caseComplete';
		public static const ITEM_START:String = 'itemStart';
		public static const ITEM_PROGRESS:String = 'itemProgress';
		public static const ITEM_COMPLETE:String = 'itemComplete';
		
		public function BenchmarkEvent(type:String) {
			super(type, false, false);
		}
		
		override public function clone():Event {
			return new BenchmarkEvent(type);
		}
		
		override public function toString():String {
			return formatToString('BenchmarkEvent', 'type');
		}
	}
}
