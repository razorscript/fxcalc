package com.razorscript.fxcalc.events.settings.ui {
	
	/**
	 * Event type constants for ThemeUISettings.
	 *
	 * @author Gene Pavlovsky
	 */
	public class ThemeUISettingsEventType {
		public static const NAME_CHANGE:String = 'nameChange';
		public static const STYLE_NAME_CHANGE:String = 'styleNameChange';
		public static const COLOR_SCHEME_NAME_CHANGE:String = 'colorSchemeNameChange';
	}
}
