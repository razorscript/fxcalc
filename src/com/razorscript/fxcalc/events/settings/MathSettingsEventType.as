package com.razorscript.fxcalc.events.settings {
	
	/**
	 * Event type constants for MathSettings.
	 *
	 * @author Gene Pavlovsky
	 */
	public class MathSettingsEventType {
		public static const DEFAULT_ANGLE_UNIT_CHANGE:String = 'defaultAngleUnitChange';
		public static const LENIENT_BRACKET_CHECKING_CHANGE:String = 'lenientBracketCheckingChange';
		public static const NUMBER_ALLOW_LEADING_EXP_CHANGE:String = 'numberAllowLeadingExpChange';
		public static const NUMBER_ALLOW_SOLO_RADIX_CHANGE:String = 'numberAllowSoloRadixChange';
	}
}
