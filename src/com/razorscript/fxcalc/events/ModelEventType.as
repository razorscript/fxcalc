package com.razorscript.fxcalc.events {
	
	/**
	 * Event type constants for Model.
	 *
	 * @author Gene Pavlovsky
	 */
	public class ModelEventType {
		public static const LAST_APP_VERSION_CHANGE:String = 'lastAppVersionChange';
		public static const CALC_MODE_CHANGE:String = 'calcModeChange';
		public static const MEMORY_VARIABLE_CHANGE:String = 'memoryVariableChange';
		public static const ANSWER_VARIABLE_CHANGE:String = 'answerVariableChange';
		public static const ANGLE_UNIT_MODE_CHANGE:String = 'angleUnitModeChange';
		public static const HISTORY_CHANGE:String = 'historyChange';
		public static const FORMULA_CHANGE:String = 'formulaChange';
		public static const TOKENS_CHANGE:String = 'tokensChange';
		public static const TOKEN_FLAGS_CHANGE:String = 'tokenFlagsChange';
		public static const RESULT_CHANGE:String = 'resultChange';
	}
}
