package com.razorscript.fxcalc.events {
	
	/**
	 * Event type constants for ViewModel.
	 *
	 * @author Gene Pavlovsky
	 */
	public class ViewModelEventType {
		public static const RESULT_CHANGE:String = 'resultChange';
		public static const NULL_RESULT_CHANGE:String = 'nullResultChange';
		public static const DISPLAY_STATE_CHANGE:String = 'displayStateChange';
		public static const IS_EDITABLE_CHANGE:String = 'isEditableChange';
		public static const RESULT_DISPLAY_FORMAT_CHANGE:String = 'resultDisplayFormatChange';
		public static const RESULT_DISPLAY_NUMBER_FORMATTER_CHANGE:String = 'resultDisplayNumberFormatterChange';
		public static const INSERT_MODE_CHANGE:String = 'insertModeChange';
		public static const INVERSE_TRIG_MODE_CHANGE:String = 'inverseTrigModeChange';
		public static const HYPERBOLIC_TRIG_MODE_CHANGE:String = 'hyperbolicTrigModeChange';
		public static const CARET_INDEX_CHANGE:String = 'caretIndexChange';
		public static const MAX_CARET_INDEX_CHANGE:String = 'maxCaretIndexChange';
		public static const HISTORY_INDEX_CHANGE:String = 'historyIndexChange';
		public static const MAX_HISTORY_INDEX_CHANGE:String = 'maxHistoryIndexChange';
		public static const TOUCH_INPUT_ENABLED_CHANGE:String = 'touchInputEnabledChange';
		public static const KEYBOARD_INPUT_ENABLED_CHANGE:String = 'keyboardInputEnabledChange';
	}
}
