package com.razorscript.fxcalc.debug {
	import com.razorscript.debug.Debug;
	import com.razorscript.debug.Logger;
	
	/**
	 * Provides properties for testing and debugging.
	 *
	 * @author Gene Pavlovsky
	 */
	public class FXDebug extends Debug {
		protected static var _instance:FXDebug;
		
		public static function get instance():FXDebug {
			if (!_instance)
				Debug._instance = _instance = new FXDebug();
			return _instance;
		}
		
		public static function set instance(value:FXDebug):void {
			_instance = value;
		}
		
		public function FXDebug() {
			super();
		}
		
		/***********************************************************************************************************************************************/
		
		/**
		 * Misc.
		 */
		
		public static const DEBUG_NORMAL:int = 1;
		public static const DEBUG_VERBOSE:int = 2;
		
		public var debugChangeLog:int = 0;
		public var debugSettings:int = 0;
		public var debugState:int = 0;
		
		/***********************************************************************************************************************************************/
		
		/**
		 * Pooling.
		 */
		
		protected var _formulaTokenPoolSize:int;
		
		public function get formulaTokenPoolSize():int {
			return _formulaTokenPoolSize;
		}
		
		public function set formulaTokenPoolSize(value:int):void {
			if (!_debugPooling)
				return;
			_formulaTokenPoolSize = value;
			if (value > _maxFormulaTokenPoolSize) {
				_maxFormulaTokenPoolSize = value;
				Logger.debug(this, 'maxFormulaTokenPoolSize=' + value);
			}
		}
		
		protected var _maxFormulaTokenPoolSize:int;
		
		public function get maxFormulaTokenPoolSize():int {
			return _maxFormulaTokenPoolSize;
		}
	}
}
