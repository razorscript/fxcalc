package com.razorscript.fxcalc.math.lexers {
	import com.razorscript.fxcalc.math.tokens.FormulaToken;
	import com.razorscript.fxcalc.math.tokens.FormulaTokenFactory;
	import com.razorscript.fxcalc.math.tokens.IFormulaTokenFactory;
	import com.razorscript.math.expression.converters.MathExpressionConvUtil;
	import com.razorscript.math.expression.lexers.IMathTokenProvider;
	import com.razorscript.math.expression.tokens.MathToken;
	
	/**
	 * An implementation of IMathTokenProvider that stores formula tokens in a vector. Each formula token contains a math token.
	 *
	 * @author Gene Pavlovsky
	 */
	public class FormulaTokenVectorMathTokenProvider implements IFormulaTokenMathTokenProvider {
		public function FormulaTokenVectorMathTokenProvider(source:Vector.<FormulaToken> = null, formulaTokenFactory:IFormulaTokenFactory = null) {
			_source = source ? source : new Vector.<FormulaToken>;
			_formulaTokenFactory = formulaTokenFactory ? formulaTokenFactory : new FormulaTokenFactory();
		}
		
		protected var _source:Vector.<FormulaToken>;
		/**
		 * The input vector of FormulaToken objects.
		 *
		 * @throws ArgumentError - if value is not a Vector.
		 */
		public function get source():* {
			return _source;
		}
		
		public function set source(value:*):void {
			if (!(value is Vector.<FormulaToken>))
				throw new ArgumentError('The argument (' + value + ') is not a Vector.<FormulaToken>.');
			_source = value;
		}
		
		public function get length():int {
			return _source.length;
		}
		
		protected var _formulaTokenFactory:IFormulaTokenFactory;
		/**
		 * The formula token factory used to create new tokens.
		 */
		public function get formulaTokenFactory():IFormulaTokenFactory {
			return _formulaTokenFactory;
		}
		
		public function set formulaTokenFactory(value:IFormulaTokenFactory):void {
			_formulaTokenFactory = value;
		}
		
		public function setSource(source:* = null):IMathTokenProvider {
			this.source = source ? source : new Vector.<FormulaToken>;
			return this;
		}
		
		public function getTokenAt(index:int):MathToken {
			return _source[index].token;
		}
		
		public function addToken(token:MathToken):void {
			_source.push(new FormulaToken(token));
		}
		
		public function getItemAt(index:int):* {
			return _source[index];
		}
		
		public function addItem(item:*):void {
			_source.push(item);
		}
		
		public function clear():void {
			var _fixed:Boolean = _source.fixed;
			_source.fixed = false;
			_source.length = 0;
			_source.fixed = _fixed;
		}
		
		public function toString():String {
			return '[FormulaTokenVectorMathTokenProvider source="' + MathExpressionConvUtil.convertToString(this) + '" length=' + length +	']';
		}
	}
}
