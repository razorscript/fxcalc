package com.razorscript.fxcalc.math.lexers {
	import com.razorscript.fxcalc.math.tokens.IFormulaTokenFactory;
	import com.razorscript.math.expression.lexers.IMathTokenProvider;
	
	/**
	 * A math token provider for MathTokenLexer that stores math tokens inside formula tokens.
	 *
	 * @author Gene Pavlovsky
	 */
	public interface IFormulaTokenMathTokenProvider extends IMathTokenProvider {
		function get formulaTokenFactory():IFormulaTokenFactory;
		function set formulaTokenFactory(value:IFormulaTokenFactory):void;
	}
}
