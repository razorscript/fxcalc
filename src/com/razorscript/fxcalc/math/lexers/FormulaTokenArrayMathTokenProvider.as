package com.razorscript.fxcalc.math.lexers {
	import com.razorscript.fxcalc.math.tokens.FormulaToken;
	import com.razorscript.fxcalc.math.tokens.FormulaTokenFactory;
	import com.razorscript.fxcalc.math.tokens.IFormulaTokenFactory;
	import com.razorscript.math.expression.converters.MathExpressionConvUtil;
	import com.razorscript.math.expression.lexers.IMathTokenProvider;
	import com.razorscript.math.expression.tokens.MathToken;
	
	/**
	 * An implementation of IMathTokenProvider that stores formula tokens in an array. Each formula token contains a math token.
	 *
	 * @author Gene Pavlovsky
	 */
	public class FormulaTokenArrayMathTokenProvider implements IFormulaTokenMathTokenProvider {
		public function FormulaTokenArrayMathTokenProvider(source:Array/*FormulaToken*/ = null, formulaTokenFactory:IFormulaTokenFactory = null) {
			_source = source ? source : [];
			_formulaTokenFactory = formulaTokenFactory ? formulaTokenFactory : new FormulaTokenFactory();
		}
		
		protected var _source:Array/*FormulaToken*/;
		/**
		 * The input array of FormulaToken objects.
		 *
		 * @throws ArgumentError - if value is not an Array.
		 */
		public function get source():* {
			return _source;
		}
		
		public function set source(value:*):void {
			if (!(value is Array))
				throw new ArgumentError('The argument (' + value + ') is not an Array.');
			_source = value;
		}
		
		public function get length():int {
			return _source.length;
		}
		
		protected var _formulaTokenFactory:IFormulaTokenFactory;
		/**
		 * The formula token factory used to create new tokens.
		 */
		public function get formulaTokenFactory():IFormulaTokenFactory {
			return _formulaTokenFactory;
		}
		
		public function set formulaTokenFactory(value:IFormulaTokenFactory):void {
			_formulaTokenFactory = value;
		}
		
		public function setSource(source:* = null):IMathTokenProvider {
			this.source = source ? source : [];
			return this;
		}
		
		public function getTokenAt(index:int):MathToken {
			return _source[index].token;
		}
		
		public function addToken(token:MathToken):void {
			_source.push(_formulaTokenFactory.createFormulaToken(token));
		}
		
		public function getItemAt(index:int):* {
			return _source[index];
		}
		
		public function addItem(item:*):void {
			_source.push(item);
		}
		
		public function clear():void {
			_source.length = 0;
		}
		
		public function toString():String {
			return '[FormulaTokenArrayMathTokenProvider source="' + MathExpressionConvUtil.convertToString(this) + '" length=' + length +	']';
		}
	}
}
