package com.razorscript.fxcalc.math {
	import com.razorscript.debug.Logger;
	import com.razorscript.fxcalc.data.constants.MathConst;
	import com.razorscript.fxcalc.math.lexers.IFormulaTokenMathTokenProvider;
	import com.razorscript.fxcalc.math.tokens.FormulaToken;
	import com.razorscript.math.expression.MathExpression;
	import com.razorscript.math.expression.MathFun;
	import com.razorscript.math.expression.MathOperator;
	import com.razorscript.math.expression.MathParser;
	import com.razorscript.math.expression.tokens.MathToken;
	import com.razorscript.math.expression.tokens.NumberCharToken;
	import com.razorscript.math.expression.tokens.OperatorToken;
	
	/**
	 * Performs live syntax checking of the formula.
	 *
	 * @author Gene Pavlovsky
	 */
	public class LiveSyntaxChecker {
		public function LiveSyntaxChecker(mathParser:MathParser = null, mathTokenProvider:IFormulaTokenMathTokenProvider = null) {
			_mathParser = mathParser;
			_mathTokenProvider = mathTokenProvider;
		}
		
		protected var _mathParser:MathParser;
		/**
		 * The MathParser used when checking syntax.
		 */
		public function get mathParser():MathParser {
			return _mathParser;
		}
		
		public function set mathParser(value:MathParser):void {
			_mathParser = value;
		}
		
		protected var _mathTokenProvider:IFormulaTokenMathTokenProvider;
		/**
		 * The math token provider used for parsing expressions.
		 */
		public function get mathTokenProvider():IFormulaTokenMathTokenProvider {
			return _mathTokenProvider;
		}
		
		public function set mathTokenProvider(value:IFormulaTokenMathTokenProvider):void {
			_mathTokenProvider = value;
		}
		
		public function updateFlags(tokens:Vector.<FormulaToken>):void {
			var mathOperator:MathOperator = _mathParser.mathOperator;
			
			var length:int = tokens.length;
			for (var i:int = 0; i < length; ++i) {
				var formulaToken:FormulaToken = tokens[i];
				var token:MathToken = formulaToken.token;
				var type:int = token.type;
				var flags:uint = 0;
				
				if (type == MathToken.OPERATOR) {
					if ((token as OperatorToken).operator == MathConst.OP_SUBTRACT) {
						// The token is a subtraction operator token. If the operator is determined to be a unary minus (negation function), set the UNARY_MINUS flag.
						var isUnaryMinus:Boolean = i == 0;
						if (!isUnaryMinus) {
							token = tokens[i - 1].token;
							type = token.type;
							if (type == MathToken.NUMBER_CHAR)
								isUnaryMinus = (token as NumberCharToken).value == 'e';
							else if (type == MathToken.OPERATOR)
								isUnaryMinus = !mathOperator.getOpInfoByOperator((token as OperatorToken).operator).isPostfix;
							else if ((type == MathToken.FUN_JUXTA) || (type == MathToken.LEFT_PAREN) || (type == MathToken.LIST_SEP) || (type == MathToken.OP_ASSIGN))
								isUnaryMinus = true;
						}
						
						if (isUnaryMinus)
							flags |= FormulaToken.UNARY_MINUS;
					}
				}
				else if (type == MathToken.FUN_JUXTA) {
					// The token is a function call token with a juxtaposed argument. If it's followed by a left parenthesis token, set the FUN_JUXTA_PAREN flag.
					if ((i < length - 1) && (tokens[i + 1].token.type == MathToken.LEFT_PAREN))
						flags |= FormulaToken.FUN_JUXTA_PAREN;
				}
				
				formulaToken.flags = flags;
			}
		}
		
		public function matchBrackets(tokens:Vector.<FormulaToken>, index:int):void {
			var length:int = tokens.length;
			var mask:uint = uint(~FormulaToken.MATCHING_BRACKET & ~FormulaToken.MISMATCHED_BRACKET);
			for (var i:int = 0; i < length; ++i)
				tokens[i].flags &= mask;
			
			if (index >= 0) {
				var match:Boolean;
				//*
				// If there are brackets on both sides of the caret, pick the bracket on the left.
				if (--index >= 0)
					match = matchBracketAt(tokens, tokens[index].token.type, index);
				if (!match && (++index < length))
					matchBracketAt(tokens, tokens[index].token.type, index);
				/*/
				// If there are brackets on both sides of the caret, pick the bracket on the right.
				if (index < length)
					match = matchBracketAt(tokens, tokens[index].token.type, index);
				if (!match && (--index >= 0))
					matchBracketAt(tokens, tokens[index].token.type, index);
				//*/
			}
		}
		
		protected function matchBracketAt(tokens:Vector.<FormulaToken>, type:int, index:int):Boolean {
			if ((type == MathToken.LEFT_PAREN) || (type == MathToken.LEFT_BRACKET) || (type == MathToken.LEFT_CURLY))
				matchLeftBracketAt(tokens, type, index);
			else if ((type == MathToken.RIGHT_PAREN) || (type == MathToken.RIGHT_BRACKET) || (type == MathToken.RIGHT_CURLY))
				matchRightBracketAt(tokens, type, index);
			else
				return false;
			return true;
		}
		
		protected function matchLeftBracketAt(tokens:Vector.<FormulaToken>, type:int, index:int):Boolean {
			var matchType:int = (type == MathToken.LEFT_PAREN) ? MathToken.RIGHT_PAREN : (type == MathToken.LEFT_BRACKET) ? MathToken.RIGHT_BRACKET : MathToken.RIGHT_CURLY;
			var level:int = 1;
			var length:int = tokens.length;
			for (var i:int = index + 1; i < length; ++i) {
				var tokenType:int = tokens[i].token.type;
				if (tokenType == type)
					++level;
				else if (tokenType == matchType)
					--level;
				if (!level) {
					tokens[index].flags |= FormulaToken.MATCHING_BRACKET;
					tokens[i].flags |= FormulaToken.MATCHING_BRACKET;
					return true;
				}
			}
			tokens[index].flags |= FormulaToken.MISMATCHED_BRACKET;
			return false;
		}
		
		protected function matchRightBracketAt(tokens:Vector.<FormulaToken>, type:int, index:int):Boolean {
			var matchType:int = (type == MathToken.RIGHT_PAREN) ? MathToken.LEFT_PAREN : (type == MathToken.RIGHT_BRACKET) ? MathToken.LEFT_BRACKET : MathToken.LEFT_CURLY;
			var level:int = 1;
			var length:int = tokens.length;
			for (var i:int = index - 1; i >= 0; --i) {
				var tokenType:int = tokens[i].token.type;
				if (tokenType == type)
					++level;
				else if (tokenType == matchType)
					--level;
				if (!level) {
					tokens[index].flags |= FormulaToken.MATCHING_BRACKET;
					tokens[i].flags |= FormulaToken.MATCHING_BRACKET;
					return true;
				}
			}
			tokens[index].flags |= FormulaToken.MISMATCHED_BRACKET;
			return false;
		}
		
		protected var resultExpr:MathExpression = new MathExpression();
		
		public function checkSyntax(tokens:Vector.<FormulaToken>):MathExpression {
			if (!tokens.length)
				return MathExpression.fromValue(0, resultExpr);
			var result:MathExpression = _mathParser.parse(_mathTokenProvider.setSource(tokens), resultExpr);
			Logger.info(this, result);
			result.disposeRPN();
			return result;
		}
	}
}
