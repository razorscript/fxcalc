package com.razorscript.fxcalc.math {
	import com.razorscript.fxcalc.math.tokens.FormulaToken;
	import com.razorscript.fxcalc.math.tokens.IFormulaTokenFactory;
	import com.razorscript.math.expression.MathExpression;

	/**
	 * Formula. Contains an array of FormulaToken objects and additional properties.
	 *
	 * @author Gene Pavlovsky
	 */
	public class Formula {
		public function Formula(tokens:Vector.<FormulaToken> = null, result:MathExpression = null, isMutable:Boolean = true) {
			_tokens = tokens ? tokens : new Vector.<FormulaToken>();
			_result = result;
			_isMutable = isMutable;
		}

		protected var _tokens:Vector.<FormulaToken>;

		public function get tokens():Vector.<FormulaToken> {
			return _tokens;
		}

		public function set tokens(value:Vector.<FormulaToken>):void {
			if (!_isMutable)
				throw new Error('Illegal operation for an immutable object.');
			_tokens = value;
		}

		protected var _result:MathExpression;

		public function get result():MathExpression {
			return _result;
		}

		public function set result(value:MathExpression):void {
			if (!_isMutable)
				throw new Error('Illegal operation for an immutable object.');
			_result = value;
		}

		protected var _isMutable:Boolean;

		public function get isMutable():Boolean {
			return _isMutable;
		}

		/**
		 * Disposes of the tokens, empties the tokens vector and sets the result to null.
		 *
		 * @param tokenFactory The formula token factory to use for disposing tokens.
		 *
		 * @throws Error - if formula is immutable (the isMutable property is false).
		 */
		public function clear(tokenFactory:IFormulaTokenFactory):void {
			if (!_isMutable)
				throw new Error('Illegal operation for an immutable object.');
			for (var i:int = _tokens.length - 1; i >= 0; --i)
				tokenFactory.disposeToken(_tokens[i]);
			_tokens.length = 0;
			_result = null;
		}

		/**
		 * Disposes of the tokens, sets the properties to null.
		 *
		 * @param tokenFactory The formula token factory to use for disposing tokens.
		 */
		public function dispose(tokenFactory:IFormulaTokenFactory):void {
			for (var i:int = _tokens.length - 1; i >= 0; --i)
				tokenFactory.disposeToken(_tokens[i]);
			_tokens = null;
			_result = null;
			_isMutable = false;
		}

		public function cloneTokens(tokenFactory:IFormulaTokenFactory):Vector.<FormulaToken> {
			var length:int = _tokens.length;
			var vec:Vector.<FormulaToken> = new Vector.<FormulaToken>(length);
			for (var i:int = 0; i < length; ++i)
				vec[i] = tokenFactory.cloneToken(_tokens[i]);
			return vec;
		}

		public function clone(tokenFactory:IFormulaTokenFactory, isMutable:Boolean = true):Formula {
			var tokens:Vector.<FormulaToken> = cloneTokens(tokenFactory);
			if (!isMutable)
				tokens.fixed = true;
			return new Formula(tokens, _result ? _result.hasError ? MathExpression.fromError(_result.source, _result.errorType, _result.errorMessage, _result.errorIndex) : MathExpression.fromValue(_result.value) : null, isMutable);
		}
	}
}
