package com.razorscript.fxcalc.math.tokens {
	import com.razorscript.math.expression.tokens.IdentifierToken;
	import com.razorscript.math.expression.tokens.ValueToken;
	import com.razorscript.math.expression.tokens.MathToken;
	import com.razorscript.math.expression.tokens.MetaToken;
	import com.razorscript.math.expression.tokens.NumberCharToken;
	import com.razorscript.math.expression.tokens.OpAssignToken;
	import com.razorscript.math.expression.tokens.OperatorToken;
	
	/**
	 * Defines static constants for common shared math tokens.
	 *
	 * @author Gene Pavlovsky
	 */
	public class MathTokenCache {
		/**
		 * Returns a MathToken with the specified type. If found in the cache, returns the cached token, otherwise creates a new token.
		 */
		public static function getMathToken(type:int):MathToken {
			var key:String = type.toString();
			return (key in tokens) ? tokens[key] : (tokens[key] = new MathToken(type));
		}
		
		/**
		 * Returns a ValueToken with the specified value. If found in the cache, returns the cached token, otherwise creates a new token.
		 */
		public static function getValueToken(value:*):ValueToken {
			var key:String = MathToken.VALUE.toString() + value.toString();
			return (key in tokens) ? tokens[key] : (tokens[key] = new ValueToken(value));
		}
		
		protected static const NUMBER_CHAR_RX:RegExp = /^[0-9.e]$/;
		
		/**
		 * Returns a NumberCharToken with the specified value. If found in the cache, returns the cached token, otherwise creates a new token.
		 */
		public static function getNumberCharToken(value:String):NumberCharToken {
			if (!NUMBER_CHAR_RX.test(value))
				throw new RangeError('The value argument (' + value + ') is outside the set {"0".."9", ".", "e"}.');
			var key:String = MathToken.NUMBER_CHAR.toString() + value;
			return (key in tokens) ? tokens[key] : (tokens[key] = new NumberCharToken(value));
		}
		
		/**
		 * Returns an IdentifierToken with the specified type and identifier name. If found in the cache, returns the cached token, otherwise creates a new token.
		 */
		public static function getIdentifierToken(type:int, name:String):IdentifierToken {
			if ((type != MathToken.VARIABLE) && (type != MathToken.CONSTANT))
				throw new RangeError('The type argument (' + type + ') is outside the set {MathToken.VARIABLE, MathToken.CONSTANT}.');
			var key:String = type.toString() + name;
			return (key in tokens) ? tokens[key] : (tokens[key] = new IdentifierToken(type, name));
		}
		
		/**
		 * Returns an OperatorToken with the specified type and operator/function value. If found in the cache, returns the cached token, otherwise creates a new token.
		 */
		public static function getOperatorToken(type:int, operator:String):OperatorToken {
			if ((type != MathToken.OPERATOR) && (type != MathToken.FUN_JUXTA) && (type != MathToken.FUN_PAREN))
				throw new RangeError('The type argument (' + type + ') is outside the set {MathToken.OPERATOR, MathToken.FUN_JUXTA, MathToken.FUN_PAREN}.');
			var key:String = type.toString() + operator;
			return (key in tokens) ? tokens[key] : (tokens[key] = new OperatorToken(type, operator));
		}
		
		/**
		 * Returns an OpAssignToken with the specified operator value and variable name. If found in the cache, returns the cached token, otherwise creates a new token.
		 */
		public static function getOpAssignToken(operator:String, varName:String):OpAssignToken {
			var key:String = MathToken.OP_ASSIGN.toString() + operator + varName;
			return (key in tokens) ? tokens[key] : (tokens[key] = new OpAssignToken(operator, varName));
		}
		
		/**
		 * Returns a MetaToken with the specified value. If found in the cache, returns the cached token, otherwise creates a new token. The MetaToken's data property is not supported.
		 */
		public static function getMetaToken(type:int, value:String):MetaToken {
			if ((type != MathToken.BLANK) && (type != MathToken.ERROR))
				throw new RangeError('The type argument (' + type + ') is outside the set {MathToken.BLANK, MathToken.ERROR}.');
			var key:String = type.toString() + value;
			return (key in tokens) ? tokens[key] : (tokens[key] = new MetaToken(type, value));
		}
		
		/**
		 * Returns the number of tokens in the cache.
		 */
		public static function get length():int {
			var numTokens:int = 0;
			for each (var item:String in tokens)
				++numTokens;
			return numTokens;
		}
		
		/**
		 * Stores cached MathToken objects.
		 */
		protected static const tokens:Object = {};
	}
}
