package com.razorscript.fxcalc.math.tokens {
	import com.razorscript.math.expression.tokens.MathToken;
	
	/**
	 * A formula token factory that creates new tokens.
	 *
	 * @author Gene Pavlovsky
	 */
	public class FormulaTokenFactory implements IFormulaTokenFactory {
		public function FormulaTokenFactory() {
		}
		
		public function createFormulaToken(token:MathToken = null, flags:uint = 0):FormulaToken {
			return new FormulaToken(token, flags);
		}
		
		public function cloneToken(token:FormulaToken):FormulaToken {
			return token.clone();
		}
		
		public function disposeToken(token:FormulaToken):void {
		}
	}
}
