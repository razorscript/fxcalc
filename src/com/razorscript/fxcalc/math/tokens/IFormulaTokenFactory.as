package com.razorscript.fxcalc.math.tokens {
	import com.razorscript.math.expression.tokens.MathToken;
	
	/**
	 * A formula token factory.
	 * Creates new tokens.
	 *
	 * @author Gene Pavlovsky
	 */
	public interface IFormulaTokenFactory {
		function createFormulaToken(token:MathToken = null, flags:uint = 0):FormulaToken;
		function cloneToken(token:FormulaToken):FormulaToken;
		function disposeToken(token:FormulaToken):void;
	}
}
