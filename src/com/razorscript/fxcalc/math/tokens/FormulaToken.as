package com.razorscript.fxcalc.math.tokens {
	import com.razorscript.math.expression.tokens.MathToken;
	
	/**
	 * Formula token. Contains a MathToken and additional properties affecting the way FormulaTokenRenderer displays the token.
	 *
	 * @author Gene Pavlovsky
	 */
	public class FormulaToken {
		/** This flag is set if the token is a subtraction operator token, and the operator is determined to be a unary minus (negation function). */
		public static const UNARY_MINUS:uint = 1 << uid++;
		/** This flag is set if the token is a function call token with a juxtaposed argument, followed by a left parenthesis token. */
		public static const FUN_JUXTA_PAREN:uint = 1 << uid++;
		/** This flag is set if the token is a member of a pair of matching brackets, one of which is near the caret. */
		public static const MATCHING_BRACKET:uint = 1 << uid++;
		/** This flag is set if the token is a mismatched bracket near the caret. */
		public static const MISMATCHED_BRACKET:uint = 1 << uid++;
		
		protected static var uid:int = 0;
		
		public function FormulaToken(token:MathToken = null, flags:uint = 0) {
			this.token = token;
			this.flags = flags;
		}
		
		public var token:MathToken;
		public var flags:uint;
		
		public function setTo(token:MathToken = null, flags:uint = 0):FormulaToken {
			this.token = token;
			this.flags = flags;
			return this;
		}
		
		public function clone():FormulaToken {
			return new FormulaToken(token, flags);
		}
		
		public function toString():String {
			return '[FormulaToken token=' + token + ' flags=' + flags + ']';
		}
	}
}
