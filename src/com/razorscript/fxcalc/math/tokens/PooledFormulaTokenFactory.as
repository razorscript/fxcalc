package com.razorscript.fxcalc.math.tokens {
	import com.razorscript.fxcalc.debug.FXDebug;
	import com.razorscript.fxcalc.math.tokens.FormulaToken;
	import com.razorscript.fxcalc.math.tokens.IFormulaTokenFactory;
	import com.razorscript.math.expression.tokens.MathToken;
	
	/**
	 * A formula token factory that uses token pooling.
	 * If the pool is empty, a new token is created, otherwise a token is taken from the pool.
	 * Disposed tokens are returned to the pool.
	 *
	 * @author Gene Pavlovsky
	 */
	public class PooledFormulaTokenFactory implements IFormulaTokenFactory {
		public function PooledFormulaTokenFactory() {
			pool = new Vector.<FormulaToken>();
		}
		
		public function createFormulaToken(token:MathToken = null, flags:uint = 0):FormulaToken {
			if (pool.length)
				return pool.pop().setTo(token, flags);
			return new FormulaToken(token, flags);
		}
		
		public function cloneToken(token:FormulaToken):FormulaToken {
			return createFormulaToken(token.token, token.flags);
		}
		
		CONFIG::release
		public function disposeToken(token:FormulaToken):void {
			pool.push(token);
		}
		
		CONFIG::debug
		public function disposeToken(token:FormulaToken):void {
			if (pool.indexOf(token) != -1)
				throw new Error('Token (' + token + ') is already in the pool.');
			pool.push(token);
		}
		
		protected var pool:Vector.<FormulaToken>;
	}
}
