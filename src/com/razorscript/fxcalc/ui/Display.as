package com.razorscript.fxcalc.ui {
	import com.razorscript.fxcalc.controllers.DisplayController;
	import com.razorscript.fxcalc.controllers.PopUpController;
	import com.razorscript.fxcalc.data.Model;
	import com.razorscript.fxcalc.data.SettingsModel;
	import feathers.controls.LayoutGroup;
	import feathers.skins.IStyleProvider;
	
	/**
	 * Displays the status bar, formula display and result display.
	 *
	 * @author Gene Pavlovsky
	 */
	public class Display extends LayoutGroup {
		public static var globalStyleProvider:IStyleProvider;
		override protected function get defaultStyleProvider():IStyleProvider {
			return globalStyleProvider;
		}
		
		public var statusBar:StatusBar;
		public var formulaEditor:FormulaEditor;
		public var resultDisplay:ResultDisplay;
		
		public function Display(model:Model, viewModel:ViewModel, settingsModel:SettingsModel, controller:DisplayController, popUpController:PopUpController) {
			super();
			this.model = model;
			this.viewModel = viewModel;
			this.settingsModel = settingsModel;
			this.controller = controller;
			this.popUpController = popUpController;
		}
		
		override protected function initialize():void {
			super.initialize();
			
			statusBar = new StatusBar(model, viewModel);
			formulaEditor = new FormulaEditor(model, viewModel, controller, popUpController);
			resultDisplay = new ResultDisplay(model, viewModel);
			addChild(statusBar);
			addChild(formulaEditor);
			addChild(resultDisplay);
		}
		
		private var model:Model;
		private var viewModel:ViewModel;
		private var settingsModel:SettingsModel;
		private var controller:DisplayController;
		private var popUpController:PopUpController;
	}
}
