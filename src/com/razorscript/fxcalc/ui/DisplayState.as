package com.razorscript.fxcalc.ui {
	import com.razorscript.utils.enum.EnumBase;
	import com.razorscript.utils.enum.EnumUtil;
	
	/**
	 * Defines constants for display states.
	 *
	 * @author Gene Pavlovsky
	 */
	public class DisplayState extends EnumBase {
		/** Ready for input, displaying ViewModel.nullResult. */
		[Enum]
		public static const INPUT:DisplayState = factory();
		
		/** Displaying a result. */
		[Enum]
		public static const RESULT:DisplayState = factory();
		
		/** Displaying an error. */
		[Enum]
		public static const ERROR:DisplayState = factory();
		
		protected static function factory():DisplayState {
			return new DisplayState(LOCK);
		}
		
		/**
		 * @private
		 */
		public function DisplayState(lock:Object) {
			super(lock);
		}
		
		EnumUtil.registerClass(DisplayState);
	}
}
