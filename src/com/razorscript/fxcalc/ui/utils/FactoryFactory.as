package com.razorscript.fxcalc.ui.utils {
	import feathers.controls.Alert;
	import feathers.controls.TextCallout;
	import feathers.core.IFeathersControl;
	
	/**
	 * Utility functions to create factory functions for Feathers UI components.
	 *
	 * @author Gene Pavlovsky
	 */
	public class FactoryFactory {
		public static function createAlertFactory(styleName:String):Function {
			return createFactory(Alert, styleName);
		}
		
		public static function createTextCalloutFactory(styleName:String):Function {
			return createFactory(TextCallout, styleName);
		}
		
		private static function createFactory(controlClass:Class, styleName:String):Function {
			return function ():IFeathersControl {
				var control:IFeathersControl = new controlClass();
				control.styleNameList.add(styleName);
				return control;
			}
		}
	}
}
