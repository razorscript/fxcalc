package com.razorscript.fxcalc.ui.themes {
	import com.razorscript.feathers.themes.RazorThemeBase;
	import com.razorscript.fxcalc.Main;
	import com.razorscript.fxcalc.ui.Display;
	import com.razorscript.fxcalc.ui.FormulaEditor;
	import com.razorscript.fxcalc.ui.Keypad;
	import com.razorscript.fxcalc.ui.ResultDisplay;
	import com.razorscript.fxcalc.ui.StatusBar;
	import com.razorscript.fxcalc.ui.controls.Caret;
	import com.razorscript.fxcalc.ui.controls.FormulaRenderer;
	import com.razorscript.fxcalc.ui.controls.renderers.FormulaTokenListItemRenderer;
	import com.razorscript.fxcalc.ui.popups.ChangeLogAlertController;
	import com.razorscript.fxcalc.ui.popups.FormulaErrorCalloutController;
	import com.razorscript.utils.Lambda;
	import feathers.controls.Alert;
	import feathers.controls.AutoSizeMode;
	import feathers.controls.Button;
	import feathers.controls.ButtonState;
	import feathers.controls.Callout;
	import feathers.controls.DecelerationRate;
	import feathers.controls.Label;
	import feathers.controls.LayoutGroup;
	import feathers.controls.Radio;
	import feathers.controls.ScrollBarDisplayMode;
	import feathers.controls.ScrollPolicy;
	import feathers.controls.SimpleScrollBar;
	import feathers.controls.TextCallout;
	import feathers.controls.ToggleButton;
	import feathers.layout.AnchorLayout;
	import feathers.layout.AnchorLayoutData;
	import feathers.layout.Direction;
	import feathers.layout.HorizontalAlign;
	import feathers.layout.HorizontalLayout;
	import feathers.layout.HorizontalLayoutData;
	import feathers.layout.RelativePosition;
	import feathers.layout.VerticalAlign;
	import feathers.layout.VerticalLayout;
	import feathers.layout.VerticalLayoutData;
	import feathers.skins.StyleNameFunctionStyleProvider;
	import flash.text.engine.FontDescription;
	import starling.display.Quad;

	/**
	 * Base class for the Razor theme for FXCalc mobile app.
	 * Handles everything except asset loading, which is left to subclasses.
	 *
	 * @see com.razorscript.fxcalc.themes.FXCalcRazorTheme
	 *
	 * @author Gene Pavlovsky
	 */
	public class FXCalcRazorThemeBase extends RazorThemeBase {
		// TODO: Auto-generate FXCalcRazorThemeColor and FXCalcRazorThemeStyle from JSON.
		protected static const COLOR_MAIN_BACKGROUND:String = 'main-background';
		protected static const COLOR_STATUS_BAR_LABEL:String = 'status-bar-label';
		protected static const COLOR_FORMULA_EDITOR_SCROLL_BAR_THUMB:String = 'formula-editor-scroll-bar-thumb';
		protected static const COLOR_KEYPAD_MAIN_BUTTON_STATES:String = 'keypad-main-button-states';
		protected static const COLOR_KEYPAD_DEL_BUTTON_STATES:String = 'keypad-del-button-states';
		protected static const COLOR_KEYPAD_FUN_BUTTON_STATES:String = 'keypad-fun-button-states';
		protected static const COLOR_FORMULA_TOKEN_RENDERER_NUMBER:String = 'formula-token-renderer-number';
		protected static const COLOR_FORMULA_TOKEN_RENDERER_VARIABLE:String = 'formula-token-renderer-variable';
		protected static const COLOR_FORMULA_TOKEN_RENDERER_CONSTANT:String = 'formula-token-renderer-constant';
		protected static const COLOR_FORMULA_TOKEN_RENDERER_OPERATOR:String = 'formula-token-renderer-operator';
		protected static const COLOR_FORMULA_TOKEN_RENDERER_OP_ASSIGN:String = 'formula-token-renderer-operator-assign';
		protected static const COLOR_FORMULA_TOKEN_RENDERER_FUNCTION:String = 'formula-token-renderer-function';
		protected static const COLOR_FORMULA_TOKEN_RENDERER_MATCHING_BRACKET:String = 'formula-token-renderer-bracket-matching';
		protected static const COLOR_FORMULA_TOKEN_RENDERER_MISMATCHED_BRACKET:String = 'formula-token-renderer-bracket-mismatched';
		protected static const COLOR_FORMULA_TOKEN_RENDERER_SPECIAL:String = 'formula-token-renderer-special';
		protected static const COLOR_FORMULA_TOKEN_RENDERER_BLANK:String = 'formula-token-renderer-blank';
		protected static const COLOR_FORMULA_TOKEN_RENDERER_ERROR:String = 'formula-token-renderer-error';

		protected static const STYLE_KEYPAD_FUN_MAIN_HEIGHT_RATIO:String = 'keypad-fun-main-height-ratio';
		protected static const STYLE_KEYPAD_FUN_BUTTON_FONT_SIZE:String = 'keypad-fun-button-font-size-sub';

		/**
		 * Constructor.
		 *
		 * @param styleName Style name.
		 * @param colorSchemeName Color scheme name.
		 * @param scaleToDPI Whether the theme's skins should be scaled based on the screen density and content scale factor.
		 */
		public function FXCalcRazorThemeBase(styleName:String = null, colorSchemeName:String = null, scaleToDPI:Boolean = true) {
			super(styleName, colorSchemeName, scaleToDPI);
		}

		protected var mainPadding:int;
		protected var displayKeypadSpacing:int;
		protected var displayHorizontalPadding:int;
		protected var displayVerticalPadding:int;
		protected var displayGap:int;
		protected var formulaRendererGap:int;
		protected var formulaTokenRendererPadding:int;
		protected var formulaEditorScrollBarThumbThickness:int;
		protected var formulaEditorScrollBarPaddingBottom:int;
		protected var resultDisplayPaddingTop:int;
		protected var resultDisplayGap:int;
		protected var keypadMainFunSpacing:int;
		protected var keypadButtonHorizontalGap:int;
		protected var keypadButtonVerticalGap:int;
		protected var mouseWheelScrollStep:int;

		override protected function initDimensions():void {
			super.initDimensions();

			// TODO: Move these to an external JSON file?
			mainPadding = scaleValue(4);
			displayKeypadSpacing = scaleValue(12);
			displayHorizontalPadding = scaleValue(12);
			displayVerticalPadding = scaleValue(8);
			displayGap = scaleValue(4);
			formulaRendererGap = scaleValue(0);
			formulaTokenRendererPadding = scaleValue(8);
			formulaEditorScrollBarThumbThickness = scaleValue(4);
			formulaEditorScrollBarPaddingBottom = scaleValue(4);
			resultDisplayPaddingTop = scaleValue(4);
			resultDisplayGap = scaleValue(4);
			keypadMainFunSpacing = scaleValue(4);
			keypadButtonHorizontalGap = scaleValue(0);
			keypadButtonVerticalGap = scaleValue(0);
			mouseWheelScrollStep = scaleValue(16);
		}

		override protected function initStyleProviders():void {
			super.initStyleProviders();

			var styleProvider:StyleNameFunctionStyleProvider;

			getStyleProviderForClass(Main).defaultStyleFunction = setMainStyles;

			getStyleProviderForClass(Display).defaultStyleFunction = setDisplayStyles;

			getStyleProviderForClass(StatusBar).defaultStyleFunction = setStatusBarStyles;
			getStyleProviderForClass(Label).setFunctionForStyleName(StatusBar.CHILD_STYLE_NAME_STATUS_BAR_LABEL, setStatusBarLabelStyles);

			getStyleProviderForClass(FormulaEditor).defaultStyleFunction = setFormulaEditorStyles;
			getStyleProviderForClass(SimpleScrollBar).setFunctionForStyleName(FormulaEditor.CHILD_STYLE_NAME_FORMULA_EDITOR_SCROLL_BAR, setFormulaEditorScrollBarStyles);
			getStyleProviderForClass(Button).setFunctionForStyleName(FormulaEditor.CHILD_STYLE_NAME_FORMULA_EDITOR_SCROLL_BAR_THUMB, setFormulaEditorScrollBarThumbStyles);

			getStyleProviderForClass(FormulaRenderer).defaultStyleFunction = setFormulaRendererStyles;

			styleProvider = getStyleProviderForClass(FormulaTokenListItemRenderer);
			styleProvider.defaultStyleFunction = setFormulaTokenRendererStyles;
			styleProvider.setFunctionForStyleName(
				FormulaTokenListItemRenderer.SYNTAX_STYLE_NAME_NUMBER, Lambda.append1(setFormulaTokenRendererStyles, COLOR_FORMULA_TOKEN_RENDERER_NUMBER));
			styleProvider.setFunctionForStyleName(
				FormulaTokenListItemRenderer.SYNTAX_STYLE_NAME_VARIABLE, Lambda.append1(setFormulaTokenRendererStyles, COLOR_FORMULA_TOKEN_RENDERER_VARIABLE));
			styleProvider.setFunctionForStyleName(
				FormulaTokenListItemRenderer.SYNTAX_STYLE_NAME_CONSTANT, Lambda.append1(setFormulaTokenRendererStyles, COLOR_FORMULA_TOKEN_RENDERER_CONSTANT, italicFontDescription));
			styleProvider.setFunctionForStyleName(
				FormulaTokenListItemRenderer.SYNTAX_STYLE_NAME_OPERATOR, Lambda.append1(setFormulaTokenRendererStyles, COLOR_FORMULA_TOKEN_RENDERER_OPERATOR));
			styleProvider.setFunctionForStyleName(
				FormulaTokenListItemRenderer.SYNTAX_STYLE_NAME_OP_ASSIGN, Lambda.append1(setFormulaTokenRendererStyles, COLOR_FORMULA_TOKEN_RENDERER_OP_ASSIGN));
			styleProvider.setFunctionForStyleName(
				FormulaTokenListItemRenderer.SYNTAX_STYLE_NAME_FUNCTION, Lambda.append1(setFormulaTokenRendererStyles, COLOR_FORMULA_TOKEN_RENDERER_FUNCTION));
			styleProvider.setFunctionForStyleName(
				FormulaTokenListItemRenderer.SYNTAX_STYLE_NAME_MATCHING_BRACKET, Lambda.append1(setFormulaTokenRendererStyles, COLOR_FORMULA_TOKEN_RENDERER_MATCHING_BRACKET));
			styleProvider.setFunctionForStyleName(
				FormulaTokenListItemRenderer.SYNTAX_STYLE_NAME_MISMATCHED_BRACKET, Lambda.append1(setFormulaTokenRendererStyles, COLOR_FORMULA_TOKEN_RENDERER_MISMATCHED_BRACKET, boldFontDescription));
			styleProvider.setFunctionForStyleName(
				FormulaTokenListItemRenderer.SYNTAX_STYLE_NAME_SPECIAL, Lambda.append1(setFormulaTokenRendererStyles, COLOR_FORMULA_TOKEN_RENDERER_SPECIAL));
			styleProvider.setFunctionForStyleName(
				FormulaTokenListItemRenderer.SYNTAX_STYLE_NAME_BLANK, Lambda.append1(setFormulaTokenRendererStyles, COLOR_FORMULA_TOKEN_RENDERER_BLANK));
			styleProvider.setFunctionForStyleName(
				FormulaTokenListItemRenderer.SYNTAX_STYLE_NAME_ERROR, Lambda.append1(setFormulaTokenRendererStyles, COLOR_FORMULA_TOKEN_RENDERER_ERROR));

			getStyleProviderForClass(Caret).defaultStyleFunction = setCaretStyles;
			getStyleProviderForClass(Caret).setFunctionForStyleName(Caret.MODE_STYLE_NAME_INSERT, setCaretInsertModeStyles);
			getStyleProviderForClass(Caret).setFunctionForStyleName(Caret.MODE_STYLE_NAME_REPLACE, setCaretReplaceModeStyles);

			getStyleProviderForClass(ResultDisplay).defaultStyleFunction = setResultDisplayStyles;
			getStyleProviderForClass(Label).setFunctionForStyleName(ResultDisplay.CHILD_STYLE_NAME_RESULT_DISPLAY_DIGITS_LABEL, setResultDisplayDigitsLabelStyles);
			getStyleProviderForClass(Label).setFunctionForStyleName(ResultDisplay.CHILD_STYLE_NAME_RESULT_DISPLAY_X10_LABEL, setResultDisplayX10LabelStyles);
			getStyleProviderForClass(Label).setFunctionForStyleName(ResultDisplay.CHILD_STYLE_NAME_RESULT_DISPLAY_EXPONENT_LABEL, setResultDisplayExponentLabelStyles);

			getStyleProviderForClass(Keypad).defaultStyleFunction = setKeypadStyles;
			getStyleProviderForClass(LayoutGroup).setFunctionForStyleName(Keypad.CHILD_STYLE_NAME_KEYPAD_BUTTON_FUN_GRID, setKeypadButtonGridStyles);
			getStyleProviderForClass(LayoutGroup).setFunctionForStyleName(Keypad.CHILD_STYLE_NAME_KEYPAD_BUTTON_MAIN_GRID, setKeypadButtonGridStyles);
			getStyleProviderForClass(LayoutGroup).setFunctionForStyleName(Keypad.CHILD_STYLE_NAME_KEYPAD_BUTTON_FUN_ROW, setKeypadButtonRowStyles);
			getStyleProviderForClass(LayoutGroup).setFunctionForStyleName(Keypad.CHILD_STYLE_NAME_KEYPAD_BUTTON_MAIN_ROW, setKeypadButtonRowStyles);

			getStyleProviderForClass(Button).setFunctionForStyleName(
				Keypad.CHILD_STYLE_NAME_KEYPAD_MAIN_BUTTON, Lambda.append1(setKeypadButtonStyles, COLOR_KEYPAD_MAIN_BUTTON_STATES));
			getStyleProviderForClass(Button).setFunctionForStyleName(
				Keypad.CHILD_STYLE_NAME_KEYPAD_DEL_BUTTON, Lambda.append1(setKeypadButtonStyles, COLOR_KEYPAD_DEL_BUTTON_STATES));
			getStyleProviderForClass(Button).setFunctionForStyleName(
				Keypad.CHILD_STYLE_NAME_KEYPAD_FUN_BUTTON, setKeypadFunButtonStyles);
			getStyleProviderForClass(Button).setFunctionForStyleName(
				Keypad.CHILD_STYLE_NAME_KEYPAD_SMALLER_FONT_BUTTON, Lambda.append1(setKeypadSmallFontButtonStyles, 1));
			getStyleProviderForClass(Button).setFunctionForStyleName(
				Keypad.CHILD_STYLE_NAME_KEYPAD_SMALLEST_FONT_BUTTON, Lambda.append1(setKeypadSmallFontButtonStyles, 2));
			getStyleProviderForClass(Radio).setFunctionForStyleName(
				Keypad.CHILD_STYLE_NAME_KEYPAD_RADIO, setKeypadFunButtonStyles);
			getStyleProviderForClass(ToggleButton).setFunctionForStyleName(
				Keypad.CHILD_STYLE_NAME_KEYPAD_FUN_BUTTON, setKeypadFunButtonStyles);

			getStyleProviderForClass(Alert).setFunctionForStyleName(ChangeLogAlertController.ALERT_STYLE_NAME, setChangeLogAlertStyles);
			getStyleProviderForClass(TextCallout).setFunctionForStyleName(FormulaErrorCalloutController.CALLOUT_STYLE_NAME, setFormulaErrorTextCalloutStyles);

			CONFIG::debug {
				getStyleProviderForClass(Alert).setFunctionForStyleName(Keypad.DEBUG_LOGS_ALERT, setChangeLogAlertStyles);
			}
		}

		/***********************************************************************************************************************************************/

		/**
		 * Display styles.
		 */

		protected function setMainStyles(main:Main):void {
			main.backgroundSkin = new Quad(1, 1, getColor(COLOR_MAIN_BACKGROUND));
			main.autoSizeMode = AutoSizeMode.STAGE;
			main.layout = new AnchorLayout();
			main.display.layoutData = new AnchorLayoutData(0, mainPadding, NaN, mainPadding);
			var layoutData:AnchorLayoutData = new AnchorLayoutData(displayKeypadSpacing, mainPadding, mainPadding, mainPadding);
			layoutData.topAnchorDisplayObject = main.display;
			main.keypad.layoutData = layoutData;
		}

		/***********************************************************************************************************************************************/

		/**
		 * Display styles.
		 */

		protected function setDisplayStyles(display:Display):void {
			var layout:VerticalLayout = new VerticalLayout();
			layout.horizontalAlign = HorizontalAlign.JUSTIFY;
			layout.paddingLeft = layout.paddingRight = displayHorizontalPadding;
			layout.paddingTop = layout.paddingBottom = displayVerticalPadding;
			layout.gap = displayGap;
			display.layout = layout;
			//display.backgroundSkin = new Quad(1, 1, 0x3f6f00); // DEBUG
		}

		/***********************************************************************************************************************************************/

		/**
		 * StatusBar styles.
		 */

		protected function setStatusBarStyles(statusBar:StatusBar):void {
			statusBar.layout = new AnchorLayout();
			statusBar.leftLabel.layoutData = new AnchorLayoutData(NaN, NaN, NaN, 0);
			statusBar.rightLabel.layoutData = new AnchorLayoutData(NaN, 0, NaN, NaN);
			//statusBar.backgroundSkin = new Quad(1, 1, 0x4f0000); // DEBUG
		}

		protected function setStatusBarLabelStyles(label:Label):void {
			label.fontStyles = getTextFormat(tinyFontSize, COLOR_STATUS_BAR_LABEL);
		}

		/***********************************************************************************************************************************************/

		/**
		 * FormulaEditor styles.
		 */

		protected function setFormulaEditorStyles(container:FormulaEditor):void {
			// NOTE: FormulaEditor assumes zero paddings.
			setScrollContainerStyles(container);
			container.scrollBarDisplayMode = ScrollBarDisplayMode.FIXED_FLOAT;
			container.horizontalScrollPolicy = ScrollPolicy.AUTO;
			container.verticalScrollPolicy = ScrollPolicy.OFF;
			container.decelerationRate = DecelerationRate.FAST;
			container.verticalMouseWheelScrollDirection = Direction.HORIZONTAL;
			container.verticalMouseWheelScrollStep = mouseWheelScrollStep;
			container.customHorizontalScrollBarStyleName = FormulaEditor.CHILD_STYLE_NAME_FORMULA_EDITOR_SCROLL_BAR;
			//container.backgroundSkin = new Quad(1, 1, 0x004f00); // DEBUG
		}

		protected function setFormulaEditorScrollBarStyles(scrollBar:SimpleScrollBar):void {
			scrollBar.paddingBottom = formulaEditorScrollBarPaddingBottom;
			scrollBar.customThumbStyleName = FormulaEditor.CHILD_STYLE_NAME_FORMULA_EDITOR_SCROLL_BAR_THUMB;
		}

		protected function setFormulaEditorScrollBarThumbStyles(thumb:Button):void {
			var defaultSkin:Quad = new Quad(1, 1, getColor(COLOR_FORMULA_EDITOR_SCROLL_BAR_THUMB));
			defaultSkin.width = defaultSkin.height = formulaEditorScrollBarThumbThickness;
			thumb.defaultSkin = defaultSkin;
			thumb.hasLabelTextRenderer = false;
		}

		/***********************************************************************************************************************************************/

		/**
		 * FormulaRenderer styles.
		 */

		protected function setFormulaRendererStyles(formulaRenderer:FormulaRenderer):void {
			// NOTE: FormulaEditor assumes that FormulaRenderer has zero paddings.
			var layout:HorizontalLayout = new HorizontalLayout();
			layout.gap = formulaRendererGap;
			formulaRenderer.layout = layout;
			//formulaRenderer.backgroundSkin = new Quad(1, 1, 0x00004f); // DEBUG
		}

		/***********************************************************************************************************************************************/

		/**
		 * FormulaTokenRenderer styles.
		 */

		protected function setFormulaTokenRendererStyles(renderer:FormulaTokenListItemRenderer, fontColor:* = -1, fontDescription:FontDescription = null):void {
			renderer.fontStyles = getTextFormat(largeFontSize, (fontColor == -1) ? COLOR_DIM_TEXT : fontColor, fontDescription);
			renderer.paddingTop = renderer.paddingBottom = formulaTokenRendererPadding;
		}

		/***********************************************************************************************************************************************/

		/**
		 * Caret styles.
		 */

		protected function setCaretStyles(caret:Caret):void {
			caret.fontStyles = getTextFormat(largeFontSize, COLOR_TEXT);
			caret.paddingTop = caret.paddingBottom = formulaTokenRendererPadding;
			caret.blinkRate = 500;
		}

		protected function setCaretInsertModeStyles(caret:Caret):void {
			setCaretStyles(caret);
			caret.text = '▏'; // *[▏]* [▎] [▍] [␣]
		}

		protected function setCaretReplaceModeStyles(caret:Caret):void {
			setCaretStyles(caret);
			caret.text = '_'; // *[_]* [▁] [▂] [␣]
		}

		/***********************************************************************************************************************************************/

		/**
		 * ResultDisplay styles.
		 */

		protected function setResultDisplayStyles(resultDisplay:ResultDisplay):void {
			//resultDisplay.backgroundSkin = new Quad(1, 1, 0x4f4f00); // DEBUG
		}

		protected function setResultDisplayDigitsLabelStyles(label:Label):void {
			label.paddingTop = resultDisplayPaddingTop;
			label.fontStyles = getTextFormat(extraLargeFontSize, COLOR_TEXT);
			label.textRendererProperties.textAlign = HorizontalAlign.RIGHT;
		}

		protected function setResultDisplayX10LabelStyles(label:Label):void {
			label.paddingTop = resultDisplayPaddingTop;
			label.paddingLeft = label.paddingRight = resultDisplayGap;
			label.fontStyles = getTextFormat(regularFontSize, COLOR_TEXT);
			label.text = 'x10';
		}

		protected function setResultDisplayExponentLabelStyles(label:Label):void {
			label.paddingTop = resultDisplayPaddingTop;
			label.fontStyles = getTextFormat(largeFontSize, COLOR_TEXT);
			label.textRendererProperties.textAlign = HorizontalAlign.RIGHT;
		}

		/***********************************************************************************************************************************************/

		/**
		 * Keypad styles.
		 */

		protected function setKeypadStyles(keypad:Keypad):void {
			var layout:VerticalLayout = new VerticalLayout();
			layout.horizontalAlign = HorizontalAlign.JUSTIFY;
			layout.gap = keypadMainFunSpacing;
			keypad.layout = layout;
			var mainKeypadPercentHeight:Number = 100 / (getStyle(STYLE_KEYPAD_FUN_MAIN_HEIGHT_RATIO) + 1);
			keypad.funKeypad.layoutData = new VerticalLayoutData(NaN, 100 - mainKeypadPercentHeight);
			keypad.mainKeypad.layoutData = new VerticalLayoutData(NaN, mainKeypadPercentHeight);
			//keypad.funKeypad.backgroundSkin = new Quad(1, 1, 0x2f3f4f); // DEBUG
			//keypad.mainKeypad.backgroundSkin = new Quad(1, 1, 0x4f3f2f); // DEBUG
		}

		protected function setKeypadButtonGridStyles(grid:LayoutGroup):void {
			var layout:VerticalLayout = new VerticalLayout();
			layout.horizontalAlign = HorizontalAlign.JUSTIFY;
			layout.gap = keypadButtonVerticalGap;
			grid.layout = layout;
		}

		protected function setKeypadButtonRowStyles(row:LayoutGroup):void {
			row.layoutData = new VerticalLayoutData(100, 100);
			var layout:HorizontalLayout = new HorizontalLayout();
			layout.verticalAlign = VerticalAlign.JUSTIFY;
			layout.gap = keypadButtonHorizontalGap;
			row.layout = layout;
		}

		protected function setKeypadBaseButtonStyles(button:Button, fontSize:int):void {
			setBaseButtonStyles(button);
			button.layoutData = new HorizontalLayoutData(100, 100);
			button.fontStyles = getTextFormat(fontSize, COLOR_TEXT, boldFontDescription);
			button.disabledFontStyles = getTextFormat(fontSize, COLOR_DISABLED_TEXT, boldFontDescription);
			if (button is ToggleButton)
				(button as ToggleButton).selectedFontStyles = getTextFormat(fontSize, COLOR_SELECTED_TEXT, boldFontDescription);
		}

		protected function setKeypadButtonStyles(button:Button, colorName:String):void {
			setKeypadBaseButtonStyles(button, extraLargeFontSize);
			setColoredButtonStates(button, {up: getTexture(BTN_WHITE_SKIN)}, getColor(colorName));
		}

		protected function setKeypadFunButtonStyles(button:Button):void {
			setKeypadBaseButtonStyles(button, largeFontSize);
			// TODO: Refactor. @see http://forum.starling-framework.org/topic/how-to-overlay-textures-using-imageskin-with-togglebutton
			if (_colorSchemeName == 'solarized-dark') {
				setButtonSkins(button, {up: getTexture(_BTN_BASE02_SKIN), hover: getTexture(_BTN_BASE02_SKIN), down: getTexture(_BTN_BASE01_SKIN), disabled: getTexture(_BTN_BASE02_SKIN),
					selectedUp: getTexture(_BTN_BASE02_SEL_SKIN), selectedHover: getTexture(_BTN_BASE02_SEL_SKIN), down: getTexture(_BTN_BASE01_SEL_SKIN), disabled: getTexture(_BTN_BASE02_SEL_SKIN)});
			}
			else if (_colorSchemeName == 'solarized-light') {
				setButtonSkins(button, {up: getTexture(_BTN_BASE2_SKIN), hover: getTexture(_BTN_BASE2_SKIN), down: getTexture(_BTN_BASE1_SKIN), disabled: getTexture(_BTN_BASE2_SKIN),
					selectedUp: getTexture(_BTN_BASE2_SEL_SKIN), selectedHover: getTexture(_BTN_BASE2_SEL_SKIN), down: getTexture(_BTN_BASE1_SEL_SKIN), disabled: getTexture(_BTN_BASE2_SEL_SKIN)});
			}
			else {
				setColoredButtonStates(button, {up: getTexture(BTN_WHITE_SKIN)}, getColor(COLOR_KEYPAD_FUN_BUTTON_STATES));
			}
		}

		protected function setKeypadSmallFontButtonStyles(button:Button, smallFont:int):void {
			var fontSize:int = getFontSize(STYLE_KEYPAD_FUN_BUTTON_FONT_SIZE + smallFont);
			button.fontStyles = getTextFormat(fontSize, COLOR_TEXT, boldFontDescription);
			button.disabledFontStyles = getTextFormat(fontSize, COLOR_DISABLED_TEXT, boldFontDescription);
			if (button is ToggleButton)
				(button as ToggleButton).selectedFontStyles = getTextFormat(fontSize, COLOR_SELECTED_TEXT, boldFontDescription);
		}

		/***********************************************************************************************************************************************/

		/**
		 * ChangeLogAlert styles.
		 */

		protected function setChangeLogAlertStyles(alert:Alert):void {
			setAlertStyles(alert);
			alert.maxWidth = alert.stage.stageWidth - scaleValue(20);
			alert.maxHeight = alert.stage.stageHeight - scaleValue(20);
		}

		/***********************************************************************************************************************************************/

		/**
		 * FormulaErrorCallout styles.
		 */

		protected function setFormulaErrorTextCalloutStyles(callout:TextCallout):void {
			setTextCalloutStyles(callout);
			callout.supportedPositions = new <String>[RelativePosition.BOTTOM];
			callout.closeOnTouchBeganOutside = callout.closeOnTouchEndedOutside = false;
			callout.wordWrap = true;
		}
	}
}
