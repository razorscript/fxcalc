package com.razorscript.fxcalc.ui.themes {
	import com.razorscript.debug.Logger;
	import com.razorscript.events.RazorEventType;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;

	/**
	 * The Razor theme for FXCalc mobile app.
	 *
	 * This version of the theme embeds its assets.
	 *
	 * @author Gene Pavlovsky
	 */
	public class FXCalcRazorTheme extends FXCalcRazorThemeBase {
		// TODO: Use AssetManager, at least for styles and color schemes?
		[Embed(source="/../assets/images/razor.xml", mimeType="application/octet-stream")]
		protected static const ATLAS_XML:Class;
		[Embed(source="/../assets/images/razor.png")]
		protected static const ATLAS_BITMAP:Class;
		[Embed(source="/../assets/images/razor-metadata.min.json", mimeType="application/octet-stream")]
		protected static const ATLAS_METADATA_JSON:Class;

		//{ region Styles

		[Embed(source="/../assets/styles/base.min.json", mimeType="application/octet-stream")]
		protected static const BASE_STYLE_JSON:Class;

		[Embed(source="/../assets/styles/rounded.min.json", mimeType="application/octet-stream")]
		protected static const ROUNDED_STYLE_JSON:Class;

		protected const FALLBACK_STYLE_JSON:Class = ROUNDED_STYLE_JSON;

		protected const styleNameClasses:Object = {
			'rounded': ROUNDED_STYLE_JSON};

		//} endregion

		//{ region Color schemes

		[Embed(source="/../assets/colors/solarized-dark.min.json", mimeType="application/octet-stream")]
		protected static const SOLARIZED_DARK_COLOR_SCHEME_JSON:Class;
		[Embed(source="/../assets/colors/solarized-light.min.json", mimeType="application/octet-stream")]
		protected static const SOLARIZED_LIGHT_COLOR_SCHEME_JSON:Class;

		protected const FALLBACK_COLOR_SCHEME_JSON:Class = SOLARIZED_DARK_COLOR_SCHEME_JSON;

		protected const colorSchemeNameClasses:Object = {
			'solarized-dark': SOLARIZED_DARK_COLOR_SCHEME_JSON,
			'solarized-light': SOLARIZED_LIGHT_COLOR_SCHEME_JSON};

		//} endregion

		/**
		 * Constructor.
		 *
		 * @param styleName Style name.
		 * @param colorSchemeName Color scheme name.
		 * @param scaleToDPI Whether the theme's skins should be scaled based on the screen density and content scale factor.
		 */
		public function FXCalcRazorTheme(styleName:String = null, colorSchemeName:String = null, scaleToDPI:Boolean = true) {
			super(styleName, colorSchemeName, scaleToDPI);
		}

		override public function load():void {
			// TODO: Refactor load/initialize sequence. Everything async should be in load(), after all async operations finish, run initialize(), containing only sync code. Then dispatch LOAD event.
			initialize();
			_isLoaded = true;
			dispatchEventWith(RazorEventType.LOAD);
		}

		override protected function initialize():void {
			initScale();
			initTextureAtlas();
			initStyle();
			super.initialize();
		}

		protected function initTextureAtlas():void {
			var atlasBitmapData:BitmapData = Bitmap(new ATLAS_BITMAP()).bitmapData;
			var atlasTexture:Texture = Texture.fromBitmapData(atlasBitmapData, false, false, 1 / _scale); // TODO: Create 2x resolution textures and pass 2 as scale here
			atlasTexture.root.onRestore = onAtlastTextureRestore;
			atlasBitmapData.dispose();
			atlas = new TextureAtlas(atlasTexture, XML(new ATLAS_XML()));

			parseTextureMetadataJSON(new ATLAS_METADATA_JSON());
		}

		protected function onAtlastTextureRestore():void {
			var atlasBitmapData:BitmapData = Bitmap(new ATLAS_BITMAP()).bitmapData;
			atlas.texture.root.uploadBitmapData(atlasBitmapData);
			atlasBitmapData.dispose();
		}

		protected function initStyle():void {
			_baseStyle = JSON.parse(new BASE_STYLE_JSON());
		}

		override protected function validateStyle():void {
			var styleClass:Class = styleNameClasses[_styleName];
			if (!styleClass) {
				// TODO: Show error alert?
				Logger.error(this, 'Unknown style:', _styleName);
				if (isStyleInitialized)
					return;
				styleClass = FALLBACK_STYLE_JSON;
			}
			parseStyleJSON(new styleClass());
		}

		override protected function validateColorScheme():void {
			var colorSchemeClass:Class = colorSchemeNameClasses[_colorSchemeName];
			if (!colorSchemeClass) {
				// TODO: Show error alert?
				Logger.error(this, 'Unknown color scheme:', _colorSchemeName);
				if (isColorSchemeInitialized)
					return;
				colorSchemeClass = FALLBACK_COLOR_SCHEME_JSON;
			}
			parseColorSchemeJSON(new colorSchemeClass());
		}
	}
}
