package com.razorscript.fxcalc.ui {
	import com.razorscript.utils.enum.EnumBase;
	import com.razorscript.utils.enum.EnumUtil;
	
	// TODO: Move enum to a separate class (NumberFormatType), rename this class to NumberFormat - should contain type and number of digits.
	
	/**
	 * Defines constants for result display formats.
	 *
	 * @author Gene Pavlovsky
	 */
	public class ResultDisplayFormat extends EnumBase {
		/** Inside the range (1e-{digits} <= |x| < 1e{maxDigits}), display results in non-exponential format. Outside the range, display results in exponential format. */
		[Enum]
		public static const NORMAL_1:ResultDisplayFormat = factory(defaultDigitsNormal);
		
		/** Inside the range (1e-({maxDigits}-1) <= |x| < 1e{maxDigits}), display results in non-exponential format. Outside the range, display results in exponential format. */
		[Enum]
		public static const NORMAL_2:ResultDisplayFormat = factory(0);
		
		/** Round off to the specified number of decimal places. Example: 1.23456789 = 1.235 (FIXED 3) */
		[Enum]
		public static const FIXED:ResultDisplayFormat = factory(defaultDigitsFixed);
		
		/** Round off to the specified significant digits. Example 123456789 = 1.234568e08 (SCIENTIFIC 7) */
		[Enum]
		public static const SCIENTIFIC:ResultDisplayFormat = factory(defaultDigitsScientific);
		
		/** Round off to the specified significant digits and display results in engineering notation. Example 123456789 = 123.5e06 (ENGINEERING 4) */
		[Enum]
		public static const ENGINEERING:ResultDisplayFormat = factory(defaultDigitsEngineering);
		
		// TODO: Abolish NORMAL_2?
		// TODO: Engineering format should have configurable offset (coefficient range). Subclass with extra properties?
		// TODO: Engineering format should have an option to display SI prefixes instead of the exponent. Subclass with extra properties?
		
		// TODO: These should be in the settings model?
		public static var defaultValue:ResultDisplayFormat = NORMAL_1;
		public static var defaultDigitsNormal:uint = 3;
		public static var defaultDigitsFixed:uint = 3;
		public static var defaultDigitsScientific:uint = 7;
		public static var defaultDigitsEngineering:uint = 4;
		
		/** Depending on the format, means either the number of decimal places (digits after the decimal point), or the number of significant digits (precision). */
		public var digits:uint;
		
		protected static function factory(digits:uint = 0):ResultDisplayFormat {
			return new ResultDisplayFormat(LOCK, digits);
		}
		
		/**
		 * @private
		 */
		public function ResultDisplayFormat(lock:Object, digits:uint) {
			super(lock);
			this.digits = digits;
		}
		
		EnumUtil.registerClass(ResultDisplayFormat);
	}
}
