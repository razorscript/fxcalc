package com.razorscript.fxcalc.ui {
	import feathers.controls.Button;
	import feathers.controls.Label;
	import feathers.controls.LayoutGroup;
	import feathers.controls.text.TextBlockTextRenderer;
	import feathers.controls.text.TextFieldTextRenderer;
	import feathers.core.ITextRenderer;
	import feathers.events.FeathersEventType;
	import feathers.layout.AnchorLayout;
	import feathers.layout.AnchorLayoutData;
	import starling.events.Event;
	
	/**
	 * DEPRECATED
	 */
	throw new Error();
	
	/**
	 * A keypad button that has a primary function and an optional secondary (shift) function.
	 *
	 * @author Gene Pavlovsky
	 */
	public class KeypadButton extends LayoutGroup {
		private var shiftLabel:Label;
		private var button:Button;
		private var _actionText:String;
		private var _actionFun:Function;
		private var _shiftActionText:String;
		private var _shiftActionFun:Function;
		
		public function KeypadButton() {
			super();
		}
		
		override protected function initialize():void {
			super.initialize();
			
			//shiftLabel = new Label();
			button = new Button();
			layout = new AnchorLayout();
			//shiftLabel.layoutData = new AnchorLayoutData(4, 0, NaN, 0);
			button.layoutData = new AnchorLayoutData(NaN, 0, NaN, 0);
			//button.layoutData = new AnchorLayoutData(4, 2, 2, 2);
			//(button.layoutData as AnchorLayoutData).topAnchorDisplayObject = shiftLabel;
			//button.isLongPressEnabled = true;
			button.addEventListener(Event.TRIGGERED, onButtonTriggered);
			//button.addEventListener(FeathersEventType.LONG_PRESS, onButtonLongPress);
			//addChild(shiftLabel);
			addChild(button);
		}
		
		private function onButtonTriggered(e:Event):void {
			if (_actionFun)
				_actionFun();
		}
		
		private function onButtonLongPress(e:Event):void {
			if (_shiftActionFun)
				_shiftActionFun();
		}
		
		public function get actionText():String {
			return _actionText;
		}
		
		public function set actionText(value:String):void {
			button.label = _actionText = value;
		}
		
		public function get actionFun():Function {
			return _actionFun;
		}
		
		public function set actionFun(value:Function):void {
			_actionFun = value;
		}
		
		public function get shiftActionText():String {
			return _shiftActionText;
		}
		
		public function set shiftActionText(value:String):void {
			/*shiftLabel.text = */_shiftActionText = value;
		}
		
		public function get shiftActionFun():Function {
			return _shiftActionFun;
		}
		
		public function set shiftActionFun(value:Function):void {
			_shiftActionFun = value;
		}
	}
}
