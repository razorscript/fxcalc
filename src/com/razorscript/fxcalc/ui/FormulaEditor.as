package com.razorscript.fxcalc.ui {
	import com.razorscript.fxcalc.controllers.DisplayController;
	import com.razorscript.fxcalc.controllers.PopUpController;
	import com.razorscript.fxcalc.data.Model;
	import com.razorscript.fxcalc.events.ModelEventType;
	import com.razorscript.fxcalc.events.ViewModelEventType;
	import com.razorscript.fxcalc.ui.controls.Caret;
	import com.razorscript.fxcalc.ui.controls.FormulaRenderer;
	import com.razorscript.utils.Lambda;
	import feathers.controls.ScrollContainer;
	import feathers.events.FeathersEventType;
	import feathers.skins.IStyleProvider;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import starling.display.DisplayObject;
	import starling.events.EnterFrameEvent;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	
	/**
	 * Displays the formula and the caret.
	 * Drag to scroll, tap to move the caret.
	 *
	 * @author Gene Pavlovsky
	 */
	public class FormulaEditor extends ScrollContainer {
		public static const CHILD_STYLE_NAME_FORMULA_EDITOR_SCROLL_BAR:String = 'fxcalc-formula-editor-scrollbar';
		public static const CHILD_STYLE_NAME_FORMULA_EDITOR_SCROLL_BAR_THUMB:String = 'fxcalc-formula-editor-scrollbar-thumb';
		
		public static var globalStyleProvider:IStyleProvider;
		override protected function get defaultStyleProvider():IStyleProvider {
			return globalStyleProvider;
		}
		
		public function FormulaEditor(model:Model, viewModel:ViewModel, controller:DisplayController, popUpController:PopUpController) {
			super();
			this.model = model;
			this.viewModel = viewModel;
			this.controller = controller;
			this.popUpController = popUpController;
		}
		
		override protected function initialize():void {
			super.initialize();
			
			addEventListener(TouchEvent.TOUCH, onTouch);
			addEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);
			addEventListener(FeathersEventType.END_INTERACTION, onEndInteraction);
			addEventListener(FeathersEventType.SCROLL_COMPLETE, onScrollComplete);
			
			formulaRenderer = new FormulaRenderer();
			formulaRenderer.addEventListener(Event.TRIGGERED, onFormulaTokenTriggered);
			addChild(formulaRenderer);
			
			caret = new Caret();
			addChild(caret);
			
			var invalidateData:Function = Lambda.replace(invalidate, INVALIDATION_FLAG_DATA);
			model.addEventListener(ModelEventType.TOKENS_CHANGE, invalidateData);
			model.addEventListener(ModelEventType.TOKEN_FLAGS_CHANGE, invalidateData);
			viewModel.addEventListener(ViewModelEventType.IS_EDITABLE_CHANGE, onIsEditableChange);
			viewModel.addEventListener(ViewModelEventType.INSERT_MODE_CHANGE, onInsertModeChange);
			viewModel.addEventListener(ViewModelEventType.CARET_INDEX_CHANGE, Lambda.replace(invalidate, INVALIDATION_FLAG_CARET_INDEX));
			viewModel.addEventListener(ViewModelEventType.DISPLAY_STATE_CHANGE, onDisplayStateChange);
			
			onIsEditableChange();
			onInsertModeChange();
			onDisplayStateChange();
		}
		
		override protected function draw():void {
			var charWidth:int = formulaRenderer.characterWidth;
			if (width && charWidth) {
				// Adjust the padding to let the container hold a whole number of characters.
				var newPadding:Number = width - Math.floor(width / charWidth) * charWidth;
				if ((paddingLeft + paddingRight) != newPadding) {
					paddingLeft = Math.floor(newPadding / 2);
					paddingRight = newPadding - paddingLeft;
					invalidate(INVALIDATION_FLAG_SCROLL_POSITION);
				}
			}
			
			var isDataInvalid:Boolean = isInvalid(INVALIDATION_FLAG_DATA);
			var isErrorDisplayInvalid:Boolean = isInvalid(INVALIDATION_FLAG_ERROR_DISPLAY);
			var isCaretIndexInvalid:Boolean = isInvalid(INVALIDATION_FLAG_CARET_INDEX);
			var isScrollPositionInvalid:Boolean = isInvalid(INVALIDATION_FLAG_SCROLL_POSITION);
			
			super.draw();
			
			if (isDataInvalid)
				commitData();
			if (isErrorDisplayInvalid)
				updateErrorDisplay();
			if (isCaretIndexInvalid)
				updateCaretIndex();
			if (isScrollPositionInvalid)
				updateScrollPosition();
		}
		
		private static const INVALIDATION_FLAG_CARET_INDEX:String = 'caretIndex';
		private static const INVALIDATION_FLAG_SCROLL_POSITION:String = 'scrollPosition';
		private static const INVALIDATION_FLAG_ERROR_DISPLAY:String = 'errorDisplay';
		
		private static const POINT:Point = new Point();
		
		private var model:Model;
		private var viewModel:ViewModel;
		private var controller:DisplayController;
		private var popUpController:PopUpController;
		
		private var formulaRenderer:FormulaRenderer;
		private var caret:Caret;
		private var touchID:int = -1;
		
		/**
		 * Handles touching the (empty) area not occupied by FormulaRenderer. This area is to the right of FormulaRenderer.
		 * Moves the caret to (just beyond) the end of the formula.
		 */
		private function onTouch(e:TouchEvent):void {
			if (!_isEnabled) {
				touchID = -1;
				return;
			}
			
			if (touchID >= 0) {
				var touch:Touch = e.getTouch(this, null, touchID);
				if (touch && (touch.phase == TouchPhase.ENDED) && (touch.target == this)) {
					touch.getLocation(stage, POINT);
					var touchObject:DisplayObject = stage.hitTest(POINT);
					if (contains(touchObject) && !formulaRenderer.contains(touchObject))
						controller.moveCaret(viewModel.maxCaretIndex);
					touchID = -1;
				}
			}
			else {
				touch = e.getTouch(this, TouchPhase.BEGAN);
				if (touch && (touch.target == this))
					touchID = touch.id;
			}
		}
		
		private function onRemovedFromStage(e:Event):void {
			touchID = -1;
		}
		
		private function onEndInteraction(e:Event):void {
			// The user has stopped scrolling the container, but the scrolling might continue animating. Add an ENTER_FRAME handler to check.
			addEventListener(EnterFrameEvent.ENTER_FRAME, onEnterFrame);
		}
		
		private function onEnterFrame(e:EnterFrameEvent):void {
			if (_horizontalAutoScrollTween && _targetHorizontalScrollPosition) {
				// An auto scroll tween has been created, round the target scroll position to the nearest character width.
				removeEventListener(EnterFrameEvent.ENTER_FRAME, onEnterFrame);
				var charWidth:int = formulaRenderer.characterWidth;
				if (!charWidth)
					return;
				var scrollX:Number = Math.min(Math.round(_targetHorizontalScrollPosition / charWidth) * charWidth, maxHorizontalScrollPosition);
				if (scrollX != _targetHorizontalScrollPosition) {
					_targetHorizontalScrollPosition = scrollX;
					_horizontalAutoScrollTween.animate('horizontalScrollPosition', scrollX);
				}
			}
		}
		
		private function onScrollComplete(e:Event):void {
			// A scroll has completed, round the scroll position to the nearest character width.
			removeEventListener(EnterFrameEvent.ENTER_FRAME, onEnterFrame);
			var charWidth:int = formulaRenderer.characterWidth;
			if (!charWidth)
				return;
			var scrollX:Number = Math.min(Math.round(horizontalScrollPosition / charWidth) * charWidth, maxHorizontalScrollPosition);
			if (scrollX != horizontalScrollPosition)
				horizontalScrollPosition = scrollX;
		}
		
		private function onFormulaTokenTriggered(e:Event, index:int):void {
			controller.moveCaret(index);
		}
		
		private function onIsEditableChange():void {
			caret.visible = viewModel.isEditable;
			if (caret.visible)
				invalidate(INVALIDATION_FLAG_CARET_INDEX);
		}
		
		private function onInsertModeChange():void {
			caret.insertMode = viewModel.insertMode;
		}
		
		private function onDisplayStateChange():void {
			if (viewModel.displayState == DisplayState.ERROR)
				invalidate(INVALIDATION_FLAG_ERROR_DISPLAY);
		}
		
		private function commitData():void {
			stopScrolling();
			formulaRenderer.data = model.tokens;
		}
		
		private function updateErrorDisplay():void {
			if (viewModel.displayState == DisplayState.ERROR) {
				popUpController.showFormulaError(getErrorOrigin());
				invalidate(INVALIDATION_FLAG_SCROLL_POSITION);
			}
		}
		
		private function getErrorOrigin():DisplayObject {
			var origin:DisplayObject = (viewModel.result.errorIndex >= 0) ? formulaRenderer.getRendererAt(viewModel.result.errorIndex) : null;
			return origin ? origin : formulaRenderer;
		}
		
		private function updateCaretIndex():void {
			if (!caret.visible && (viewModel.caretIndex != 0) && (viewModel.caretIndex != viewModel.maxCaretIndex))
				return;
			formulaRenderer.validate();
			var bounds:Rectangle = formulaRenderer.getTokenBounds(viewModel.caretIndex);
			if (bounds)
				caret.x = bounds.x; // NOTE: Assuming there are zero paddings.
			caret.updateBlink();
			invalidate(INVALIDATION_FLAG_SCROLL_POSITION);
		}
		
		// TODO: Debug scrolling / padding. Sometimes a character is cut off in the middle, this is not supposed to happen.
		private function updateScrollPosition():void {
			var target:DisplayObject;
			if (viewModel.displayState == DisplayState.ERROR) {
				target = getErrorOrigin();
			}
			else {
				if (!caret.visible && (viewModel.caretIndex != 0) && (viewModel.caretIndex != viewModel.maxCaretIndex))
					return;
				target = formulaRenderer.getRendererAt(viewModel.caretIndex);
				if (!target)
					target = caret;
			}
			var scrollX:Number;
			var charWidth:int = formulaRenderer.characterWidth;
			var targetWidth:int = Math.ceil(target.width);
			//Logger.debug(this, 'updateScrollPosition()', target.x, targetWidth, width, horizontalScrollPosition, maxHorizontalScrollPosition);
			 // NOTE: Assuming there are zero paddings.
			if (target.x < horizontalScrollPosition) {
				scrollX = target.x;
				if (charWidth)
					scrollX = Math.floor(scrollX / charWidth) * charWidth;
				horizontalScrollPosition = scrollX;
			}
			else if (target.x + targetWidth > horizontalScrollPosition + width) {
				scrollX = Math.round(target.x + targetWidth - width);
				if (charWidth)
					scrollX = Math.ceil(scrollX / charWidth) * charWidth;
				horizontalScrollPosition = Math.min(scrollX, maxHorizontalScrollPosition);
			}
 		}
	}
}
