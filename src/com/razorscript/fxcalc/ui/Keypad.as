package com.razorscript.fxcalc.ui {
	import com.razorscript.debug.Logger;
	import com.razorscript.feathers.themes.ThemeManager;
	import com.razorscript.fxcalc.controllers.KeypadController;
	import com.razorscript.fxcalc.data.Model;
	import com.razorscript.fxcalc.data.SettingsModel;
	import com.razorscript.fxcalc.data.constants.MathConst;
	import com.razorscript.fxcalc.events.ViewModelEventType;
	import com.razorscript.fxcalc.math.tokens.MathTokenCache;
	import com.razorscript.fxcalc.ui.utils.FactoryFactory;
	import com.razorscript.math.AngleUnit;
	import com.razorscript.math.expression.MathFun;
	import com.razorscript.math.expression.MathOperator;
	import com.razorscript.math.expression.tokens.MathToken;
	import com.razorscript.utils.Lambda;
	import feathers.controls.Alert;
	import feathers.controls.Button;
	import feathers.controls.Callout;
	import feathers.controls.Label;
	import feathers.controls.LayoutGroup;
	import feathers.controls.Radio;
	import feathers.controls.Slider;
	import feathers.controls.ToggleButton;
	import feathers.core.ToggleGroup;
	import feathers.data.ListCollection;
	import feathers.events.FeathersEventType;
	import feathers.layout.Direction;
	import feathers.layout.HorizontalAlign;
	import feathers.layout.RelativePosition;
	import feathers.layout.VerticalLayout;
	import feathers.skins.IStyleProvider;
	import flash.ui.Keyboard;
	import flash.utils.getTimer;
	import starling.events.Event;
	import starling.events.KeyboardEvent;
	
	/**
	 * Calculator buttons keypad.
	 *
	 * @author Gene Pavlovsky
	 */
	public class Keypad extends LayoutGroup {
		public static const CHILD_STYLE_NAME_KEYPAD_BUTTON_FUN_GRID:String = 'fxcalc-keypad-button-fun-grid';
		public static const CHILD_STYLE_NAME_KEYPAD_BUTTON_MAIN_GRID:String = 'fxcalc-keypad-button-main-grid';
		public static const CHILD_STYLE_NAME_KEYPAD_BUTTON_FUN_ROW:String = 'fxcalc-keypad-button-fun-row';
		public static const CHILD_STYLE_NAME_KEYPAD_BUTTON_MAIN_ROW:String = 'fxcalc-keypad-button-main-row';
		public static const CHILD_STYLE_NAME_KEYPAD_MAIN_BUTTON:String = 'fxcalc-keypad-main-button';
		public static const CHILD_STYLE_NAME_KEYPAD_DEL_BUTTON:String = 'fxcalc-keypad-del-button';
		public static const CHILD_STYLE_NAME_KEYPAD_FUN_BUTTON:String = 'fxcalc-keypad-fun-button';
		public static const CHILD_STYLE_NAME_KEYPAD_SMALLER_FONT_BUTTON:String = 'fxcalc-keypad-smaller-font-button';
		public static const CHILD_STYLE_NAME_KEYPAD_SMALLEST_FONT_BUTTON:String = 'fxcalc-keypad-smallest-font-button';
		public static const CHILD_STYLE_NAME_KEYPAD_RADIO:String = 'fxcalc-keypad-radio';
		
		public static var globalStyleProvider:IStyleProvider;
		override protected function get defaultStyleProvider():IStyleProvider {
			return globalStyleProvider;
		}
		
		public var mainKeypad:LayoutGroup;
		public var funKeypad:LayoutGroup;
		
		public function Keypad(model:Model, viewModel:ViewModel, settingsModel:SettingsModel, controller:KeypadController) {
			super();
			this.model = model;
			this.viewModel = viewModel;
			this.settingsModel = settingsModel;
			this.controller = controller;
		}
		
		private var model:Model;
		private var viewModel:ViewModel;
		private var settingsModel:SettingsModel;
		private var controller:KeypadController;
		
		private var actions:Object;
		private var shortcuts:Object;
		
		private var defaultFunButtonLayout:Array/*Array*/;
		private var defaultMainButtonLayout:Array/*Array*/;
		private var funButtonLayout:Array/*Array*/;
		private var mainButtonLayout:Array/*Array*/;
		
		private var resultDisplayFormatGroup:ToggleGroup;
		private var resultDisplayFormatButtons:Object = {};
		private var insertModeButton:ToggleButton;
		private var inverseTrigModeButton:ToggleButton;
		private var hyperbolicTrigModeButton:ToggleButton;
		private var funSinButton:Button;
		private var funCosButton:Button;
		private var funTanButton:Button;
		private var moveCaretLeftButton:Button;
		private var moveCaretRightButton:Button;
		private var gotoHistoryPrevButton:Button;
		private var gotoHistoryNextButton:Button;
		
		override protected function initialize():void {
			super.initialize();
			
			createActions();
			createButtonLayout();
			createButtons();
			addEventListeners();
		}
		
		private static const CTRL_KEY:uint = 1 << 8;
		private static const ALT_KEY:uint = 1 << 9;
		private static const SHIFT_KEY:uint = 1 << 10;
		
		private function onKeyDown(e:KeyboardEvent):void {
			var key:uint = e.keyCode;
			if (e.ctrlKey)
				key |= CTRL_KEY;
			if (e.altKey)
				key |= ALT_KEY;
			if (e.shiftKey)
				key |= SHIFT_KEY;
			
			if (key in shortcuts)
				shortcuts[key]();
		}
		
		private function toggleInsertMode():void {
			controller.setInsertMode(!insertModeButton.isSelected);
		}
		
		private function toggleInverseTrigMode():void {
			controller.setInverseTrigMode(!inverseTrigModeButton.isSelected);
		}
		
		private function toggleHyperbolicTrigMode():void {
			controller.setHyperbolicTrigMode(!hyperbolicTrigModeButton.isSelected);
		}
		
		/* Returns an action which executes a function. */
		private function createFunAction(label:String, fun:Function = null, ... keys):Object {
			for each (var key:uint in keys)
				shortcuts[key] = fun;
			return {label: label, fun: fun};
		}
		
		/* Returns an action which adds a formula token. */
		private function createInputTokenAction(label:String, token:MathToken, ... keys):Object {
			var fun:Function = Lambda.replace(controller.inputToken, token);
			for each (var key:uint in keys)
				shortcuts[key] = fun;
			return {label: label, fun: fun};
		}
		
		private static const LABEL_SHOW_SETTINGS:String = '…';
		//private static const LABEL_SHOW_SETTINGS:String = '⋯';
		private static const LABEL_CLEAR_AC:String = 'AC';
		private static const LABEL_DELETE:String = 'DEL';
		private static const LABEL_BACKSPACE:String = '←BS';
		private static const LABEL_EXPONENT:String = 'EXP';
		private static const LABEL_EQUALS:String = '=';
		private static const LABEL_TOGGLE_INSERT:String = 'INS';
		private static const LABEL_TOGGLE_INVERSE_TRIG:String = 'inv';
		private static const LABEL_TOGGLE_HYPERBOLIC_TRIG:String = 'hyp';
		private static const LABEL_ANGLE_DEGREE:String = 'Deg';
		private static const LABEL_ANGLE_RADIAN:String = 'Rad';
		private static const LABEL_ANGLE_GRAD:String = 'Grad';
		private static const LABEL_FORMAT_NORM1:String = 'Nr1';
		private static const LABEL_FORMAT_NORM2:String = 'Nr2';
		private static const LABEL_FORMAT_FIX:String = 'Fix';
		private static const LABEL_FORMAT_SCI:String = 'Sci';
		private static const LABEL_FORMAT_ENG:String = 'Eng';
		private static const LABEL_SIN:String = 'sin';
		private static const LABEL_COS:String = 'cos';
		private static const LABEL_TAN:String = 'tan';
		private static const LABEL_MOVE_CARET_LEFT:String = '◀';
		private static const LABEL_MOVE_CARET_RIGHT:String = '▶';
		private static const LABEL_GOTO_HISTORY_PREV:String = '▲';
		private static const LABEL_GOTO_HISTORY_NEXT:String = '▼';
		
		private function createActions():void {
			actions = {};
			shortcuts = {};
			
			actions['showSettings'] = createFunAction(LABEL_SHOW_SETTINGS, controller.showSettings, Keyboard.F12);
			actions['clearAC'] = createFunAction(LABEL_CLEAR_AC, controller.clearAC, Keyboard.ESCAPE);
			actions['delete'] = createFunAction(LABEL_DELETE, controller.deleteToken, Keyboard.DELETE);
			actions['backspace'] = createFunAction(LABEL_BACKSPACE, Lambda.replace(controller.deleteToken, true), Keyboard.BACKSPACE);
			actions['toggleInsertMode'] = createFunAction(LABEL_TOGGLE_INSERT);
			shortcuts[Keyboard.INSERT] = toggleInsertMode;
			actions['toggleInverseTrigMode'] = createFunAction(LABEL_TOGGLE_INVERSE_TRIG);
			shortcuts[Keyboard.I] = toggleInverseTrigMode;
			actions['toggleHyperbolicTrigMode'] = createFunAction(LABEL_TOGGLE_HYPERBOLIC_TRIG);
			shortcuts[Keyboard.H] = toggleHyperbolicTrigMode;
			// TODO: If a move caret button is continously pressed, start repeating their function after a delay.
			actions['moveCaretLeft'] = createFunAction(LABEL_MOVE_CARET_LEFT, controller.moveCaretLeft, Keyboard.LEFT);
			actions['moveCaretRight'] = createFunAction(LABEL_MOVE_CARET_RIGHT, controller.moveCaretRight, Keyboard.RIGHT);
			shortcuts[Keyboard.HOME] = controller.moveCaretHome;
			shortcuts[Keyboard.END] = controller.moveCaretEnd;
			
			for (var i:int = 0; i < 10; ++i) {
				var char:String = String.fromCharCode(48 + i);
				actions['digit' + char] = createInputTokenAction(char, MathTokenCache.getNumberCharToken(char), Keyboard.NUMBER_0 + i, Keyboard.NUMPAD_0 + i);
			}
			actions['radixPoint'] = createInputTokenAction('.', MathTokenCache.getNumberCharToken('.'), Keyboard.PERIOD);
			actions['exponent'] = createInputTokenAction(LABEL_EXPONENT, MathTokenCache.getNumberCharToken('e'), Keyboard.E);
			actions['space'] = createInputTokenAction('␣', MathTokenCache.getMetaToken(MathToken.BLANK, ' '), Keyboard.SPACE);
			
			actions['opAdd'] = createInputTokenAction(MathConst.OP_ADD, MathTokenCache.getOperatorToken(MathToken.OPERATOR, MathConst.OP_ADD), Keyboard.EQUAL | SHIFT_KEY, Keyboard.NUMPAD_ADD);
			actions['opSubtract'] = createInputTokenAction(MathConst.OP_SUBTRACT, MathTokenCache.getOperatorToken(MathToken.OPERATOR, MathConst.OP_SUBTRACT), Keyboard.MINUS, Keyboard.NUMPAD_SUBTRACT);
			actions['opUnaryMinus'] = createInputTokenAction('(-)', MathTokenCache.getOperatorToken(MathToken.OPERATOR, MathOperator.SUBTRACT));
			actions['opMultiply'] = createInputTokenAction(MathConst.OP_MULTIPLY, MathTokenCache.getOperatorToken(MathToken.OPERATOR, MathConst.OP_MULTIPLY), Keyboard.NUMBER_8 | SHIFT_KEY, Keyboard.NUMPAD_MULTIPLY);
			actions['opDivide'] = createInputTokenAction(MathConst.OP_DIVIDE, MathTokenCache.getOperatorToken(MathToken.OPERATOR, MathConst.OP_DIVIDE), Keyboard.SLASH, Keyboard.NUMPAD_DIVIDE);
			actions['opIntDiv'] = createInputTokenAction('\\', MathTokenCache.getOperatorToken(MathToken.OPERATOR, MathOperator.INT_DIVIDE), Keyboard.BACKSLASH);
			actions['opModulo'] = createInputTokenAction('mod', MathTokenCache.getOperatorToken(MathToken.OPERATOR, MathOperator.MODULO), Keyboard.NUMBER_5 | SHIFT_KEY | ALT_KEY);
			actions['opReciprocal'] = createInputTokenAction('x' + MathOperator.RECIPROCAL, MathTokenCache.getOperatorToken(MathToken.OPERATOR, MathOperator.RECIPROCAL), Keyboard.R);
			actions['opSquare'] = createInputTokenAction('x' + MathOperator.SQUARE, MathTokenCache.getOperatorToken(MathToken.OPERATOR, MathOperator.SQUARE), Keyboard.Q);
			actions['opCube'] = createInputTokenAction('x' + MathOperator.CUBE, MathTokenCache.getOperatorToken(MathToken.OPERATOR, MathOperator.CUBE), Keyboard.W);
			actions['opPow'] = createInputTokenAction('^', MathTokenCache.getOperatorToken(MathToken.OPERATOR, MathOperator.EXPONENTIATE), Keyboard.NUMBER_6 | SHIFT_KEY, Keyboard.Y);
			actions['opNthRoot'] = createInputTokenAction('ⁿ√', MathTokenCache.getOperatorToken(MathToken.OPERATOR, MathOperator.NTH_ROOT), Keyboard.Y | ALT_KEY);
			actions['opFactorial'] = createInputTokenAction('x!', MathTokenCache.getOperatorToken(MathToken.OPERATOR, MathOperator.FACTORIAL), Keyboard.NUMBER_1 | SHIFT_KEY);
			actions['opPercentage'] = createInputTokenAction('%', MathTokenCache.getOperatorToken(MathToken.OPERATOR, MathOperator.PERCENTAGE), Keyboard.NUMBER_5 | SHIFT_KEY);
			
			actions['funSqrt'] = createInputTokenAction('√', MathTokenCache.getOperatorToken(MathToken.FUN_JUXTA, MathFun.SQRT), Keyboard.Q | ALT_KEY);
			actions['funCurt'] = createInputTokenAction('³√', MathTokenCache.getOperatorToken(MathToken.FUN_JUXTA, MathFun.CURT), Keyboard.W | ALT_KEY);
			actions['funSin'] = createFunAction(LABEL_SIN, controller.inputTokenSin, Keyboard.S);
			actions['funCos'] = createFunAction(LABEL_COS, controller.inputTokenCos, Keyboard.C);
			actions['funTan'] = createFunAction(LABEL_TAN, controller.inputTokenTan, Keyboard.T);
			actions['funLn'] = createInputTokenAction('ln', MathTokenCache.getOperatorToken(MathToken.FUN_JUXTA, MathFun.LN), Keyboard.L);
			actions['funLogN'] = createInputTokenAction('log', MathTokenCache.getOperatorToken(MathToken.FUN_JUXTA, MathFun.LOG_N), Keyboard.L | ALT_KEY);
			actions['funLog2'] = createInputTokenAction('lb', MathTokenCache.getOperatorToken(MathToken.FUN_JUXTA, MathFun.LOG_2), Keyboard.B);
			actions['funLog10'] = createInputTokenAction('lg', MathTokenCache.getOperatorToken(MathToken.FUN_JUXTA, MathFun.LOG_10), Keyboard.G);
			actions['funExp'] = createInputTokenAction('eⁿ', MathTokenCache.getOperatorToken(MathToken.FUN_JUXTA, MathFun.EXP), Keyboard.L | CTRL_KEY);
			actions['funExp10'] = createInputTokenAction('10ⁿ', MathTokenCache.getOperatorToken(MathToken.FUN_JUXTA, MathFun.EXP_10), Keyboard.G | CTRL_KEY);
			actions['funMin'] = createInputTokenAction('min', MathTokenCache.getOperatorToken(MathToken.FUN_PAREN, MathFun.MIN), Keyboard.Z);
			actions['funMax'] = createInputTokenAction('max', MathTokenCache.getOperatorToken(MathToken.FUN_PAREN, MathFun.MAX), Keyboard.X);
			actions['funRandom'] = createInputTokenAction('rand', MathTokenCache.getOperatorToken(MathToken.FUN_JUXTA, 'rand'), Keyboard.R | ALT_KEY);
			actions['funRandomRange'] = createInputTokenAction('randR', MathTokenCache.getOperatorToken(MathToken.FUN_PAREN, 'randR'), Keyboard.R | ALT_KEY | CTRL_KEY);
			actions['funRandom3'] = createInputTokenAction('rand3', MathTokenCache.getIdentifierToken(MathToken.VARIABLE, 'rand3'), Keyboard.R | ALT_KEY | SHIFT_KEY);
			actions['funRound'] = createInputTokenAction('round', MathTokenCache.getOperatorToken(MathToken.FUN_JUXTA, MathFun.ROUND), Keyboard.D | ALT_KEY);
			actions['funRoundToDigits'] = createInputTokenAction('roundD', MathTokenCache.getOperatorToken(MathToken.FUN_PAREN, 'roundD'), Keyboard.D | ALT_KEY | CTRL_KEY);
			actions['funRoundToNearest'] = createInputTokenAction('roundN', MathTokenCache.getOperatorToken(MathToken.FUN_PAREN, 'roundN'), Keyboard.D | ALT_KEY | SHIFT_KEY);
			
			actions['leftParen'] = createInputTokenAction('(', MathTokenCache.getMathToken(MathToken.LEFT_PAREN), Keyboard.NUMBER_9 | SHIFT_KEY);
			actions['rightParen'] = createInputTokenAction(')', MathTokenCache.getMathToken(MathToken.RIGHT_PAREN), Keyboard.NUMBER_0 | SHIFT_KEY);
			actions['comma'] = createInputTokenAction(',', MathTokenCache.getMathToken(MathToken.LIST_SEP), Keyboard.COMMA);
			
			actions['constPi'] = createInputTokenAction('π', MathTokenCache.getIdentifierToken(MathToken.CONSTANT, MathConst.CONST_NAME_PI), Keyboard.P);
			actions['constE'] = createInputTokenAction('e', MathTokenCache.getIdentifierToken(MathToken.CONSTANT, MathConst.CONST_NAME_E), Keyboard.E | ALT_KEY);
			actions['constI'] = createInputTokenAction('i', MathTokenCache.getIdentifierToken(MathToken.CONSTANT, MathConst.CONST_NAME_I), Keyboard.I | ALT_KEY, Keyboard.J);
			actions['varMem'] = createInputTokenAction('M', MathTokenCache.getIdentifierToken(MathToken.VARIABLE, MathConst.VAR_NAME_MEMORY), Keyboard.M);
			actions['varAns'] = createInputTokenAction('Ans', MathTokenCache.getIdentifierToken(MathToken.VARIABLE, MathConst.VAR_NAME_ANSWER), Keyboard.A);
			for each (var varName:String in MathConst.VAR_NAMES)
				actions['var' + varName] = createInputTokenAction(varName, MathTokenCache.getIdentifierToken(MathToken.VARIABLE, varName), Keyboard[varName] | SHIFT_KEY);
			
			// TODO: Refactor entry of assignment operators. Check previous token, if possible: set varName, convert direct assignment to compound assignment.
			actions['assign'] = createInputTokenAction(':=', MathTokenCache.getOpAssignToken(MathOperator.ASSIGN, null), Keyboard.EQUAL);
			//actions['rassign'] = createInputTokenAction('→', MathTokenCache.getOpAssignToken(MathOperator.RASSIGN, null), Keyboard.EQUAL | ALT_KEY);
			
			actions['equals'] = createFunAction(LABEL_EQUALS, controller.equals, Keyboard.EQUAL | CTRL_KEY, Keyboard.ENTER, Keyboard.NUMPAD_ENTER);
			actions['memClear'] = createFunAction('MC', controller.memClear, Keyboard.C | CTRL_KEY);
			// TODO: Set as MR button's long-press action?
			actions['memRestore'] = createFunAction('MR', controller.memRestore, Keyboard.R | CTRL_KEY);
			actions['memStore'] = createFunAction('MS', controller.memStore, Keyboard.S | CTRL_KEY);
			actions['memPlus'] = createFunAction('M+', controller.memPlus, Keyboard.P | CTRL_KEY);
			actions['memMinus'] = createFunAction('M−', controller.memMinus, Keyboard.M | CTRL_KEY);
			
			actions['gotoHistoryPrev'] = createFunAction(LABEL_GOTO_HISTORY_PREV, controller.gotoHistoryPrev, Keyboard.UP);
			actions['gotoHistoryNext'] = createFunAction(LABEL_GOTO_HISTORY_NEXT, controller.gotoHistoryNext, Keyboard.DOWN);
			shortcuts[Keyboard.HOME | CTRL_KEY] = controller.gotoHistoryFirst;
			shortcuts[Keyboard.END | CTRL_KEY] = controller.gotoHistoryLast;
			
			actions['aumCycle'] = createFunAction('DRG', cycleAngleUnitMode);
			//actions['aumDeg'] = createFunAction(LABEL_ANGLE_DEGREE);
			//actions['aumRad'] = createFunAction(LABEL_ANGLE_RADIAN);
			//actions['aumGrad'] = createFunAction(LABEL_ANGLE_GRAD);
			shortcuts[Keyboard.F1] = Lambda.fix(setAngleUnitMode, LABEL_ANGLE_DEGREE);
			shortcuts[Keyboard.F2] = Lambda.fix(setAngleUnitMode, LABEL_ANGLE_RADIAN);
			shortcuts[Keyboard.F3] = Lambda.fix(setAngleUnitMode, LABEL_ANGLE_GRAD);
			
			actions['rdfNorm1'] = createFunAction(LABEL_FORMAT_NORM1);
			actions['rdfNorm2'] = createFunAction(LABEL_FORMAT_NORM2);
			actions['rdfFix'] = createFunAction(LABEL_FORMAT_FIX);
			actions['rdfSci'] = createFunAction(LABEL_FORMAT_SCI);
			actions['rdfEng'] = createFunAction(LABEL_FORMAT_ENG);
			shortcuts[Keyboard.F5] = toggleNormalResultDisplayFormat;
			shortcuts[Keyboard.F6] = Lambda.fix(setResultDisplayFormat, LABEL_FORMAT_FIX);
			shortcuts[Keyboard.F7] = Lambda.fix(setResultDisplayFormat, LABEL_FORMAT_SCI);
			shortcuts[Keyboard.F8] = Lambda.fix(setResultDisplayFormat, LABEL_FORMAT_ENG);
			
			shortcuts[Keyboard.F9] = controller.clearMode;
			shortcuts[Keyboard.F10] = controller.clearMemory;
			shortcuts[Keyboard.F11] = controller.clearHistory;
			
			CONFIG::debug {
				for (i = 1; i <= 4; ++i) {
					char = String.fromCharCode(48 + i);
					actions['dev' + char] = createFunAction('D' + char, this['devFun' + char], (Keyboard.NUMBER_0 + i) | CTRL_KEY);
				}
			}
		}
		
		private function createButtonLayout():void {
			/* TODO: Read layout from embedded (or external?) XML. Refactor. */
			/* TODO: Support multiple layouts. */
			/* TODO: Support shift/alt keys / more advanced multi-keys. */
			defaultFunButtonLayout = [
				[actions['funExp10'], actions['funLog10'], actions['moveCaretLeft'], actions['moveCaretRight'], actions['gotoHistoryPrev'], actions['showSettings']],
				[actions['funExp'], actions['funLn'], actions['opFactorial'], actions['constPi'], actions['gotoHistoryNext'], actions['rdfNorm1']],
				[actions['opCube'], actions['funCurt'], actions['opNthRoot'], actions['funSin'], actions['aumCycle'], actions['rdfFix']],
				[actions['opSquare'], actions['funSqrt'], actions['opPow'], actions['funCos'], actions['toggleHyperbolicTrigMode'], actions['rdfSci']],
				[actions['opReciprocal'], actions['memRestore'], actions['memMinus'], actions['funTan'], actions['toggleInverseTrigMode'], actions['rdfEng']],
				[actions['memClear'], actions['memStore'], actions['memPlus'], actions['leftParen'], actions['rightParen'], actions['toggleInsertMode']]
			];
			defaultMainButtonLayout = [
				[actions['digit7'], actions['digit8'], actions['digit9'], actions['backspace'], actions['clearAC']],
				[actions['digit4'], actions['digit5'], actions['digit6'], actions['opMultiply'], actions['opDivide']],
				[actions['digit1'], actions['digit2'], actions['digit3'], actions['opAdd'], actions['opSubtract']],
				[actions['digit0'], actions['radixPoint'], actions['exponent'], actions['varAns'], actions['equals']]
			];
			funButtonLayout = defaultFunButtonLayout;
			mainButtonLayout = defaultMainButtonLayout;
		}
		
		private function createButtons():void {
			resultDisplayFormatGroup = new ToggleGroup();
			
			funKeypad = createButtonsFromLayout(funButtonLayout, CHILD_STYLE_NAME_KEYPAD_BUTTON_FUN_GRID, CHILD_STYLE_NAME_KEYPAD_BUTTON_FUN_ROW);
			mainKeypad = createButtonsFromLayout(mainButtonLayout, CHILD_STYLE_NAME_KEYPAD_BUTTON_MAIN_GRID, CHILD_STYLE_NAME_KEYPAD_BUTTON_MAIN_ROW);
			addChild(funKeypad);
			addChild(mainKeypad);
			
			resultDisplayFormatButtons[LABEL_FORMAT_NORM2] = resultDisplayFormatButtons[LABEL_FORMAT_NORM1];
			moveCaretLeftButton.isLongPressEnabled = moveCaretRightButton.isLongPressEnabled = true;
			gotoHistoryPrevButton.isLongPressEnabled = gotoHistoryNextButton.isLongPressEnabled = true;
		}
		
		private function createButtonsFromLayout(buttonLayout:Array, gridStyleName:String, rowStyleName:String):LayoutGroup {
			var grid:LayoutGroup = new LayoutGroup();
			grid.styleNameList.add(gridStyleName);
			for (var y:int = 0; y < buttonLayout.length; ++y) {
				var buttonRow:Array = buttonLayout[y];
				var row:LayoutGroup = new LayoutGroup();
				row.styleNameList.add(rowStyleName);
				for (var x:int = 0; x < buttonRow.length; ++x) {
					var data:Object = buttonRow[x];
					var keypadButton:Button = createButton(data);
					row.addChild(keypadButton);
				}
				grid.addChild(row);
			}
			return grid;
		}
		
		private function createButton(data:Object):Button {
			var button:Button;
			if ([LABEL_FORMAT_NORM1, LABEL_FORMAT_NORM2, LABEL_FORMAT_FIX, LABEL_FORMAT_SCI, LABEL_FORMAT_ENG].indexOf(data.label) != -1) {
				button = resultDisplayFormatButtons[data.label] = new Radio();
				button.isLongPressEnabled = data.label != LABEL_FORMAT_NORM2;
				(button as Radio).toggleGroup = resultDisplayFormatGroup;
				button.styleNameList.add(CHILD_STYLE_NAME_KEYPAD_RADIO);
			}
			else if (data.label == LABEL_TOGGLE_INSERT) {
				button = insertModeButton = new ToggleButton();
				button.addEventListener(Event.CHANGE, onInsertModeChange);
			}
			else if (data.label == LABEL_TOGGLE_INVERSE_TRIG) {
				button = inverseTrigModeButton = new ToggleButton();
				button.addEventListener(Event.CHANGE, onInverseTrigModeChange);
			}
			else if (data.label == LABEL_TOGGLE_HYPERBOLIC_TRIG) {
				button = hyperbolicTrigModeButton = new ToggleButton();
				button.addEventListener(Event.CHANGE, onHyperbolicTrigModeChange);
			}
			else {
				button = new Button();
				if (data.label == LABEL_SIN)
					funSinButton = button;
				else if (data.label == LABEL_COS)
					funCosButton = button;
				else if (data.label == LABEL_TAN)
					funTanButton = button;
				else if (data.label == LABEL_MOVE_CARET_LEFT)
					moveCaretLeftButton = button;
				else if (data.label == LABEL_MOVE_CARET_RIGHT)
					moveCaretRightButton = button;
				else if (data.label == LABEL_GOTO_HISTORY_PREV)
					gotoHistoryPrevButton = button;
				else if (data.label == LABEL_GOTO_HISTORY_NEXT)
					gotoHistoryNextButton = button;
				button.addEventListener(Event.TRIGGERED, Lambda.replace(data.fun));
			}
			setButtonStyles(button, data.label);
			button.label = data.label;
			//if ('shiftfun' in data)
				//button.shiftfun = data.shiftfun;
			return button;
		}
		
		private function setButtonStyles(button:Button, label:String):void {
			switch (label) {
				case LABEL_CLEAR_AC:
				case LABEL_DELETE:
				case LABEL_BACKSPACE:
				case LABEL_EQUALS: // TODO: Add separate style?
					button.styleNameList.add(CHILD_STYLE_NAME_KEYPAD_DEL_BUTTON);
					break;
				case '1': // TODO: Increase font size for main buttons (besides Ans?)
				case '2':
				case '3':
				case '4':
				case '5':
				case '6':
				case '7':
				case '8':
				case '9':
				case '0':
				case '.':
				case LABEL_EXPONENT:
				case LABEL_EQUALS:
				case MathConst.OP_ADD:
				case MathConst.OP_SUBTRACT:
				case MathConst.OP_MULTIPLY:
				case MathConst.OP_DIVIDE:
				case MathConst.VAR_NAME_ANSWER:
					button.styleNameList.add(CHILD_STYLE_NAME_KEYPAD_MAIN_BUTTON);
					break;
				default:
					button.styleNameList.add(CHILD_STYLE_NAME_KEYPAD_FUN_BUTTON);
			}
		}
		
		private function addEventListeners():void {
			moveCaretLeftButton.addEventListener(FeathersEventType.LONG_PRESS, controller.moveCaretHome);
			moveCaretRightButton.addEventListener(FeathersEventType.LONG_PRESS, controller.moveCaretEnd);
			gotoHistoryPrevButton.addEventListener(FeathersEventType.LONG_PRESS, controller.gotoHistoryFirst);
			gotoHistoryNextButton.addEventListener(FeathersEventType.LONG_PRESS, controller.gotoHistoryLast);
			
			resultDisplayFormatGroup.addEventListener(Event.CHANGE, onResultDisplayFormatChange);
			resultDisplayFormatButtons[LABEL_FORMAT_NORM1].addEventListener(Event.TRIGGERED, onResultDisplayFormatNormTriggered);
			resultDisplayFormatButtons[LABEL_FORMAT_FIX].addEventListener(FeathersEventType.LONG_PRESS, onResultDisplayFormatLongPress);
			resultDisplayFormatButtons[LABEL_FORMAT_SCI].addEventListener(FeathersEventType.LONG_PRESS, onResultDisplayFormatLongPress);
			resultDisplayFormatButtons[LABEL_FORMAT_ENG].addEventListener(FeathersEventType.LONG_PRESS, onResultDisplayFormatLongPress);
			
			//model.addEventListener(ModelEventType.ANGLE_UNIT_MODE_CHANGE, onModelAngleUnitModeChange);
			viewModel.addEventListener(ViewModelEventType.RESULT_DISPLAY_FORMAT_CHANGE, onModelResultDisplayFormatChange);
			viewModel.addEventListener(ViewModelEventType.INSERT_MODE_CHANGE, onModelInsertModeChange);
			viewModel.addEventListener(ViewModelEventType.INVERSE_TRIG_MODE_CHANGE, onModelTrigModeChange);
			viewModel.addEventListener(ViewModelEventType.HYPERBOLIC_TRIG_MODE_CHANGE, onModelTrigModeChange);
			viewModel.addEventListener(ViewModelEventType.KEYBOARD_INPUT_ENABLED_CHANGE, onKeyboardInputEnabledChange);
			viewModel.addEventListener(ViewModelEventType.IS_EDITABLE_CHANGE, onCaretIndexChange);
			viewModel.addEventListener(ViewModelEventType.CARET_INDEX_CHANGE, onCaretIndexChange);
			viewModel.addEventListener(ViewModelEventType.MAX_CARET_INDEX_CHANGE, onCaretIndexChange);
			viewModel.addEventListener(ViewModelEventType.HISTORY_INDEX_CHANGE, onHistoryIndexChange);
			viewModel.addEventListener(ViewModelEventType.MAX_HISTORY_INDEX_CHANGE, onHistoryIndexChange);
			
			//onModelAngleUnitModeChange();
			onModelResultDisplayFormatChange();
			onModelInsertModeChange();
			onModelTrigModeChange();
			onKeyboardInputEnabledChange();
			onCaretIndexChange();
			onHistoryIndexChange();
		}
		
		CONFIG::debug {
			private static const DEBUG_BUTTON_TRIPLE_TAP_TIME:int = 500;
			
			private var debugButtonLastTime:int;
			private var debugButtonTaps:int;
			
			private function onDebugButtonTriggered():void {
				var time:int = getTimer();
				debugButtonTaps = (!debugButtonLastTime || ((time - debugButtonLastTime) <= DEBUG_BUTTON_TRIPLE_TAP_TIME)) ? (debugButtonTaps + 1) : 1;
				if (debugButtonTaps >= 3) {
					debugButtonTaps = debugButtonLastTime = 0;
					devFun4();
				}
				else {
					debugButtonLastTime = time;
				}
			}
		}
		
		private function cycleAngleUnitMode():void {
			controller.setAngleUnitMode((model.angleUnitMode == AngleUnit.DEGREE) ? AngleUnit.RADIAN : (model.angleUnitMode == AngleUnit.RADIAN) ? AngleUnit.GRAD : AngleUnit.DEGREE);
		}
		
		private function setAngleUnitMode(label:String):void {
			var unit:AngleUnit;
			if (label == LABEL_ANGLE_DEGREE)
				unit = AngleUnit.DEGREE;
			else if (label == LABEL_ANGLE_RADIAN)
				unit = AngleUnit.RADIAN;
			else if (label == LABEL_ANGLE_GRAD)
				unit = AngleUnit.GRAD;
			if (model.angleUnitMode != unit)
				controller.setAngleUnitMode(unit);
		}
		
		private var digitsLabel:Label;
		
		private function onResultDisplayFormatLongPress(e:Event):void {
			var button:Radio = e.currentTarget as Radio;
			if (resultDisplayFormatGroup.selectedItem != button)
				resultDisplayFormatGroup.selectedItem = button;
			
			// TODO: PopUpController should take care of showing this callout.
			var label:String = button.label;
			var group:LayoutGroup = new LayoutGroup();
			var layout:VerticalLayout = new VerticalLayout();
			layout.horizontalAlign = HorizontalAlign.CENTER;
			group.layout = layout;
			digitsLabel = new Label();
			group.addChild(digitsLabel);
			var slider:Slider = new Slider();
			group.addChild(slider);
			
			slider.direction = Direction.VERTICAL;
			slider.height = 300 * ThemeManager.instance.theme.scale;
			// TODO: The values should be initialized by PopUpController.
			slider.step = 1;
			if (label == LABEL_FORMAT_NORM1) {
				slider.minimum = 0;
				slider.maximum = 11; // TODO: If leadingZero setting (to be added) is false, add 1.
				slider.value = ResultDisplayFormat.NORMAL_1.digits;
			}
			else if (label == LABEL_FORMAT_FIX) {
				slider.minimum = 0;
				slider.maximum = 11; // TODO: If leadingZero setting (to be added) is false, add 1.
				slider.value = ResultDisplayFormat.FIXED.digits;
			}
			else if (label == LABEL_FORMAT_SCI) {
				slider.minimum = 1;
				slider.maximum = 12;
				slider.value = ResultDisplayFormat.SCIENTIFIC.digits;
			}
			else if (label == LABEL_FORMAT_ENG) {
				slider.minimum = 1;
				slider.maximum = 12; // If using fractionDigits, 11.
				slider.value = ResultDisplayFormat.ENGINEERING.digits;
			}
			slider.addEventListener(Event.CHANGE, onSliderChange);
			digitsLabel.text = slider.value.toString();
			
			Callout.show(group, button, new <String>[RelativePosition.LEFT]);
		}
		
		private function onSliderChange(e:Event):void {
			var slider:Slider = e.currentTarget as Slider;
			var format:ResultDisplayFormat = viewModel.resultDisplayFormat;
			format.digits = slider.value;
			digitsLabel.text = slider.value.toString();
			controller.setResultDisplayFormat(format);
		}
		
		private function onResultDisplayFormatChange():void {
			setResultDisplayFormat((resultDisplayFormatGroup.selectedItem as Radio).label);
		}
		
		private function onResultDisplayFormatNormTriggered():void {
			var label:String = (resultDisplayFormatGroup.selectedItem as Radio).label;
			if (label == LABEL_FORMAT_NORM1)
				controller.setResultDisplayFormat(ResultDisplayFormat.NORMAL_2);
			else if (label == LABEL_FORMAT_NORM2)
				controller.setResultDisplayFormat(ResultDisplayFormat.NORMAL_1);
		}
		
		private var lastNormalResultDisplayFormat:ResultDisplayFormat;
		
		private function toggleNormalResultDisplayFormat():void {
			var format:ResultDisplayFormat = viewModel.resultDisplayFormat;
			controller.setResultDisplayFormat((format == ResultDisplayFormat.NORMAL_1) ? ResultDisplayFormat.NORMAL_2 : (format == ResultDisplayFormat.NORMAL_2) ? ResultDisplayFormat.NORMAL_1 : lastNormalResultDisplayFormat);
		}
		
		private function setResultDisplayFormat(label:String):void {
			var format:ResultDisplayFormat;
			if (label == LABEL_FORMAT_NORM1)
				format = ResultDisplayFormat.NORMAL_1;
			else if (label == LABEL_FORMAT_NORM2)
				format = ResultDisplayFormat.NORMAL_2;
			else if (label == LABEL_FORMAT_FIX)
				format = ResultDisplayFormat.FIXED;
			else if (label == LABEL_FORMAT_SCI)
				format = ResultDisplayFormat.SCIENTIFIC;
			else if (label == LABEL_FORMAT_ENG)
				format = ResultDisplayFormat.ENGINEERING;
			
			if (viewModel.resultDisplayFormat != format)
				controller.setResultDisplayFormat(format);
		}
		
		private function onModelResultDisplayFormatChange():void {
			var format:ResultDisplayFormat = viewModel.resultDisplayFormat;
			if ((format == ResultDisplayFormat.NORMAL_1) || (format == ResultDisplayFormat.NORMAL_2))
				lastNormalResultDisplayFormat = format;
			
			var label:String;
			if (format == ResultDisplayFormat.NORMAL_1)
				label = LABEL_FORMAT_NORM1;
			else if (format == ResultDisplayFormat.NORMAL_2)
				label = LABEL_FORMAT_NORM2;
			else if (format == ResultDisplayFormat.FIXED)
				label = LABEL_FORMAT_FIX;
			else if (format == ResultDisplayFormat.SCIENTIFIC)
				label = LABEL_FORMAT_SCI;
			else if (format == ResultDisplayFormat.ENGINEERING)
				label = LABEL_FORMAT_ENG;
			
			if ((resultDisplayFormatGroup.selectedItem as Radio).label != label)
				resultDisplayFormatGroup.selectedItem = resultDisplayFormatButtons[label];
			
			if ((label == LABEL_FORMAT_NORM1) || (label == LABEL_FORMAT_NORM2)) {
				var button:Button = resultDisplayFormatGroup.selectedItem as Button;
				button.label = label;
				if (label == LABEL_FORMAT_NORM1)
					resultDisplayFormatButtons[LABEL_FORMAT_NORM1].addEventListener(FeathersEventType.LONG_PRESS, onResultDisplayFormatLongPress);
				else
					resultDisplayFormatButtons[LABEL_FORMAT_NORM1].removeEventListener(FeathersEventType.LONG_PRESS, onResultDisplayFormatLongPress);
			}
		}
		
		private function onInsertModeChange():void {
			controller.setInsertMode(insertModeButton.isSelected);
			CONFIG::debug {
				onDebugButtonTriggered();
			}
		}
		
		private function onModelInsertModeChange():void {
			insertModeButton.isSelected = viewModel.insertMode;
		}
		
		private function onInverseTrigModeChange():void {
			controller.setInverseTrigMode(inverseTrigModeButton.isSelected);
		}
		
		private function onHyperbolicTrigModeChange():void {
			controller.setHyperbolicTrigMode(hyperbolicTrigModeButton.isSelected);
		}
		
		private function onModelTrigModeChange():void {
			var invStr:String = viewModel.inverseTrigMode ? '⁻¹' : '';
			var hypStr:String = viewModel.hyperbolicTrigMode ? 'h' : '';
			funSinButton.label = LABEL_SIN + hypStr + invStr;
			funCosButton.label = LABEL_COS + hypStr + invStr;
			funTanButton.label = LABEL_TAN + hypStr + invStr;
			var smallFont:int = (invStr + hypStr).length;
			setSmallFontButtonStyle(funSinButton, smallFont);
			setSmallFontButtonStyle(funCosButton, smallFont);
			setSmallFontButtonStyle(funTanButton, smallFont);
			inverseTrigModeButton.isSelected = viewModel.inverseTrigMode;
			hyperbolicTrigModeButton.isSelected = viewModel.hyperbolicTrigMode;
		}
		
		private function setSmallFontButtonStyle(button:Button, smallFont:int = 0):void {
			button.styleNameList.remove(CHILD_STYLE_NAME_KEYPAD_SMALLER_FONT_BUTTON);
			button.styleNameList.remove(CHILD_STYLE_NAME_KEYPAD_SMALLEST_FONT_BUTTON);
			if ((smallFont > 0) && (smallFont <= 2))
				button.styleNameList.add(CHILD_STYLE_NAME_KEYPAD_SMALLER_FONT_BUTTON);
			else if (smallFont >= 3)
				button.styleNameList.add(CHILD_STYLE_NAME_KEYPAD_SMALLEST_FONT_BUTTON);
		}
		
		private function onKeyboardInputEnabledChange():void {
			// TODO: Also listen to ADDED_TO_STAGE and REMOVED_FROM_STAGE events, only register keyboard event handlers when the object is on the stage.
			if (viewModel.keyboardInputEnabled)
				stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
			else
				stage.removeEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
		}
		
		private function onCaretIndexChange():void {
			moveCaretLeftButton.isEnabled = (viewModel.caretIndex > 0) || !viewModel.isEditable;
			moveCaretRightButton.isEnabled = (viewModel.caretIndex < viewModel.maxCaretIndex) || !viewModel.isEditable;
		}
		
		private function onHistoryIndexChange():void {
			gotoHistoryPrevButton.isEnabled = viewModel.historyIndex > 0;
			gotoHistoryNextButton.isEnabled = viewModel.historyIndex < viewModel.maxHistoryIndex;
		}
		
		CONFIG::debug {
			private function devFun1(... args):void {
				controller.loadSettings();
				controller.loadState();
			}
			
			private function devFun2(... args):void {
				settingsModel.math.lenientBracketChecking = !settingsModel.math.lenientBracketChecking;
				settingsModel.math.numberAllowSoloRadix = !settingsModel.math.numberAllowSoloRadix;
				settingsModel.ui.theme.colorSchemeName = (settingsModel.ui.theme.colorSchemeName == 'solarized-dark') ? 'solarized-light' : 'solarized-dark';
				controller.saveSettings();
				controller.saveState();
			}
			
			private function devFun3(... args):void {
				controller.wipeSettings();
				controller.wipeState();
			}
			
			public static const DEBUG_LOGS_ALERT:String = 'fxcalc-debug-logs-alert';
			
			private function devFun4(... args):void {
				Alert.show(Logger.formatLogs(null, false, false).join('\n'), 'Logs', new ListCollection([{label: 'OK'}]), null, true, true, FactoryFactory.createAlertFactory(DEBUG_LOGS_ALERT));
			}
		}
	}
}
