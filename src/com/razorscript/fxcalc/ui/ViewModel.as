package com.razorscript.fxcalc.ui {
	import com.razorscript.fxcalc.events.ViewModelEventType;
	import com.razorscript.fxcalc.ui.ResultDisplayFormat;
	import com.razorscript.math.NumberFormatter;
	import com.razorscript.math.expression.MathExpression;
	import starling.events.EventDispatcher;
	
	/**
	 * Stores and controls access to UI data and state.
	 *
	 * @author Gene Pavlovsky
	 */
	public class ViewModel extends EventDispatcher {
		public function ViewModel() {
			super();
		}
		
		private var _result:MathExpression;
		
		public function get result():MathExpression {
			return _result ? _result : _nullResult;
		}
		
		public function set result(value:MathExpression):void {
			_result = value;
			dispatchEventWith(ViewModelEventType.RESULT_CHANGE);
		}
		
		private var _nullResult:MathExpression;
		
		public function get nullResult():MathExpression {
			return _nullResult;
		}
		
		public function set nullResult(value:MathExpression):void {
			_nullResult = value;
			dispatchEventWith(ViewModelEventType.NULL_RESULT_CHANGE);
			if (!_result)
				dispatchEventWith(ViewModelEventType.RESULT_CHANGE);
		}
		
		private var _displayState:DisplayState;
		
		public function get displayState():DisplayState {
			return _displayState;
		}
		
		public function set displayState(value:DisplayState):void {
			_displayState = value;
			dispatchEventWith(ViewModelEventType.DISPLAY_STATE_CHANGE);
		}
		
		private var _isEditable:Boolean;
		
		public function get isEditable():Boolean {
			return _isEditable;
		}
		
		public function set isEditable(value:Boolean):void {
			_isEditable = value;
			dispatchEventWith(ViewModelEventType.IS_EDITABLE_CHANGE);
		}
		
		private var _resultDisplayFormat:ResultDisplayFormat;
		/**
		 * The result display formatting mode.
		 *
		 * The default value is ResultDisplayFormat.NORMAL_1.
		 */
		public function get resultDisplayFormat():ResultDisplayFormat {
			return _resultDisplayFormat;
		}
		
		public function set resultDisplayFormat(value:ResultDisplayFormat):void {
			_resultDisplayFormat = value;
			dispatchEventWith(ViewModelEventType.RESULT_DISPLAY_FORMAT_CHANGE);
		}
		
		private var _resultDisplayNumberFormatter:NumberFormatter;
		/**
		 * The result display number formatter.
		 */
		public function get resultDisplayNumberFormatter():NumberFormatter {
			return _resultDisplayNumberFormatter;
		}
		
		public function set resultDisplayNumberFormatter(value:NumberFormatter):void {
			_resultDisplayNumberFormatter = value;
			dispatchEventWith(ViewModelEventType.RESULT_DISPLAY_NUMBER_FORMATTER_CHANGE);
		}
		
		private var _insertMode:Boolean;
		/**
		 * Specifies whether to use the insert or replace mode.
		 * If true, adding a new token inserts the token at the caret index.
		 * If false, adding a new token replaces the token at the caret index with the new token.
		 */
		public function get insertMode():Boolean {
			return _insertMode;
		}
		
		public function set insertMode(value:Boolean):void {
			_insertMode = value;
			dispatchEventWith(ViewModelEventType.INSERT_MODE_CHANGE);
		}
		
		private var _inverseTrigMode:Boolean; // TODO: Use tristate off/one-shot/locked
		/**
		 * Specifies whether to use inverse trigonometric functions.
		 */
		public function get inverseTrigMode():Boolean {
			return _inverseTrigMode;
		}
		
		public function set inverseTrigMode(value:Boolean):void {
			_inverseTrigMode = value;
			dispatchEventWith(ViewModelEventType.INVERSE_TRIG_MODE_CHANGE);
		}
		
		private var _hyperbolicTrigMode:Boolean; // TODO: Use tristate off/one-shot/locked
		/**
		 * Specifies whether to use hyperbolic trigonometric functions.
		 */
		public function get hyperbolicTrigMode():Boolean {
			return _hyperbolicTrigMode;
		}
		
		public function set hyperbolicTrigMode(value:Boolean):void {
			_hyperbolicTrigMode = value;
			dispatchEventWith(ViewModelEventType.HYPERBOLIC_TRIG_MODE_CHANGE);
		}
		
		private var _caretIndex:int;
		
		public function get caretIndex():int {
			return _caretIndex;
		}
		
		public function set caretIndex(value:int):void {
			if (uint(value) > _maxCaretIndex)
				throw new RangeError('Value (' + value + ') for the caretIndex property is outside the range [0, ' + _maxCaretIndex + '].');
			_caretIndex = value;
			dispatchEventWith(ViewModelEventType.CARET_INDEX_CHANGE);
		}
		
		private var _maxCaretIndex:int;
		
		public function get maxCaretIndex():int {
			return _maxCaretIndex;
		}
		
		public function set maxCaretIndex(value:int):void {
			_maxCaretIndex = value;
			if (_caretIndex > _maxCaretIndex)
				caretIndex = _maxCaretIndex;
			dispatchEventWith(ViewModelEventType.MAX_CARET_INDEX_CHANGE);
		}
		
		private var _historyIndex:int;
		
		public function get historyIndex():int {
			return _historyIndex;
		}
		
		public function set historyIndex(value:int):void {
			if (uint(value) > _maxHistoryIndex)
				throw new RangeError('Value (' + value + ') for the historyIndex property is outside the range [0, ' + _maxHistoryIndex + '].');
			_historyIndex = value;
			dispatchEventWith(ViewModelEventType.HISTORY_INDEX_CHANGE);
		}
		
		private var _maxHistoryIndex:int;
		
		public function get maxHistoryIndex():int {
			return _maxHistoryIndex;
		}
		
		public function set maxHistoryIndex(value:int):void {
			_maxHistoryIndex = value;
			dispatchEventWith(ViewModelEventType.MAX_HISTORY_INDEX_CHANGE);
		}
		
		private var _touchInputEnabled:Boolean;
		/**
		 * Specifies whether to enable touch input.
		 */
		public function get touchInputEnabled():Boolean {
			return _touchInputEnabled;
		}
		
		public function set touchInputEnabled(value:Boolean):void {
			_touchInputEnabled = value;
			dispatchEventWith(ViewModelEventType.TOUCH_INPUT_ENABLED_CHANGE);
		}
		
		private var _keyboardInputEnabled:Boolean;
		/**
		 * Specifies whether to enable keyboard input.
		 */
		public function get keyboardInputEnabled():Boolean {
			return _keyboardInputEnabled;
		}
		
		public function set keyboardInputEnabled(value:Boolean):void {
			_keyboardInputEnabled = value;
			dispatchEventWith(ViewModelEventType.KEYBOARD_INPUT_ENABLED_CHANGE);
		}
	}
}
