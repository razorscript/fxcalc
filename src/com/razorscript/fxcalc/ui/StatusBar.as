package com.razorscript.fxcalc.ui {
	import com.razorscript.fxcalc.data.Model;
	import com.razorscript.fxcalc.events.ModelEventType;
	import com.razorscript.fxcalc.events.ViewModelEventType;
	import com.razorscript.math.AngleUnit;
	import com.razorscript.utils.Lambda;
	import com.razorscript.utils.StrUtil;
	import feathers.controls.Label;
	import feathers.controls.LayoutGroup;
	import feathers.skins.IStyleProvider;
	
	/**
	 * Displays calculator state: calculation mode, angle unit, result display format, shift/alpha key toggle state etc.
	 *
	 * @author Gene Pavlovsky
	 */
	public class StatusBar extends LayoutGroup {
		public static const CHILD_STYLE_NAME_STATUS_BAR_LABEL:String = 'fxcalc-status-bar-label';
			
		public static var globalStyleProvider:IStyleProvider;
		override protected function get defaultStyleProvider():IStyleProvider {
			return globalStyleProvider;
		}
	
		public var leftLabel:Label;
		public var rightLabel:Label;
		
		public function StatusBar(model:Model, viewModel:ViewModel) {
			super();
			this.model = model;
			this.viewModel = viewModel;
		}
		
		override protected function initialize():void {
			super.initialize();
			
			// TODO: Use separate multi-colored labels for each status field. Use separate invalidation flag for each label.
			leftLabel = new Label();
			leftLabel.styleNameList.add(CHILD_STYLE_NAME_STATUS_BAR_LABEL);
			addChild(leftLabel);
			
			rightLabel = new Label();
			rightLabel.styleNameList.add(CHILD_STYLE_NAME_STATUS_BAR_LABEL);
			addChild(rightLabel);
			
			var invalidateData:Function = Lambda.replace(invalidate, INVALIDATION_FLAG_DATA);
			model.addEventListener(ModelEventType.MEMORY_VARIABLE_CHANGE, invalidateData);
			model.addEventListener(ModelEventType.ANGLE_UNIT_MODE_CHANGE, invalidateData);
			viewModel.addEventListener(ViewModelEventType.RESULT_DISPLAY_FORMAT_CHANGE, invalidateData);
			viewModel.addEventListener(ViewModelEventType.INVERSE_TRIG_MODE_CHANGE, invalidateData);
			viewModel.addEventListener(ViewModelEventType.HYPERBOLIC_TRIG_MODE_CHANGE, invalidateData);
			viewModel.addEventListener(ViewModelEventType.HISTORY_INDEX_CHANGE, invalidateData);
			viewModel.addEventListener(ViewModelEventType.MAX_HISTORY_INDEX_CHANGE, invalidateData);
		}
		
		override protected function draw():void {
			var isDataInvalid:Boolean = isInvalid(INVALIDATION_FLAG_DATA);
			
			super.draw();
			
			if (isDataInvalid)
				commitData();
		}
		
		private var model:Model;
		private var viewModel:ViewModel;
		
		private function commitData():void {
			leftLabel.text = getMemoryVariableStatus() + '  ' + getInverseTrigMode() + '  ' + getHyperbolicTrigMode() + '  ' + getAngleUnitMode() + '  ' + getResultDisplayFormat();
			rightLabel.text = (viewModel.historyIndex + 1) + '/' + (viewModel.maxHistoryIndex + 1);
		}
		
		private function getMemoryVariableStatus():String {
			return model.memoryVariable ? 'M' : ' ';
		}
		
		private function getResultDisplayFormat():String {
			var resultDisplayFormat:ResultDisplayFormat = viewModel.resultDisplayFormat;
			// TODO: Determine NORMAL_2 digits from actual maxDigits. Or just abolish NORMAL_2.
			var digits:String = (resultDisplayFormat == ResultDisplayFormat.NORMAL_2) ? '11' : StrUtil.rpad(resultDisplayFormat.digits.toString(), ' ', 2);
			if ((resultDisplayFormat == ResultDisplayFormat.NORMAL_1) || (resultDisplayFormat == ResultDisplayFormat.NORMAL_2))
				return 'Nor:' + digits;
			else if (resultDisplayFormat == ResultDisplayFormat.FIXED)
				return 'Fix:' + digits;
			else if (resultDisplayFormat == ResultDisplayFormat.SCIENTIFIC)
				return 'Sci:' + digits;
			else if (resultDisplayFormat == ResultDisplayFormat.ENGINEERING)
				return 'Eng:' + digits;
			return '';
		}
		
		private function getAngleUnitMode():String {
			var angleUnitMode:AngleUnit = model.angleUnitMode;
			if (angleUnitMode == AngleUnit.DEGREE)
				return 'Deg';
			else if (angleUnitMode == AngleUnit.RADIAN)
				return 'Rad';
			else if (angleUnitMode == AngleUnit.GRAD)
				return 'Gra';
			return '';
		}
		
		private function getInverseTrigMode():String {
			return viewModel.inverseTrigMode ? 'Inv' : '   ';
		}
		
		private function getHyperbolicTrigMode():String {
			return viewModel.hyperbolicTrigMode ? 'Hyp' : '   ';
		}
	}
}
