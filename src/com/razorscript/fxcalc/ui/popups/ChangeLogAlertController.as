package com.razorscript.fxcalc.ui.popups {
	import com.razorscript.fxcalc.controllers.AppController;
	import com.razorscript.fxcalc.data.changeLog.ChangeLogEntry;
	import com.razorscript.fxcalc.ui.utils.FactoryFactory;
	import com.razorscript.utils.Lambda;
	import com.razorscript.utils.StrUtil;
	import com.razorscript.utils.VecUtil;
	import feathers.controls.Alert;
	import feathers.data.ListCollection;
	import starling.events.Event;
	
	/**
	 * Displays the change log in an alert.
	 *
	 * @author Gene Pavlovsky
	 */
	public class ChangeLogAlertController {
		public static const ALERT_STYLE_NAME:String = 'fxcalc-change-log-alert';
		
		public function ChangeLogAlertController(appController:AppController) {
			this.appController = appController;
		}
		
		public function show(entries:Vector.<ChangeLogEntry>):void {
			var alert:Alert = Alert.show(formatChangeLog(entries), 'Release notes',	new ListCollection([{label: 'OK'}]),
				null, true, true, FactoryFactory.createAlertFactory(ALERT_STYLE_NAME));
			alert.addEventListener(Event.CLOSE, onAlertClose);
			CONFIG::debug {
				trace('\n' + formatChangeLog(entries) + '\n');
			}
		}
		
		protected function formatChangeLog(entries:Vector.<ChangeLogEntry>):String {
			return VecUtil.genericMap(entries, Lambda.slice3(formatChangeLogEntry), null, new Vector.<String>()).join('\n\n\n');
		}
		
		protected function formatChangeLogEntry(entry:ChangeLogEntry):String {
			var lines:Vector.<String> = new Vector.<String>();
			var version:String = entry.version.toString() + (entry.description ? (' - ' + entry.description) : '');
			lines.push(version + '\n' + StrUtil.repeat('—', version.length));
			/*if (entry.description)
				lines.push(entry.description);*/
			if (entry.features)
				lines.push('Features:\n\n' + entry.features.map(Lambda.slice3(function (str:String):String { return '• ' + str; })).join('\n'));
			if (entry.bugfixes)
				lines.push('Bugfixes:\n\n' + entry.bugfixes.map(Lambda.slice3(function (str:String):String { return '• ' + str; })).join('\n'));
			return lines.join('\n\n');
		}
		
		protected function onAlertClose(e:Event):void {
			appController.saveState();
		}
		
		protected var appController:AppController;
	}
}
