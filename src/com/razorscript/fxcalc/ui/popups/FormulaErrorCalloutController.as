package com.razorscript.fxcalc.ui.popups {
	import com.razorscript.fxcalc.events.ViewModelEventType;
	import com.razorscript.fxcalc.ui.DisplayState;
	import com.razorscript.fxcalc.ui.ViewModel;
	import com.razorscript.fxcalc.ui.utils.FactoryFactory;
	import feathers.controls.Label;
	import feathers.controls.TextCallout;
	import feathers.core.PopUpManager;
	import starling.display.DisplayObject;
	
	/**
	 * Displays a formula error in a callout.
	 *
	 * @author Gene Pavlovsky
	 */
	public class FormulaErrorCalloutController {
		public static const CALLOUT_STYLE_NAME:String = 'fxcalc-formula_error-callout';
		
		public function FormulaErrorCalloutController(viewModel:ViewModel) {
			this.viewModel = viewModel;
			viewModel.addEventListener(ViewModelEventType.DISPLAY_STATE_CHANGE, onDisplayStateChangeFormulaError);
		}
		
		public function show(errorMessage:String, origin:DisplayObject):void {
			callout = TextCallout.show(formatFormulaError(errorMessage), origin, null, false, FactoryFactory.createTextCalloutFactory(CALLOUT_STYLE_NAME));
		}
		
		protected function onDisplayStateChangeFormulaError():void {
			if ((viewModel.displayState != DisplayState.ERROR) && PopUpManager.isPopUp(callout))
				callout.close();
		}
		
		protected function formatFormulaError(msg:String):String {
			// Unexpected right parenthesis.
			// Mismatched (left|right) parenthesis.
			msg = msg.replace(/(Unexpected|Mismatched) (left|right)/, '$1');
			// Not enough arguments for function "fun" (X, expected Y).
			// Too many arguments for function "fun" (X, expected Y).
			// Not enough operands for operator "op" (X, expected Y).
			msg = msg.replace(/for (function|operator) "[^"]*" /, '');
			// (Invalid|Missing) target for assignment operator "op".
			// Missing source for assignment operator "op".
			msg = msg.replace(/(target|source) for assignment operator "[^"]*"/, 'assignment $1');
			// Function "fun" requires parentheses.
			msg = msg.replace(/Function "[^"]*"/, 'Function');
			return msg.replace(/\.$/, '');
		}
		
		protected var callout:TextCallout;
		
		protected var viewModel:ViewModel;
	}
}
