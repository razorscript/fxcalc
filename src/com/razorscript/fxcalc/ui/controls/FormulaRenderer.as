package com.razorscript.fxcalc.ui.controls {
	import com.razorscript.fxcalc.math.tokens.FormulaToken;
	import com.razorscript.fxcalc.math.tokens.MathTokenCache;
	import com.razorscript.fxcalc.ui.controls.renderers.FormulaTokenListItemRenderer;
	import com.razorscript.math.expression.tokens.MathToken;
	import feathers.controls.LayoutGroup;
	import feathers.skins.IStyleProvider;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import starling.display.DisplayObject;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	
	/**
	 * Renders the formula using a FormulaTokenRenderer for each token.
	 *
	 * @author Gene Pavlovsky
	 */
	
	/** Dispatched when the user taps or clicks one of the formula token renderers. */
	[Event(name="triggered", type="starling.events.Event")]
	public class FormulaRenderer extends LayoutGroup {
		public static var globalStyleProvider:IStyleProvider;
		override protected function get defaultStyleProvider():IStyleProvider {
			return globalStyleProvider;
		}
		
		public function FormulaRenderer() {
			super();
			renderers = new Vector.<FormulaTokenListItemRenderer>();
		}
		
		public function get data():Vector.<FormulaToken> {
			return _data;
		}
		
		public function set data(value:Vector.<FormulaToken>):void {
			_data = value;
			commitData();
		}
		
		public function get characterWidth():int {
			return _characterWidth;
		}
		
		private static const POINT:Point = new Point();
		
		public function getTokenIndexAtPoint(x:Number, y:Number):int {
			POINT.setTo(x, y);
			var obj:DisplayObject = stage.hitTest(POINT);
			return (obj && (obj is FormulaTokenListItemRenderer)) ? renderers.indexOf(obj as FormulaTokenListItemRenderer) : -1;
		}
		
		public function getTokenBounds(index:uint):Rectangle {
			return (index < _data.length + (touchable ? 1 : 0)) ? renderers[index].bounds : null;
		}
		
		public function getRendererAt(index:uint):FormulaTokenListItemRenderer {
			return (index < _data.length + (touchable ? 1 : 0)) ? renderers[index] : null;
		}
		
		private static const SPACE_TOKEN:FormulaToken = new FormulaToken(MathTokenCache.getMetaToken(MathToken.BLANK, ' '));
		
		override protected function initialize():void {
			super.initialize();
			
			// Determine the character width.
			var renderer:FormulaTokenListItemRenderer = addFormulaTokenRenderer();
			renderer.data = SPACE_TOKEN;
			renderer.validate();
			_characterWidth = Math.ceil(renderer.width);
			renderer.visible = renderer.includeInLayout = false;
			
			// TODO: Add a setting to toggle the interaction mode (tap / drag / native)
			//addEventListener(TouchEvent.TOUCH, onTouch);
			//addEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);
		}
		
		private var renderers:Vector.<FormulaTokenListItemRenderer>;
		private var _characterWidth:int;
		private var _data:Vector.<FormulaToken>;
		
		private function commitData():void {
			var numTokens:int = _data ? _data.length : 0;
			var numRenderers:int = renderers.length;
			
			for (var i:int = 0; i < numTokens; ++i)
				updateRenderer(i, _data[i], numRenderers);
			
			// Add an extra space to make sure there's always touchable space to move the caret to (just beyond) the end of the formula.
			if (touchable)
				updateRenderer(i++, SPACE_TOKEN, numRenderers);
			
			for (; i < numRenderers; ++i) {
				// Hide unused token renderers and exclude them from layout.
				var renderer:FormulaTokenListItemRenderer = renderers[i];
				renderer.visible = renderer.includeInLayout = false;
			}
		}
		
		private function updateRenderer(index:int, data:FormulaToken, numRenderers:int):void {
			// Reuse existing or add new token renderers as necessary.
			var renderer:FormulaTokenListItemRenderer = (index < numRenderers) ? renderers[index] : addFormulaTokenRenderer();
			renderer.data = data;
			renderer.index = index;
			renderer.width = _characterWidth * renderer.text.length;
			renderer.visible = renderer.includeInLayout = true;
		}
		
		private function addFormulaTokenRenderer():FormulaTokenListItemRenderer {
			var renderer:FormulaTokenListItemRenderer = new FormulaTokenListItemRenderer();
			renderer.owner = this;
			// TODO: Add a setting to toggle the interaction mode (tap / drag / native)
			renderer.addEventListener(Event.TRIGGERED, onRendererTriggered);
			renderers.push(renderer);
			addChild(renderer);
			return renderer;
		}
		
		private function onRendererTriggered(e:Event, location:Point):void {
			var renderer:FormulaTokenListItemRenderer = e.currentTarget as FormulaTokenListItemRenderer;
			var index:int = renderers.indexOf(renderer);
			if (index >= 0) {
				if (renderer.data && (renderer.data.token.type != MathToken.FUN_PAREN)) {
					var width:int = renderer.width;
					var numChars:int = Math.round(width / _characterWidth);
					// If the renderer is 2, 3 or more characters wide, and respectively the right quarter, half or whole of the last character's was touched, assume the user was aiming at the next renderer.
					if ((numChars > 1) && (location.x + (((numChars == 2) ? 0.25 : (numChars == 3) ? 0.5 : 1) * _characterWidth) >= width))
						++index;
				}
				dispatchEventWith(Event.TRIGGERED, false, index);
			}
		}
		
		private var touchID:int = -1;
		
		private function onTouch(e:TouchEvent):void {
			if (!_isEnabled) {
				touchID = -1;
				return;
			}
			
			if (touchID >= 0) {
				var touch:Touch = e.getTouch(this, null, touchID);
				if (touch && ((touch.phase == TouchPhase.MOVED) || (touch.phase == TouchPhase.ENDED))) {
					touch.getLocation(stage, POINT);
					var touchObject:DisplayObject = stage.hitTest(POINT);
					if (contains(touchObject) && (touchObject is FormulaTokenListItemRenderer)) {
						var index:int = renderers.indexOf(touchObject as FormulaTokenListItemRenderer);
						if (index >= 0)
							dispatchEventWith(Event.TRIGGERED, false, index);
					}
					if (touch.phase == TouchPhase.ENDED)
						touchID = -1;
				}
			}
			else {
				touch = e.getTouch(this, TouchPhase.BEGAN);
				if (touch)
					touchID = touch.id;
			}
		}
		
		private function onRemovedFromStage(e:Event):void {
			touchID = -1;
		}
	}
}
