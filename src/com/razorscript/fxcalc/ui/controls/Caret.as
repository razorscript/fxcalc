package com.razorscript.fxcalc.ui.controls {
	import com.razorscript.debug.Logger;
	import feathers.controls.Label;
	import feathers.skins.IStyleProvider;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	/**
	 * Formula display caret.
	 *
	 * @author Gene Pavlovsky
	 */
	public class Caret extends Label {
		public static const MODE_STYLE_NAME_INSERT:String = 'fxcalc-caret-insert';
		public static const MODE_STYLE_NAME_REPLACE:String = 'fxcalc-caret-replace';
		
		public static var globalStyleProvider:IStyleProvider;
		override protected function get defaultStyleProvider():IStyleProvider {
			return globalStyleProvider;
		}
		
		public function Caret() {
			super();
			touchable = false;
			insertMode = true;
		}
		
		public function get insertMode():Boolean {
			return _modeStyleName == MODE_STYLE_NAME_INSERT;
		}
		
		public function set insertMode(value:Boolean):void {
			modeStyleName = value ? MODE_STYLE_NAME_INSERT : MODE_STYLE_NAME_REPLACE;
		}
		
		private var _blinkRate:Number = 500;
		/**
		 * The blinking rate in milliseconds. If set to 0, the caret will not blink.
		 */
		public function get blinkRate():Number {
			return _blinkRate;
		}
		
		public function set blinkRate(value:Number):void {
			_blinkRate = value;
			updateBlink();
		}
		
		override public function set visible(value:Boolean):void {
			super.visible = value;
			updateBlink();
		}
		
		override protected function initialize():void {
			super.initialize();
			updateBlink();
		}
		
		private var _modeStyleName:String;
		
		private function get modeStyleName():String {
			return _modeStyleName;
		}
		
		private function set modeStyleName(value:String):void {
			if (_modeStyleName)
				styleNameList.remove(_modeStyleName);
			_modeStyleName = value;
			if (_modeStyleName)
				styleNameList.add(_modeStyleName);
		}
		
		private var blinkTimer:Timer;
		
		public function updateBlink():void {
			alpha = 1;
			if (!visible || !isInitialized || !_blinkRate) {
				if (blinkTimer)
					blinkTimer.stop();
				return;
			}
			
			if (!blinkTimer) {
				blinkTimer = new Timer(_blinkRate);
				blinkTimer.addEventListener(TimerEvent.TIMER, onBlinkTimer);
			}
			else {
				blinkTimer.reset();
				blinkTimer.delay = _blinkRate;
			}
			blinkTimer.start();
		}
		
		private function onBlinkTimer(e:TimerEvent):void {
			alpha = alpha ? 0 : 1;
		}
	}
}
