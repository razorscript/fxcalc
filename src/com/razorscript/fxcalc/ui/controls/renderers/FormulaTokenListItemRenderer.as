package com.razorscript.fxcalc.ui.controls.renderers {
	import com.razorscript.fxcalc.data.constants.MathConst;
	import com.razorscript.fxcalc.ui.controls.FormulaRenderer;
	import com.razorscript.fxcalc.math.tokens.FormulaToken;
	import com.razorscript.math.expression.MathFun;
	import com.razorscript.math.expression.MathOperator;
	import com.razorscript.math.expression.tokens.OpAssignToken;
	import com.razorscript.math.expression.tokens.MetaToken;
	import com.razorscript.math.expression.tokens.IdentifierToken;
	import com.razorscript.math.expression.tokens.MathToken;
	import com.razorscript.math.expression.tokens.NumberCharToken;
	import com.razorscript.math.expression.tokens.ValueToken;
	import com.razorscript.math.expression.tokens.OperatorToken;
	import feathers.controls.Label;
	import feathers.skins.IStyleProvider;
	import flash.geom.Point;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	
	/**
	 * Renders a formula token.
	 *
	 * @author Gene Pavlovsky
	 */
	
	/** Dispatched when the user taps or clicks the renderer. */
	[Event(name="triggered", type="starling.events.Event")]
	public class FormulaTokenListItemRenderer extends Label {
		public static const SYNTAX_STYLE_NAME_NUMBER:String = 'fxcalc-formula-token-list-item-renderer-number';
		public static const SYNTAX_STYLE_NAME_VARIABLE:String = 'fxcalc-formula-token-list-item-renderer-variable';
		public static const SYNTAX_STYLE_NAME_CONSTANT:String = 'fxcalc-formula-token-list-item-renderer-constant';
		public static const SYNTAX_STYLE_NAME_OPERATOR:String = 'fxcalc-formula-token-list-item-renderer-operator';
		public static const SYNTAX_STYLE_NAME_OP_ASSIGN:String = 'fxcalc-formula-token-list-item-renderer-operator-assign';
		public static const SYNTAX_STYLE_NAME_FUNCTION:String = 'fxcalc-formula-token-list-item-renderer-function';
		public static const SYNTAX_STYLE_NAME_MATCHING_BRACKET:String = 'fxcalc-formula-token-list-item-renderer-matching-bracket';
		public static const SYNTAX_STYLE_NAME_MISMATCHED_BRACKET:String = 'fxcalc-formula-token-list-item-renderer-mismatched-bracket';
		public static const SYNTAX_STYLE_NAME_SPECIAL:String = 'fxcalc-formula-token-list-item-renderer-special';
		public static const SYNTAX_STYLE_NAME_BLANK:String = 'fxcalc-formula-token-list-item-renderer-blank';
		public static const SYNTAX_STYLE_NAME_ERROR:String = 'fxcalc-formula-token-list-item-renderer-error';
		
		public static var globalStyleProvider:IStyleProvider;
		override protected function get defaultStyleProvider():IStyleProvider {
			return globalStyleProvider;
		}
		
		public function FormulaTokenListItemRenderer() {
			super();
		}
		
		private var _data:FormulaToken;
		/**
		 * An item from the owner's data provider.
		 */
		public function get data():FormulaToken {
			return _data;
		}
		
		public function set data(value:FormulaToken):void {
			_data = value;
			commitData();
		}
		
		private var _index:int;
		/**
		 * The index of the item within the owner's data provider.
		 */
		public function get index():int {
			return _index;
		}
		
		public function set index(value:int):void {
			_index = value;
		}
		
		private var _owner:FormulaRenderer;
		/**
		 * The container that contains this item renderer.
		 */
		public function get owner():FormulaRenderer {
			return _owner;
		}
		
		public function set owner(value:FormulaRenderer):void {
			_owner = value;
		}
		
		override protected function initialize():void {
			super.initialize();
			
			addEventListener(TouchEvent.TOUCH, onTouch);
			addEventListener(Event.REMOVED_FROM_STAGE, onRemovedFromStage);
		}
		
		private var _syntaxStyleName:String;
		
		private function get syntaxStyleName():String {
			return _syntaxStyleName;
		}
		
		private function set syntaxStyleName(value:String):void {
			if (_syntaxStyleName)
				styleNameList.remove(_syntaxStyleName);
			_syntaxStyleName = value;
			if (_syntaxStyleName)
				styleNameList.add(_syntaxStyleName);
		}
		
		private function commitData():void {
			if (!_data) {
				setDataProperties(null, null);
				return;
			}
			
			var token:MathToken = _data.token;
			var type:int = token.type;
			if (type == MathToken.VALUE)
				setDataProperties(SYNTAX_STYLE_NAME_NUMBER, (token as ValueToken).value.toString());
			else if (type == MathToken.NUMBER_CHAR)
				setDataProperties(SYNTAX_STYLE_NAME_NUMBER, (token as NumberCharToken).value);
			else if (type == MathToken.VARIABLE)
				setDataProperties(SYNTAX_STYLE_NAME_VARIABLE, (token as IdentifierToken).name);
			else if (type == MathToken.CONSTANT)
				setDataProperties(SYNTAX_STYLE_NAME_CONSTANT, formatConstant((token as IdentifierToken).name));
			else if (type == MathToken.OPERATOR)
				setDataProperties(SYNTAX_STYLE_NAME_OPERATOR, formatOperator((token as OperatorToken).operator));
			else if (type == MathToken.OP_ASSIGN)
				setDataProperties(SYNTAX_STYLE_NAME_OP_ASSIGN, formatOpAssign((token as OpAssignToken).operator, (token as OpAssignToken).varName));
			else if (type == MathToken.FUN_JUXTA)
				setDataProperties(SYNTAX_STYLE_NAME_FUNCTION, formatFunJuxta((token as OperatorToken).operator));
			else if (type == MathToken.FUN_PAREN)
				setDataProperties(SYNTAX_STYLE_NAME_FUNCTION, (token as OperatorToken).operator); // TODO: Do any of the functions with parens need formatting?
			else if (type == MathToken.BLANK)
				setDataProperties(SYNTAX_STYLE_NAME_BLANK, (token as MetaToken).value);
			else if (type == MathToken.ERROR)
				setDataProperties(SYNTAX_STYLE_NAME_ERROR, (token as MetaToken).value);
			else if (_data.flags & FormulaToken.MATCHING_BRACKET)
				setDataProperties(SYNTAX_STYLE_NAME_MATCHING_BRACKET, token.typeShortName);
			else if (_data.flags & FormulaToken.MISMATCHED_BRACKET)
				setDataProperties(SYNTAX_STYLE_NAME_MISMATCHED_BRACKET, token.typeShortName);
			else
				setDataProperties(SYNTAX_STYLE_NAME_SPECIAL, token.typeShortName);
		}
		
		private function setDataProperties(syntaxStyleName:String, text:String):void {
			this.text = text;
			this.syntaxStyleName = syntaxStyleName;
		}
		
		private static const CONST_VALUE:Object = createConstValue();
		private static function createConstValue():Object {
			var obj:Object = {};
			obj[MathConst.CONST_NAME_PI] = 'π';
			return obj;
		}
		
		private function formatConstant(name:String):String {
			return (name in CONST_VALUE) ? CONST_VALUE[name] : name;
		}
		
		private static const OP_VALUE:Object = createOpValue();
		private static function createOpValue():Object {
			var obj:Object = {};
			return obj;
		}
		
		private function formatOperator(operator:String):String {
			if ((operator == MathOperator.SUBTRACT_ALT) && (_data.flags & FormulaToken.UNARY_MINUS))
				operator = MathOperator.SUBTRACT;
			return (operator in OP_VALUE) ? OP_VALUE[operator] : operator;
		}
		
		private function formatOpAssign(operator:String, varName:String):String {
			if (operator == MathOperator.RASSIGN)
				return MathOperator.RASSIGN + varName;
			else if (operator == MathOperator.RASSIGN_ADD)
				return MathOperator.RASSIGN + varName + MathConst.OP_ADD;
			else if (operator == MathOperator.RASSIGN_SUBTRACT)
				return MathOperator.RASSIGN + varName + MathConst.OP_SUBTRACT;
			else if (operator == MathOperator.ASSIGN)
				return (varName ? varName : '') + MathOperator.ASSIGN; // TODO: Refactor entry of assignment operators (must have varName), then remove the condition.
			throw new Error('Unsupported assignment operator: ' + operator + '.');
		}
		
		/** These functions don't need a space before their argument, if the argument is juxtaposed. */
		private static const FUN_VALUE:Object = createFunValue();
		private static function createFunValue():Object {
			var obj:Object = {};
			obj[MathFun.SQRT] = '√';
			obj[MathFun.CURT] = '³√';
			obj[MathFun.EXP_10] = '10ⁿ';
			obj[MathFun.EXP] = 'eⁿ';
			return obj;
		}
		
		/** These functions need a space before their argument, if the argument is juxtaposed. */
		private static const FUN_VALUE_SPACE:Object = createFunValueSpace();
		private static function createFunValueSpace():Object {
			var obj:Object = {};
			obj[MathFun.ARCSIN] = 'sin⁻¹';
			obj[MathFun.ARCSINH] = 'sinh⁻¹';
			obj[MathFun.ARCCOS] = 'cos⁻¹';
			obj[MathFun.ARCCOSH] = 'cosh⁻¹';
			obj[MathFun.ARCTAN] = 'tan⁻¹';
			obj[MathFun.ARCTANH] = 'tanh⁻¹';
			return obj;
		}
		
		private function formatFunJuxta(fun:String):String {
			// TODO: FUN_VALUE should contain all the functions that don't need a space if the argument is juxtaposed! Refactor this?
			if (fun in FUN_VALUE)
				return FUN_VALUE[fun];
			if (fun in FUN_VALUE_SPACE)
				fun = FUN_VALUE_SPACE[fun];
			// TODO: Add a setting controlling whether to add space.
			return (_data.flags & FormulaToken.FUN_JUXTA_PAREN) ? fun : (fun + ' ');
		}
		
		private static const POINT:Point = new Point();
		
		private var touchID:int = -1;
		
		private function onTouch(e:TouchEvent):void {
			if (!_isEnabled) {
				touchID = -1;
				return;
			}
			
			if (touchID >= 0) {
				var touch:Touch = e.getTouch(this, null, touchID);
				if (touch && (touch.phase == TouchPhase.ENDED)) {
					touch.getLocation(stage, POINT);
					if (contains(stage.hitTest(POINT))) {
						touch.getLocation(this, POINT);
						dispatchEventWith(Event.TRIGGERED, false, POINT);
					}
					touchID = -1;
				}
			}
			else {
				touch = e.getTouch(this, TouchPhase.BEGAN);
				if (touch)
					touchID = touch.id;
			}
		}
		
		private function onRemovedFromStage(e:Event):void {
			touchID = -1;
		}
	}
}
