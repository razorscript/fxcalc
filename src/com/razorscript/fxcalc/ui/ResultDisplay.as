package com.razorscript.fxcalc.ui {
	import com.razorscript.fxcalc.data.Model;
	import com.razorscript.fxcalc.data.constants.MathConst;
	import com.razorscript.fxcalc.events.ViewModelEventType;
	import com.razorscript.math.expression.MathExpression;
	import com.razorscript.utils.Lambda;
	import feathers.controls.Label;
	import feathers.controls.LayoutGroup;
	import feathers.layout.AnchorLayout;
	import feathers.layout.AnchorLayoutData;
	import feathers.skins.IStyleProvider;
	
	/**
	 * Displays the calculation results.
	 *
	 * @author Gene Pavlovsky
	 */
	public class ResultDisplay extends LayoutGroup {
		public static const CHILD_STYLE_NAME_RESULT_DISPLAY_DIGITS_LABEL:String = 'fxcalc-result-display-digits-label';
		public static const CHILD_STYLE_NAME_RESULT_DISPLAY_X10_LABEL:String = 'fxcalc-result-display-x10-label';
		public static const CHILD_STYLE_NAME_RESULT_DISPLAY_EXPONENT_LABEL:String = 'fxcalc-result-display-exponent-label';
		
		public static var globalStyleProvider:IStyleProvider;
		override protected function get defaultStyleProvider():IStyleProvider {
			return globalStyleProvider;
		}
		
		public function ResultDisplay(model:Model, viewModel:ViewModel) {
			super();
			this.model = model;
			this.viewModel = viewModel;
		}
		
		override protected function initialize():void {
			super.initialize();
			
			layout = new AnchorLayout();
			var layoutData:AnchorLayoutData;
			
			exponentLabel = new Label();
			exponentLabel.styleNameList.add(CHILD_STYLE_NAME_RESULT_DISPLAY_EXPONENT_LABEL);
			layoutData = new AnchorLayoutData(0, 0);
			exponentLabel.layoutData = layoutData;
			addChild(exponentLabel);
			// Fix the exponent label width to an integer value that can fit the negative maximum exponent value.
			// TODO: Allow 3-digit exponent.
			exponentLabel.text = '-' + MathConst.MAX_EXPONENT;
			exponentLabel.validate();
			exponentLabel.width = Math.ceil(exponentLabel.width);
			exponentLabel.visible = false;
			
			x10Label = new Label();
			x10Label.styleNameList.add(CHILD_STYLE_NAME_RESULT_DISPLAY_X10_LABEL);
			layoutData = new AnchorLayoutData(NaN, 0, 0);
			layoutData.rightAnchorDisplayObject = exponentLabel;
			x10Label.layoutData = layoutData;
			addChild(x10Label);
			// Fix the x10 label width to an integer value.
			x10Label.validate();
			layoutData.bottom = Math.round(0.25 * x10Label.height);
			x10Label.layoutData = layoutData;
			x10Label.width = Math.ceil(x10Label.width);
			x10Label.visible = false;
			
			// TODO: Use segment or dot matrix display instead? Add support for thousands separator. Don't reserve chars for grouping commas and the decimal point, have them between characters instead. Use fixed character number display that scales to available space.
			digitsLabel = new Label();
			digitsLabel.styleNameList.add(CHILD_STYLE_NAME_RESULT_DISPLAY_DIGITS_LABEL);
			layoutData = new AnchorLayoutData(NaN, 0, NaN, 0);
			// TODO: Rearrange x10 and digits label so that they can somewhat overlap. When exponent is negative, the exponent sign is actually on top of the x10 label.
			layoutData.rightAnchorDisplayObject = x10Label;
			digitsLabel.layoutData = layoutData;
			addChild(digitsLabel);
			// Determine the character width.
			digitsLabel.text = ' ';
			digitsLabel.validate();
			charWidth = Math.ceil(digitsLabel.width);
			// Set the digits label minimum width to an integer value that can fit at least two characters (sign and one significant digit).
			digitsLabel.minWidth = 2 * charWidth;
			
			var invalidateData:Function = Lambda.replace(invalidate, INVALIDATION_FLAG_DATA);
			viewModel.addEventListener(ViewModelEventType.RESULT_CHANGE, invalidateData);
			viewModel.addEventListener(ViewModelEventType.DISPLAY_STATE_CHANGE, invalidateData);
			viewModel.addEventListener(ViewModelEventType.RESULT_DISPLAY_NUMBER_FORMATTER_CHANGE, invalidateData);
		}
		
		override protected function draw():void {
			if (digitsLabel.width && charWidth) {
				// Set maxDigitsChars to the maximum number of characters the digits label can fit, but not exceeding MathExt.MAX_SIGNIFICANT_DIGITS plus 2 extra characters for the sign and the decimal point.
				//maxDigitsChars = Math.min(Math.floor(digitsLabel.width / charWidth), MathExt.DOUBLE_DIGITS + 2);
				// TODO: For testing; remove then next line when done
				//maxDigitsChars = 13; // 1 character for the sign.
				//maxDigitsChars = 11; // 1 character for the sign.
				var newPadding:Number = digitsLabel.width - (maxDigits + 2) * charWidth; // 2 extra characters for the sign and the decimal point.
				if (digitsLabel.paddingLeft != newPadding) {
					digitsLabel.paddingLeft = newPadding;
					invalidate(INVALIDATION_FLAG_DATA);
				}
			}
			
			var isDataInvalid:Boolean = isInvalid(INVALIDATION_FLAG_DATA);
			
			super.draw();
			
			if (isDataInvalid)
				commitData();
		}
		
		private var model:Model;
		private var viewModel:ViewModel;
		private var digitsLabel:Label;
		private var x10Label:Label;
		private var exponentLabel:Label;
		private var charWidth:int;
		private const maxDigits:int = 12; // TODO: Customizable max digits.
		
		private static const NUMBER_RX:RegExp = /^ (-? \d* \.? \d*) (e [-+]? \d+)? $/x;
		
		private function commitData():void {
			/*if (!maxDigitsChars) {
				invalidate(INVALIDATION_FLAG_DATA);
				return;
			}*/
			if (viewModel.displayState != DisplayState.ERROR) {
				var result:MathExpression = viewModel.result;
				if (result && isFinite(result.value)) {
					var rxMatch:Array = NUMBER_RX.exec(viewModel.resultDisplayNumberFormatter.format(result.value));
					digitsLabel.text = rxMatch[1];
					x10Label.visible = exponentLabel.visible = rxMatch[2];
					exponentLabel.text = rxMatch[2] ? rxMatch[2].substr(1) : '';
					return;
				}
			}
			digitsLabel.text = ' ';
			x10Label.visible = exponentLabel.visible = false;
		}
	}
}
