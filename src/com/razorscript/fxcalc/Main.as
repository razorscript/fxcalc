package com.razorscript.fxcalc {
	import com.razorscript.debug.Logger;
	import com.razorscript.events.RazorEventType;
	import com.razorscript.feathers.themes.ThemeManager;
	import com.razorscript.feathers.utils.themes.applyStylesToTree;
	import com.razorscript.fxcalc.controllers.AppController;
	import com.razorscript.fxcalc.controllers.ChangeLogController;
	import com.razorscript.fxcalc.controllers.DisplayController;
	import com.razorscript.fxcalc.controllers.KeypadController;
	import com.razorscript.fxcalc.controllers.PopUpController;
	import com.razorscript.fxcalc.controllers.SettingsController;
	import com.razorscript.fxcalc.controllers.StateController;
	import com.razorscript.fxcalc.controllers.ThemeController;
	import com.razorscript.fxcalc.controllers.supportClasses.AppControllerDeps;
	import com.razorscript.fxcalc.data.ChangeLogModel;
	import com.razorscript.fxcalc.data.Model;
	import com.razorscript.fxcalc.data.SettingsModel;
	import com.razorscript.fxcalc.events.ViewModelEventType;
	import com.razorscript.fxcalc.ui.Display;
	import com.razorscript.fxcalc.ui.Keypad;
	import com.razorscript.fxcalc.ui.ViewModel;
	import feathers.controls.LayoutGroup;
	import feathers.skins.IStyleProvider;
	
	/**
	 * Main class initializes models and controllers, and implements the view.
	 *
	 * @author Gene Pavlovsky
	 */
	public class Main extends LayoutGroup {
		public static var globalStyleProvider:IStyleProvider;
		override protected function get defaultStyleProvider():IStyleProvider {
			return globalStyleProvider;
		}
		
		public var keypad:Keypad;
		public var display:Display;
		
		public function Main() {
			super();
			touchable = false;
		}
		
		override protected function initialize():void {
			Logger.debug(this, 'initialize()');
			super.initialize();
			
			deps = new AppControllerDeps();
			initModel();
			initController();
		}
		
		override protected function draw():void {
			// TODO: Reposition components on resize?
			super.draw();
		}
		
		protected function initModel():void {
			deps.model = model = new Model();
			deps.viewModel = viewModel = new ViewModel();
			viewModel.addEventListener(ViewModelEventType.TOUCH_INPUT_ENABLED_CHANGE, onTouchInputEnabledChange);
			deps.settingsModel = settingsModel = new SettingsModel();
			deps.changeLogModel = changeLogModel = new ChangeLogModel();
		}
		
		protected function initController():void {
			deps.settingsController = settingsController = new SettingsController(settingsModel);
			deps.stateController = stateController = new StateController(model, viewModel);
			deps.changeLogController = changeLogController = new ChangeLogController(changeLogModel);
			deps.popUpController = popUpController = new PopUpController(model, viewModel, settingsModel);
			appController = new AppController(deps);
			appController.addEventListener(RazorEventType.INIT, onAppControllerInit);
			appController.initialize();
		}
		
		protected function onAppControllerInit():void {
			Logger.info(this, 'AppController initialized.');
			ThemeManager.instance.addEventListener(RazorEventType.VALIDATE, onThemeValidate);
			themeController = new ThemeController(model, settingsModel);
			themeController.addEventListener(RazorEventType.INIT, onThemeControllerInit);
			themeController.initialize();
		}
		
		protected function onThemeControllerInit():void {
			popUpController.setAppController(appController);
			keypadController = new KeypadController(model, viewModel, settingsModel, appController, popUpController);
			displayController = new DisplayController(model, viewModel, settingsModel, appController, popUpController);
			initView();
			appController.loadState();
		}
		
		protected function initView():void {
			// Set the style provider for this object. This object had been instantiated before the theme was loaded, so it's style provider was not set yet.
			styleProvider = globalStyleProvider;
			display = new Display(model, viewModel, settingsModel, displayController, popUpController);
			keypad = new Keypad(model, viewModel, settingsModel, keypadController);
			addChild(display);
			addChild(keypad);
			Logger.info(this, 'View created.');
		}
		
		protected function onTouchInputEnabledChange():void {
			touchable = viewModel.touchInputEnabled;
			// TODO: Show/hide throbber.
		}
		
		protected function onThemeValidate():void {
			// TODO: If a new theme is loaded, the existing controls' style providers must be somehow updated to the new theme's style providers.
			applyStylesToTree(this);
		}
		
		protected var deps:AppControllerDeps;
		protected var model:Model;
		protected var viewModel:ViewModel;
		protected var settingsModel:SettingsModel;
		protected var changeLogModel:ChangeLogModel;
		protected var appController:AppController;
		protected var settingsController:SettingsController;
		protected var stateController:StateController;
		protected var changeLogController:ChangeLogController;
		protected var themeController:ThemeController;
		protected var popUpController:PopUpController;
		protected var keypadController:KeypadController;
		protected var displayController:DisplayController;
	}
}
