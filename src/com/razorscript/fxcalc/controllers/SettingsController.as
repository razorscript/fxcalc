package com.razorscript.fxcalc.controllers {
	import com.razorscript.debug.Logger;
	import com.razorscript.events.RazorEventType;
	import com.razorscript.fxcalc.controllers.supportClasses.DataStorageControllerBase;
	import com.razorscript.fxcalc.data.SettingsModel;
	import com.razorscript.fxcalc.data.constants.DataStorageConst;
	import com.razorscript.fxcalc.debug.FXDebug;
	import com.razorscript.io.DataType;
	import com.razorscript.io.IDataStorage;
	import starling.events.Event;
	
	/**
	 * Handles saving and loading the settings.
	 *
	 * @author Gene Pavlovsky
	 */
	public class SettingsController extends DataStorageControllerBase {
		// TODO: Move embed to DataStorageConst? Refactor DataStorageControllerBase. One controller might need many data storages or loaders.
		[Embed(source="/../assets/settings.default.min.json", mimeType="application/octet-stream")]
		protected static const DEFAULT_SETTINGS_JSON:Class;
		
		public function SettingsController(settingsModel:SettingsModel) {
			super();
			this.settingsModel = settingsModel;
		}
		
		public function initialize():void {
			initDataStorage();
			CONFIG::debug {
				if (FXDebug.instance.debugSettings)
					Logger.debug(this, 'dataStorage=' + dataStorage);
			}
			
			clear();
			dispatchEventWith(RazorEventType.INIT);
		}
		
		override protected function createDataStorage():IDataStorage {
			var ds:IDataStorage = _createDataStorage(DataStorageConst.SETTINGS_CLASS, DataStorageConst.SETTINGS_PROPERTIES);
			ds.dataType = DataType.STRING;
			return ds;
		}
		
		public function clear():void {
			CONFIG::debug {
				if (FXDebug.instance.debugSettings)
					Logger.debug(this, 'clear()');
			}
			parseJSON(new DEFAULT_SETTINGS_JSON());
		}
		
		public function reset():void {
			clear();
			save();
		}
		
		public function load():void {
			dataStorage.load();
		}
		
		public function save():void {
			dataStorage.save(JSON.stringify(settingsModel, null, 2));
		}
		
		public function wipe():void {
			dataStorage.wipe();
		}
		
		override protected function onLoad(e:Event):void {
			if (e.data)  {
				parseJSON(e.data as String);
			}
			else {
				CONFIG::debug {
					if (FXDebug.instance.debugSettings)
						Logger.debug(this, 'No user settings.');
				}
			}
			dispatchEvent(e);
		}
		
		override protected function onSave(e:Event):void {
			CONFIG::debug {
				if (FXDebug.instance.debugSettings)
					Logger.debug(this, 'Saved user settings.');
			}
			dispatchEvent(e);
		}
		
		override protected function onWipe(e:Event):void {
			CONFIG::debug {
				if (FXDebug.instance.debugSettings)
					Logger.debug(this, 'Wiped user settings.');
			}
			dispatchEvent(e);
		}
		
		protected function parseJSON(text:String):void {
			parseJSONTo(text, settingsModel);
			CONFIG::debug {
				if (FXDebug.instance.debugSettings) {
					Logger.debug(this, (FXDebug.instance.debugSettings == FXDebug.DEBUG_VERBOSE) ?
						('Loaded settings:\n' + JSON.stringify(settingsModel, null, 2)) : 'Loaded settings.');
				}
			}
		}
		
		protected var settingsModel:SettingsModel;
	}
}
