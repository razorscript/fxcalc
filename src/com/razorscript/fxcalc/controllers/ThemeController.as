package com.razorscript.fxcalc.controllers {
	import com.razorscript.debug.Logger;
	import com.razorscript.events.RazorEventType;
	import com.razorscript.feathers.themes.ITheme;
	import com.razorscript.feathers.themes.ThemeManager;
	import com.razorscript.fxcalc.data.Model;
	import com.razorscript.fxcalc.data.SettingsModel;
	import com.razorscript.fxcalc.data.settings.ui.ThemeUISettings;
	import com.razorscript.fxcalc.events.settings.ui.ThemeUISettingsEventType;
	import com.razorscript.fxcalc.ui.themes.FXCalcRazorTheme;
	import starling.events.EventDispatcher;
	
	/**
	 * Handles switching themes, theme styles and color schemes.
	 *
	 * @author Gene Pavlovsky
	 */
	public class ThemeController extends EventDispatcher {
		public function ThemeController(model:Model, settingsModel:SettingsModel) {
			this.model = model;
			this.settingsModel = settingsModel;
		}
		
		public function initialize():void {
			ThemeManager.instance.addEventListener(RazorEventType.LOAD, onFirstThemeLoad);
			var themeSettings:ThemeUISettings = settingsModel.ui.theme;
			themeSettings.addEventListener(ThemeUISettingsEventType.NAME_CHANGE, onThemeNameChange);
			themeSettings.addEventListener(ThemeUISettingsEventType.STYLE_NAME_CHANGE, onStyleNameChange);
			themeSettings.addEventListener(ThemeUISettingsEventType.COLOR_SCHEME_NAME_CHANGE, onColorSchemeNameChange);
			onThemeNameChange();
		}
		
		protected const FALLBACK_THEME_FACTORY:Function = createRazorTheme;
		
		protected const themeNameFactories:Object = {
			'Razor': createRazorTheme};
		
		protected function createRazorTheme(settings:ThemeUISettings):ITheme {
			return new FXCalcRazorTheme(settings.styleName, settings.colorSchemeName);
		}
		
		protected function onFirstThemeLoad():void {
			// TODO: Revisit after refactoring ThemeBase/RazorThemeBase initialize(): listen to VALIDATE event instead?
			ThemeManager.instance.removeEventListener(RazorEventType.LOAD, onFirstThemeLoad);
			ThemeManager.instance.addEventListener(RazorEventType.LOAD, onThemeLoad);
			dispatchEventWith(RazorEventType.INIT);
		}
		
		protected function onThemeLoad():void {
			ThemeManager.instance.theme.dispatchEventWith(RazorEventType.VALIDATE);
		}
		
		protected function onThemeNameChange():void {
			var themeSettings:ThemeUISettings = settingsModel.ui.theme;
			if (themeName == themeSettings.name)
				return;
			themeName = themeSettings.name;
			var themeFactory:Function = themeNameFactories[themeSettings.name];
			if (!themeFactory) {
				// TODO: Show error alert?
				Logger.error(this, 'Unknown theme:', themeSettings.name);
				if (ThemeManager.instance.theme)
					return;
				themeFactory = FALLBACK_THEME_FACTORY;
			}
			ThemeManager.instance.theme = themeFactory(themeSettings);
		}
		
		protected function onStyleNameChange():void {
			ThemeManager.instance.theme.styleName = settingsModel.ui.theme.styleName;
		}
		
		protected function onColorSchemeNameChange():void {
			ThemeManager.instance.theme.colorSchemeName = settingsModel.ui.theme.colorSchemeName;
		}
		
		protected var model:Model;
		protected var settingsModel:SettingsModel;
		protected var themeName:String;
	}
}
