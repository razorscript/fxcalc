package com.razorscript.fxcalc.controllers {
	import com.razorscript.debug.Logger;
	import com.razorscript.events.RazorEventType;
	import com.razorscript.fxcalc.controllers.supportClasses.DataStorageControllerBase;
	import com.razorscript.fxcalc.data.Model;
	import com.razorscript.fxcalc.data.StateConverter;
	import com.razorscript.fxcalc.data.constants.DataStorageConst;
	import com.razorscript.fxcalc.data.dto.StateDTO;
	import com.razorscript.fxcalc.debug.FXDebug;
	import com.razorscript.fxcalc.ui.ViewModel;
	import com.razorscript.io.DataType;
	import com.razorscript.io.IDataStorage;
	import starling.events.Event;
	
	/**
	 * Handles saving and loading the app state.
	 *
	 * @author Gene Pavlovsky
	 */
	public class StateController extends DataStorageControllerBase {
		public function StateController(model:Model, viewModel:ViewModel) {
			super();
			this.model = model;
			this.viewModel = viewModel;
		}
		
		public function initialize():void {
			initDataStorage();
			CONFIG::debug {
				if (FXDebug.instance.debugState)
					Logger.debug(this, 'dataStorage=' + dataStorage);
			}
			
			stateConverter = new StateConverter(model, viewModel);
			dispatchEventWith(RazorEventType.INIT);
		}
		
		override protected function createDataStorage():IDataStorage {
			var ds:IDataStorage = _createDataStorage(DataStorageConst.STATE_CLASS, DataStorageConst.STATE_PROPERTIES);
			ds.dataType = DataType.STRING;
			return ds;
		}
		
		public function load():void {
			dataStorage.load();
		}
		
		public function save():void {
			var stateDTO:StateDTO = stateConverter.saveState();
			dataStorage.save(JSON.stringify(stateDTO, null, 2));
		}
		
		public function wipe():void {
			dataStorage.wipe();
		}
		
		override protected function onLoad(e:Event):void {
			if (e.data)  {
				parseJSON(e.data as String);
			}
			else {
				CONFIG::debug {
					if (FXDebug.instance.debugState)
						Logger.debug(this, 'No app state.');
				}
			}
			dispatchEvent(e);
		}
		
		override protected function onSave(e:Event):void {
			CONFIG::debug {
				if (FXDebug.instance.debugState)
					Logger.debug(this, 'Saved app state.');
			}
			dispatchEvent(e);
		}
		
		override protected function onWipe(e:Event):void {
			CONFIG::debug {
				if (FXDebug.instance.debugState)
					Logger.debug(this, 'Wiped app state.');
			}
			dispatchEvent(e);
		}
		
		protected function parseJSON(text:String):void {
			var stateDTO:StateDTO = new StateDTO();
			parseJSONTo(text, stateDTO);
			stateConverter.restoreState(stateDTO);
			CONFIG::debug {
				if (FXDebug.instance.debugState) {
					Logger.debug(this, (FXDebug.instance.debugState == FXDebug.DEBUG_VERBOSE) ?
						('Loaded app state:\n' + JSON.stringify(stateDTO, null, 2)) : 'Loaded app state.');
				}
			}
		}
		
		protected var model:Model;
		protected var viewModel:ViewModel;
		protected var stateConverter:StateConverter;
	}
}
