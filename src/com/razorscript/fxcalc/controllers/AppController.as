package com.razorscript.fxcalc.controllers {
	import com.razorscript.app.Version;
	import com.razorscript.debug.Debug;
	import com.razorscript.debug.LogLevel;
	import com.razorscript.debug.Logger;
	import com.razorscript.events.RazorEventType;
	import com.razorscript.fxcalc.controllers.supportClasses.AppControllerDeps;
	import com.razorscript.fxcalc.data.CalcMode;
	import com.razorscript.fxcalc.data.ChangeLogModel;
	import com.razorscript.fxcalc.data.Model;
	import com.razorscript.fxcalc.data.SettingsModel;
	import com.razorscript.fxcalc.data.changeLog.ChangeLogEntry;
	import com.razorscript.fxcalc.data.constants.AppConst;
	import com.razorscript.fxcalc.data.constants.MathConst;
	import com.razorscript.fxcalc.data.settings.MathSettings;
	import com.razorscript.fxcalc.debug.FXDebug;
	import com.razorscript.fxcalc.events.ModelEventType;
	import com.razorscript.fxcalc.events.ViewModelEventType;
	import com.razorscript.fxcalc.events.settings.MathSettingsEventType;
	import com.razorscript.fxcalc.math.LiveSyntaxChecker;
	import com.razorscript.fxcalc.math.lexers.FormulaTokenVectorMathTokenProvider;
	import com.razorscript.fxcalc.math.tokens.PooledFormulaTokenFactory;
	import com.razorscript.fxcalc.ui.DisplayState;
	import com.razorscript.fxcalc.ui.ResultDisplayFormat;
	import com.razorscript.fxcalc.ui.ViewModel;
	import com.razorscript.math.AngleUnit;
	import com.razorscript.math.FPMode;
	import com.razorscript.math.MathContext;
	import com.razorscript.math.MathExt;
	import com.razorscript.math.NumberFormatter;
	import com.razorscript.math.RoundingMode;
	import com.razorscript.math.errors.ArithmeticError;
	import com.razorscript.math.expression.MathExpression;
	import com.razorscript.math.expression.MathFun;
	import com.razorscript.math.expression.MathOperator;
	import com.razorscript.math.expression.MathParser;
	import com.razorscript.math.expression.lexers.MathTokenLexer;
	import com.razorscript.math.expression.tokens.IMathTokenFactory;
	import com.razorscript.math.expression.tokens.PooledMathTokenFactory;
	import com.razorscript.math.functions.MathLib;
	import com.razorscript.math.functions.polymorphism.TypeConverter;
	import com.razorscript.math.objects.ComplexNumber;
	import com.razorscript.math.objects.NumberFun0;
	import com.razorscript.math.objects.conversions.TypeConversionFun;
	import com.razorscript.utils.Lambda;
	import com.razorscript.utils.ObjUtil;
	import com.razorscript.utils.StrUtil;
	import com.razorscript.utils.TimerUtil;
	import com.razorscript.utils.objectParser.conversions.ObjectParserConversionFun;
	import starling.events.Event;
	import starling.events.EventDispatcher;
	
	/**
	 * Main app controller.
	 *
	 * @author Gene Pavlovsky
	 */
	public class AppController extends EventDispatcher {
		public function AppController(deps:AppControllerDeps) {
			model = deps.model;
			viewModel = deps.viewModel;
			settingsModel = deps.settingsModel;
			changeLogModel = deps.changeLogModel;
			
			settingsController = deps.settingsController;
			stateController = deps.stateController;
			changeLogController = deps.changeLogController;
			popUpController = deps.popUpController;
		}
		
		public function clearAll():void {
			clearMode();
			clearMemory(true);
			clearHistory();
			clearFormula();
		}
		
		public function clearAC():void {
			clearFormula();
			viewModel.result = null;
		}
		
		public function clearMode():void {
			// TODO: Get this from settings.
			model.calcMode = CalcMode.COMPUTE;
			model.angleUnitMode = settingsModel.math.defaultAngleUnit;
			// TODO: Get these from settings.
			// TODO: Refactor - enum values shouldn't contain digits.
			ResultDisplayFormat.NORMAL_1.digits = ResultDisplayFormat.defaultDigitsNormal;
			ResultDisplayFormat.FIXED.digits = ResultDisplayFormat.defaultDigitsFixed;
			ResultDisplayFormat.SCIENTIFIC.digits = ResultDisplayFormat.defaultDigitsScientific;
			ResultDisplayFormat.ENGINEERING.digits = ResultDisplayFormat.defaultDigitsEngineering;
			viewModel.resultDisplayFormat = ResultDisplayFormat.defaultValue;
			viewModel.insertMode = true;
		}
		
		public function clearMemory(clearAnswer:Boolean = false):void {
			model.mathParser.mathContext.clearAllVariables();
			model.memoryVariable = 0;
			if (clearAnswer)
				model.answerVariable = 0;
		}
		
		public function clearHistory():void {
			model.removeAllHistoryItems();
			model.answerVariable = 0;
			viewModel.historyIndex = 0;
			viewModel.result = null;
		}
		
		public function clearFormula():void {
			model.clearFormula();
			moveCaretHome();
		}
		
		protected function moveCaretHome():void {
			viewModel.caretIndex = 0;
			if (!viewModel.isEditable)
				viewModel.displayState = DisplayState.INPUT;
		}
		
		// DEBUG: Testing. (?)
		public function loadSettings():void {
			settingsController.load();
		}
		public function saveSettings():void {
			settingsController.save();
		}
		public function resetSettings():void {
			settingsController.reset();
		}
		public function wipeSettings():void {
			settingsController.clear();
			settingsController.wipe();
		}
		
		// DEBUG: Testing. (?)
		public function loadState():void {
			viewModel.touchInputEnabled = viewModel.keyboardInputEnabled = false;
			stateController.load();
		}
		public function saveState():void {
			stateController.save();
		}
		public function wipeState():void {
			stateController.wipe();
		}
		
		/***********************************************************************************************************************************************/
		
		//{ region Initialization
		// TODO: Use regions.
		/**
		 * Initialization.
		 */
		
		public function initialize():void {
			initDebug();
			initObjectParser();
			initSettingsController();
		}
		
		CONFIG::release
		protected function initDebug():void {
			Logger.severityLevels = LogLevel.none();
			// TODO: Add some level of logging and debugging in release version as well (to include diagnostic info in user error reports).
		}
		
		CONFIG::debug
		protected function initDebug():void {
			var debug:FXDebug;
			Debug.instance = FXDebug.instance = debug = new FXDebug();
			debug.debugMathParser = Debug.DEBUG_MATH_PARSER_PARSE | Debug.DEBUG_MATH_PARSER_EVAL;
			//debug.debugPooling = true;
			debug.debugDataStorage = true;
			debug.debugBinUtil = true;
			debug.debugChangeLog = FXDebug.DEBUG_NORMAL;
			debug.debugSettings = FXDebug.DEBUG_NORMAL;
			debug.debugState = FXDebug.DEBUG_NORMAL;
		}
		
		protected static const ENUM_CLASSES:Vector.<Class> = new <Class>[AngleUnit, FPMode, RoundingMode, CalcMode, DisplayState, ResultDisplayFormat];
		
		protected function initObjectParser():void {
			CONFIG::debug {
				ObjUtil.objectParser.strict = true;
			}
			ObjectParserConversionFun.extend(ObjUtil.objectParser, ENUM_CLASSES);
		}
		
		protected function initSettingsController():void {
			settingsController.addEventListener(RazorEventType.INIT, onSettingsControllerInit);
			settingsController.addEventListener(RazorEventType.LOAD_ERROR, onSettingsLoadError);
			settingsController.addEventListener(RazorEventType.SAVE_ERROR, onSettingsSaveError);
			settingsController.addEventListener(RazorEventType.WIPE_ERROR, onSettingsWipeError);
			settingsController.addEventListener(RazorEventType.PARSE_ERROR, onSettingsParseError);
			settingsController.initialize();
		}
		
		protected function onSettingsControllerInit():void {
			// One-shot: Continue app init even if loading settings fails.
			settingsController.addEventListener(RazorEventType.LOAD, onSettingsLoad);
			settingsController.addEventListener(RazorEventType.LOAD_ERROR, onSettingsLoad);
			settingsController.load();
		}
		
		protected function onSettingsLoad():void {
			settingsController.removeEventListener(RazorEventType.LOAD, onSettingsLoad);
			settingsController.removeEventListener(RazorEventType.LOAD_ERROR, onSettingsLoad);
			
			stateController.addEventListener(RazorEventType.INIT, onStateControllerInit);
			stateController.addEventListener(RazorEventType.LOAD, onStateLoad);
			// One-shot: Check if app was updated on first state load.
			stateController.addEventListener(RazorEventType.LOAD, onStateFirstLoad);
			stateController.addEventListener(RazorEventType.LOAD_ERROR, onStateLoadError);
			stateController.addEventListener(RazorEventType.SAVE_ERROR, onStateSaveError);
			stateController.addEventListener(RazorEventType.WIPE_ERROR, onStateWipeError);
			stateController.addEventListener(RazorEventType.PARSE_ERROR, onStateParseError);
			stateController.initialize();
		}
		
		protected function onStateControllerInit():void {
			initSyntaxChecker();
			createNumberFormatters();
			addEventListeners();
			initModel();
			dispatchEventWith(RazorEventType.INIT);
		}
		
		protected function addEventListeners():void {
			/// SettingsModel / Math events.
			settingsModel.math.addEventListener(MathSettingsEventType.LENIENT_BRACKET_CHECKING_CHANGE, onMathSettingsLenientBracketCheckingChange);
			settingsModel.math.addEventListener(MathSettingsEventType.NUMBER_ALLOW_LEADING_EXP_CHANGE, onMathSettingsNumberAllowLeadingExpChange);
			settingsModel.math.addEventListener(MathSettingsEventType.NUMBER_ALLOW_SOLO_RADIX_CHANGE, onMathSettingsNumberAllowSoloRadixChange);
			
			/// Model events.
			model.addEventListener(ModelEventType.FORMULA_CHANGE, onFormulaChange);
			model.addEventListener(ModelEventType.TOKENS_CHANGE, onTokensChange);
			model.addEventListener(ModelEventType.HISTORY_CHANGE, onHistoryChange);
			model.addEventListener(ModelEventType.RESULT_CHANGE, onResultChange);
			
			/// ViewModel events.
			viewModel.addEventListener(ViewModelEventType.DISPLAY_STATE_CHANGE, onDisplayStateChange);
			viewModel.addEventListener(ViewModelEventType.IS_EDITABLE_CHANGE, onIsEditableChange);
			viewModel.addEventListener(ViewModelEventType.CARET_INDEX_CHANGE, onCaretIndexChange);
			viewModel.addEventListener(ViewModelEventType.HISTORY_INDEX_CHANGE, onHistoryIndexChange);
			viewModel.addEventListener(ViewModelEventType.RESULT_DISPLAY_FORMAT_CHANGE, onResultDisplayFormatChange);
		}
		
		protected function initSyntaxChecker():void {
			syntaxChecker = new LiveSyntaxChecker();
		}
		
		protected function initModel():void {
			initMathParser();
			clearAll();
			viewModel.nullResult = MathExpression.fromValue(0);
			//viewModel.nullResult = null; // TODO: add a user setting to toggle showing "0" or nothing.
			
			/*CONFIG::debug {
				model.mathTokenProvider.setSource(model.formula.tokens);
				stringConverter.convert('sin 1.25e-30 + cos(2.77e+25) - pi + e - Ans + M + xxx / 10.2', model.mathTokenProvider);
				model.dispatchEventWith(ModelEventType.TOKENS_CHANGE);
			}*/
		}
		
		protected function initMathParser():void {
			var mathSettings:MathSettings = settingsModel.math;
			
			var mathContext:MathContext = createMathContext();
			
			var typeConverter:TypeConverter = TypeConversionFun.extend(new TypeConverter());
			var mathLib:MathLib = new MathLib(mathContext, typeConverter);
			var mathFun:MathFun = new MathFun(mathContext, mathLib);
			var mathOperator:MathOperator = new MathOperator(mathFun);
			
			var lexer:MathTokenLexer = new MathTokenLexer();
			lexer.lenientBracketChecking = mathSettings.lenientBracketChecking;
			lexer.numberAllowLeadingExp = mathSettings.numberAllowLeadingExp;
			lexer.numberAllowSoloRadix = mathSettings.numberAllowSoloRadix;
			
			var tokenFactory:IMathTokenFactory = new PooledMathTokenFactory();
			var mathParser:MathParser = model.mathParser = syntaxChecker.mathParser = new MathParser(mathContext, mathFun, mathOperator, lexer, valueTransform, tokenFactory);
			model.mathTokenProvider = syntaxChecker.mathTokenProvider = new FormulaTokenVectorMathTokenProvider();
			model.formulaTokenFactory = new PooledFormulaTokenFactory();
			
			CONFIG::debug {
				mathContext.setVariable('rand3', new NumberFun0(Lambda.fix(mathLib.probabilityMath.random, 3)));
				//stringConverter = new StringConverter(mathContext, mathFun, mathParser.mathOperator, mathParser.tokenFactory, StringConverter.KEEP_WHITESPACE | StringConverter.USE_ERROR_TOKENS);
			}
		}
		
		//CONFIG::debug
		//protected var stringConverter:StringConverter;
		
		protected function createMathContext():MathContext {
			var mathContext:MathContext = new MathContext(RoundingMode.HALF_AWAY_FROM_ZERO, AngleUnit.DEGREE, 2.2e-322, 1e-14, FPMode.MATHIC);
			// TODO: Use separate variables objects and switch them on calculation mode change (real<->complex). Create the new Number/ComplexNumber values on mode change?
			mathContext.setConstant(MathConst.CONST_NAME_PI, Math.PI);
			mathContext.setConstant(MathConst.CONST_NAME_E, Math.E);
			// TODO: Add/remove the `I` constant on calculation mode change (real<->complex)?
			mathContext.setConstant(MathConst.CONST_NAME_I, new ComplexNumber(0, 1));
			mathContext.setVariable(MathConst.VAR_NAME_MEMORY, 0);
			mathContext.setVariable(MathConst.VAR_NAME_ANSWER, 0);
			mathContext.protectVariables(MathConst.VAR_NAME_MEMORY, MathConst.VAR_NAME_ANSWER);
			for each (var varName:String in MathConst.VAR_NAMES)
				mathContext.setVariable(varName, 0);
			return mathContext;
		}
		//} endregion
		
		/***********************************************************************************************************************************************/
		
		/**
		 * SettingsController events.
		 */
		
		protected function onSettingsLoadError(e:Event):void {
			// TODO: Show error alert.
			Logger.error(this, 'Failed to load settings:', e.data);
		}
		
		protected function onSettingsSaveError(e:Event):void {
			// TODO: Show error alert.
			Logger.error(this, 'Failed to save settings:', e.data);
		}
		
		protected function onSettingsWipeError(e:Event):void {
			// TODO: Show error alert.
			Logger.error(this, 'Failed to wipe settings:', e.data);
		}
		
		protected function onSettingsParseError(e:Event):void {
			// TODO: Show error alert?
			// TODO: Set objectParser.strict = false?
			Logger.error(this, 'Failed to parse settings:', e.data);
		}
		
		/***********************************************************************************************************************************************/
		
		/**
		 * SettingsModel / Math events.
		 */
		
		protected function onMathSettingsLenientBracketCheckingChange():void {
			model.mathParser.lexer.lenientBracketChecking = settingsModel.math.lenientBracketChecking;
		}
		
		protected function onMathSettingsNumberAllowLeadingExpChange():void {
			(model.mathParser.lexer as MathTokenLexer).numberAllowLeadingExp = settingsModel.math.numberAllowLeadingExp;
		}
		
		protected function onMathSettingsNumberAllowSoloRadixChange():void {
			(model.mathParser.lexer as MathTokenLexer).numberAllowSoloRadix = settingsModel.math.numberAllowSoloRadix;
		}
		
		/***********************************************************************************************************************************************/
		
		/**
		 * StateController events.
		 */
		
		protected function onStateLoad():void {
			viewModel.touchInputEnabled = viewModel.keyboardInputEnabled = true;
		}
		
		protected function onStateFirstLoad():void {
			stateController.removeEventListener(RazorEventType.LOAD, onStateFirstLoad);
			
			if (model.lastAppVersion < AppConst.VERSION.versionCode) {
				Logger.info(this, 'App updated: ', model.lastAppVersion, '->', AppConst.VERSION.versionCode);
				initChangeLogController();
			}
		}
		
		protected function initChangeLogController():void {
			changeLogController.addEventListener(RazorEventType.INIT, onChangeLogControllerInit);
			changeLogController.addEventListener(RazorEventType.LOAD, onChangeLogLoad);
			changeLogController.addEventListener(RazorEventType.LOAD_ERROR, onChangeLogLoadError);
			changeLogController.addEventListener(RazorEventType.PARSE_ERROR, onChangeLogParseError);
			changeLogController.initialize();
		}
		
		protected function onChangeLogControllerInit():void {
			changeLogController.load();
		}
		
		protected function onStateLoadError(e:Event):void {
			// TODO: Show error alert.
			Logger.error(this, 'Failed to load state:', e.data);
		}
		
		protected function onStateSaveError(e:Event):void {
			// TODO: Show error alert.
			Logger.error(this, 'Failed to save state:', e.data);
		}
		
		protected function onStateWipeError(e:Event):void {
			// TODO: Show error alert.
			Logger.error(this, 'Failed to wipe state:', e.data);
		}
		
		protected function onStateParseError(e:Event):void {
			// TODO: Show error alert?
			// TODO: Set objectParser.strict = false?
			Logger.error(this, 'Failed to parse state:', e.data);
		}
		
		/***********************************************************************************************************************************************/
		
		/**
		 * ChangeLogController events.
		 */
		
		protected function onChangeLogLoad():void {
			// App state persistence and change log were introduced in version 0.2.1 (2001). Prevent showing earlier change log entries.
			var entries:Vector.<ChangeLogEntry> = changeLogModel.getEntries(new Version(Math.max(model.lastAppVersion, 2001) + 1), AppConst.VERSION);
			if (entries.length)
				popUpController.showChangeLog(entries);
		}
		
		protected function onChangeLogLoadError(e:Event):void {
			// TODO: Show error alert.
			Logger.error(this, 'Failed to load change log:', e.data);
		}
		
		protected function onChangeLogParseError(e:Event):void {
			// TODO: Show error alert?
			// TODO: Set objectParser.strict = false?
			Logger.error(this, 'Failed to parse change log:', e.data);
		}
		
		/***********************************************************************************************************************************************/
		
		/**
		 * Model events.
		 */
		
		protected function onFormulaChange():void {
			if (model.formula == model.editableFormula) {
				if (viewModel.historyIndex == viewModel.maxHistoryIndex) {
					// The formula was set to the editable formula by setting the history index.
					moveCaretHome();
				}
				else {
					// The formula was set to the editable formula by `Model.clearFormula()` or `Model.copyFormulaIfImmutable()` (respectively clear or copy-on-write).
					viewModel.historyIndex = viewModel.maxHistoryIndex;
				}
			}
		}
		
		protected function onTokensChange():void {
			viewModel.maxCaretIndex = model.numTokens;
			syntaxChecker.updateFlags(model.tokens);
			// The caret index might be invalid, match brackets after a short delay.
			TimerUtil.remove(matchBracketsDelayedCall);
			matchBracketsDelayedCall = TimerUtil.delayCall(matchBrackets, 20);
			// Check syntax after a period of inactivity.
			TimerUtil.remove(checkSyntaxDelayedCall);
			checkSyntaxDelayedCall = TimerUtil.delayCall(checkSyntax, 250); // TODO: Add a setting for check syntax delay.
		}
		
		protected function onHistoryChange():void {
			viewModel.maxHistoryIndex = model.numHistoryItems;
		}
		
		protected function onResultChange():void {
			// TODO: Add a setting to display result from the last viewed history item, result from the last history item (should == Ans), nullResult.
			if (model.result) {
				// The current formula is a history item.
				viewModel.result = model.result;
				viewModel.displayState = model.result.hasError ? DisplayState.ERROR : DisplayState.RESULT;
			}
			else {
				// The current formula is the editable formula.
				// TODO: Add setting: nullResult / result from the last history item (should == Ans).
				/*
				viewModel.result = null;
				/*/
				var lastHistoryIndex:int = model.numHistoryItems - 1;
				viewModel.result = (lastHistoryIndex >= 0) ? model.getHistoryItemAt(lastHistoryIndex).result : null;
				//*/
			}
		}
		
		/***********************************************************************************************************************************************/
		
		/**
		 * ViewModel events.
		 */
		
		protected function onDisplayStateChange():void {
			viewModel.isEditable = viewModel.displayState == DisplayState.INPUT;
			if (viewModel.isEditable && viewModel.result && viewModel.result.hasError)
				viewModel.result = null;
		}
		
		protected function onIsEditableChange():void {
			// The caret index might be invalid, match brackets after a short delay.
			TimerUtil.remove(matchBracketsDelayedCall);
			matchBracketsDelayedCall = TimerUtil.delayCall(matchBrackets, 20);
		}
		
		protected function onCaretIndexChange():void {
			matchBrackets();
		}
		
		protected function onHistoryIndexChange():void {
			if (viewModel.historyIndex < viewModel.maxHistoryIndex) // The history index points to a history item, not the editable formula.
				model.formula = model.getHistoryItemAt(viewModel.historyIndex);
			else if (model.formula != model.editableFormula) // The history index points to the editable formula and the current formula is not the editable formula.
				model.formula = model.editableFormula;
		}
		
		protected function onResultDisplayFormatChange():void {
			viewModel.resultDisplayNumberFormatter = getNumberFormatter(viewModel.resultDisplayFormat);
		}
		
		/***********************************************************************************************************************************************/
		
		/**
		 * Live syntax checker.
		 */
		
		protected var syntaxChecker:LiveSyntaxChecker;
		protected var matchBracketsDelayedCall:Object;
		protected var checkSyntaxDelayedCall:Object;
		
		protected function matchBrackets():void {
			TimerUtil.remove(matchBracketsDelayedCall);
			matchBracketsDelayedCall = null;
			syntaxChecker.matchBrackets(model.tokens, viewModel.isEditable ? viewModel.caretIndex : -1);
			model.dispatchEventWith(ModelEventType.TOKEN_FLAGS_CHANGE);
		}
		
		protected function checkSyntax():void {
			TimerUtil.remove(checkSyntaxDelayedCall);
			checkSyntaxDelayedCall = null;
			//var result:MathExpression = syntaxChecker.checkSyntax(model.tokens);
			// TODO: Receive and display check syntax errors.
			//model.dispatchEventWith(ModelEventType.TOKEN_FLAGS_CHANGE);
		}
		
		/***********************************************************************************************************************************************/
		
		/**
		 * Result display format.
		 */
		
		protected var normal1NumberFormatter:NumberFormatter;
		protected var normal2NumberFormatter:NumberFormatter;
		protected var fixedNumberFormatter:NumberFormatter;
		protected var scientificNumberFormatter:NumberFormatter;
		protected var engineeringNumberFormatter:NumberFormatter;
		
		protected function createNumberFormatters():void {
			const maxDigits:int = 12; // TODO: Customizable max digits.
			normal1NumberFormatter = NumberFormatter.createNormalNumberFormatter(maxDigits, Math.pow(10, -ResultDisplayFormat.NORMAL_1.digits));
			normal2NumberFormatter = NumberFormatter.createNormalNumberFormatter(maxDigits);
			fixedNumberFormatter = NumberFormatter.createFixedNumberFormatter(maxDigits, ResultDisplayFormat.FIXED.digits);
			scientificNumberFormatter = NumberFormatter.createScientificNumberFormatter(ResultDisplayFormat.SCIENTIFIC.digits);
			engineeringNumberFormatter = NumberFormatter.createEngineeringNumberFormatter(ResultDisplayFormat.ENGINEERING.digits);
		}
		
		protected function getNumberFormatter(format:ResultDisplayFormat):NumberFormatter {
			var nf:NumberFormatter;
			// TODO: Refactor. Add support for settings: max digits, exclude leading zero (never, always, auto - if 0 is in the left-most digit indicator).
			if (format == ResultDisplayFormat.NORMAL_1) {
				nf = normal1NumberFormatter;
				nf.normalLower = Math.pow(10, -format.digits);
			}
			else if (format == ResultDisplayFormat.NORMAL_2) {
				nf = normal2NumberFormatter;
			}
			else if (format == ResultDisplayFormat.FIXED) {
				nf = fixedNumberFormatter;
				nf.fractionDigits = format.digits;
			}
			else if (format == ResultDisplayFormat.SCIENTIFIC) {
				nf = scientificNumberFormatter;
				nf.maxDigits = format.digits;
			}
			else if (format == ResultDisplayFormat.ENGINEERING) {
				nf = engineeringNumberFormatter;
				nf.maxDigits = format.digits;
			}
			nf.exponentWidth = 2;
			return nf;
		}
		
		/***********************************************************************************************************************************************/
		
		/**
		 * Math.
		 */
		
		protected function valueTransform(value:*):* {
			// TODO: Handle other mathematical objects.
			if (value is Number) {
				if (MathExt.isNaN(value))
					throw new ArithmeticError('Invalid operation.');
				var absValue:Number = Math.abs(value);
				// TODO: Add a setting to choose the allowed range.
				if (absValue >= 1e100)
					throw new RangeError('Out of range.');
				if (absValue <= 1e-100)
					return 0;
			}
			return value;
		}
		
		protected var model:Model;
		protected var viewModel:ViewModel;
		protected var settingsModel:SettingsModel;
		protected var changeLogModel:ChangeLogModel;
		
		protected var settingsController:SettingsController;
		protected var stateController:StateController;
		protected var changeLogController:ChangeLogController;
		protected var popUpController:PopUpController;
	}
}
