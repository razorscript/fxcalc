package com.razorscript.fxcalc.controllers {
	import com.razorscript.fxcalc.data.Model;
	import com.razorscript.fxcalc.data.SettingsModel;
	import com.razorscript.fxcalc.data.changeLog.ChangeLogEntry;
	import com.razorscript.fxcalc.ui.ViewModel;
	import com.razorscript.fxcalc.ui.popups.ChangeLogAlertController;
	import com.razorscript.fxcalc.ui.popups.FormulaErrorCalloutController;
	import starling.display.DisplayObject;
	
	/**
	 * Handles displaying pop-ups, alerts and callouts.
	 *
	 * @author Gene Pavlovsky
	 */
	public class PopUpController {
		public function PopUpController(model:Model, viewModel:ViewModel, settingsModel:SettingsModel) {
			this.model = model;
			this.viewModel = viewModel;
			this.settingsModel = settingsModel;
		}
		
		public function setAppController(appController:AppController):void {
			if (this.appController)
				throw new Error('Illegal operation for an already initialized property appController.');
			this.appController = appController;
		}
		
		/***********************************************************************************************************************************************/
		
		/**
		 * Change log.
		 */
		
		public function showChangeLog(entries:Vector.<ChangeLogEntry>):void {
			if (!changeLogAlertController)
				changeLogAlertController = new ChangeLogAlertController(appController);
			changeLogAlertController.show(entries);
		}
		
		protected var changeLogAlertController:ChangeLogAlertController;
		
		/***********************************************************************************************************************************************/
		
		/**
		 * Formula error.
		 */
		
		public function showFormulaError(origin:DisplayObject):void {
			if (!formulaErrorController)
				formulaErrorController = new FormulaErrorCalloutController(viewModel);
			formulaErrorController.show(viewModel.result.errorMessage, origin);
		}
		
		protected var formulaErrorController:FormulaErrorCalloutController;
		
		/***********************************************************************************************************************************************/
		
		protected var model:Model;
		protected var viewModel:ViewModel;
		protected var settingsModel:SettingsModel;
		protected var appController:AppController;
	}
}
