package com.razorscript.fxcalc.controllers {
	import com.razorscript.debug.Logger;
	import com.razorscript.events.RazorEventType;
	import com.razorscript.fxcalc.controllers.supportClasses.DataLoaderControllerBase;
	import com.razorscript.fxcalc.data.ChangeLogModel;
	import com.razorscript.fxcalc.data.constants.DataStorageConst;
	import com.razorscript.fxcalc.debug.FXDebug;
	import com.razorscript.io.DataType;
	import com.razorscript.io.IDataLoader;
	import flash.utils.ByteArray;
	import starling.events.Event;
	
	/**
	 * Handles loading the change log.
	 *
	 * @author Gene Pavlovsky
	 */
	public class ChangeLogController extends DataLoaderControllerBase {
		public function ChangeLogController(changeLogModel:ChangeLogModel) {
			super();
			this.changeLogModel = changeLogModel;
		}
		
		public function initialize():void {
			initDataLoader();
			CONFIG::debug {
				if (FXDebug.instance.debugChangeLog)
					Logger.debug(this, 'dataLoader=' + dataLoader);
			}
			
			dispatchEventWith(RazorEventType.INIT);
		}
		
		override protected function createDataLoader():IDataLoader {
			var ds:IDataLoader = _createDataLoader(DataStorageConst.CHANGE_LOG_CLASS, DataStorageConst.CHANGE_LOG_PROPERTIES);
			// TODO: Use DataType.STRING - blocked by `EmbedDataLoader: Implement data type conversions (@see SharedObjectDataStorage's byteArrayToData() and dataToByteArray()).
			ds.dataType = DataType.BYTE_ARRAY;
			return ds;
		}
		
		public function load():void {
			dataLoader.load();
		}
		
		override protected function onLoad(e:Event):void {
			if (e.data)  {
				parseJSON((e.data as ByteArray).toString());
			}
			else {
				CONFIG::debug {
					if (FXDebug.instance.debugChangeLog)
						Logger.debug(this, 'No change log.');
				}
			}
			dispatchEvent(e);
		}
		
		protected function parseJSON(text:String):void {
			parseJSONTo(text, changeLogModel);
			CONFIG::debug {
				if (FXDebug.instance.debugChangeLog) {
					Logger.debug(this, (FXDebug.instance.debugChangeLog == FXDebug.DEBUG_VERBOSE) ?
						('Loaded change log:\n' + JSON.stringify(changeLogModel, null, 2)) : 'Loaded change log.');
				}
			}
		}
		
		protected var changeLogModel:ChangeLogModel;
	}
}
