package com.razorscript.fxcalc.controllers.supportClasses {
	import com.razorscript.events.RazorEventType;
	import com.razorscript.io.IDataStorage;
	import com.razorscript.utils.ClassUtil;
	import com.razorscript.utils.ObjUtil;
	import com.razorscript.utils.objectParser.errors.ObjectParserError;
	import flash.utils.getQualifiedClassName;
	import starling.events.Event;
	import starling.events.EventDispatcher;
	
	/**
	 * Base class for data storage controllers.
	 *
	 * @author Gene Pavlovsky
	 */
	public class DataStorageControllerBase extends EventDispatcher {
		public function DataStorageControllerBase() {
		}
		
		protected function initDataStorage():void {
			dataStorage = createDataStorage();
			dataStorage.addEventListener(RazorEventType.LOAD, onLoad);
			dataStorage.addEventListener(RazorEventType.SAVE, onSave);
			dataStorage.addEventListener(RazorEventType.WIPE, onWipe);
			dataStorage.addEventListener(RazorEventType.LOAD_ERROR, onError);
			dataStorage.addEventListener(RazorEventType.SAVE_ERROR, onError);
			dataStorage.addEventListener(RazorEventType.WIPE_ERROR, onError);
		}
		
		/**
		 * Creates the data storage.
		 * Subclasses must override this method.
		 *
		 * @return A data storage.
		 */
		protected function createDataStorage():IDataStorage {
			throw new Error('Illegal abstract method call.');
		}
		
		protected function _createDataStorage(dsClass:Class, properties:Object):IDataStorage {
			var ds:IDataStorage = new dsClass() as IDataStorage;
			if (!ds)
				throw new ArgumentError('Class ' + getQualifiedClassName(dsClass) + ' is not an IDataStorage.');
			ObjUtil.copy(properties, ds);
			return ds;
		}
		
		protected function onLoad(e:Event):void {
			dispatchEvent(e);
		}
		
		protected function onSave(e:Event):void {
			dispatchEvent(e);
		}
		
		protected function onWipe(e:Event):void {
			dispatchEvent(e);
		}
		
		protected function onError(e:Event):void {
			dispatchEventWith(e.type, false, dataStorage.error);
		}
		
		protected function parseJSONTo(text:String, target:Object, reviver:Function = null):void {
			try {
				ObjUtil.parseJSONTo(text, target, reviver);
			}
			catch (err:ObjectParserError) {
				dispatchEventWith(RazorEventType.PARSE_ERROR, false, ClassUtil.getClassName(err.target) + '.' + err.property + ': ' + err.message);
			}
			catch (err:Error) {
				dispatchEventWith(RazorEventType.PARSE_ERROR, false, err.message);
			}
		}
		
		protected var dataStorage:IDataStorage;
	}
}
