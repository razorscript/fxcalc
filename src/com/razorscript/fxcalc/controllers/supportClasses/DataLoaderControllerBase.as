package com.razorscript.fxcalc.controllers.supportClasses {
	import com.razorscript.events.RazorEventType;
	import com.razorscript.io.IDataLoader;
	import com.razorscript.utils.ClassUtil;
	import com.razorscript.utils.ObjUtil;
	import com.razorscript.utils.objectParser.errors.ObjectParserError;
	import flash.utils.getQualifiedClassName;
	import starling.events.Event;
	import starling.events.EventDispatcher;
	
	/**
	 * Base class for data loader controllers.
	 *
	 * @author Gene Pavlovsky
	 */
	public class DataLoaderControllerBase extends EventDispatcher {
		public function DataLoaderControllerBase() {
		}
		
		protected function initDataLoader():void {
			dataLoader = createDataLoader();
			dataLoader.addEventListener(RazorEventType.LOAD, onLoad);
			dataLoader.addEventListener(RazorEventType.LOAD_ERROR, onError);
		}
		
		/**
		 * Creates the data loader.
		 * Subclasses must override this method.
		 *
		 * @return A data loader.
		 */
		protected function createDataLoader():IDataLoader {
			throw new Error('Illegal abstract method call.');
		}
		
		protected function _createDataLoader(dsClass:Class, properties:Object):IDataLoader {
			var ds:IDataLoader = new dsClass() as IDataLoader;
			if (!ds)
				throw new ArgumentError('Class ' + getQualifiedClassName(dsClass) + ' is not an IDataLoader.');
			ObjUtil.copy(properties, ds);
			return ds;
		}
		
		protected function onLoad(e:Event):void {
			dispatchEvent(e);
		}
		
		protected function onError(e:Event):void {
			dispatchEventWith(e.type, false, dataLoader.error);
		}
		
		protected function parseJSONTo(text:String, target:Object, reviver:Function = null):void {
			try {
				ObjUtil.parseJSONTo(text, target, reviver);
			}
			catch (err:ObjectParserError) {
				dispatchEventWith(RazorEventType.PARSE_ERROR, false, ClassUtil.getClassName(err.target) + '.' + err.property + ': ' + err.message);
			}
			catch (err:Error) {
				dispatchEventWith(RazorEventType.PARSE_ERROR, false, err.message);
			}
		}
		
		protected var dataLoader:IDataLoader;
	}
}
