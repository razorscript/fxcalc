package com.razorscript.fxcalc.controllers.supportClasses {
	import com.razorscript.fxcalc.controllers.ChangeLogController;
	import com.razorscript.fxcalc.controllers.PopUpController;
	import com.razorscript.fxcalc.controllers.SettingsController;
	import com.razorscript.fxcalc.controllers.StateController;
	import com.razorscript.fxcalc.data.ChangeLogModel;
	import com.razorscript.fxcalc.data.Model;
	import com.razorscript.fxcalc.data.SettingsModel;
	import com.razorscript.fxcalc.ui.ViewModel;
	
	/**
	 * AppController dependencies.
	 *
	 * @author Gene Pavlovsky
	 */
	public class AppControllerDeps {
		public var model:Model;
		public var viewModel:ViewModel;
		public var changeLogModel:ChangeLogModel;
		public var settingsModel:SettingsModel;
		
		public var settingsController:SettingsController;
		public var stateController:StateController;
		public var changeLogController:ChangeLogController;
		public var popUpController:PopUpController;
	}
}
