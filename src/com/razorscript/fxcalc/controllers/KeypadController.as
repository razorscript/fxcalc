package com.razorscript.fxcalc.controllers {
	import com.razorscript.debug.Logger;
	import com.razorscript.fxcalc.data.Model;
	import com.razorscript.fxcalc.data.SettingsModel;
	import com.razorscript.fxcalc.data.constants.MathConst;
	import com.razorscript.fxcalc.events.ModelEventType;
	import com.razorscript.fxcalc.math.tokens.FormulaToken;
	import com.razorscript.fxcalc.math.tokens.IFormulaTokenFactory;
	import com.razorscript.fxcalc.math.tokens.MathTokenCache;
	import com.razorscript.fxcalc.ui.DisplayState;
	import com.razorscript.fxcalc.ui.ResultDisplayFormat;
	import com.razorscript.fxcalc.ui.ViewModel;
	import com.razorscript.math.AngleUnit;
	import com.razorscript.math.expression.MathExpression;
	import com.razorscript.math.expression.MathFun;
	import com.razorscript.math.expression.MathOperator;
	import com.razorscript.math.expression.lexers.IMathTokenProvider;
	import com.razorscript.math.expression.tokens.MathToken;
	import com.razorscript.math.expression.tokens.OpAssignToken;
	import com.razorscript.math.expression.tokens.OperatorToken;
	
	/**
	 * Handles user interaction with the keypad and keyboard input.
	 *
	 * @author Gene Pavlovsky
	 */
	public class KeypadController {
		public function KeypadController(model:Model, viewModel:ViewModel, settingsModel:SettingsModel, appController:AppController, popUpController:PopUpController) {
			this.model = model;
			this.viewModel = viewModel;
			this.settingsModel = settingsModel;
			this.appController = appController;
			this.popUpController = popUpController;
		}
		
		public function showSettings():void {
			// TODO: Show settings.
			settingsModel.ui.theme.colorSchemeName = (settingsModel.ui.theme.colorSchemeName == 'solarized-dark') ? 'solarized-light' : 'solarized-dark';
			appController.saveSettings();
		}
		
		public function inputToken(token:MathToken):void {
			if (!viewModel.isEditable) {
				appController.clearFormula(); // Clear the formula, move the caret home.
				if (token.type == MathToken.OPERATOR) {
					var operator:String = (token as OperatorToken).operator;
					if ((operator == MathConst.OP_ADD) || (operator == MathConst.OP_SUBTRACT) || (operator == MathConst.OP_MULTIPLY) || (operator == MathConst.OP_DIVIDE))
						addToken(MathTokenCache.getIdentifierToken(MathToken.VARIABLE, MathConst.VAR_NAME_ANSWER));
				}
			}
			
			var formulaTokenFactory:IFormulaTokenFactory = model.formulaTokenFactory;
			var caretIndex:int = viewModel.caretIndex;
			var wantLeftParen:Boolean = (token.type == MathToken.FUN_PAREN);
			if (viewModel.insertMode) {
				model.addTokenAt(formulaTokenFactory.createFormulaToken(token), caretIndex++);
			}
			else {
				if ((caretIndex < viewModel.maxCaretIndex) && (model.getTokenAt(caretIndex).token.type == MathToken.FUN_PAREN)) {
					// Token to be replaced has a left parenthesis.
					if (wantLeftParen)
						wantLeftParen = false;
					else
						model.removeTokenAt(caretIndex + 1);
				}
				model.setTokenAt(formulaTokenFactory.createFormulaToken(token), caretIndex++);
			}
			if (wantLeftParen)
				model.addTokenAt(formulaTokenFactory.createFormulaToken(MathTokenCache.getMathToken(MathToken.LEFT_PAREN)), caretIndex++);
			// TODO: Add a setting to automatically add the closing bracket after an opening bracket (only when in insert mode?).
			/*if (wantLeftParen || (token.type == MathToken.LEFT_PAREN))
				model.addTokenAt(formulaTokenFactory.createFormulaToken(MathTokenCache.getMathToken(MathToken.RIGHT_PAREN)), caretIndex)*/;
			// TODO: If the token is a variable or a constant, display it's value in a callout?
			viewModel.caretIndex = caretIndex;
		}
		
		public function clearAC():void {
			appController.clearAC();
		}
		
		public function clearMode():void {
			appController.clearMode();
		}
		
		public function clearMemory():void {
			appController.clearMemory();
		}
		
		public function clearHistory():void {
			appController.clearHistory();
		}
		
		public function deleteToken(backspace:Boolean = false):void {
			if (!viewModel.isEditable || (backspace && !viewModel.caretIndex))
				return;
			if (backspace || (viewModel.caretIndex == viewModel.maxCaretIndex))
				moveCaretLeft();
			if (model.numTokens)
				model.removeTokenAt(viewModel.caretIndex, (model.getTokenAt(viewModel.caretIndex).token.type == MathToken.FUN_PAREN) ? 2 : 1);
		}
		
		public function setInsertMode(value:Boolean):void {
			if (viewModel.insertMode == value)
				return;
			viewModel.insertMode = value;
		}
		
		public function setInverseTrigMode(value:Boolean):void {
			if (viewModel.inverseTrigMode == value)
				return;
			viewModel.inverseTrigMode = value;
		}
		
		public function setHyperbolicTrigMode(value:Boolean):void {
			if (viewModel.hyperbolicTrigMode == value)
				return;
			viewModel.hyperbolicTrigMode = value;
		}
		
		public function inputTokenSin():void {
			var inv:Boolean = viewModel.inverseTrigMode, hyp:Boolean = viewModel.hyperbolicTrigMode;
			inputToken(MathTokenCache.getOperatorToken(MathToken.FUN_JUXTA, inv ? (hyp ? MathFun.ARCSINH : MathFun.ARCSIN) : (hyp ? MathFun.SINH : MathFun.SIN)));
			viewModel.inverseTrigMode = viewModel.hyperbolicTrigMode = false;
		}
		
		public function inputTokenCos():void {
			var inv:Boolean = viewModel.inverseTrigMode, hyp:Boolean = viewModel.hyperbolicTrigMode;
			inputToken(MathTokenCache.getOperatorToken(MathToken.FUN_JUXTA, inv ? (hyp ? MathFun.ARCCOSH : MathFun.ARCCOS) : (hyp ? MathFun.COSH : MathFun.COS)));
			viewModel.inverseTrigMode = viewModel.hyperbolicTrigMode = false;
		}
		
		public function inputTokenTan():void {
			var inv:Boolean = viewModel.inverseTrigMode, hyp:Boolean = viewModel.hyperbolicTrigMode;
			inputToken(MathTokenCache.getOperatorToken(MathToken.FUN_JUXTA, inv ? (hyp ? MathFun.ARCTANH : MathFun.ARCTAN) : (hyp ? MathFun.TANH : MathFun.TAN)));
			viewModel.inverseTrigMode = viewModel.hyperbolicTrigMode = false;
		}
		
		public function moveCaretHome():void {
			viewModel.caretIndex = 0;
			if (!viewModel.isEditable)
				viewModel.displayState = DisplayState.INPUT;
		}
		
		public function moveCaretEnd():void {
			viewModel.caretIndex = viewModel.maxCaretIndex;
			if (!viewModel.isEditable)
				viewModel.displayState = DisplayState.INPUT;
		}
		
		public function moveCaretLeft():void {
			// TODO: Add setting to wrap-around at the end (like FX-991EX).
			if (!viewModel.isEditable)
				moveCaretEnd();
			else
				viewModel.caretIndex -= (viewModel.caretIndex > 0) ? ((viewModel.caretIndex > 1) && (model.getTokenAt(viewModel.caretIndex - 2).token.type == MathToken.FUN_PAREN)) ? 2 : 1 : 0; // 0 resets the caret blink timer.
		}
		
		public function moveCaretRight():void {
			// TODO: Add setting to wrap-around at the end (like FX-991EX).
			if (!viewModel.isEditable)
				moveCaretHome();
			else
				viewModel.caretIndex += (viewModel.caretIndex < viewModel.maxCaretIndex) ? (model.getTokenAt(viewModel.caretIndex).token.type == MathToken.FUN_PAREN) ? 2 : 1 : 0; // 0 resets the caret blink timer.
		}
		
		public function setResultDisplayFormat(value:ResultDisplayFormat):void {
			viewModel.resultDisplayFormat = value;
		}
		
		public function setAngleUnitMode(value:AngleUnit):void {
			model.angleUnitMode = value;
		}
		
		public function equals():void {
			if (!model.numTokens)
				return;
			var result:MathExpression = model.result = eval(model.tokens);
			Logger.info(this, result);
			if (!result.hasError)
				model.answerVariable = result.value;
			// Add the current (editable) formula as the last history item (clearing instead of cloning the formula), then display the last history item.
			model.addHistoryItem(model.formula, false);
			viewModel.historyIndex = viewModel.maxHistoryIndex - 1;
		}
		
		protected function eval(tokens:Vector.<FormulaToken>):MathExpression {
			var source:IMathTokenProvider = model.mathTokenProvider.setSource(tokens);
			// Check for a misplaced assignment operator token. If the expression contains an assignment operator token, it must be the last token.
			for (var i:int = tokens.length - 2; i >= 0; --i) {
				var token:MathToken = tokens[i].token;
				if ((token is OpAssignToken) && ((token as OpAssignToken).operator != MathOperator.ASSIGN)) // TODO: Refactor entry of assignment operators.
					return MathExpression.fromError(source, MathExpression.SYNTAX_ERROR, 'Unexpected assignment operator.', i);
			}
			return model.mathParser.eval(source);
		}
		
		public function memClear():void {
			model.memoryVariable = 0;
		}
		
		public function memStore():void {
			if (!model.numTokens)
				return;
			removeOpAssignToken();
			addToken(MathTokenCache.getOpAssignToken(MathOperator.RASSIGN, MathConst.VAR_NAME_MEMORY));
			equals();
			if (!model.result.hasError)
				model.dispatchEventWith(ModelEventType.MEMORY_VARIABLE_CHANGE);
		}
		
		public function memPlus():void {
			if (!model.numTokens)
				return;
			removeOpAssignToken();
			addToken(MathTokenCache.getOpAssignToken(MathOperator.RASSIGN_ADD, MathConst.VAR_NAME_MEMORY));
			equals();
			if (!model.result.hasError)
				model.dispatchEventWith(ModelEventType.MEMORY_VARIABLE_CHANGE);
			// TODO: Display some indication (e.g. flash result or whole screen)?
		}
		
		public function memMinus():void {
			if (!model.numTokens)
				return;
			removeOpAssignToken();
			addToken(MathTokenCache.getOpAssignToken(MathOperator.RASSIGN_SUBTRACT, MathConst.VAR_NAME_MEMORY));
			equals();
			if (!model.result.hasError)
				model.dispatchEventWith(ModelEventType.MEMORY_VARIABLE_CHANGE);
			// TODO: Display some indication (e.g. flash result or whole screen)?
		}
		
		public function memRestore():void {
			if (viewModel.isEditable && model.numTokens) {
				inputToken(MathTokenCache.getIdentifierToken(MathToken.VARIABLE, MathConst.VAR_NAME_MEMORY));
			}
			else {
				appController.clearFormula(); // Clear the formula, move the caret home.
				addToken(MathTokenCache.getIdentifierToken(MathToken.VARIABLE, MathConst.VAR_NAME_MEMORY));
				equals();
			}
		}
		
		protected function addToken(token:MathToken):void {
			model.addToken(model.formulaTokenFactory.createFormulaToken(token));
			viewModel.caretIndex = viewModel.maxCaretIndex;
		}
		
		protected function removeOpAssignToken():void {
			var index:int = model.numTokens - 1;
			if ((index >= 0) && (model.getTokenAt(index).token is OpAssignToken))
				model.removeTokenAt(index);
		}
		
		public function gotoHistoryPrev():void {
			gotoHistory(viewModel.historyIndex - 1);
		}
		
		public function gotoHistoryNext():void {
			gotoHistory(viewModel.historyIndex + 1);
		}
		
		public function gotoHistoryFirst():void {
			gotoHistory(0);
		}
		
		public function gotoHistoryLast():void {
			gotoHistory(viewModel.maxHistoryIndex);
		}
		
		protected function gotoHistory(index:int):void {
			if ((index >= 0) && (index <= viewModel.maxHistoryIndex)) {
				viewModel.historyIndex = index;
				viewModel.caretIndex = 0;
			}
		}
		
		CONFIG::debug {
			// DEBUG: Remove later.
			public function loadSettings():void {
				appController.loadSettings();
			}
			public function saveSettings():void {
				appController.saveSettings();
			}
			public function resetSettings():void {
				appController.resetSettings();
			}
			public function wipeSettings():void {
				appController.wipeSettings();
			}
			public function loadState():void {
				appController.loadState();
			}
			public function saveState():void {
				appController.saveState();
			}
			public function wipeState():void {
				appController.wipeState();
			}
		}
		
		protected var model:Model;
		protected var viewModel:ViewModel;
		protected var settingsModel:SettingsModel;
		protected var appController:AppController;
		protected var popUpController:PopUpController;
	}
}
