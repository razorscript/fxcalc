package com.razorscript.fxcalc.controllers {
	import com.razorscript.fxcalc.data.Model;
	import com.razorscript.fxcalc.data.SettingsModel;
	import com.razorscript.fxcalc.ui.DisplayState;
	import com.razorscript.fxcalc.ui.ViewModel;
	import com.razorscript.math.expression.tokens.MathToken;
	
	/**
	 * Handles user interaction with the display.
	 *
	 * @author Gene Pavlovsky
	 */
	public class DisplayController {
		public function DisplayController(model:Model, viewModel:ViewModel, settingsModel:SettingsModel, appController:AppController, popUpController:PopUpController) {
			this.model = model;
			this.viewModel = viewModel;
			this.settingsModel = settingsModel;
			this.appController = appController;
			this.popUpController = popUpController;
		}
		
		public function moveCaret(index:int):void {
			if (!viewModel.isEditable)
				viewModel.displayState = DisplayState.INPUT;
			viewModel.caretIndex = ((index > 0) && (model.getTokenAt(index - 1).token.type == MathToken.FUN_PAREN)) ? (index + 1) : index;
		}
		
		protected var model:Model;
		protected var viewModel:ViewModel;
		protected var settingsModel:SettingsModel;
		protected var appController:AppController;
		protected var popUpController:PopUpController;
	}
}
