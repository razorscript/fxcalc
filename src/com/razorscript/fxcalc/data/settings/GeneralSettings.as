package com.razorscript.fxcalc.data.settings {
	import com.razorscript.utils.objectParser.IObjectParserTarget;
	import starling.events.EventDispatcher;
	
	/**
	 * Stores and controls access to general settings.
	 *
	 * @author Gene Pavlovsky
	 */
	public class GeneralSettings extends EventDispatcher implements IObjectParserTarget {
		public function GeneralSettings() {
		}
		
		// TODO: Implement various settings properties.
	}
}
