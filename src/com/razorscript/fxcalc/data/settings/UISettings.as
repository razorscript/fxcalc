package com.razorscript.fxcalc.data.settings {
	import com.razorscript.fxcalc.data.settings.ui.ThemeUISettings;
	import com.razorscript.utils.objectParser.IObjectParserTarget;
	import starling.events.EventDispatcher;
	
	/**
	 * Stores and controls access to UI settings.
	 *
	 * @author Gene Pavlovsky
	 */
	public class UISettings extends EventDispatcher implements IObjectParserTarget {
		public function UISettings() {
			_theme = new ThemeUISettings();
		}
		
		protected var _theme:ThemeUISettings;
		
		public function get theme():ThemeUISettings {
			return _theme;
		}
	}
}
