package com.razorscript.fxcalc.data.settings.ui {
	import com.razorscript.fxcalc.events.settings.ui.ThemeUISettingsEventType;
	import com.razorscript.utils.objectParser.IObjectParserTarget;
	import starling.events.EventDispatcher;
	
	/**
	 * Stores and controls access to ThemeUI settings.
	 *
	 * @author Gene Pavlovsky
	 */
	public class ThemeUISettings extends EventDispatcher implements IObjectParserTarget {
		public function ThemeUISettings() {
		}
		
		protected var _name:String;
		/**
		 * Theme name.
		 */
		public function get name():String {
			return _name;
		}
		
		public function set name(value:String):void {
			_name = value;
			dispatchEventWith(ThemeUISettingsEventType.NAME_CHANGE);
		}
		
		protected var _styleName:String;
		/**
		 * Style name.
		 */
		public function get styleName():String {
			return _styleName;
		}
		
		public function set styleName(value:String):void {
			_styleName = value;
			dispatchEventWith(ThemeUISettingsEventType.STYLE_NAME_CHANGE);
		}
		
		protected var _colorSchemeName:String;
		/**
		 * Color scheme name.
		 */
		public function get colorSchemeName():String {
			return _colorSchemeName;
		}
		
		public function set colorSchemeName(value:String):void {
			_colorSchemeName = value;
			dispatchEventWith(ThemeUISettingsEventType.COLOR_SCHEME_NAME_CHANGE);
		}
	}
}
