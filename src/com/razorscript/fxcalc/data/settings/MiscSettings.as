package com.razorscript.fxcalc.data.settings {
	import com.razorscript.utils.objectParser.IObjectParserTarget;
	import starling.events.EventDispatcher;
	
	/**
	 * Stores and controls access to misc settings.
	 *
	 * @author Gene Pavlovsky
	 */
	public class MiscSettings extends EventDispatcher implements IObjectParserTarget {
		public function MiscSettings() {
		}
		
		// TODO: Implement various settings properties.
	}
}
