package com.razorscript.fxcalc.data.settings {
	import com.razorscript.fxcalc.events.settings.MathSettingsEventType;
	import com.razorscript.math.AngleUnit;
	import com.razorscript.utils.objectParser.IObjectParserTarget;
	import starling.events.EventDispatcher;
	
	/**
	 * Stores and controls access to math settings.
	 *
	 * @author Gene Pavlovsky
	 */
	public class MathSettings extends EventDispatcher implements IObjectParserTarget {
		public function MathSettings() {
		}
		
		protected var _defaultAngleUnit:AngleUnit;
		/**
		 * The default angle unit.
		 * @see com.razorscript.fxcalc.data.Model#angleUnit
		 */
		public function get defaultAngleUnit():AngleUnit {
			return _defaultAngleUnit;
		}
		
		public function set defaultAngleUnit(value:AngleUnit):void {
			_defaultAngleUnit = value;
			dispatchEventWith(MathSettingsEventType.DEFAULT_ANGLE_UNIT_CHANGE);
		}
		
		protected var _lenientBracketChecking:Boolean;
		/**
		 * If true, unmatched opening brackets are automatically closed. Otherwise, they are reported as errors.
		 * @see com.razorscript.math.expression.lexers.LexerBase#lenientBracketChecking
		 */
		public function get lenientBracketChecking():Boolean {
			return _lenientBracketChecking;
		}
		
		public function set lenientBracketChecking(value:Boolean):void {
			_lenientBracketChecking = value;
			dispatchEventWith(MathSettingsEventType.LENIENT_BRACKET_CHECKING_CHANGE);
		}
		
		protected var _numberAllowLeadingExp:Boolean;
		/**
		 * If true, numbers like "e±XXX" are treated as "1e±XXX". Otherwise, they are reported as errors.
		 * @see com.razorscript.math.expression.lexers.MathTokenLexer#numberAllowLeadingExp
		 */
		public function get numberAllowLeadingExp():Boolean {
			return _numberAllowLeadingExp;
		}
		
		public function set numberAllowLeadingExp(value:Boolean):void {
			_numberAllowLeadingExp = value;
			dispatchEventWith(MathSettingsEventType.NUMBER_ALLOW_LEADING_EXP_CHANGE);
		}
		
		protected var _numberAllowSoloRadix:Boolean;
		/**
		 * If true, numbers "." are treated as "0". Otherwise, they are reported as errors.
		 * @see com.razorscript.math.expression.lexers.MathTokenLexer#numberAllowSoloRadix
		 */
		public function get numberAllowSoloRadix():Boolean {
			return _numberAllowSoloRadix;
		}
		
		public function set numberAllowSoloRadix(value:Boolean):void {
			_numberAllowSoloRadix = value;
			dispatchEventWith(MathSettingsEventType.NUMBER_ALLOW_SOLO_RADIX_CHANGE);
		}
	}
}
