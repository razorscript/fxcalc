package com.razorscript.fxcalc.data.constants {
	import com.razorscript.app.Version;
	
	/**
	 * App constants.
	 *
	 * @author Gene Pavlovsky
	 */
	public class AppConst {
		public static const NAME:String = CONFIG::appName;
		public static const VERSION:Version = new Version(CONFIG::appVersion);
		public static const BUILD_DATE:Date = new Date(CONFIG::appBuildTime);
	}
}
