package com.razorscript.fxcalc.data.constants {
	import com.razorscript.io.EmbedDataLoader;
	import com.razorscript.io.FileDataStorage;
	import com.razorscript.io.SharedObjectDataStorage;
	import flash.utils.CompressionAlgorithm;
	
	/**
	 * Data storage constants.
	 *
	 * @author Gene Pavlovsky
	 */
	public class DataStorageConst {
		public static const COMPRESSION:String = CompressionAlgorithm.LZMA;
		
		CONFIG::desktop {
			// TODO: Implement FileDataStorage
			/*public static const SETTINGS_CLASS:Class = FileDataStorage;
			public static const SETTINGS_PROPERTIES:Object = {name: 'settings.json'};
			
			public static const STATE_CLASS:Class = FileDataStorage;
			public static const STATE_PROPERTIES:Object = {name: 'state.json.dat'};
			
			public static const CHANGE_LOG_CLASS:Class = FileDataStorage;
			public static const CHANGE_LOG_PROPERTIES:Object = {name: 'changelog.json'};*/
		}
		
		CONFIG::mobile {
			public static const SETTINGS_CLASS:Class = SharedObjectDataStorage;
			public static const SETTINGS_PROPERTIES:Object = {name: 'settings', compression: COMPRESSION};
			
			public static const STATE_CLASS:Class = SharedObjectDataStorage;
			public static const STATE_PROPERTIES:Object = {name: 'state', compression: COMPRESSION};
			
			[Embed(source="/../assets/changelog.min.json", mimeType="application/octet-stream")]
			protected static const CHANGE_LOG_JSON:Class;
			public static const CHANGE_LOG_CLASS:Class = EmbedDataLoader;
			public static const CHANGE_LOG_PROPERTIES:Object = {dataClass: CHANGE_LOG_JSON};
		}
		
		CONFIG::web {
			public static const SETTINGS_CLASS:Class = SharedObjectDataStorage;
			public static const SETTINGS_PROPERTIES:Object = {name: 'settings', compression: COMPRESSION};
			
			public static const STATE_CLASS:Class = SharedObjectDataStorage;
			public static const STATE_PROPERTIES:Object = {name: 'state', compression: COMPRESSION};
			
			[Embed(source="/../assets/changelog.min.json", mimeType="application/octet-stream")]
			protected static const CHANGE_LOG_JSON:Class;
			public static const CHANGE_LOG_CLASS:Class = EmbedDataLoader;
			public static const CHANGE_LOG_PROPERTIES:Object = {dataClass: CHANGE_LOG_JSON};
		}
	}
}
