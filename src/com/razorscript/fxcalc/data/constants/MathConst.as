package com.razorscript.fxcalc.data.constants {
	import com.razorscript.math.expression.MathOperator;
	
	/**
	 * Math constants.
	 *
	 * @author Gene Pavlovsky
	 */
	public class MathConst {
		/** The maximum positive exponent value. */
		public static const MAX_EXPONENT:int = 99;
		
		public static const OP_ADD:String = MathOperator.ADD;
		public static const OP_SUBTRACT:String = MathOperator.SUBTRACT_ALT;
		public static const OP_MULTIPLY:String = MathOperator.MULTIPLY_ALT;
		public static const OP_DIVIDE:String = MathOperator.DIVIDE_ALT;
		
		public static const CONST_NAME_PI:String = 'pi';
		public static const CONST_NAME_E:String = 'e';
		public static const CONST_NAME_I:String = 'i';
		
		public static const VAR_NAME_ANSWER:String = 'Ans';
		public static const VAR_NAME_MEMORY:String = 'M';
		public static const VAR_NAMES:Array = ['A', 'B', 'C', 'D', 'E', 'F', 'X', 'Y'];
	}
}
