package com.razorscript.fxcalc.data.dto {
	import com.razorscript.debug.Logger;
	import com.razorscript.utils.objectParser.VersionedObjectParserTarget;
	
	/**
	 * App state DTO.
	 *
	 * @author Gene Pavlovsky
	 */
	public class StateDTO extends VersionedObjectParserTarget {
		/** The current version number of the state format. */
		public static const FORMAT_VERSION:int = 3002;
		
		public function StateDTO() {
			super(FORMAT_VERSION);
		}
		
		public var appVersion:int;
		
		override protected function upgradeObject(obj:Object, objectFormatVersion:int):void {
			Logger.debug(this, 'upgradeObject()', objectFormatVersion, '->', currentFormatVersion);
		}
	}
}
