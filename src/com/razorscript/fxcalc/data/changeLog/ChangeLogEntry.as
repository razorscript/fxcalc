package com.razorscript.fxcalc.data.changeLog {
	import com.razorscript.app.Version;
	import com.razorscript.utils.StrUtil;
	
	/**
	 * Contains properties describing a change log entry.
	 *
	 * @author Gene Pavlovsky
	 */
	public class ChangeLogEntry {
		public function ChangeLogEntry(version:*, description:String, features:Array/*String*/, bugfixes:Array/*String*/) {
			_version = new Version(version);
			_description = description;
			_features = features;
			_bugfixes = bugfixes;
		}
		
		protected var _version:Version;
		
		public function get version():Version {
			return _version;
		}
		
		protected var _description:String;
		
		public function get description():String {
			return _description;
		}
		
		protected var _features:Array;
		
		public function get features():Array/*String*/ {
			return _features;
		}
		
		protected var _bugfixes:Array;
		
		public function get bugfixes():Array/*String*/ {
			return _bugfixes;
		}
		
		public function toString():String {
			return '[ChangeLogEntry version="' + version + '" description="' + description + '" features=' + StrUtil.arrayToString(features) +
				' bugfixes=' + StrUtil.arrayToString(bugfixes) + ']';
		}
	}
}
