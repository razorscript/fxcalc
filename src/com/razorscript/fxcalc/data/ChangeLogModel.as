package com.razorscript.fxcalc.data {
	import com.razorscript.app.Version;
	import com.razorscript.fxcalc.data.changeLog.ChangeLogEntry;
	import com.razorscript.utils.Lambda;
	import com.razorscript.utils.objectParser.IObjectParserCustomTarget;
	import starling.events.Event;
	import starling.events.EventDispatcher;

	/**
	 * Stores and controls access to app change log.
	 *
	 * @author Gene Pavlovsky
	 */
	public class ChangeLogModel extends EventDispatcher implements IObjectParserCustomTarget {
		public function ChangeLogModel() {
			super();
			entries = new Vector.<ChangeLogEntry>();
		}

		/**
		 * Return a vector of change log entries with version in the specified range [minVersion, maxVersion].
		 *
		 * @param minVersion A version that represents the minimum value (inclusive) of the entry's version to include in the result.
		 * @param maxVersion A version that represents the maximum value (inclusive) of the entry's version to include in the result.
		 * @return Vector of change log entries with version in the specified range.
		 */
		public function getEntries(minVersion:Version = null, maxVersion:Version = null):Vector.<ChangeLogEntry> {
			if (!minVersion)
				minVersion = Version.MIN_VALUE;
			if (!maxVersion)
				maxVersion = Version.MAX_VALUE;
			return entries.filter(Lambda.slice3(function (entry:ChangeLogEntry):Boolean {
				var versionCode:int = entry.version.versionCode;
				return (versionCode >= minVersion.versionCode) && (versionCode <= maxVersion.versionCode);
			}));
		}

		/**
		 * Parses the specified object into this object.
		 *
		 * @param obj An object to parse into this object.
		 * @return Boolean true to tell ObjectParser that no further action is required.
		 */
		public function parseObject(obj:Object):Boolean {
			var arr:Array = obj as Array;
			if (!arr)
				throw new ArgumentError('The obj argument (' + obj + ') is not an Array.');

			entries.length = 0;
			var length:int = arr.length;
			for (var i:int = 0; i < length; ++i) {
				obj = arr[i];
				entries.push(new ChangeLogEntry(obj.version, obj.description, obj.features, obj.bugfixes));
			}
			dispatchEventWith(Event.CHANGE);

			return true;
		}

		protected var entries:Vector.<ChangeLogEntry>;
	}
}
