package com.razorscript.fxcalc.data {
	import com.razorscript.debug.Logger;
	import com.razorscript.fxcalc.data.settings.GeneralSettings;
	import com.razorscript.fxcalc.data.settings.MathSettings;
	import com.razorscript.fxcalc.data.settings.MiscSettings;
	import com.razorscript.fxcalc.data.settings.UISettings;
	import com.razorscript.math.AngleUnit;
	import com.razorscript.utils.objectParser.VersionedObjectParserTarget;
	
	/**
	 * Stores and controls access to app settings.
	 *
	 * @author Gene Pavlovsky
	 */
	public class SettingsModel extends VersionedObjectParserTarget {
		/** The current version number of the settings format. */
		public static const FORMAT_VERSION:int = 3004;
		
		public function SettingsModel() {
			super(FORMAT_VERSION);
			_general = new GeneralSettings();
			_ui = new UISettings();
			_math = new MathSettings();
			_misc = new MiscSettings();
		}
		
		protected var _general:GeneralSettings;
		
		public function get general():GeneralSettings {
			return _general;
		}
		
		protected var _ui:UISettings;
		
		public function get ui():UISettings {
			return _ui;
		}
		
		protected var _math:MathSettings;
		
		public function get math():MathSettings {
			return _math;
		}
		
		protected var _misc:MiscSettings;
		
		public function get misc():MiscSettings {
			return _misc;
		}
		
		override protected function upgradeObject(obj:Object, objectFormatVersion:int):void {
			Logger.debug(this, 'upgradeObject()', objectFormatVersion, '->', currentFormatVersion);
			if (objectFormatVersion <= 3002) {
				// #145: Change default angle unit to Degree
				if (obj.math && (obj.math.defaultAngleUnit == AngleUnit.RADIAN.toString()))
					obj.math.defaultAngleUnit = AngleUnit.DEGREE.toString();
			}
		}
	}
}
