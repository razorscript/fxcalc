package com.razorscript.fxcalc.data {
	import com.razorscript.fxcalc.data.constants.MathConst;
	import com.razorscript.fxcalc.events.ModelEventType;
	import com.razorscript.fxcalc.math.Formula;
	import com.razorscript.fxcalc.math.lexers.IFormulaTokenMathTokenProvider;
	import com.razorscript.fxcalc.math.tokens.FormulaToken;
	import com.razorscript.fxcalc.math.tokens.IFormulaTokenFactory;
	import com.razorscript.math.AngleUnit;
	import com.razorscript.math.expression.MathExpression;
	import com.razorscript.math.expression.MathParser;
	import starling.events.EventDispatcher;
	
	/**
	 * Stores and controls access to app data and state.
	 *
	 * @author Gene Pavlovsky
	 */
	public class Model extends EventDispatcher {
		public function Model() {
			super();
			_formula = _editableFormula = new Formula();
			_history = new Vector.<Formula>();
		}
		
		protected var _lastAppVersion:int;
		
		public function get lastAppVersion():int {
			return _lastAppVersion;
		}
		
		public function set lastAppVersion(value:int):void {
			_lastAppVersion = value;
			dispatchEventWith(ModelEventType.LAST_APP_VERSION_CHANGE);
		}
		
		protected var _mathParser:MathParser;
		/**
		 * The MathParser used for parsing and evaluating expressions.
		 */
		public function get mathParser():MathParser {
			return _mathParser;
		}
		
		public function set mathParser(value:MathParser):void {
			_mathParser = value;
		}
		
		protected var _mathTokenProvider:IFormulaTokenMathTokenProvider;
		/**
		 * The math token provider used for parsing expressions.
		 */
		public function get mathTokenProvider():IFormulaTokenMathTokenProvider {
			return _mathTokenProvider;
		}
		
		public function set mathTokenProvider(value:IFormulaTokenMathTokenProvider):void {
			_mathTokenProvider = value;
			if (_formulaTokenFactory)
				_mathTokenProvider.formulaTokenFactory = _formulaTokenFactory;
		}
		
		/**
		 * The value of the memory variable.
		 */
		public function get memoryVariable():Number {
			return _mathParser.mathContext.getVariable(MathConst.VAR_NAME_MEMORY);
		}
		
		public function set memoryVariable(value:Number):void {
			_mathParser.mathContext.setVariable(MathConst.VAR_NAME_MEMORY, value);
			dispatchEventWith(ModelEventType.MEMORY_VARIABLE_CHANGE);
		}
		
		/**
		 * The value of the answer variable.
		 */
		public function get answerVariable():Number {
			return _mathParser.mathContext.getVariable(MathConst.VAR_NAME_ANSWER);
		}
		
		public function set answerVariable(value:Number):void {
			_mathParser.mathContext.setVariable(MathConst.VAR_NAME_ANSWER, value);
			dispatchEventWith(ModelEventType.ANSWER_VARIABLE_CHANGE);
		}
		
		protected var _calcMode:CalcMode;
		/**
		 * The calculation mode.
		 */
		public function get calcMode():CalcMode {
			return _calcMode;
		}
		
		public function set calcMode(value:CalcMode):void {
			_calcMode = value;
			dispatchEventWith(ModelEventType.CALC_MODE_CHANGE);
		}
		
		/**
		 * The angle unit for entering and displaying angle values.
		 */
		public function get angleUnitMode():AngleUnit {
			return _mathParser.mathContext.angleUnit;
		}
		
		public function set angleUnitMode(value:AngleUnit):void {
			_mathParser.mathContext.angleUnit = value;
			dispatchEventWith(ModelEventType.ANGLE_UNIT_MODE_CHANGE);
		}
		
		protected var _history:Vector.<Formula>;
		/**
		 * The formula history.
		 */
		public function get historyItems():Vector.<Formula> {
			return _history;
		}
		
		public function get numHistoryItems():int {
			return _history.length;
		}
		
		public function getHistoryItemAt(index:int):Formula {
			return _history[index];
		}
		
		public function setHistoryItemAt(formula:Formula, index:int, cloneFormula:Boolean = true):void {
			_history[index] = createHistoryItem(formula, cloneFormula);
			dispatchEventWith(ModelEventType.HISTORY_CHANGE);
		}
		
		public function addHistoryItem(formula:Formula, cloneFormula:Boolean = true):void {
			_history.push(createHistoryItem(formula, cloneFormula));
			dispatchEventWith(ModelEventType.HISTORY_CHANGE);
		}
		
		public function addHistoryItemAt(formula:Formula, index:int, cloneFormula:Boolean = true):void {
			_history.splice(index, 0, createHistoryItem(formula, cloneFormula));
			dispatchEventWith(ModelEventType.HISTORY_CHANGE);
		}
		
		protected function createHistoryItem(formula:Formula, cloneFormula:Boolean):Formula {
			var historyItem:Formula = cloneFormula ? formula.clone(_formulaTokenFactory, false) : new Formula(formula.tokens, formula.result, false);
			if (!cloneFormula) {
				formula.tokens = new Vector.<FormulaToken>();
				formula.result = null;
			}
			return historyItem;
		}
		
		public function removeHistoryItemAt(index:int):void {
			_history[index].dispose(_formulaTokenFactory);
			_history.splice(index, 1);
			dispatchEventWith(ModelEventType.HISTORY_CHANGE);
		}
		
		public function removeAllHistoryItems():void {
			for (var i:int = _history.length - 1; i >= 0; --i)
				_history[i].dispose(_formulaTokenFactory);
			_history.length = 0;
			dispatchEventWith(ModelEventType.HISTORY_CHANGE);
		}
		
		protected var _formulaTokenFactory:IFormulaTokenFactory;
		/**
		 * The formula token factory used to create new tokens.
		 */
		public function get formulaTokenFactory():IFormulaTokenFactory {
			return _formulaTokenFactory;
		}
		
		public function set formulaTokenFactory(value:IFormulaTokenFactory):void {
			_formulaTokenFactory = value;
			if (_mathTokenProvider)
				_mathTokenProvider.formulaTokenFactory = _formulaTokenFactory;
		}
		
		protected var _editableFormula:Formula;
		/**
		 * The editable formula, as opposed to immutable formulae stored in the formula history.
		 */
		public function get editableFormula():Formula {
			return _editableFormula;
		}
		
		protected var _formula:Formula;
		/**
		 * The formula being displayed or edited.
		 */
		public function get formula():Formula {
			return _formula;
		}
		
		public function set formula(value:Formula):void {
			_formula = value;
			dispatchFormulaChangeEvents();
		}
		
		protected function dispatchFormulaChangeEvents(formulaChange:Boolean = true, formulaClear:Boolean = false):void {
			if (formulaChange)
				dispatchEventWith(ModelEventType.FORMULA_CHANGE);
			dispatchEventWith(ModelEventType.TOKENS_CHANGE);
			if (formulaChange || formulaClear)
				dispatchEventWith(ModelEventType.RESULT_CHANGE);
		}
		
		public function clearFormula():void {
			var hasFormulaChanged:Boolean;
			if (_formula != _editableFormula) {
				_formula = _editableFormula;
				hasFormulaChanged = true;
			}
			_formula.clear(_formulaTokenFactory);
			dispatchFormulaChangeEvents(hasFormulaChanged, true);
		}
		
		protected function copyFormulaIfImmutable():Boolean {
			if (_formula != _editableFormula) {
				_editableFormula.tokens = _formula.cloneTokens(_formulaTokenFactory); // TODO: Should check _editableFormula.tokens and dispose if not null?
				_editableFormula.result = null;
				_formula = _editableFormula;
				return true;
			}
			return false;
		}
		
		public function get tokens():Vector.<FormulaToken> {
			return _formula.tokens;
		}
		
		public function get numTokens():int {
			return _formula.tokens.length;
		}
		
		public function getTokenAt(index:int):FormulaToken {
			return _formula.tokens[index];
		}
		
		public function setTokenAt(token:FormulaToken, index:int):void {
			var hasFormulaChanged:Boolean = copyFormulaIfImmutable();
			_formula.tokens[index] = token;
			dispatchFormulaChangeEvents(hasFormulaChanged);
		}
		
		public function addToken(token:FormulaToken):void {
			var hasFormulaChanged:Boolean = copyFormulaIfImmutable();
			_formula.tokens.push(token);
			dispatchFormulaChangeEvents(hasFormulaChanged);
		}
		
		public function addTokenAt(token:FormulaToken, index:int):void {
			var hasFormulaChanged:Boolean = copyFormulaIfImmutable();
			_formula.tokens.splice(index, 0, token);
			dispatchFormulaChangeEvents(hasFormulaChanged);
		}
		
		public function removeTokenAt(index:int, count:int = 1):void {
			var hasFormulaChanged:Boolean = copyFormulaIfImmutable();
			_formulaTokenFactory.disposeToken(_formula.tokens[index]);
			_formula.tokens.splice(index, count);
			dispatchFormulaChangeEvents(hasFormulaChanged);
		}
		
		public function get result():MathExpression {
			return _formula.result;
		}
		
		public function set result(value:MathExpression):void {
			var hasFormulaChanged:Boolean = copyFormulaIfImmutable();
			_formula.result = value;
			if (hasFormulaChanged)
				dispatchEventWith(ModelEventType.FORMULA_CHANGE);
			dispatchEventWith(ModelEventType.RESULT_CHANGE);
		}
	}
}
