package com.razorscript.fxcalc.data {
	import com.razorscript.fxcalc.data.Model;
	import com.razorscript.fxcalc.data.constants.AppConst;
	import com.razorscript.fxcalc.data.dto.StateDTO;
	import com.razorscript.fxcalc.ui.ViewModel;
	
	/**
	 * Converts the app state data between data models and app state DTO.
	 *
	 * @author Gene Pavlovsky
	 */
	public class StateConverter {
		public function StateConverter(model:Model, viewModel:ViewModel) {
			this.model = model;
			this.viewModel = viewModel;
		}
		
		public function saveState(stateDTO:StateDTO = null):StateDTO {
			if (!stateDTO)
				stateDTO = new StateDTO();
			
			stateDTO.appVersion = AppConst.VERSION.versionCode;
			
			return stateDTO;
		}
		
		public function restoreState(stateDTO:StateDTO):void {
			model.lastAppVersion = stateDTO.appVersion;
		}
		
		protected var model:Model;
		protected var viewModel:ViewModel;
	}
}
