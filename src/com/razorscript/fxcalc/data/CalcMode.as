package com.razorscript.fxcalc.data {
	import com.razorscript.utils.enum.EnumBase;
	import com.razorscript.utils.enum.EnumUtil;
	
	/**
	 * Defines constants for calculation modes.
	 *
	 * @author Gene Pavlovsky
	 */
	public class CalcMode extends EnumBase {
		/** General calculations mode. */
		[Enum]
		public static const COMPUTE:CalcMode = factory();
		
		/** Standard deviation mode. */
		[Enum]
		public static const STANDARD_DEVIATION:CalcMode = factory();
		
		/** Regression calculations mode. */
		[Enum]
		public static const REGRESSION:CalcMode = factory();
		
		protected static function factory():CalcMode {
			return new CalcMode(LOCK);
		}
		
		/**
		 * @private
		 */
		public function CalcMode(lock:Object) {
			super(lock);
		}
		
		EnumUtil.registerClass(CalcMode);
	}
}
