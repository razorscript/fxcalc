package com.razorscript.events {
	import starling.events.Event;
	
	/**
	 * Defines methods provided by Starling Framework's event dispatcher.
	 *
	 * @see http://doc.starling-framework.org/core/starling/events/EventDispatcher.html
	 *
	 * @author Gene Pavlovsky
	 */
	public interface IEventDispatcher {
		/**
		 * Registers an event listener at a certain object.
		 *
		 * @see http://doc.starling-framework.org/core/starling/events/EventDispatcher.html#addEventListener()
		 */
		function addEventListener(type:String, listener:Function):void;

		/**
		 * Removes an event listener from the object.
		 *
		 * @see http://doc.starling-framework.org/core/starling/events/EventDispatcher.html#removeEventListener()
		 */
		function removeEventListener(type:String, listener:Function):void;

		/**
		 * Removes all event listeners with a certain type, or all of them if type is null.
		 *
		 * @see http://doc.starling-framework.org/core/starling/events/EventDispatcher.html#removeEventListeners()
		 */
		function removeEventListeners(type:String = null):void;

		/**
		 * Dispatches an event to all objects that have registered listeners for its type.
		 *
		 * @see http://doc.starling-framework.org/core/starling/events/EventDispatcher.html#dispatchEvent()
		 */
		function dispatchEvent(event:Event):void;

		/**
		 * Dispatches an event with the given parameters to all objects that have registered listeners for the given type.
		 *
		 * @see http://doc.starling-framework.org/core/starling/events/EventDispatcher.html#dispatchEventWith()
		 */
		function dispatchEventWith(type:String, bubbles:Boolean = false, data:Object = null):void;

		/**
		 * If called with one argument, figures out if there are any listeners registered for the given event type. If called with two arguments, also determines if a specific listener is registered.
		 *
		 * @see http://doc.starling-framework.org/core/starling/events/EventDispatcher.html#hasEventListener()
		 */
		function hasEventListener(type:String, listener:Function = null):Boolean;
	}
}
