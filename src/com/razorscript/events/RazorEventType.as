package com.razorscript.events {
	
	/**
	 * Common event type constants.
	 *
	 * @author Gene Pavlovsky
	 */
	public class RazorEventType {
		/** Dispatched when something initializes. */
		public static const INIT:String = 'init';
		/** Dispatched when something validates. */
		public static const VALIDATE:String = 'validate';
		
		/** Dispatched when a load operation completes. */
		public static const LOAD:String = 'load';
		/** Dispatched when a load operation fails. */
		public static const LOAD_ERROR:String = 'loadError';
		/** Dispatched when a save operation completes. */
		public static const SAVE:String = 'save';
		/** Dispatched when a save operation fails. */
		public static const SAVE_ERROR:String = 'saveError';
		/** Dispatched when a wipe operation completes. */
		public static const WIPE:String = 'wipe';
		/** Dispatched when a wipe operation fails. */
		public static const WIPE_ERROR:String = 'wipeError';
		
		/** Dispatched when parsing an XML or JSON data fails. */
		public static const PARSE_ERROR:String = 'parseError';
	}
}
