package com.razorscript {
	
	/**
	 * This namespace is used for internal implementation details which can't be made private, protected or internal, because they need to be visible to classes in other packages.
	 */
	public namespace razor_internal = 'http://www.razorscript.com/2015/razor/internal';
}
