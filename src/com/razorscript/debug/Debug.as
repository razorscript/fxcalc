package com.razorscript.debug {
	import com.razorscript.fxcalc.math.tokens.MathTokenCache;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	/**
	 * Provides properties for testing and debugging.
	 *
	 * @author Gene Pavlovsky
	 */
	public class Debug {
		protected static var _instance:Debug;
		
		public static function get instance():Debug {
			return _instance ? _instance : (_instance = new Debug());
		}
		
		public static function set instance(value:Debug):void {
			_instance = value;
		}
		
		public function Debug() {
		}
		
		/***********************************************************************************************************************************************/
		
		/**
		 * Misc.
		 */
		
		public static const DEBUG_MATH_PARSER_PARSE:int = 1;
		public static const DEBUG_MATH_PARSER_EVAL:int = 2;
		
		public var debugMathParser:int = 0;
		
		public var debugDataStorage:Boolean;
		public var debugBinUtil:Boolean;
		
		/***********************************************************************************************************************************************/
		
		/**
		 * Pooling.
		 */
		
		protected var _debugPooling:Boolean;
		
		public function get debugPooling():Boolean {
			return _debugPooling;
		}
		
		public function set debugPooling(value:Boolean):void {
			_debugPooling = value;
			if (value) {
				mathTokenCacheSize = MathTokenCache.length;
				if (!mathTokenCacheTimer) {
					mathTokenCacheTimer = new Timer(5000);
					mathTokenCacheTimer.addEventListener(TimerEvent.TIMER, updateMathTokenCacheSize);
				}
				mathTokenCacheTimer.reset();
				mathTokenCacheTimer.start();
			}
			else {
				if (mathTokenCacheTimer)
					mathTokenCacheTimer.stop();
			}
		}
		
		protected var mathTokenCacheTimer:Timer;
		
		protected function updateMathTokenCacheSize(e:TimerEvent):void {
			mathTokenCacheSize = MathTokenCache.length;
		}
		
		protected var _mathTokenCacheSize:int;
		
		public function get mathTokenCacheSize():int {
			return _mathTokenCacheSize;
		}
		
		public function set mathTokenCacheSize(value:int):void {
			if (!_debugPooling)
				return;
			_mathTokenCacheSize = value;
			if (value > _maxMathTokenCacheSize) {
				_maxMathTokenCacheSize = value;
				Logger.debug(this, 'maxMathTokenCacheSize=' + value);
			}
		}
		
		protected var _maxMathTokenCacheSize:int;
		
		public function get maxMathTokenCacheSize():int {
			return _maxMathTokenCacheSize;
		}
		
		protected var _timerDelayedCallPoolSize:int;
		
		public function get timerDelayedCallPoolSize():int {
			return _timerDelayedCallPoolSize;
		}
		
		public function set timerDelayedCallPoolSize(value:int):void {
			if (!_debugPooling)
				return;
			_timerDelayedCallPoolSize = value;
			if (value > _maxTimerDelayedCallPoolSize) {
				_maxTimerDelayedCallPoolSize = value;
				Logger.debug(this, 'maxTimerDelayedCallPoolSize=' + value);
			}
		}
		
		protected var _maxTimerDelayedCallPoolSize:int;
		
		public function get maxTimerDelayedCallPoolSize():int {
			return _maxTimerDelayedCallPoolSize;
		}
		
		protected var _mathTokenPoolSizes:Object = {};
		
		public function getMathTokenPoolSize(name:String):int {
			return (name in _mathTokenPoolSizes) ? _mathTokenPoolSizes[name] : 0;
		}
		
		public function setMathTokenPoolSize(name:String, value:int):void {
			if (!_debugPooling)
				return;
			_mathTokenPoolSizes[name] = value;
			if (!(name in _maxMathTokenPoolSizes) || (value > _maxMathTokenPoolSizes[name])) {
				_maxMathTokenPoolSizes[name] = value;
				Logger.debug(this, 'maxMathTokenPoolSizes[' + name + ']=' + value);
			}
		}
		
		protected var _maxMathTokenPoolSizes:Object = {};
		
		public function getMaxMathTokenPoolSize(name:String):int {
			return (name in _maxMathTokenPoolSizes) ? _maxMathTokenPoolSizes[name] : 0;
		}
	}
}
