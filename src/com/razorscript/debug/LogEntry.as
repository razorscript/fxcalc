package com.razorscript.debug {
	import com.razorscript.razor_internal;
	import com.razorscript.utils.ClassUtil;
	
	/**
	 * Contains properties describing a log entry.
	 *
	 * @author Gene Pavlovsky
	 */
	public class LogEntry {
		public function LogEntry(severity:int, reporter:*, message:String, timestamp:Number) {
			_severity = severity;
			_reporter = reporter;
			_message = message;
			_timestamp = timestamp;
		}
		
		protected var _severity:int;
		/**
		 * Severity level.
		 */
		public function get severity():int {
			return _severity;
		}
		
		/**
		 * Severity level name.
		 */
		public function get severityName():String {
			return LogLevel.getName(_severity);
		}
		
		protected var _reporter:*;
		/**
		 * Reporter, usually 'this' object or a class reference.
		 */
		public function get reporter():* {
			return _reporter;
		}
		
		/**
		 * Reporter class name.
		 */
		public function get reporterClassName():String {
			return ClassUtil.getClassName(_reporter);
		}
		
		protected var _message:String;
		/**
		 * Message.
		 */
		public function get message():String {
			return _message;
		}
		
		protected var _timestamp:Number;
		/**
		 * Event timestamp.
		 */
		public function get timestamp():Number {
			return _timestamp;
		}
		
		/**
		 * Event timestamp, formatted as a string in fixed-point notation, with 3 digits after the decimal point.
		 */
		public function get timestampString():String {
			return (timestamp / 1000).toFixed(3);
		}
		
		/**
		 * Formats a log message.
		 */
		public function toString(wantSeverity:Boolean = true, wantReporter:Boolean = true, wantTimestamp:Boolean = true):String {
			return ((wantTimestamp && (_timestamp >= 0)) ? ((_timestamp / 1000).toFixed(3) + (((wantSeverity && _severity) || (wantReporter && _reporter)) ? ' ' : ': ')) : '') +
				((wantSeverity && _severity) ? ('[' + LogLevel.getName(_severity) + '] ') : '') +
				((wantReporter && _reporter) ? (ClassUtil.getClassName(_reporter) + ': ') : '') +
				_message;
		}
		
		razor_internal function setTo(severity:int = 0, reporter:* = null, message:String = null, timestamp:Number = NaN):LogEntry {
			_severity = severity;
			_reporter = reporter;
			_message = message;
			_timestamp = timestamp;
			return this;
		}
	}
}
