package com.razorscript.debug {
	
	public function getStackTrace():Array {
		try {
			throw new Error();
		}
		catch (err:Error) {
			var str:String = err.getStackTrace();
		}
		return str.replace(/^\s*at /mg, '').split('\n').slice(2);
	}
}
