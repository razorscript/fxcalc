package com.razorscript.debug {
	import com.razorscript.razor_internal;

	use namespace razor_internal;

	/**
	 * Defines constants for severity log levels.
	 *
	 * @author Gene Pavlovsky
	 */
	public class LogLevel {
		/** Uncategorized. */
		public static const UNSET:int = 	register('');
		/** System is unusable. */
		public static const ALERT:int = 	register('alert');
		/** Critical conditions. */
		public static const CRIT:int = 		register('crit');
		/** Error conditions. */
		public static const ERR:int = 		register('err');
		/** May indicate that an error will occur if action is not taken. */
		public static const WARN:int = 		register('warn');
		/** Events that are unusual, but not error conditions. */
		public static const NOTICE:int = 	register('notice');
		/** Normal operational messages that require no action. */
		public static const INFO:int = 		register('info');
		/** Information useful to developers for debugging. */
		public static const DEBUG:int = 	register('debug');

		razor_internal static var uid:int = 0;

		protected static var levelNames:Vector.<String>;

		public static function register(name:String):int {
			if (!uid)
				levelNames = new Vector.<String>();
			var id:int = uid++;
			levelNames[id] = name;
			return id;
		}

		public static function getName(level:int):String {
			return levelNames[level];
		}

		/**
		 * Returns an empty vector of severity levels.
		 *
		 * @return An empty vector of severity levels.
		 */
		public static function none():Vector.<int> {
			return new Vector.<int>();
		}

		/**
		 * Returns a vector of severity levels, containing all registered severity levels.
		 * If includingNone is true, the UNSET (uncategorized) severity level is included, otherwise not.
		 *
		 * @return A vector of severity levels, containing all registered severity levels.
		 */
		public static function all(includingNone:Boolean = true):Vector.<int> {
			return upTo(uid - 1, includingNone);
		}

		/**
		 * Returns a vector of severity levels, starting from the most severe and ending with the specified level (inclusive).
		 *
		 * @param level A severity level.
		 * @return A vector of severity levels, starting from the most severe and ending with the specified level (inclusive).
		 */
		public static function downTo(level:int):Vector.<int> {
			var vec:Vector.<int> = new Vector.<int>();
			for (var i:int = level; i < uid; ++i)
				vec.push(i);
			return vec;
		}

		/**
		 * Returns a vector of severity levels, starting from the least severe and ending with the specified level (inclusive).
		 * If includingNone is true, the UNSET (uncategorized) severity level is included, otherwise not.
		 *
		 * @param level A severity level.
		 * @param includingNone Whether to include the UNSET (uncategorized) severity level.
		 * @return A vector of severity levels, starting from the least severe and ending with the specified level (inclusive).
		 */
		public static function upTo(level:int, includingNone:Boolean = false):Vector.<int> {
			var vec:Vector.<int> = new Vector.<int>();
			var endIndex:int = includingNone ? 0 : 1;
			for (var i:int = level; i >= endIndex; --i)
				vec.push(i);
			return vec;
		}
	}
}
