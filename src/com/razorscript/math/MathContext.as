package com.razorscript.math {
	import com.razorscript.utils.StrUtil;

	/**
	 * Defines a math context, containing variables, constants and settings which affect the behavior of certain numerical functions.
	 *
	 * @author Gene Pavlovsky
	 */
	public class MathContext {
		/** If specified, integer functions with fraction arguments will return integer fraction (denominator is 1) results. Otherwise, they will return integer results (default). */
		public static const USE_INTEGER_FRACTIONS:uint = 1 << uid++;
		/** If specified, functions with real arguments can return real or complex results. Otherwise, they can return real results only (default). */
		public static const ALLOW_COMPLEX_RESULTS:uint = 1 << uid++;

		protected static var uid:int = 0;

		public static var defaultAbsoluteError:Number = 2.2e-322;
		public static var defaultRelativeError:Number = 1e-14;

		/**
		 * Constructor.
		 *
		 * @param roundingMode The variable name. If null, RoundingMode.defaultValue is used.
		 * @param angleUnit The angle unit. If null, AngleUnit.defaultValue is used.
		 * @param absoluteError The absolute error value (epsilon) to use for comparison functions. If negative, MathContext.defaultAbsoluteError is used.
		 * @param relativeError The relative error value (nu) to use for comparison functions. If negative, MathContext.defaultRelativeError is used.
		 * @param fpMode Floating point operation mode. If null, FPMode.defaultValue is used.
		 * @param options Additional options.
		 * @param variables The variables object. If null, an empty object is used.
		 * @param constants The constants object. If null, an empty object is used.
		 */
		public function MathContext(roundingMode:RoundingMode = null, angleUnit:AngleUnit = null, absoluteError:Number = -1, relativeError:Number = -1, fpMode:FPMode = null, options:uint = 0, variables:Object = null, constants:Object = null) {
			this.roundingMode = roundingMode ? roundingMode : RoundingMode.defaultValue;
			this.angleUnit = angleUnit ? angleUnit : AngleUnit.defaultValue;
			this.absoluteError = (absoluteError >= 0) ? absoluteError : defaultAbsoluteError;
			this.relativeError = (relativeError >= 0) ? relativeError : defaultRelativeError;
			this.fpMode = fpMode ? fpMode : FPMode.defaultValue;
			this.useIntegerFractions = options & USE_INTEGER_FRACTIONS;
			this.allowComplexResults = options & ALLOW_COMPLEX_RESULTS;
			this.variables = variables ? variables : {};
			this.constants = constants ? constants : {};
			this.protectedVariables = new Vector.<String>();
		}

		/**
		 * The algorithm to use for rounding.
		 */
		public var roundingMode:RoundingMode;

		/**
		 * The unit to use for trigonometric functions.
		 */
		public var angleUnit:AngleUnit;

		/**
		 * The absolute error value (epsilon) to use for comparison functions.
		 * Absolute error is necessary when comparing numbers which are very close to zero - for example, when expecting an answer of zero due to subtraction.
		 * The value should be chosen carefully based on the magnitude of the numbers being subtracted - it should be something like (maxInput * MathExt.DOUBLE_EPSILON).
		 * Chosing a good value depends on what numerical computing is being performed, how stable the algorithms are etc.
		 * When comparing numbers which are not very close to zero, relative error works well.
		 * Set this property to 0 to disable using absolute error.
		 *
		 * @see com.razorscript.math.MathExt#nearlyZero()
		 * @see com.razorscript.math.MathExt#nearlyEqual()
		 */
		public var absoluteError:Number;

		/**
		 * The relative error value (nu) to use for comparison functions.
		 * Relative error works well when comparing numbers which are not very close to zero.
		 * A reasonable value is MathExt.DOUBLE_EPSILON, or some small multiple of MathExt.DOUBLE_EPSILON.
		 * Anything smaller than that and it risks being equivalent to no epsilon. It can be made larger within a reasonable extent, if greater error is expected.
		 * Chosing a good value depends on what numerical computing is being performed, how stable the algorithms are etc.
		 * When comparing numbers which are very close to zero, absolute error is necessary.
		 * Set this property to 0 to disable using relative error.
		 *
		 * @see com.razorscript.math.MathExt#nearlyEqual()
		 */
		public var relativeError:Number;

		/**
		 * Floating point operation mode.
		 */
		public var fpMode:FPMode;

		/**
		 * Whether the integer functions with fraction arguments should return integer fraction results or numbers.
		 * If true, integer functions with fraction arguments will return integer fraction (denominator is 1) results.
		 * If false, integer functions with fraction arguments will return integer results.
		 */
		public var useIntegerFractions:Boolean;

		/**
		 * Whether to allow functions with real arguments to return complex results.
		 * If true, functions with real arguments can return real or complex results (e.g. sqrt(-1) returns the complex number `i`).
		 * If false, functions with real arguments can return real results only (e.g. sqrt(-1) throws a 'Domain error' ArithmeticError).
		 */
		public var allowComplexResults:Boolean;

		/**
		 * Contains the defined variables and their values. Values should be mathematical objects.
		 * It is preferrable to use hasVariable(), getVariable(), setVariable(), unsetVariable(), unsetAllVariables() and clearAllVariables() rather than directly accessing this property.
		 */
		public var variables:Object;

		/**
		 * A vector containing the list of all defined variables' names.
		 */
		public function get varNames():Vector.<String> {
			var names:Vector.<String> = new Vector.<String>();
			for (var name:String in variables)
				names.push(name);
			return names;
		}

		/**
		 * Returns if the variable with a specified name is defined.
		 *
		 * @param name The variable name.
		 * @return Boolean true if the variable is defined.
		 */
		public function hasVariable(name:String):Boolean {
			return name in variables;
		}

		/**
		 * Returns the value of the variable with a specified name if that name is defined, otherwise null.
		 *
		 * @param name The variable name.
		 * @return The variable value if the variable is defined, otherwise null.
		 */
		public function getVariable(name:String):* {
			return variables[name];
		}

		/**
		 * Sets the value of the variable with a specified name. If the variable doesn't exist, creates a new variable.
		 *
		 * @param name The variable name.
		 * @param value The variable value.
		 */
		public function setVariable(name:String, value:*):void {
			variables[name] = value;
		}

		/**
		 * Deletes the variable with a specified name. The variable becomes undefined.
		 *
		 * @param name The variable name.
		 */
		public function unsetVariable(name:String):void {
			delete variables[name];
		}

		/**
		 * Contains the list of variable names not affected by unsetAllVariables() and clearAllVariables().
		 * It is preferrable to use isProtectedVariable(), protectVariables() and unprotectVariables() rather than directly accessing this property.
		 */
		public var protectedVariables:Vector.<String>;

		/**
		 * Deletes all defined unprotected variables.
		 */
		public function unsetAllVariables():void {
			for (var name:String in variables) {
				if (protectedVariables.indexOf(name) == -1)
					delete variables[name];
			}
		}

		/**
		 * Sets the values of all defined unprotected variables to 0.
		 */
		public function clearAllVariables():void {
			for (var name:String in variables) {
				if (protectedVariables.indexOf(name) == -1)
					variables[name] = 0;
			}
		}

		/**
		 * Adds variables with the specified names to the list of protected variables.
		 *
		 * @param name A variable name.
		 */
		public function protectVariables(... names):void {
			for each (var name:String in names) {
				if (protectedVariables.indexOf(name) == -1)
					protectedVariables.push(name);
			}
		}

		/**
		 * Removes variables with the specified names from the list of protected variables.
		 *
		 * @param name A variable name.
		 */
		public function unprotectVariables(... names):void {
			for each (var name:String in names) {
				var index:int = protectedVariables.indexOf(name);
				if (index != -1)
					protectedVariables.splice(index, 1);
			}
		}

		/**
		 * Returns true if the variable with a specified name is in the list of protected variables.
		 *
		 * @param name The variable name.
		 * @return Boolean true if the variable is in the list of protected variables.
		 */
		public function isProtectedVariable(name:String):Boolean {
			return protectedVariables.indexOf(name) != -1;
		}

		/**
		 * Contains the defined constants and their values. Values should be mathematical objects.
		 * It is preferrable to use hasConstant(), getConstant() and setConstant() rather than directly accessing this property.
		 */
		public var constants:Object;

		/**
		 * An array containing the list of all defined constants' names.
		 */
		public function get constNames():Vector.<String> {
			var names:Vector.<String> = new Vector.<String>();
			for (var name:String in constants)
				names.push(name);
			return names;
		}

		/**
		 * Returns if the constant with a specified name is defined.
		 *
		 * @param name The constant name.
		 * @return Boolean true if the constant is defined.
		 */
		public function hasConstant(name:String):Boolean {
			return name in constants;
		}

		/**
		 * Returns the value of the constant with a specified name if that name is defined, otherwise null.
		 *
		 * @param name The constant name.
		 * @return The constant value if the constant is defined, otherwise null.
		 */
		public function getConstant(name:String):* {
			return constants[name];
		}

		/**
		 * Sets the value of the constant with a specified name. If the constant already exists, throws an Error.
		 *
		 * @param name The constant name.
		 * @param value The constant value.
		 *
		 * @throws Error - if name is already defined.
		 */
		public function setConstant(name:String, value:*):void {
			if (name in constants)
				throw new Error('Constant "' + name + '" already exists.');
			constants[name] = value;
		}

		public function toString():String {
			return '[MathContext roundingMode=' + roundingMode + ' angleUnit=' + angleUnit + ' relativeError=' + relativeError + ' absoluteError=' + absoluteError +
				' variables=' + StrUtil.objectToString(variables) + ' constants=' + StrUtil.objectToString(constants) + ']';
		}
	}
}
