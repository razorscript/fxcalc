package com.razorscript.math {
	import com.razorscript.utils.enum.EnumBase;
	import com.razorscript.utils.enum.EnumUtil;
	
	/**
	 * Defines constants for floating point operation modes.
	 *
	 * @author Gene Pavlovsky
	 */
	public class FPMode extends EnumBase {
		/**
		 * Direct floating point operation mode. Behaves like a computer.
		 * The underlying math library functions are used as is.
		 * This mode provides the same precision as the underlying math library, and the same pitfalls.
		 * In some cases calculation results might be at odds with mathematics (but expected by a computer scientist).
		 */
		[Enum]
		public static const DIRECT:FPMode = factory();
		
		/**
		 * Mathic floating point operation mode. Behaves like a scientific calculator.
		 * Various checks and special case handlers are used together with the underlying math library functions.
		 * Some precision is sacrificed in order to avoid some of the typical problems associated with the pitfalls of floating point operations.
		 * In most cases calculation results should match those expected by mathematics.
		 */
		[Enum]
		public static const MATHIC:FPMode = factory();
		
		public static var defaultValue:FPMode = DIRECT;
		
		protected static function factory():FPMode {
			return new FPMode(LOCK);
		}
		
		/**
		 * @private
		 */
		public function FPMode(lock:Object) {
			super(lock);
		}
		
		EnumUtil.registerClass(FPMode);
	}
}
