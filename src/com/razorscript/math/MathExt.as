package com.razorscript.math {
	import com.razorscript.math.errors.ArithmeticError;
	import com.razorscript.utils.StrUtil;

	/**
	 * Extra math functions.
	 *
	 * @author Gene Pavlovsky
	 */
	public class MathExt {
		/**
		 * The number of significant digits a double can accurately represent (for normal numbers). Another definition is the max number of digits for decimal-binary-decimal round-trip.
		 * The 53-bit significand of a double is equivalent to (53 * LOG10_2) ≈ 15.955 decimal digits.
		 * Due to wobble, in some ranges almost a full bit might be wasted, bringing the number of accurate decimal digits down to (52 * LOG10_2) ≈ 15.654.
		 * Therefore, for any normal double, 15 digits can be always relied on.
		 *
		 * @see http://www.exploringbinary.com/number-of-digits-required-for-round-trip-conversions/
		 * @see http://docs.oracle.com/cd/E19957-01/806-3568/ncg_goldberg.html
		 * @see https://randomascii.wordpress.com/2012/03/08/float-precisionfrom-zero-to-100-digits-2/
		 */
		public static const DOUBLE_ACC_DIGITS:int = 15;
		/**
		 * The number of significant digits sufficient to uniquely identify a double. Another definition is the min number of digits for binary-decimal-binary round-trip.
		 * The 53-bit significand of a double is equivalent to (53 * LOG10_2) ≈ 15.955 decimal digits.
		 * Due to wobble, in some ranges almost a full decimal digit might be wasted, requiring ceil(16.955) = 17 decimal digits to uniquely identify a double.
		 * Any double, printed as a decimal with 17 digits of precision (with correct rounding), can be converted back to the original double.
		 *
		 * @see http://www.exploringbinary.com/number-of-digits-required-for-round-trip-conversions/
		 * @see http://docs.oracle.com/cd/E19957-01/806-3568/ncg_goldberg.html
		 * @see https://randomascii.wordpress.com/2012/03/08/float-precisionfrom-zero-to-100-digits-2/
		 */
		public static const DOUBLE_UNIQ_DIGITS:int = 17;

		/** The machine epsilon, ε is defined as the smallest positive number such that (1.0 + ε) is not equal to 1.0, with an approximate value of 2.22044604925031308084e-16. */
		public static const DOUBLE_EPSILON:Number = 2.22044604925031308084e-16;

		/** The smallest representable positive subnormal double (Number.MIN_VALUE), with an approximate value of 4.94065645841246544176e-324. */
		public static const DOUBLE_MIN:Number = 4.94065645841246544176e-324;
		/** The smallest representable positive normal double, with an approximate value of 2.22507385850720138309e-308. */
		public static const DOUBLE_MIN_NORMAL:Number = 2.22507385850720138309e-308;
		/** The largest representable finite double (Number.MAX_VALUE), with an approximate value of 1.79769313486231570814e+308. */
		public static const DOUBLE_MAX:Number = 1.79769313486231570814e+308;
		/** The smallest representable finite double (-Number.MAX_VALUE), with an approximate value of -1.79769313486231570814e+308. */
		public static const DOUBLE_NEG_MAX:Number = -1.79769313486231570814e+308;

		/** The smallest integer n, for which any integer x in the range (-n >= x >= n) is uniquely representable by a double (2^53), with a value of -9007199254740992. */
		public static const DOUBLE_INT_MIN:Number = 9007199254740992;
		/** The largest integer n, for which any integer x in the range (-n <= x <= n) is uniquely representable by a double (2^53), with a value of 9007199254740992. */
		public static const DOUBLE_INT_MAX:Number = 9007199254740992;

		/**
		 * A mathematical constant for the base-10 logarithm of 2, with an approximate value of 0.30102999566398114.
		 * Multiply a base-2 logarithm by this value to obtain the base-10 logarithm.
		 */
		public static const LOG10_2:Number = Math.LN2 / Math.LN10;
		/**
		 * A mathematical constant for the base-2 logarithm of 10, with an approximate value of 3.3219280948873626.
		 * Multiply a base-10 logarithm by this value to obtain the base-2 logarithm.
		 */
		public static const LOG2_10:Number = Math.LN10 / Math.LN2;

		/** A mathematical constant for one-half of pi (π/2), with an approximate value of 1.5707963267948966. */
		public static const PI1_2:Number = Math.PI / 2;
		/** A mathematical constant for pi times two (π*2), with an approximate value of 6.283185307179586. */
		public static const PI_X2:Number = Math.PI * 2;

		/** A mathematical constant for the 2 to the power of 16, with a value of 65,536. */
		public static const POW2_16:Number = 65536;
		/** A mathematical constant for the 2 to the power of 32, with a value of 4,294,967,296 (10 digits). */
		public static const POW2_32:Number = 4294967296;
		/** A mathematical constant for the 2 to the power of 64, with an approximate value of 18,446,744,073,709,552,000 (20 digits), the accurate value (not representable by a double) is 18,446,744,073,709,551,616. */
		public static const POW2_64:Number = 18446744073709552000;

		/** A mathematical constant for pi divided by 180 (π/180), with an approximate value of 0.017453292519943296. Multiply an angle in degrees by this constant to convert it to radians. */
		public static const DEG2RAD:Number = Math.PI / 180;
		/** A mathematical constant for pi divided by 200 (π/200), with an approximate value of 0.015707963267948966. Multiply an angle in degrees by this constant to convert it to grads. */
		public static const GRAD2RAD:Number = Math.PI / 200;

		/** A mathematical constant for 180 divided by pi (180/π), with an approximate value of 57.29577951308232. Multiply an angle in radians by this constant to convert it to degrees. */
		public static const RAD2DEG:Number = 180 / Math.PI;
		/** A mathematical constant for 200 divided by pi (200/π), with an approximate value of 63.66197723675813. Multiply an angle in radians by this constant to convert it to grads. */
		public static const RAD2GRAD:Number = 200 / Math.PI;

		/**
		 * Returns true if a number is an integer.
		 *
		 * @param x A number.
		 * @return Boolean true if the number is an integer.
		 */
		[Inline]
		public static function isInteger(x:Number):Boolean {
			return x == Math.round(x);
		}

		/**
		 * Returns true if a number is NaN (not-a-number). Faster than the global isNaN().
		 *
		 * @param x A number.
		 * @return Boolean true if the number is a NaN (not-a-number).
		 */
		[Inline]
		public static function isNaN(x:Number):Boolean {
			return x != x;
		}

		/**
		 * Returns true if a number is not NaN (not-a-number). Faster than the global !isNaN().
		 *
		 * @param x A number.
		 * @return Boolean true if the number is not a NaN (not-a-number).
		 */
		[Inline]
		public static function isNotNaN(x:Number):Boolean {
			return x == x;
		}

		/**
		 * Returns the sign of a number (0 if the number is zero, ±1 if the number is positive or negative).
		 *
		 * @param x A number.
		 * @return The sign of the number (0 or ±1).
		 */
		[Inline]
		public static function sign(x:Number):int {
			return (x < 0) ? -1 : (x > 0) ? 1 : 0;
		}

		/**
		 * Calculates the quotient of floored division of two values.
		 * The result is rounded to the nearest integer towards negative infinity.
		 * The value of `y * div(x, y) + mod(x, y)` is equal to x.
		 *
		 * @see http://en.wikipedia.org/wiki/Modulo_operation
		 *
		 * @param x A number.
		 * @param y A number.
		 * @return The quotient of x divided by y, rounded to the nearest integer towards negative infinity.
		 */
		[Inline]
		public static function div(x:Number, y:Number):Number {
			return Math.floor(x / y);
		}

		/**
		 * Calculates the remainder of floored division of two values.
		 * The result has the sign of the divisor (unlike ActionScript's `%` operator).
		 * The value of `y * div(x, y) + mod(x, y)` is equal to x.
		 *
		 * @see http://en.wikipedia.org/wiki/Modulo_operation
		 *
		 * @param x A number.
		 * @param y A number.
		 * @return The remainder of x divided by y, with the sign of y.
		 */
		[Inline]
		public static function mod(x:Number, y:Number):Number {
			return (y == 0) ? x : (x - y * Math.floor(x / y));
		}

		/**
		 * Clamps a number into the specified range.
		 *
		 * @param x A number.
		 * @param min A number that represents the minimum value (inclusive) of the result.
		 * @param max A number that represents the maximum value (inclusive) of the result.
		 * @return The number clamped into the specified range.
		 */
		[Inline]
		public static function clamp(x:Number, min:Number, max:Number):Number {
			return (x < min) ? min : (x > max) ? max : x;
		}

		/**
		 * Returns the number of significant digits a number has.
		 *
		 * @param x A number.
		 * @return The number of significant digits.
		 */
		public static function digits(x:Number):int {
			return x.toString().replace(/^ [-+]? 0* \.? 0* (\d*) \.? (\d*)/x, '$1$2').length;
		}

		/**
		 * Returns the nearest representable number which is greater than x.
		 *
		 * @param x A number of type Number or Double.
		 * @return The nearest representable number which is greater than x.
		 */
		public static function nextNumber(x:Number):Number {
			var scale:Number, y:Number;

			// NaNs and Infinities.
			if (isNaN(x) || !isFinite(x))
				return x;

			// Max normal.
			if (x == DOUBLE_MAX)
				return Infinity;

			// [Min normal, Max normal).
			if (x >= DOUBLE_MIN_NORMAL) {
				scale = Math.pow(2, Math.max(0, (Math.log(Math.abs(x)) * Math.LOG2E + 1021) / 2));
				y = DOUBLE_MIN * scale * scale;
				while ((x + y) == x)
					y *= 2;
				return x + y;
			}

			// [Min subnormal, Min normal).
			if (x >= DOUBLE_MIN)
				return x * 2;

			// Zero.
			if (x == 0)
				return DOUBLE_MIN;

			// [-Max normal, -Min normal).
			if (x < -DOUBLE_MIN_NORMAL) {
				scale = Math.pow(2, Math.max(0, (Math.log(Math.abs(x)) * Math.LOG2E + 1021) / 2));
				y = DOUBLE_MIN * scale * scale;
				while ((x + y) == x)
					y *= 2;
				return x + y;
			}

			// [-Min normal, -Min subnormal].
			return x / 2; // (x <= -DOUBLE_MIN)
		}

		/**
		 * Returns the nearest representable number which is less than x.
		 *
		 * @param x A number of type Number or Double.
		 * @return The nearest representable number which is less than x.
		 */
		[Inline]
		public static function priorNumber(x:Number):Number {
			return -nextNumber(-x);
		}

		/**
		 * Returns true if a number is nearly equal to 0.
		 * Compares the absolute difference between the number and 0 to absoluteError.
		 *
		 * @param x The number to compare with 0.
		 * @param absoluteError The maximum absolute difference between x and 0. If absoluteError is 0, the function will return true if x is exactly equal to 0.
		 * @return True if x is nearly equal to 0.
		*/
		[Inline]
		public static function nearlyZero(x:Number, absoluteError:Number):Boolean {
			return Math.abs(x) <= absoluteError;
		}

		/**
		 * Compares two numbers and returns true if they are nearly equal.
		 *
		 * Compares the absolute difference between the numbers to absoluteError.
		 * Absolute error is necessary when comparing numbers which are very close to zero - for example, when expecting an answer of zero due to subtraction.
		 * The value should be chosen carefully based on the magnitude of the numbers being subtracted - it should be something like (maxInput * DOUBLE_EPSILON).
		 * Chosing a good value depends on what numerical computing is being performed, how stable the algorithms are etc.
		 * When comparing numbers which are not very close to zero, relative error works well.
		 * Set absoluteError to 0 to disable using absolute error.
		 *
		 * Compares the relative difference between the numbers to relativeError.
		 * Relative error works well when comparing numbers which are not very close to zero.
		 * A reasonable value is DOUBLE_EPSILON, or some small multiple of DOUBLE_EPSILON.
		 * Anything smaller than that and it risks being equivalent to no epsilon. It can be made larger within a reasonable extent, if greater error is expected.
		 * Chosing a good value depends on what numerical computing is being performed, how stable the algorithms are etc.
		 * When comparing numbers which are very close to zero, absolute error is necessary.
		 * Set relativeError to 0 to disable using relative error.
		 *
		 * @see http://stackoverflow.com/questions/3874627/floating-point-comparison-functions-for-c-sharp
		 * @see https://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition
		 * @see http://floating-point-gui.de/errors/comparison/
		 *
		 * @param x The first number to compare.
		 * @param y The second number to compare.
		 * @param absoluteError The maximum absolute difference between x and y.
		 * @param relativeError The maximum relative difference between x and y. If relativeError is 0, only the absolute difference comparison is used.
		 * @return True if x and y are nearly equal.
		 */
		public static function nearlyEqual(x:Number, y:Number, absoluteError:Number, relativeError:Number):Boolean {
			// Shortcut, also handles infinities.
			if (x == y)
				return true;
			// A NaN is not equal to any number, including another NaN.
			if (isNaN(x) || isNaN(y))
				return false;

			if (isFinite(x) && isFinite(y)) {
				// Both numbers are finite, do the absolute difference comparison first.
				var diff:Number = Math.abs(x - y);
				if (diff <= absoluteError)
					return true;

				// Use relative difference comparison.
				x = Math.abs(x);
				y = Math.abs(y);
				return diff <= ((x > y) ? x : y) * relativeError;
			}
			else {
				// One number is finite, the other is not.
				return false;
			}
		}

		/**
		 * Returns a number rounded towards zero.
		 *
		 * @param x The number to round.
		 * @return The rounded number.
		 */
		[Inline]
		public static function fix(x:Number):Number {
			return (x >= 0) ? Math.floor(x) : Math.ceil(x);
		}

		/**
		 * Returns a number rounded according to the specified rounding mode.
		 *
		 * @param x The number to round.
		 * @param roundingMode The rounding mode to use.
		 * @return The rounded number.
		 *
		 * @throws ArithmeticError - if roundingMode is RoundingMode.UNNECESSARY and value is not a whole number.
		 */
		public static function round(x:Number, roundingMode:RoundingMode):Number {
			if (roundingMode == RoundingMode.AWAY_FROM_ZERO)
				return (x >= 0) ? Math.ceil(x) : Math.floor(x);
			if (roundingMode == RoundingMode.TOWARDS_ZERO)
				return fix(x);

			if (roundingMode == RoundingMode.CEILING)
				return Math.ceil(x);
			if (roundingMode == RoundingMode.FLOOR)
				return Math.floor(x);

			if (roundingMode == RoundingMode.HALF_AWAY_FROM_ZERO)
				return (x >= 0) ? Math.round(x) : -Math.round(-x);
			if (roundingMode == RoundingMode.HALF_TOWARDS_ZERO)
				return (x >= 0) ? -Math.round(-x) : Math.round(x);

			if (roundingMode == RoundingMode.HALF_UP)
				return Math.round(x);
			if (roundingMode == RoundingMode.HALF_DOWN)
				return -Math.round(-x);

			if ((roundingMode == RoundingMode.HALF_EVEN) || (roundingMode == RoundingMode.HALF_ODD)) {
				var value1:Number = Math.round(x);
				var value2:Number = -Math.round(-x);
				if (value1 == value2)
					return value1;
				if (roundingMode == RoundingMode.HALF_EVEN)
					return (value1 % 2) ? value2 : value1;
				else
					return (value1 % 2) ? value1 : value2;
			}

			if (roundingMode == RoundingMode.UNNECESSARY) {
				if (isInteger(x))
					return x;
				else
					throw new ArithmeticError('Rounding is necessary (' + x + ').');
			}

			return x;
		}

		/**
		 * Rounds a number to the specified number of decimal places according to the specified rounding mode.
		 *
		 * @param x The number to round.
		 * @param fractionDigits An integer between 0 and 17, inclusive, that represents the desired number of decimal places.
		 * @param roundingMode The rounding mode to use.
		 * @return The rounded number.
		 */
		public static function roundToDigits(value:Number, fractionDigits:int, roundingMode:RoundingMode = null):Number {
			if (!fractionDigits)
				return round(value, roundingMode);

			const EXP10:Number = Math.pow(10, fractionDigits);
			return round(value * EXP10, roundingMode) / EXP10;
		}

		/**
		 * Rounds a number to the specified interval. Returns the multiple of the interval which is nearest to the specified number according to the specified rounding mode.
		 *
		 * @param x The number to round.
		 * @param interval A positive number that represents the rounding interval.
		 * @param roundingMode The rounding mode to use.
		 * @return The rounded number.
		 */
		public static function roundToNearest(x:Number, interval:Number, roundingMode:RoundingMode = null):Number {
			return round(x / interval, roundingMode) * interval;
		}

		/**
		 * Returns a pseudo-random number n with a specified number of fractional digits, where (min <= n < max).
		 * If fractionDigits is a negative value, no rounding is performed.
		 *
		 * @param min A number that represents the minimum value (inclusive) of the result.
		 * @param max A number that represents the maximum value (exclusive) of the result.
		 * @param fractionDigits An integer between 0 and 17, inclusive, that represents the desired number of decimal places, or a negative number, in which case no rounding is performed.
		 * @return A pseudo-random number.
		 */
		public static function randomRange(min:Number, max:Number, fractionDigits:int = -1):Number {
			return (fractionDigits < 0) ? (min + (max - min) * Math.random()) : roundToDigits(min + (max - min) * Math.random(), fractionDigits, RoundingMode.FLOOR);
		}

		/**
		 * Returns a pseudo-random signed integer value (stored in type Number) n with a specified number of fractional digits, where (min <= n <= max).
		 *
		 * @param min A number that represents the minimum value (inclusive) of the result.
		 * @param max A number that represents the maximum value (inclusive) of the result.
		 * @return A pseudo-random signed integer.
		 */
		[Inline]
		public static function randomRangeInt(min:Number, max:Number):Number {
			return Math.floor(Math.random() * (max - min + 1)) + min;
		}

		/**
		 * Converts angle to radians from the specified angle unit.
		 *
		 * @param x The angle to convert to radians.
		 * @param angleUnit The angle unit to convert from.
		 * @return The angle converted to radians.
		 */
		[Inline]
		public static function angleToRadians(x:Number, angleUnit:AngleUnit):Number {
			return (angleUnit == AngleUnit.RADIAN) ? x : ((angleUnit == AngleUnit.DEGREE) ? DEG2RAD : GRAD2RAD) * x;
		}

		/**
		 * Converts angle from radians to the specified angle unit.
		 *
		 * @param x The angle to convert from radians.
		 * @param angleUnit The angle unit to convert to.
		 * @return The angle converted to a specified angle unit.
		 */
		[Inline]
		public static function angleFromRadians(x:Number, angleUnit:AngleUnit):Number {
			return (angleUnit == AngleUnit.RADIAN) ? x : ((angleUnit == AngleUnit.DEGREE) ? RAD2DEG : RAD2GRAD) * x;
		}
	}
}
