package com.razorscript.math {
	import com.razorscript.utils.enum.EnumBase;
	import com.razorscript.utils.enum.EnumUtil;
	
	/**
	 * Defines constants for supported angle units.
	 *
	 * 90° = Pi/2 radians = 100 grads
	 *
	 * @author Gene Pavlovsky
	 */
	public class AngleUnit extends EnumBase {
		/** Degrees (1 turn = 360 degrees). */
		[Enum(ordinal=0)]
		public static const DEGREE:AngleUnit = factory();
		
		/** Radians (1 turn = 2*π ≈ 6.28 radians). */
		[Enum(ordinal=1)]
		public static const RADIAN:AngleUnit = factory();
		
		/** Grads (1 turn = 400 grads). */
		[Enum(ordinal=2)]
		public static const GRAD:AngleUnit = factory();
		
		public static var defaultValue:AngleUnit = DEGREE;
		
		protected static function factory():AngleUnit {
			return new AngleUnit(LOCK);
		}
		
		/**
		 * @private
		 */
		public function AngleUnit(lock:Object) {
			super(lock);
		}
		
		EnumUtil.registerClass(AngleUnit);
	}
}
