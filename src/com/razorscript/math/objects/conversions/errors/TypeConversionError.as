package com.razorscript.math.objects.conversions.errors {
	
	/**
	 * Thrown when an exceptional condition has occurred during converting a value to another type.
	 *
	 * @author Gene Pavlovsky
	 */
	public class TypeConversionError extends Error {
		public function TypeConversionError(message:* = '', id:* = 0) {
			super(message, id);
		}
	}
}
