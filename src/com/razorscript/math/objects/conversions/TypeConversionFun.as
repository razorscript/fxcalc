package com.razorscript.math.objects.conversions {
	import com.razorscript.math.functions.polymorphism.TypeConverter;
	import com.razorscript.math.objects.ComplexNumber;
	import com.razorscript.math.objects.Fraction;
	import com.razorscript.math.objects.NumberFun0;
	import com.razorscript.math.objects.conversions.errors.TypeConversionError;

	/**
	 * Type conversion functions.
	 *
	 * @author Gene Pavlovsky
	 */
	public class TypeConversionFun {
		/**
		 * Adds conversions to a type converter and returns that type converter.
		 *
		 * @param typeConverter A type converter.
		 * @param safeConv Whether to add safe conversions.
		 * @param unsafeConv Whether to add unsafe conversions.
		 * @param compoundConv Whether to add additional compound conversions, using at most one intermediate conversion.
		 * @return The type converter.
		 */
		public static function extend(typeConverter:TypeConverter, safeConv:Boolean = true, unsafeConv:Boolean = true, compoundConv:Boolean = true):TypeConverter {
			if (safeConv) {
				typeConverter.addConversion(Number, Fraction, TypeConversionFun.numberToFraction);
				typeConverter.addConversion(Number, ComplexNumber, TypeConversionFun.numberToComplexNumber);
				typeConverter.addConversion(NumberFun0, Number, TypeConversionFun.numberFun0ToNumber);
			}

			if (unsafeConv) {
				typeConverter.addConversion(Fraction, Number, TypeConversionFun.fractionToNumber);
				typeConverter.addConversion(ComplexNumber, Number, TypeConversionFun.complexNumberToNumber);
			}

			if (compoundConv)
				typeConverter.createCompoundConversions();

			return typeConverter;
		}

		public static function numberToFraction(x:Number):Fraction {
			return new Fraction(x);
		}

		public static function numberToComplexNumber(x:Number):ComplexNumber {
			return new ComplexNumber(x);
		}

		public static function numberFun0ToNumber(x:NumberFun0):Number {
			return x.fun();
		}

		// TODO: MathLib or client code should provide conversion functions, using isZero (supporting FPMode) instead of exact comparisons.
		// TODO: Remove range and division by zero checks, letting them be detected by subsequent code? Or keep these as is and add custom functions.

		public static function fractionToNumber(x:Fraction):Number {
			if (x.d == 0)
				throw new TypeConversionError('Failed to convert a Fraction (' + x + ') to Number: Division by zero.');
			var result:Number = x.n / x.d;
			if (isFinite(result))
				return result;
			throw new TypeConversionError('Failed to convert a Fraction (' + x + ') to Number: Result is out of range.');
		}

		public static function complexNumberToNumber(x:ComplexNumber):Number {
			if (x.im == 0)
				return x.re;
			throw new TypeConversionError('Failed to convert a ComplexNumber (' + x + ') to Number: Value has a non-zero imaginary part.');
		}
	}
}
