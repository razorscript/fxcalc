package com.razorscript.math.objects {
	import com.razorscript.razor_internal;
	
	/**
	 * Fraction mathematical object.
	 *
	 * @author Gene Pavlovsky
	 */
	public class Fraction {
		/**
		 * Whether or not to automatically reduce fractions on creation.
		 */
		public static var autoReduce:Boolean = true;
		
		/**
		 * TODO: Add autoParse (working name) option - if yes, and numerator/denominator are not integers, find best approximation.
		 * If no, keep numerator and denominator as floats (or `n = numerator / denominator; d = 1`). Add some flag to test if n and d (or just n) are integers.
		 */
		
		public function Fraction(numerator:* = 0, denominator:* = 1) {
			// TODO: Throw error if denominator == 0? Or handle this in MathLib?
			n = Number(numerator);
			d = Number(denominator);
			if (d < 0) {
				n = -n;
				d = -d;
			}
		}
		
		// TODO: Change to getters for guaranteed immutability? Is there a performance hit?
		// TODO: Store sign separately?
		
		/**
		 * Numerator. Stores the sign of the fraction.
		 */
		public var n:Number;
		
		/**
		 * Denominator. Always positive.
		 */
		public var d:Number;
		
		public function clone():* {
			return new Fraction(n, d);
		}
		
		public function toString():String {
			return n + '⁄' + d;
		}
		
		public function valueOf():Number {
			return n / d;
		}
		
		/**
		 * @private
		 */
		public const _typeName:String = 'Fraction';
		
		/*protected function gcd(a:Number, b:Number):Number {
			if (a < 0)
				a = -a; // TODO: Needed?
			if (b < 0)
				b = -b; // TODO: Needed?
			if (!a)
				return b;
			if (!b)
				return a;
			while (1) {
				a %= b;
				if (!a)
					return b;
				b %= a;
				if (!b)
					return a;
			}
		}*/
	}
}
