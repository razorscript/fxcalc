package com.razorscript.math.objects {
	import com.razorscript.razor_internal;
	
	/**
	 * Complex number mathematical object.
	 *
	 * @author Gene Pavlovsky
	 */
	public class ComplexNumber {
		public function ComplexNumber(re:* = 0, im:* = 0) {
			this.re = Number(re);
			this.im = Number(im);
		}
		
		public var re:Number;
		public var im:Number;
		
		public function clone():* {
			return new ComplexNumber(re, im);
		}
		
		public function toString():String {
			return (re ? (re + ((im >= 0) ? '+' : '')) : '') + ((im == 1) ? '' : (im == -1) ? '-' : im) + 'i';
		}
		
		/**
		 * @private
		 */
		public const _typeName:String = 'ComplexNumber';
	}
}
