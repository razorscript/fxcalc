package com.razorscript.math.objects {
	import com.razorscript.razor_internal;
	
	/**
	 * Number nullary function mathematical object.
	 *
	 * @author Gene Pavlovsky
	 */
	public class NumberFun0 {
		public function NumberFun0(fun:Function) {
			if (fun.length)
				throw new ArgumentError('The fun argument is not a nullary function.');
			this.fun = fun;
		}
		
		public var fun:Function;
		
		public function eval():Number {
			return fun();
		}
		
		public function clone():* {
			return new NumberFun0(fun);
		}
		
		public function toString():String {
			return '[NumberFun0]';
		}
		
		/**
		 * @private
		 */
		public const _typeName:String = 'NumberFun0';
	}
}
