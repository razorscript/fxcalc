package com.razorscript.math.supportClasses {
	import com.razorscript.math.errors.ArithmeticError;
	import com.razorscript.utils.StrUtil;
	import com.razorscript.utils.VecUtil;

	/**
	 * Represents a very limited high-precision integer number.
	 * Number is stored in a Vector.<uint>.
	 *
	 * @author Gene Pavlovsky
	 */
	public class SimpleBigInt {
		public function SimpleBigInt(numWords:int) {
			words = new Vector.<uint>(numWords);
			_length = numWords;
		}

		protected var _length:int;
		/**
		 * The number of words in the number storage vector.
		 */
		public function get length():int {
			return _length;
		}

		public function set length(value:int):void {
			_length = words.length = value;
		}

		/**
		 * Whether all the bits in this number are zero.
		 */
		public function get isZero():Boolean {
			for (var i:int = 0; i < _length; ++i) {
				if (words[i])
					return _isZeroFlag = false;
			}
			return _isZeroFlag = true;
		}

		protected var _isZeroFlag:Boolean;
		/**
		 * Whether all the bits in this number are zero after the last operation.
		 * This flag is only updated by div16(), mul16() and isZero getter.
		 */
		public function get isZeroFlag():Boolean {
			return _isZeroFlag;
		}

		/**
		 * Returns the value of the word at the specified index, 0 being the index of the most significant word.
		 *
		 * @param index Index of the word to get (0 being the most significant word).
		 * @return The value of the word as an uint.
		 */
		public function getWordAt(index:uint):uint {
			return words[index];
		}

		/**
		 * Sets the number to 0.
		 */
		public function clear():void {
			for (var i:int = 0; i < _length; ++i)
				words[i] = 0;
		}

		/**
		 * Insert the bits from value into this number, shifted in from the least significant end by the specified number of bits.
		 *
		 * A shift of zero or less means that none of the bits will be shifted in.
		 * A shift of one means that the high bit of value will be in the least significant bit of this number.
		 * A shift of 32 means that all value's bits will be in the least significant word of this number. Etc.
		 *
		 * If the replace argument is true, the corresponding bits in this number will be cleared before insertion.
		 *
		 * @param value An integer containing the bits to insert.
		 * @param shiftDepth The depth to shift the bits to.
		 * @param replace If true, the corresponding bits in the words vector will be cleared before insertion.
		 */
		public function insertLowBits(value:uint, shiftDepth:int, replace:Boolean = false):void {
			if (shiftDepth <= 0)
				return;

			// Get the index of the high word.
			var wordIndex:uint = _length - 1 - (shiftDepth >> 5);
			shiftDepth &= 0x1f;

			var word:uint;
			// If there are bits to insert into the high word, and the index of the high word is within range, copy the high word bits.
			if (shiftDepth && (wordIndex < _length)) {
				word = replace ? (words[wordIndex] & ~(0xffffffff >>> (32 - shiftDepth))) : words[wordIndex];
				words[wordIndex] = word | (value >>> (32 - shiftDepth));
			}

			++wordIndex;
			// If the index of the low word is within range, copy the low word bits.
			if (wordIndex < _length) {
				word = replace ? (words[wordIndex] & ~(0xffffffff << shiftDepth)) : words[wordIndex];
				words[wordIndex] = word | (value << shiftDepth);
			}
		}

		/**
		 * Insert the bits from value into this number, shifted in from the most significant end by the specified number of bits.
		 *
		 * A shift of zero or less means that none of the bits will be shifted in.
		 * A shift of one means that the high bit of value will be in the most significant bit of this number.
		 * A shift of 32 means that all value's bits will be in the most significant word of this number.
		 *
		 * If the replace argument is true, the corresponding bits in this number will be cleared before insertion.
		 *
		 * @param value An integer containing the bits to insert.
		 * @param shiftDepth The depth to shift the bits to.
		 * @param replace If true, the corresponding bits in the words vector will be cleared before insertion.
		 */
		public function insertHighBits(value:uint, shiftDepth:int, replace:Boolean = false):void {
			insertLowBits(value, ((_length + 1) << 5) - shiftDepth, replace);
		}

		/**
		 * Divides this number by a specified value, and returns the remainder, an integer between 0 and (value - 1).
		 *
		 * @param value An integer between 1 and 65535 to divide by.
		 * @return The remainder from dividing this number by value.
		 *
		 * @throws ArithmeticError - if value is 0.
		 * @throws RangeError - if value is greater than 65535.
		 */
		public function div16(value:uint):uint {
			if (!value)
				throw new ArithmeticError('Division by zero.');
			if (value > 65535)
				throw new RangeError('The value argument (' + value + ') is outside the range [1, 65535].');

			_isZeroFlag = true;

			var dividend:uint;
			var remainder:uint = 0;

			// Counting trailing zero words doesn't provide a performance gain, since after a division some digits usually get written to every trailing word.

			// Skip leading zero words.
			for (var i:int = 0; (i < _length) && (words[i] == 0); ++i)
				;

			// Long division.
			for (; i < _length; ++i) {
				var word:uint = words[i];
				var s16:uint = word >>> 16;
				var s0:uint = word & 0xffff;

				dividend = (remainder << 16) + s16;
				s16 = (dividend / value) << 16;
				remainder = dividend % value;

				dividend = (remainder << 16) + s0;
				s0 = dividend / value;
				remainder = dividend % value;

				words[i] = word = s0 | s16;
				_isZeroFlag &&= word == 0;
			}

			return remainder;
		}

		/**
		 * Multiplies the number by a specified value, and returns the overflow, an integer between 0 and (value - 1).
		 *
		 * @param value An integer between 0 and 65535 to multiply by.
		 * @return The overflow from multiplying this number by value.
		 *
		 * @throws RangeError - if value is greater than 65535.
		 */
		public function mul16(value:uint):uint {
			if (value > 65535)
				throw new RangeError('The value argument (' + value + ') is outside the range [0, 65535].');

			_isZeroFlag = true;

			var result:uint;
			var overflow:uint = 0;

			// Count leading zero words.
			for (var i:int = 0; (i < _length) && (words[i] == 0); ++i)
				;
			var firstIndex:int = i;

			// Skip trailing zero words.
			for (i = _length - 1; (i >= 0) && (words[i] == 0); --i)
				;

			// Long multiplication.
			for (; (i >= firstIndex) || (overflow && (i >= 0)); --i) {
				var word:uint = words[i];
				var s16:uint = word >>> 16;
				var s0:uint = word & 0xffff;

				result = value * s0 + overflow;
				s0 = result & 0xffff;
				overflow = result >>> 16;

				result = value * s16 + overflow;
				s16 = (result & 0xffff) << 16;
				overflow = result >>> 16;

				words[i] = word = s0 | s16;
				_isZeroFlag &&= word == 0;
			}

			return overflow;
		}

		public function toString():String {
			var str:String = '';
			for (var i:int = 0; i < _length; ++i)
				str += StrUtil.lpad(words[i].toString(2), '0', 32);
			return '[SimpleBigInt length=' + length + ' bits=' + VecUtil.joinGrouped(StrUtil.splitw(str, 8), 4, ',', '__') + ']';
		}

		/** Number storage vector holding words in big-endian order (0 is the index of the most significant word). */
		protected var words:Vector.<uint>;
	}
}
