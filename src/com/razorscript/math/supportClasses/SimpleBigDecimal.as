package com.razorscript.math.supportClasses {
	import com.razorscript.razor_internal;
	import com.razorscript.utils.StrUtil;

	use namespace razor_internal;

	/**
	 * Represents a very limited high-precision decimal floating-point number.
	 * Number is represented in normalized exponential notation, with the coefficient stored as a string of decimal digits, starting with a non-zero digit. Insignificant trailing zeros are not stored.
	 *
	 * @author Gene Pavlovsky
	 */
	public class SimpleBigDecimal {
		/** The minimum value of base-10 exponent for representable finite non-zero numbers. */
		public static const EXPONENT_MIN:int = -int.MAX_VALUE;
		/** The maximum value of base-10 exponent for representable finite non-zero numbers. */
		public static const EXPONENT_MAX:int = int.MAX_VALUE;
		/** The value of base-10 exponent used to represent ∞ (if dig == '') and NaNs (if dig != ''). */
		public static const EXPONENT_INFINITY:int = int.MIN_VALUE;

		/** The number +0. */
		public static const ZERO:SimpleBigDecimal = new SimpleBigDecimal(1, 0, '0');
		/** The number -0. */
		public static const NEG_ZERO:SimpleBigDecimal = new SimpleBigDecimal(-1, 0, '0');
		/** Positive infinity. */
		public static const INFINITY:SimpleBigDecimal = new SimpleBigDecimal(1, EXPONENT_INFINITY);
		/** Negative infinity. */
		public static const NEG_INFINITY:SimpleBigDecimal = new SimpleBigDecimal(-1, EXPONENT_INFINITY);
		/** A NaN (not-a-number). */
		public static const NAN:SimpleBigDecimal = new SimpleBigDecimal(1, EXPONENT_INFINITY, 'NaN');

		public function SimpleBigDecimal(sign:int = 1, exp:int = 0, dig:String = '') {
			this.sign = sign;
			this.exp = exp;
			this.dig = dig;
		}

		/** The sign of the number (±1). Zeros are also signed (±0). */
		public var sign:int;
		/** The base-10 exponent of the number. */
		public var exp:int;
		/** The significant digits of the number as a string of decimal digits, starting with a non-zero digit. The decimal point is implied at index 1, and not stored in the string. Insignificant trailing zeros are not stored. */
		public var dig:String;

		/**
		 * Whether this number is ±0.
		 */
		public function get isZero():Boolean {
			return dig == '0';
		}

		/**
		 * Whether this number is ∞.
		 */
		public function get isInfinity():Boolean {
			return (exp == EXPONENT_INFINITY) && !dig;
		}

		/**
		 * Whether this number is NaN (not-a-number).
		 */
		public function get isNaN():Boolean {
			return (exp == EXPONENT_INFINITY) && dig;
		}

		/**
		 * If necessary, rounds the number to the specified precision (number of significant digits), using the half away from zero rounding method.
		 * If the number is an infinity or NaN, or the number contains less characters than precision, no rounding is performed.
		 * If precision is 0, rounds the most significant digit half away from zero. If rounded down, the number becomes 0. If rounded up, the number's exponent is incremented and digits are set to 1.
		 * Returns this object.
		 *
		 * @param precision The number of significant digits to keep.
		 *
		 * @throws RangeError - if precision is negative.
		 * @return This object.
		 */
		public function roundToPrecision(precision:int):SimpleBigDecimal {
			if (precision < 0)
				throw new RangeError('The precision argument (' + precision + ') must be a non-negative number.');
			if ((dig.length <= precision) || (exp == EXPONENT_INFINITY))
				return this;

			// Get the first digit that will be discarded.
			var digit:uint = dig.charCodeAt(precision) - 48;
			if (precision) {
				// Round half away from zero.
				if (digit < 5) {
					// Round down.
					dig = StrUtil.rtrim(dig.substr(0, precision), '0');
				}
				else {
					// Round up.
					for (var i:int = precision - 1; i >= 0; --i) {
						digit = dig.charCodeAt(i) - 47; // Increment by 1.
						if (digit < 10)
							break;
					}
					if (digit == 10) {
						// Rounding the number has incremented it's exponent.
						dig = '1';
						++exp;
					}
					else {
						dig = (i > 0) ? (dig.substr(0, i) + digit) : digit.toString();
					}
				}
			}
			else {
				// Precision is 0, round half away from zero.
				if (digit < 5) {
					dig = '0';
					exp = 0;
				}
				else {
					dig = '1';
					++exp;
				}
			}

			return this;
		}

		/**
		 * Sets the object's properties.
		 * Returns this object.
		 *
		 * @param sign The sign of the number (±1). Zeros are also signed (±0).
		 * @param exp The base-10 exponent of the number.
		 * @param dig The significant digits of the number as a string of decimal digits.
		 * @return This object.
		 */
		public function setTo(sign:int = 0, exp:int = 0, dig:String = ''):SimpleBigDecimal {
			this.sign = sign;
			this.exp = exp;
			this.dig = dig;
			return this;
		}

		/**
		 * Copies the object's properties from another object.
		 * Returns this object.
		 *
		 * @param srcDec A source object to copy from.
		 * @return This object.
		 */
		public function copyFrom(srcDec:SimpleBigDecimal):SimpleBigDecimal {
			sign = srcDec.sign;
			exp = srcDec.exp;
			dig = srcDec.dig;
			return this;
		}

		/**
		 * Returns a new SimpleBigDecimal that is identical to this one.
		 *
		 * @return A copy of this SimpleBigDecimal.
		 */
		public function clone():SimpleBigDecimal {
			return new SimpleBigDecimal(sign, exp, dig);
		}

		public function toString():String {
			if (exp == EXPONENT_INFINITY)
				return _toStringInfNaN();
			return ((sign < 0) ? '-' : '') + (dig ? (dig.charAt(0) + ((dig.length > 1) ? ('.' + dig.substr(1)) : '')) : '0') + 'e' + ((exp > 0) ? '+' : '') + exp;
		}

		/**
		 * Returns a string representation of the number in exponential notation.
		 * The string will contain one non-zero digit before the decimal point and the specified number of digits after the decimal point.
		 * Trailing zeros are added if necessary.
		 * If fractionDigits is 0, the decimal point is not included.
		 * If rounding is necessary, the half-away from zero rounding method is used.
		 *
		 * @param fractionDigits The desired number of digits after the decimal point.
		 * @param trailingZeros Whether to include trailing zeros. If false, insignificant trailing zeros are not included, and if there are no digits after the decimal point, it is not included either.
		 * @return The string representation of the number in exponential notation.
		 *
		 * @throws RangeError - if fractionDigits is negative.
		 */
		public function toExponential(fractionDigits:int = 6, trailingZeros:Boolean = true):String {
			if (fractionDigits < 0)
				throw new RangeError('The fractionDigits argument (' + fractionDigits + ') must be a non-negative number.');
			if (exp == EXPONENT_INFINITY)
				return _toStringInfNaN();

			// One digit before the decimal point and fractionDigits digits after the decimal point.
			return copyAndRound(fractionDigits + 1)._toExponential(fractionDigits, trailingZeros);
		}

		razor_internal function _toExponential(fractionDigits:int, trailingZeros:Boolean):String {
			var str:String = dig.charAt(0);
			if (fractionDigits)
				str += trailingZeros ? ('.' + StrUtil.rpad(dig.substr(1, fractionDigits), '0', fractionDigits)) : StrUtil.rtrim0_dp('.' + dig.substr(1, fractionDigits));
			return ((sign < 0) ? '-' : '') + str + 'e' + ((exp >= 0) ? '+' : '') + exp;
		}

		/**
		 * Returns a string representation of the number in fixed-point (non-exponential) notation.
		 * The string will contain the specified number of digits after the decimal point.
		 * Trailing zeros are added if necessary.
		 * If fractionDigits is 0, the decimal point is not included.
		 * If rounding is necessary, the half-away from zero rounding method is used.
		 *
		 * @param fractionDigits The desired number of digits after the decimal point.
		 * @param trailingZeros Whether to include trailing zeros. If false, insignificant trailing zeros are not included, and if there are no digits after the decimal point, it is not included either.
		 * @return The string representation of the number in fixed-point notation.
		 *
		 * @throws RangeError - if fractionDigits is negative.
		 */
		public function toFixed(fractionDigits:int = 6, trailingZeros:Boolean = true):String {
			if (fractionDigits < 0)
				throw new RangeError('The fractionDigits argument (' + fractionDigits + ') must be a non-negative number.');
			if (exp == EXPONENT_INFINITY)
				return _toStringInfNaN();

			// If the exponent is negative, there are -(exp + 1) leading zeros after the decimal point, each zero is a fractional digit but not a significant digit. Keep (exp + 1) less significant digits.
			// If the exponent is non-negative, there are (exp + 1) digits before the decimal point, each digit is a significant digit but not a fractional digit. Keep (exp + 1) more significant digits.
			var precision:int = fractionDigits + exp + 1;
			var bigDec:SimpleBigDecimal = (precision >= 0) ? copyAndRound(precision) : (sign > 0) ? ZERO : NEG_ZERO;
			return bigDec._toFixed(fractionDigits, trailingZeros);
		}

		razor_internal function _toFixed(fractionDigits:int, trailingZeros:Boolean):String {
			var str:String;
			var numDigits:int = dig.length;
			if (exp < 0) {
				// All digits are after the decimal point.
				str = '0';
				if (fractionDigits) {
					var numZeros:int = -(exp + 1);
					if (trailingZeros)
						str += '.' + ((numZeros < fractionDigits) ? StrUtil.rpad(StrUtil.repeat('0', numZeros) + dig.substr(0, fractionDigits - numZeros), '0', fractionDigits) : StrUtil.repeat('0', fractionDigits));
					else if (numZeros < fractionDigits)
						str += StrUtil.rtrim0_dp('.' + StrUtil.repeat('0', numZeros) + dig.substr(0, fractionDigits - numZeros));
				}
			}
			else if ((exp + 1) >= numDigits) {
				// All digits are before the decimal point.
				str = dig;
				numZeros = exp + 1 - numDigits;
				if (numZeros)
					str += StrUtil.repeat('0', numZeros);
				if (fractionDigits && trailingZeros)
					str += '.' + StrUtil.repeat('0', fractionDigits);
			}
			else {
				// Some digits are before and some are after the decimal point.
				var dpIndex:int = 1 + exp;
				str = dig.substr(0, dpIndex);
				if (fractionDigits)
					str += trailingZeros ? ('.' + StrUtil.rpad(dig.substr(dpIndex, fractionDigits), '0', fractionDigits)) : StrUtil.rtrim0_dp('.' + dig.substr(dpIndex, fractionDigits));
			}
			return ((sign < 0) ? '-' : '') + str;
		}

		/**
		 * Returns a string representation of the number in either fixed-point or exponential notation, whichever is more appropriate for it's magnitude.
		 * If the number's decimal exponent exp is in the range (fixedMinExponent <= exp < precision), the fixed-point notation is used, otherwise exponential notation is used.
		 * The string will contain the specified number of significant digits.
		 * In both cases insignificant trailing zeros after the decimal point are not included.
		 * If there are no digits after the decimal point, the decimal point is not included.
		 * If precision is 0, precision is set to 1.
		 * If rounding is necessary, the half-away from zero rounding method is used.
		 *
		 * @param precision The desired number of significant digits. If precision is 0, precision is set to 1.
		 * @param trailingZeros Whether to include trailing zeros. If false, insignificant trailing zeros are not included, and if there are no digits after the decimal point, it is not included either.
		 * @param fixedMinExponent The low endpoint of the decimal exponent's range where the fixed-point notation is used.
		 * @return The string representation of the number in general format.
		 *
		 * @throws RangeError - if precision is negative.
		 */
		public function toGeneral(precision:int = 6, trailingZeros:Boolean = false, fixedMinExponent:int = -4):String {
			if (precision < 0)
				throw new RangeError('The precision argument (' + precision + ') must be a non-negative number.');
			if (exp == EXPONENT_INFINITY)
				return _toStringInfNaN();

			if (precision == 0)
				precision = 1;
			var bigDec:SimpleBigDecimal = copyAndRound(precision);
			return ((exp >= fixedMinExponent) && (exp < precision)) ? bigDec._toFixed(Math.max(0, precision - exp - 1), trailingZeros) : bigDec._toExponential(precision - 1, trailingZeros);
		}

		protected function _toStringInfNaN():String {
			return dig ? ('NaN(' + dig + ')') : ((sign < 0) ? '-Infinity' : 'Infinity');
		}

		protected static var copyDec:SimpleBigDecimal;
		protected function copyAndRound(precision:int):SimpleBigDecimal {
			if (copyDec)
				copyDec.setTo(sign, exp, dig);
			else
				copyDec = new SimpleBigDecimal(sign, exp, dig);

			return copyDec.roundToPrecision(precision);
		}
	}
}
