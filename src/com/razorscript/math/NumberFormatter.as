package com.razorscript.math {
	import com.razorscript.razor_internal;
	import com.razorscript.math.supportClasses.SimpleBigDecimal;
	import com.razorscript.utils.StrUtil;

	use namespace razor_internal;

	/**
	 * Formats numeric values with a strict upper limit on the total number of digits (maxDigits).
	 * That limit applies only to the number's significant digits, as well as leading or trailing zeros, and not to the sign, decimal point, digit grouping commas and the decimal exponent.
	 * Useful for implementing a UI component similar to a scientific calculator's numeric display, which has a fixed number of indicators for digits, and separate indicators for sign, decimal point, commas and exponent.
	 *
	 * @author Gene Pavlovsky
	 */
	public class NumberFormatter {
		/**
		 * Returns a new normal number formatter with the specified properties.
		 *
		 * Inside the range (normalLower <= |x| < normalUpper), formats numbers in fixed-point (non-exponential) format, with the number of fractional digits determined by maxDigits.
		 * Outside the range, formats numbers in exponential format, with precision equal to maxDigits.
		 * The effective left endpoint of the range is determined by normalLower and maxDigits.
		 * The right endpoint of the range is determined by maxDigits.
		 *
		 * If normalLower is a negative value, the effective value used is based on the maxDigits property.
		 * If normalLower is 0, the effective value used is 0.
		 * If normalLower is a positive value smaller than can be represented in non-exponential format using maxDigits digits, the effective value used is based on the maxDigits property.
		 *
		 * The string will contain at most maxDigits digits.
		 * Insignificant trailing zeros after the decimal point are not included.
		 * If there are no digits after the decimal point, the decimal point is not included.
		 * If rounding is necessary, the half-away from zero rounding method is used.
		 *
		 * @param maxDigits The maximum number of digits.
		 * @param normalLower The left endpoint of the range of numbers to format in non-exponential format.
		 * @return A new NumberFormatter.
		 *
		 * @throws RangeError - if maxDigits is not positive.
		 */
		public static function createNormalNumberFormatter(maxDigits:int, normalLower:Number = -1):NumberFormatter {
			var nf:NumberFormatter = new NumberFormatter(maxDigits);
			nf.normalLower = normalLower;
			nf.trailingZeros = false;
			return nf;
		}

		/**
		 * Returns a new fixed-point number formatter with the specified properties.
		 *
		 * Inside the range (normalLower <= |x| < normalUpper), formats numbers in fixed-point (non-exponential) format, with the number of fractional digits determined by fractionDigits and maxDigits.
		 * Outside the range, formats numbers in exponential format, with precision equal to maxDigits.
		 * The left endpoint of the range is 0.
		 * The right endpoint of the range is determined by maxDigits.
		 *
		 * The string will contain at most fractionDigits digits after the decimal point.
		 * Trailing zeros are added if necessary.
		 * If fractionDigits is 0, the decimal point is not included.
		 * If rounding is necessary, the half-away from zero rounding method is used.
		 *
		 * @param maxDigits The maximum number of digits.
		 * @param fractionDigits The maximum number of fractional digits.
		 * @param normalUpper The right endpoint of the range of numbers to format in non-exponential format.
		 * @return A new NumberFormatter.
		 *
		 * @throws RangeError - if maxDigits is not positive.
		 * @throws RangeError - if fractionDigits is negative.
		 */
		public static function createFixedNumberFormatter(maxDigits:int, fractionDigits:int):NumberFormatter {
			var nf:NumberFormatter = new NumberFormatter(maxDigits, fractionDigits);
			nf.normalLower = 0;
			nf.trailingZeros = true;
			return nf;
		}

		/**
		 * Returns a new scientific notation number formatter.
		 *
		 * Formats numbers in exponential format with the coefficient range of [1; 10).
		 * The string will contain one non-zero digit before the decimal point and (maxDigits - 1) digits after the decimal point.
		 * Trailing zeros are added if necessary.
		 * If fractionDigits is 0, the decimal point is not included.
		 * If rounding is necessary, the half-away from zero rounding method is used.
		 *
		 * @param maxDigits The maximum number of significant digits.
		 * @return A new NumberFormatter.
		 *
		 * @throws RangeError - if maxDigits is not positive.
		 */
		public static function createScientificNumberFormatter(maxDigits:int):NumberFormatter {
			var nf:NumberFormatter = new NumberFormatter(maxDigits);
			nf.trailingZeros = true;
			nf.scientificNotation = true;
			return nf;
		}

		/**
		 * Returns a new engineering notation number formatter.
		 *
		 * Formats numbers in exponential format with the coefficient range of [1; 1000) and the exponent divisible by 3.
		 * The string will contain at most maxDigits digits.
		 * Insignificant trailing zeros after the decimal point are not included.
		 * If there are no digits after the decimal point, the decimal point is not included.
		 * If rounding is necessary, the half-away from zero rounding method is used.
		 *
		 * @param maxDigits The maximum number of significant digits.
		 * @return A new NumberFormatter.
		 *
		 * @throws RangeError - if maxDigits is not positive.
		 */
		public static function createEngineeringNumberFormatter(maxDigits:int):NumberFormatter {
			var nf:NumberFormatter = new NumberFormatter(maxDigits);
			nf.trailingZeros = false;
			nf.engineeringNotation = true;
			return nf;
		}

		public function NumberFormatter(maxDigits:int = MathExt.DOUBLE_UNIQ_DIGITS, fractionDigits:int = MathExt.DOUBLE_UNIQ_DIGITS) {
			this.maxDigits = maxDigits;
			this.fractionDigits = fractionDigits;
		}

		protected var _maxDigits:int;
		/**
		 * The maximum number of digits.
		 *
		 * @throws RangeError - if value is not positive.
		 */
		public function get maxDigits():int {
			return _maxDigits;
		}

		public function set maxDigits(value:int):void {
			if (value < 1)
				throw new RangeError('The maxDigits argument (' + value + ') must be a positive number.');
			_maxDigits = value;
			_normalMinLower = Math.pow(10, (_leadingZero ? 1 : 0) - value);
			_normalMaxUpper = Math.pow(10, value);
		}

		protected var _fractionDigits:int;
		/**
		 * The number of fractional digits.
		 *
		 * @throws RangeError - if value is negative.
		 */
		public function get fractionDigits():int {
			return _fractionDigits;
		}

		public function set fractionDigits(value:int):void {
			if (value > MathExt.DOUBLE_UNIQ_DIGITS)
				throw new RangeError('The fractionDigits argument (' + value + ') must be a non-negative number.');
			_fractionDigits = value;
		}

		/** The smallest number that can be represented in non-exponential format using no more than maxDigits digits. */
		protected var _normalMinLower:Number;
		protected var _normalLower:Number = -1;
		/**
		 * If the scientificNotation property is false, determines the range of numbers to format in non-exponential format.
		 * Inside the range (normalLower <= |x| < normalUpper), format numbers in non-exponential format.
		 * Outside the range, format numbers in exponential format.
		 *
		 * If set to a negative value, the effective value used is based on the maxDigits property.
		 * If set to 0, the effective value used is 0.
		 * If set to a positive value smaller than can be represented in non-exponential format using maxDigits digits, the effective value used is based on the maxDigits property.
		 */
		public function get normalLower():Number {
			return (_normalLower > 0) ? Math.max(_normalLower, _normalMinLower) : (_normalLower < 0) ? _normalMinLower : 0;
		}

		public function set normalLower(value:Number):void {
			_normalLower = value;
		}

		/**
		 * The requested value of normalLower.
		 */
		public function get normalExplicitLower():Number {
			return _normalLower;
		}

		/** The smallest number that can't be represented in non-exponential format using no more than maxDigits digits. */
		protected var _normalMaxUpper:Number;
		protected var _normalUpper:Number = -1;
		/**
		 * If the scientificNotation property is false, determines the range of numbers to format in non-exponential format.
		 * Inside the range (normalLower <= |x| < normalUpper), format numbers in non-exponential format.
		 * Outside the range, format numbers in exponential format.
		 *
		 * If set to a negative value, the effective value used is based on the maxDigits property.
		 * If set to a value larger than can be represented in non-exponential format using maxDigits digits, the effective value used is based on the maxDigits property.
		 */
		public function get normalUpper():Number {
			return (_normalUpper > 0) ? Math.min(_normalUpper, _normalMaxUpper) : _normalMaxUpper;
		}

		public function set normalUpper(value:Number):void {
			_normalUpper = value;
		}

		/**
		 * The requested value of normalUpper.
		 */
		public function get normalExplicitUpper():Number {
			return _normalUpper;
		}

		protected var _leadingZero:Boolean = true;
		/**
		 * Whether a leading zero is included in a formatted number when there are no integer digits before the decimal separator.
		 */
		public function get leadingZero():Boolean {
			return _leadingZero;
		}

		public function set leadingZero(value:Boolean):void {
			_leadingZero = value;
			_normalMinLower = Math.pow(10, (_leadingZero ? 1 : 0) - _maxDigits);
		}

		/**
		 * Whether insignificant trailing zeros are included in a formatted number.
		 * If true, trailing zeros after the decimal point are included up to the limit determined by fractionDigits and maxDigits properties.
		 * If false, insignificant trailing zeros after the decimal point are not included. If there are no digits after the decimal point, the decimal point is not included.
		 */
		public var trailingZeros:Boolean;

		/**
		 * If set to a non-empty string, defines the character that should be used for the sign if the number is non-negative.
		 * If set to an empty string, the sign is used only for negative numbers.
		 */
		public var numberSign:String = '';

		/**
		 * If set to a non-empty string, defines the character that should be used for the exponent's sign if the exponent is non-negative.
		 * If set to an empty string, the sign is used only for negative exponents.
		 */
		public var exponentSign:String = '+';

		/**
		 * The minimum number of characters to print for the exponent digits. The exponent is left-padded with zeros if necessary.
		 * Set this property to 0 to print the exponent as is.
		 */
		public var exponentWidth:int;

		/**
		 * Whether thousands should be grouped using comma as the digit grouping separator.
		 * If true, thousands are grouped.
		 */
		public var groupDigits:Boolean;

		/**
		 * Whether numbers which are nearly half-way between two numbers differing only in the last digit, should be rounded to exactly half-way, if there's no other representable number closer to being half-way.
		 * If true, numbers are examined and rounded if necessary. Subnormal numbers are not examined or rounded.
		 */
		public var roundNearlyEquidistant:Boolean = true;

		protected var _scientificNotation:Boolean;
		/**
		 * Specifies whether to use the scientific notation, with the coefficient range of [1; 10).
		 */
		public function get scientificNotation():Boolean {
			return _scientificNotation;
		}
		public function set scientificNotation(value:Boolean):void {
			_scientificNotation = value;
			if (!value && _engineeringNotation)
				_engineeringNotation = false;
		}

		protected var _engineeringNotation:Boolean;
		/**
		 * Specifies whether to use the engineering notation. Implies scientific notation.
		 * If true, the exponent is chosen to be divisible by 3, with the coefficient range of [1; 1000).
		 * Coefficient range can be changed using engineeringOffset property.
		 */
		public function get engineeringNotation():Boolean {
			return _engineeringNotation;
		}

		public function set engineeringNotation(value:Boolean):void {
			_engineeringNotation = value;
			if (value && !_scientificNotation)
				_scientificNotation = true;
		}

		/**
		 * When using the engineering notation, shifts the coefficient range to [10 ^ engineeringOffset; 10 ^ (engineeringOffset + 3)), adjusting the exponent accordingly.
		 * If set to a positive or negative number, the decimal point is moved (3 * engineeringOffset) positions respectively right or left.
		 * Set this property to 0 to use the standard engineering notation with the coefficient range of [1; 1000).
		 * Example: engineeringOffset=1, format(5e3) returns 5000e0.
		 */
		public var engineeringOffset:int;

		/**
		 * The engineering notation number formatter used by reformatEngineering().
		 */
		public var engineeringReformatter:NumberFormatter;

		/**
		 * Formats a numeric value.
		 *
		 * @param num A Number.
		 * @return The formatted string.
		 */
		public function format(num:Number):String {
			// Save x as the last formatted number.
			lastValue = num;
			// If x is NaN or Infinity, just use toString().
			if (MathExt.isNaN(num) || !isFinite(num))
				return num.toString();

			var useExponential:Boolean = _scientificNotation;
			if (!useExponential && num) {
				var absNum:Number = Math.abs(num);
				useExponential = (absNum < normalLower) || (absNum >= normalUpper);
			}

			wasLastEngineering = _engineeringNotation;
			if (_engineeringNotation)
				engOffset = engineeringOffset;

			convertToBigDecimal(num);
			return _engineeringNotation ? formatEngineering() : useExponential ? formatScientific() : formatFixed();
		}

		/**
		 * Reformats the result of the last format() call in engineering notation.
		 * If this number formatter is not an engineering notation number formatter, the engineeringReformatter property must be set to an engineering notation number formatter, which is used for reformatting.
		 *
		 * @param incOffset If the last formatted (or reformatted) number was in engineering notation, add incOffset to engOffset before reformatting.
		 * @return The formatted string.
		 */
		public function reformatEngineering(incOffset:int):String {
			// If lastValue is NaN or Infinity, just use toString().
			if (MathExt.isNaN(lastValue) || !isFinite(lastValue))
				return lastValue.toString();
			if (!bigDec)
				return '';

			var nf:NumberFormatter = _engineeringNotation ? this : engineeringReformatter;
			if (wasLastEngineering) {
				nf.engOffset += incOffset;
			}
			else {
				wasLastEngineering = true;
				nf.bigDec = bigDec;
				nf.engOffset = nf.engineeringOffset;
			}
			return nf.formatEngineering();
		}

		/** The last number formatted using format(). Used by reformatEngineering(). */
		protected var lastValue:Number = 0;
		/** The last finite number formatted using format(), converted to a SimpleBigDecimal and rounded to a precision specified by this number formatter. */
		protected var bigDec:SimpleBigDecimal;
		/** Whether the last formatted number was formatted or reformatted in engineering notation. */
		protected var wasLastEngineering:Boolean;
		/** The actual engineering offset to use. When formatting a number in engineering notation, engineeringOffset is used, subsequent calls to reformatEngineering() increment or decrement the offset. */
		protected var engOffset:int;

		protected static const TBD_PRINT_DIG:int = 24;
		protected static const TBD_ACC_DIG:int = MathExt.DOUBLE_ACC_DIGITS; // 15.
		protected static const TBD_DIFF_DIG:int = TBD_PRINT_DIG - TBD_ACC_DIG; // 9.
		protected static const TBD_DIFF_BORROW:int = Math.pow(10, TBD_DIFF_DIG); // 1,000,000,000.

		/**
		 * Converts a number to a SimpleBigDecimal with at least (maxDigits + 1) significant digits and no rounding.
		 *
		 * If roundNearlyEquidistant property is true, and the number is nearly half-way between two numbers differing only in the last digit, the number is examined to determine if it should be rounded to exactly half-way.
		 * If there's no other representable number closer to being half-way, the number is rounded to exactly half-way. Subnormal numbers are not examined or rounded.
		 * Many decimal numbers can't be exactly represented as a double, so the nearest representable double is used.
		 * If a decimal number ends with '5', the double representing that number might end with a '4', followed by one or more '9's and then other digits.
		 * For example, 6.5e+300, stored in a double and printed to precision 21, is 6.49999999999999944907e+300.
		 * If formatting 6.5e+300 in scientific notation with precision 1, the correct result is 7e+300, but without double rounding the result would be 6e+300.
		 * If 6.49999999999999944907e+300 is first rounded to 15 digits, it becomes 6.5e+300 again, which is then rounded to the correct result, 7e+300.
		 * Due to wobble, determining whether the number should be rounded is tricky.
		 * For example, 1.5e+54, stored in a double and printed to precision 21, is 1.49999999999999994730e+54.
		 * Rounded to 15 or 16 digits, it becomes 1.5e+54 again, however so does the previous representable double, 1.49999999999999977715e+54, which doesn't actually represent 1.5e+54.
		 * This method uses the following algorithm:
		 *		Print the number to at least 24 significant digits and no rounding (equivalent to a precision of at least 21).
		 *		If the numbers first 15 digits end with a '4' and one or more '9's, or the 15th digit is '4' and the 16th digit is '9',
		 *			Get the next representable double and print it to at least 24 significant digits and no rounding.
		 *			If this number's absolute distance from the half-way number is less than or equal to the next representable double's,
		 *				Replace the previously matched digit sequence (the one that starts with a '4') with the digit '5'.
		 *
		 * @see com.razorscript.math.MathExt#DOUBLE_ACC_DIGITS
		 * @see com.razorscript.math.MathExt#DOUBLE_UNIQ_DIGITS
		 *
		 * @param x A number.
		 * @return The number converted to SimpleBigDecimal.
		 */
		protected function convertToBigDecimal(num:Number):void {
			initDouble(num);

			var bigDec:SimpleBigDecimal;
			if (double.isZero) {
				// The number is ±0.
				bigDec = (double.sign > 0) ? SimpleBigDecimal.ZERO : SimpleBigDecimal.NEG_ZERO;
			}
			else if (!roundNearlyEquidistant || double.isSubnormal) {
				// Either roundNearlyEquidistant property is false, or the number is subnormal.
				bigDec = double._toBigDecimal(_maxDigits + 1);
			}
			else {
				bigDec = double._toBigDecimal(Math.max(_maxDigits + 1, TBD_PRINT_DIG));
				var dig:String = bigDec.dig;

				var indexOf4:int;
				var numDigits:int = bigDec.dig.length;
				if ((numDigits > TBD_ACC_DIG) && (dig.substr(TBD_ACC_DIG - 1, 2) == '49')) {
					// The 15th digit is '4' and the 16th digit is '9'.
					indexOf4 = TBD_ACC_DIG - 1;
				}
				else if (numDigits >= TBD_ACC_DIG) {
					// Number has at least 15 significant digits.
					var i:int = TBD_ACC_DIG - 1;
					if (dig.charAt(i) == '9') {
						// The number ends with a '9', skip through any preceding '9's.
						while ((--i > 1) && (dig.charAt(i) == '9'))
							;
						// Check if the digit before the '9's is a '4'.
						if (dig.charAt(i) == '4')
							indexOf4 = i;
					}
				}

				if (indexOf4) {
					// The number is nearly half-way between two numbers which have (indexOf4 - 1) significant digits and differ only in the last digit.
					bigDec = double.advance((num < 0) ? -1 : 1)._toBigDecimal(TBD_PRINT_DIG);
					var digNext:String = bigDec.dig;
					if (digNext.charAt(indexOf4) == '5') {
						// The next representable double is on the other side of the half-way number, and might be further away from the half-way number than this number.
						// Calculate the floor of the absolute distance between this number and the half-way number (subtract 1 to get the floor).
						var distThis:int = TBD_DIFF_BORROW - parseInt(StrUtil.rpad(dig.substr(TBD_ACC_DIG, TBD_DIFF_DIG), '0', TBD_DIFF_DIG)) - 1;
						// Calculate the ceiling of the absolute distance between the next representable double and the half-way number (add 1 to get the ceiling).
						var distNext:int = parseInt(StrUtil.rpad(digNext.substr(TBD_ACC_DIG, TBD_DIFF_DIG), '0', TBD_DIFF_DIG)) + 1;
						// If this number's absolute distance from the half-way number is less than or equal to the next representable double's, round this number to the half-way number.
						if (distThis <= distNext)
							dig = dig.substr(0, indexOf4) + '5';
					}
					bigDec.dig = dig;
				}
			}
			setBigDec(bigDec);
		}

		protected function setBigDec(srcDec:SimpleBigDecimal):void {
			if (bigDec)
				bigDec.setTo(srcDec.sign, srcDec.exp, srcDec.dig);
			else
				bigDec = srcDec.clone();
		}

		protected static var double:Double;
		protected static function initDouble(num:Number):void {
			if (double)
				double.number = num;
			else
				double = new Double(num);
		}

		protected function formatScientific():String {
			var exp:int = bigDec.exp;
			bigDec.roundToPrecision(_maxDigits);
			if (bigDec.exp > exp) {
				// Rounding the number has incremented it's exponent. If the number is no longer in the range of numbers to format in exponential format, use non-exponential format.
				exp = bigDec.exp;
				var mag:Number = Math.pow(10, exp);
				if (!_scientificNotation && (mag >= normalLower) && (mag < normalUpper))
					return formatFixed();
			}
			var dig:String = bigDec.dig;

			var str:String = dig.charAt(0);
			var fractionDigits:int = _maxDigits - 1;
			if (fractionDigits)
				str += trailingZeros ? ('.' + StrUtil.rpad(dig.substr(1, fractionDigits), '0', fractionDigits)) : StrUtil.rtrim0_dp('.' + dig.substr(1, fractionDigits));
			return ((bigDec.sign < 0) ? '-' : numberSign) + str + 'e' + _formatExp(exp);
		}

		protected function formatEngineering():String {
			if (bigDec.isZero) {
				engOffset = 0;
				return ((bigDec.sign < 0) ? '-' : numberSign) + '0' + 'e' + _formatExp(0);
			}

			// Copy bigDec and round to the maxDigits precision.
			var copyDec:SimpleBigDecimal = copyAndRound(bigDec, _maxDigits);
			var exp:int = copyDec.exp;

			// Calculate the amount by which to decrease the exponent to round it down to the nearest multiple of 3, which would shift the coefficient range to [1; 1000).
			var expShift:int = MathExt.mod(exp, 3);
			// Calculate the valid range for engineering offset and clamp engOffset into this range.
			if (engOffset > 0)
				engOffset = Math.min(engOffset, Math.floor((_maxDigits - 1 - expShift) / 3));
			else if (engOffset < 0)
				engOffset = Math.max(engOffset, Math.ceil(-(_maxDigits - (_leadingZero ? 1 : 0) + expShift + ((copyDec.dig.substr(0, 4) >= '9995') ? 1 : 0)) / 3)); // ((copyDec.dig.substr(0, 1) >= '5') ? 1 : 0)) / 3));
			// Shift the coefficient range to [10 ^ engOffset; 10 ^ (engOffset + 3)) and adjust the exponent accordingly.
			expShift += 3 * engOffset;

			// Calculate the number of fractional digits, the position of the decimal point, and the required precision.
			var fractionDigits:int = Math.min(_maxDigits, _maxDigits - ((expShift < 0) ? (_leadingZero ? 1 : 0) : (expShift + 1)));
			var dpIndex:int = 1 + expShift;
			var precision:int = fractionDigits + dpIndex;
			if (precision < _maxDigits) {
				// All digits are after the decimal point, and there are enough leading zeros after the decimal point that one or more least significant digits have to be discarded.
				// Copy bigDec and round to the decreased precision.
				copyDec = copyAndRound(bigDec, precision);
				if (copyDec.exp > exp) {
					// Rounding the number has incremented it's exponent. Move the decimal point one position to the right.
					++dpIndex;
				}
			}
			var str:String = _formatFixed(copyDec.dig, dpIndex, fractionDigits);
			return ((copyDec.sign < 0) ? '-' : numberSign) + str + 'e' + _formatExp(exp - expShift);
		}

		protected function _formatExp(exp:int):String {
			return ((exp < 0) ? '-' : exponentSign) + (exponentWidth ? StrUtil.lpad(Math.abs(exp).toString(), '0', exponentWidth) : Math.abs(exp));
		}

		protected function formatFixed():String {
			var exp:int = bigDec.exp;
			// If the exponent is negative, there is either a digit "0" or nothing before the decimal point, the maximum number of fractional digits is respectively (maxDigits - 1) or maxDigits.
			// If the exponent is non-negative, there are (exp + 1) digits before the decimal point, the maximum number of fractional digits is (maxDigits - (exp + 1)).
			var fractionDigits:int = Math.min(_fractionDigits, _maxDigits - ((exp < 0) ? (_leadingZero ? 1 : 0) : (exp + 1)));
			// If the exponent is negative, there are -(exp + 1) leading zeros after the decimal point, each zero is a fractional digit but not a significant digit. Decrease precision by (exp + 1) digits.
			// If the exponent is non-negative, there are (exp + 1) digits before the decimal point, each digit is a significant digit but not a fractional digit. Increase precision by (exp + 1) digits, but not over maxDigits.
			var precision:int = Math.min(fractionDigits + exp + 1, _maxDigits);
			if (precision >= 0) {
				bigDec.roundToPrecision(precision);
				if (bigDec.exp > exp) {
					// Rounding the number has incremented it's exponent. If the number is no longer in the range of numbers to format in non-exponential format, use exponential format, otherwise recalculate fractionDigits.
					exp = bigDec.exp;
					if (Math.pow(10, exp) >= normalUpper)
						return formatScientific();
					fractionDigits = Math.min(_fractionDigits, _maxDigits - ((exp < 0) ? (_leadingZero ? 1 : 0) : (exp + 1)));
				}
			}
			else {
				bigDec.setTo(bigDec.sign, 0, '0');
			}
			var str:String = _formatFixed(bigDec.dig, 1 + exp, fractionDigits);
			return ((bigDec.sign < 0) ? '-' : numberSign) + str;
		}

		protected function _formatFixed(dig:String, dpIndex:int, fractionDigits:int):String {
			var numDigits:int = dig.length;
			var str:String;
			if (dpIndex <= 0) {
				// All digits are after the decimal point.
				str = _leadingZero ? '0' : '';
				if (fractionDigits) {
					var numZeros:int = -dpIndex;
					if (trailingZeros)
						str += '.' + ((numZeros < fractionDigits) ? StrUtil.rpad(StrUtil.repeat('0', numZeros) + dig.substr(0, fractionDigits - numZeros), '0', fractionDigits) : StrUtil.repeat('0', fractionDigits));
					else if (numZeros < fractionDigits)
						str += StrUtil.rtrim0_dp('.' + StrUtil.repeat('0', numZeros) + dig.substr(0, fractionDigits - numZeros));
				}
				if (!str)
					str = '0';
			}
			else if (dpIndex >= numDigits) {
				// All digits are before the decimal point.
				str = dig;
				numZeros = dpIndex - numDigits;
				if (numZeros)
					str += StrUtil.repeat('0', numZeros);
				if (groupDigits)
					str = _groupDigits(str);
				if (fractionDigits && trailingZeros)
					str += '.' + StrUtil.repeat('0', fractionDigits);
			}
			else {
				// Some digits are before and some are after the decimal point.
				str = groupDigits ? _groupDigits(dig.substr(0, dpIndex)) : dig.substr(0, dpIndex);
				if (fractionDigits)
					str += trailingZeros ? ('.' + StrUtil.rpad(dig.substr(dpIndex, fractionDigits), '0', fractionDigits)) : StrUtil.rtrim0_dp('.' + dig.substr(dpIndex, fractionDigits));
			}
			return str;
		}

		/**
		 * Divides the string into 3-character substrings, then joins them together using comma "," as the group separator character. The first substring may contain less than 3 characters.
		 *
		 * @param str A string.
		 * @return The grouped string.
		 */
		protected function _groupDigits(str:String):String {
			return StrUtil.splitw(str, 3, true).join(',');
		}

		/** Used by formatEngineering() to store a copy of bigDec, rounded to a specified precision, while keeping bigDec's precision unchanged. */
		protected static var copyDec:SimpleBigDecimal;
		protected function copyAndRound(bigDec:SimpleBigDecimal, precision:int):SimpleBigDecimal {
			if (copyDec)
				copyDec.setTo(bigDec.sign, bigDec.exp, bigDec.dig);
			else
				copyDec = new SimpleBigDecimal(bigDec.sign, bigDec.exp, bigDec.dig);

			return copyDec.roundToPrecision(precision);
		}
	}
}
