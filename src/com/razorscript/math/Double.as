package com.razorscript.math {
	import com.razorscript.debug.Logger;
	import com.razorscript.math.supportClasses.SimpleBigDecimal;
	import com.razorscript.math.supportClasses.SimpleBigInt;
	import com.razorscript.razor_internal;
	import com.razorscript.utils.IntUtil;
	import com.razorscript.utils.StrUtil;
	import flash.utils.ByteArray;

	use namespace razor_internal;

	/**
	 * Like the built-in Number data type, represents an IEEE 754 double-precision floating-point number (binary64).
	 * Provides properties that allow inspection and manipulation of the sign, exponent and fraction (significand).
	 *
	 * @author Gene Pavlovsky
	 */
	public class Double {
		/** The minimum value of base-2 exponent for representable finite non-zero numbers. */
		public static const EXPONENT_MIN:int = -1022;
		/** The maximum value of base-2 exponent for representable finite non-zero numbers. */
		public static const EXPONENT_MAX:int = 1023;
		/** The value of base-2 exponent used to represent ±0 (if fraction == 0) and subnormals (if fraction != 0). */
		public static const EXPONENT_ZERO:int = EXPONENT_MIN - 1;
		/** The value of base-2 exponent used to represent ∞ (if fraction == 0) and NaNs (if fraction != 0). */
		public static const EXPONENT_INFINITY:int = EXPONENT_MAX + 1;

		/** The number +0. */
		public static const ZERO:Double = new Double(0);
		/** The number -0. */
		public static const NEG_ZERO:Double = new Double(-0);
		/** The smallest representable positive subnormal number (Number.MIN_VALUE), with an approximate value of 4.94065645841246544176e-324. */
		public static const MIN_VALUE:Double = new Double(MathExt.DOUBLE_MIN);
		/** The smallest representable positive normal number, with an approximate value of 2.22507385850720138309e-308. */
		public static const MIN_NORMAL:Double = new Double(MathExt.DOUBLE_MIN_NORMAL);
		/** The largest representable finite number (Number.MAX_VALUE), with an approximate value of 1.79769313486231570814e+308. */
		public static const MAX_VALUE:Double = new Double(MathExt.DOUBLE_MAX);
		/** The smallest representable finite number (-Number.MAX_VALUE), with an approximate value of -1.79769313486231570814e+308. */
		public static const NEG_MAX_VALUE:Double = new Double(MathExt.DOUBLE_NEG_MAX);
		/** The IEEE 754 value representing positive infinity. */
		public static const INFINITY:Double = new Double(Infinity);
		/** The IEEE 754 value representing negative infinity. */
		public static const NEG_INFINITY:Double = new Double(-Infinity);
		/** The IEEE 754 value representing NaN (not-a-number). */
		public static const NAN:Double = new Double(NaN);

		/**
		 * Determines whether two Doubles are equal.
		 * Unlike the built-in Number data type, two NaN values with the same payload are considered to be equal.
		 *
		 * @param x A Double.
		 * @param y A Double.
		 * @return Boolean true if this number is equal to the specified value, otherwise false.
		 */
		public static function equals(x:Double, y:Double):Boolean {
			return x.equals(y);
		}

		/**
		 * Returns the number of ULPs between two Doubles.
		 * ULP stands for "unit of least precision" or "unit in the last place" and is the spacing between floating-point numbers, i.e., the value the least significant digit represents if it is 1.
		 * The result is a signed integer value (stored in type Number) representing the number of distinct representations between x and y.
		 * If either number is a NaN, returns NaN.
		 * If one number is an infinity and the other number is finite, returns Infinity.
		 * If numbers are infinities of the same sign, returns NaN.
		 * If numbers are infinities of opposite sign, returns Infinity.
		 *
		 * @param x A Double.
		 * @param y A Double.
		 * @return The number of ULPs between the two numbers.
		 */
		public static function distance(x:Double, y:Double):Number {
			return x.distance(y);
		}

		/**
		 * Returns a new Double which is the nearest representable number greater than the specified value.
		 *
		 * @param x A Double.
		 * @return A new Double r such that distance(x, r) == 1.
		 */
		public static function next(x:Double):Double {
			return x.clone().advance(1);
		}

		/**
		 * Returns a new Double which is the nearest representable number less than the specified value.
		 *
		 * @param x A Double.
		 * @return A new Double r such that distance(x, r) == -1.
		 */
		public static function prior(x:Double):Double {
			return x.clone().advance(-1);
		}

		/**
		 * Returns a new Double whose distance from the specified Double is the specified number of ULPs.
		 *
		 * @param x A Double.
		 * @param distance A signed integer number of ULPs (stored in type Number). Must be not NaN and not Infinity.
		 * @return A new Double r such that distance(x, r) == distance.
		 */
		public static function advance(x:Double, distance:Number):Double {
			return x.clone().advance(distance);
		}

		/**
		 * Converts a string of binary digits in IEEE 754 double-precision floating-point format (binary64) to a floating-point number, and returns a new Double holding this number.
		 * Leading whitespace characters are not allowed, as are trailing non-numeric characters.
		 *
		 * @see com.razorscript.math.Double#bitString
		 *
		 * @param str The string to read and convert to a floating-point number. Must contain a string of binary digits.
		 * @return A new Double.
		 *
		 * @throws RangeError - if str is not a valid bitString.
		 */
		public static function fromBitString(str:String):Double {
			var double:Double = new Double();
			double.bitString = str;
			return double;
		}

		/**
		 * Converts a string of hexadecimal digits in IEEE 754 double-precision floating-point format (binary64) to a floating-point number, and returns a new Double holding this number.
		 * Leading whitespace characters are not allowed, as are trailing non-numeric characters.
		 *
		 * @see com.razorscript.math.Double#hexString
		 *
		 * @param str The string to read and convert to a floating-point number. Must contain a string of hexadecimal digits.
		 * @return A new Double.
		 *
		 * @throws RangeError - if str is not a valid hexString.
		 */
		public static function fromHexString(str:String):Double {
			var double:Double = new Double();
			double.hexString = str;
			return double;
		}

		/**
		 * Converts a hexfloat to a floating-point number, and returns a new Double holding this number.
		 * Leading whitespace characters are ignored, as are trailing non-numeric characters.
		 *
		 * @see com.razorscript.math.Double#hexFloat
		 *
		 * @param str The string to read and convert to a floating-point number. Must contain a hexfloat.
		 * @return A number or NaN.
		 *
		 * @throws RangeError - if value is not a valid hexfloat.
		 */
		public static function fromHexFloat(str:String):Double {
			var double:Double = new Double();
			double.hexFloat = str;
			return double;
		}

		/**
		 * Constructor.
		 *
		 * If a single numeric argument is passed, creates a Double holding the specified number.
		 * If a single string argument is passed, parses the string and creates a Double holding the parsed number.
		 * If a single Double argument is passed, creates a clone of the specified Double.
		 * If three arguments are passed, creates a Double with the specified sign, exponent and fraction (stored bits as a string of binary or hexadecimal digits).
		 * If no arguments are passed, creates a Double holding a NaN (not-a-number).
		 *
		 * @param sign The sign of the number (±1). Zeros are also signed (±0).
		 * @param exponent The base-2 exponent of the number.
		 * @param fraction The stored bits of the significand of the number as a string of binary or hexadecimal digits. Doesn't include the implicit leading bit.
		 */
		public function Double(... args) {
			if (!byteArray)
				byteArray = new ByteArray();

			if (args.length == 3) {
				setTo.apply(null, args);
			}
			else if (args.length == 1) {
				var value:* = args[0];
				if (value is Number)
					number = value;
				else if (value is String)
					parseDouble(value);
				else if (value is Double)
					copyFrom(value);
				else
					throw new ArgumentError('The argument (' + value + ') is not a Number, String or a Double.');
			}
			else {
				copyFrom(NAN);
			}
		}

		protected static var byteArray:ByteArray;

		protected var _number:Number;
		/**
		 * The primitive Number value of the object.
		 */
		public function get number():Number {
			if (invalidateNumberFlag)
				validateNumber();
			return _number;
		}

		public function set number(value:Number):void {
			// The equality operator works correctly for non-zero finite numbers and infinities, but can't distinguish two zeros of opposite sign, or two NaNs with different payloads.
			if (!invalidateNumberFlag && (_number == value) && (value != 0) && MathExt.isNotNaN(value))
				return; // Number is validated and equal to value, and value is not zero or NaN.
			_number = value;
			invalidateNumberFlag = false;
			readWordsFromNumber();
		}

		/**
		 * The sign of the number (±1). Zeros are also signed (±0).
		 *
		 * @throws RangeError - if value is not 1 or -1.
		 */
		public function get sign():int {
			return (w32 & SIGN_MASK) ? -1 : 1;
		}

		public function set sign(value:int):void {
			if ((value != 1) && (value != -1))
				throw new RangeError('Value (' + value + ') for the sign property is outside the set {-1, 1}.');
			if (sign == value)
				return;

			if (value == 1)
				w32 &= ~SIGN_MASK; // Positive, clear the sign bit.
			else
				w32 |= SIGN_MASK; // Negative, set the sign bit.

			_number = value * Math.abs(_number);
		}

		/**
		 * The base-2 exponent of the number.
		 *
		 * @throws RangeError - if value is outside the range [EXPONENT_ZERO, EXPONENT_INFINITY].
		 */
		public function get exponent():int {
			return ((w32 & EXP_MASK) >> EXP_SHIFT) - EXP_BIAS;
		}

		public function set exponent(value:int):void {
			if ((value < EXPONENT_ZERO) || (value > EXPONENT_INFINITY))
				throw new RangeError('Value (' + value + ') for the exponent property is outside the range [' + EXPONENT_ZERO + ', ' + EXPONENT_INFINITY + '].');
			if (exponent == value)
				return;

			// Clear the exponent bits.
			w32 &= ~EXP_MASK;
			// Copy the exponent bits.
			w32 |= ((value + EXP_BIAS) << EXP_SHIFT);

			invalidateNumberFlag = true;
		}

		/**
		 * The stored bits of the significand of the number as a string of binary digits.
		 * Doesn't include the implicit leading bit.
		 *
		 * @throws RangeError - if value's length is not FRACT_WIDTH.
		 * @throws RangeError - if value contains invalid digits.
		 */
		public function get fraction():String {
			return StrUtil.lpad((w32 & FRACT_MASK).toString(2), '0', FRACT_WIDTH - 32) + StrUtil.lpad(w0.toString(2), '0', 32);
		}

		public function set fraction(value:String):void {
			parseFractionDigits(value, 2);
		}

		/**
		 * The stored bits of the significand of the number as a string of hexadecimal digits.
		 * Doesn't include the implicit leading bit.
		 *
		 * @throws RangeError - if value's length is not FRACT_WIDTH / 4.
		 * @throws RangeError - if value contains invalid digits.
		 */
		public function get hexFraction():String {
			return StrUtil.lpad((w32 & FRACT_MASK).toString(16), '0', (FRACT_WIDTH - 32) >> 2) + StrUtil.lpad(w0.toString(16), '0', 8);
		}

		public function set hexFraction(value:String):void {
			parseFractionDigits(value, 16);
		}

		/** The base argument must be either 2 (binary) or 16 (hexadecimal). */
		protected function parseFractionDigits(value:String, base:int):void {
			var numDigits:int = (base == 2) ? FRACT_WIDTH : (FRACT_WIDTH >> 2);
			if (value.length != numDigits)
				throw new RangeError('Value (' + value + ') is not ' + numDigits + ' digits long (actual length: ' + value.length + ' digits).');

			numDigits -= (base == 2) ? 32 : 8;
			var bits0:Number = parseInt(value.substr(numDigits), base);
			var bits32:Number = parseInt(value.substr(0, numDigits), base);
			if ((bits0 != bits0) || (bits32 != bits32))
				throw new RangeError('Value (' + value + ') contains invalid digit(s) (only ' + ((base == 2) ? 'binary' : 'hexadecimal') + ' digits are allowed).');
			if ((bits0 == w0) && (bits32 == (w32 & FRACT_MASK)))
				return;
			w0 = bits0;
			// Clear the fraction bits.
			w32 &= ~FRACT_MASK;
			// Copy the fraction bits.
			w32 |= bits32;

			invalidateNumberFlag = true;
		}

		/**
		 * The most significant 20 bits of the significand of the number as an uint.
		 * Doesn't include the implicit leading bit.
		 */
		public function get fractionHigh():uint {
			return w32 & FRACT_MASK;
		}

		public function set fractionHigh(value:uint):void {
			value &= FRACT_MASK;
			if (value == (w32 & FRACT_MASK))
				return;
			// Clear the fraction bits.
			w32 &= ~FRACT_MASK;
			// Copy the fraction bits.
			w32 |= value;

			invalidateNumberFlag = true;
		}

		/**
		 * The least significant 32 bits of the significand of the number as an uint.
		 */
		public function get fractionLow():uint {
			return w0;
		}

		public function set fractionLow(value:uint):void {
			if (value == w0)
				return;
			w0 = value;
			invalidateNumberFlag = true;
		}

		/**
		 * The significand of the number as a string of binary digits.
		 * Includes the implicit leading bit (with the value of 1 for normal numbers, 0 for special datums).
		 *
		 * @throws RangeError - if value's first character, representing the implicit leading bit, is invalid.
		 */
		public function get significand():String {
			return (((exponent >= EXPONENT_MIN) && (exponent <= EXPONENT_MAX)) ? '1' : '0') + fraction;
		}

		public function set significand(value:String):void {
			if ((exponent >= EXPONENT_MIN) && (exponent <= EXPONENT_MAX) && (value.charAt(0) != '1'))
				throw new RangeError('Number is a normal number, but the leading bit of value (' + value + ') is not 1.');
			fraction = value.substr(1);
		}

		/**
		 * The significand of the number as a string of hexadecimal digits.
		 * Includes the implicit leading bit (with the value of 1 for normal numbers, 0 for special datums).
		 *
		 * @throws RangeError - if value's first character, representing the implicit leading bit, is invalid.
		 */
		public function get hexSignificand():String {
			return (((exponent >= EXPONENT_MIN) && (exponent <= EXPONENT_MAX)) ? '1' : '0') + hexFraction;
		}

		public function set hexSignificand(value:String):void {
			if ((exponent >= EXPONENT_MIN) && (exponent <= EXPONENT_MAX) && (value.charAt(0) != '1'))
				throw new RangeError('Number is a normal number, but the leading bit of value (' + value + ') is not 1.');
			hexFraction = value.substr(1);
		}

		/**
		 * The NaN payload of the number as a string of binary digits.
		 * Doesn't check to verify if the number is actually a NaN.
		 *
		 * @throws RangeError - if value's length is not NAN_PAYLOAD_WIDTH.
		 * @throws RangeError - if value contains invalid digits.
		 */
		public function get nanPayload():String {
			return StrUtil.lpad((w32 & NAN_PAYLOAD_MASK).toString(2), '0', NAN_PAYLOAD_WIDTH - 32) + StrUtil.lpad(w0.toString(2), '0', 32);
		}

		public function set nanPayload(value:String):void {
			fraction = ((w32 & QNAN_MASK) ? '1' : '0') + value;
		}

		/**
		 * The NaN payload of the number as a string of hexadecimal digits.
		 * Doesn't check to verify if the number is actually a NaN.
		 *
		 * @throws RangeError - if value's length is not FRACT_WIDTH / 4.
		 * @throws RangeError - if value contains invalid digits.
		 */
		public function get hexNanPayload():String {
			return StrUtil.lpad((w32 & NAN_PAYLOAD_MASK).toString(16), '0', (FRACT_WIDTH - 32) >> 2) + StrUtil.lpad(w0.toString(16), '0', 8);
		}

		public function set hexNanPayload(value:String):void {
			var qNaN:uint = w32 & QNAN_MASK;
			hexFraction = value;
			w32 &= ~QNAN_MASK;
			w32 |= qNaN;
		}

		/**
		 * The number as a string of binary digits in IEEE 754 double-precision floating-point format (binary64).
		 *
		 * @throws RangeError - if value's length is not 64.
		 * @throws RangeError - if value contains invalid digits.
		 */
		public function get bitString():String {
			return StrUtil.lpad(w32.toString(2), '0', 32) + StrUtil.lpad(w0.toString(2), '0', 32);
		}

		public function set bitString(value:String):void {
			parseDigits(value, 2);
		}

		/**
		 * The number as a string of hexadecimal digits in IEEE 754 double-precision floating-point format (binary64).
		 *
		 * @throws RangeError - if value's length is not 16.
		 * @throws RangeError - if value contains invalid digits.
		 */
		public function get hexString():String {
			return StrUtil.lpad(w32.toString(16), '0', 8) + StrUtil.lpad(w0.toString(16), '0', 8);
		}

		public function set hexString(value:String):void {
			parseDigits(value, 16);
		}

		/** The base argument must be either 2 (binary) or 16 (hexadecimal). */
		protected function parseDigits(value:String, base:int):void {
			var numDigits:int = (base == 2) ? 64 : 16;
			if (value.length != numDigits)
				throw new RangeError('Value (' + value + ') is not ' + numDigits + ' digits long (actual length: ' + value.length + ' digits).');

			numDigits >>= 1;
			var bits0:Number = parseInt(value.substr(numDigits), base);
			var bits32:Number = parseInt(value.substr(0, numDigits), base);
			if ((bits0 != bits0) || (bits32 != bits32))
				throw new RangeError('Value (' + value + ') contains invalid digit(s) (only ' + ((base == 2) ? 'binary' : 'hexadecimal') + ' digits are allowed).');
			if ((bits0 == w0) && (bits32 == w32))
				return;
			w0 = bits0;
			w32 = bits32;

			invalidateNumberFlag = true;
		}

		/**
		 * The number as a string in hexfloat format, in the form [-][01].hhhhhhhhhhhhhp[-+]dddd.
		 * The digit before the radix point is 0 if the number is subnormal, 1 otherwise.
		 * The stored bits of the significand of the number are formatted as a string of hexadecimal digits.
		 * Insignificant trailing zeros after the decimal point are not included.
		 * If there are no digits after the decimal point, the decimal point is not included.
		 * The base-2 exponent is formatted as a decimal number using at least one digit but at most as many digits as necessary to represent the value exactly.
		 *
		 * @see http://www.gnu.org/software/libc/manual/html_node/Floating_002dPoint-Conversions.html
		 *
		 * @throws RangeError - if value is not a valid hexfloat.
		 */
		public function get hexFloat():String {
			return toHexFloat(false);
		}

		protected static const HEX_FLOAT_RX:RegExp = /^ (-?) ([01]) (?: \. ([0-9a-f]*))? p ([-+] \d+) $/xi;

		public function set hexFloat(value:String):void {
			var rxMatch:Array = HEX_FLOAT_RX.exec(value);
			if (rxMatch) {
				sign == (rxMatch[1] == '-') ? -1 : 1;
				exponent = (rxMatch[2] == '0') ? EXPONENT_ZERO : parseInt(rxMatch[4]);
				if (rxMatch[3])
					hexFraction = StrUtil.rpad(rxMatch[3], '0', FRACT_WIDTH >> 2);
				else
					fractionHigh = fractionLow = 0;
			}
			else {
				rxMatch = NUMBER_INF_NAN_RX.exec(value);
				if (!rxMatch)
					throw new Error('Value (' + value + ') is not a valid hexfloat.');
				copyFrom(rxMatch[1] ? NAN : rxMatch[2] ? INFINITY : NEG_INFINITY);
			}
		}

		/**
		 * Whether this number is ±0 (a positive or a negative zero).
		 */
		public function get isZero():Boolean {
			return (exponent == EXPONENT_ZERO) && isFractionZero;
		}

		/**
		 * Whether this number is subnormal (denormalized) number.
		 */
		public function get isSubnormal():Boolean {
			return (exponent == EXPONENT_ZERO) && !isFractionZero;
		}

		/**
		 * Whether this number is ∞ (an Infinity).
		 */
		public function get isInfinity():Boolean {
			return (exponent == EXPONENT_INFINITY) && isFractionZero;
		}

		/**
		 * Whether this number is a NaN (not-a-number).
		 */
		public function get isNaN():Boolean {
			return (exponent == EXPONENT_INFINITY) && !isFractionZero;
		}

		/**
		 * Whether this number is a qNaN (quiet not-a-number).
		 */
		public function get isQuietNaN():Boolean {
			return isNaN && (w32 & QNAN_MASK);
		}

		/**
		 * Whether the fraction contains any non-zero bits.
		 */
		protected function get isFractionZero():Boolean {
			return !w0 && !(w32 & FRACT_MASK);
		}

		/**
		 * Determines whether this number is equal to the specified value.
		 * Unlike the built-in Number data type, two NaN values with the same payload are considered to be equal.
		 *
		 * @param value A Double.
		 * @return Boolean true if this number is equal to the specified value, otherwise false.
		 */
		public function equals(value:Double):Boolean {
			return (w0 == value.w0) && (w32 == value.w32);
		}

		/**
		 * Returns the number of ULPs between this number and the specified value.
		 * ULP stands for "unit of least precision" or "unit in the last place" and is the spacing between floating-point numbers, i.e., the value the least significant digit represents if it is 1.
		 * The result is a signed integer value (stored in type Number) representing the number of distinct representations between this number and value.
		 * If either number is a NaN, returns NaN.
		 * If one number is an infinity and the other number is finite, returns Infinity.
		 * If numbers are infinities of the same sign, returns NaN.
		 * If numbers are infinities of opposite sign, returns Infinity.
		 *
		 * @param value A Double.
		 * @return The number of ULPs between the two numbers.
		 */
		public function distance(value:Double):Number {
			var x:Double = this, y:Double = value;
			// If either of the numbers is NaN, return NaN.
			if (x.isNaN || y.isNaN)
				return NaN;

			var _isXInf:Boolean = x.isInfinity;
			var _isYInf:Boolean = y.isInfinity;
			// If one number is an infinity and the other number is finite, return Infinity.
			if (_isXInf != _isYInf)
				return Infinity;
			// If numbers are infinities of the same or opposite sign, return NaN or Infinity respectively.
			if (_isXInf && _isYInf)
				return (x.sign == y.sign) ? NaN : Infinity;

			// If numbers are of opposite sign, calculate the difference in two steps.
			if (x.sign != y.sign) {
				var dist:Number = 0;
				// If x is not 0, recursively get x's distance from the same-signed zero and add it to dist.
				if (!x.isZero)
					dist += (x.sign > 0) ? x.distance(ZERO) : x.distance(NEG_ZERO);
				// If y is not 0, recursively get y's distance from the same-signed zero and subtract it from dist.
				if (!y.isZero)
					dist -= (y.sign > 0) ? y.distance(ZERO) : y.distance(NEG_ZERO);
				return dist;
			}

			// Numbers are of the same sign. Calculate the difference.
			var d0:uint, d32:uint;
			if ((x.w32 > y.w32) || ((x.w32 == y.w32) && (x.w0 >= y.w0))) {
				// |x| >= |y|; distance(x, y) = sign(x) * (|x| - |y|).
				d0 = x.w0 - y.w0;
				d32 = x.w32 - y.w32;
				if (d0 > x.w0)
					--d32; // Borrow.
				return x.sign * (d32 * MathExt.POW2_32 + d0);
			}
			else {
				// |x| < |y|; distance(x, y) = -sign(x) * (|y| - |x|).
				d0 = y.w0 - x.w0;
				d32 = y.w32 - x.w32;
				if (d0 > y.w0)
					--d32; // Borrow.
				return -x.sign * (d32 * MathExt.POW2_32 + d0);
			}
		}

		/**
		 * Advances the number by a specified number of ULPs.
		 * Returns this object.
		 *
		 * @param distance A signed integer number of ULPs (stored in type Number). Must be a finite number with absolute value no more than 2^53.
		 * @return This object.
		 *
		 * @throws RangeError - if distance is NaN, an Infinity or abs(distance) >= 2^53.
		 */
		public function advance(distance:Number):Double {
			// If distance is NaN or has an absolute value over 2^53, throw an exception.
			if (MathExt.isNaN(distance) || (Math.abs(distance) > MathExt.DOUBLE_INT_MAX))
				throw new RangeError('The distance argument (' + distance + ') is outside the range [-' + MathExt.DOUBLE_INT_MAX + ', ' + MathExt.DOUBLE_INT_MAX + '].');
			// If distance is 0, or the number is NaN or Infinity, do nothing.
			if (!distance || (exponent == EXPONENT_INFINITY))
				return this;

			// If the number is 0, set the sign to match the sign of distance.
			if (isZero)
				sign = (distance > 0) ? 1 : -1;

			var _sign:int = sign;
			var a32:uint = Math.abs(distance) / MathExt.POW2_32;
			var a0:uint = Math.abs(distance) % MathExt.POW2_32;
			if (sign * distance > 0) {
				// Add.
				w0 += a0;
				w32 += a32;
				if (w0 < a0)
					++w32; // Carry.
			}
			else {
				// Subtract.
				if (w0 < a0)
					++a32; // Borrow.
				w0 -= a0;
				w32 -= a32;
			}

			// Check for overflow condition.
			if ((exponent == EXPONENT_INFINITY) || (sign != _sign)) {
				w0 = w32 = 0;
				sign = (distance > 0) ? 1 : -1;
				exponent = EXPONENT_INFINITY;
			}

			invalidateNumberFlag = true;
			return this;
		}

		/**
		 * Increments the number (advances the number by one ULP), resulting in the number becoming the nearest representable number which is greater than this number.
		 * Returns this object.
		 *
		 * @return This object.
		 */
		public function next():Double {
			return advance(1);
		}

		/**
		 * Decrements the number (advances the number by one ULP), resulting in the number becoming the nearest representable number which is greater than this number.
		 * Returns this object.
		 *
		 * @return This object.
		 */
		public function prior():Double {
			return advance(-1);
		}

		/**
		 * Returns a string representation of the number in hexfloat format, in the form [-][01].hhhhhhhhhhhhhp[-+]dddd.
		 * The digit before the radix point is 0 if the number is subnormal, 1 otherwise.
		 * The stored bits of the significand of the number are formatted as a string of hexadecimal digits.
		 * Insignificant trailing zeros after the decimal point are not included.
		 * If there are no digits after the decimal point, the decimal point is not included.
		 * The base-2 exponent is formatted as a decimal number using at least one digit but at most as many digits as necessary to represent the value exactly.
		 *
		 * @see http://www.gnu.org/software/libc/manual/html_node/Floating_002dPoint-Conversions.html
		 *
		 * @param trailingZeros Whether to include trailing zeros. If false, insignificant trailing zeros are not included, and if there are no digits after the decimal point, it is not included either.
		 * @return The string representation of the number in hexfloat format.
		 */
		public function toHexFloat(trailingZeros:Boolean = false):String {
			var exp:int = exponent;
			if (exp == EXPONENT_INFINITY)
				return _toStringInfNaN();

			var dig:String = isFractionZero ? '' : (StrUtil.lpad((w32 & FRACT_MASK).toString(16), '0', (FRACT_WIDTH - 32) >> 2) + StrUtil.lpad(w0.toString(16), '0', 8));
			if (dig)
				dig = '.' + (trailingZeros ? dig : StrUtil.rtrim(dig, '0'));
			return ((sign < 0) ? '-' : '') + ((exp > EXPONENT_ZERO) ? '1' : '0') + dig + 'p' + ((exp > EXPONENT_ZERO) ? (((exp >= 0) ? '+' : '') + exp) : '+0');
		}

		/**
		 * Returns a string representation of the number in exponential notation.
		 * The string will contain one non-zero digit before the decimal point and the specified number of digits after the decimal point.
		 * Trailing zeros are added if necessary.
		 * If fractionDigits is 0, the decimal point is not included.
		 * If rounding is necessary, the half-away from zero rounding method is used.
		 *
		 * @see com.razorscript.math.supportClasses.SimpleBigDecimal#toExponential()
		 *
		 * @param fractionDigits The desired number of digits after the decimal point.
		 * @param trailingZeros Whether to include trailing zeros. If false, insignificant trailing zeros are not included, and if there are no digits after the decimal point, it is not included either.
		 * @return The string representation of the number in exponential notation.
		 *
		 * @throws RangeError - if fractionDigits is negative.
		 */
		public function toExponential(fractionDigits:int = 6, trailingZeros:Boolean = true):String {
			if (fractionDigits < 0)
				throw new RangeError('The fractionDigits argument (' + fractionDigits + ') must be a non-negative number.');
			if (exponent == EXPONENT_INFINITY)
				return _toStringInfNaN();

			// One digit before the decimal point and fractionDigits digits after the decimal point.
			return _toBigDecimalAndRound(fractionDigits + 1)._toExponential(fractionDigits, trailingZeros);
		}

		// TODO: Add exponential engineering formats (e / E), and engineering with SI prefix format.

		/**
		 * Returns a string representation of the number in fixed-point (non-exponential) notation.
		 * The string will contain the specified number of digits after the decimal point.
		 * Trailing zeros are added if necessary.
		 * If fractionDigits is 0, the decimal point is not included.
		 * If rounding is necessary, the half-away from zero rounding method is used.
		 *
		 * @see com.razorscript.math.supportClasses.SimpleBigDecimal#toFixed()
		 *
		 * @param fractionDigits The desired number of digits after the decimal point.
		 * @param trailingZeros Whether to include trailing zeros. If false, insignificant trailing zeros are not included, and if there are no digits after the decimal point, it is not included either.
		 * @return The string representation of the number in fixed-point notation.
		 *
		 * @throws RangeError - if fractionDigits is negative.
		 */
		public function toFixed(fractionDigits:int = 6, trailingZeros:Boolean = true):String {
			if (fractionDigits < 0)
				throw new RangeError('The fractionDigits argument (' + fractionDigits + ') must be a non-negative number.');
			if (exponent == EXPONENT_INFINITY)
				return _toStringInfNaN();

			// Estimate the decimal exponent, using a small epsilon to guarantee rounding in the desired direction. The estimate is either accurate, or 1 (1+ for subnormal numbers) greater than the actual value.
			var estDecExp:int = (invalidateNumberFlag ? (MathExt.LOG10_2 * (exponent + Math.log(1 + (w32 & FRACT_MASK) / FRACT_MASK) * Math.LOG2E)) : (Math.log(number) * Math.LOG10E)) + 1e-5;
			// If the exponent is negative, there are -(exp + 1) leading zeros after the decimal point, each zero is a fractional digit but not a significant digit. Print (exp + 1) less digits.
			// If the exponent is non-negative, there are (exp + 1) digits before the decimal point, each digit is a significant digit but not a fractional digit. Print (exp + 1) more digits.
			var precision:int = fractionDigits + estDecExp + 1;
			if (precision >= 0) {
				var bigDec:SimpleBigDecimal = _toBigDecimal(precision + 1); // One extra digit for rounding.
				// TODO: Make a test suite, test if this ever happens then remove this assertion.
				if (bigDec.exp > estDecExp)
					Logger.warn(this, 'Assertion failed: (' + bigDec.exp + ' > ' + estDecExp + '); number=' + number + ' fractionDigits=' + fractionDigits + '.');
				// Recalculate the precision using the actual value of the decimal exponent.
				precision = fractionDigits + bigDec.exp + 1;
			}
			bigDec = (precision >= 0) ? bigDec.roundToPrecision(precision) : (sign > 0) ? SimpleBigDecimal.ZERO : SimpleBigDecimal.NEG_ZERO;
			return bigDec._toFixed(fractionDigits, trailingZeros);
		}

		/**
		 * Returns a string representation of the number in either fixed-point or exponential notation, whichever is more appropriate for it's magnitude.
		 * If the number's decimal exponent exp is in the range (fixedMinExponent <= exp < precision), the fixed-point notation is used, otherwise exponential notation is used.
		 * The string will contain the specified number of significant digits.
		 * In both cases insignificant trailing zeros after the decimal point are not included.
		 * If there are no digits after the decimal point, the decimal point is not included.
		 * If precision is 0, precision is set to 1.
		 * If rounding is necessary, the half-away from zero rounding method is used.
		 *
		 * @see com.razorscript.math.supportClasses.SimpleBigDecimal#toGeneral()
		 *
		 * @param precision The desired number of significant digits. If precision is 0, precision is set to 1.
		 * @param trailingZeros Whether to include trailing zeros. If false, insignificant trailing zeros are not included, and if there are no digits after the decimal point, it is not included either.
		 * @param fixedMinExponent The low endpoint of the decimal exponent's range where the fixed-point notation is used.
		 * @return The string representation of the number in general format.
		 *
		 * @throws RangeError - if precision is negative.
		 */
		public function toGeneral(precision:int = 6, trailingZeros:Boolean = false, fixedMinExponent:int = -4):String {
			if (precision < 0)
				throw new RangeError('The precision argument (' + precision + ') must be a non-negative number.');
			if (exponent == EXPONENT_INFINITY)
				return _toStringInfNaN();

			if (precision == 0)
				precision = 1;
			var bigDec:SimpleBigDecimal = _toBigDecimalAndRound(precision);
			var exp:int = bigDec.exp;
			return ((exp >= fixedMinExponent) && (exp < precision)) ? bigDec._toFixed(Math.max(0, precision - exp - 1), trailingZeros) : bigDec._toExponential(precision - 1, trailingZeros);
		}

		/**
		 * Returns a new SimpleBigDecimal representing this number.
		 * The SimpleBigDecimal will contain the specified number of significant digits.
		 * If round argument is true, one extra digit is printed and then the digits are rounded to precision.
		 * If round argument is false, no extra digits are printed and no rounding is performed.
		 *
		 * @param precision The desired number of significant digits. If precision is 0, precision is set to unlimited.
		 * @param round If true, one extra digit is printed and then the digits are rounded to precision, otherwise no extra digits are printed and no rounding is performed (i.e. the number is truncated).
		 * @return A new SimpleBigDecimal representing this number.
		 *
		 * @throws RangeError - if precision is negative.
		 */
		public function toBigDecimal(precision:int = MathExt.DOUBLE_UNIQ_DIGITS, round:Boolean = true):SimpleBigDecimal {
			if (precision < 0)
				throw new RangeError('The precision argument (' + precision + ') must be a non-negative number.');
			if (exponent == EXPONENT_INFINITY)
				return new SimpleBigDecimal(sign, SimpleBigDecimal.EXPONENT_INFINITY, isFractionZero ? '' : ('0x' + hexNanPayload));

			if (precision == 0) {
				precision = int.MAX_VALUE;
				round = false;
			}
			return round ? _toBigDecimalAndRound(precision).clone() : _toBigDecimal(precision).clone();
		}

		/**
		 * Prints the digits and returns the bigDec SimpleBigDecimal object. Used internally by NumberFormatter.
		 * This method returns the protected static variable bigDec, which is liable to change, and so should be used with caution.
		 *
		 * @see com.razorscript.math.NumberFormatter
		 */
		razor_internal function _toBigDecimal(precision:int):SimpleBigDecimal {
			if (isZero) {
				initBigDec(sign, 0, '0');
			}
			else {
				printDigits(precision);
				bigDec.dig = StrUtil.rtrim(bigDec.dig.substr(0, precision), '0');
				if (!bigDec.dig) {
					// This shouldn't happen.
					// TODO: Make a test suite, test if this ever happens then remove this safety code.
					Logger.warn(this, 'Converting value (' + number + ') to big decimal with precision=' + precision + ' resulted in zero significant digits.');
					bigDec.exp = 0;
					bigDec.dig = '0';
				}
			}
			return bigDec;
		}

		razor_internal function _toBigDecimalAndRound(precision:int):SimpleBigDecimal {
			return _toBigDecimal(precision + 1).roundToPrecision(precision);
		}

		protected function _toStringInfNaN():String {
			return isFractionZero ? ((sign < 0) ? '-Infinity' : 'Infinity') : 'NaN';
		}

		protected static const NUMBER_INF_NAN_RX:RegExp = /^ \s* (?: (NaN) | ((?: \+ \s*)? Inf) | (- \s* Inf))/xi;
		protected static const NUMBER_RX:RegExp = /^ \s* ([-+]?) \s* (0*) (\d*) \.? (0*) (\d*) (?: e ([-+]? \d+))?/xi;

		protected static const PD_MAX_VALUE_MIN_DIG:String = '179769313486231560836'; // The digits of (MAX_VALUE - 0.5 ULP), rounded up to 21 significant digits.
		protected static const PD_MAX_VALUE_MAX_DIG:String = '179769313486231580793'; // The digits of (MAX_VALUE + 0.5 ULP), rounded down to 21 significant digits.
		protected static const PD_MAX_VALUE_EXP:int = 308; // The base-10 exponent of MAX_VALUE.
		protected static const PD_TRUSTED_RANGE:Number = 3.125e-309; // If the absolute value of a number is not greater than this experimentally determined value, parseFloat() seems to be accurate.

		/**
		 * Converts a string to a floating-point number. Provides more accurate results than parseFloat().
		 * Leading whitespace characters are ignored, as are trailing non-numeric characters.
		 * If the string can't be parsed, the result is NaN.
		 * Sets the number property to the parsed number.
		 * Returns this object.
		 *
		 * The parseFloat() function is inaccurate for many inputs, with confirmed error of up to 7 ULPs for some inputs.
		 * This function relies on parseFloat() to get the first conversion candidate, accurate enough to differ from the correct conversion by at most 1 in the 15th digit.
		 * The first candidate is then compared with the real number specified by the input string (the target), to determine if there is another representable number which is a better conversion.
		 * The result is almost always the correct conversion. If the target is half-way between two representable numbers, the result is either correct, or 1 ULP away from correct.
		 *
		 * @param str The string to read and convert to a floating-point number.
		 * @return This object.
		 */
		public function parseDouble(str:String):Double {
			// Extract the sign, digits and exponent from the input string.
			var rxMatch:Array = NUMBER_RX.exec(str);
			var sign:int = (rxMatch[1] == '-') ? -1 : 1;
			var targetExp:int = (rxMatch[6] ? parseInt(rxMatch[6]) : 0) + (rxMatch[3] ? rxMatch[3].length : -rxMatch[4].length) - 1;
			var targetDig:String = rxMatch[3] ? (rxMatch[3] + rxMatch[4] + rxMatch[5]) : rxMatch[5];
			var num:Number;
			if (targetDig) {
				if (targetExp == PD_MAX_VALUE_EXP) {
					// The target exponent is equal to the exponent of MAX_VALUE, check the target digits to determine if the result should be ±MAX_VALUE or ±Infinity.
					str = StrUtil.rpad(targetDig.substr(0, 21), '0', 21);
					if (str > PD_MAX_VALUE_MAX_DIG)
						return copyFrom((sign > 0) ? INFINITY : NEG_INFINITY);
					else if (str >= PD_MAX_VALUE_MIN_DIG)
						return copyFrom((sign > 0) ? MAX_VALUE : NEG_MAX_VALUE);
				}
				else if (targetExp > PD_MAX_VALUE_EXP) {
					return copyFrom((sign > 0) ? INFINITY : NEG_INFINITY);
				}
				// Reformat the input string in exponential notation, with at most 21 digits. Workaround for a parseFloat() bug (https://bugbase.adobe.com/index.cfm?event=bug&id=4031880).
				num = parseFloat(((sign < 0) ? '-' : '') + targetDig.substr(0, 1) + '.' + StrUtil.rtrim(targetDig.substr(1, (targetExp == PD_MAX_VALUE_EXP) ? 14 : 20), '0') + 'e' + targetExp);
			}
			else if (rxMatch[2] || rxMatch[4]) {
				num = 0;
			}
			else {
				rxMatch = NUMBER_INF_NAN_RX.exec(str);
				return copyFrom((!rxMatch || rxMatch[1]) ? NAN : rxMatch[2] ? INFINITY : NEG_INFINITY);
			}
			// If the number is 0, an infinity or NaN, copy the properties from a corresponding Double constant and return.
			if (num == 0)
				return copyFrom((sign > 0) ? ZERO : NEG_ZERO);
			else if (!isFinite(num))
				return copyFrom((sign > 0) ? INFINITY : NEG_INFINITY);
			else if (num != num)
				return copyFrom(NAN);
			number = num;
			// If the absolute value of a number is not greater than the experimentally determined PD_TRUSTED_RANGE, parseFloat() seems to be accurate, return.
			if (Math.abs(num) <= PD_TRUSTED_RANGE)
				return this;

			// The number returned by parseFloat() is the first candidate.
			printDigits(PD_PRINT_DIG);
			var exp:int = bigDec.exp;
			var dig:String = bigDec.dig;
			// Calculate the distance and direction from the first candidate to the target. If the distance is 0, the first candidate is a perfect match.
			var diff:Number = _pdSubtract(targetDig, targetExp, dig, exp, targetExp);
			if (!diff)
				return this;
			var direction:int = (diff < 0) ? -sign : sign;

			// Save the first candidate.
			var _w0:uint = w0;
			var _w32:uint = w32;
			// Advance the number one ULP towards the target. This is the second candidate. The two candidates might be on different sides of the target.
			advance(direction);
			printDigits(PD_PRINT_DIG);
			// Calculate the distance between the two candidates (equivalent to one ULP), and then the number of ULPs between the second candidate and the target.
			var ulp:Number = _pdSubtract(bigDec.dig, bigDec.exp, dig, exp, targetExp);
			var steps:Number = diff / ulp - 1; // Already advanced one ULP.

			if (Math.abs((steps % 1) - 0.5) < 1e-4) {
				// The number of ULPs between the number and the target is nearly half-way between two integers. Compare the absolute distances from the two candidates to the target to break the tie.
				if (steps > 0) {
					// Advance the number floor(steps) ULPs towards the target. This is the new first candidate. If floor(steps) is 0, the second candidate becomes the new first candidate.
					steps = Math.floor(steps);
					if (steps) {
						advance(direction * steps);
						printDigits(PD_PRINT_DIG);
					}
					// Calculate the distance from the new first candidate to the target.
					diff = _pdSubtract(targetDig, targetExp, bigDec.dig, bigDec.exp, targetExp);
					// Save the new first candidate.
					_w0 = w0;
					_w32 = w32;
					// Advance the number one more ULP towards the target. This is the new second candidate.
					advance(direction);
					printDigits(PD_PRINT_DIG);
				}
				diff = Math.abs(diff);
				// Calculate the absolute distance from the second candidate to the target.
				var diff2:Number = Math.abs(_pdSubtract(targetDig, targetExp, bigDec.dig, bigDec.exp, targetExp));

				// The two candidates are on different sides of the target. The candidate with the smaller absolute distance to the target wins. If there's still a tie, the candidate further away from zero wins.
				if ((diff < diff2) || ((diff == diff2) && (direction * sign < 0))) {
					// Restore the first candidate.
					w0 = _w0;
					w32 = _w32;
				}
			}
			else {
				// The number of ULPs between the number and the target can be rounded with sufficient certainty that the resulting number will be the nearest representable to the target.
				steps = Math.round(steps);
				if (steps > 0) {
					// Advance the number steps ULPs towards the target.
					advance(direction * steps);
				}
				else if (steps < 0) {
					// Restore the first candidate.
					w0 = _w0;
					w32 = _w32;
				}
			}

			return this;
		}

		protected static const PD_PRINT_DIG:int = 24;
		protected static const PD_ACC_DIG:int = MathExt.DOUBLE_ACC_DIGITS; // 15.
		protected static const PD_DIFF_DIG:int = PD_PRINT_DIG - PD_ACC_DIG; // 9.
		protected static const PD_DIFF_BORROW:int = Math.pow(10, PD_DIFF_DIG); // 1,000,000,000.

		/**
		 * Subtracts two close numbers. Each number is represented in normalized exponential notation, with the coefficient stored as a string of decimal digits, starting with a non-zero digit.
		 * The numbers must be close enough that, rounded to 15 digits, they are allowed to differ by at most 1 in the last digit. The numbers' exponents are allowed to differ by at most 1.
		 * The result is scaled according to the specified reference exponent, which is allowed to differ from either number's exponent by at most 1.
		 *
		 * @param xDig The coefficient of the number x.
		 * @param xExp The exponent of the number x.
		 * @param yDig The coefficient of the number y.
		 * @param yExp The exponent of the number y.
		 * @param refExp The reference exponent to determine the scale of the result.
		 * @return The difference between x and y.
		 */
		protected function _pdSubtract(xDig:String, xExp:int, yDig:String, yExp:int, refExp:int):Number {
			// If the exponents are not equal, align the digits and adjust the exponents.
			if (xExp < yExp) {
				++xExp;
				xDig = '0' + xDig;
			}
			else if (xExp > yExp) {
				++yExp;
				yDig = '0' + yDig;
			}

			var i:int = 0;
			var xLen:int = xDig.length;
			var yLen:int = yDig.length;
			var length:int = Math.min(xLen, yLen);
			// Skip the matching digits.
			while ((i < length) && (xDig.charAt(i) == yDig.charAt(i)))
				++i;
			var sign:int;
			if (i == length) {
				// All the digits match.
				if (xLen == yLen)
					return 0;
				// The number which has more digits is greater.
				sign = (xLen > yLen) ? 1 : -1;
			}
			else {
				// Determine the sign of the difference by comparing the first non-matching digits.
				sign = (xDig.charCodeAt(i) > yDig.charCodeAt(i)) ? 1 : -1;
			}
			if (sign < 0) {
				// If x is smaller than y, swap the numbers.
				var str:String = xDig;
				xDig = yDig;
				yDig = str;
			}

			// Calculate the difference.
			var xInt:int = parseInt(StrUtil.rpad(xDig.substr(PD_ACC_DIG, PD_DIFF_DIG), '0', PD_DIFF_DIG));
			var yInt:int = parseInt(StrUtil.rpad(yDig.substr(PD_ACC_DIG, PD_DIFF_DIG), '0', PD_DIFF_DIG));
			var diff:Number = sign * ((xInt >= yInt) ? (xInt - yInt + 1) : (xInt - yInt + PD_DIFF_BORROW - 1)); // If borrowing a digit, round using floor, otherwise round using ceiling.
			return (xExp < refExp) ? diff : (xExp == refExp) ? (10 * diff) : (100 * diff);
		}

		/**
		 * Sets the object's properties.
		 * Returns this object.
		 *
		 * @param sign The sign of the number (±1). Zeros are also signed (±0).
		 * @param exponent The base-10 exponent of the number.
		 * @param fraction The stored bits of the significand of the number as a string of binary or hexadecimal digits. Doesn't include the implicit leading bit.
		 * @return This object.
		 */
		public function setTo(sign:int, exponent:int, fraction:String):Double {
			this.sign = sign;
			this.exponent = exponent;
			if (fraction.length == FRACT_WIDTH)
				this.fraction = fraction;
			else
				hexFraction = fraction;
			return this;
		}

		/**
		 * Copies the object's properties from another object.
		 * Returns this object.
		 *
		 * @param srcDec A source object to copy from.
		 * @return This object.
		 */
		public function copyFrom(src:Double):Double {
			_number = src._number;
			w0 = src.w0;
			w32 = src.w32;
			invalidateNumberFlag = src.invalidateNumberFlag;
			return this;
		}

		/**
		 * Returns a new Double that is identical to this one.
		 *
		 * @return A copy of this double.
		 */
		public function clone():Double {
			return new Double(this);
		}

		public function toString():String {
			return '[Double number=' + StrUtil.rpad(toExponential(20), ' ', 28) +
				' bits=' + ((sign > 0) ? 0 : 1) + ',' + StrUtil.lpad(((w32 & EXP_MASK) >> EXP_SHIFT).toString(2), '0', EXP_WIDTH) + ',' +
				(((exponent >= EXPONENT_MIN) && (exponent <= EXPONENT_MAX)) ? '(1)' : '(0)') + fraction + ' sign=' + sign + ' exponent=' + exponent +
				(isZero ? ' isZero' : '') + (isSubnormal ? ' isSubnormal' : '') + (isInfinity ? ' isInfinity' : '') + (isQuietNaN ? ' isQuietNaN' : isNaN ? ' isNaN' : '') + ']';
		}

		protected static const EXP_WIDTH:int = 11;
		protected static const EXP_SHIFT:int = 31 - EXP_WIDTH;
		protected static const EXP_BIAS:int = 1023;
		protected static const FRACT_WIDTH:int = 52;
		protected static const NAN_PAYLOAD_WIDTH:int = FRACT_WIDTH - 1;

		protected static const SIGN_MASK:uint = uint(1 << 31);
		protected static const EXP_MASK:uint = SIGN_MASK >> (EXP_WIDTH - 1) >>> 1;
		protected static const FRACT_MASK:uint = 0xffffffff >>> (EXP_WIDTH + 1);
		protected static const IMPLICIT_BIT_MASK:uint = 1 << (FRACT_WIDTH - 32);
		protected static const NAN_PAYLOAD_MASK:uint = FRACT_MASK >> 1;
		protected static const QNAN_MASK:uint = IMPLICIT_BIT_MASK >> 1;

		/** Whether the number needs to be validated. */
		protected var invalidateNumberFlag:Boolean;

		/** Word 0, holding bits 0 to 31. */
		protected var w0:uint;
		/** Word 1, holding bits 32 to 63. */
		protected var w32:uint;

		/**
		 * Converts words into number and sets the invalidateNumberFlag to false.
		 */
		protected function validateNumber():void {
			writeWordsToNumber();
			invalidateNumberFlag = false;
		}

		/**
		 * Converts words into number.
		 */
		protected function readWordsFromNumber():void {
			byteArray.position = 0;
			byteArray.writeDouble(_number);
			byteArray.position = 0;
			w32 = byteArray.readUnsignedInt();
			w0 = byteArray.readUnsignedInt();
		}

		/**
		 * Converts number into words.
		 */
		protected function writeWordsToNumber():void {
			byteArray.position = 0;
			byteArray.writeUnsignedInt(w32);
			byteArray.writeUnsignedInt(w0);
			byteArray.position = 0;
			_number = byteArray.readDouble();
		}

		/**
		 * Prints the digits of this number in normalized exponential format (omitting the sign and the decimal point).
		 * Assumes this number is finite and numDigits is positive.
		 * Digits are printed until the desired number of digits is reached, or there are no more digits to print.
		 * Actual number of digits printed might be slightly more or less than the desired number of digits. A few trailing zeros might be printed.
		 * Results are stored in the bigDec SimpleBigDecimal object.
		 *
		 * number ≈ bigDec.sign * parseFloat(bigDec.dig.charAt(0) + '.' + bigDec.dig.substr(1) + 'e' + bigDec.exp)
		 *
		 * @see https://randomascii.wordpress.com/2012/03/08/float-precisionfrom-zero-to-100-digits-2/
		 *
		 * @param numDigits The number of significant digits to print.
		 */
		protected function printDigits(numDigits:int):void {
			var exp:int = (exponent == EXPONENT_ZERO) ? EXPONENT_MIN : exponent;
			var high:uint = (exponent == EXPONENT_ZERO) ? (w32 & FRACT_MASK) : ((w32 & FRACT_MASK) | IMPLICIT_BIT_MASK);

			initBigDec(sign, -1, '');
			if (exp >= 0) {
				_printIntDigits(high, w0, exp, numDigits);
				numDigits -= bigDec.dig.length;
			}
			if (numDigits > 0) {
				_printFractDigits(high, w0, exp, numDigits);
			}
			if (!bigDec.dig) {
				bigDec.exp = 0;
				bigDec.dig = '0';
			}
		}

		/**
		 * Prints the integer digits of the specified number.
		 * Digits are appended to the _dec.dig string variable until the desired number of digits is reached, or there are no more digits to print.
		 * Actual number of digits printed might exceed the desired number of digits by at most 3. Trailing zeros are printed.
		 * The number's decimal exponent is stored in the _dec.exp variable.
		 *
		 * @param high The high (most significant) word of the number's significand, including the implicit leading bit.
		 * @param low The low (least significant) word of the number's significand.
		 * @param exp The number's bias-adjusted base-2 exponent.
		 * @param numDigits The desired number of digits to print.
		 */
		protected function _printIntDigits(high:uint, low:uint, exp:int, numDigits:int):void {
			initIntDigits();
			initBigInt();
			bigInt.insertLowBits(high, exp + 64 - FRACT_WIDTH);
			bigInt.insertLowBits(low, exp + 32 - FRACT_WIDTH);
			// Check isZero property first, to be able to use the faster isZeroFlag property in the while loop condition.
			if (!bigInt.isZero) {
				var i:int = 0;
				do {
					intDigits[i++] = bigInt.div16(10000);
				} while (!bigInt.isZeroFlag);
				// The intDigits vector now contains the integer part digits in reverse, element 0 holding the least significant digit(s) and element (i - 1) holding the most significant digit(s).
				// Each element of the vector is a number between 0 and 9999 and represents 4 digits or from 1 to 4 digits for element (i - 1).
				// Append leading digits without left-padding with zeros.
				var dig:String = intDigits[--i].toString();
				bigDec.exp += dig.length + (i << 2);
				var breakIndex:int = Math.max(0, i - Math.ceil((numDigits - dig.length) / 4));
				// Append the rest of the digits, left-padding each element with zeros to 4 characters, if necessary.
				while (--i >= breakIndex)
					dig += IntUtil.lpad0_4(intDigits[i]);
				bigDec.dig += dig;
			}
		}

		/**
		 * Prints the fractional digits of the specified number.
		 * Digits are appended to the _dec.dig string variable until the desired number of digits is reached, or there are no more digits to print.
		 * Actual number of digits printed might exceed the desired number of digits by at most 3. At most 3 trailing zeros might be printed.
		 * The number's decimal exponent is stored in the _dec.exp variable.
		 *
		 * @param high The high (most significant) word of the number's significand, including the implicit leading bit.
		 * @param low The low (least significant) word of the number's significand.
		 * @param exp The number's bias-adjusted base-2 exponent.
		 * @param numDigits The desired number of digits to print.
		 */
		protected function _printFractDigits(high:uint, low:uint, exp:int, numDigits:int):void {
			initBigInt();
			bigInt.insertHighBits(high, FRACT_WIDTH - exp - 32);
			bigInt.insertHighBits(low, FRACT_WIDTH - exp);
			// Check isZero property first, to be able to use the faster isZeroFlag property in the while loops conditions.
			if (!bigInt.isZero) {
				var dig:String = '';
				if (bigDec.exp < 0) {
					// Decimal exponent is negative, integer part is zero. Count leading zeros and adjust the decimal exponent.
					do {
						var over:uint = bigInt.mul16(10000);
						if (over) {
							// Overflow is a number between 0 and 9999 and represents 4 digits.
							// Append leading digits without left-padding with zeros.
							dig = over.toString();
							numDigits -= dig.length;
							bigDec.exp -= 4 - dig.length;
							break;
						}
						else {
							// 4 leading zeros.
							bigDec.exp -= 4;
						}
					} while (!bigInt.isZeroFlag);
				}
				while ((numDigits > 0) && !bigInt.isZeroFlag) {
					dig += IntUtil.lpad0_4(bigInt.mul16(10000));
					numDigits -= 4;
				}
				bigDec.dig += dig;
			}
		}

		protected static var bigDec:SimpleBigDecimal;
		protected static function initBigDec(sign:int, exp:int, dig:String):void {
			if (bigDec)
				bigDec.setTo(sign, exp, dig);
			else
				bigDec = new SimpleBigDecimal(sign, exp, dig);
		}

		/** The number of words in bigInt. The 52-bit significand can be shifted 1022 positions after the radix point, that's 1074 bits (34 words) for the fractional part. Integer part needs 1024 bits (32 words). */
		protected static const BIG_INT_WORDS:uint = 34;

		protected static var bigInt:SimpleBigInt;
		protected static function initBigInt():void {
			if (bigInt)
				bigInt.clear();
			else
				bigInt = new SimpleBigInt(BIG_INT_WORDS);
		}

		/** The number of elements in intDigits. The integer part of the largest representable finite number MAX_VALUE has 308 decimal digits, that's 77 uints containing 4 digits each. */
		protected static const INT_DIGITS_LENGTH:uint = 77;

		protected static var intDigits:Vector.<uint>;
		protected static function initIntDigits():void {
			if (!intDigits)
				intDigits = new Vector.<uint>(INT_DIGITS_LENGTH);
		}
	}
}
