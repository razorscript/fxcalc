package com.razorscript.math.errors {
	
	/**
	 * Thrown when an exceptional arithmetic condition has occurred.
	 *
	 * @author Gene Pavlovsky
	 */
	public class ArithmeticError extends Error {
		// TODO: Add static constants with error ids and standard messages (e.g. DIVISION_BY_ZERO, 'Division by zero.').
		
		public function ArithmeticError(message:* = '', id:* = 0) {
			super(message, id);
		}
	}
}
