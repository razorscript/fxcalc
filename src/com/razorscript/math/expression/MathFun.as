package com.razorscript.math.expression {
	import com.razorscript.math.MathContext;
	import com.razorscript.math.expression.supportClasses.MathFunBase;
	import com.razorscript.math.functions.MathLib;
	import com.razorscript.math.functions.mathLib.ArithmeticMath;
	import com.razorscript.math.functions.mathLib.LogicalMath;
	import com.razorscript.math.functions.mathLib.ProbabilityMath;
	import com.razorscript.math.functions.mathLib.RelationalMath;
	import com.razorscript.math.functions.mathLib.StatisticsMath;
	import com.razorscript.math.functions.mathLib.TrigonometryMath;
	
	/**
	 * Provides implementations and function info for math functions.
	 * Defines constants for math function names.
	 *
	 * @author Gene Pavlovsky
	 */
	public class MathFun extends MathFunBase {
		/**
		 * Arithmetic.
		 */
		public static const ID:String = 'id';
		public static const NEG:String = 'neg';
		public static const DIV:String = 'div';
		public static const MOD:String = 'mod';
		public static const REM:String = 'rem';
		public static const ABS:String = 'abs';
		public static const SIGN:String = 'sign';
		public static const FLOOR:String = 'floor';
		public static const CEIL:String = 'ceil';
		public static const FIX:String = 'fix';
		public static const ROUND:String = 'round';
		public static const ROUND_TO_DIGITS:String = 'roundToDigits';
		public static const ROUND_TO_NEAREST:String = 'roundToNearest';
		public static const CLAMP:String = 'clamp';
		
		public static const LN:String = 'ln';
		public static const LOG_2:String = 'log2';
		public static const LOG_10:String = 'log10';
		public static const LOG_N:String = 'logN';
		
		public static const SQUARE:String = 'square';
		public static const CUBE:String = 'cube';
		public static const POW:String = 'pow';
		public static const EXP_10:String = 'exp10';
		public static const EXP:String = 'expE';
		
		public static const SQRT:String = 'sqrt';
		public static const CURT:String = 'curt';
		public static const NTH_ROOT:String = 'nthRoot';
		
		public static const GCD:String = 'gcd';
		public static const LCM:String = 'lcm';
		
		/**
		 * Logical.
		 */
		public static const BOOLEAN:String = 'boolean';
		public static const NOT:String = 'not';
		public static const AND:String = 'and';
		public static const OR:String = 'or';
		public static const XOR:String = 'xor';
		
		/**
		 * Probability.
		 */
		public static const FAC:String = 'fac';
		public static const NCR:String = 'ncr';
		public static const NPR:String = 'npr';
		public static const RANDOM:String = 'random';
		public static const RANDOM_RANGE:String = 'randomRange';
		public static const RANDOM_RANGE_INT:String = 'randomRangeInt';
		
		/**
		 * Relational.
		 */
		public static const EQUAL:String = 'equal';
		public static const UNEQUAL:String = 'unequal';
		public static const LARGER:String = 'larger';
		public static const LARGER_EQ:String = 'largerEq';
		public static const SMALLER:String = 'smaller';
		public static const SMALLER_EQ:String = 'smallerEq';
		public static const COMPARE:String = 'compare';
		
		/**
		 * Statistics.
		 */
		public static const MIN:String = 'min';
		public static const MAX:String = 'max';
		
		/**
		 * Trigonometry.
		 */
		public static const SIN:String = 'sin';
		public static const COS:String = 'cos';
		public static const TAN:String = 'tan';
		public static const COT:String = 'cot';
		public static const SEC:String = 'sec';
		public static const CSC:String = 'csc';
		
		public static const ARCSIN:String = 'arcsin';
		public static const ARCCOS:String = 'arccos';
		public static const ARCTAN:String = 'arctan';
		public static const ARCTAN_YX:String = 'arctanYX';
		public static const ARCCOT:String = 'arccot';
		public static const ARCSEC:String = 'arcsec';
		public static const ARCCSC:String = 'arccsc';
		
		public static const SINH:String = 'sinh';
		public static const COSH:String = 'cosh';
		public static const TANH:String = 'tanh';
		public static const COTH:String = 'coth';
		public static const SECH:String = 'sech';
		public static const CSCH:String = 'csch';
		
		public static const ARCSINH:String = 'arcsinh';
		public static const ARCCOSH:String = 'arccosh';
		public static const ARCTANH:String = 'arctanh';
		public static const ARCCOTH:String = 'arccoth';
		public static const ARCSECH:String = 'arcsech';
		public static const ARCCSCH:String = 'arccsch';
		
		public function MathFun(mathContext:MathContext, mathLib:MathLib) {
			_mathLib = mathLib;
			super(mathContext);
		}
		
		protected var _mathLib:MathLib;
		
		public function get mathLib():MathLib {
			return _mathLib;
		}
		
		override public function set mathContext(value:MathContext):void {
			_mathLib.mathContext = super.mathContext = value;
		}
		
		override protected function initFunctions():void {
			var arithmeticMath:ArithmeticMath = _mathLib.arithmeticMath;
			addFun(MathOperator.ADD, arithmeticMath.add);
			addFun(MathOperator.SUBTRACT, arithmeticMath.subtract);
			addFun(MathOperator.MULTIPLY, arithmeticMath.multiply);
			addFun(MathOperator.DIVIDE, arithmeticMath.divide);
			addFun(DIV, arithmeticMath.div);
			addFun(MOD, arithmeticMath.mod);
			addFun(REM, arithmeticMath.rem);
			addFun(MathOperator.FRACTION, arithmeticMath.fraction);
			addFun(MathOperator.RECIPROCAL, arithmeticMath.reciprocal);
			addFun(ID, arithmeticMath.identity);
			addFun(NEG, arithmeticMath.negate);
			addFun(ABS, arithmeticMath.abs);
			addFun(SIGN, arithmeticMath.sign);
			
			addFun(FLOOR, arithmeticMath.floor);
			addFun(CEIL, arithmeticMath.ceil);
			addFun(FIX, arithmeticMath.fix);
			addFun(ROUND, arithmeticMath.round);
			addFun(ROUND_TO_DIGITS, arithmeticMath.roundToDigits);
			addFun(ROUND_TO_NEAREST, arithmeticMath.roundToNearest);
			
			addFun(CLAMP, arithmeticMath.clamp);
			
			addFun(LN, arithmeticMath.ln);
			addFun(LOG_2, arithmeticMath.log2);
			addFun(LOG_10, arithmeticMath.log10);
			addFun(LOG_N, arithmeticMath.logN);
			
			addFun(SQUARE, arithmeticMath.square);
			addFun(CUBE, arithmeticMath.cube);
			addFun(POW, arithmeticMath.pow);
			addFun(EXP_10, arithmeticMath.exp10);
			addFun(EXP, arithmeticMath.exp);
			
			addFun(SQRT, arithmeticMath.sqrt);
			addFun(CURT, arithmeticMath.curt);
			addFun(NTH_ROOT, arithmeticMath.nthRoot);
			
			addFun(GCD, arithmeticMath.gcd, 2);
			addFun(LCM, arithmeticMath.lcm, 2);
			
			/// Logical.
			var logicalMath:LogicalMath = _mathLib.logicalMath;
			addFun(BOOLEAN, logicalMath.boolean);
			addFun(NOT, logicalMath.not);
			addFun(AND, logicalMath.and);
			addFun(OR, logicalMath.or);
			addFun(XOR, logicalMath.xor);
			
			/// Probability.
			var probabilityMath:ProbabilityMath = _mathLib.probabilityMath;
			addFun(FAC, probabilityMath.fac);
			addFun(NCR, probabilityMath.ncr);
			addFun(NPR, probabilityMath.npr);
			
			addFun(RANDOM, probabilityMath.random, 0, 1);
			addFun(RANDOM_RANGE, probabilityMath.randomRange, 0, 3);
			addFun(RANDOM_RANGE_INT, probabilityMath.randomRangeInt);
			
			/// Relational.
			var relationalMath:RelationalMath = _mathLib.relationalMath;
			addFun(EQUAL, relationalMath.equal);
			addFun(UNEQUAL, relationalMath.unequal);
			addFun(LARGER, relationalMath.larger);
			addFun(LARGER_EQ, relationalMath.largerEq);
			addFun(SMALLER, relationalMath.smaller);
			addFun(SMALLER_EQ, relationalMath.smallerEq);
			addFun(COMPARE, relationalMath.compare);
			
			/// Statistics.
			var statisticsMath:StatisticsMath = _mathLib.statisticsMath;
			addFun(MIN, statisticsMath.min, 2);
			addFun(MAX, statisticsMath.max, 2);
			
			/// Trigonometry.
			var trigonometryMath:TrigonometryMath = _mathLib.trigonometryMath;
			addFun(SIN, trigonometryMath.sin);
			addFun(COS, trigonometryMath.cos);
			addFun(TAN, trigonometryMath.tan);
			addFun(COT, trigonometryMath.cot);
			addFun(SEC, trigonometryMath.sec);
			addFun(CSC, trigonometryMath.csc);
			
			addFun(ARCSIN, trigonometryMath.asin);
			addFun(ARCCOS, trigonometryMath.acos);
			addFun(ARCTAN, trigonometryMath.atan);
			addFun(ARCTAN_YX, trigonometryMath.atanYX);
			addFun(ARCCOT, trigonometryMath.acot);
			addFun(ARCSEC, trigonometryMath.asec);
			addFun(ARCCSC, trigonometryMath.acsc);
			
			addFun(SINH, trigonometryMath.sinh);
			addFun(COSH, trigonometryMath.cosh);
			addFun(TANH, trigonometryMath.tanh);
			addFun(COTH, trigonometryMath.coth);
			addFun(SECH, trigonometryMath.sech);
			addFun(CSCH, trigonometryMath.csch);
			
			addFun(ARCSINH, trigonometryMath.asinh);
			addFun(ARCCOSH, trigonometryMath.acosh);
			addFun(ARCTANH, trigonometryMath.atanh);
			addFun(ARCCOTH, trigonometryMath.acoth);
			addFun(ARCSECH, trigonometryMath.asech);
			addFun(ARCCSCH, trigonometryMath.acsch);
		}
		
		override protected function initAliases():void {
			addAliases(MathOperator.ADD, MathOperator.ASSIGN_ADD, MathOperator.RASSIGN_ADD);
			addAliases(MathOperator.SUBTRACT, MathOperator.ASSIGN_SUBTRACT, MathOperator.RASSIGN_SUBTRACT);
			addAliases(MathOperator.MULTIPLY, MathOperator.ASSIGN_MULTIPLY, MathOperator.RASSIGN_MULTIPLY, MathOperator.MULTIPLY_IMPLICIT, MathOperator.MULTIPLY_IMPLICIT_VAR);
			addAliases(MathOperator.DIVIDE, MathOperator.ASSIGN_DIVIDE, MathOperator.RASSIGN_DIVIDE);
			addAliases(DIV, MathOperator.INT_DIVIDE, MathOperator.ASSIGN_INT_DIVIDE, MathOperator.RASSIGN_INT_DIVIDE);
			addAliases(MOD, MathOperator.MODULO, MathOperator.ASSIGN_MODULO, MathOperator.RASSIGN_MODULO);
			addAliases(ID, 'identity', MathOperator.ASSIGN, MathOperator.RASSIGN);
			addAliases(NEG, 'negate');
			addAliases(SIGN, 'sgn');
			
			addAliases(FLOOR, 'int');
			addAliases(CEIL, 'ceiling');
			addAliases(FIX, 'trunc');
			addAliases(ROUND_TO_DIGITS, 'roundD');
			addAliases(ROUND_TO_NEAREST, 'roundN');
			
			addAliases(LOG_2, 'lb', 'ld');
			addAliases(LOG_10, 'lg');
			
			addAliases(SQUARE, MathOperator.SQUARE, 'sqr');
			addAliases(CUBE, MathOperator.CUBE);
			addAliases(POW, MathOperator.EXPONENTIATE, 'power', MathOperator.ASSIGN_EXPONENTIATE, MathOperator.RASSIGN_EXPONENTIATE);
			
			addAliases(NTH_ROOT, MathOperator.NTH_ROOT, 'root');
			
			/// Logical.
			addAliases(BOOLEAN, 'bool');
			// TODO: Or move to MathOperator and swap with current function names?
			//addAliases(NOT, '¬');
			//addAliases(AND, '∧');
			//addAliases(OR, '∨');
			//addAliases(XOR, '><');
			
			/// Probability.
			addAliases(FAC, MathOperator.FACTORIAL);
			
			addAliases(RANDOM, 'rand');
			addAliases(RANDOM_RANGE, 'randR');
			addAliases(RANDOM_RANGE_INT, 'randI');
			
			/// Relational.
			// TODO: Or move to MathOperator and swap with current function names?
			//addAliases(EQUAL, '==');
			//addAliases(UNEQUAL, '!=');
			//addAliases(LARGER, '>');
			//addAliases(LARGER_EQ, '>=');
			//addAliases(SMALLER, '<');
			//addAliases(SMALLER_EQ, '<=');
			addAliases(COMPARE, 'cmp'/*, '<=>'*/);
			
			/// Statistics.
			
			/// Trigonometry.
			addAliases(COT, 'cotan');
			addAliases(CSC, 'cosec');
			
			addAliases(ARCSIN, 'asin');
			addAliases(ARCCOS, 'acos');
			addAliases(ARCTAN, 'atan');
			addAliases(ARCTAN_YX, 'atanYX', 'arctan2', 'atan2');
			addAliases(ARCCOT, 'acot', 'arccotan', 'acotan');
			addAliases(ARCSEC, 'asec');
			addAliases(ARCCSC, 'acsc', 'arccosec', 'acosec');
			
			addAliases(COTH, 'cotanh');
			addAliases(CSCH, 'cosech');
			
			addAliases(ARCSINH, 'asinh');
			addAliases(ARCCOSH, 'acosh');
			addAliases(ARCTANH, 'atanh');
			addAliases(ARCCOTH, 'acoth', 'arccotanh', 'acotanh');
			addAliases(ARCSECH, 'asech');
			addAliases(ARCCSCH, 'acsch', 'arccosech', 'acosech');
		}
	}
}
