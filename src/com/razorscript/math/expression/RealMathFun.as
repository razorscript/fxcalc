package com.razorscript.math.expression {
	import com.razorscript.math.MathContext;
	import com.razorscript.math.functions.MathLib;
	import com.razorscript.math.functions.mathLib.ArithmeticMath;
	import com.razorscript.math.functions.mathLib.LogicalMath;
	import com.razorscript.math.functions.mathLib.ProbabilityMath;
	import com.razorscript.math.functions.mathLib.RelationalMath;
	import com.razorscript.math.functions.mathLib.StatisticsMath;
	import com.razorscript.math.functions.mathLib.TrigonometryMath;
	
	/**
	 * A faster version of MathFun which only supports real numbers (built-in Number class).
	 *
	 * @author Gene Pavlovsky
	 */
	public class RealMathFun extends MathFun {
		public function RealMathFun(mathContext:MathContext, mathLib:MathLib) {
			super(mathContext, mathLib);
		}
		
		override protected function initFunctions():void {
			var arithmeticMath:ArithmeticMath = _mathLib.arithmeticMath;
			// TODO: Fix links to math functions after all functions are converted to polymorphic functions.
			addFun(MathOperator.ADD, arithmeticMath.addReal);
			addFun(MathOperator.SUBTRACT, arithmeticMath.subtractReal);
			addFun(MathOperator.MULTIPLY, arithmeticMath.multiplyReal);
			addFun(MathOperator.DIVIDE, arithmeticMath.divideReal);
			addFun(DIV, arithmeticMath.divReal);
			addFun(MOD, arithmeticMath.modReal);
			addFun(REM, arithmeticMath.remReal);
			addFun(MathOperator.RECIPROCAL, arithmeticMath.reciprocalReal);
			addFun(ID, arithmeticMath.identity);
			addFun(NEG, arithmeticMath.negateReal);
			addFun(ABS, arithmeticMath.absReal);
			addFun(SIGN, arithmeticMath.signReal);
			
			addFun(FLOOR, arithmeticMath.floorReal);
			addFun(CEIL, arithmeticMath.ceilReal);
			addFun(FIX, arithmeticMath.fixReal);
			addFun(ROUND, arithmeticMath.roundReal);
			addFun(ROUND_TO_DIGITS, arithmeticMath.roundToDigitsReal);
			addFun(ROUND_TO_NEAREST, arithmeticMath.roundToNearestReal);
			
			addFun(CLAMP, arithmeticMath.clampReal);
			
			addFun(LN, arithmeticMath.ln);
			addFun(LOG_2, arithmeticMath.log2);
			addFun(LOG_10, arithmeticMath.log10);
			addFun(LOG_N, arithmeticMath.logN);
			
			addFun(SQUARE, arithmeticMath.square);
			addFun(CUBE, arithmeticMath.cube);
			addFun(POW, arithmeticMath.pow);
			addFun(EXP_10, arithmeticMath.exp10);
			addFun(EXP, arithmeticMath.exp);
			
			addFun(SQRT, arithmeticMath.sqrt);
			addFun(CURT, arithmeticMath.curt);
			addFun(NTH_ROOT, arithmeticMath.nthRoot);
			
			addFun(GCD, arithmeticMath.gcd, 2);
			addFun(LCM, arithmeticMath.lcm, 2);
			
			/// Logical.
			var logicalMath:LogicalMath = _mathLib.logicalMath;
			addFun(BOOLEAN, logicalMath.boolean);
			addFun(NOT, logicalMath.not);
			addFun(AND, logicalMath.and);
			addFun(OR, logicalMath.or);
			addFun(XOR, logicalMath.xor);
			
			/// Probability.
			var probabilityMath:ProbabilityMath = _mathLib.probabilityMath;
			addFun(FAC, probabilityMath.fac);
			addFun(NCR, probabilityMath.ncr);
			addFun(NPR, probabilityMath.npr);
			
			addFun(RANDOM, probabilityMath.random, 0, 1);
			addFun(RANDOM_RANGE, probabilityMath.randomRange, 0, 3);
			addFun(RANDOM_RANGE_INT, probabilityMath.randomRangeInt, 0, 3);
			
			/// Relational.
			var relationalMath:RelationalMath = _mathLib.relationalMath;
			addFun(EQUAL, relationalMath.equal);
			addFun(UNEQUAL, relationalMath.unequal);
			addFun(LARGER, relationalMath.larger);
			addFun(LARGER_EQ, relationalMath.largerEq);
			addFun(SMALLER, relationalMath.smaller);
			addFun(SMALLER_EQ, relationalMath.smallerEq);
			addFun(COMPARE, relationalMath.compare);
			
			/// Statistics.
			var statisticsMath:StatisticsMath = _mathLib.statisticsMath;
			addFun(MIN, statisticsMath.min, 2);
			addFun(MAX, statisticsMath.max, 2);
			
			/// Trigonometry.
			var trigonometryMath:TrigonometryMath = _mathLib.trigonometryMath;
			addFun(SIN, trigonometryMath.sin);
			addFun(COS, trigonometryMath.cos);
			addFun(TAN, trigonometryMath.tan);
			addFun(COT, trigonometryMath.cot);
			addFun(SEC, trigonometryMath.sec);
			addFun(CSC, trigonometryMath.csc);
			
			addFun(ARCSIN, trigonometryMath.asin);
			addFun(ARCCOS, trigonometryMath.acos);
			addFun(ARCTAN, trigonometryMath.atan);
			addFun(ARCTAN_YX, trigonometryMath.atanYX);
			addFun(ARCCOT, trigonometryMath.acot);
			addFun(ARCSEC, trigonometryMath.asec);
			addFun(ARCCSC, trigonometryMath.acsc);
			
			addFun(SINH, trigonometryMath.sinh);
			addFun(COSH, trigonometryMath.cosh);
			addFun(TANH, trigonometryMath.tanh);
			addFun(COTH, trigonometryMath.coth);
			addFun(SECH, trigonometryMath.sech);
			addFun(CSCH, trigonometryMath.csch);
			
			addFun(ARCSINH, trigonometryMath.asinh);
			addFun(ARCCOSH, trigonometryMath.acosh);
			addFun(ARCTANH, trigonometryMath.atanh);
			addFun(ARCCOTH, trigonometryMath.acoth);
			addFun(ARCSECH, trigonometryMath.asech);
			addFun(ARCCSCH, trigonometryMath.acsch);
		}
	}
}
