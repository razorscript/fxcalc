package com.razorscript.math.expression.errors {
	
	/**
	 * Contains information about a single lexer error.
	 *
	 * @author Gene Pavlovsky
	 */
	public class MathLexerErrorItem {
		public function MathLexerErrorItem(message:String, index:int = -1, length:int = -1) {
			this.message = message;
			this.index = index;
			this.length = length;
		}
		
		/** If an error occurs during lexical analysis, contains the error message, otherwise null. */
		public var message:String;
		/** If an error occurs during lexical analysis, and the index of the error in the input (source) can be determined, contains the error index, otherwise -1. */
		public var index:int = -1;
		/** If an error occurs during lexical analysis, and the length of the error in the input (source) can be determined, contains the error length, otherwise -1. */
		public var length:int = -1;
		
		public function toString():String {
			return '[MathLexerErrorItem message="' + message + '" index=' + index + ' length=' + length + ']';
		}
	}
}
