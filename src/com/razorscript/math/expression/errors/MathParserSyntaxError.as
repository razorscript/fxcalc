package com.razorscript.math.expression.errors {
	
	/**
	 * Thrown when a syntax error is encountered in a math expression.
	 * This exception indicates an invalid input, not a MathParser bug, and is always handled by MathParser.
	 *
	 * @see com.razorscript.math.expression.MathParser#parse()
	 * @see com.razorscript.math.expression.MathParser#eval()
	 *
	 * @author Gene Pavlovsky
	 */
	public class MathParserSyntaxError extends Error {
		public function MathParserSyntaxError(message:* = '', id:* = 0) {
			super(message, id);
		}
	}
}
