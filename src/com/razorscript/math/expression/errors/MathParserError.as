package com.razorscript.math.expression.errors {
	import com.razorscript.math.expression.MathExpression;
	
	/**
	 * Thrown when an exceptional condition has occurred during parsing or evaluating a math expression.
	 * This exception indicates a MathParser bug, not an invalid input, which sets the error information properties of a math expression object.
	 *
	 * @author Gene Pavlovsky
	 */
	public class MathParserError extends Error {
		public function MathParserError(expr:MathExpression, message:String, index:int = -1, id:* = 0):void {
			super(message, id);
			this.expr = expr;
			this.index = index;
		}
		
		/** The math expression that caused the exception. */
		public var expr:MathExpression;
		/** If the index of the error in the lexer's input (source) can be determined, contains the error index, otherwise -1. */
		public var index:int;
	}
}
