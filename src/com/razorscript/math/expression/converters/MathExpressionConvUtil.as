package com.razorscript.math.expression.converters {
	import com.razorscript.math.expression.converters.MathTokenConverter;
	import com.razorscript.math.expression.lexers.IMathTokenProvider;
	import com.razorscript.math.expression.tokens.IdentifierToken;
	import com.razorscript.math.expression.tokens.MathToken;
	import com.razorscript.math.expression.tokens.MetaToken;
	import com.razorscript.math.expression.tokens.NumberCharToken;
	import com.razorscript.math.expression.tokens.ValueToken;
	import com.razorscript.math.expression.tokens.OpAssignToken;
	import com.razorscript.math.expression.tokens.OperatorToken;
	import flash.utils.getQualifiedClassName;
	
	/**
	 * Math expression string conversion utility functions.
	 *
	 * @author Gene Pavlovsky
	 */
	public class MathExpressionConvUtil {
		protected static var _instance:MathExpressionConvUtil;
		
		public static function get instance():MathExpressionConvUtil {
			return _instance ? _instance : (_instance = new MathExpressionConvUtil());
		}
		
		public static function set instance(value:MathExpressionConvUtil):void {
			_instance = value;
		}
		
		public static function convertToString(source:*, sep:String = ''):String {
			return instance.convertToString(source, sep);
		}
		
		public static function mathTokenToString(token:MathToken, index:int = 0, source:* = null):String {
			return instance.mathTokenToString(token, index, source);
		}
		
		protected var _mathTokenProviderConverter:MathTokenConverter;
		
		protected function get mathTokenProviderConverter():MathTokenConverter {
			return _mathTokenProviderConverter ? _mathTokenProviderConverter : (_mathTokenProviderConverter = new MathTokenConverter());
		}
		
		public function convertToString(source:*, sep:String = ''):String {
			if (source is String)
				return source;
			else if (source is IMathTokenProvider)
				return mathTokenProviderConverter.convertToString(source as IMathTokenProvider, sep);
			else
				throw new Error('Unsupported conversion from ' + getQualifiedClassName(source) + '.');
		}
		
		public function mathTokenToString(token:MathToken, index:int = 0, source:* = null):String {
			var type:int = token.type;
			if (type == MathToken.VALUE)
				return (token as ValueToken).value.toString();
			else if (type == MathToken.NUMBER_CHAR)
				return (token as NumberCharToken).value;
			else if ((type == MathToken.VARIABLE) || (type == MathToken.CONSTANT))
				return (token as IdentifierToken).name;
			else if (token is OpAssignToken)
				return opAssignTokenToString(token as OpAssignToken);
			else if ((type == MathToken.OPERATOR) || (type == MathToken.FUN_PAREN))
				return (token as OperatorToken).operator;
			else if (type == MathToken.FUN_JUXTA)
				return (token as OperatorToken).operator + ' '; // TODO: Verify if this is the right thing to do in every case.
			else if ((type == MathToken.BLANK) || (type == MathToken.ERROR))
				return (token as MetaToken).value;
			else
				return token.typeShortName;
		}
		
		[Inline]
		protected final function opAssignTokenToString(token:OpAssignToken):String {
			return token.varName ? (token.varName + token.operator) : token.operator;
		}
	}
}
