package com.razorscript.math.expression.converters {
	import com.razorscript.math.MathContext;
	import com.razorscript.math.expression.MathFun;
	import com.razorscript.math.expression.MathOperator;
	import com.razorscript.math.expression.lexers.IMathTokenProvider;
	import com.razorscript.math.expression.tokens.IMathTokenFactory;
	import com.razorscript.math.expression.tokens.MathToken;
	import com.razorscript.math.expression.tokens.MathTokenFactory;
	import com.razorscript.math.expression.tokens.NumberCharToken;
	import com.razorscript.math.expression.tokens.metadata.ErrorMetadata;

	/**
	 * Reads an input string, converts it into a sequence of tokens, and writes them to an IMathTokenProvider.
	 *
	 * @author Gene Pavlovsky
	 */
	public class StringConverter extends ConverterBase {
		/** If specified, numbers are parsed and converted to number tokens. Otherwise, each numeric character is converted to a separate number char token. */
		public static const PARSE_NUMBERS:uint = 1 << uid++;
		/** If specified, whitespace characters are added as blank tokens. Otherwise, whitespace characters are discarded. */
		public static const KEEP_WHITESPACE:uint = 1 << uid++;
		/** If specified, consecutive whitespace characters are merged into multi-character blank tokens. Otherwise, each whitespace character is converted to a separate blank token. */
		public static const MERGE_WHITESPACE:uint = 1 << uid++;
		/** If specified, errors (e.g. unknown identifiers) are added as error tokens. Otherwise, errors are discarded. */
		public static const USE_ERROR_TOKENS:uint = 1 << uid++;

		public static const USE_DEFAULT_OPTIONS:uint = uint(1 << 31);
		public static const DEFAULT_OPTIONS:uint = PARSE_NUMBERS;

		protected static var uid:int = 0;

		public function StringConverter(mathContext:MathContext, mathFun:MathFun, mathOperator:MathOperator, tokenFactory:IMathTokenFactory = null, options:uint = USE_DEFAULT_OPTIONS) {
			super();
			if (options == USE_DEFAULT_OPTIONS)
				options = DEFAULT_OPTIONS;
			parseNumbers = options & PARSE_NUMBERS;
			keepWhitespace = options & KEEP_WHITESPACE;
			mergeWhitespace = options & MERGE_WHITESPACE;
			useErrorTokens = options & USE_ERROR_TOKENS;
			_tokenFactory = tokenFactory ? tokenFactory : new MathTokenFactory();
			_lexer = new StringConverterLexer(mathContext, mathFun, mathOperator, _tokenFactory);
		}

		/**
		 * If true, numbers are parsed and converted to number tokens. Otherwise, each numeric character is converted to a separate number char token.
		 */
		public var parseNumbers:Boolean;

		/**
		 * If true, whitespace characters are added as blank tokens. Otherwise, whitespace characters are discarded.
		 */
		public var keepWhitespace:Boolean;

		/**
		 * If true, consecutive whitespace characters are merged into multi-character blank tokens. Otherwise, each whitespace character is converted to a separate blank token.
		 * Must be used together with keepWhitespace.
		 */
		public var mergeWhitespace:Boolean;

		/**
		 * If true, errors (e.g. unknown identifiers) are added as error tokens. Otherwise, errors are discarded.
		 */
		public var useErrorTokens:Boolean;

		public function get source():String {
			return _lexer.source;
		}

		/**
		 * Reads an input string, converts it into a sequence of tokens, and writes them to an IMathTokenProvider.
		 * @see com.razorscript.math.expression.lexers.StringLexer
		 * @see com.razorscript.math.expression.lexers.IMathTokenProvider
		 *
		 * If there is an error converting the expression, the error information properties contain the error information.
		 *
		 * @param source A string.
		 * @param mathTokenProvider A token provider to write the tokens to.
		 * @param append If true, tokens are appended to the token provider. Otherwise, the token provider is cleared first.
		 * @return The specified token provider.
		 *
		 * @throws MathParserError - if an exceptional condition has occurred during converting the expression.
		 */
		public function convert(source:String, mathTokenProvider:IMathTokenProvider, append:Boolean = false):IMathTokenProvider {
			if (!append)
				mathTokenProvider.clear();

			clearErrors();

			_lexer.parseNumbers = parseNumbers;
			_lexer.keepWhitespace = keepWhitespace;
			_lexer.mergeWhitespace = mergeWhitespace;
			_lexer.source = source;

			var errorMessage:String;
			var errorIndex:int;

			while (true) {
				var index:int = _lexer.index;
				var token:MathToken = _lexer.readToken();
				var hasError:Boolean = _lexer.hasError;

				if (lastHasError && (!hasError || (_lexer.errorMessage != errorMessage))) {
					// Finish tracking an error.
					if (useErrorTokens)
						mathTokenProvider.addToken(_tokenFactory.createMetaToken(MathToken.ERROR, source.substring(errorIndex, index), new ErrorMetadata(errorMessage), errorIndex));
					addError(errorMessage, errorIndex, index - errorIndex);
					lastHasError = false;
				}

				if (token != null) {
					if (token.type == MathToken.NUMBER_CHAR)
						addNumberCharTokens(token as NumberCharToken, mathTokenProvider);
					else
						mathTokenProvider.addToken(token);
				}
				else if (hasError) {
					if (!lastHasError) {
						// Start tracking a new error.
						errorMessage = _lexer.errorMessage;
						errorIndex = index;
					}
					_lexer.stepOverError();
				}
				else {
					break;
				}
				var lastHasError:Boolean = hasError;
			}

			return mathTokenProvider;
		}

		protected function addNumberCharTokens(token:NumberCharToken, mathTokenProvider:IMathTokenProvider):void {
			var isExpectingExpSign:Boolean;
			var index:int = token.index;
			var str:String = token.value;
			var length:int = str.length;
			for (var i:int = 0; i < length; ++i, ++index) {
				var char:String = str.charAt(i);
				if (isExpectingExpSign && ((char == MathOperator.ADD) || (char == MathOperator.SUBTRACT) || (char == MathOperator.SUBTRACT_ALT)))
					mathTokenProvider.addToken(_tokenFactory.createOperatorToken(MathToken.OPERATOR, char, index));
				else
					mathTokenProvider.addToken(_tokenFactory.createNumberCharToken(char, index));
				isExpectingExpSign = (char == 'e');
			}
		}

		protected var _tokenFactory:IMathTokenFactory;
		protected var _lexer:StringConverterLexer;
	}
}
