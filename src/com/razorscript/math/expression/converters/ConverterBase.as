package com.razorscript.math.expression.converters {
	import com.razorscript.math.expression.errors.MathLexerErrorItem;
	
	/**
	 * Abstract base class for math expression source converters.
	 *
	 * @author Gene Pavlovsky
	 */
	public class ConverterBase {
		public function ConverterBase() {
			_errors = new Vector.<MathLexerErrorItem>();
		}
		
		protected var _errors:Vector.<MathLexerErrorItem>;
		
		public function get errors():Vector.<MathLexerErrorItem> {
			return _errors;
		}
		
		public function get numErrors():int {
			return _errors.length;
		}
		
		public function get hasError():Boolean {
			return _errors.length;
		}
		
		public function getErrorAt(index:int):MathLexerErrorItem {
			return _errors[index];
		}
		
		protected function addError(message:String, index:int = -1, length:int = -1):void {
			_errors.push(new MathLexerErrorItem(message, index, length));
		}
		
		protected function clearErrors():void {
			_errors.length = 0;
		}
	}
}
