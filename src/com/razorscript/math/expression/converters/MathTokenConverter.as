package com.razorscript.math.expression.converters {
	import com.razorscript.math.expression.converters.MathExpressionConvUtil;
	import com.razorscript.math.expression.lexers.IMathTokenProvider;

	/**
	 * Reads and converts tokens from an IMathTokenProvider.
	 *
	 * @author Gene Pavlovsky
	 */
	public class MathTokenConverter {
		/**
		 * Reads a math expression from an IMathTokenProvider, and converts it to a string.
		 *
		 * @param source A math token provider.
		 * @param sep Separator string.
		 * @return The string representation of the math expression.
		 */
		public function convertToString(source:IMathTokenProvider, sep:String = ''):String {
			var mathTokenToString:Function = MathExpressionConvUtil.instance.mathTokenToString;
			var str:String = '';
			var length:int = source.length;
			for (var i:int = 0; i < length; ++i) {
				if (sep && str)
					str += sep;
				str += mathTokenToString(source.getTokenAt(i));
			}
			return str;
		}
	}
}
