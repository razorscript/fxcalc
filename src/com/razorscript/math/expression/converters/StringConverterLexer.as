package com.razorscript.math.expression.converters {
	import com.razorscript.math.MathContext;
	import com.razorscript.math.expression.MathFun;
	import com.razorscript.math.expression.MathOperator;
	import com.razorscript.math.expression.lexers.StringLexer;
	import com.razorscript.math.expression.tokens.IMathTokenFactory;
	import com.razorscript.math.expression.tokens.MathToken;
	import com.razorscript.utils.NumUtil;
	
	/**
	 * Reads an input string and converts it into a sequence of tokens.
	 *
	 * Used by StringConverter.
	 * @see com.razorscript.math.expression.converters.StringConverter
	 *
	 * @author Gene Pavlovsky
	 */
	public class StringConverterLexer extends StringLexer {
		protected static var uid:int = 0;
		
		public function StringConverterLexer(mathContext:MathContext = null, mathFun:MathFun = null, mathOperator:MathOperator = null, tokenFactory:IMathTokenFactory = null, options:uint = 0) {
			super(mathContext, mathFun, mathOperator, tokenFactory, options);
		}
		
		public var parseNumbers:Boolean;
		public var keepWhitespace:Boolean;
		public var mergeWhitespace:Boolean;
		
		override public function readToken():MathToken {
			var token:MathToken;
			
			if (queuedToken) {
				// Use the queued token.
				token = queuedToken;
				queuedToken = null;
			}
			else {
				if (keepWhitespace) {
					if (_index >= _length)
						return appendTokenQueue.poll();
					if (_source.charCodeAt(_index) <= 32)
						return readWhitespace();
				}
				else {
					// Skip all whitespace characters.
					while ((_index < _length) && (_source.charCodeAt(_index) <= 32))
						++_index;
					if (_index >= _length)
						return appendTokenQueue.poll();
				}
				
				// Determine the kind of token by the character at the current index, and read the token.
				var char:String = _source.charAt(_index);
				if (isNumber(char))
					token = readNumber();
				else if (isIdentifier(char))
					token = readIdentifier();
				else if (isSpecial(char))
					token = readSpecial(char);
				else if ((token = matchOperator()) == null)
					setError('Unknown token.');
				
				if (queuedToken) {
					// Two tokens were read. Use the queued token and queue the second token.
					var nextToken:MathToken = token;
					token = queuedToken;
					queuedToken = nextToken;
				}
			}
			
			lastToken = token;
			return token;
		}
		
		protected function readWhitespace():MathToken {
			var startIndex:int = _index++;
			if (mergeWhitespace) {
				while ((_index < _length) && (_source.charCodeAt(_index) <= 32))
					++_index;
			}
			return _tokenFactory.createMetaToken(MathToken.BLANK, _source.substring(startIndex, _index), null, startIndex);
		}
		
		override protected function readNumber():MathToken {
			var str:String = matchRegExp(NUMBER_RX);
			var token:MathToken = parseNumbers ? _tokenFactory.createValueToken(NumUtil.parseNumber(str), _index) : _tokenFactory.createNumberCharToken(str, _index);
			_index += str.length;
			return token;
		}
		
		override protected function readVar(str:String, tokenType:int):MathToken {
			var token:MathToken = _tokenFactory.createIdentifierToken(tokenType, str, _index);
			_index += str.length;
			return token;
		}
		
		override protected function readFunction(str:String):MathToken {
			var index:int = _index;
			_index += str.length;
			// Skip all whitespace characters.
			while ((_index < _length) && (_source.charCodeAt(_index) <= 32))
				++_index;
			// If the next non-blank token is a left parenthesis, the operator token is a function call with arguments in parentheses, otherwise it's a function call with a juxtaposed argument.
			var token:MathToken = _tokenFactory.createOperatorToken(((_index < _length) && (_source.charAt(_index) == '(')) ? MathToken.FUN_PAREN : MathToken.FUN_JUXTA, str, index);
			return token;
		}
		
		override protected function readSpecial(char:String):MathToken {
			var token:MathToken = _tokenFactory.createMathToken(SPECIAL_TOKEN_TYPE[char], _index++);
			return token;
		}
		
		override protected function readOperator(str:String):MathToken {
			var token:MathToken;
			if (_mathOperator.getOpInfoByOperator(str).isAssign)
				token = _tokenFactory.createOpAssignToken(str, null, _index);
			else
				token = _tokenFactory.createOperatorToken(MathToken.OPERATOR, str, _index);
			_index += str.length;
			return token;
		}
	}
}
