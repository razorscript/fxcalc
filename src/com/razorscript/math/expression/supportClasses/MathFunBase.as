package com.razorscript.math.expression.supportClasses {
	import com.razorscript.math.MathContext;
	import com.razorscript.math.expression.supportClasses.FunInfo;
	import com.razorscript.math.expression.tokens.OperatorToken;
	import flash.utils.Dictionary;

	/**
	 * Provides methods to create and look up operator info.
	 *
	 * @author Gene Pavlovsky
	 */
	public class MathFunBase {
		public function MathFunBase(mathContext:MathContext) {
			_mathContext = mathContext;
			initialize();
		}

		protected var _mathContext:MathContext;
		/**
		 * The MathContext used when evaluating functions.
		 */
		public function get mathContext():MathContext {
			return _mathContext;
		}

		public function set mathContext(value:MathContext):void {
			_mathContext = value;
		}

		/**
		 * Returns if the function with a specified name is defined.
		 *
		 * @param name A function name.
		 * @return Boolean true if the function is defined.
		 */
		[Inline]
		public final function hasFun(name:String):Boolean {
			return name in _funInfo;
		}

		/**
		 * Returns the FunInfo function info object for a given operator (or function) token.
		 *
		 * @param token An operator (or function) token.
		 * @return The FunInfo function info object.
		 */
		[Inline]
		public final function getFunInfoByToken(token:OperatorToken):FunInfo {
			return _funInfo[token.operator];
		}

		/**
		 * Returns the FunInfo function info object for a given function name.
		 *
		 * @param name A function name.
		 * @return The FunInfo function info object.
		 */
		[Inline]
		public final function getFunInfoByName(name:String):FunInfo {
			return _funInfo[name];
		}

		protected var _funInfo:Object;

		/**
		 * A vector containing the list of function names.
		 */
		public function get names():Vector.<String> {
			var names:Vector.<String> = new Vector.<String>();
			for (var name:String in _funInfo)
				names.push(name);
			return names;
		}

		CONFIG::debug
		protected var _funNames:Dictionary = new Dictionary();

		/**
		 * Create a new function info object and stores it with the specified name as the key.
		 *
		 * @param name The name of the function.
		 * @param fun Function object.
		 * @param minArity Minimum required number of operands for the function. If the value is negative, the Function length property determines both minArity and maxArity.
		 * @param maxArity Maximum allowed number of operands for the function. If the value is negative, maxArity is set to the same value as minArity.
		 */
		CONFIG::release
		public function addFun(name:String, fun:Function, minArity:int = -1, maxArity:int = -1):void {
			_funInfo[name] = new FunInfo(fun, minArity, maxArity);
		}

		CONFIG::debug
		public function addFun(name:String, fun:Function, minArity:int = -1, maxArity:int = -1):void {
			if (!fun)
				throw new Error('Function "' + name + '": the fun argument must not be null.');
			if (fun in _funNames)
				throw new Error('Function "' + name + '" already exists as "' + _funNames[fun] + '", add an alias instead.');
			_funNames[fun] = name;
			if (name in _funInfo)
				throw new Error('Function "' + name + '" already exists.');
			_funInfo[name] = new FunInfo(fun, minArity, maxArity);
		}

		/**
		 * Adds aliases to an existing function name.
		 *
		 * @param name Target function name.
		 * @param ... aliases The alias for the function name.
		 *
		 * @throws Error - if name is undefined.
		 * @throws Error - if an alias is already defined.
		 */
		CONFIG::release
		public function addAliases(name:String, ... aliases):void {
			for each (var alias:String in aliases)
				_funInfo[alias] = _funInfo[name];
		}

		CONFIG::debug
		public function addAliases(name:String, ... aliases):void {
			if (!(name in _funInfo))
				throw new Error('Undefined function "' + name + '".');
			for each (var alias:String in aliases) {
				if (alias in _funInfo)
					throw new Error('Function "' + alias + '" already exists.');
				_funInfo[alias] = _funInfo[name];
			}
		}

		protected function initialize():void {
			_funInfo = {};

			initFunctions();
			initAliases();
		}

		protected function initFunctions():void {
		}

		protected function initAliases():void {
		}
	}
}
