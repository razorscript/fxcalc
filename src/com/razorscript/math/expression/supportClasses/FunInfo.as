package com.razorscript.math.expression.supportClasses {

	/**
	 * Function information.
	 *
	 * @author Gene Pavlovsky
	 */
	public class FunInfo {
		/**
		 * Constructor.
		 *
		 * @param fun Function object.
		 * @param minArity Minimum required number of operands for the function. If the value is negative, the Function length property determines both minArity and maxArity.
		 * @param maxArity Maximum allowed number of operands for the function. If the value is negative, maxArity is set to the same value as minArity.
		 *
		 * @throws RangeError - if maxArity is less than minArity.
		 */
		public function FunInfo(fun:Function, minArity:int = -1, maxArity:int = -1) {
			this.fun = fun;
			if (minArity >= 0) {
				if ((maxArity >= 0) && (maxArity < minArity))
					throw new RangeError('The maxArity argument (' + maxArity + ') must be greater than or equal to minArity argument (' + minArity + ').');
				this.minArity = minArity;
				this.maxArity = (maxArity >= 0) ? maxArity : int.MAX_VALUE;
			}
			else {
				this.minArity = this.maxArity = fun.length;
			}
		}

		/** Function. */
		public var fun:Function;
		/** Minimum required number of arguments to the function. */
		public var minArity:int;
		/** Maximum allowed number of arguments to the function. */
		public var maxArity:int;

		[Inline]
		public final function get isNullary():Boolean {
			return maxArity == 0;
		}

		[Inline]
		public final function get isUnary():Boolean {
			return (minArity == 1) && (maxArity == 1);
		}

		[Inline]
		public final function get isBinary():Boolean {
			return (minArity == 2) && (maxArity == 2);
		}

		[Inline]
		public final function get isVariadic():Boolean {
			return minArity != maxArity;
		}

		public function toString():String {
			return '[FunInfo minArity=' + minArity + ' maxArity=' + maxArity + ']';
		}
	}
}
