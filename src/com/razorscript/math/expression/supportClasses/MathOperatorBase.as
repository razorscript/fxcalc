package com.razorscript.math.expression.supportClasses {
	import com.razorscript.math.expression.supportClasses.OpInfo;
	import com.razorscript.math.expression.tokens.MathToken;
	import com.razorscript.math.expression.tokens.OperatorToken;

	/**
	 * Provides methods to create and look up operator info.
	 *
	 * @author Gene Pavlovsky
	 */
	public class MathOperatorBase {
		public function MathOperatorBase(mathFun:MathFunBase) {
			_mathFun = mathFun;
			initialize();
		}

		protected var _mathFun:MathFunBase;
		/**
		 * The MathFun used when adding operator aliases.
		 */
		public function get mathFun():MathFunBase {
			return _mathFun;
		}

		/**
		 * Returns if the specified operator is defined.
		 *
		 * @param name An operator.
		 * @return Boolean true if the operator is defined.
		 */
		[Inline]
		public final function hasOperator(operator:String):Boolean {
			return operator in _opInfo;
		}

		/**
		 * Returns the OpInfo operator info object for a given operator token.
		 *
		 * @param token An operator token.
		 * @return The OpInfo operator info object.
		 */
		[Inline]
		public final function getOpInfoByToken(token:OperatorToken):OpInfo {
			var type:int = token.type;
			return _opInfo[((type == MathToken.OPERATOR) || (type == MathToken.OP_ASSIGN)) ? token.operator : type];
		}

		/**
		 * Returns the OpInfo operator info object for a given operator.
		 *
		 * @param operator An operator.
		 * @return The OpInfo operator info object.
		 */
		[Inline]
		public final function getOpInfoByOperator(operator:String):OpInfo {
			return _opInfo[operator];
		}

		protected var _opInfo:Object;

		/**
		 * A vector containing the list of operator strings.
		 */
		public function get operators():Vector.<String> {
			var ops:Vector.<String> = new Vector.<String>();
			for (var operator:String in _opInfo)
				ops.push(operator);
			return ops;
		}

		/**
		 * A vector containing the list of regular (non-assignment) operator strings.
		 */
		public function get regularOperators():Vector.<String> {
			var ops:Vector.<String> = new Vector.<String>();
			for (var operator:String in _opInfo) {
				if (!(_opInfo[operator] as OpInfo).isAssign)
					ops.push(operator);
			}
			return ops;
		}

		/**
		 * A vector containing the list of assignment operator strings.
		 */
		public function get assignmentOperators():Vector.<String> {
			var ops:Vector.<String> = new Vector.<String>();
			for (var operator:String in _opInfo) {
				if ((_opInfo[operator] as OpInfo).isAssign)
					ops.push(operator);
			}
			return ops;
		}

		/**
		 * Store the operator info object with the specified operators as keys.
		 *
		 * @param opInfo The operator info object.
		 * @param ... operators The operator.
		 */
		CONFIG::release
		public function setOpInfo(opInfo:OpInfo, ... operators):void {
			for each (var operator:String in operators)
				_opInfo[operator] = opInfo;
		}

		CONFIG::debug
		public function setOpInfo(opInfo:OpInfo, ... operators):void {
			for each (var operator:String in operators) {
				if (operator in _opInfo)
					throw new Error('Operator "' + operator + '" already exists.');
				_opInfo[operator] = opInfo;
			}
		}

		/**
		 * Adds aliases to an existing operator.
		 *
		 * @param name Target operator.
		 * @param ... aliases The alias for the operator.
		 *
		 * @throws Error - if operator is undefined.
		 * @throws Error - if an alias is already defined.
		 */
		CONFIG::release
		public function addAliases(operator:String, ... aliases):void {
			for each (var alias:String in aliases)
				_opInfo[alias] = _opInfo[operator];
			_mathFun.addAliases.apply(null, [operator].concat(aliases));
		}

		CONFIG::debug
		public function addAliases(operator:String, ... aliases):void {
			if (!(operator in _opInfo))
				throw new Error('Undefined operator "' + operator + '".');
			for each (var alias:String in aliases) {
				if (alias in _opInfo)
					throw new Error('Operator "' + alias + '" already exists.');
				_opInfo[alias] = _opInfo[operator];
			}
			_mathFun.addAliases.apply(null, [operator].concat(aliases));
		}

		protected function initialize():void {
			_opInfo = {};
		}
	}
}
