package com.razorscript.math.expression.supportClasses {

	/**
	 * Operator information.
	 *
	 * @author Gene Pavlovsky
	 */
	public class OpInfo {
		public static const LEFT:int = -1;
		public static const RIGHT:int = 1;
		public static const PREFIX:int = -1;
		public static const INFIX:int = 0;
		public static const POSTFIX:int = 1;

		/**
		 * Constructor.
		 *
		 * @param precedence Operator precedence. Larger value means higher precedence.
		 * @param associativity Operator associativity (valid values are the static constants LEFT, RIGHT).
		 * @param position Operator position (valid values are the static constants PREFIX, INFIX, POSTFIX).
		 * @param isAssign Whether the operator is an assignment operator.
		 */
		public function OpInfo(precedence:int, associativity:int = LEFT, position:int = INFIX, isAssign:Boolean = false) {
			prec = precedence;
			ass = associativity;
			pos = position;
			this.isAssign = isAssign;
		}

		/** Operator precedence. Larger value means higher precedence. */
		public var prec:int;
		/** Operator associativity (valid values are the static constants LEFT, RIGHT). */
		public var ass:int;
		/** Operator position (valid values are the static constants PREFIX, INFIX, POSTFIX). */
		public var pos:int;
		/** Whether the operator is an assignment operator. */
		public var isAssign:Boolean;

		[Inline]
		public final function get isPrefix():Boolean {
			return pos == PREFIX;
		}

		[Inline]
		public final function get isInfix():Boolean {
			return pos == INFIX;
		}

		[Inline]
		public final function get isPostfix():Boolean {
			return pos == POSTFIX;
		}

		[Inline]
		public final function get isUnary():Boolean {
			return pos != INFIX;
		}

		[Inline]
		public final function get isBinary():Boolean {
			return isInfix;
		}

		[Inline]
		public final function get isLeftAss():Boolean {
			return ass == LEFT;
		}

		[Inline]
		public final function get isRightAss():Boolean {
			return ass == RIGHT;
		}

		public function toString():String {
			return '[OpInfo prec=' + prec + ' ass=' + ass + ' pos=' + pos + ' isAssign=' + isAssign + ']';
		}
	}
}
