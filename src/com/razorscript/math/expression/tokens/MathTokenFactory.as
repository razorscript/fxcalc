package com.razorscript.math.expression.tokens {
	
	/**
	 * A math token factory that creates new tokens.
	 *
	 * @author Gene Pavlovsky
	 */
	public class MathTokenFactory implements IMathTokenFactory {
		public function MathTokenFactory() {
		}
		
		public function createMathToken(type:int, index:int = -1):MathToken {
			return new MathToken(type, index);
		}
		
		public function createValueToken(value:*, index:int = -1):ValueToken {
			return new ValueToken(value, index);
		}
		
		public function createNumberCharToken(value:String, index:int = -1):NumberCharToken {
			return new NumberCharToken(value, index);
		}
		
		public function createIdentifierToken(type:int, name:String, index:int = -1):IdentifierToken {
			return new IdentifierToken(type, name, index);
		}
		
		public function createOperatorToken(type:int, operator:String, index:int = -1):OperatorToken {
			return new OperatorToken(type, operator, index);
		}
		
		public function createOpAssignToken(operator:String, varName:String, index:int = -1):OpAssignToken {
			return new OpAssignToken(operator, varName, index);
		}
		
		public function createMetaToken(type:int, value:String, data:Object = null, index:int = -1):MetaToken {
			return new MetaToken(type, value, data, index);
		}
		
		public function cloneToken(token:MathToken):MathToken {
			return token.clone();
		}
		
		public function disposeToken(token:MathToken):void {
		}
	}
}
