package com.razorscript.math.expression.tokens {
	import com.razorscript.math.expression.tokens.IdentifierToken;
	import com.razorscript.math.expression.tokens.ValueToken;
	import com.razorscript.math.expression.tokens.MathToken;
	import com.razorscript.math.expression.tokens.MetaToken;
	import com.razorscript.math.expression.tokens.NumberCharToken;
	import com.razorscript.math.expression.tokens.OpAssignToken;
	import com.razorscript.math.expression.tokens.OperatorToken;
	
	/**
	 * A math token factory.
	 * Creates new tokens.
	 *
	 * @author Gene Pavlovsky
	 */
	public interface IMathTokenFactory {
		function createMathToken(type:int, index:int = -1):MathToken;
		function createValueToken(value:*, index:int = -1):ValueToken;
		function createNumberCharToken(value:String, index:int = -1):NumberCharToken;
		function createIdentifierToken(type:int, name:String, index:int = -1):IdentifierToken;
		function createOperatorToken(type:int, operator:String, index:int = -1):OperatorToken;
		function createOpAssignToken(operator:String, varName:String, index:int = -1):OpAssignToken;
		function createMetaToken(type:int, value:String, data:Object = null, index:int = -1):MetaToken;
		
		function cloneToken(token:MathToken):MathToken;
		function disposeToken(token:MathToken):void;
	}
}
