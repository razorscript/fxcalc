package com.razorscript.math.expression.tokens {
	import com.razorscript.razor_internal;
	
	/**
	 * Represents an assignment operator in a math expression.
	 *
	 * @author Gene Pavlovsky
	 */
	public class OpAssignToken extends OperatorToken {
		public function OpAssignToken(operator:String, varName:String, index:int = -1) {
			super(MathToken.OP_ASSIGN, operator, index);
			_varName = varName;
		}
		
		protected var _varName:String;
		/**
		 * The assignment variable name.
		 */
		public function get varName():String {
			return _varName;
		}
		
		override public function clone():MathToken {
			return new OpAssignToken(_operator, _varName, _index);
		}
		
		override public function toString():String {
			return '[OpAssignToken type="' + typeName + '" operator="' + _operator + '" varName="' + _varName + '" index=' + _index + ']';
		}
		
		razor_internal function setOpAssignTokenTo(operator:String, varName:String, index:int):OpAssignToken {
			_operator = operator;
			_varName = varName;
			_index = index;
			return this;
		}
		
		razor_internal function setVarName(varName:String):void {
			_varName = varName;
		}
	}
}
