package com.razorscript.math.expression.tokens {
	import com.razorscript.razor_internal;

	/**
	 * Represents an operator or a function call in a math expression.
	 *
	 * @author Gene Pavlovsky
	 */
	public class OperatorToken extends MathToken {
		/**
		 * Constructor.
		 *
		 * @param type MathToken.OPERATOR for operators, MathToken.OP_ASSIGN for assignment operators, MathToken.FUN_JUXTA for function calls with a juxtaposed argument, MathToken.FUN_PAREN for function calls with arguments in parentheses.
		 * @param operator An operator string for operators, a function name for function calls.
		 * @param index Index of the token in the lexer's input (source).
		 */
		public function OperatorToken(type:int, operator:String, index:int = -1) {
			super(type, index);
			_operator = operator;
		}

		protected var _operator:String;
		/**
		 * The operator string or the function name.
		 */
		public function get operator():String {
			return _operator;
		}

		override public function clone():MathToken {
			return new OperatorToken(_type, _operator, _index);
		}

		override public function toString():String {
			return '[OperatorToken type="' + typeName + '" operator="' + _operator + '" index=' + _index + ']';
		}

		razor_internal function setOperatorTokenTo(type:int, operator:String, index:int):OperatorToken {
			_type = type;
			_operator = operator;
			_index = index;
			return this;
		}
	}
}
