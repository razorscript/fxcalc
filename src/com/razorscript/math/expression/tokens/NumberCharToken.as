package com.razorscript.math.expression.tokens {
	import com.razorscript.razor_internal;
	
	/**
	 * Represents a character which is a part of a number (digit, radix point or lowercase exponent indicator) in a math expression.
	 * MathTokenLexer merges consecutive NumberCharToken objects into a ValueToken.
	 * The value should match the /^[0-9.e]$/ RegExp.
	 *
	 * @author Gene Pavlovsky
	 */
	public class NumberCharToken extends MathToken {
		public function NumberCharToken(value:String, index:int = -1) {
			super(MathToken.NUMBER_CHAR, index);
			_value = value;
		}
		
		protected var _value:String;
		/**
		 * The character which is a part of a number (digit, radix point or lowercase exponent indicator). Should match the /^[0-9.e]$/ RegExp.
		 */
		public function get value():String {
			return _value;
		}
		
		override public function clone():MathToken {
			return new NumberCharToken(_value, _index);
		}
		
		override public function toString():String {
			return '[NumberCharToken type="' + typeName + '" value=' + _value + ' index=' + _index + ']';
		}
		
		razor_internal function setNumberCharTokenTo(value:String, index:int):NumberCharToken {
			_value = value;
			_index = index;
			return this;
		}
	}
}
