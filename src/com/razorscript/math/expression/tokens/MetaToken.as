package com.razorscript.math.expression.tokens {
	import com.razorscript.razor_internal;
	import com.razorscript.utils.StrUtil;
	
	/**
	 * Represents a part of a math expression not related to it's evaluation, e.g. whitespace characters, annotations or error descriptions.
	 *
	 * @author Gene Pavlovsky
	 */
	public class MetaToken extends MathToken {
		public function MetaToken(type:int, value:String, data:Object = null, index:int = -1) {
			super(type, index);
			_value = value;
			_data = data;
		}
		
		protected var _value:String;
		/**
		 * The string value of the token, e.g. whitespace characters.
		 */
		public function get value():String {
			return _value;
		}
		
		protected var _data:Object;
		/**
		 * Additional data.
		 */
		public function get data():Object {
			return _data;
		}
		
		override public function clone():MathToken {
			return new MetaToken(_type, _value, _data, _index);
		}
		
		override public function toString():String {
			return '[MetaToken type="' + typeName + '" value="' + _value + '"' + (data ? (' data=' + StrUtil.objectToString(data)) : '') + ' index=' + _index + ']';
		}
		
		razor_internal function setMetaTokenTo(type:int, value:String, data:Object, index:int):MetaToken {
			_type = type;
			_value = value;
			_data = data;
			_index = index;
			return this;
		}
	}
}
