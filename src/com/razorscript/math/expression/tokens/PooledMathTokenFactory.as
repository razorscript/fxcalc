package com.razorscript.math.expression.tokens {
	import com.razorscript.debug.Debug;
	import com.razorscript.razor_internal;
	
	use namespace razor_internal;
	
	/**
	 * A math token factory that uses token pooling, with a separate pool for each token class.
	 * If the pool is empty, a new token is created, otherwise a token is taken from the pool.
	 * Disposed tokens are returned to the pool.
	 *
	 * @author Gene Pavlovsky
	 */
	public class PooledMathTokenFactory implements IMathTokenFactory {
		public function PooledMathTokenFactory() {
			mathTokenPool = new Vector.<MathToken>();
			valueTokenPool = new Vector.<MathToken>();
			numberCharTokenPool = new Vector.<MathToken>();
			identifierTokenPool = new Vector.<MathToken>();
			operatorTokenPool = new Vector.<MathToken>();
			opAssignTokenPool = new Vector.<MathToken>();
			metaTokenPool = new Vector.<MathToken>();
			
			typePoolMap = new Vector.<Vector.<MathToken>>(MathToken.uid);
			typePoolMap[MathToken.UNKNOWN] = mathTokenPool;
			typePoolMap[MathToken.VALUE] = valueTokenPool;
			typePoolMap[MathToken.NUMBER_CHAR] = numberCharTokenPool;
			typePoolMap[MathToken.VARIABLE] = identifierTokenPool;
			typePoolMap[MathToken.CONSTANT] = identifierTokenPool;
			typePoolMap[MathToken.OPERATOR] = operatorTokenPool;
			typePoolMap[MathToken.OP_ASSIGN] = opAssignTokenPool;
			typePoolMap[MathToken.FUN_JUXTA] = operatorTokenPool;
			typePoolMap[MathToken.FUN_PAREN] = operatorTokenPool;
			typePoolMap[MathToken.LEFT_PAREN] = mathTokenPool;
			typePoolMap[MathToken.RIGHT_PAREN] = mathTokenPool;
			typePoolMap[MathToken.LEFT_BRACKET] = mathTokenPool;
			typePoolMap[MathToken.RIGHT_BRACKET] = mathTokenPool;
			typePoolMap[MathToken.LEFT_CURLY] = mathTokenPool;
			typePoolMap[MathToken.RIGHT_CURLY] = mathTokenPool;
			typePoolMap[MathToken.LIST_SEP] = mathTokenPool;
			typePoolMap[MathToken.BLANK] = metaTokenPool;
			typePoolMap[MathToken.ERROR] = metaTokenPool;
		}
		
		public function createMathToken(type:int, index:int = -1):MathToken {
			var pool:Vector.<MathToken> = mathTokenPool;
			if (pool.length)
				return pool.pop().setMathTokenTo(type, index);
			return new MathToken(type, index);
		}
		
		public function createValueToken(value:*, index:int = -1):ValueToken {
			var pool:Vector.<MathToken> = valueTokenPool;
			if (pool.length)
				return (pool.pop() as ValueToken).setValueTokenTo(value, index);
			return new ValueToken(value, index);
		}
		
		public function createNumberCharToken(value:String, index:int = -1):NumberCharToken {
			var pool:Vector.<MathToken> = numberCharTokenPool;
			if (pool.length)
				return (pool.pop() as NumberCharToken).setNumberCharTokenTo(value, index);
			return new NumberCharToken(value, index);
		}
		
		public function createIdentifierToken(type:int, name:String, index:int = -1):IdentifierToken {
			var pool:Vector.<MathToken> = identifierTokenPool;
			if (pool.length)
				return (pool.pop() as IdentifierToken).setIdentifierTokenTo(type, name, index);
			return new IdentifierToken(type, name, index);
		}
		
		public function createOperatorToken(type:int, operator:String, index:int = -1):OperatorToken {
			var pool:Vector.<MathToken> = operatorTokenPool;
			if (pool.length)
				return (pool.pop() as OperatorToken).setOperatorTokenTo(type, operator, index);
			return new OperatorToken(type, operator, index);
		}
		
		public function createOpAssignToken(operator:String, varName:String, index:int = -1):OpAssignToken {
			var pool:Vector.<MathToken> = opAssignTokenPool;
			if (pool.length)
				return (pool.pop() as OpAssignToken).setOpAssignTokenTo(operator, varName, index);
			return new OpAssignToken(operator, varName, index);
		}
		
		public function createMetaToken(type:int, value:String, data:Object = null, index:int = -1):MetaToken {
			var pool:Vector.<MathToken> = metaTokenPool;
			if (pool.length)
				return (pool.pop() as MetaToken).setMetaTokenTo(type, value, data, index);
			return new MetaToken(type, value, data, index);
		}
		
		public function cloneToken(token:MathToken):MathToken {
			if (token is ValueToken) {
				var valueToken:ValueToken = token as ValueToken;
				return createValueToken(valueToken.value, valueToken.index);
			}
			else if (token is NumberCharToken) {
				var numberCharToken:NumberCharToken = token as NumberCharToken;
				return createNumberCharToken(numberCharToken.value, numberCharToken.index);
			}
			else if (token is IdentifierToken) {
				var identifierToken:IdentifierToken = token as IdentifierToken;
				return createIdentifierToken(identifierToken.type, identifierToken.name, identifierToken.index);
			}
			else if (token is OpAssignToken) {
				var opAssignToken:OpAssignToken = token as OpAssignToken;
				return createOpAssignToken(opAssignToken.operator, opAssignToken.varName, opAssignToken.index);
			}
			else if (token is OperatorToken) {
				var operatorToken:OperatorToken = token as OperatorToken;
				return createOperatorToken(operatorToken.type, operatorToken.operator, operatorToken.index);
			}
			else if (token is MetaToken) {
				var metaToken:MetaToken = token as MetaToken;
				return createMetaToken(metaToken.type, metaToken.value, metaToken.data, metaToken.index);
			}
			else {
				return createMathToken(token.type, token.index);
			}
		}
		
		CONFIG::release
		public function disposeToken(token:MathToken):void {
			typePoolMap[token.type].push(token);
		}
		
		CONFIG::debug
		public function disposeToken(token:MathToken):void {
			var pool:Vector.<MathToken> = typePoolMap[token.type];
			if (pool.indexOf(token) != -1)
				throw new Error('Token (' + token + ') is already in the pool.');
			pool.push(token);
			if (Debug.instance.debugPooling)
				Debug.instance.setMathTokenPoolSize(getPoolName(pool), pool.length);
		}
		
		protected var mathTokenPool:Vector.<MathToken>;
		protected var valueTokenPool:Vector.<MathToken>;
		protected var numberCharTokenPool:Vector.<MathToken>;
		protected var identifierTokenPool:Vector.<MathToken>;
		protected var operatorTokenPool:Vector.<MathToken>;
		protected var opAssignTokenPool:Vector.<MathToken>;
		protected var metaTokenPool:Vector.<MathToken>;
		
		CONFIG::debug
		protected function getPoolName(pool:Vector.<MathToken>):String {
			switch (pool) {
				case mathTokenPool: return 'MathToken';
				case valueTokenPool: return 'ValueToken';
				case numberCharTokenPool: return 'NumberCharToken';
				case identifierTokenPool: return 'IdentifierToken';
				case operatorTokenPool: return 'OperatorToken';
				case opAssignTokenPool: return 'OpAssignToken';
				case metaTokenPool: return 'MetaToken';
				default: return 'Unknown';
			}
		}
		
		/**
		 * Maps a token type to a token pool.
		 */
		protected var typePoolMap:Vector.<Vector.<MathToken>>;
	}
}
