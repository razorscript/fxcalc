package com.razorscript.math.expression.tokens {
	import com.razorscript.razor_internal;

	use namespace razor_internal;

	/**
	 * Represents a token in a math expression. Used directly for tokens which are defined by type alone. Tokens that also have a value are implemented as subclasses.
	 *
	 * @author Gene Pavlovsky
	 */
	public class MathToken {
		public static const UNKNOWN:int = 			registerType('unset', 'unset');
		public static const VALUE:int = 				registerType('value', 'value');
		public static const NUMBER_CHAR:int =		registerType('num-c', 'number-char');
		public static const VARIABLE:int = 			registerType('var', 	'variable');
		public static const CONSTANT:int = 			registerType('const', 'constant');
		public static const OPERATOR:int = 			registerType('op', 		'operator');
		public static const OP_ASSIGN:int = 		registerType('op=', 	'operator-assign');
		public static const FUN_JUXTA:int = 		registerType('fun_', 	'function-juxta');
		public static const FUN_PAREN:int = 		registerType('fun(', 	'function-paren');
		public static const LEFT_PAREN:int = 		registerType('(', 		'left-paren');
		public static const RIGHT_PAREN:int = 	registerType(')', 		'right-paren');
		public static const LEFT_BRACKET:int = 	registerType('[', 		'left-bracket');
		public static const RIGHT_BRACKET:int = registerType(']', 		'right-bracket');
		public static const LEFT_CURLY:int = 		registerType('{', 		'left-curly');
		public static const RIGHT_CURLY:int = 	registerType('}', 		'right-curly');
		public static const LIST_SEP:int = 			registerType(',',			'list-sep');
		public static const BLANK:int = 				registerType('_',			'blank');
		public static const ERROR:int = 				registerType('err',		'error');

		razor_internal static var uid:int = 0;

		protected static var typeNames:Vector.<String>;
		protected static var typeShortNames:Vector.<String>;

		/**
		 * Registers a custom token type and returns it's unique identifier.
		 *
		 * @param typeString The name of the token type.
		 * @param typeStringShort The short name of the token type.
		 * @return A unique token type identifier.
		 */
		public static function registerType(typeShortName:String, typeName:String):int {
			if (!uid) {
				typeShortNames = new Vector.<String>();
				typeNames = new Vector.<String>();
			}
			var id:int = uid++;
			typeShortNames[id] = typeShortName;
			typeNames[id] = typeName;
			return id;
		}

		public static function getTypeShortName(type:int):String {
			return typeShortNames[type];
		}

		public static function getTypeName(type:int):String {
			return typeNames[type];
		}

		public function MathToken(type:int, index:int = -1) {
			_type = type;
			_index = index;
		}

		protected var _type:int;
		/**
		 * The unique token type identifier. Valid values are defined as static constants.
		 * @see com.razorscript.math.expression.tokens.MathToken
		 */
		public function get type():int {
			return _type;
		}

		/**
		 * The token type short name. Useful for testing and error reporting. Used by MathTokenLexer.getBracketCharAt().
		 * @see com.razorscript.math.expression.lexers.MathTokenLexer#getBracketCharAt()
		 */
		public function get typeShortName():String {
			return typeShortNames[_type];
		}

		/**
		 * The token type name. Useful for testing and error reporting.
		 */
		public function get typeName():String {
			return typeNames[_type];
		}

		protected var _index:int;
		/**
		 * The index of the token in the lexer's input (source). Useful for testing and error reporting.
		 */
		public function get index():int {
			return _index;
		}

		public function clone():MathToken {
			return new MathToken(_type, _index);
		}

		public function toString():String {
			return '[MathToken type="' + typeName + '" index=' + _index + ']';
		}

		razor_internal function setMathTokenTo(type:int, index:int):MathToken {
			_type = type;
			_index = index;
			return this;
		}

		razor_internal function setIndex(index:int):void {
			_index = index;
		}
	}
}
