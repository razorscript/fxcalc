package com.razorscript.math.expression.tokens.metadata {
	
	/**
	 * Error meta token data.
	 *
	 * @author Gene Pavlovsky
	 */
	public class ErrorMetadata {
		public function ErrorMetadata(message:String) {
			this.message = message;
		}
		
		/** The error message. */
		public var message:String;
		
		public function toString():String {
			return '[ErrorMetaData message="' + message + '"]';
		}
	}
}
