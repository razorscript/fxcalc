package com.razorscript.math.expression.tokens {
	import com.razorscript.razor_internal;
	
	/**
	 * Represents a mathematical object in a math expression.
	 *
	 * @author Gene Pavlovsky
	 */
	public class ValueToken extends MathToken {
		public function ValueToken(value:*, index:int = -1) {
			super(MathToken.VALUE, index);
			_value = value;
		}
		
		protected var _value:*;
		/**
		 * The value of the token.
		 */
		public function get value():* {
			return _value;
		}
		
		override public function clone():MathToken {
			return new ValueToken(_value, _index);
		}
		
		override public function toString():String {
			return '[ValueToken type="' + typeName + '" value=' + _value + ' index=' + _index + ']';
		}
		
		razor_internal function setValueTokenTo(value:*, index:int):ValueToken {
			_value = value;
			_index = index;
			return this;
		}
		
		[Inline]
		razor_internal final function transformValue(valueTransform:Function):void {
			_value = valueTransform(_value);
		}
	}
}
