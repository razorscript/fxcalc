package com.razorscript.math.expression.tokens {
	import com.razorscript.razor_internal;
	
	/**
	 * Represents an identifier (variable or a constant) in a math expression.
	 *
	 * @author Gene Pavlovsky
	 */
	public class IdentifierToken extends MathToken {
		public function IdentifierToken(type:int, name:String, index:int = -1) {
			super(type, index);
			_name = name;
		}
		
		protected var _name:String;
		/**
		 * The identifier's name.
		 */
		public function get name():String {
			return _name;
		}
		
		override public function clone():MathToken {
			return new IdentifierToken(_type, _name, _index);
		}
		
		override public function toString():String {
			return '[IdentifierToken type="' + typeName + '" name="' + _name + '" index=' + _index + ']';
		}
		
		razor_internal function setIdentifierTokenTo(type:int, name:String, index:int):IdentifierToken {
			_type = type;
			_name = name;
			_index = index;
			return this;
		}
	}
}
