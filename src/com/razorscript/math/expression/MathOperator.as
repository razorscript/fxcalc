package com.razorscript.math.expression {
	import com.razorscript.math.expression.supportClasses.MathOperatorBase;
	import com.razorscript.math.expression.supportClasses.OpInfo;
	import com.razorscript.math.expression.tokens.MathToken;

	/**
	 * Provides operator info for math operators.
	 * Defines constants for operator string values.
	 *
	 * @author Gene Pavlovsky
	 */
	public class MathOperator extends MathOperatorBase {
		/**
		 * Prefix operators are implemented as function calls with a juxtaposed argument (MathToken.FUN_JUXTA).
		 */

		/**
		 * Infix operators.
		 */
		public static const ADD:String = '+';
		public static const SUBTRACT:String = '-';
		public static const SUBTRACT_ALT:String = '−';
		public static const MULTIPLY:String = '*';
		public static const MULTIPLY_ALT:String = '×';
		public static const MULTIPLY_IMPLICIT_VAR:String = '·'; // Implicit multiplication (by juxtaposition) with 2nd operand being a constant or a variable.
		public static const MULTIPLY_IMPLICIT:String = '◦'; // Implicit multiplication (by juxtaposition) with 2nd operand being a sub-expression, a prefix unary operator or a function.
		public static const DIVIDE:String = '/';
		public static const DIVIDE_ALT:String = '÷';
		public static const INT_DIVIDE:String = '\\';
		public static const MODULO:String = '%';
		public static const EXPONENTIATE:String = '^'; // Right-associative.
		public static const NTH_ROOT:String = 'ⁿ√';
		public static const FRACTION:String = '⁄'/*'⌟'*/; // Right-associative. TODO: Is it?
		public static const ERROR:String = '±';
		public static const PERCENTAGE:String = 'ṗ';
		public static const NPR:String = 'Ṗ';
		public static const NCR:String = 'Ċ';
		//public static const COMPLEX_POLAR:String = '∠';

		/**
		 * Postfix operators.
		 */
		public static const RECIPROCAL:String = '⁻¹';
		public static const SQUARE:String = '²';
		public static const CUBE:String = '³';
		public static const FACTORIAL:String = '!';
		//public static const DEGREE:String = 'Ď';
		//public static const RADIAN:String = 'Ř';
		//public static const GRAD:String = 'Ğ';

		/**
		 * Right-associative (regular) assignment operators. The value of right-associative compound assignment operators is the result of the assignment operator function.
		 **/
		public static const ASSIGN:String = '=';
		public static const ASSIGN_ADD:String = '+=';
		public static const ASSIGN_SUBTRACT:String = '-=';
		public static const ASSIGN_MULTIPLY:String = '*=';
		public static const ASSIGN_DIVIDE:String = '/=';
		public static const ASSIGN_INT_DIVIDE:String = '\\=';
		public static const ASSIGN_MODULO:String = '%=';
		public static const ASSIGN_EXPONENTIATE:String = '^=';

		/**
		 * Left-associative (L-value on the right) assignment operators. The value of left-associative compound assignment operators is the original value (operator argument).
		 */
		public static const RASSIGN:String = '→';
		public static const RASSIGN_ADD:String = '→+';
		public static const RASSIGN_SUBTRACT:String = '→-';
		public static const RASSIGN_MULTIPLY:String = '→*';
		public static const RASSIGN_DIVIDE:String = '→/';
		public static const RASSIGN_INT_DIVIDE:String = '→\\';
		public static const RASSIGN_MODULO:String = '→%';
		public static const RASSIGN_EXPONENTIATE:String = '→^';

		public function MathOperator(mathFun:MathFun) {
			super(mathFun);
		}

		protected static const LEFT:int = OpInfo.LEFT;
		protected static const RIGHT:int = OpInfo.RIGHT;
		protected static const PREFIX:int = OpInfo.PREFIX;
		protected static const INFIX:int = OpInfo.INFIX;
		protected static const POSTFIX:int = OpInfo.POSTFIX;

		override protected function initialize():void {
			super.initialize();

			// Parenthetical sub-expressions.
			setOpInfo(new OpInfo(14));

			// Explicit function calls with parentheses e.g. sin() Rec().
			setOpInfo(new OpInfo(13, RIGHT, PREFIX), MathToken.FUN_PAREN);

			// Postfix unary functions - square, cube, reciprocal, factorial, DMS, engineering symbols, DRG conversions, metric conversions.
			setOpInfo(new OpInfo(12, LEFT, POSTFIX), FACTORIAL, RECIPROCAL, SQUARE, CUBE);

			// Powers and roots (x^y, x-root-y).
			setOpInfo(new OpInfo(11, RIGHT), EXPONENTIATE, NTH_ROOT);

			// Fractions.
			setOpInfo(new OpInfo(10, RIGHT), FRACTION);

			// Implicit multiplication with 2nd operand being a constant or a variable - e.g. ln 3 e = ln (3 * e), sin 2 PI = sin (2 * PI), sin 2x cos 2x = (sin(2*x)) * (cos(2*x))
			setOpInfo(new OpInfo(9, LEFT), MULTIPLY_IMPLICIT_VAR);

			// Prefix unary functions - sin, sqrt, log, ln, exp_10, exp_e, base-n symbols (d, h, b, o), Neg, Not, Det, Trn, arg, Abs, Conjg.
			setOpInfo(new OpInfo(8, RIGHT, PREFIX), MathToken.FUN_JUXTA);

			// Implicit multiplication with 2nd operand being a sub-expression, a prefix unary operator or a function e.g. A log 2, 2 sqrt 3, sin 2(1+3) = sin 2 * (1+3), sin A(1+3) = (sin A) * (1+3), sin 2x cos 2x = (sin (2*x)) * (cos (2*x))
			setOpInfo(new OpInfo(7, LEFT), MULTIPLY_IMPLICIT);

			// nPr, nCr, complex number polar coordinate symbol.
			setOpInfo(new OpInfo(6, LEFT), NCR, NPR);

			// Dot product.
			//setOpInfo(new OpInfo(5, LEFT));

			// Multiplication, division.
			setOpInfo(new OpInfo(4, LEFT), MULTIPLY, DIVIDE, INT_DIVIDE, MODULO);
			addAliases(MULTIPLY, MULTIPLY_ALT);
			addAliases(DIVIDE, DIVIDE_ALT);

			// Addition, subtraction.
			setOpInfo(new OpInfo(3, LEFT), ADD, SUBTRACT);
			addAliases(SUBTRACT, SUBTRACT_ALT);

			// Logical AND.
			//setOpInfo(new OpInfo(2, LEFT));

			// Logical OR, XOR, XNOR.
			//setOpInfo(new OpInfo(1, LEFT));

			// Assignments.
			setOpInfo(new OpInfo(0, RIGHT, INFIX, true), ASSIGN, ASSIGN_ADD, ASSIGN_SUBTRACT, ASSIGN_MULTIPLY, ASSIGN_DIVIDE, ASSIGN_INT_DIVIDE, ASSIGN_MODULO, ASSIGN_EXPONENTIATE);
			setOpInfo(new OpInfo(0, LEFT, INFIX, true), RASSIGN, RASSIGN_ADD, RASSIGN_SUBTRACT, RASSIGN_MULTIPLY, RASSIGN_DIVIDE, RASSIGN_INT_DIVIDE, RASSIGN_MODULO, RASSIGN_EXPONENTIATE);
		}
	}
}
