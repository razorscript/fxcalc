package com.razorscript.math.expression {
	import com.razorscript.razor_internal;
	import com.razorscript.math.expression.converters.MathExpressionConvUtil;
	import com.razorscript.math.expression.errors.MathParserError;
	import com.razorscript.math.expression.tokens.IMathTokenFactory;
	import com.razorscript.math.expression.tokens.MathToken;

	use namespace razor_internal;

	/**
	 * Represents a mathematical expression used by MathParser.
	 * Contains the results of parsing and evaluating the expression. If an error occurs, contains the error information.
	 *
	 * @see com.razorscript.math.expression.MathParser#parse()
	 * @see com.razorscript.math.expression.MathParser#eval()
	 *
	 * @author Gene Pavlovsky
	 */
	public class MathExpression {
		/**
		 * Returns a MathExpression containing the specified value.
		 * If expr is not null, stores the result in expr, otherwise creates a new MathExpression.
		 * Useful for custom result reporting and testing.
		 *
		 * @param value A value.
		 * @param expr A MathExpression object to store the result in. If null, a new MathExpression object is created.
		 * @return A new MathExpression containing the specified value.
		 */
		public static function fromValue(value:*, expr:MathExpression = null):MathExpression {
			if (!expr)
				expr = new MathExpression();
			else
				expr.reset();
			expr._value = value;
			return expr;
		}

		/**
		 * Returns a MathExpression containing the specified error information.
		 * If expr is not null, stores the result in expr, otherwise creates a new MathExpression.
		 * Useful for custom error reporting and testing.
		 *
		 * @param source An expression (data type depends on the lexer property).
		 * @param type The error type.
		 * @param message The error message.
		 * @param index The index of the error in the source.
		 * @param expr A MathExpression object to store the result in. If null, a new MathExpression object is created.
		 * @return A new MathExpression containing the specified error information.
		 */
		public static function fromError(source:*, type:int = 0, message:String = null, index:int = -1, expr:MathExpression = null):MathExpression {
			if (!expr)
				expr = new MathExpression(source);
			else
				expr.reset(source);
			expr._errorType = type;
			expr._errorMessage = message;
			expr._errorIndex = index;
			return expr;
		}

		protected var _source:*;
		/**
		 * The original expression.
		 */
		public function get source():* {
			return _source;
		}

		protected var _value:*;
		/**
		 * The result of evaluating the expression.
		 */
		public function get value():* {
			return _value;
		}

		// TODO: Add errorID property everywhere.

		protected var _errorType:int;
		/**
		 * If an error occurs during parsing or evaluating, contains the error type, otherwise 0.
		 */
		public function get errorType():int {
			return _errorType;
		}

		protected var _errorMessage:String;
		/**
		 * If an error occurs during parsing or evaluating, contains the error message, otherwise null.
		 */
		public function get errorMessage():String {
			return _errorMessage;
		}

		protected var _errorIndex:int = -1;
		/**
		 * If an error occurs during parsing or evaluating, and the index of the error in the lexer's input (source) can be determined, contains the error index, otherwise -1.
		 */
		public function get errorIndex():int {
			return _errorIndex;
		}

		/**
		 * Returns true if there was an error parsing or evaluating the expression.
		 */
		public function get hasError():Boolean {
			return _errorType;
		}

		public static const SYNTAX_ERROR:int = 1;
		public static const MATH_ERROR:int = 2;

		protected static const ERROR_NAMES:Vector.<String> = new <String>['Success', 'Syntax error', 'Math error'];

		/**
		 * If an error occurs during parsing or evaluating, contains the error name, otherwise null.
		 */
		public function get errorName():String {
			return ERROR_NAMES[_errorType];
		}

		protected var _mathParser:MathParser;

		/**
		 * The MathParser used to parse and evaluate this expression.
		 */
		public function get mathParser():MathParser {
			return _mathParser;
		}

		/**
		 * The expression parsed into RPN.
		 */
		razor_internal var rpn:Vector.<MathToken>;

		public function MathExpression(source:* = null, mathParser:MathParser = null) {
			_source = source;
			_mathParser = mathParser;
		}

		public function reset(source:* = null, mathParser:MathParser = null):void {
			disposeRPN();
			_source = source;
			_mathParser = mathParser;
			_value = NaN; // TODO: Refactor using NaN and isNaN(expr.value) (if used anywhere). Use either null or add a new mathematical object containing additional error info / original exception object?
			_errorType = 0;
			_errorMessage = null;
			_errorIndex = -1;
		}

		/**
		 * Disposes of all tokens in the rpn vector. After calling this method the expression can't be evaluated again.
		 */
		public function disposeRPN():void {
			if (!rpn)
				return;
			var tokenFactory:IMathTokenFactory = _mathParser.tokenFactory;
			for (var i:int = rpn.length - 1; i >= 0; --i)
				tokenFactory.disposeToken(rpn[i]);
			rpn = null;
		}

		/**
		 * Evaluates this expression using the same MathParser that was used to parse it.
		 * If there is an error evaluating the expression, the error information properties contain the error information.
		 *
		 * If an expression is to be parsed and evaluated only once, consider using MathParser.eval().
		 * @see com.razorscript.math.expression.MathParser#eval()
		 *
		 * @return This object.
		 *
		 * @throws MathParserError - if an exceptional condition has occurred during evaluating the expression.
		 */
		public function eval():MathExpression {
			if (!rpn)
				throw new MathParserError(this, 'Unparsed expression.');
			_mathParser._eval(this);
			return this;
		}

		razor_internal function setValue(value:*):void {
			_value = value;
		}

		/**
		 * Sets the error information properties.
		 *
		 * @param type The error type.
		 * @param message The error message.
		 * @param index The index of the error in the source.
		 */
		razor_internal function setError(type:int = 0, message:String = null, index:int = -1):MathExpression {
			_value = NaN;
			_errorType = type;
			_errorMessage = message;
			_errorIndex = index;
			return this;
		}

		public function clone():MathExpression {
			var expr:MathExpression = new MathExpression(_source, _mathParser);
			expr._value = _value;
			expr._errorType = _errorType;
			expr._errorMessage = _errorMessage;
			expr._errorIndex = _errorIndex;
			expr.rpn = rpn;
			return expr;
		}

		public function toString():String {
			return '[MathExpression' + (_source ? (' source="' + MathExpressionConvUtil.convertToString(_source) + '"') : '') + ' value=' + _value +
				(hasError ? (' errorName="' + errorName + '" errorMessage="' + _errorMessage + '" errorIndex=' + _errorIndex) : '') + ']';
		}
	}
}
