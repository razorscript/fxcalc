package com.razorscript.math.expression.lexers {
	import com.razorscript.math.expression.converters.MathExpressionConvUtil;
	import com.razorscript.math.expression.tokens.IMathTokenFactory;
	import com.razorscript.math.expression.tokens.MathToken;
	
	/**
	 * An implementation of IMathTokenProvider that stores math tokens in an array.
	 *
	 * @author Gene Pavlovsky
	 */
	public class MathTokenArrayMathTokenProvider implements IMathTokenProvider {
		public function MathTokenArrayMathTokenProvider(source:Array/*MathToken*/ = null) {
			_source = source ? source : [];
		}
		
		protected var _source:Array/*MathToken*/;
		/**
		 * The input array of MathToken objects.
		 *
		 * @throws ArgumentError - if value is not an Array.
		 */
		public function get source():* {
			return _source;
		}
		
		public function set source(value:*):void {
			if (!(value is Array))
				throw new ArgumentError('The argument (' + value + ') is not an Array.');
			_source = value;
		}
		
		public function get length():int {
			return _source.length;
		}
		
		public function setSource(source:* = null):IMathTokenProvider {
			this.source = source ? source : [];
			return this;
		}
		
		public function getTokenAt(index:int):MathToken {
			return _source[index];
		}
		
		public function addToken(token:MathToken):void {
			_source.push(token);
		}
		
		public function getItemAt(index:int):* {
			return _source[index];
		}
		
		public function addItem(item:*):void {
			_source.push(item);
		}
		
		public function clear():void {
			_source.length = 0;
		}
		
		public function toString():String {
			return '[MathTokenArrayMathTokenProvider source="' + MathExpressionConvUtil.convertToString(this) + '" length=' + length +	']';
		}
	}
}
