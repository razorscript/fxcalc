package com.razorscript.math.expression.lexers {
	import com.razorscript.math.MathContext;
	import com.razorscript.math.expression.MathFun;
	import com.razorscript.math.expression.MathOperator;
	import com.razorscript.math.expression.lexers.IMathLexer;
	import com.razorscript.math.expression.lexers.LexerBase;
	import com.razorscript.math.expression.supportClasses.OpInfo;
	import com.razorscript.math.expression.tokens.IMathTokenFactory;
	import com.razorscript.math.expression.tokens.MathToken;
	import com.razorscript.utils.NumUtil;

	/**
	 * Reads an input string and converts it into a sequence of tokens.
	 *
	 * @author Gene Pavlovsky
	 */
	public class StringLexer extends LexerBase implements IMathLexer {
		/** If specified, numbers "." are treated as "0". Otherwise, they are reported as errors. */
		public static const NUMBER_ALLOW_SOLO_RADIX:uint = 1 << (LexerBase.uid + uid++);

		protected static var uid:int = 0;

		public function StringLexer(mathContext:MathContext = null, mathFun:MathFun = null, mathOperator:MathOperator = null, tokenFactory:IMathTokenFactory = null, options:uint = 0) {
			super(mathContext, mathFun, mathOperator, tokenFactory, options);
			numberAllowSoloRadix = options & NUMBER_ALLOW_SOLO_RADIX;
		}

		/**
		 * If true, numbers "." are treated as "0". Otherwise, they are reported as errors.
		 */
		public var numberAllowSoloRadix:Boolean;

		override public function setEnvironment(mathContext:MathContext = null, mathFun:MathFun = null, mathOperator:MathOperator = null, tokenFactory:IMathTokenFactory = null):void {
			var hasMathOperatorChanged:Boolean = _mathOperator != mathOperator;
			super.setEnvironment(mathContext, mathFun, mathOperator, tokenFactory);
			if (hasMathOperatorChanged)
				updateOpMaxLength();
		}

		override public function set mathOperator(value:MathOperator):void {
			if (_mathOperator == value)
				return;
			_mathOperator = value;
			updateOpMaxLength();
		}

		protected var _source:String;
		/**
		 * The input string.
		 *
		 * @throws ArgumentError - if value is not a String.
		 */
		public function get source():* {
			return _source;
		}

		public function set source(value:*):void {
			if (!(value is String))
				throw new ArgumentError('The argument (' + value + ') is not a String.');
			_source = value;
			initSource(_source.length);
		}

		/**
		 * @see com.razorscript.math.expression.lexers.LexerBase#checkBrackets()
		 */
		override protected function getBracketCharAt(index:int):String {
			return _source.charAt(index);
		}

		/**
		 * Reads and returns a token at the current index in the input string.
		 * Sets the current index to the index of the character after the last character of the token.
		 * If there are no more tokens left, returns null.
		 * If an error occurs, sets the errorMessage property and returns null.
		 *
		 * @return The next token, or null if no more tokens left or an error has occurred.
		 */
		public function readToken():MathToken {
			var token:MathToken;

			if (queuedToken) {
				// Use the queued token.
				token = queuedToken;
				queuedToken = null;
			}
			else {
				// Skip all whitespace characters.
				while ((_index < _length) && (_source.charCodeAt(_index) <= 32))
					++_index;
				if (_index >= _length)
					return appendTokenQueue.poll();

				// Determine the kind of token by the character at the current index, and read the token.
				var char:String = _source.charAt(_index);
				if (isNumber(char))
					token = readNumber();
				else if (isIdentifier(char))
					token = readIdentifier();
				else if (isSpecial(char))
					token = readSpecial(char);
				else if ((token = matchOperator()) == null)
					setError('Unknown token.');

				if (queuedToken) {
					// Two tokens were read. Use the queued token and queue the second token.
					var nextToken:MathToken = token;
					token = queuedToken;
					queuedToken = nextToken;
				}
			}

			lastToken = token;
			return token;
		}

		protected static const NUMBER_CHARS:String = '0123456789.';
		protected static const IDENTIFIER_CHARS:String = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		protected static const ID_FOLLOW_CHARS:String = IDENTIFIER_CHARS + '_0123456789';
		protected static const SPECIAL_CHARS:String = '(),'; // '()[]{},';

		protected static const SPECIAL_TOKEN_TYPE:Object = {
			'(':MathToken.LEFT_PAREN, ')':MathToken.RIGHT_PAREN,
			'[':MathToken.LEFT_BRACKET, ']':MathToken.RIGHT_BRACKET,
			'{':MathToken.LEFT_CURLY, '}':MathToken.RIGHT_CURLY,
			',':MathToken.LIST_SEP};

		protected static const NUMBER_RX:RegExp = /\d* \.? \d* (?: e [-+]? \d+)?/xig; // TODO: How to read complex numbers, uncertainties, DMS angles, fractions?
		protected static const IDENTIFIER_RX:RegExp = /[a-zA-Z] \w*/xg;

		[Inline]
		protected final function isNumber(char:String):Boolean {
			return NUMBER_CHARS.indexOf(char) != -1;
		}

		[Inline]
		protected final function isIdentifier(char:String):Boolean {
			return IDENTIFIER_CHARS.indexOf(char) != -1;
		}

		[Inline]
		protected final function isSpecial(char:String):Boolean {
			return SPECIAL_CHARS.indexOf(char) != -1;
		}

		protected var opMaxLength:int;

		protected function updateOpMaxLength():void {
			opMaxLength = 1;
			if (_mathOperator) {
				var ops:Vector.<String> = _mathOperator.operators;
				for (var i:int = ops.length - 1; i >= 0; --i) {
					if (ops[i].length > opMaxLength)
						opMaxLength = ops[i].length;
				}
			}
		}

		/**
		 * Executes the rx RegExp on the input string, starting from the current index, and returns the complete matching substring if a match is found at the current index, otherwise returns null.
		 *
		 * @param rx The RegExp to execute.
		 * @return The matching substring if a match is found at the current index, otherwise null.
		 */
		protected function matchRegExp(rx:RegExp):String {
			rx.lastIndex = _index;
			var rxMatch:Object = rx.exec(_source);
			return (rxMatch && (rxMatch.index == _index)) ? rxMatch[0] : null;
		}

		/**
		 * Parses a number and returns a mathematical object token.
		 *
		 * @return A mathematical object token (ValueToken).
		 */
		protected function readNumber():MathToken {
			if (isExpectingInfixOp)
				return setError('Unexpected number.');
			var str:String = matchRegExp(NUMBER_RX);
			if ((str == '.') || (str.substr(0, 2).toLowerCase() == '.e')) {
				if (!numberAllowSoloRadix)
					return setError('Invalid number.');
				str = '0' + str;
			}
			// The character at the current index is one of NUMBER_CHARS, matchRegExp(NUMBER_RX) is guaranteed to return a match - no need for error checking.
			var token:MathToken = _tokenFactory.createValueToken(NumUtil.parseNumber(str), _index);
			_index += str.length;
			isExpectingInfixOp = true;
			return token;
		}

		/**
		 * Reads an identifier and returns an appropriate kind of token depending on the kind of identifier.
		 * First checks if the identifier is a variable name, then a constant name, finally a function name.
		 * If the identifier is a variable name, returns a variable identifier token.
		 * If the identifier is a constant name, returns a constant identifier token.
		 * If the identifier is a function name, returns an operator (function call) token.
		 * If implicit multiplication is detected, queues an implicit multiplication operator token (OperatorToken).
		 *
		 * @return An identifier token (IdentifierToken) or an operator (function call) token (OperatorToken).
		 */
		protected function readIdentifier():MathToken {
			var index:int = _index + 1;
			while ((index < _length) && (ID_FOLLOW_CHARS.indexOf(_source.charAt(index)) != -1))
				++index;
			var str:String = _source.substring(_index, index);
			index = str.length;
			while (true) {
				if (_mathContext.hasVariable(str))
					return readVar(str, MathToken.VARIABLE);
				else if (_mathContext.hasConstant(str))
					return readVar(str, MathToken.CONSTANT);
				else if (_mathFun.hasFun(str))
					return readFunction(str);

				if (--index == 0)
					break;
				str = str.substr(0, index);
			}
			return setError('Unknown identifier.');
		}

		/**
		 * Returns an identifier token with the specified name and type.
		 * If implicit multiplication is detected, queues an implicit multiplication operator token (OperatorToken).
		 *
		 * @param str The identifier name.
		 * @param type The type of identifier token to return.
		 * @return An identifier token (IdentifierToken).
		 */
		protected function readVar(str:String, type:int):MathToken {
			if (isExpectingInfixOp)
				queuedToken = _tokenFactory.createOperatorToken(MathToken.OPERATOR, MathOperator.MULTIPLY_IMPLICIT_VAR, _index);
			var token:MathToken = _tokenFactory.createIdentifierToken(type, str, _index);
			_index += str.length;
			isExpectingInfixOp = true;
			return token;
		}

		/**
		 * Returns an operator (function call) with the specified function name.
		 * If implicit multiplication is detected, queues an implicit multiplication operator token (OperatorToken).
		 *
		 * @param str The function name.
		 * @return An operator (function call) token (OperatorToken).
		 */
		protected function readFunction(str:String):MathToken {
			if (isExpectingInfixOp)
				queuedToken = _tokenFactory.createOperatorToken(MathToken.OPERATOR, MathOperator.MULTIPLY_IMPLICIT, _index);
			var index:int = _index;
			_index += str.length;
			// Skip all whitespace characters.
			while ((_index < _length) && (_source.charCodeAt(_index) <= 32))
				++_index;
			// If the next non-blank token is a left parenthesis, the operator token is a function call with arguments in parentheses, otherwise it's a function call with a juxtaposed argument.
			var token:MathToken = _tokenFactory.createOperatorToken(((_index < _length) && (_source.charAt(_index) == '(')) ? MathToken.FUN_PAREN : MathToken.FUN_JUXTA, str, index);
			isExpectingInfixOp = false;
			return token;
		}

		/**
		 * Reads a character and returns an appropriate kind of token depending on the character.
		 * If the character is not an operator, returns a math token.
		 * If the character is an operator, returns an operator token.
		 * If implicit multiplication is detected, queues an implicit multiplication operator token (OperatorToken).
		 *
		 * @param char The character to read.
		 * @return A token (MathToken) if the character is not an operator, an operator token (OperatorToken).
		 */
		protected function readSpecial(char:String):MathToken {
			var type:int = SPECIAL_TOKEN_TYPE[char];
			if (isExpectingInfixOp) {
				if (type == MathToken.LEFT_PAREN)
					queuedToken = _tokenFactory.createOperatorToken(MathToken.OPERATOR, MathOperator.MULTIPLY_IMPLICIT, _index);
			}
			else {
				if (type == MathToken.LIST_SEP)
					return setError('Unexpected comma.');
				else if ((type == MathToken.RIGHT_PAREN) && (lastToken.type != MathToken.LEFT_PAREN)) // A right parenthesis can't be the first token, so lastToken is not null.
					return setError('Unexpected right parenthesis.');
			}
			var token:MathToken = _tokenFactory.createMathToken(type, _index++);
			isExpectingInfixOp = type == MathToken.RIGHT_PAREN; /*(type == MathToken.RIGHT_PAREN) || (type == MathToken.RIGHT_BRACKET) || (type == MathToken.RIGHT_CURLY);*/
			return token;
		}

		/**
		 * Reads an operator and returns an appropriate kind of token depending on the kind of operator.
		 * If the operator is an assignment operator, returns an assignment operator token.
		 * If the operator is a regular operator, returns an operator token.
		 *
		 * @return An operator token (OperatorToken), or an implicit multiplication operator token (OperatorToken) if implicit multiplication is detected.
		 */
		protected function matchOperator():MathToken {
			var str:String = _source.substr(_index, opMaxLength);
			var length:int = str.length;
			while (true) {
				if (_mathOperator.hasOperator(str))
					return readOperator(str);

				if (--length == 0)
					break;
				str = str.substr(0, length);
			}
			return null;
		}

		/**
		 * Returns an assignment operator token if an infix operator was expected and the specified operator is a regular operator.
		 * Returns an operator token if an infix operator was expected and the specified operator is a regular operator.
		 * Returns an operator (function call) token if an operator wasn't expected and the specified operator is a unary minus (negation function) or a unary plus (identity function).
		 * Prefix operators are implemented as function calls with a juxtaposed argument (MathToken.FUN_JUXTA).
		 *
		 * @param str The operator.
		 * @return An assigment operator token (OpAssignToken), an operator token (OperatorToken) or an operator (function call) token (OperatorToken).
		 */
		protected function readOperator(str:String):MathToken {
			var token:MathToken;
			var opInfo:OpInfo = _mathOperator.getOpInfoByOperator(str);
			if (isExpectingInfixOp) {
				if (opInfo.isAssign) {
					token = _tokenFactory.createOpAssignToken(str, null, _index);
					isExpectingInfixOp = false;
				}
				else {
					token = _tokenFactory.createOperatorToken(MathToken.OPERATOR, str, _index);
					isExpectingInfixOp = opInfo.isPostfix;
				}
			}
			else if (!opInfo.isAssign && (str in INFIX_TO_PREFIX_OP)) {
				token = _tokenFactory.createOperatorToken(MathToken.FUN_JUXTA, INFIX_TO_PREFIX_OP[str], _index);
				//isExpectingInfixOp = false; // Already false.
			}
			else {
				return setError('Unexpected operator.');
			}
			_index += str.length;
			return token;
		}
	}
}
