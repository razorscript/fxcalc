package com.razorscript.math.expression.lexers {
	import com.razorscript.math.expression.tokens.MathToken;
	
	/**
	 * A math token provider for MathTokenLexer.
	 * Reads tokens from a data source.
	 *
	 * @author Gene Pavlovsky
	 */
	public interface IMathTokenProvider {
		function get source():*;
		function set source(value:*):void;
		function get length():int;
		function setSource(source:* = null):IMathTokenProvider;
		function getTokenAt(index:int):MathToken;
		function addToken(token:MathToken):void;
		function getItemAt(index:int):*;
		function addItem(item:*):void;
		function clear():void;
	}
}
