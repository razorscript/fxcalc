package com.razorscript.math.expression.lexers {
	import com.razorscript.math.expression.MathFun;
	import com.razorscript.math.expression.MathOperator;
	import com.razorscript.math.expression.tokens.IMathTokenFactory;
	import com.razorscript.math.expression.tokens.MathToken;
	import com.razorscript.math.MathContext;
	
	/**
	 * A math expression lexer.
	 * Reads an expression and returns it as a sequence of tokens.
	 *
	 * @author Gene Pavlovsky
	 */
	public interface IMathLexer {
		function get mathContext():MathContext;
		function set mathContext(value:MathContext):void;
		function get mathFun():MathFun;
		function set mathFun(value:MathFun):void;
		function get mathOperator():MathOperator;
		function set mathOperator(value:MathOperator):void;
		function get tokenFactory():IMathTokenFactory;
		function set tokenFactory(value:IMathTokenFactory):void;
		
		function get source():*;
		function set source(value:*):void;
		function get index():int;
		function get length():int;
		function get errorMessage():String;
		function get hasError():Boolean;
		
		function get lenientBracketChecking():Boolean;
		function set lenientBracketChecking(value:Boolean):void;
		
		function setEnvironment(mathContext:MathContext = null, mathFun:MathFun = null, mathOperator:MathOperator = null, tokenFactory:IMathTokenFactory = null):void;
		function checkBrackets():Boolean;
		function readToken():MathToken;
	}
}
