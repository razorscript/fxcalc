package com.razorscript.math.expression.lexers {
	import com.razorscript.math.expression.converters.MathExpressionConvUtil;
	import com.razorscript.math.expression.tokens.IMathTokenFactory;
	import com.razorscript.math.expression.tokens.MathToken;
	
	/**
	 * An implementation of IMathTokenProvider that stores math tokens in a vector.
	 *
	 * @author Gene Pavlovsky
	 */
	public class MathTokenVectorMathTokenProvider implements IMathTokenProvider {
		public function MathTokenVectorMathTokenProvider(source:Vector.<MathToken> = null) {
			_source = source ? source : new Vector.<MathToken>();
		}
		
		protected var _source:Vector.<MathToken>;
		/**
		 * The input vector of MathToken objects.
		 *
		 * @throws ArgumentError - if value is not a Vector.<MathToken>.
		 */
		public function get source():* {
			return _source;
		}
		
		public function set source(value:*):void {
			if (!(value is Vector.<MathToken>))
				throw new ArgumentError('The argument (' + value + ') is not a Vector.<MathToken>.');
			_source = value;
		}
		
		public function get length():int {
			return _source.length;
		}
		
		public function setSource(source:* = null):IMathTokenProvider {
			this.source = source ? source : new Vector.<MathToken>();
			return this;
		}
		
		public function getTokenAt(index:int):MathToken {
			return _source[index];
		}
		
		public function addToken(token:MathToken):void {
			_source.push(token);
		}
		
		public function getItemAt(index:int):* {
			return _source[index];
		}
		
		public function addItem(item:*):void {
			_source.push(item);
		}
		
		public function clear():void {
			var _fixed:Boolean = _source.fixed;
			_source.fixed = false;
			_source.length = 0;
			_source.fixed = _fixed;
		}
		
		public function toString():String {
			return '[MathTokenVectorMathTokenProvider source="' + MathExpressionConvUtil.convertToString(this) + '" length=' + length +	']';
		}
	}
}
