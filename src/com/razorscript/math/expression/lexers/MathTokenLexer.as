package com.razorscript.math.expression.lexers {
	import com.razorscript.razor_internal;
	import com.razorscript.math.MathExt;
	import com.razorscript.math.expression.MathOperator;
	import com.razorscript.math.expression.lexers.IMathLexer;
	import com.razorscript.math.expression.lexers.IMathTokenProvider;
	import com.razorscript.math.expression.lexers.LexerBase;
	import com.razorscript.math.expression.tokens.IMathTokenFactory;
	import com.razorscript.math.expression.tokens.MathToken;
	import com.razorscript.math.expression.tokens.MetaToken;
	import com.razorscript.math.expression.tokens.NumberCharToken;
	import com.razorscript.math.expression.tokens.OperatorToken;
	import com.razorscript.math.expression.tokens.metadata.ErrorMetadata;
	import com.razorscript.utils.NumUtil;

	use namespace razor_internal;

	/**
	 * Reads and sanitizes tokens from an IMathTokenProvider.
	 *
	 * @author Gene Pavlovsky
	 */
	public class MathTokenLexer extends LexerBase implements IMathLexer {
		/** If specified, numbers like "e±XXX" are treated as "1e±XXX". Otherwise, they are reported as errors. */
		public static const NUMBER_ALLOW_LEADING_EXP:uint = 1 << (LexerBase.uid + uid++);
		/** If specified, numbers "." are treated as "0". Otherwise, they are reported as errors. */
		public static const NUMBER_ALLOW_SOLO_RADIX:uint = 1 << (LexerBase.uid + uid++);

		protected static var uid:int = 0;

		public function MathTokenLexer(mathOperator:MathOperator = null, tokenFactory:IMathTokenFactory = null, options:uint = 0) {
			super(null, null, mathOperator, tokenFactory, options);
			numberAllowLeadingExp = options & NUMBER_ALLOW_LEADING_EXP;
			numberAllowSoloRadix = options & NUMBER_ALLOW_SOLO_RADIX;
		}

		/**
		 * If true, numbers like "e±XXX" are treated as "1e±XXX". Otherwise, they are reported as errors.
		 */
		public var numberAllowLeadingExp:Boolean;

		/**
		 * If true, numbers "." are treated as "0". Otherwise, they are reported as errors.
		 */
		public var numberAllowSoloRadix:Boolean;

		protected var _source:IMathTokenProvider;
		/**
		 * The math token provider.
		 *
		 * @throws ArgumentError - if value is not an IMathTokenProvider.
		 */
		public function get source():* {
			return _source;
		}

		public function set source(value:*):void {
			if (!(value is IMathTokenProvider))
				throw new ArgumentError('The argument (' + value + ') is not an IMathTokenProvider.');
			_source = value;
			initSource(_source.length);
		}

		/**
		 * @see com.razorscript.math.expression.lexers.LexerBase#checkBrackets()
		 */
		override protected function getBracketCharAt(index:int):String {
			return _source.getTokenAt(index).typeShortName;
		}

		/**
		 * Reads and returns a token at the current index in the input array.
		 * Sets the current index to the index of the character after the last character of the token.
		 * If there are no more tokens left, returns null.
		 * If an error occurs, sets the errorMessage property and returns null.
		 *
		 * @return The next token, or null if no more tokens left or an error has occurred.
		 */
		public function readToken():MathToken {
			var token:MathToken;

			if (queuedToken) {
				// Use the queued token.
				token = queuedToken;
				queuedToken = null;
			}
			else {
				var type:int;
				// Skip all blank tokens.
				while ((_index < _length) && ((type = _source.getTokenAt(_index).type) == MathToken.BLANK))
					++_index;
				if (_index >= _length)
					return appendTokenQueue.poll();

				// Read the token.
				if (type == MathToken.VALUE)
					token = isExpectingInfixOp ? setError('Unexpected number.') : _readToken(true);
				else if (type == MathToken.NUMBER_CHAR)
					token = readNumberChars();
				else if ((type == MathToken.VARIABLE) || (type == MathToken.CONSTANT))
					token = _readToken(true, MathOperator.MULTIPLY_IMPLICIT_VAR);
				else if (type == MathToken.OPERATOR)
					token = readOperator();
				else if (type == MathToken.OP_ASSIGN)
					token = isExpectingInfixOp ? _readToken(false) : setError('Unexpected operator.');
				else if (type == MathToken.FUN_JUXTA)
					token = readFunJuxta();
				else if ((type == MathToken.FUN_PAREN) || (type == MathToken.LEFT_PAREN))
					token = _readToken(false, MathOperator.MULTIPLY_IMPLICIT);
				else if (type == MathToken.RIGHT_PAREN) /*((type == MathToken.RIGHT_PAREN) || (type == MathToken.RIGHT_BRACKET) || (type == MathToken.RIGHT_CURLY))*/
					token = (isExpectingInfixOp || (lastToken.type == MathToken.LEFT_PAREN)) ? _readToken(true) : setError('Unexpected right parenthesis.'); // A right parenthesis can't be the first token, so lastToken is not null.
				else if (type == MathToken.LIST_SEP)
					token = isExpectingInfixOp ? _readToken(false) : setError('Unexpected comma.');
				else if (type == MathToken.ERROR)
					readError();
				else
					setError('Unknown token.');

				if (queuedToken) {
					// Two tokens were read. Use the queued token and queue the second token.
					var nextToken:MathToken = token;
					token = queuedToken;
					queuedToken = nextToken;
				}
			}

			lastToken = token;
			return token;
		}

		/**
		 * Returns a token at the current index in the input array.
		 *
		 * @param willExpectInfixOp The value to set the isExpectingInfixOp property to.
		 * @param implicitOperator If not null, the value of operator token to queue if an infix operator is expected.
		 * @return The next token, or an implicit operator token (OperatorToken) if the implicitOperatorToken argument is not null and an infix operator is expected.
		 */
		protected function _readToken(willExpectInfixOp:Boolean, implicitOperator:String = null):MathToken {
			if (implicitOperator && isExpectingInfixOp)
				queuedToken = _tokenFactory.createOperatorToken(MathToken.OPERATOR, implicitOperator, _index);
			var token:MathToken = _tokenFactory.cloneToken(_source.getTokenAt(_index));
			token.setIndex(_index++);
			isExpectingInfixOp = willExpectInfixOp;
			return token;
		}

		protected static const NUMBER_RX:RegExp = /^ \d* \.? \d* (?: e [-+]? \d+)? $/x; // TODO: How to read complex numbers, uncertainties, DMS angles, fractions?

		/**
		 * Merges consecutive NumberCharToken objects and returns a mathematical object token.
		 *
		 * @return A mathematical object token (ValueToken).
		 */
		protected function readNumberChars():MathToken {
			// Implicit multiplication with 2nd operand being a number is not allowed.
			if (isExpectingInfixOp)
				return setError('Unexpected number.');

			// Concatenate values of consecutive NumberCharToken objects.
			var i:int = _index;
			var str:String = (_source.getTokenAt(i++) as NumberCharToken).value;
			var isExpectingExpSign:Boolean = str == 'e';
			if (isExpectingExpSign) {
				if (!numberAllowLeadingExp)
					return setError('Invalid number.');
				str = '1' + str;
			}
			while (i < _length) {
				var token:MathToken = _source.getTokenAt(i);
				if (isExpectingExpSign && (token.type == MathToken.OPERATOR)) {
					var operator:String = (token as OperatorToken).operator;
					if ((operator != MathOperator.ADD) && (operator != MathOperator.SUBTRACT) && (operator != MathOperator.SUBTRACT_ALT))
						return setError('Invalid number.');
					str += (operator == MathOperator.SUBTRACT_ALT) ? MathOperator.SUBTRACT : operator;
					isExpectingExpSign = false;
				}
				else if (token is NumberCharToken) {
					var char:String = (token as NumberCharToken).value;
					str += char;
					isExpectingExpSign = char == 'e';
				}
				else {
					break;
				}
				++i;
			}

			if ((str == '.') || (str.substr(0, 2) == '.e')) {
				if (!numberAllowSoloRadix)
					return setError('Invalid number.');
				str = '0' + str;
			}
			// Test if the number is valid.
			if (NUMBER_RX.test(str)) {
				var value:Number = NumUtil.parseNumber(str);
				if (MathExt.isNaN(value))
					throw new Error('Failed to parse a supposedly valid number: "' + str + '".');
				token = _tokenFactory.createValueToken(value, _index);
				_index = i;
				isExpectingInfixOp = true;
				return token;
			}
			return setError('Invalid number.');
		}

		/**
		 * Returns an operator token if an infix operator was expected and the identifier at the current index is an operator.
		 * Returns an operator (function call) token if an operator wasn't expected and the operator is a unary minus (negation function) or a unary plus (identity function).
		 * Prefix operators are implemented as function calls with a juxtaposed argument (MathToken.FUN_JUXTA).
		 *
		 * @return An operator token (OperatorToken), an operator (function call) token (OperatorToken) if an operator wasn't expected and the identifier is a minus or a plus, otherwise null.
		 */
		protected function readOperator():MathToken {
			var token:MathToken = _source.getTokenAt(_index);
			var operator:String = (token as OperatorToken).operator;
			if (isExpectingInfixOp) {
				token = _tokenFactory.cloneToken(token);
				token.setIndex(_index++);
				isExpectingInfixOp = _mathOperator.getOpInfoByOperator(operator).isPostfix;
			}
			else if (operator in INFIX_TO_PREFIX_OP) {
				token = _tokenFactory.createOperatorToken(MathToken.FUN_JUXTA, INFIX_TO_PREFIX_OP[operator], _index++);
				//isExpectingInfixOp = false; // Already false.
			}
			else {
				return setError('Unexpected operator.');
			}
			return token;
		}

		/**
		 * Returns an operator (function call) token. Peeks at the next token to see if the operator token is a function call with arguments in parentheses.
		 *
		 * @return An operator (function call) token (OperatorToken), or an implicit multiplication operator token (OperatorToken) if implicit multiplication is detected.
		 */
		protected function readFunJuxta():MathToken {
			var token:MathToken = _readToken(false, MathOperator.MULTIPLY_IMPLICIT);
			if (token.type == MathToken.FUN_JUXTA) {
				var type:int;
				// Skip all blank tokens.
				while ((_index < _length) && ((type = _source.getTokenAt(_index).type) == MathToken.BLANK))
					++_index;
				if ((_index < _length) && (type == MathToken.LEFT_PAREN)) {
					// The next non-blank token is a left parenthesis, the operator token is a function call with arguments in parentheses.
					var newToken:MathToken = _tokenFactory.createOperatorToken(MathToken.FUN_PAREN, (token as OperatorToken).operator, token.index);
					_tokenFactory.disposeToken(token);
					token = newToken;
				}
			}
			return token;
		}

		/**
		 * Reads an error token. Sets the errorMessage property to the error message contained in the token's data.
		 */
		protected function readError():void {
			var token:MathToken = _source.getTokenAt(_index);
			var data:ErrorMetadata = (token as MetaToken).data as ErrorMetadata;
			setError(data ? data.message : 'Unspecified error.');
		}
	}
}
