package com.razorscript.math.expression.lexers {
	import com.razorscript.ds.Queue;
	import com.razorscript.ds.Stack;
	import com.razorscript.math.MathContext;
	import com.razorscript.math.expression.MathFun;
	import com.razorscript.math.expression.MathOperator;
	import com.razorscript.math.expression.errors.MathLexerErrorItem;
	import com.razorscript.math.expression.tokens.IMathTokenFactory;
	import com.razorscript.math.expression.tokens.MathToken;

	/**
	 * Abstract base class for lexers.
	 *
	 * @author Gene Pavlovsky
	 */
	public class LexerBase {
		/** If specified, unmatched opening brackets are automatically closed. Otherwise, they are reported as errors. */
		public static const LENIENT_BRACKET_CHECKING:uint = 1 << uid++;

		protected static var uid:int = 0;

		public function LexerBase(mathContext:MathContext = null, mathFun:MathFun = null, mathOperator:MathOperator = null, tokenFactory:IMathTokenFactory = null, options:uint = 0) {
			_lenientBracketChecking = options & LENIENT_BRACKET_CHECKING;
			setEnvironment(mathContext, mathFun, mathOperator, tokenFactory);
			appendTokenQueue = new Queue();
			stack = new Stack();
		}

		protected var _lenientBracketChecking:Boolean;
		/**
		 * If true, unmatched opening brackets are automatically closed. Otherwise, they are reported as errors.
		 */
		public function get lenientBracketChecking():Boolean {
			return _lenientBracketChecking;
		}

		public function set lenientBracketChecking(value:Boolean):void {
			_lenientBracketChecking = value;
		}

		public function setEnvironment(mathContext:MathContext = null, mathFun:MathFun = null, mathOperator:MathOperator = null, tokenFactory:IMathTokenFactory = null):void {
			_mathContext = mathContext;
			_mathFun = mathFun;
			_mathOperator = mathOperator;
			_tokenFactory = tokenFactory;
		}

		protected var _mathContext:MathContext;
		/**
		 * The MathContext for classifying identifiers (matching variable and constant names).
		 */
		public function get mathContext():MathContext {
			return _mathContext;
		}

		public function set mathContext(value:MathContext):void {
			_mathContext = value;
		}

		protected var _mathFun:MathFun;
		/**
		 * The MathFun for classifying identifiers (matching function names).
		 */
		public function get mathFun():MathFun {
			return _mathFun;
		}

		public function set mathFun(value:MathFun):void {
			_mathFun = value;
		}

		protected var _mathOperator:MathOperator;
		/**
		 * The MathOperator used for determining operator position.
		 */
		public function get mathOperator():MathOperator {
			return _mathOperator;
		}

		public function set mathOperator(value:MathOperator):void {
			_mathOperator = value;
		}

		protected var _tokenFactory:IMathTokenFactory;
		/**
		 * The math token factory used to create new tokens.
		 */
		public function get tokenFactory():IMathTokenFactory {
			return _tokenFactory;
		}

		public function set tokenFactory(value:IMathTokenFactory):void {
			_tokenFactory = value;
		}

		protected function initSource(length:int):void {
			_index = 0;
			_length = length;
			_errorMessage = null;
			isExpectingInfixOp = false;
			appendTokenQueue.clear();
		}

		protected var _index:int;
		/**
		 * The current index in the input (source).
		 */
		public function get index():int {
			return _index;
		}

		protected var _length:int;
		/**
		 * The length of the input (source).
		 */
		public function get length():int {
			return _length;
		}

		protected var _errorMessage:String;
		/**
		 * If an error occurs during lexical analysis, contains the error message, otherwise null.
		 */
		public function get errorMessage():String {
			return _errorMessage;
		}

		/**
		 * If an error occurs during lexical analysis, contains the lexer error message and index, otherwise null.
		 */
		public function get errorItem():MathLexerErrorItem {
			return _errorMessage ? new MathLexerErrorItem(_errorMessage, _index) : null;
		}

		/**
		 * Whether or not an error has occurred.
		 */
		public function get hasError():Boolean {
			return _errorMessage;
		}

		/**
		 * Resets the error status and increments the current index in the input (source).
		 */
		public function stepOverError():void {
			if (_errorMessage) {
				_errorMessage = null;
				++_index;
			}
		}

		protected static const BRACKET_NAME:Object = {')': 'parenthesis', ']': 'square bracket', '}': 'curly brace'};

		/**
		 * Used by checkBrackets() as the expected closing bracket stack.
		 */
		protected var stack:Stack;

		/**
		 * Checks if the brackets (parentheses, square brackets, curly braces) match.
		 * If an error occurs, sets the errorMessage property and returns false.
		 *
		 * @return Boolean true if all the brackets match.
		 */
		public function checkBrackets():Boolean {
			stack.clear();

			for (var i:int = 0; i < _length; ++i) {
				var char:String = getBracketCharAt(i);
				if ((char == '(') || (char == '[') || (char == '{')) {
					// Char is an opening bracket, push it's index and the matching closing bracket on the stack.
					stack.push(i, (char == '(') ? ')' : (char == '[') ? ']' : '}');
				}
				else if ((char == ')') || (char == ']') || (char == '}')) {
					// Char is a closing bracket, check if it matches the expected closing bracket at the top of the stack.
					if (stack.peek() != char) {
						// The bracket doesn't match.
						return setBracketError('Mismatched right ' + BRACKET_NAME[char] + '.', i);
					}
					// Pop the bracket and it's index from the stack.
					stack.pop();
					stack.pop();
				}
				else if (char == ',') {
					// Char is a comma.
					if (!stack.length) {
						// The comma is not within a set of brackets.
						return setBracketError('Unexpected comma.', i);
					}
				}
			}

			if (stack.length) {
				// At least one opening bracket doesn't have a matching closing bracket.
				if (lenientBracketChecking) {
					while (!stack.isEmpty) {
						// Pop a bracket and it's index off the stack.
						char = stack.pop();
						stack.pop();
						// Create a new bracket token with the next index after the last input token's index, and add it to the append token queue.
						appendTokenQueue.add(_tokenFactory.createMathToken((char == ')') ? MathToken.RIGHT_PAREN : (char == ']') ? MathToken.RIGHT_BRACKET : MathToken.RIGHT_CURLY, _length));
					}
				}
				else {
					// Pop items off the stack until two items (a bracket and it's index) remain.
					while (stack.length > 2)
						stack.pop();
					// Pop the first mismatched bracket and the index from the stack.
					return setBracketError('Mismatched left ' + BRACKET_NAME[stack.pop()] + '.', stack.pop());
				}
			}
			return true;
		}

		/**
		 * Returns the character (bracket, comma or other) at the specified index.
		 * Subclasses must override this method.
		 * Used by checkBrackets().
		 * @see com.razorscript.math.expression.lexers.LexerBase#checkBrackets()
		 *
		 * @param index Index of the character to get.
		 * @return The character at the specified index.
		 */
		protected function getBracketCharAt(index:int):String {
			throw new Error('Illegal abstract method call.');
		}

		/**
		 * Sets the errorMessage and index properties, and returns false.
		 */
		protected function setBracketError(message:String, index:int):Boolean {
			_errorMessage = message;
			_index = index;
			return false;
		}

		/**
		 * Sets the errorMessage property, and returns null.
		 */
		protected function setError(message:String):MathToken {
			_errorMessage = message;
			appendTokenQueue.clear();
			return null;
		}

		/**
		 * The queue of tokens to append after there are no more tokens left.
		 * Used by checkBrackets().
		 * @see com.razorscript.math.expression.lexers.LexerBase#checkBrackets()
		 */
		protected var appendTokenQueue:Queue;

		/**
		 * The queued (buffered) token.
		 */
		protected var queuedToken:MathToken;

		/**
		 * The last token read.
		 */
		protected var lastToken:MathToken;

		/**
		 * Whether the next token can be an infix operator.
		 */
		protected var isExpectingInfixOp:Boolean;
		protected static const INFIX_TO_PREFIX_OP:Object = createInfixToPrefixOp();
		protected static function createInfixToPrefixOp():Object {
			var obj:Object = {};
			obj[MathOperator.ADD] = MathFun.ID;
			obj[MathOperator.SUBTRACT] = obj[MathOperator.SUBTRACT_ALT] = MathFun.NEG;
			return obj;
		}
	}
}
