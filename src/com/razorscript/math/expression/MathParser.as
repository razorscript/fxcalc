package com.razorscript.math.expression {
	import com.razorscript.razor_internal;
	import com.razorscript.debug.Debug;
	import com.razorscript.debug.Logger;
	import com.razorscript.ds.Stack;
	import com.razorscript.math.MathContext;
	import com.razorscript.math.errors.ArithmeticError;
	import com.razorscript.math.expression.MathExpression;
	import com.razorscript.math.expression.errors.MathParserError;
	import com.razorscript.math.expression.errors.MathParserSyntaxError;
	import com.razorscript.math.expression.lexers.IMathLexer;
	import com.razorscript.math.expression.supportClasses.FunInfo;
	import com.razorscript.math.expression.supportClasses.OpInfo;
	import com.razorscript.math.expression.tokens.IMathTokenFactory;
	import com.razorscript.math.expression.tokens.IdentifierToken;
	import com.razorscript.math.expression.tokens.ValueToken;
	import com.razorscript.math.expression.tokens.MathToken;
	import com.razorscript.math.expression.tokens.MathTokenFactory;
	import com.razorscript.math.expression.tokens.OpAssignToken;
	import com.razorscript.math.expression.tokens.OperatorToken;
	import com.razorscript.math.functions.polymorphism.errors.DispatchError;
	import com.razorscript.math.objects.conversions.errors.TypeConversionError;

	use namespace razor_internal;

	/**
	 * Parses and evaluates mathematical expressions.
	 * Can support different source data types using different lexers (default lexer supports string expressions).
	 *
	 * @author Gene Pavlovsky
	 */
	public class MathParser {
		public function MathParser(mathContext:MathContext, mathFun:MathFun, mathOperator:MathOperator, lexer:IMathLexer, valueTransform:Function = null, tokenFactory:IMathTokenFactory = null) {
			_mathContext = mathContext;
			_mathFun = mathFun;
			_mathOperator = mathOperator;
			_lexer = lexer;
			_valueTransform = valueTransform;
			_tokenFactory = tokenFactory ? tokenFactory : new MathTokenFactory();

			stack = new Stack();
		}

		protected var _mathContext:MathContext;
		/**
		 * The MathContext used when parsing and evaluating expressions.
		 */
		public function get mathContext():MathContext {
			return _mathContext;
		}

		public function set mathContext(value:MathContext):void {
			_mathContext = value;
		}

		protected var _mathFun:MathFun;
		/**
		 * The MathFun used when parsing and evaluating expressions.
		 */
		public function get mathFun():MathFun {
			return _mathFun;
		}

		public function set mathFun(value:MathFun):void {
			_mathFun = value;
		}

		protected var _mathOperator:MathOperator;
		/**
		 * The MathOperator used when parsing and evaluating expressions.
		 */
		public function get mathOperator():MathOperator {
			return _mathOperator;
		}

		public function set mathOperator(value:MathOperator):void {
			_mathOperator = value;
		}

		protected var _lexer:IMathLexer;
		/**
		 * The lexical analyzer used when parsing expressions.
		 */
		public function get lexer():IMathLexer {
			return _lexer;
		}

		public function set lexer(value:IMathLexer):void {
			_lexer = value;
		}

		protected var _tokenFactory:IMathTokenFactory;
		/**
		 * The math token factory used to create new tokens.
		 */
		public function get tokenFactory():IMathTokenFactory {
			return _tokenFactory;
		}

		public function set tokenFactory(value:IMathTokenFactory):void {
			_tokenFactory = value;
		}

		// TODO: Rename to validateValue/checkValue?
		protected var _valueTransform:Function;
		/**
		 * An optional value transform function.
		 * Used by parse() and eval() to transform each value by applying this function to the value, then assigning the function's result back to the value.
		 * If the function throws an ArithmeticError, the resulting MathExpression contains the error information.
		 */
		public function get valueTransform():Function {
			return _valueTransform;
		}

		public function set valueTransform(value:Function):void {
			_valueTransform = value;
		}

		/**
		 * Parses an expression using a variation of the Shunting yard algorithm, converting it into RPN.
		 * Returns a MathExpression containing the result of parsing the expression or the error information if an error has occurred.
		 * If expr is not null, stores the result in expr, otherwise creates a new MathExpression.
		 *
		 * The lexer property allows to use different lexers to support different types of expression sources.
		 * @see com.razorscript.math.expression.MathParser#lexer
		 *
		 * The returned MathExpression expr can be evaluated using expr.eval().
		 * @see com.razorscript.math.expression.MathExpression#eval()
		 *
		 * Useful if an expression is to be parsed once, but evaluated multiple times (e.g. in a grapher or a zero finder).
		 * If an expression is to be parsed and evaluated only once, consider using eval().
		 * @see com.razorscript.math.expression.MathParser#eval()
		 *
		 * @see http://wcipeg.com/wiki/Shunting_yard_algorithm
		 * @see https://en.wikipedia.org/wiki/Shunting-yard_algorithm
		 *
		 * @param source An expression to parse (data type depends on the lexer property).
		 * @param expr A MathExpression object to store the result in. If null, a new MathExpression object is created.
		 * @return A MathExpression containing the result of parsing the expression or the error information if an error has occurred.
		 *
		 * @throws ArgumentError - if source data type is not supported by the lexer.
		 * @throws MathParserError - if an exceptional condition has occurred during parsing the expression.
		 */
		public function parse(source:*, expr:MathExpression = null):MathExpression {
			if (expr)
				expr.reset(source, this);
			else
				expr = new MathExpression(source, this);

			_parse(expr);
			return expr;
		}

		/**
		 * Parses and evaluates an expression.
		 * Parses an expression using a variation of the Shunting yard algorithm, converting it into RPN.
		 * Evaluates the RPN using a common stack-based RPN evaluation algorithm. Discards the RPN, unless keepRPN is true.
		 * Returns a MathExpression containing the result of evaluating the expression or the error information if an error has occurred.
		 * If expr is not null, stores the result in expr, otherwise creates a new MathExpression.
		 *
		 * The lexer property allows to use different lexers to support different types of expression sources.
		 * @see com.razorscript.math.expression.MathParser#lexer
		 *
		 * Useful if an expression is to be parsed and evaluated only once.
		 * If an expression is to be parsed once, but evaluated multiple times (e.g. in a grapher or a zero finder), consider using parse().
		 * @see com.razorscript.math.expression.MathParser#parse()
		 *
		 * However, if keepRPN is true, the returned MathExpression expr can be evaluated again using expr.eval().
		 * @see com.razorscript.math.expression.MathExpression#eval()
		 *
		 * @param source An expression to parse and evaluate (data type depends on the lexer property).
		 * @param keepRPN Whether to keep the expression RPN after evaluation. If true, the returned MathExpression expr can be evaluated again using expr.eval().
		 * @param expr A MathExpression object to store the result in. If null, a new MathExpression object is created.
		 * @return A MathExpression containing the result of evaluating the expression or the error information if an error has occurred.
		 *
		 * @throws ArgumentError - if source data type is not supported by the lexer.
		 * @throws MathParserError - if an exceptional condition has occurred during parsing or evaluating the expression.
		 */
		public function eval(source:*, keepRPN:Boolean = false, expr:MathExpression = null):MathExpression {
			if (expr)
				expr.reset(source, this);
			else
				expr = new MathExpression(source, this);

			if (_parse(expr)) {
				_eval(expr);
				if (!keepRPN)
					expr.disposeRPN();
			}
			return expr;
		}

		protected static const STATE_PARSE:int = 1;
		protected static const STATE_EVAL:int = 2;

		protected var state:int;

		/**
		 * Used by _parse() as the operator stack. Can contain left parenthesis tokens, function tokens and operator tokens.
		 * Used by _eval() as the operand stack. Can contain mathematical objects or list start tokens.
		 */
		protected var stack:Stack;

		protected function _parse(expr:MathExpression):Boolean {
			CONFIG::debug {
				if (Debug.instance.debugMathParser & Debug.DEBUG_MATH_PARSER_PARSE)
					Logger.debug(this, 'parse() source=' + expr.source);
			}

			state = STATE_PARSE;
			stack.clear();

			_mathFun.mathContext = _mathContext;
			_lexer.setEnvironment(_mathContext, _mathFun, _mathOperator, _tokenFactory);
			_lexer.source = expr.source;
			// Check the brackets first. The main loop of the parsing algorithm assumes there are no mismatched brackets and list separators.
			if (!_lexer.checkBrackets())
				return setError(expr, MathExpression.SYNTAX_ERROR, _lexer.errorMessage, _lexer.index);

			var maxTokenIndex:int = _lexer.length - 1;
			var rpn:Vector.<MathToken> = new Vector.<MathToken>();
			var token:MathToken, stackToken:MathToken;
			var type:int, lastType:int;
			var isEmptySubExpr:Boolean;

			try {
				// While there are tokens to be read, read a token.
				while ((token = _lexer.readToken()) != null) {
					CONFIG::debug {
						if (Debug.instance.debugMathParser & Debug.DEBUG_MATH_PARSER_PARSE)
							Logger.debug(this, token);
					}

					type = token.type;
					if (isEmptySubExpr) {
						// If the sub-expression is empty, and token is a right parenthesis or a list separator (a comma), return an error.
						if ((type == MathToken.RIGHT_PAREN) || (type == MathToken.LIST_SEP)) {
							return setError(expr, MathExpression.SYNTAX_ERROR, 'Unexpected ' + ((token.index > maxTokenIndex) ? 'end of expression' :
								((type == MathToken.RIGHT_PAREN) ? 'right parenthesis' : 'comma')) + '.', token.index, rpn, token);
						}
						isEmptySubExpr = false;
					}

					if (type == MathToken.VALUE) {
						// Token is a mathematical object, add it to the output queue.
						if (_valueTransform)
							(token as ValueToken).transformValue(_valueTransform);
						rpn.push(token);
					}
					else if (type == MathToken.CONSTANT) {
						// Token is a constant, create a new mathematical object token holding it's value and add it to the output queue.
						rpn.push(_tokenFactory.createValueToken(evalConstant((token as IdentifierToken).name), token.index));
					}
					else if (type == MathToken.VARIABLE) {
						// Token is a variable, add it to the output queue.
						rpn.push(token);
					}
					else if (type == MathToken.LEFT_PAREN) {
						// Token is a left parenthesis, push it onto the stack.
						stack.push(token);
						// Unless the last token was a function call with arguments in parentheses, set the empty sub-expression flag.
						isEmptySubExpr = lastType != MathToken.FUN_PAREN;
					}
					else if (type == MathToken.RIGHT_PAREN) {
						// Token is a right parenthesis, pop tokens off the stack until a left parenthesis token is popped.
						while ((stackToken = stack.pop()).type != MathToken.LEFT_PAREN)
							rpn.push(stackToken); // If the popped token is not a left parenthesis, add it to the output queue.
						// Dispose of the left parenthesis token.
						_tokenFactory.disposeToken(stackToken);
					}
					else if (type == MathToken.LIST_SEP) {
						// Token is a list separator (a comma). Dispose of it.
						_tokenFactory.disposeToken(token);
						// While the token at the top of the stack is not a left parenthesis, pop it off the stack onto the output queue (don't pop the left parenthesis token).
						while (stack.top().type != MathToken.LEFT_PAREN)
							rpn.push(stack.pop());
						// Set the empty sub-expression flag.
						isEmptySubExpr = true;
					}
					else {
						// Token is an operator or a function call (function calls are considered operators).
						parseOperator(token as OperatorToken, type, expr, rpn);
					}

					lastType = type;
				}
			}
			catch (err:MathParserSyntaxError) {
				return setError(expr, MathExpression.SYNTAX_ERROR, err.message, token.index, rpn, token);
			}
			catch (err:ArithmeticError) {
				return setError(expr, MathExpression.SYNTAX_ERROR, err.message, token.index, rpn, token);
			}
			catch (err:RangeError) {
				return setError(expr, MathExpression.SYNTAX_ERROR, err.message, token.index, rpn, token);
			}

			if (_lexer.hasError)
				return setError(expr, MathExpression.SYNTAX_ERROR, _lexer.errorMessage, _lexer.index, rpn);

			// While there are still tokens on the stack, pop a token off the stack and add it to the output queue.
			while (!stack.isEmpty)
				rpn.push(stack.pop());

			expr.rpn = rpn;
			return true;
		}

		protected function parseOperator(token:OperatorToken, type:int, expr:MathExpression, rpn:Vector.<MathToken>):void {
			if (!token)
				throw new MathParserError(expr, 'Unknown token type: ' + token.typeName + '.', token.index);

			var opInfo:OpInfo = _mathOperator.getOpInfoByToken(token);
			if (!opInfo)
				throw new MathParserError(expr, 'Unknown ' + ((type == MathToken.OPERATOR) ? 'operator' : (type == MathToken.OP_ASSIGN) ? 'assignment operator' : 'function') + ': ' + token.operator + '.', token.index);

			if ((type == MathToken.OP_ASSIGN) && !(token as OpAssignToken).varName) {
				// Token is an assignment operator, but the assignment variable name is not set.
				var idToken:MathToken;
				if (opInfo.isRightAss) {
					// Token is a right-associative (regular) assignment operator, the last token on the output queue should contain the variable name (L-value). Remove the last token from the output queue.
					if (rpn.length)
						idToken = rpn.pop();
				}
				else {
					// Token is a left-associative (L-value on the right) assignment operator, the next input token should contain the variable name (L-value). Read a token.
					idToken = _lexer.readToken();
				}
				if (!idToken || (idToken.type != MathToken.VARIABLE))
					throw new MathParserSyntaxError((idToken ? 'Invalid' : 'Missing') + ' target for assignment operator "' + token.operator + '".');
				// Set the token's assignment variable name.
				(token as OpAssignToken).setVarName((idToken as IdentifierToken).name);
				// Dispose of the identifier token.
				_tokenFactory.disposeToken(idToken);
			}

			if (opInfo.isPostfix) {
				// Token is a unary postfix operator, add it to the output queue.
				rpn.push(token);
			}
			else {
				if (opInfo.isBinary) {
					// Token is a binary infix operator.
					// While there is a higher-or-equal-precedence left-associative operator or higher-precedence right-associative operator token at the top of the stack, pop it off the stack onto the output queue.
					var stackToken:MathToken;
					while (((stackToken = stack.peek()) != null) && (stackToken is OperatorToken)) {
						var stackOpInfo:OpInfo = _mathOperator.getOpInfoByToken(stackToken as OperatorToken);
						if ((stackOpInfo.prec > opInfo.prec) || ((stackOpInfo.prec == opInfo.prec) && opInfo.isLeftAss))
							rpn.push(stack.pop());
						else
							break;
					}
				}
				// Push the token onto the stack.
				stack.push(token);
				// If token is a function call, add a left parenthesis token to the output queue. For function calls with a juxtaposed argument this is not required, but still used, to provide more accurate error reporting.
				if ((type != MathToken.OPERATOR) && (type != MathToken.OP_ASSIGN))
					rpn.push(_tokenFactory.createMathToken(MathToken.LEFT_PAREN, token.index));
			}
		}

		protected static const LIST_START:String = '(';

		razor_internal function _eval(expr:MathExpression):Boolean {
			CONFIG::debug {
				if (Debug.instance.debugMathParser & Debug.DEBUG_MATH_PARSER_EVAL)
					Logger.debug(this, 'eval() rpn=' + expr.rpn);
			}

			state = STATE_EVAL;
			stack.clear();

			var rpn:Vector.<MathToken> = expr.rpn;
			var type:int;

			try {
				// While there are tokens to be read, read a token.
				for each (var token:MathToken in rpn) {
					CONFIG::debug {
						if (Debug.instance.debugMathParser & Debug.DEBUG_MATH_PARSER_EVAL)
							Logger.debug(this, token);
					}

					type = token.type;
					if (type == MathToken.VALUE) {
						// Token is a mathematical object, push it's value onto the stack.
						stack.push((token as ValueToken).value); // The value has transformValue() already applied to it by parse().
					}
					else if (type == MathToken.VARIABLE) {
						// Token is a variable, push it's value onto the stack.
						stack.push(evalVariable((token as IdentifierToken).name));
					}
					else if (type == MathToken.LEFT_PAREN) {
						// Token is a left parenthesis, push a list start token onto the stack.
						stack.push(LIST_START);
					}
					else {
						// Token is an operator or a function call, evaluate and push the result onto the stack.
						stack.push(transformValue(evalOperator(token as OperatorToken, type, expr)))
					}
				}
			}
			catch (err:MathParserSyntaxError) {
				return setError(expr, MathExpression.SYNTAX_ERROR, err.message, token.index);
			}
			catch (err:ArithmeticError) {
				return setError(expr, MathExpression.MATH_ERROR, err.message, token.index);
			}
			catch (err:RangeError) {
				return setError(expr, MathExpression.MATH_ERROR, err.message, token.index);
			}
			catch (err:TypeConversionError) {
				// TODO: Is TypeConversionError invalid user input or a bug?
				return setError(expr, MathExpression.MATH_ERROR, err.message, token.index);
			}
			catch (err:DispatchError) {
				// TODO: Is DispatchError invalid user input or a bug?
				return setError(expr, MathExpression.MATH_ERROR, err.message, token.index);
			}

			// If there is only one value on the stack, that value is the result of the evaluation.
			if (stack.length == 1)
				expr.setValue(evalValue(stack.pop()));
			else if (stack.isEmpty)
				return setError(expr, MathExpression.SYNTAX_ERROR, 'Empty result.');
			else
				return setError(expr, MathExpression.SYNTAX_ERROR, 'Too many values (' + stack.length + ').');

			return true;
		}

		protected function evalOperator(token:OperatorToken, type:int, expr:MathExpression):* {
			if (!token)
				throw new MathParserError(expr, 'Unknown token type: ' + token.typeName + '.', token.index);

			var funInfo:FunInfo = _mathFun.getFunInfoByToken(token);
			if (!funInfo)
				throw new MathParserError(expr, 'Unknown ' + ((type == MathToken.OPERATOR) ? 'operator' : (type == MathToken.OP_ASSIGN) ? 'assignment operator' : 'function') + ': ' + token.operator + '.', token.index);

			var stackValue:*;
			var x:*;

			if (type == MathToken.FUN_JUXTA) {
				// Token is a function call with a juxtaposed argument.
				if (funInfo.maxArity != 1)
					throw new MathParserSyntaxError('Function "' + token.operator + '" requires parentheses.');
				// Pop an item off the stack, expecting an argument.
				if ((stackValue = stack.pop()) == LIST_START)
					throw new MathParserSyntaxError('Not enough arguments for function "' + token.operator + '" (0, expected 1).');
				// Pop an item off the stack, expecting a list start token.
				if (stack.pop() != LIST_START)
					throw new MathParserError(expr, 'Unexpected item on the stack: ' + stackValue + ', expected a list start token.', token.index);
				// Evaluate the function and return the result.
				return funInfo.fun(stackValue);
			}
			else if (type == MathToken.FUN_PAREN) {
				// Token is a function call with arguments in parentheses.
				var args:Array = [];
				// Pop items off the stack until a list start token is popped.
				while ((stackValue = stack.pop()) != LIST_START)
					args.push(stackValue); // If the popped value is not a list start token, add it to the args array.
				if (args.length < funInfo.minArity)
					throw new MathParserSyntaxError('Not enough arguments for function "' + token.operator + '" (' + args.length + ', expected ' + formatArity(funInfo) + ').');
				else if (args.length > funInfo.maxArity)
					throw new MathParserSyntaxError('Too many arguments for function "' + token.operator + '" (' + args.length + ', expected ' + formatArity(funInfo) + ').');
				// Evaluate the function and return the result.
				return funInfo.fun.apply(null, args.reverse());
			}
			else if (type == MathToken.OPERATOR) {
				// Token is an operator.
				if (funInfo.minArity > 2)
					throw new MathParserError(expr, 'Invalid operator function: ' + token.operator, token.index);
				if (stack.length < funInfo.minArity)
					throw new MathParserSyntaxError('Not enough operands for operator "' + token.operator + '" (' + stack.length + ', expected ' + funInfo.minArity + ').');

				// Pop appropriate number of operands from the stack, evaluate the operator function and return the result.
				if (funInfo.isUnary) {
					return funInfo.fun(stack.pop());
				}
				else {
					x = stack.pop();
					return funInfo.fun(stack.pop(), x);
				}
			}
			else {
				// Token is an assignment operator.
				var opInfo:OpInfo = _mathOperator.getOpInfoByToken(token);
				if (!opInfo)
					throw new MathParserError(expr, 'Unknown assignment operator: ' + token.operator + '.', token.index);
				if (funInfo.minArity > 2)
					throw new MathParserError(expr, 'Invalid assignment operator function: ' + token.operator, token.index);
				if (!stack.length)
					throw new MathParserSyntaxError('Missing source for assignment operator "' + token.operator + '".');
				var varName:String = (token as OpAssignToken).varName;
				x = stack.pop();
				var value:*;
				if (funInfo.isUnary) {
					// Direct assignment.
					value = funInfo.fun(x);
				}
				else {
					// Compound assignment.
					value = funInfo.fun(evalVariable(varName), x);
				}
				_mathContext.setVariable(varName, transformValue(value));
				// If token is a right-associative (regular) assignment operator, return the resulting value of the assignment variable, otherwise return the original assignment value.
				return opInfo.isRightAss ? value : x;
			}
		}

		protected function formatArity(funInfo:FunInfo):String {
			if (funInfo.minArity == funInfo.maxArity)
				return funInfo.minArity.toString();
			else
				return funInfo.minArity + '..' + ((funInfo.maxArity == int.MAX_VALUE) ? '*' : funInfo.maxArity);
		}

		// TODO: Check for duplicate or missing calls to transformValue(). Skip transformValue() on variable read (assume already done on write)?
		[Inline]
		protected final function transformValue(value:*):* {
			return _valueTransform ? _valueTransform(value) : value;
		}

		[Inline]
		protected final function evalValue(value:*):* {
			return ('eval' in value) ? value.eval() : value;
		}

		[Inline] // TODO: Benchmark any performance gains.
		protected final function evalVariable(name:String):* {
			var value:* = _mathContext.getVariable(name);
			if (value == null)
				throw new MathParserSyntaxError('Undefined variable.');
			return transformValue(value);
		}

		[Inline] // TODO: Benchmark any performance gains.
		protected final function evalConstant(name:String):* {
			var value:* = _mathContext.getConstant(name);
			if (value == null)
				throw new MathParserSyntaxError('Undefined constant.');
			return transformValue(value);
		}

		/**
		 * Sets the MathExpression error information properties.
		 * Disposes of the tokens on the stack, and clears the stack.
		 * If rpn vector is not null, disposes of the tokens it contains.
		 * If token is not null, disposes of it.
		 */
		protected function setError(expr:MathExpression, type:int = 0, message:String = null, index:int = -1, rpn:Vector.<MathToken> = null, token:MathToken = null):Boolean {
			expr.setError(type, message, index);

			if (state != STATE_EVAL) {
				while (!stack.isEmpty)
					_tokenFactory.disposeToken(stack.pop());
			}
			else {
				stack.clear();
			}

			if (rpn) {
				for (var i:int = rpn.length - 1; i >= 0; --i)
					_tokenFactory.disposeToken(rpn[i]);
			}
			if (token)
				_tokenFactory.disposeToken(token);

			return false;
		}
	}
}
