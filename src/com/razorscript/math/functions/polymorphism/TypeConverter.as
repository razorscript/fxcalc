package com.razorscript.math.functions.polymorphism {
	import com.razorscript.math.functions.polymorphism.supportClasses.TypeConversion;
	import com.razorscript.math.objects.conversions.errors.TypeConversionError;
	import com.razorscript.utils.Lambda;
	import flash.utils.Dictionary;

	/**
	 * Defines the available type conversions.
	 *
	 * @author Gene Pavlovsky
	 */
	public class TypeConverter {
		public function TypeConverter() {
			clear();
		}

		/**
		 * Removes all conversions.
		 */
		public function clear():void {
			fromTypeDicts = new Dictionary();
			fromTypeConversions = new Dictionary();
		}

		/**
		 * Adds a conversion. If the conversion already exists, it is replaced.
		 *
		 * @param fromClass The conversion input class.
		 * @param toClass The conversion output class.
		 * @param fun The conversion function, is invoked with one argument (value) and should return the converted value:
		 *		function conversion(value:fromClass):toClass;
		 * @param order A number that defines the conversion's priority with regards to implicit type conversion (lower order means higher priority).
		 */
		public function addConversion(fromClass:Class, toClass:Class, fun:Function, order:int = -1):void {
			_addConversion(TypeUtil.getClassName(fromClass), TypeUtil.getClassName(toClass), fun, order);
		}

		protected function _addConversion(fromType:String, toType:String, fun:Function, order:int):void {
			var vec:Vector.<TypeConversion>;
			var dict:Dictionary = fromTypeDicts[fromType];
			if (dict) {
				var conversion:TypeConversion = dict[toType];
				if (conversion) {
					conversion.fun = fun;
					if (order >= 0)
						conversion.order = order;
					return;
				}
				vec = fromTypeConversions[fromType];
			}
			else {
				dict = fromTypeDicts[fromType] = new Dictionary();
				vec = fromTypeConversions[fromType] = new Vector.<TypeConversion>();
			}
			vec[vec.length] = dict[toType] = new TypeConversion(fromType, toType, fun, (order >= 0) ? order : autoOrder++);
		}

		/**
		 * Removes a conversion, if it exists.
		 *
		 * @param fromClass The conversion input class.
		 * @param toClass The conversion output class.
		 */
		public function removeConversion(fromClass:Class, toClass:Class):void {
			var dict:Dictionary = fromTypeDicts[TypeUtil.getClassName(fromClass)];
			if (dict)
				delete dict[TypeUtil.getClassName(toClass)];
		}

		/**
		 * Returns the conversion function for the specified input and output classes, or null if the conversion is not supported.
		 *
		 * @param fromClass The conversion input class.
		 * @param toClass The conversion output class.
		 * @return The conversion function, is invoked with one argument (value) and should return the converted value:
		 *		function conversion(value:fromClass):toClass;
		 */
		public function getConversion(fromClass:Class, toClass:Class):TypeConversion {
			var dict:Dictionary = fromTypeDicts[TypeUtil.getClassName(fromClass)];
			return dict ? dict[TypeUtil.getClassName(toClass)] : null;
		}

		/**
		 * Sorts the conversions in ascending order by conversion order.
		 * Call this function after all conversions are added, or if their order is modified later.
		 * It is not necessary to call this function if only automatic ordering is used (i.e. order is not set explicitly for any of the conversions).
		 */
		public function sortConversions():void {
			for each (var vec:Vector.<TypeConversion> in fromTypeConversions)
				vec.sort(compareTypeConversionsByOrderAsc);
		}

		/**
		 * Creates additional compound conversions, using at most one intermediate conversion.
		 */
		public function createCompoundConversions():void {
			// List all added types.
			var allTypes:Dictionary = new Dictionary();
			for (var fromType:String in fromTypeDicts) {
				allTypes[fromType] = true;
				for (var toType:String in fromTypeDicts[fromType])
					allTypes[toType] = true;
			}

			var minOrderType:String;
			var minOrder:int;

			for (fromType in fromTypeDicts) {
				var fromConvDict:Dictionary = fromTypeDicts[fromType];
				for (toType in allTypes) {
					if ((toType == fromType) || fromConvDict[toType])
						continue;
					// Direct conversion from fromType to toType doesn't exist. Match intermediate types allowing to perform the conversion in two steps.
					minOrderType = null;
					for (var qType:String in fromConvDict) {
						var qConvDict:Dictionary = fromTypeDicts[qType];
						if (qConvDict && qConvDict[toType]) {
							var order:int = Math.max(fromConvDict[qType].order, qConvDict[toType].order);
							// Choose matching intermediate types that requires conversions with the lowest order.
							if (!minOrderType || (order < minOrder)) {
								minOrderType = qType;
								minOrder = order;
							}
						}
					}
					if (minOrderType)
						_addConversion(fromType, toType, Lambda.compose(fromTypeDicts[minOrderType][toType].fun, fromConvDict[minOrderType].fun), minOrder);
				}
			}

			sortConversions();
		}

		/**
		 * Converts a value to the specified output class.
		 *
		 * @param value A value.
		 * @param toClass The conversion output class.
		 * @return The converted value.
		 *
		 * @throws TypeConversionError - if the conversion is not supported.
		 */
		public function convert(value:*, toClass:Class):* {
			if (value is toClass)
				return value;
			var dict:Dictionary = fromTypeDicts[TypeUtil.getType(value)];
			if (dict) {
				var conversion:TypeConversion = dict[TypeUtil.getClassName(toClass)];
				if (conversion)
					return conversion.fun(value);
			}
			throw new TypeConversionError('Unsupported conversion from ' + TypeUtil.getType(value) + ' to ' + TypeUtil.getClassName(toClass) + '".');
		}

		// TODO: Use Object instead of Dictionary, unless Dictionary with string keys is faster than object (test)?

		// Maps an input type name to a dictionary mapping an output type name to a type conversion definition.
		public var fromTypeDicts:Dictionary/*String => Dictionary: (String => TypeConversion)*/;

		// Maps an input type name to a vector of type conversion definitions.
		public var fromTypeConversions:Dictionary/*String => Vector.<TypeConversion>*/;

		protected var autoOrder:int = 1e6;

		protected static function compareTypeConversionsByOrderAsc(a:TypeConversion, b:TypeConversion):int {
			return a.order - b.order;
		}
	}
}
