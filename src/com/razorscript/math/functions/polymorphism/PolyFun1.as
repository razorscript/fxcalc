package com.razorscript.math.functions.polymorphism {
	import com.razorscript.math.functions.polymorphism.errors.DispatchError;
	import com.razorscript.math.functions.polymorphism.supportClasses.TypeConversion;

	/**
	 * Defines a 1-ary polymorphic function with single dispatch.
	 * Selects which function to call at run time based on the type of the passed argument.
	 * Supports implicit type conversion.
	 *
	 * @author Gene Pavlovsky
	 */
	public class PolyFun1 extends PolyFunBase {
		public function PolyFun1(typeConverter:TypeConverter) {
			super(typeConverter);
		}

		/**
		 * Adds a version of the function with the specified signature.
		 *
		 * @param xClass The type of the argument used for dynamic dispatch.
		 * @param fun A function: function (x:xClass):*;
		 */
		public function addSignature(xClass:Class, fun:Function):void {
			setSignatureFun(TypeUtil.getClassName(xClass), fun);
		}

		/**
		 * Calls the function.
		 *
		 * @param x A value.
		 * @return The return value of the called function.
		 *
		 * @throws DispatchError - if called with arguments not matching any defined signatures.
		 */
		public function call(x:*):* {
			// TODO: Use x.constructor (Class) as key? Measure performance difference.
			var xType:String = TypeUtil.getType(x);
			// Use an exactly matching signature if it exists.
			var fun:Function = getSignatureFun(xType);
			if (fun)
				return fun(x);

			var xConversions:Vector.<TypeConversion> = typeConverter.fromTypeConversions[xType];
			if (xConversions) {
				// Match signatures against types the argument can be converted to.
				var length:int = xConversions.length;
				for (var i:int = 0; i < length; ++i) {
					var xConvToQ:TypeConversion = xConversions[i];
					var qSigFun:Function = getSignatureFun(xConvToQ.toType);
					// The conversions vector is sorted in ascending order by conversion order. Choose the first matching signature.
					if (qSigFun)
						return qSigFun(xConvToQ.fun(x));
				}
			}

			throw new DispatchError(this, 'No matching signature for (' + xType + ').');
		}

		[Inline]
		protected final function getSignatureFun(xType:String):Function {
			return signatureFunctions[xType];
		}

		[Inline]
		protected final function setSignatureFun(xType:String, fun:Function):void {
			signatureFunctions[xType] = fun;
		}
	}
}
