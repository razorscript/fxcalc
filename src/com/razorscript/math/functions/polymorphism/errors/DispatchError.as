package com.razorscript.math.functions.polymorphism.errors {
	import com.razorscript.math.functions.polymorphism.PolyFunBase;
	
	/**
	 * Thrown when a polymorphic function is called with arguments not matching any defined signatures.
	 *
	 * @author Gene Pavlovsky
	 */
	public class DispatchError extends Error {
		public function DispatchError(polyFun:PolyFunBase, message:* = '', id:* = 0) {
			super(message, id);
			this.polyFun = polyFun;
		}
		
		/** The polymorphic function that caused the exception. */
		public var polyFun:PolyFunBase;
	}
}
