package com.razorscript.math.functions.polymorphism {
	import com.razorscript.math.functions.polymorphism.errors.DispatchError;
	import com.razorscript.math.functions.polymorphism.supportClasses.TypeConversion;
	import flash.utils.Dictionary;

	/**
	 * Defines a 2-ary polymorphic function with double dispatch.
	 * Selects which function to call at run time based on the types of the passed arguments.
	 * Supports implicit type conversion.
	 *
	 * @author Gene Pavlovsky
	 */
	public class PolyFun2 extends PolyFunBase {
		public function PolyFun2(typeConverter:TypeConverter) {
			super(typeConverter);
		}

		/**
		 * Adds a version of the function with the specified signature.
		 *
		 * @param xClass The type of the first argument used for dynamic dispatch.
		 * @param yClass The type of the second argument used for dynamic dispatch.
		 * @param fun A function: function (x:xClass, y:yClass):*;
		 */
		public function addSignature(xClass:Class, yClass:Class, fun:Function):void {
			setSignatureFun(TypeUtil.getClassName(xClass), TypeUtil.getClassName(yClass), fun);
		}

		/**
		 * Calls the function.
		 *
		 * @param x A value.
		 * @param y A value.
		 * @return The return value of the called function.
		 *
		 * @throws DispatchError - if called with arguments not matching any defined signatures.
		 */
		public function call(x:*, y:*):* {
			var xType:String = TypeUtil.getType(x);
			var yType:String = TypeUtil.getType(y);
			// Use an exactly matching signature if it exists.
			var fun:Function = getSignatureFun(xType, yType);
			if (fun)
				return fun(x, y);

			if (xType == yType) {
				var xConversions:Vector.<TypeConversion> = typeConverter.fromTypeConversions[xType];
				if (xConversions) {
					// Match signatures against types the arguments can be converted to.
					var length:int = xConversions.length;
					for (var i:int = 0; i < length; ++i) {
						var xConvToQ:TypeConversion = xConversions[i];
						var qSigFun:Function = getSignatureFun(xConvToQ.toType, xConvToQ.toType);
						// The conversions vector is sorted in ascending order by conversion order. Choose the first matching signature.
						if (qSigFun)
							return qSigFun(xConvToQ.fun(x), xConvToQ.fun(y));
					}
				}
			}
			else {
				var xConvDict:Dictionary = typeConverter.fromTypeDicts[xType];
				var yConvDict:Dictionary = typeConverter.fromTypeDicts[yType];
				// Match signatures that require a conversion of only one of the arguments.
				var xSigFun:Function = getSignatureFun(xType, xType);
				var ySigFun:Function = getSignatureFun(yType, yType);
				var xConvToY:TypeConversion = (ySigFun && xConvDict) ? xConvDict[yType] : null;
				var yConvToX:TypeConversion = (xSigFun && yConvDict) ? yConvDict[xType] : null;
				// If there are two matching signatures, choose the one that requires a conversion with the lowest order.
				// TODO: Test if conversion succeeds (doesn't return null), if not, try the other conversion.
				if (yConvToX && (!xConvToY || (yConvToX.order < xConvToY.order)))
					return xSigFun(x, yConvToX.fun(y));
				else if (xConvToY)
					return ySigFun(xConvToY.fun(x), y);

				// NOTE: If necessary, adapt the following commented out code (original xType == yType handler), allowing for separate conversions for both arguments.
				/*if (xConvDict && yConvDict) {
					// Match signatures against types the value can be converted to.
					var minOrderSignature:String;
					var minOrderConversion:TypeConversion;
					for (var signature:String in signatureFunctions) {
						// Only consider signatures with two arguments of the same type.
						var i:int = signature.indexOf(',');
						if (i == -1)
							continue;
						var sigType:String = signature.substr(0, i);
						if (sigType != signature.substr(i + 1))
							continue;

						var conversion:TypeConversion = xConvDict[sigType];
						if (conversion && (!minOrderConversion || (conversion.order < minOrderConversion.order))) {
							// Choose matching signature that requires a conversion with the lowest order.
							minOrderSignature = signature;
							minOrderConversion = conversion;
						}
					}
					if (minOrderSignature)
						return signatureFunctions[minOrderSignature](minOrderConversion.fun(x), minOrderConversion.fun(y));
				}*/
			}

			throw new DispatchError(this, 'No matching signature for (' + xType + ', ' + yType + ').');
		}

		[Inline]
		protected final function getSignatureFun(xType:String, yType:String):Function {
			return signatureFunctions[xType + ',' + yType];
		}

		[Inline]
		protected final function setSignatureFun(xType:String, yType:String, fun:Function):void {
			signatureFunctions[xType + ',' + yType] = fun;
		}
	}
}
