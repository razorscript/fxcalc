package com.razorscript.math.functions.polymorphism.supportClasses {
	
	/**
	 * Defines a type conversion.
	 *
	 * @author Gene Pavlovsky
	 */
	public class TypeConversion {
		public function TypeConversion(fromType:String, toType:String, fun:Function, order:int = -1) {
			this.fromType = fromType;
			this.toType = toType;
			this.fun = fun;
			this.order = order;
		}
		
		public var fromType:String;
		public var toType:String;
		public var fun:Function;
		public var order:int;
		
		public function toString():String {
			return '[TypeConversion fromType="' + fromType + '" toType="' + toType + '" order=' + order + ']';
		}
	}
}
