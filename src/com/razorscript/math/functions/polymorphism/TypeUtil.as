package com.razorscript.math.functions.polymorphism {
	import com.razorscript.math.objects.ComplexNumber;
	import com.razorscript.math.objects.Fraction;
	import com.razorscript.utils.ClassUtil;
	import com.razorscript.utils.VecUtil;
	import flash.utils.Dictionary;
	import flash.utils.getQualifiedClassName;

	/**
	 * Type utility functions.
	 *
	 * @author Gene Pavlovsky
	 */
	public class TypeUtil {
		/**
		 * Returns the type name of the specified value.
		 *
		 * @param value A value.
		 * @return The type name.
		 */
		public static function getType(value:*):String {
			if (value is Number)
				return 'Number';
			else if (value == null)
				return 'null';
			else if ('_typeName' in value)
				return value._typeName;
			else if (value is Boolean)
				return 'Boolean';
			throw new Error('Unsupported type: ' + getQualifiedClassName(value) + '.');
		}

		/**
		 * Returns the specified class's type name.
		 *
		 * @param classRef A class object.
		 * @return The type name.
		 */
		public static function getClassName(classRef:Class):String {
			var name:String = classNames[classRef];
			return name ? name : (classNames[classRef] = ClassUtil.getClassName(classRef));
		}

		protected static var classNames:Dictionary/*Class => String*/ = new Dictionary();
	}
}
