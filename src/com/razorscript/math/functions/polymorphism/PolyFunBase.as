package com.razorscript.math.functions.polymorphism {
	
	// TODO: Make type converter optional?
	// TODO: Move to general-purpose utils package?
	
	/**
	 * Base class for polymorphic functions.
	 *
	 * @author Gene Pavlovsky
	 */
	public class PolyFunBase {
		public function PolyFunBase(typeConverter:TypeConverter) {
			this.typeConverter = typeConverter;
			signatureFunctions = {};
		}
		
		protected var typeConverter:TypeConverter;
		protected var signatureFunctions:Object;
	}
}
