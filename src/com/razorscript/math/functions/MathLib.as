package com.razorscript.math.functions {
	import com.razorscript.math.MathContext;
	import com.razorscript.math.functions.mathLib.ArithmeticMath;
	import com.razorscript.math.functions.mathLib.LogicalMath;
	import com.razorscript.math.functions.mathLib.ProbabilityMath;
	import com.razorscript.math.functions.mathLib.RelationalMath;
	import com.razorscript.math.functions.mathLib.StatisticsMath;
	import com.razorscript.math.functions.mathLib.TrigonometryMath;
	import com.razorscript.math.functions.polymorphism.TypeConverter;
	
	// TODO: Implement bitwise functions.
	// TODO: Implement complex functions.
	// TODO: Implement matrix and vector functions.
	// TODO: Implement probability functions.
	// TODO: Implement statistics functions.
	// TODO: Implement unit conversion functions.
	// TODO: Implement percentage calculations.
	// TODO: Implement margin of error (uncertainty interval) calculations.
	// TODO: Support DMS angles, fractions, complex numbers, vectors and 2D matrices.
	// TODO: Implement more functions, borrow from MathJS functions.
	// TODO: Validate implemented functions, refer to MathJS functions.
	// TODO: Write a comprehensive test suite checking the various edge cases, refer to MathJS tests.
	// TODO: Add input range limits to functions (e.g. sin(x) = Math Error with x>7.85e8).
	
	/**
	 * Math functions library.
	 *
	 * @author Gene Pavlovsky
	 */
	public class MathLib {
		public function MathLib(mathContext:MathContext, typeConverter:TypeConverter) {
			arithmeticMath = new ArithmeticMath(this);
			logicalMath = new LogicalMath(this);
			probabilityMath = new ProbabilityMath(this);
			relationalMath = new RelationalMath(this);
			statisticsMath = new StatisticsMath(this);
			trigonometryMath = new TrigonometryMath(this);
			this.mathContext = mathContext;
			_typeConverter = typeConverter;
		}
		
		protected var _mathContext:MathContext;
		
		public function get mathContext():MathContext {
			return _mathContext;
		}
		
		public function set mathContext(value:MathContext):void {
			_mathContext = value;
			arithmeticMath.mathContext = value;
			logicalMath.mathContext = value;
			probabilityMath.mathContext = value;
			relationalMath.mathContext = value;
			statisticsMath.mathContext = value;
			trigonometryMath.mathContext = value;
		}
		
		protected var _typeConverter:TypeConverter;
		
		public function get typeConverter():TypeConverter {
			return _typeConverter;
		}
		
		public var arithmeticMath:ArithmeticMath;
		public var logicalMath:LogicalMath;
		public var probabilityMath:ProbabilityMath;
		public var relationalMath:RelationalMath;
		public var statisticsMath:StatisticsMath;
		public var trigonometryMath:TrigonometryMath;
	}
}
