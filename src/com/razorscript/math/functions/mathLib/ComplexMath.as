package com.razorscript.math.functions.mathLib {
	import com.razorscript.math.functions.MathLib;
	import com.razorscript.math.objects.ComplexNumber;
	import com.razorscript.math.objects.Number;

	/**
	 * Complex math functions.
	 *
	 * @author Gene Pavlovsky
	 */
	public class ComplexMath extends MathBase {
		public function ComplexMath(mathLib:MathLib) {
			super(mathLib);
		}

		/**
		 * Returns the argument of a complex value.
		 *
		 * @param x A mathematical object.
		 * @return The argument of x.
		 */
		public function arg(x:*):* {
			if (x is Number)
				return new Number(Math.atan2(0, x as Number));
			else if (x is ComplexNumber)
				return new Number(Math.atan2((x as ComplexNumber).im, (x as ComplexNumber).re));
		}

		/**
		 * Returns the complex conjugate of a complex value.
		 *
		 * @param x A mathematical object.
		 * @return The complex conjugate of x.
		 */
		public function conj(x:*):* {
			if (x is Number)
				return x;
			else if (x is ComplexNumber)
				return new ComplexNumber((x as ComplexNumber).re, -(x as ComplexNumber).im);
		}

		/**
		 * Returns the real part of a complex value.
		 *
		 * @param x A mathematical object.
		 * @return The real part of x.
		 */
		public function re(x:*):* {
			if (x is Number)
				return x;
			else if (x is ComplexNumber)
				return new Number((x as ComplexNumber).re);
		}

		/**
		 * Returns the imaginary part of a complex value.
		 *
		 * @param x A mathematical object.
		 * @return The imaginary part of x.
		 */
		public function im(x:*):* {
			if (x is Number)
				return 0;
			else if (x is ComplexNumber)
				return new Number((x as ComplexNumber).im);
		}
	}
}
