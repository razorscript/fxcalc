package com.razorscript.math.functions.mathLib {
	import com.razorscript.math.MathExt;
	import com.razorscript.math.RoundingMode;
	import com.razorscript.math.errors.ArithmeticError;
	import com.razorscript.math.functions.MathLib;
	import com.razorscript.math.functions.polymorphism.PolyFun1;
	import com.razorscript.math.functions.polymorphism.PolyFun2;
	import com.razorscript.math.objects.ComplexNumber;
	import com.razorscript.math.objects.Fraction;

	/**
	 * Arithmetic math functions.
	 *
	 * @author Gene Pavlovsky
	 */
	public class ArithmeticMath extends MathBase {
		public function ArithmeticMath(mathLib:MathLib) {
			super(mathLib);
		}

		/***********************************************************************************************************************************************/

		/**
		 * Arithmetic functions.
		 */

		protected var _add:Function;
		/**
		 * Adds two values.
		 *
		 * @param x:(Number | Fraction | ComplexNumber) First addend.
		 * @param y:(Number | Fraction | ComplexNumber) Second addend.
		 * @return (Number | Fraction | ComplexNumber) The sum.
		 */
		public function get add():Function {
			if (!_add) {
				var pf2:PolyFun2 = new PolyFun2(mathLib.typeConverter);
				pf2.addSignature(Number, Number, addReal);
				pf2.addSignature(Fraction, Fraction, addFraction);
				pf2.addSignature(ComplexNumber, ComplexNumber, addComplex);
				_add = pf2.call;
			}
			return _add;
		}

		public function addReal(x:Number, y:Number):Number {
			return x + y;
			// TODO: Unify treatment of add/subtract based on signs of the arguments. This code is valid if both arguments have the same sign.
			/*if (isMathicFP) {
				var sum:Number = x + y;
				return nearlyEqual(sum, (Math.abs(x) >= Math.abs(y)) ? x : y) ? x : sum;
			}*/
		}

		public function addFraction(x:Fraction, y:Fraction):Fraction {
			return new Fraction(x.n * y.d + x.d * y.n, x.d * y.d);
		}

		public function addComplex(x:ComplexNumber, y:ComplexNumber):ComplexNumber {
			return new ComplexNumber(x.re + y.re, x.im + y.im);
		}

		/// ------------------------------------------------------------------------------------------------------------------------------------

		protected var _subtract:Function;
		/**
		 * Subtracts two values.
		 *
		 * @param x:(Number | Fraction | ComplexNumber) Minuend.
		 * @param y:(Number | Fraction | ComplexNumber) Subtrahend.
		 * @return (Number | Fraction | ComplexNumber) The difference.
		 */
		public function get subtract():Function {
			if (!_subtract) {
				var pf2:PolyFun2 = new PolyFun2(mathLib.typeConverter);
				pf2.addSignature(Number, Number, subtractReal);
				pf2.addSignature(Fraction, Fraction, subtractFraction);
				pf2.addSignature(ComplexNumber, ComplexNumber, subtractComplex);
				_subtract = pf2.call;
			}
			return _subtract;
		}

		public function subtractReal(x:Number, y:Number):Number {
			return x - y;
			// TODO: Unify treatment of add/subtract based on signs of the arguments. This code is valid if both arguments have the same sign.
			//return (isMathicFP && nearlyEqual(x, y)) ? 0 : (x - y);
		}

		public function subtractFraction(x:Fraction, y:Fraction):Fraction {
			return new Fraction(x.n * y.d - x.d * y.n, x.d * y.d);
		}

		public function subtractComplex(x:ComplexNumber, y:ComplexNumber):ComplexNumber {
			return new ComplexNumber(x.re - y.re, x.im - y.im);
		}

		/// ------------------------------------------------------------------------------------------------------------------------------------

		protected var _multiply:Function;
		/**
		 * Multiplies two values.
		 *
		 * @param x:(Number | Fraction | ComplexNumber) First factor.
		 * @param y:(Number | Fraction | ComplexNumber) Second factor.
		 * @return (Number | Fraction | ComplexNumber) The product.
		 */
		public function get multiply():Function {
			if (!_multiply) {
				var pf2:PolyFun2 = new PolyFun2(mathLib.typeConverter);
				pf2.addSignature(Number, Number, multiplyReal);
				pf2.addSignature(Fraction, Fraction, multiplyFraction);
				pf2.addSignature(ComplexNumber, ComplexNumber, multiplyComplex);
				_multiply = pf2.call;
			}
			return _multiply;
		}

		public function multiplyReal(x:Number, y:Number):Number {
			return x * y;
		}

		public function multiplyFraction(x:Fraction, y:Fraction):Fraction {
			return new Fraction(x.n * y.n, x.d * y.d);
		}

		public function multiplyComplex(x:ComplexNumber, y:ComplexNumber):ComplexNumber {
			return new ComplexNumber(
				x.re * y.re - x.im * y.im,
				x.re * y.im + x.im * y.re);
		}

		/// ------------------------------------------------------------------------------------------------------------------------------------

		protected var _divide:Function;
		/**
		 * Divides two values.
		 *
		 * @param x:(Number | Fraction | ComplexNumber) Dividend.
		 * @param y:(Number | Fraction | ComplexNumber) Divisor.
		 * @return (Number | Fraction | ComplexNumber) The quotient.
		 *
		 * @throws ArithmeticError - if y is nearly 0.
		 */
		public function get divide():Function {
			if (!_divide) {
				var pf2:PolyFun2 = new PolyFun2(mathLib.typeConverter);
				pf2.addSignature(Number, Number, divideReal);
				pf2.addSignature(Fraction, Fraction, divideFraction);
				pf2.addSignature(ComplexNumber, ComplexNumber, divideComplex);
				_divide = pf2.call;
			}
			return _divide;
		}

		public function divideReal(x:Number, y:Number):Number {
			checkDiv0(y);
			return x / y;
		}

		public function divideFraction(x:Fraction, y:Fraction):Fraction {
			return new Fraction(x.n * y.d, x.d * y.n);
		}

		public function divideComplex(x:ComplexNumber, y:ComplexNumber):ComplexNumber {
			var d:Number = y.re * y.re + y.im * y.im;
			checkDiv0(d);
			return new ComplexNumber(
				(x.re * y.re + x.im * y.im) / d,
				(x.im * y.re - x.re * y.im) / d);
		}

		/// ------------------------------------------------------------------------------------------------------------------------------------

		protected var _div:Function;
		/**
		 * Calculates the quotient of floored division of two values.
		 * The result is rounded to the nearest integer towards negative infinity.
		 * The function is equivalent to `floor(x / y)`.
		 *
		 * @see http://en.wikipedia.org/wiki/Modulo_operation
		 *
		 * @param x:(Number | Fraction | ComplexNumber) Dividend.
		 * @param y:(Number | Fraction | ComplexNumber) Divisor.
		 * @return (Number | Fraction | ComplexNumber) The quotient, rounded to the nearest integer towards negative infinity.
		 *
		 * @throws ArithmeticError - if y is nearly 0.
		 */
		public function get div():Function {
			if (!_div) {
				var pf2:PolyFun2 = new PolyFun2(mathLib.typeConverter);
				pf2.addSignature(Number, Number, divReal);
				pf2.addSignature(Fraction, Fraction, divFraction);
				pf2.addSignature(ComplexNumber, ComplexNumber, divComplex);
				_div = pf2.call;
			}
			return _div;
		}

		public function divReal(x:Number, y:Number):Number {
			checkDiv0(y);
			return Math.floor(x / y);
		}

		public function divFraction(x:Fraction, y:Fraction):Fraction {
			return null; // TODO: Implement.
			/*var d:Number = x.d * y.n;
			checkDiv0(d);
			return Math.floor(x.n * y.d / d);*/
		}

		public function divComplex(x:ComplexNumber, y:ComplexNumber):ComplexNumber {
			var d:Number = y.re * y.re + y.im * y.im;
			checkDiv0(d);
			return new ComplexNumber(
				Math.floor((x.re * y.re + x.im * y.im) / d),
				Math.floor((x.im * y.re - x.re * y.im) / d));
		}

		/// ------------------------------------------------------------------------------------------------------------------------------------

		protected var _mod:Function;
		/**
		 * Calculates the remainder of floored division of two values.
		 * For real numbers, the result has the sign of the divisor (unlike ActionScript's `%` operator).
		 * The function is equivalent to `x - y * div(x, y)` and `x - y * floor(x / y)`.
		 * Supports complex numbers, using the definition in terms of div.
		 * If y is zero, returns x.
		 *
		 * @see http://en.wikipedia.org/wiki/Modulo_operation
		 *
		 * @param x:(Number | Fraction | ComplexNumber) Dividend.
		 * @param y:(Number | Fraction | ComplexNumber) Divisor.
		 * @return (Number | Fraction | ComplexNumber) The remainder, with the sign of the divisor.
		 */
		public function get mod():Function {
			if (!_mod) {
				var pf2:PolyFun2 = new PolyFun2(mathLib.typeConverter);
				pf2.addSignature(Number, Number, modReal);
				pf2.addSignature(Fraction, Fraction, modFraction);
				pf2.addSignature(ComplexNumber, ComplexNumber, modComplex);
				_mod = pf2.call;
			}
			return _mod;
		}

		public function modReal(x:Number, y:Number):Number {
			return isZero(y) ? x : (x - y * Math.floor(x / y));
		}

		public function modFraction(x:Fraction, y:Fraction):Fraction {
			return null; // TODO: Implement.
			/*if (isNaN(this['n']) || isNaN(this['d'])) {
				return new Fraction(NaN);
			}

			if (a === undefined) {
				return new Fraction(this["s"] * this["n"] % this["d"], 1);
			}

			parse(a, b);
			if (0 === P["n"] && 0 === this["d"]) {
				Fraction(0, 0); // Throw div/0
			}

			// New attempt: a1 / b1 = a2 / b2 * q + r
			// => b2 * a1 = a2 * b1 * q + b1 * b2 * r
			// => (b2 * a1 % a2 * b1) / (b1 * b2)

			return new Fraction(
							(this["s"] * P["d"] * this["n"]) % (P["n"] * this["d"]),
							P["d"] * this["d"]
							);*/
		}

		public function modComplex(x:ComplexNumber, y:ComplexNumber):ComplexNumber {
			if (isZero(y.re) && isZero(y.im))
				return x;
			y = multiplyComplex(y, divComplex(x, y));
			return new ComplexNumber(x.re - y.re, x.im - y.im);
		}

		/// ------------------------------------------------------------------------------------------------------------------------------------

		protected var _rem:Function;
		/**
		 * Calculates the remainder of truncated division of two values.
		 * For real numbers, the result has the sign of the dividend (like ActionScript's `%` operator).
		 * The function is equivalent to `x - y * fix(x / y)`.
		 * Supports complex numbers, using the definition in terms of fix.
		 *
		 * @see http://en.wikipedia.org/wiki/Modulo_operation
		 *
		 * @param x:(Number | Fraction | ComplexNumber) Dividend.
		 * @param y:(Number | Fraction | ComplexNumber) Divisor.
		 * @return (Number | Fraction | ComplexNumber) The remainder, with the sign of the dividend.
		 *
		 * @throws ArithmeticError - if y is nearly 0.
		 */
		public function get rem():Function {
			if (!_rem) {
				var pf2:PolyFun2 = new PolyFun2(mathLib.typeConverter);
				pf2.addSignature(Number, Number, remReal);
				pf2.addSignature(Fraction, Fraction, remFraction);
				pf2.addSignature(ComplexNumber, ComplexNumber, remComplex);
				_rem = pf2.call;
			}
			return _rem;
		}

		public function remReal(x:Number, y:Number):Number {
			checkDiv0(y);
			return x % y;
		}

		public function remFraction(x:Fraction, y:Fraction):Fraction {
			return null; // TODO: Implement.
		}

		public function remComplex(x:ComplexNumber, y:ComplexNumber):ComplexNumber {
			y = multiplyComplex(y, fixComplex(divideComplex(x, y)));
			return new ComplexNumber(x.re - y.re, x.im - y.im);
		}

		/// ------------------------------------------------------------------------------------------------------------------------------------

		protected var _fraction:Function;
		/**
		 * Divides two values. If possible, returns Fraction as the result.
		 *
		 * @param x:(Number | Fraction | ComplexNumber) Numerator.
		 * @param y:(Number | Fraction | ComplexNumber) Denominator.
		 * @return (Number | Fraction | ComplexNumber) The quotient.
		 *
		 * @throws ArithmeticError - if y is nearly 0.
		 */
		public function get fraction():Function {
			if (!_fraction) {
				var pf2:PolyFun2 = new PolyFun2(mathLib.typeConverter);
				pf2.addSignature(Number, Number, fractionReal);
				pf2.addSignature(Fraction, Fraction, fractionFraction);
				pf2.addSignature(ComplexNumber, ComplexNumber, fractionComplex);
				_fraction = pf2.call;
			}
			return _fraction;
		}

		public function fractionReal(x:Number, y:Number):* {
			checkDiv0(y);
			// TODO: Only return Fraction if the numbers are integers (or the fraction can be simplified).
			// TODO: How to handle a_b_c fractions? E.g. a_b_c_d should throw an error but (a_b)_(c_d) is equivalent to (a_b)/(c_d).
			return new Fraction(x, y);
		}

		public function fractionFraction(x:Fraction, y:Fraction):Fraction {
			return new Fraction(x.n * y.d, x.d * y.n);
		}

		public function fractionComplex(x:ComplexNumber, y:ComplexNumber):ComplexNumber {
			return divideComplex(x, y); // TODO: Implement.
		}

		/// ------------------------------------------------------------------------------------------------------------------------------------

		protected var _reciprocal:Function;
		/**
		 * Calculates the multiplicative inverse (reciprocal) of a value.
		 *
		 * @param x:(Number | Fraction | ComplexNumber) A value.
		 * @return (Number | Fraction | ComplexNumber) The reciprocal of the value.
		 *
		 * @throws ArithmeticError - if x is nearly 0.
		 */
		public function get reciprocal():Function {
			if (!_reciprocal) {
				var pf1:PolyFun1 = new PolyFun1(mathLib.typeConverter);
				pf1.addSignature(Number, reciprocalReal);
				pf1.addSignature(Fraction, reciprocalFraction);
				pf1.addSignature(ComplexNumber, reciprocalComplex);
				_reciprocal = pf1.call;
			}
			return _reciprocal;
		}

		public function reciprocalReal(x:Number):Number {
			checkDiv0(x);
			return 1 / x;
		}

		public function reciprocalFraction(x:Fraction):Fraction {
			checkDiv0(x.n);
			return new Fraction(x.d, x.n);
		}

		public function reciprocalComplex(x:ComplexNumber):ComplexNumber {
			var d:Number = x.re * x.re + x.im * x.im;
			checkDiv0(d);
			return new ComplexNumber(x.re / d, -x.im / d);
		}

		/// ------------------------------------------------------------------------------------------------------------------------------------

		/**
		 * Returns the argument as is.
		 *
		 * @param x:(Number | Fraction | ComplexNumber) A value.
		 * @return (Number | Fraction | ComplexNumber) The value as is.
		 */
		public function identity(x:*):* {
			return x;
		}

		/// ------------------------------------------------------------------------------------------------------------------------------------

		protected var _negate:Function;
		/**
		 * Calculates the additive inverse (negation) of a value.
		 *
		 * @param x:(Number | Fraction | ComplexNumber) A value.
		 * @return (Number | Fraction | ComplexNumber) The negation of the value.
		 */
		public function get negate():Function {
			if (!_negate) {
				var pf1:PolyFun1 = new PolyFun1(mathLib.typeConverter);
				pf1.addSignature(Number, negateReal);
				pf1.addSignature(Fraction, negateFraction);
				pf1.addSignature(ComplexNumber, negateComplex);
				_negate = pf1.call;
			}
			return _negate;
		}

		public function negateReal(x:Number):Number {
			return -x;
		}

		public function negateFraction(x:Fraction):Fraction {
			return new Fraction(-x.n, x.d);
		}

		public function negateComplex(x:ComplexNumber):ComplexNumber {
			return new ComplexNumber(Math.floor(x.re), Math.floor(x.im));
		}

		/// ------------------------------------------------------------------------------------------------------------------------------------

		protected var _abs:Function;
		/**
		 * Calculates the absolute value of a value.
		 *
		 * @param x:(Number | Fraction | ComplexNumber) A value.
		 * @return (Number | Fraction | ComplexNumber) The absolute value of the value.
		 */
		public function get abs():Function {
			if (!_abs) {
				var pf1:PolyFun1 = new PolyFun1(mathLib.typeConverter);
				pf1.addSignature(Number, absReal);
				pf1.addSignature(Fraction, absFraction);
				pf1.addSignature(ComplexNumber, absComplex);
				_abs = pf1.call;
			}
			return _abs;
		}

		public function absReal(x:Number):Number {
			return Math.abs(x);
		}

		public function absFraction(x:Fraction):Fraction {
			return new Fraction(Math.abs(x.n), x.d);
		}

		public function absComplex(x:ComplexNumber):Number {
			return Math.sqrt(x.re * x.re + x.im * x.im);
		}

		/// ------------------------------------------------------------------------------------------------------------------------------------

		protected var _sign:Function;
		/**
		 * Returns the sign of a value.
		 *
		 * For fractions, the result is returned as a fraction or a number, according to the MathContext settings.
		 * @see com.razorscript.math.MathContext#useIntegerFractions
		 *
		 * @param x:(Number | Fraction | ComplexNumber) A value.
		 * @return (Number | Fraction | ComplexNumber) The sign of the value.
		 */
		public function get sign():Function {
			if (!_sign) {
				var pf1:PolyFun1 = new PolyFun1(mathLib.typeConverter);
				pf1.addSignature(Number, signReal);
				pf1.addSignature(Fraction, signFraction);
				pf1.addSignature(ComplexNumber, signComplex);
				_sign = pf1.call;
			}
			return _sign;
		}

		public function signReal(x:Number):Number {
			return MathExt.sign(x);
		}

		public function signFraction(x:Fraction):* {
			return integerFraction(MathExt.sign(x.n));
		}

		public function signComplex(x:ComplexNumber):ComplexNumber {
			var abs:Number = absComplex(x);
			return new ComplexNumber(x.re / abs, x.im / abs);
		}

		/// ------------------------------------------------------------------------------------------------------------------------------------

		protected var _floor:Function;
		/**
		 * Rounds a value towards negative infinity.
		 *
		 * For fractions, the result is returned as a fraction or a number, according to the MathContext settings.
		 * @see com.razorscript.math.MathContext#useIntegerFractions
		 *
		 * For complex numbers, applies separately to real and imaginary parts.
		 *
		 * @param x:(Number | Fraction | ComplexNumber) A value.
		 * @return (Number | Fraction | ComplexNumber) The rounded value.
		 */
		public function get floor():Function {
			if (!_floor) {
				var pf1:PolyFun1 = new PolyFun1(mathLib.typeConverter);
				pf1.addSignature(Number, floorReal);
				//pf1.addSignature(Fraction, floorFraction);
				pf1.addSignature(ComplexNumber, floorComplex);
				_floor = pf1.call;
			}
			return _floor;
		}

		public function floorReal(x:Number):Number {
			return Math.floor(x);
		}

		public function floorFraction(x:Fraction):* {
			return integerFraction(Math.floor(x.valueOf()));
		}

		public function floorComplex(x:ComplexNumber):ComplexNumber {
			return new ComplexNumber(Math.floor(x.re), Math.floor(x.im));
		}

		/// ------------------------------------------------------------------------------------------------------------------------------------

		protected var _ceil:Function;
		/**
		 * Rounds a value towards positive infinity.
		 *
		 * For fractions, the result is returned as a fraction or a number, according to the MathContext settings.
		 * @see com.razorscript.math.MathContext#useIntegerFractions
		 *
		 * For complex numbers, applies separately to real and imaginary parts.
		 *
		 * @param x:(Number | Fraction | ComplexNumber) A value.
		 * @return (Number | Fraction | ComplexNumber) The rounded value.
		 */
		public function get ceil():Function {
			if (!_ceil) {
				var pf1:PolyFun1 = new PolyFun1(mathLib.typeConverter);
				pf1.addSignature(Number, ceilReal);
				//pf1.addSignature(Fraction, ceilFraction);
				pf1.addSignature(ComplexNumber, ceilComplex);
				_ceil = pf1.call;
			}
			return _ceil;
		}

		public function ceilReal(x:Number):Number {
			return Math.ceil(x);
		}

		public function ceilFraction(x:Fraction):* {
			return integerFraction(Math.ceil(x.valueOf()));
		}

		public function ceilComplex(x:ComplexNumber):ComplexNumber {
			return new ComplexNumber(Math.ceil(x.re), Math.ceil(x.im));
		}

		/// ------------------------------------------------------------------------------------------------------------------------------------

		protected var _fix:Function;
		/**
		 * Rounds a value towards zero.
		 *
		 * For fractions, the result is returned as a fraction or a number, according to the MathContext settings.
		 * @see com.razorscript.math.MathContext#useIntegerFractions
		 *
		 * For complex numbers, applies separately to real and imaginary parts.
		 *
		 * @param x:(Number | Fraction | ComplexNumber) A value.
		 * @return (Number | Fraction | ComplexNumber) The rounded value.
		 */
		public function get fix():Function {
			if (!_fix) {
				var pf1:PolyFun1 = new PolyFun1(mathLib.typeConverter);
				pf1.addSignature(Number, fixReal);
				//pf1.addSignature(Fraction, fixFraction);
				pf1.addSignature(ComplexNumber, fixComplex);
				_fix = pf1.call;
			}
			return _fix;
		}

		public function fixReal(x:Number):Number {
			return MathExt.fix(x);
		}

		public function fixFraction(x:Fraction):* {
			return integerFraction(MathExt.fix(x.valueOf()));
		}

		public function fixComplex(x:ComplexNumber):ComplexNumber {
			return new ComplexNumber(MathExt.fix(x.re), MathExt.fix(x.im));
		}

		/// ------------------------------------------------------------------------------------------------------------------------------------

		protected var _round:Function;
		/**
		 * Rounds a value according to the MathContext settings.
		 * @see com.razorscript.math.MathContext#roundingMode
		 *
		 * For fractions, the result is returned as a fraction or a number, according to the MathContext settings.
		 * @see com.razorscript.math.MathContext#useIntegerFractions
		 *
		 * For complex numbers, applies separately to real and imaginary parts.
		 *
		 * @param x:(Number | Fraction | ComplexNumber) A value.
		 * @return (Number | Fraction | ComplexNumber) The rounded value.
		 */
		public function get round():Function {
			if (!_round) {
				var pf1:PolyFun1 = new PolyFun1(mathLib.typeConverter);
				pf1.addSignature(Number, roundReal);
				//pf1.addSignature(Fraction, roundFraction);
				pf1.addSignature(ComplexNumber, roundComplex);
				_round = pf1.call;
			}
			return _round;
		}

		public function roundReal(x:Number):Number {
			return MathExt.round(x, mathContext.roundingMode);
		}

		public function roundFraction(x:Fraction):* {
			return integerFraction(MathExt.round(x.valueOf(), mathContext.roundingMode));
		}

		public function roundComplex(x:ComplexNumber):ComplexNumber {
			return new ComplexNumber(MathExt.round(x.re, mathContext.roundingMode), MathExt.round(x.im, mathContext.roundingMode));
		}

		/// ------------------------------------------------------------------------------------------------------------------------------------

		protected var _roundToDigits:Function;
		/**
		 * Rounds a value to the specified number of decimal places according to the MathContext settings.
		 * @see com.razorscript.math.MathContext#roundingMode
		 *
		 * For complex numbers, applies separately to real and imaginary parts.
		 *
		 * @param x:(Number | ComplexNumber) A value.
		 * @param fractionDigits An integer between 0 and 17, inclusive, that represents the desired number of decimal places.
		 * @return (Number | ComplexNumber) The rounded value.
		 *
		 * @throws RangeError - if fractionDigits is out of range.
		 */
		public function get roundToDigits():Function {
			if (!_roundToDigits) {
				var pf2:PolyFun2 = new PolyFun2(mathLib.typeConverter);
				pf2.addSignature(Number, Number, roundToDigitsReal);
				pf2.addSignature(ComplexNumber, Number, roundToDigitsComplex);
				_roundToDigits = pf2.call;
			}
			return _roundToDigits;
		}

		public function roundToDigitsReal(x:Number, fractionDigits:int):Number {
			if ((fractionDigits < 0) || (fractionDigits > MathExt.DOUBLE_UNIQ_DIGITS))
				throw new RangeError('The fractionDigits argument (' + fractionDigits + ') is outside the range [0, ' + MathExt.DOUBLE_UNIQ_DIGITS + '].');
			return MathExt.roundToDigits(x, fractionDigits, mathContext.roundingMode);
		}

		public function roundToDigitsComplex(x:ComplexNumber, fractionDigits:int):ComplexNumber {
			return new ComplexNumber(
				MathExt.roundToDigits(x.re, fractionDigits, mathContext.roundingMode), MathExt.roundToDigits(x.im, fractionDigits, mathContext.roundingMode));
		}

		/// ------------------------------------------------------------------------------------------------------------------------------------

		protected var _roundToNearest:Function;
		/**
		 * Rounds a value to the specified interval according to the MathContext settings.
		 *
		 * Returns the multiple of the interval which is nearest to the specified value.
		 * @see com.razorscript.math.MathContext#roundingMode
		 *
		 * For complex numbers, applies separately to real and imaginary parts.
		 *
		 * @param x:(Number | Fraction | ComplexNumber) A value.
		 * @param interval:(Number | Fraction | ComplexNumber) A positive value that represents the rounding interval.
		 * @return (Number | Fraction | ComplexNumber) The rounded value.
		 *
		 * @throws RangeError - if interval is out of range.
		 */
		public function get roundToNearest():Function {
			if (!_roundToNearest) {
				var pf2:PolyFun2 = new PolyFun2(mathLib.typeConverter);
				pf2.addSignature(Number, Number, roundToNearestReal);
				//pf2.addSignature(Fraction, Fraction, roundToNearestFraction);
				pf2.addSignature(ComplexNumber, ComplexNumber, roundToNearestComplex);
				_roundToNearest = pf2.call;
			}
			return _roundToNearest;
		}

		public function roundToNearestReal(x:Number, interval:Number):Number {
			if (interval <= 0)
				throw new RangeError('The interval argument (' + interval + ') must be a positive number.');
			return MathExt.roundToNearest(x, interval, mathContext.roundingMode);
		}

		public function roundToNearestFraction(x:Fraction, interval:Fraction):Fraction {
			return null; // TODO: Implement.
		}

		public function roundToNearestComplex(x:ComplexNumber, interval:ComplexNumber):ComplexNumber {
			if ((interval.re <= 0) || (interval.im <= 0))
				throw new RangeError('The interval argument (' + interval + ') must be a positive number.');
			return new ComplexNumber(
				MathExt.roundToNearest(x.re, interval.re, mathContext.roundingMode), MathExt.roundToNearest(x.im, interval.im, mathContext.roundingMode));
		}

		/// ------------------------------------------------------------------------------------------------------------------------------------

		protected var _clamp:Function;
		/**
		 * Forces a value into the specified range.
		 * For complex numbers, applies separately to real and imaginary parts.
		 *
		 * @param x:(Number | Fraction | ComplexNumber) A value.
		 * @param min:(Number | Fraction | ComplexNumber) The minimum bound of the range.
		 * @param max:(Number | Fraction | ComplexNumber) The maximum bound of the range.
		 * @return (Number | Fraction | ComplexNumber) The clamped value.
		 *
		 * @throws ArithmeticError - if min is greater than max.
		 */
		public function get clamp():Function {
			if (!_clamp) {
				/*var pf2:PolyFun2 = new PolyFun2(mathLib.typeConverter);
				pf2.addSignature(Number, Number, clampReal);
				pf2.addSignature(Fraction, Fraction, clampFraction);
				pf2.addSignature(ComplexNumber, ComplexNumber, clampComplex);
				_clamp = pf2.call;*/
				// TODO: Need PolyFun3 or PolyFunN to use polymorphism here. Alternatively, use ifs and type converter.
				_clamp = clampReal;
			}
			return _clamp;
		}

		public function clampReal(x:Number, min:Number, max:Number):Number {
			if (max < min)
				throw new ArithmeticError('The min argument (' + min + ') is greater than the max argument (' + max + ').');
			return (x < min) ? min : (x > max) ? max : x;
		}

		public function clampFraction(x:Fraction, min:Fraction, max:Fraction):Fraction {
			var minValue:Number = min.valueOf();
			var maxValue:Number = max.valueOf();
			if (maxValue < minValue)
				throw new ArithmeticError('The min argument (' + min + ') is greater than the max argument (' + max + ').');
			var xValue:Number = x.valueOf();
			return (xValue < minValue) ? min : (xValue > maxValue) ? max : x;
		}

		public function clampComplex(x:ComplexNumber, min:ComplexNumber, max:ComplexNumber):ComplexNumber {
			if (max.re < min.re)
				throw new ArithmeticError('The min argument (' + min + ') is greater than the max argument (' + max + ') in the real part.');
			if (max.im < min.im)
				throw new ArithmeticError('The min argument (' + min + ') is greater than the max argument (' + max + ') in the imaginary part.');
			return new ComplexNumber(
				(x.re < min.re) ? min.re : (x.re > max.re) ? max.re : x.re,
				(x.im < min.im) ? min.im : (x.im > max.im) ? max.im : x.im);
		}

		/// ------------------------------------------------------------------------------------------------------------------------------------

		protected var _gcd:Function;
		/**
		 * Returns the greatest common divisor of two or more values.
		 *
		 * @see https://en.wikipedia.org/wiki/Euclidean_algorithm
		 *
		 * @param x:(Number | Fraction) A value.
		 * @param y:(Number | Fraction) A value.
		 * @param ... rest (Number | Fraction) A value.
		 * @return (Number | Fraction) The greatest common divisor of all the values.
		 *
		 * @throws ArithmeticError - (Number) if any argument is not a whole number.
		 */
		public function get gcd():Function {
			if (!_gcd) {
				/*var pf2:PolyFun2 = new PolyFun2(mathLib.typeConverter);
				pf2.addSignature(Number, Number, gcdReal);
				pf2.addSignature(Fraction, Fraction, gcdFraction);
				_gcd = pf2.call;*/
				// TODO: Need PolyFunN to use polymorphism here. Alternatively, use ifs and type converter.
				_gcd = gcdReal;
			}
			return _gcd;
		}

		public function gcdReal(x:*, y:*, ... rest):* {
			var result:Number = _gcdReal(x, y);
			if (rest.length) {
				for (var i:int = 0; i < rest.length; ++i)
					result = _gcdReal(result, rest[i]);
			}
			return result;
		}

		protected function _gcdReal(x:*, y:*):* {
			if (!MathExt.isInteger(x) || !MathExt.isInteger(y))
				throw new ArithmeticError('The arguments must be integer numbers.');

			var r:Number;
			while (y != 0) {
				r = x % y;
				x = y;
				y = r;
			}
			return Math.abs(x);
		}

		public function gcdFraction(x:Fraction, y:Fraction):Fraction {
			return null; // TODO: Implement.
			// gcd(a / b, c / d) = gcd(a, c) / gcd(b, d)
			//return new Fraction(gcd(P["n"], this["n"]), P["d"] * this["d"] / gcd(P["d"], this["d"]));
		}

		/// ------------------------------------------------------------------------------------------------------------------------------------

		protected var _lcm:Function;
		/**
		 * Returns the least common multiple of two or more values.
		 *
		 * @param x:(Number | Fraction) A value.
		 * @param y:(Number | Fraction) A value.
		 * @param ... rest (Number | Fraction) A value.
		 * @return (Number | Fraction) The least common multiple of all the values.
		 *
		 * @throws ArithmeticError - (Number) if any argument is not a whole number.
		 */
		public function get lcm():Function {
			if (!_lcm) {
				/*var pf2:PolyFun2 = new PolyFun2(mathLib.typeConverter);
				pf2.addSignature(Number, Number, lcmReal);
				pf2.addSignature(Fraction, Fraction, lcmFraction);
				_lcm = pf2.call;*/
				// TODO: Need PolyFunN to use polymorphism here. Alternatively, use ifs and type converter.
				_lcm = lcmReal;
			}
			return _lcm;
		}

		public function lcmReal(x:*, y:*, ... rest):* {
			var result:Number = _lcmReal(x, y);
			if (rest.length) {
				for (var i:int = 0; i < rest.length; ++i)
					result = _lcmReal(result, rest[i]);
			}
			return result;
		}

		protected function _lcmReal(x:*, y:*):* {
			if (!MathExt.isInteger(x) || !MathExt.isInteger(y))
				throw new ArithmeticError('The arguments must be integer numbers.');

			return x * y / _gcdReal(x, y);
		}

		public function lcmFraction(x:Fraction, y:Fraction):Fraction {
			return null; // TODO: Implement.
			// lcm(a / b, c / d) = lcm(a, c) / gcd(b, d)
			//if (P["n"] === 0 && this["n"] === 0) {
				//return new Fraction;
			//}
			//return new Fraction(P["n"] * this["n"] / gcd(P["n"], this["n"]), gcd(P["d"], this["d"]));
		}

		/***********************************************************************************************************************************************/

		/**
		 * Logarithms.
		 */

		protected var _ln:Function;
		public function ln(x:*):* {
			return Math.log(x);
		}

		/// ------------------------------------------------------------------------------------------------------------------------------------

		protected var _log2:Function;
		public function log2(x:*):* {
			return Math.log(x) * Math.LOG2E;
		}

		/// ------------------------------------------------------------------------------------------------------------------------------------

		protected var _log10:Function;
		public function log10(x:*):* {
			return Math.log(x) * Math.LOG10E;
		}

		/// ------------------------------------------------------------------------------------------------------------------------------------

		protected var _logN:Function;
		public function logN(n:*, x:*):* {
			var d:Number = Math.log(n);
			checkDiv0(d);
			return Math.log(x) / d;
		}

		/***********************************************************************************************************************************************/

		/**
		 * Powers.
		 */

		protected var _square:Function;
		public function square(x:*):* {
			return x * x;
		}

		/// ------------------------------------------------------------------------------------------------------------------------------------

		protected var _cube:Function;
		public function cube(x:*):* {
			return x * x * x;
		}

		/// ------------------------------------------------------------------------------------------------------------------------------------

		protected var _pow:Function;
		public function pow(x:*, n:*):* {
			// TODO: Test with negative x and n.
			return Math.pow(x, n);
		}

		/// ------------------------------------------------------------------------------------------------------------------------------------

		protected var _exp10:Function;
		public function exp10(x:*):* {
			return Math.pow(10, x);
		}

		/// ------------------------------------------------------------------------------------------------------------------------------------

		protected var _exp:Function;
		public function exp(x:*):* {
			return Math.exp(x);
		}

		/***********************************************************************************************************************************************/

		/**
		 * Roots.
		 */

		protected var _sqrt:Function;
		public function sqrt(x:*):* {
			return Math.sqrt(x);
		}

		/// ------------------------------------------------------------------------------------------------------------------------------------

		protected var _curt:Function;
		public function curt(x:*):* {
			return (x < 0) ? -Math.pow(-x, 1 / 3) : Math.pow(x, 1 / 3);
		}

		/// ------------------------------------------------------------------------------------------------------------------------------------

		protected var _nthRoot:Function;
		public function nthRoot(n:*, x:*):* {
			// TODO: Take a look at MathJS's nthRoot!
			// TODO: (n % 2 != 0) ? (n % 2) returns -1 if (n < 0).
			return (((n % 2) == 1) && (x < 0)) ? -Math.pow(-x, 1 / n) : Math.pow(x, 1 / n);
		}
	}
}
