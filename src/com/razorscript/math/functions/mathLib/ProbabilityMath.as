package com.razorscript.math.functions.mathLib {
	import com.razorscript.math.MathExt;
	import com.razorscript.math.RoundingMode;
	import com.razorscript.math.errors.ArithmeticError;
	import com.razorscript.math.functions.MathLib;

	/**
	 * Probability math functions.
	 *
	 * @author Gene Pavlovsky
	 */
	public class ProbabilityMath extends MathBase {
		public function ProbabilityMath(mathLib:MathLib) {
			super(mathLib);
		}

		/**
		 *
		 * @param x
		 * @return
		 *
		 * @throws ArithmeticError - if x is negative or not a whole number.
		 */
		public function fac(x:*):* {
			if (!MathExt.isInteger(x) || (x < 0))
				throw new ArithmeticError('The argument be a non-negative integer number.');
			var result:Number = 1;
			while (x > 1)
				result *= x--;
			return result;
		}

		public function npr(x:*, y:*):* {
			return fac(x) / fac(y);
		}

		public function ncr(x:*, y:*):* {
			return fac(x) / fac(y) / fac(x - y);
		}

		/**
		 * Returns a pseudo-random number n with a specified number of fractional digits, where (0 <= n < 1).
		 * If fractionDigits is a negative value, no rounding is performed.
		 *
		 * @param fractionDigits An integer between 0 and 17, inclusive, that represents the desired number of decimal places, or a negative number, in which case no rounding is performed.
		 * @return A pseudo-random number.
		 *
		 * @throws RangeError - if fractionDigits is out of range.
		 */
		public function random(fractionDigits:* = -1):* {
			return (fractionDigits < 0) ? Math.random() : MathExt.roundToDigits(Math.random(), fractionDigits, RoundingMode.FLOOR);
		}

		/**
		 * Returns a pseudo-random number n with a specified number of fractional digits, where (min <= n < max).
		 * If fractionDigits is a negative value, no rounding is performed.
		 *
		 * @param min A number that represents the minimum value (inclusive) of the result.
		 * @param max A number that represents the maximum value (exclusive) of the result.
		 * @param fractionDigits An integer between 0 and 17, inclusive, that represents the desired number of decimal places, or a negative number, in which case no rounding is performed.
		 * @return A pseudo-random number.
		 *
		 * @throws ArithmeticError - if min is greater than max.
		 * @throws RangeError - if fractionDigits is out of range.
		 */
		public function randomRange(min:* = 0, max:* = 1, fractionDigits:* = -1):* {
			if (max < min)
				throw new ArithmeticError('The min argument (' + min + ') is greater than the max argument (' + max + ').');
			return MathExt.randomRange(min, max, fractionDigits);
		}

		/**
		 * Returns a pseudo-random signed integer value (stored in type Number) n with a specified number of fractional digits, where (min <= n <= max).
		 *
		 * @param min A number that represents the minimum value (inclusive) of the result.
		 * @param max A number that represents the maximum value (inclusive) of the result.
		 * @return A pseudo-random signed integer.
		 *
		 * @throws ArithmeticError - if min is greater than max.
		 * @throws RangeError - if fractionDigits is out of range.
		 *
		 */
		public function randomRangeInt(min:*, max:*):* {
			if (max < min)
				throw new ArithmeticError('The min argument (' + min + ') is greater than the max argument (' + max + ').');
			return MathExt.randomRangeInt(min, max);
		}
	}
}
