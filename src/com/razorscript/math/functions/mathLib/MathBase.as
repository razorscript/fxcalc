package com.razorscript.math.functions.mathLib {
	import com.razorscript.math.FPMode;
	import com.razorscript.math.MathContext;
	import com.razorscript.math.MathExt;
	import com.razorscript.math.errors.ArithmeticError;
	import com.razorscript.math.functions.MathLib;
	import com.razorscript.math.objects.Fraction;
	
	/**
	 * Base class for math function libraries.
	 *
	 * @author Gene Pavlovsky
	 */
	public class MathBase {
		public function MathBase(mathLib:MathLib) {
			this.mathLib = mathLib;
		}
		
		public var mathLib:MathLib;
		public var mathContext:MathContext;
		
		[Inline]
		protected final function get isMathicFP():Boolean {
			return mathContext.fpMode == FPMode.MATHIC;
		}
		
		[Inline]
		protected final function integerFraction(x:Number):* {
			return mathContext.useIntegerFractions ? new Fraction(x) : x;
		}
		
		[Inline]
		protected final function checkDiv0(divisor:Number):void {
			if (isZero(divisor))
				throw new ArithmeticError('Division by zero.');
		}
		
		// TODO: Rename to *Real? Add type-specific isZero, nearlyZero, isEqual, nearlyEqual?
		
		[Inline]
		protected final function nearlyZero(x:Number):Boolean {
			return MathExt.nearlyZero(x, mathContext.absoluteError);
		}
		
		[Inline]
		protected final function isZero(x:Number):Boolean {
			return isMathicFP ? MathExt.nearlyZero(x, mathContext.absoluteError) : (x == 0);
		}
		
		[Inline]
		protected final function nearlyEqual(x:Number, y:Number):Boolean {
			return MathExt.nearlyEqual(x, y, mathContext.absoluteError, mathContext.relativeError);
		}
		
		[Inline]
		protected final function isEqual(x:Number, y:Number):Boolean {
			return isMathicFP ? MathExt.nearlyEqual(x, y, mathContext.absoluteError, mathContext.relativeError) : (x == y);
		}
		
		[Inline]
		protected final function angleToRadians(x:Number):Number {
			return MathExt.angleToRadians(x, mathContext.angleUnit);
		}
		
		[Inline]
		protected final function angleFromRadians(x:Number):Number {
			return MathExt.angleFromRadians(x, mathContext.angleUnit);
		}
	}
}
