package com.razorscript.math.functions.mathLib {
	import com.razorscript.math.AngleUnit;
	import com.razorscript.math.FPMode;
	import com.razorscript.math.MathExt;
	import com.razorscript.math.errors.ArithmeticError;
	import com.razorscript.math.functions.MathLib;

	/**
	 * Trigonometry math functions.
	 *
	 * @author Gene Pavlovsky
	 */
	public class TrigonometryMath extends MathBase {
		public function TrigonometryMath(mathLib:MathLib) {
			super(mathLib);
		}

		/**
		 * Converts an angle to quadrants (1 quadrant = 1/4 turn = 90° = π/2 radians = 100 grad).
		 * If the angle is an integer multiple of a quadrant, returns the angle in quadrants, adding or subtracting an appropriate number of full turns if necessary to get a value between 0 and 3 (inclusive).
		 * Otherwise (if the angle is oblique), returns -1.
		 *
		 * @param x An angle.
		 * @return The angle converted to quadrants (between 0 and 3, inclusive), if the angle is an integer multiple of a quadrant, otherwise -1.
		 */
		protected function _angleToQuads(x:Number):int {
			var angleUnit:AngleUnit = mathContext.angleUnit;
			var rightAngle:Number = (angleUnit == AngleUnit.RADIAN) ? MathExt.PI1_2 : (angleUnit == AngleUnit.DEGREE) ? 90 : 100;
			if (nearlyZero(x % rightAngle)) {
				x = Math.round(x / rightAngle);
				if (x < 0)
					x += 4 * Math.ceil(-x / 4);
				else if (x >= 4)
					x -= 4 * Math.floor(x / 4);
				return x;
			}
			return -1;
		}

		[Inline]
		protected final function angleToQuads(x:Number):int {
			return isMathicFP ? _angleToQuads(x) : -1;
		}

		public function sin(x:*):* {
			// TODO: Test this.
			var a:int = angleToQuads(x);
			return (a < 0) ? Math.sin(angleToRadians(x)) : (a == 1) ? 1 : (a == 3) ? -1 : 0;
		}

		public function cos(x:*):* {
			// TODO: Test this.
			var a:int = angleToQuads(x);
			return (a < 0) ? Math.cos(angleToRadians(x)) : (a == 0) ? 1 : (a == 2) ? -1 : 0;
		}

		/**
		 *
		 * @param x
		 * @return
		 *
		 * @throws ArithmeticError - if cos(x) is 0.
		 */
		public function tan(x:*):* {
			// TODO: Test this.
			var a:int = angleToQuads(x);
			if (a < 0)
				return Math.tan(angleToRadians(x));
			if ((a == 1) || (a == 3))
				throw new ArithmeticError('Division by zero.');
			return 0;
		}

		/**
		 *
		 * @param x
		 * @return
		 *
		 * @throws ArithmeticError - if sin(x) is 0.
		 */
		public function cot(x:*):* {
			// TODO: Test this.
			var a:int = angleToQuads(x);
			if (a < 0) {
				x = angleToRadians(x);
				return Math.cos(x) / Math.sin(x);
			}
			if ((a == 0) || (a == 2))
				throw new ArithmeticError('Division by zero.');
			return 0;
		}

		public function sec(x:*):* {
			// TODO: Test this.
			var a:int = angleToQuads(x);
			if (a < 0)
				return 1 / Math.cos(angleToRadians(x));
			if ((a == 1) || (a == 3))
				throw new ArithmeticError('Division by zero.');
			return (a == 0) ? 1 : -1;
		}

		public function csc(x:*):* {
			// TODO: Test this.
			var a:int = angleToQuads(x);
			if (a < 0)
				return 1 / Math.sin(angleToRadians(x));
			if ((a == 0) || (a == 2))
				throw new ArithmeticError('Division by zero.');
			return (a == 1) ? 1 : -1;
		}

		/***********************************************************************************************************************************************/

		/**
		 * Inverse trigonometric functions.
		 */

		public function asin(x:*):* {
			return angleFromRadians(Math.asin(x));
		}

		public function acos(x:*):* {
			return angleFromRadians(Math.acos(x));
		}

		public function atan(x:*):* {
			return angleFromRadians(Math.atan(x));
		}

		public function atanYX(y:*, x:*):* {
			return angleFromRadians(Math.atan2(y, x));
		}

		public function acot(x:*):* {
			// TODO: Test this.
			checkDiv0(x);
			return angleFromRadians(Math.atan(1 / x));
		}

		public function asec(x:*):* {
			// TODO: Test this.
			checkDiv0(x);
			return angleFromRadians(Math.acos(1 / x));
		}

		public function acsc(x:*):* {
			// TODO: Test this.
			checkDiv0(x);
			return angleFromRadians(Math.asin(1 / x));
		}

		/***********************************************************************************************************************************************/

		/**
		 * Hyperbolic functions.
		 */

		public function sinh(x:*):* {
			return (Math.exp(x) - Math.exp(-x)) / 2;
		}

		public function cosh(x:*):* {
			return (Math.exp(x) + Math.exp(-x)) / 2;
		}

		public function tanh(x:*):* {
			return (Math.exp(x) - Math.exp(-x)) / (Math.exp(x) + Math.exp(-x));
		}

		public function csch(x:*):* {
			// TODO: Test this.
			checkDiv0(x);
			return 2 / (Math.exp(x) - Math.exp(-x));
		}

		public function sech(x:*):* {
			return 2 / (Math.exp(x) + Math.exp(-x))
		}

		public function coth(x:*):* {
			// TODO: Test this.
			checkDiv0(x);
			var e:Number = Math.exp(2 * x);
			return (e + 1) / (e - 1);
		}

		/***********************************************************************************************************************************************/

		/**
		 * Inverse hyperbolic functions.
		 */

		public function asinh(x:*):* {
			return Math.log(x + Math.sqrt(x * x + 1));
		}

		public function acosh(x:*):* {
			return Math.log(x + Math.sqrt(x * x - 1));
		}

		public function atanh(x:*):* {
			// TODO: Test for division by zero.
			return Math.log(x + Math.sqrt(x * x + 1)) / Math.log(x + Math.sqrt(x * x - 1));
		}

		public function acsch(x:*):* {
			// TODO: Test for division by zero.
			return 1 / Math.log(x + Math.sqrt(x * x + 1));
		}

		public function asech(x:*):* {
			// TODO: Test for division by zero.
			return 1 / Math.log(x + Math.sqrt(x * x - 1));
		}

		public function acoth(x:*):* {
			// TODO: Test for division by zero.
			return Math.log(x + Math.sqrt(x * x - 1)) / Math.log(x + Math.sqrt(x * x + 1));
		}
	}
}
