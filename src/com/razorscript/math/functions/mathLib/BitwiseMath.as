package com.razorscript.math.functions.mathLib {
	import com.razorscript.math.functions.MathLib;
	
	/**
	 * Bitwise math functions.
	 *
	 * @author Gene Pavlovsky
	 */
	public class BitwiseMath extends MathBase {
		public function BitwiseMath(mathLib:MathLib) {
			super(mathLib);
		}
		
		public function not(x:*):* {
			return ~x;
		}
		
		public function and(x:*, y:*):* {
			return x & y;
		}
		
		public function or(x:*, y:*):* {
			return x | y;
		}
		
		public function xor(x:*, y:*):* {
			return (x & ~y) | (~x & y);
		}
		
		public function shiftLeft(x:*, y:*):* {
			return x << y;
		}
		
		public function shiftRight(x:*, y:*):* {
			return x >> y;
		}
		
		public function shiftRightUnsigned(x:*, y:*):* {
			return x >>> y;
		}
	}
}
