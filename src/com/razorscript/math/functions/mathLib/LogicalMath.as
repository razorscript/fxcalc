package com.razorscript.math.functions.mathLib {
	import com.razorscript.math.functions.MathLib;
	import com.razorscript.math.functions.polymorphism.PolyFun1;
	import com.razorscript.math.functions.polymorphism.PolyFun2;
	import com.razorscript.math.objects.ComplexNumber;
	import com.razorscript.math.objects.Fraction;

	/**
	 * Logical math functions.
	 *
	 * @author Gene Pavlovsky
	 */
	public class LogicalMath extends MathBase {
		public function LogicalMath(mathLib:MathLib) {
			super(mathLib);

			var pf1:PolyFun1 = new PolyFun1(mathLib.typeConverter);
			pf1.addSignature(Number, booleanReal);
			pf1.addSignature(Fraction, booleanFraction);
			pf1.addSignature(ComplexNumber, booleanComplex);
			_boolean = pf1.call;
		}

		protected var _boolean:Function;
		/**
		 * Converts a value to Boolean.
		 *
		 * @param x:(Boolean | Number | Fraction | ComplexNumber) A value.
		 * @return Boolean - The value converted to Boolean.
		 */
		public function boolean(x:*):Boolean {
			return (x is Boolean) ? x : _boolean(x);
		}

		public function booleanReal(x:Number):Boolean {
			return !isZero(x);
		}

		public function booleanFraction(x:Fraction):Boolean {
			return !isZero(x.n);
		}

		public function booleanComplex(x:ComplexNumber):Boolean {
			return !isZero(x.re) || !isZero(x.im);
		}

		/// ------------------------------------------------------------------------------------------------------------------------------------

		/**
		 * Returns the logical NOT of a value.
		 *
		 * @param x:(Boolean | Number | Fraction | ComplexNumber) A value.
		 * @return Boolean - The logical NOT of the values.
		 */
		public function not(x:*):Boolean {
			return !boolean(x);
		}

		/**
		 * Returns the logical AND of two values.
		 *
		 * @param x:(Boolean | Number | Fraction | ComplexNumber) A value.
		 * @param y:(Boolean | Number | Fraction | ComplexNumber) A value.
		 * @return Boolean - The logical AND of the values.
		 */
		public function and(x:*, y:*):Boolean {
			return boolean(x) && boolean(y);
		}

		/**
		 * Returns the logical OR of two values.
		 *
		 * @param x:(Boolean | Number | Fraction | ComplexNumber) A value.
		 * @param y:(Boolean | Number | Fraction | ComplexNumber) A value.
		 * @return Boolean - The logical OR of the values.
		 */
		public function or(x:*, y:*):Boolean {
			return boolean(x) || boolean(y);
		}

		/**
		 * Returns the logical XOR of two values.
		 *
		 * @param x:(Boolean | Number | Fraction | ComplexNumber) A value.
		 * @param y:(Boolean | Number | Fraction | ComplexNumber) A value.
		 * @return Boolean - The logical XOR of the values.
		 */
		public function xor(x:*, y:*):Boolean {
			x = boolean(x);
			y = boolean(y);
			return (x && !y) || (!x && y);
		}
	}
}
