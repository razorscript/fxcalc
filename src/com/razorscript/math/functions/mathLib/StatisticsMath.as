package com.razorscript.math.functions.mathLib {
	import com.razorscript.math.functions.MathLib;

	/**
	 * Statistics math functions.
	 *
	 * @author Gene Pavlovsky
	 */
	public class StatisticsMath extends MathBase {
		public function StatisticsMath(mathLib:MathLib) {
			super(mathLib);
		}

		/**
		 * Returns the minimum value of a list of mathematical objects.
		 *
		 * @param ... args A mathematical object.
		 * @return The minimum value.
		 */
		public function min(... args):* {
			return Math.min.apply(null, args);
		}

		/**
		 * Returns the maximum value of a list of mathematical objects.
		 *
		 * @param ... args A mathematical object.
		 * @return The maximum value.
		 */
		public function max(... args):* {
			return Math.max.apply(null, args);
		}

		/**
		 * Returns the mean value of a list of mathematical objects.
		 *
		 * @param ... args A mathematical object.
		 * @return The mean of all values.
		 */
		public function mean(... args):* {
			// TODO: Implement.
			return null;
		}

		/**
		 * Returns the median value of a list of mathematical objects.
		 *
		 * @param ... args A mathematical object.
		 * @return The median.
		 */
		public function median(... args):* {
			// TODO: Implement.
			return null;
		}

		/**
		 * Returns the sum of a list of mathematical objects.
		 *
		 * @param ... args A mathematical object.
		 * @return The sum of all values.
		 */
		public function sum(... args):* {
			// TODO: Implement.
			return null;
		}
	}
}
