package com.razorscript.math.functions.mathLib {
	import com.razorscript.math.functions.MathLib;
	
	/**
	 * Relational math functions.
	 *
	 * @author Gene Pavlovsky
	 */
	public class RelationalMath extends MathBase {
		public function RelationalMath(mathLib:MathLib) {
			super(mathLib);
		}
		
		public function equal(x:*, y:*):Boolean {
			return nearlyEqual(x, y);
		}
		
		public function unequal(x:*, y:*):Boolean {
			return !nearlyEqual(x, y);
		}
		
		public function larger(x:*, y:*):Boolean {
			return !nearlyEqual(x, y) && (x > y);
		}
		
		public function largerEq(x:*, y:*):Boolean {
			return nearlyEqual(x, y) || (x > y);
		}
		
		public function smaller(x:*, y:*):Boolean {
			return !nearlyEqual(x, y) && (x < y);
		}
		
		public function smallerEq(x:*, y:*):Boolean {
			return nearlyEqual(x, y) || (x <= y);
		}
		
		public function compare(x:*, y:*):* {
			return nearlyEqual(x, y) ? 0 : (x > y) ? 1 : -1;
		}
	}
}
