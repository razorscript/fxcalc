package com.razorscript.math {
	import com.razorscript.utils.StrUtil;
	import flash.utils.IDataInput;
	import flash.utils.IDataOutput;
	import flash.utils.IExternalizable;

	/**
	 * A 64-bit unsigned integer number.
	 *
	 * @author Gene Pavlovsky
	 */
	public class UInt64 implements IExternalizable {
		/** The largest representable UInt64, with a value of 18,446,744,073,709,551,615. */
		public static const MAX_VALUE:UInt64 = new UInt64(0xffffffff, 0xffffffff);
		/** The smallest representable UInt64, with a value of 0. */
		public static const MIN_VALUE:UInt64 = new UInt64();

		/**
		 * Constructor.
		 *
		 * @param high The most significant 32 bits of the number.
		 * @param low The least significant 32 bits of the number.
		 */
		public function UInt64(high:uint = 0, low:uint = 0) {
			w32 = high;
			w0 = low;
		}

		public function get number():Number {
			return w32 * POW_2_32 + w0;
		}

		public function set number(value:Number):void {
			if (value < 0) {
				value = Math.abs(value);
				w32 = 0xffffffff - value / POW_2_32;
				w0 = 0xffffffff - value % POW_2_32;
			}
			else {
				w32 = value / POW_2_32;
				w0 = value % POW_2_32;
			}
		}

		protected static const POW_2_32:Number = 4294967296;

		/**
		 * The most significant 32 bits of the number.
		 */
		public function get high():uint {
			return w32;
		}

		public function set high(value:uint):void {
			w32 = value;
		}

		/**
		 * The least significant 32 bits of the number.
		 */
		public function get low():uint {
			return w0;
		}

		public function set low(value:uint):void {
			w0 = value;
		}

		/**
		 * The number as a string of binary digits.
		 *
		 * @throws RangeError - if value's length is not 64, or value contains invalid digits.
		 */
		public function get bitString():String {
			return StrUtil.lpad(w32.toString(2), '0', 32) + StrUtil.lpad(w0.toString(2), '0', 32);
		}

		public function set bitString(value:String):void {
			parseDigits(value, 2);
		}

		/**
		 * The number as a string of hexadecimal digits.
		 *
		 * @throws RangeError - if value's length is not 16, or value contains invalid digits.
		 */
		public function get hexString():String {
			return StrUtil.lpad(w32.toString(16), '0', 8) + StrUtil.lpad(w0.toString(16), '0', 8);
		}

		public function set hexString(value:String):void {
			parseDigits(value, 16);
		}

		/** The base argument must be either 2 (binary) or 16 (hexadecimal). */
		protected function parseDigits(value:String, base:int):void {
			const wordDigits:int = (base == 2) ? 32 : 8;
			if (value.length != (wordDigits << 1))
				throw new RangeError('Value (' + value + ') is not ' + (wordDigits << 1) + ' digits long (actual length: ' + value.length + ' digits).');
			var bits0:Number = parseInt(value.substr(wordDigits), base);
			var bits32:Number = parseInt(value.substr(0, wordDigits), base);
			if ((bits0 != bits0) || (bits32 != bits32))
				throw new RangeError('Value (' + value + ') contains invalid digit(s) (only ' + ((base == 2) ? 'binary' : 'hexadecimal') + ' digits are allowed).');
			w0 = bits0;
			w32 = bits32;
		}

		/**
		 * Whether all the bits in this number are zero.
		 */
		public function get isZero():Boolean {
			return !(w0 || w32);
		}

		/**
		 * Returns a new UInt64 that is identical to this one.
		 * @return
		 */
		public function clone():UInt64 {
			return new UInt64(w32, w0);
		}

		public function toString():String {
			// TODO: Implement printing decimal digits.
			return '[UInt64 hexString=0x' + StrUtil.splitw(hexString, 8).join('_') + ']';
		}

		public function writeExternal(output:IDataOutput):void {
			output.writeUnsignedInt(w32);
			output.writeUnsignedInt(w0);
		}

		public function readExternal(input:IDataInput):void {
			w32 = input.readUnsignedInt();
			w0 = input.readUnsignedInt();
		}

		/** Word 0, holding bits 0 to 31. */
		protected var w0:uint;
		/** Word 1, holding bits 32 to 63. */
		protected var w32:uint;
	}
}
