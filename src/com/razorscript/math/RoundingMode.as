package com.razorscript.math {
	import com.razorscript.utils.enum.EnumBase;
	import com.razorscript.utils.enum.EnumUtil;
	
	/**
	 * Defines constants for rounding modes.
	 *
	 * @author Gene Pavlovsky
	 */
	public class RoundingMode extends EnumBase {
		/**
		 * Rounding mode to round away from zero.
		 * Always increments the digit prior to a non-zero discarded fraction.
		 * Note that this rounding mode never decreases the magnitude of the calculated value.
		 */
		[Enum]
		public static const AWAY_FROM_ZERO:RoundingMode = factory();
		
		/**
		 * Rounding mode to round towards zero.
		 * Never increments the digit prior to a discarded fraction (i.e. truncates).
		 * Note that this rounding mode never increases the magnitude of the calculated value.
		 */
		[Enum]
		public static const TOWARDS_ZERO:RoundingMode = factory();
		
		/**
		 * Rounding mode to round towards positive infinity.
		 * Note that this rounding mode never decreases the calculated value.
		 */
		[Enum]
		public static const CEILING:RoundingMode = factory();
		
		/**
		 * Rounding mode to round towards negative infinity.
		 * Note that this rounding mode never increases the calculated value.
		 */
		[Enum]
		public static const FLOOR:RoundingMode = factory();
		
		/**
		 * Rounding mode to round towards the nearest neighbor unless both neighbors are equidistant, in which case round away from zero.
		 */
		[Enum]
		public static const HALF_AWAY_FROM_ZERO:RoundingMode = factory();
		
		/**
		 * Rounding mode to round towards the nearest neighbor unless both neighbors are equidistant, in which case round towards zero.
		 */
		[Enum]
		public static const HALF_TOWARDS_ZERO:RoundingMode = factory();
		
		/**
		 * Rounding mode to round towards the nearest neighbor unless both neighbors are equidistant, in which case round towards positive infinity.
		 */
		[Enum]
		public static const HALF_UP:RoundingMode = factory();
		
		/**
		 * Rounding mode to round towards the nearest neighbor unless both neighbors are equidistant, in which case round towards negative infinity.
		 */
		[Enum]
		public static const HALF_DOWN:RoundingMode = factory();
		
		/**
		 * Rounding mode to round towards the nearest neighbor unless both neighbors are equidistant, in which case, round towards the even neighbor.
		 * Note that this is the rounding mode that statistically minimizes cumulative error when applied repeatedly over a sequence of calculations.
		 * It is sometimes known as "Banker's rounding" and is chiefly used in the USA.
		 */
		[Enum]
		public static const HALF_EVEN:RoundingMode = factory();
		
		/**
		 * Rounding mode to round towards the nearest neighbor unless both neighbors are equidistant, in which case, round towards the odd neighbor.
		 * Note that this is the rounding mode that statistically minimizes cumulative error when applied repeatedly over a sequence of calculations.
		 */
		[Enum]
		public static const HALF_ODD:RoundingMode = factory();
		
		/**
		 * Rounding mode to return the original value without any rounding.
		 */
		[Enum]
		public static const NONE:RoundingMode = factory();
		
		/**
		 * Rounding mode to assert that the requested operation has an integer result, hence no rounding is necessary.
		 * If this rounding mode is specified on an operation that yields a non-integer result, an ArithmeticException is thrown.
		 */
		[Enum]
		public static const UNNECESSARY:RoundingMode = factory();
		
		public static var defaultValue:RoundingMode = HALF_AWAY_FROM_ZERO;
		
		protected static function factory():RoundingMode {
			return new RoundingMode(LOCK);
		}
		
		/**
		 * @private
		 */
		public function RoundingMode(lock:Object) {
			super(lock);
		}
		
		EnumUtil.registerClass(RoundingMode);
	}
}
