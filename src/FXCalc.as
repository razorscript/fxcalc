package {
	import com.razorscript.debug.Logger;
	import com.razorscript.fxcalc.Main;
	import com.razorscript.fxcalc.data.constants.AppConst;
	import feathers.system.DeviceCapabilities;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.geom.Rectangle;
	import starling.core.Starling;
	import starling.display.Stage;
	import starling.utils.SystemUtil;
	
	/**
	 * List of some popular phones.
	 * X220-Res - use this resolution with desktop profile for testing, to get realistic physical dimensions on ThinkPad X220's 1366x768 screen (125 dpi)
	 *
	 * Device			Resolution	DPI		X220-Res	720-Res
	 * ----------------------------------------------
	 * TP X220		1366x768		125
	 * IPS235V		1920x1080		 96
	 * iPhone 3		320x480			163		246x369		720x1080
	 * GalaxyS1		480x800			233		258x430		720x1200
	 * GalaxyS3		720x1280		306		297x528		720x1280
	 * iPhone 4		640x960			326		246x369		720x1080
	 * iPhone 5		640x1136		326		246x437		720x1278
	 * iPhone 6		750x1334		326		288x512		720x1280
	 * GalaxyS4		1080x1920		441		306x544		720x1280
	 * GalaxyN3		1080x1920		386		351x624		720x1280
	 * GalaxyS6		1440x2560		577		315x560		720x1280
	 * GalaxyN4		1440x2560		515		351x624		720x1280
	 */
	
	[SWF(width="306", height="544", frameRate="60")]
	
	/**
	 * Main app start-up class.
	 *
	 * @author Gene Pavlovsky
	 */
	public class FXCalc extends Sprite {
		public function FXCalc() {
			super();
			
			Logger.info(this, AppConst.NAME, AppConst.VERSION);
			
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			
			if (SystemUtil.isDesktop)
				DeviceCapabilities.dpi = 125;
			
			mouseEnabled = mouseChildren = false;

			// TODO: Add a launch image?
			//showLaunchImage();
			loaderInfo.addEventListener(Event.COMPLETE, onLoaderComplete);
		}
		
		private var _starling:Starling;
		/*private var _launchImage:Loader;
		private var _savedAutoOrients:Boolean;
		
		private function showLaunchImage():void {
			var filePath:String;
			var isPortraitOnly:Boolean = false;
			if(Capabilities.manufacturer.indexOf("iOS") >= 0)
			{
				var isCurrentlyPortrait:Boolean = this.stage.orientation == StageOrientation.DEFAULT || this.stage.orientation == StageOrientation.UPSIDE_DOWN;
				if(Capabilities.screenResolutionX == 1242 && Capabilities.screenResolutionY == 2208)
				{
					//iphone 6 plus
					filePath = isCurrentlyPortrait ? "Default-414w-736h@3x.png" : "Default-414w-736h-Landscape@3x.png";
				}
				else if(Capabilities.screenResolutionX == 1536 && Capabilities.screenResolutionY == 2048)
				{
					//ipad retina
					filePath = isCurrentlyPortrait ? "Default-Portrait@2x.png" : "Default-Landscape@2x.png";
				}
				else if(Capabilities.screenResolutionX == 768 && Capabilities.screenResolutionY == 1024)
				{
					//ipad classic
					filePath = isCurrentlyPortrait ? "Default-Portrait.png" : "Default-Landscape.png";
				}
				else if(Capabilities.screenResolutionX == 750)
				{
					//iphone 6
					isPortraitOnly = true;
					filePath = "Default-375w-667h@2x.png";
				}
				else if(Capabilities.screenResolutionX == 640)
				{
					//iphone retina
					isPortraitOnly = true;
					if(Capabilities.screenResolutionY == 1136)
					{
						filePath = "Default-568h@2x.png";
					}
					else
					{
						filePath = "Default@2x.png";
					}
				}
				else if(Capabilities.screenResolutionX == 320)
				{
					//iphone classic
					isPortraitOnly = true;
					filePath = "Default.png";
				}
			}

			if(filePath)
			{
				var file:File = File.applicationDirectory.resolvePath(filePath);
				if(file.exists)
				{
					var bytes:ByteArray = new ByteArray();
					var stream:FileStream = new FileStream();
					stream.open(file, FileMode.READ);
					stream.readBytes(bytes, 0, stream.bytesAvailable);
					stream.close();
					this._launchImage = new Loader();
					this._launchImage.loadBytes(bytes);
					this.addChild(this._launchImage);
					this._savedAutoOrients = this.stage.autoOrients;
					this.stage.autoOrients = false;
					if(isPortraitOnly)
					{
						this.stage.setOrientation(StageOrientation.DEFAULT);
					}
				}
			}
		}*/
		
		private function onLoaderComplete(e:Event):void {
			Logger.debug(this, 'onLoaderComplete()');
			Starling.multitouchEnabled = true;
			_starling = new Starling(Main, stage);
			_starling.supportHighResolutions = true;
			_starling.skipUnchangedFrames = true;
			
			/*CONFIG::debug {
				_starling.enableErrorChecking = true;
			}*/
			
			resizeStage();
			
			_starling.addEventListener(StarlingEvent.ROOT_CREATED, onStarlingRootCreated);
			_starling.start();
			
			stage.addEventListener(Event.RESIZE, onStageResize, false, int.MAX_VALUE, true);
			stage.addEventListener(Event.DEACTIVATE, onStageDeactivate, false, 0, true);
		}
		
		private function resizeStage():void {
			Logger.debug(this, 'Native stage:  ', stage.stageWidth + 'x' + stage.stageHeight);
			var starlingStage:Stage = _starling.stage;
			
			starlingStage.stageWidth = stage.stageWidth;
			starlingStage.stageHeight = stage.stageHeight;
			Logger.debug(this, 'Starling stage:', starlingStage.stageWidth + 'x' + starlingStage.stageHeight, 'scale', _starling.contentScaleFactor);
			CONFIG::debug {
				_starling.showStatsAt('right', 'bottom');
			}
		}
		
		private function onStarlingRootCreated(e:Object):void {
			Logger.debug(this, 'onStarlingRootCreated()');
			
			/*if (_launchImage) {
				removeChild(_launchImage);
				_launchImage.unloadAndStop(true);
				_launchImage = null;
				stage.autoOrients = _savedAutoOrients;
			}*/
		}
		
		private function onStageResize(e:Event):void {
			resizeStage();
			
			var viewPort:Rectangle = _starling.viewPort;
			viewPort.width = stage.stageWidth;
			viewPort.height = stage.stageHeight;
			_starling.viewPort = viewPort;
		}
		
		private function onStageDeactivate(e:Event):void {
			_starling.stop(true);
			stage.addEventListener(Event.ACTIVATE, onStageActivate, false, 0, true);
		}
		
		private function onStageActivate(e:Event):void {
			stage.removeEventListener(Event.ACTIVATE, onStageActivate);
			_starling.start();
		}
	}
}

class StarlingEvent {
	public static const ROOT_CREATED:String = "rootCreated";
}
