package {
	
	/**
	 * Contains embedded fonts.
	 *
	 * @author Gene Pavlovsky
	 */
	public class FontLibrary {
		[Embed(source	='/../assets/fonts/DejaVuSansMono.ttf',
			fontFamily	='DejaVuSansMono',
			fontStyle		='normal',
			fontWeight	='normal',
			unicodeRange='U+0020-007E,U+00A1-024F,U+0370-03FF,U+1E00-1EFF,U+2000-209F,U+20A0-20CF,U+2100-218F,U+2190-21FF,U+2200-22FF,U+2300-23FF,U+2423,U+2580-259F,U+25A0-25FF')]
		public static const DEJAVU_SANS_MONO:Class;
		
		[Embed(source	='/../assets/fonts/DejaVuSansMono-Bold.ttf',
			fontFamily	='DejaVuSansMono',
			fontStyle		='normal',
			fontWeight	='bold',
			unicodeRange='U+0020-007E,U+00A1-024F,U+0370-03FF,U+1E00-1EFF,U+2000-209F,U+20A0-20CF,U+2100-218F,U+2190-21FF,U+2200-22FF,U+2300-23FF,U+2423,U+2580-259F,U+25A0-25FF')]
		public static const DEJAVU_SANS_MONO_BOLD:Class;
		
		[Embed(source	='/../assets/fonts/DejaVuSansMono-Oblique.ttf',
			fontFamily	='DejaVuSansMono',
			fontStyle		='italic',
			fontWeight	='normal',
			unicodeRange='U+0020-007E,U+00A1-024F,U+0370-03FF,U+1E00-1EFF')]
		public static const DEJAVU_SANS_MONO_OBLIQUE:Class;
		
		/*[Embed(source	='/../assets/fonts/DejaVuSansMono-BoldOblique.ttf',
			fontFamily	='DejaVuSansMono',
			fontStyle		='italic',
			fontWeight	='bold',
			unicodeRange='U+0020-007E,U+00A1-024F,U+0370-03FF,U+1E00-1EFF')]
		public static const DEJAVU_SANS_MONO_BOLD_OBLIQUE:Class;*/
	}
}
