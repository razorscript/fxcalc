#!/bin/sh
#
# Builds the project using ant.
#

cd "$(dirname "$0")"
ant -f build/build.xml "$@"
