#!/bin/sh
#
# Unless up to date, builds the app for the specified target using build.sh.
# Runs the app on the specified target.
#

default='android'
mobile='android'

android='android-adl'
android_adl='android-adl-desktop'

ios='ios-adl'
ios_adl='ios-adl-desktop'
ios_device='ios-device-interpreter'

desktop='desktop-adl'
desktop_adl='desktop-adl-desktop'

web='web-standalone'
web_browser='web-browser-local'

build_opts=
run_opts=

cd "$(dirname "$0")"
. ./scripts/share/app.sh

usage() {
	echo "Usage: $(basename "$0") [option]* [target]"
	echo
	echo "Default target: $default"
	echo
	echo "Run targets:"
	echo
	echo -e "  mobile                        Run the mobile app ($mobile)."
	echo -e "    android                     Run the Android app ($android)."
	echo -e "      android-adl[-option]      Run the Android app in ADL${android_adl:+ ($android_adl)}."
	echo -e "      android-device            Run the Android app on device."
	echo -e "        [-captive-runtime]        Package with a captive runtime."
	echo -e "    ios                         Run the iOS app ($ios)."
	echo -e "      ios-adl[-option]          Run the iOS app in ADL${ios_adl:+ ($ios_adl)}."
	echo -e "      ios-device                Run the iOS app on device${ios_device:+ ($ios_device)}."
	echo -e "        -interpreter              Use interpreter (deploys faster, runs slower)."
	echo -e "        -compiler                 Use compiler (deploys slower, runs faster)."
	echo
	echo -e "  desktop                       Run the desktop app ($desktop)."
	echo -e "    desktop-adl[-option]        Run the desktop app in ADL${desktop_adl:+ ($desktop_adl)}."
	echo
	echo -e "  web                           Run the web app ($web)."
	echo -e "    web-browser                 Run the web app in browser${web_browser:+ ($web_browser)}."
	echo -e "      -file                       Deploy to local filesystem, access via file URI."
	echo -e "      -local                      Deploy to local web server, access via HTTP URI."
	echo -e "      -remote                     Deploy to remote web server, access via HTTP URI."
	echo -e "    web-standalone              Run the web app in standalone Flash Player."
	echo
	echo "ADL options:"
	echo
	echo -e "  TARGET-adl                    Run the TARGET app in ADL."
	echo -e "    -desktop                      Use desktop profile."
	echo -e "    -desktop-ext                  Use extendedDesktop profile."
	echo -e "    -mobile-SCREEN_SIZE           Use mobileDevice profile with screen size set to SCREEN_SIZE."
	echo
	echo "Options:"
	echo
	echo -e "  --release                     Use release build configuration."
	echo -e "  --debug                       Use debug build configuration, connect to debugger."
	echo -e "  --fd                          Use an existing build from FlashDevelop or another IDE, implies --debug."
	echo -e "  --help                        Display this help and exit."
	exit 0
}

while test $# -gt 0; do
	case $1 in
		--release|--debug)
			build_opts="${build_opts} -Dapp.build=${1#--}"
			run_opts="${run_opts} $1"
		;;
		--fd)
			skip_build=1
			run_opts="${run_opts} $1"
		;;
		--help|help)
			usage
		;;
		-*)
			die "Unknown option: \"$1\""
		;;
		*)
			target=$1
		;;
	esac
	
	shift
done
[[ ! $target ]] && target=$default

while true; do
	ref=${target//-/_}
	[[ ${!ref} ]] || break
	target=${!ref}
done

[[ $skip_build ]] || ./build.sh $build_opts "build-${target%%-[-a-z]*}" || die "Unknown run target: \"$target\""
echo -e "\nrun-${target}:\n"
eval exec ./scripts/run-app.sh $run_opts \"$target\"
