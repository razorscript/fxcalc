#!/bin/sh
cd "$(dirname "$0")"

if test "$1" = "--clean"; then
	rm -fv razor.png razor.xml
	exit 0
fi

echo -n "Generating razor texture atlas... "
./gen-atlas.sh razor razor
code=$?
if test $code -eq 69; then
	echo -e '\nUp to date.'
elif test $code -eq 0; then
	echo done
else
	echo failed
	exit 1
fi
