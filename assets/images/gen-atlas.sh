#!/bin/sh
SHOEBOX="C:/Program Files (x86)/ShoeBox/ShoeBox.exe"

if test $# -lt 2; then
	echo "Usage: "$(basename "$0")" tex-dir name"
	exit 1
fi

os=$(uname -o)
test "${os,,}" = cygwin && tex_dir=$(cygpath -a -m "$1") || tex_dir=$(realpath "$1")

[[ ! -f $2.png ]] && rm -f "$2.xml"
xml="$2.xml"
test -f "$xml" || outdated=1
files=
for file in "$tex_dir"/*png; do
	test -z "$outdated" -a "$file" -nt "$xml" && outdated=1
	test "$files" && files="$files,$file" || files="$file"
done
test "$outdated" || exit 69

"$SHOEBOX" "plugin=shoebox.plugin.spriteSheet::PluginCreateSpriteSheet" "files=${files// /\\ }" "texPowerOfTwo=true" "renderDebugLayer=false" "texPadding=1" "animationMaxFrames=100" "texMaxSize=2048" "texExtrudeSize=0" "texSquare=false" "fileGenerate2xSize=false" "animationNameIds=@name_###.png" "texCropAlpha=true" "animationFrameIdStart=0" "fileFormatLoop=\t<SubTexture name=\"@ID\" x=\"@x\" y=\"@y\" width=\"@w\" height=\"@h\" frameX=\"-@fx\" frameY=\"-@fy\" frameWidth=\"@fw\" frameHeight=\"@fh\"/>\n" "scale=1" "useCssOverHack=false" "fileName=${2// /\\ }.xml" "fileFormatOuter=<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<TextureAtlas imagePath=\"@TexName\">\n@loop</TextureAtlas>"
mv "$tex_dir/$2".* .
