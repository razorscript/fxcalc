# ******************************************************************************
# Color scheme (common, applied before theme-specific script)
#

s,{background1},{base03},g
s,{background2},{base02},g
s,{content1},{base01},g
s,{content2},{base00},g
s,{content3},{base0},g
s,{content4},{base1},g
s,{highlight1},{base2},g
s,{highlight2},{base3},g
