#!/bin/bash
SOLARIZE="../../../../../git/flashdevelop-solarized/solarized/solarize.sh"
SOLARIZE_DARK_OPTS=
SOLARIZE_LIGHT_OPTS="-l"
SOLARIZE_OPTS="-s -q -c"

COLORS_PRE="pre.sed"
COLORS_DARK="dark.sed"
COLORS_LIGHT="light.sed"
COLORS_POST="post.sed"

error() {
	{
		test "$1" && echo -e "Error during: $@" || echo -e "Error"
	} >&2
	exit 1
}

gen_colors() {
	file=../solarized-$1.json
	eval local colors='${COLORS_'${1^^}'}' up_to_date sol_up_to_date
	[[ ! -f $file || solarized.json -nt $file || $COLORS_PRE -nt $file || $colors -nt $file || $COLORS_POST -nt $file ]] || up_to_date=1
	[[ ! -f solarize-$1.sed || gen-colors.sh -nt solarize-$1.sed ]] || sol_up_to_date=1
	[[ $up_to_date && $sol_up_to_date ]] && return
	echo "  solarized-$1.json"
	[[ $sol_up_to_date ]] || eval $SOLARIZE $SOLARIZE_OPTS '${SOLARIZE_'${1^^}_OPTS'}' >solarize-$1.sed
	sed -f $COLORS_PRE -f $colors -f $COLORS_POST -f solarize-$1.sed solarized.json >$file || error solarized-$1
	let ++gen_count
}

gen_colors dark
gen_colors light
[[ $gen_count ]] || exit 69
