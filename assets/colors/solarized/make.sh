#!/bin/bash
cd "$(dirname "$0")"

if test "$1" = "--clean"; then
	cd ..
	rm -fv solarized/solarize-{light,dark}.sed solarized-{light,dark}.json
	exit 0
fi

exec ./gen-colors.sh
