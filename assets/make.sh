#!/bin/bash
cd "$(dirname "$0")"

if test "$1" = "--clean"; then
	./colors/make.sh --clean
	./styles/make.sh --clean
	find . -name '*.min.json' -exec rm -fv {} +
	./images/make.sh --clean
	exit 0
fi

test "$1" = "--quiet" && exec >/dev/null

set -e
./colors/make.sh
echo
./styles/make.sh
echo
./min-json.sh
echo
./images/make.sh
echo
