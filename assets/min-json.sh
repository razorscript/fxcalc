#!/bin/bash
echo "Minifying JSON files: "
while read file; do
	outfile=${file%.json}.min.json
	test "$file" -ot "$outfile" && continue
	let ++count
	echo "'${file#./}' -> '${outfile#./}'"
	jsmin "$file" >"$outfile"
	jsonlint "$outfile" >/dev/null
	if test $? -ne 0; then
		errors=1
		echo
		rm -f "$outfile"
	fi
done < <(find . -maxdepth 2 -name '*.json' -and -not -name '*.min.json')

test "$count" || echo "Up to date."
test "$errors" && exit 1
exit 0
