#!/bin/bash
cd "$(dirname "$0")"

if test "$1" = "--clean"; then
	for make in */make.sh; do
		[[ -x "$make" ]] || continue
		"$make" --clean
	done
	exit 0
fi

echo "Generating styles: "
for make in */make.sh; do
	[[ -x "$make" ]] || continue
	"$make"
	code=$?
	if test $code -eq 0; then
		let ++gen_count
	elif test $code -ne 69; then
		exit $code
	fi
done
[[ $gen_count ]] || echo 'Up to date.'
