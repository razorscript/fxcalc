#include <stdio.h>

static const double num = 1.79769313486231570815e+308;

int main(int argc, char **argv) {
	if (argc == 1) {
		printf("const = %1.16e\n", num);
		return 0;
	}
	for (int i = 1; i < argc; ++i) {
	  double d;
		sscanf(argv[i], "%lf", &d);
		printf("arg%d = %1.16e\n", i, d);
	}
}
