package dto {
	
	/**
	 * ...
	 *
	 * @author Gene Pavlovsky
	 */
	public class TestDTO {
		public function TestDTO() {
		}
		
		public var bool:Boolean;
		public var arr:Array;
		public var vec:Vector.<Number>;
		public var num:Number;
		public var date:Date;
		public var str:String;
		public var obj:TestDTO;
		
		public function toString():String {
			return "[TestDTO bool=" + bool + " arr=[" + arr + "] vec=[" + vec + "] num=" + num + " date=" + date + " str=\"" + str + "\" obj=" + obj + "]";
		}
	}
}
