package com.razorscript.sandbox {
	
	/**
	 * ...
	 * @author Gene Pavlovsky
	 */
	public class GetSetTestData {
		public function GetSetTestData(data:Number = 0) {
			this.num = data;
		}
		
		public var _num:Number;
		
		public function get num():Number {
			return _num;
		}
		
		public function set num(value:Number):void {
			_num = value;
		}
		
		public function toString():String {
			return "[GetSetTestData data=" + num + "]";
		}
	}
}
