package com.razorscript.test {
	import com.razorscript.utils.StrUtil;
	import flash.events.TimerEvent;
	import flash.text.TextField;
	import flash.utils.getTimer;
	import flash.utils.Timer;
	/**
	 * ...
	 * @author Gene Pavlovsky
	 */
	public class TestBase {
		
		public function TestBase(logText:TextField, statusText:TextField) {
			this.logText = logText;
			this.statusText = statusText;
		}
		
		protected function assertEquals(value1:String, value2:String):Boolean {
			if (value1 != value2) {
				if (logAssertFail)
					appendLog('assertion failed: ' + value1 + ' == ' + value2);
				++numFailed;
			}
			else if (logAssertOk) {
				appendLog('assertion passed: ' + value1 + ' == ' + value2);
			}
			++numTotal;
			return value1 == value2;
		}
		
		protected function assertTrue(value:*):Boolean {
			if (!value) {
				if (logAssertFail)
					appendLog('assertion failed: ' + value + ' != ' + true);
				++numFailed;
			}
			else if (logAssertOk) {
				appendLog('assertion passed: ' + value + ' == ' + true);
			}
			++numTotal;
			return value;
		}
		
		protected function assertFalse(value:*):Boolean {
			if (value) {
				if (logAssertFail)
					appendLog('assertion failed: ' + value + ' != ' + false);
				++numFailed;
			}
			else if (logAssertOk) {
				appendLog('assertion passed: ' + value + ' == ' + false);
			}
			++numTotal;
			return !value;
		}
		
		protected function get log():String {
			return logText.text;
		}
		
		protected function set log(value:String):void {
			logText.text = value;
		}
		
		protected function get status():String {
			return statusText.text;
		}
		
		protected function set status(value:String):void {
			statusText.text = value;
		}
		
		protected function appendLog(... args):void {
			logText.appendText(args.join(' ') + '\n');
			logText.scrollV = logText.maxScrollV;
		}
		
		protected function formatAssertStatus():String {
			return StrUtil.format('{0:0{3}}/{1} ({2:.1p}) assertions failed.', numFailed, numTotal, numTotal ? (numFailed / numTotal) : 0, numFailed.toString().length);
		}
		
		protected function resetAssertCounters():void {
			numFailed = numTotal = 0;
		}
		
		protected function runAsync(workFun:Function, onComplete:Function):void {
			var timer:Timer = new Timer(20);
			timer.addEventListener(TimerEvent.TIMER, createOnTimer(workFun, onComplete));
			timer.start();
		}
		
		protected function createOnTimer(workFun:Function, onComplete:Function):Function {
			var startTime:int = getTimer();
			var onTimer:Function = function (event:TimerEvent):void {
				var iterStartTime:int = getTimer();
				var isDone:Boolean = !workFun(iterStartTime - startTime);
				var subIterCount:int = 1;
				while (!isDone) {
					var currentTime:int = getTimer();
					if (currentTime - iterStartTime >= ITER_TIME)
						break;
					isDone = !workFun(currentTime - startTime);
					++subIterCount;
				}
				if (isDone) {
					var timer:Timer = event.target as Timer;
					timer.removeEventListener(TimerEvent.TIMER, onTimer);
					timer.stop();
					onComplete(getTimer() - startTime);
				}
				if (subIterCount >= 5)
					trace(subIterCount, 'sub-iterations per iteration.');
			}
			return onTimer;
		}
		
		private static const ITER_TIME:int = 100;
		
		protected var numFailed:Number = 0;
		protected var numTotal:Number = 0;
		protected var logAssertOk:Boolean = false;
		protected var logAssertFail:Boolean = true;
		
		protected var logText:TextField;
		protected var statusText:TextField;
	}
}
