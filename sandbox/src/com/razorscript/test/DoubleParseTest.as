package com.razorscript.test {
	import com.razorscript.math.Double;
	import com.razorscript.math.MathExt;
	import com.razorscript.utils.NumUtil;
	import com.razorscript.utils.StrUtil;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	/**
	 * ...
	 * @author Gene Pavlovsky
	 */
	public class DoubleParseTest extends TestBase {
		
		public function DoubleParseTest(logText:TextField, statusText:TextField) {
			super(logText, statusText);
			logText.stage.addEventListener(MouseEvent.CLICK, onClick);
		}
		
		private function onClick(e:MouseEvent):void {
			if (e.ctrlKey)
				abortFlag = true;
		}
		
		public function run():void {
			testProblemNumbers();
			//testAllNumbers();
			testSubnormalNumbers();
		}
		
		private function testAllNumbers():void {
			resetAssertCounters();
			maxError = 0;
			//logAssertFail = false;
			dbl = new Double(0);
			nextFun = testAllNext;
			runAsync(testRange, onTestSubnormalDone);
		}
		
		private function testAllNext():Boolean {
			dbl.exponent = Math.random() * 2048 - 1023;
			dbl.fractionHigh = Math.random() * MathExt.POW2_32;
			dbl.fractionLow = Math.random() * MathExt.POW2_32;
			return true;
		}
		
		private function testSubnormalNumbers():void {
			resetAssertCounters();
			maxError = 0;
			logAssertFail = true;
			// This is the smallest number at which parseFloat() starts being inaccurate. What's so special about this number?
			dbl = new Double(3.12500000000122191154e-309);
			nextFun = testSubnormalNext;
			runAsync(testRange, onTestSubnormalDone);
		}
		
		private function testSubnormalNext():Boolean {
			//dbl.number = Math.random() * Math.random() * MathExt.DOUBLE_MIN_NORMAL;
			dbl.number = Math.random() * MathExt.DOUBLE_MIN_NORMAL;
			return true;
		}
		
		private function onTestSubnormalDone(elapsed:int):void {
			appendLog(StrUtil.format('Finished in {:.1f} s.', (elapsed / 1000)));
		}
		
		private function testRange(elapsed:int):Boolean {
			if (abortFlag) {
				appendLog('Aborted by user.');
				return false;
			}
			testRangePrintStatus(elapsed);
			for (var i:int = 0; i < NUM_PER_ITER; ++i) {
				var str:String = dbl.toExponential(TOEXP_DIGITS);
				//var str:String = dbl.toFixed(TOFIX_DIGITS);
				//if (!assertEquals(str, NumUtil.toExponential(parseFloat(str), TOEXP_DIGITS))) {
				//if (!assertEquals(str, NumUtil.toExponential(NumUtil.parseNumber(str), TOEXP_DIGITS))) {
				//if (!assertEquals(str, NumUtil.toFixed(parseFloat(str), TOFIX_DIGITS))) {
				//if (!assertEquals(str, NumUtil.toFixed(NumUtil.parseNumber(str), TOFIX_DIGITS))) {
				if (!assertEquals(dbl.number.toExponential(20), NumUtil.parseNumber(str).toExponential(20))) {
					var error:Number = Math.abs(NumUtil.distance(NumUtil.parseNumber(str), parseFloat(str)));
					if (error > maxError) {
						maxError = error;
						trace(dbl.bitString, dbl.number, error);
					}
				}
				if (!nextFun()) {
					testRangePrintStatus(elapsed);
					return false;
				}
			}
			return true;
		}
		
		private function testRangePrintStatus(elapsed:int):void {
			status = StrUtil.format('Elapsed: {:6.1f} s. Number: {:23.16e} {:#23a}. Max error: {:.0f} ULPs. {}', (elapsed / 1000), dbl.number, dbl.number, maxError, formatAssertStatus());
		}
		
		private static const NUM_PER_ITER:int = 50;
		
		private var dbl:Double;
		private var nextFun:Function;
		private var abortFlag:Boolean;
		private var maxError:Number;
		
		private function testProblemNumbers():void {
			var numbers:Array = [
				['0', 0],
				['0.0', 0],
				['0.', 0],
				['.0', 0],
				['.', NaN],
				['0e500', 0],
				['0.0e500', 0],
				['0.e500', 0],
				['.0e500', 0],
				['.e500', NaN],
				['e', NaN],
				['', NaN],
				['NaN', NaN],
				['inf', Infinity],
				['-inf', -Infinity],
				['+Infinity', Infinity],
				['Infinity', Infinity],
				['-Infinity', -Infinity],
				['e-1', NaN],
				['e0', NaN],
				['e1', NaN],
				['6.9294956446009195e15', 6929495644600920],
				['3.7455744005952583e15', 3745574400595258.5],
				['2.2250738585072012e-308', 2.2250738585072014e-308],
				['2.2250738585072011e-308', 2.2250738585072009e-308]];
			
			var numNum:int = numbers.length;
			
			resetAssertCounters();
			appendLog('Using parseNumber().');
			for (var i:int = 0; i < numNum; ++i)
				testParseNumber.apply(this, numbers[i]);
			appendLog(formatAssertStatus(), '\n');
			
			resetAssertCounters();
			appendLog('Using parseFloat().');
			for (i = 0; i < numNum; ++i)
				testParseFloat.apply(this, numbers[i]);
			appendLog(formatAssertStatus(), '\n');
		}
		
		private function testParseNumber(str:String, expected:Number):void {
			assertEquals(NumUtil.toExponential(expected, TOEXP_DIGITS), NumUtil.toExponential(NumUtil.parseNumber(str), TOEXP_DIGITS));
		}
		
		private function testParseFloat(str:String, expected:Number):void {
			assertEquals(NumUtil.toExponential(expected, TOEXP_DIGITS), NumUtil.toExponential(parseFloat(str), TOEXP_DIGITS));
		}
		
		private static const TOEXP_DIGITS:int = 16;
		private static const TOFIX_DIGITS:int = 350;
	}
}
