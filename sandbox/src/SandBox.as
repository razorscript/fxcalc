package {
	import avmplus.getQualifiedClassName;
	import com.razorscript.razor_internal;
	import com.razorscript.app.Version;
	import com.razorscript.bench.BenchUtil;
	import com.razorscript.bench.Benchmark;
	import com.razorscript.bench.BenchmarkCase;
	import com.razorscript.bench.formatters.BenchmarkFormatOptions;
	import com.razorscript.bench.formatters.BenchmarkTableFormatter;
	import com.razorscript.bench.runners.BenchmarkRunner;
	import com.razorscript.bench.runners.supportClasses.BenchmarkRunnerAsyncMode;
	import com.razorscript.bench.ui.BenchmarkConsole;
	import com.razorscript.debug.Debug;
	import com.razorscript.debug.LogLevel;
	import com.razorscript.debug.Logger;
	import com.razorscript.debug.getStackTrace;
	import com.razorscript.ds.ArrayIterator;
	import com.razorscript.ds.BitVector;
	import com.razorscript.ds.BitVectorIterator;
	import com.razorscript.ds.Deque;
	import com.razorscript.ds.IIterator;
	import com.razorscript.ds.ObjectIterator;
	import com.razorscript.ds.Queue;
	import com.razorscript.ds.Stack;
	import com.razorscript.ds.VectorIterator;
	import com.razorscript.fxcalc.data.SettingsModel;
	import com.razorscript.math.AngleUnit;
	import com.razorscript.math.Double;
	import com.razorscript.math.FPMode;
	import com.razorscript.math.MathContext;
	import com.razorscript.math.MathExt;
	import com.razorscript.math.NumberFormatter;
	import com.razorscript.math.RoundingMode;
	import com.razorscript.math.errors.ArithmeticError;
	import com.razorscript.math.expression.MathExpression;
	import com.razorscript.math.expression.MathFun;
	import com.razorscript.math.expression.MathOperator;
	import com.razorscript.math.expression.MathParser;
	import com.razorscript.math.expression.converters.MathExpressionConvUtil;
	import com.razorscript.math.expression.converters.MathTokenConverter;
	import com.razorscript.math.expression.converters.StringConverter;
	import com.razorscript.math.expression.converters.StringConverterLexer;
	import com.razorscript.math.expression.lexers.LexerBase;
	import com.razorscript.math.expression.lexers.MathTokenArrayMathTokenProvider;
	import com.razorscript.math.expression.lexers.MathTokenLexer;
	import com.razorscript.math.expression.lexers.MathTokenVectorMathTokenProvider;
	import com.razorscript.math.expression.lexers.StringLexer;
	import com.razorscript.math.expression.tokens.IMathTokenFactory;
	import com.razorscript.math.expression.tokens.IdentifierToken;
	import com.razorscript.math.expression.tokens.MathToken;
	import com.razorscript.math.expression.tokens.MathTokenFactory;
	import com.razorscript.math.expression.tokens.OperatorToken;
	import com.razorscript.math.expression.tokens.ValueToken;
	import com.razorscript.math.functions.MathLib;
	import com.razorscript.math.functions.mathLib.ArithmeticMath;
	import com.razorscript.math.functions.polymorphism.PolyFun1;
	import com.razorscript.math.functions.polymorphism.PolyFun2;
	import com.razorscript.math.functions.polymorphism.TypeConverter;
	import com.razorscript.math.objects.ComplexNumber;
	import com.razorscript.math.objects.Fraction;
	import com.razorscript.math.objects.conversions.TypeConversionFun;
	import com.razorscript.math.supportClasses.SimpleBigDecimal;
	import com.razorscript.math.supportClasses.SimpleBigInt;
	import com.razorscript.sandbox.GetSetTestData;
	import com.razorscript.utils.BinUtil;
	import com.razorscript.utils.ClassUtil;
	import com.razorscript.utils.Lambda;
	import com.razorscript.utils.NumUtil;
	import com.razorscript.utils.ObjUtil;
	import com.razorscript.utils.StrUtil;
	import com.razorscript.utils.TimerUtil;
	import com.razorscript.utils.enum.IEnum;
	import com.razorscript.utils.objectParser.conversions.ObjectParserConversionFun;
	import dto.TestDTO;
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.net.SharedObject;
	import flash.net.SharedObject;
	import flash.system.ApplicationDomain;
	import flash.utils.ByteArray;
	import flash.utils.Dictionary;
	import flash.utils.Timer;
	import flash.utils.getTimer;
	
	use namespace razor_internal;
	
	/**
	 * Sandbox for testing various things.
	 *
	 * @author Gene Pavlovsky
	 */
	public class SandBox extends BenchmarkConsole {
		public function SandBox() {
			super();
		}
		
		override protected function initGlobals():void {
			Logger.severityLevels = LogLevel.all();
			Logger.logFun = Logger.traceLogger;
			Logger.traceFun = null; // Use StrUtil.trace().
			StrUtil.trace = trace;
			//Debug.instance.debugPooling = true;
		}
		
		override protected function setupBenchRunner():void {
			super.setupBenchRunner();
			
			benchRunner.asyncMode = BenchmarkRunner.RUN_AUTO;
			benchRunner.gcMode = BenchmarkRunner.GC_IF_IMMINENT;
			benchRunner.itemDuration = 1000;
			benchRunner.asyncTimeSlice = 250;
			//benchRunner.reportTimeSlices = true;
		}
		
		override protected function run():void {
			try {
				testMisc();
				//testJSON();
				//testHashSpeed();
				//testGetSetSpeed();
				//testPolymorphism();
				//testBenchmarkRunner();
				//testFunUtil();
				//testArrayLengthPerformance();
				//testFormatter();
				//testSimpleBigInt();
				//testNumUtil();
				//testObjUtil();
				//testBitVector();
				//testObjectIterator();
				//testArrayIterator();
				//testVectorIterator();
				//testDeque();
				//testTime();
				//testDouble();
				//testStrUtil();
				//testMathParser();
				//testMathConverter();
				//testMathExt();
				//testMathFun();
				//testNumberFormatter();
				//new DoubleParseTest(logText, statusText).run();
			}
			catch (err:Error) {
				trace('An exception has occurred:\n  ' + err.getStackTrace());
			}
		}
		
		protected function testMisc():void {
		}
		
		protected function testVersion():void {
			var v1:Version = new Version();
			var v2:Version = new Version('6.2.4');
			var v3:Version = new Version(5003001);
			trace([v1, v2, v3].map(Lambda.slice3(versionToString)).join('\n'));
		}
		
		protected function versionToString(ver:Version):String {
			return '[Version versionNumber="' + ver.versionNumber + '" versionCode=' + ver.versionCode + ']';
		}
		
		[Embed(source="/../../assets/settings.default.json", mimeType="application/octet-stream")]
		protected static const DEFAULT_SETTINGS_JSON:Class;
		
		protected static const ENUM_CLASSES:Vector.<Class> = new <Class>[AngleUnit, FPMode, RoundingMode];
		
		protected function testJSON():void {
			ObjectParserConversionFun.extend(ObjUtil.objectParser, ENUM_CLASSES);
			var source:Object = JSON.parse(new DEFAULT_SETTINGS_JSON());
			var target:SettingsModel = new SettingsModel();
			trace(JSON.stringify(target, null, 2));
			trace('');
			benchRunner.run(BenchUtil.funC(1, 'parseObject', target.parseObject, source));
			benchRunner.run(BenchUtil.funD(100, 'parseObject', target.parseObject, source));
			trace('');
			//target.parseObject(JSON.parse(new BASE_SETTINGS_JSON()));
			trace(JSON.stringify(target, null, 2));
			
			/*var sourceDTO:TestDTO = new TestDTO();
			sourceDTO.bool = false;
			sourceDTO.arr = ['a', 'b', 'c'];
			sourceDTO.vec = new <Number>[1, 2, 3];
			sourceDTO.date = new Date();
			sourceDTO.num = Math.PI;
			sourceDTO.str = 'test-dto-1';
			var innerDTO:TestDTO = new TestDTO();
			innerDTO.date = new Date(2000, 1, 1, 0, 0, 0, 0);
			innerDTO.num = Math.E;
			innerDTO.str = 'test-dto-1-inner';
			sourceDTO.obj = innerDTO;
			var jsonStr:String = JSON.stringify(sourceDTO, null, 2);
			trace('source=' + StrUtil.valueToString(sourceDTO));
			trace('json=' + jsonStr);
			var jsonObj:Object = JSON.parse(jsonStr);
			trace('obj=' + StrUtil.objectToString(jsonObj));
			var parsedDTO:TestDTO = new TestDTO();
			parseJSONTo(jsonObj, parsedDTO);
			trace('parsed=' + StrUtil.valueToString(parsedDTO));*/
		}
		
		protected function testHashSpeed():void {
			var bench:BenchmarkCase = new BenchmarkCase();
			
			const values:Array = [MathExpressionConvUtil, MathTokenConverter, StringConverter, StringConverterLexer, LexerBase, MathTokenArrayMathTokenProvider, MathTokenLexer, MathTokenVectorMathTokenProvider, StringLexer, IMathTokenFactory, IdentifierToken, MathToken, MathTokenFactory, OperatorToken, ValueToken, MathLib, ArithmeticMath, PolyFun1, PolyFun2, TypeConverter, ComplexNumber, Fraction, TypeConversionFun, SimpleBigDecimal];
			for each (var classRef:Class in values) {
				var stringKey:String = getQualifiedClassName(classRef);
				objectHash[stringKey] = classRef;
				dictStringKeys[stringKey] = classRef;
				dictObjectKeys[classRef] = classRef;
			}
			
			var lookupObject:Class = PolyFun1;
			var lookupString:String = getQualifiedClassName(lookupObject);
			
			bench.addFun('object', testHashSpeed_object, lookupString);
			bench.addFun('dict-string-keys', testHashSpeed_dictStringKeys, lookupString);
			bench.addFun('dict-object-keys', testHashSpeed_dictObjectKeys, lookupObject);
			
			benchRunner.run(bench);
		}
		
		protected var objectHash:Object = {};
		protected var dictStringKeys:Dictionary = new Dictionary();
		protected var dictObjectKeys:Dictionary = new Dictionary();
		
		protected static const TEST_HASH_SPEED_ITER:int = 1000;
		
		protected function testHashSpeed_object(key:String):void {
			for (var i:int = 0; i < TEST_HASH_SPEED_ITER; ++i)
				objectHash[key];
		}
		
		protected function testHashSpeed_dictStringKeys(key:String):void {
			for (var i:int = 0; i < TEST_HASH_SPEED_ITER; ++i)
				dictStringKeys[key];
		}
		
		protected function testHashSpeed_dictObjectKeys(key:Class):void {
			for (var i:int = 0; i < TEST_HASH_SPEED_ITER; ++i)
				dictObjectKeys[key];
		}
		
		protected function testGetSetSpeed():void {
			var data:GetSetTestData = new GetSetTestData(69);
			var bench:BenchmarkCase = new BenchmarkCase();
			
			bench.addFun('direct get', testGetSetSpeed_directGet, data);
			bench.addFun('direct set', testGetSetSpeed_directSet, data, 125);
			bench.addFunC(1, '# reset', testGetSetSpeed_directSet, data, 69);
			bench.addFun('getter fun', testGetSetSpeed_propertyGet, data);
			bench.addFun('setter fun', testGetSetSpeed_propertySet, data, 125);
			
			benchRunner.run(bench);
		}
		
		protected function testGetSetSpeed_directGet(data:GetSetTestData):Number {
			for (var i:int = 0; i < 1000; ++i)
				data._num;
			return data._num;
		}
		
		protected function testGetSetSpeed_directSet(data:GetSetTestData, value:Number):Number {
			for (var i:int = 0; i < 1000; ++i)
				data._num = value;
			return data._num;
		}
		
		protected function testGetSetSpeed_propertyGet(data:GetSetTestData):Number {
			for (var i:int = 0; i < 1000; ++i)
				data.num;
			return data.num;
		}
		
		protected function testGetSetSpeed_propertySet(data:GetSetTestData, value:Number):Number {
			for (var i:int = 0; i < 1000; ++i)
				data.num = value;
			return data.num;
		}
		
		protected function testPolymorphism():void {
			var nClass:Class = Number;
			var cClass:Class = ComplexNumber;
			var n:Number = 357, n2:Number = 113;
			var c:ComplexNumber = new ComplexNumber(25, -69), c2:ComplexNumber = new ComplexNumber(35, 19);
			var f:Fraction = new Fraction(5, 4), f2:Fraction = new Fraction(1, 16);
			var bench:BenchmarkCase = new BenchmarkCase();
			
			var typeConverter:TypeConverter = TypeConversionFun.extend(new TypeConverter());
			//trace(typeConverter.convert(n, ComplexNumber));
			//trace(typeConverter.convert(f, ComplexNumber));
			//trace(typeConverter.convert(new ComplexNumber(3.14), Fraction));
			//typeConverter.addConversion(Number, ComplexNumber, testCN);
			//trace(typeConverter.convert(n, ComplexNumber));
			//typeConverter.removeConversion(Number, ComplexNumber);
			//trace(typeConverter.convert(n, ComplexNumber));
			
			var negate:PolyFun1 = new PolyFun1(typeConverter);
			negate.addSignature(Number, negateReal);
			//negate.addSignature(Fraction, negateFraction);
			negate.addSignature(ComplexNumber, negateComplex);
			bench.addFun('negateReal(' + n + ')', negateReal, n);
			bench.addFun('negateComplex(' + c + ')', negateComplex, c);
			bench.addFun('negate(' + n + ')', negate.call, n);
			bench.addFun('negate(' + c + ')', negate.call, c);
			bench.addFun('negate(' + f + ')', negate.call, f);
			bench.addFun('negate(' + n + ')', negate.call, n);
			bench.addFun('negate(' + c + ')', negate.call, c);
			bench.addFun('negate(' + f + ')', negate.call, f);
			
			var add:PolyFun2 = new PolyFun2(typeConverter);
			add.addSignature(Number, Number, addReal);
			add.addSignature(Fraction, Fraction, addFraction);
			add.addSignature(ComplexNumber, ComplexNumber, addComplex);
			bench.addFun('addReal(' + n + ', ' + n2 + ')', addReal, n, n2);
			bench.addFun('addComplex(' + c + ', ' + c2 + ')', addComplex, c, c2);
			bench.addFun('add(' + n + ', ' + n2 + ')', add.call, n, n2);
			bench.addFun('add(' + c + ', ' + c2 + ')', add.call, c, c2);
			bench.addFun('add(' + f + ', ' + f2 + ')', add.call, f, f2);
			bench.addFun('add(' + n + ', ' + c + ')', add.call, n, c);
			bench.addFun('add(' + c + ', ' + n + ')', add.call, c, n);
			bench.addFun('add(' + n + ', ' + f + ')', add.call, n, f);
			bench.addFun('add(' + f + ', ' + n + ')', add.call, f, n);
			bench.addFun('add(' + f + ', ' + c + ')', add.call, f, c);
			bench.addFun('add(' + c + ', ' + f + ')', add.call, c, f);
			
			benchRunner.run(bench);
		}
		
		protected function testCN(n:Number):ComplexNumber {
			return new ComplexNumber(69, 69);
		}
		
		private function negateReal(x:Number):Number {
			return -x;
		}
		
		private function negateFraction(x:Fraction):Fraction {
			return new Fraction(-x.n, x.d);
		}
		
		private function negateComplex(x:ComplexNumber):ComplexNumber {
			return new ComplexNumber(-x.re, -x.im);
		}
		
		private function addReal(x:Number, y:Number):Number {
			return x + y;
		}
		
		private static const primes:Vector.<int> = new <int>[2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43];
		
		private function addFraction(x:Fraction, y:Fraction):Fraction {
			var n:Number = x.n * y.d + y.n * x.d;
			var d:Number = x.d * y.d;
			for (var i:int = primes.length - 1; i >= 0; --i) {
				var prime:int = primes[i];
				while (MathExt.isInteger(n / prime) && MathExt.isInteger(d / prime)) {
					n /= prime;
					d /= prime;
				}
			}
			return new Fraction(n, d);
		}
		
		private function addComplex(x:ComplexNumber, y:ComplexNumber):ComplexNumber {
			return new ComplexNumber(x.re + y.re, x.im + y.im);
		}
		
		protected function testBenchmarkRunner():void {
			Debug.instance.debugPooling = false;
			Logger.severityLevels.splice(Logger.severityLevels.indexOf(LogLevel.WARN), 1);
			benchRunner.multiItemFormatOptions = new BenchmarkTableFormatter().formatOptions;
			benchRunner.itemDuration = 2100;
			benchRunner.asyncTimeSlice = 500;
			
			var bench:Benchmark = new Benchmark('TestBench');
			
			var bcD:BenchmarkCase = new BenchmarkCase('TestDivide');
			bcD.addFun('getTimer', getTimer);
			bcD.addFun('div', divide, Math.PI, Math.E);
			//*
			bench.add(bcD);
			/*/
			benchRunner.run(bcD);
			//*/
			
			var bcL:BenchmarkCase = new BenchmarkCase('TestLoopN');
			//for (var i:int = 1e4; i <= 1e5; i *= 10)
				//for (var j:int = 1; j <= 8; j <<= 1)
					//bcL.addFunC(100, 'loopN(' + j + '*' + i + ')', loopN, j * i);
			//bcL.addFunC(100000, 'loop10', loopN, 10);
			//bcL.addFunC(100000, 'loop20', loopN, 20);
			//bcL.addFunC(100000, 'loop50', loopN, 50);
			//bcL.addFunC(100000, 'loop100', loopN, 100);
			//bcL.addFunC(100000, 'loop200', loopN, 200);
			//bcL.addFunC(100, 'loop10', loopN, 10);
			//bcL.addFunC(100, 'loop20', loopN, 20);
			//bcL.addFunC(100, 'loop50', loopN, 50);
			//bcL.addFunC(100, 'loop100', loopN, 100);
			//bcL.addFunC(100, 'loop200', loopN, 200);
			//bcL.addFunC(100, 'loop400', loopN, 400);
			//bcL.addFunC(100, 'loop800', loopN, 800);
			//bcL.addFunC(100, 'loop1600', loopN, 1600);
			//bcL.addFunC(100, 'loop3200', loopN, 3200);
			//bcL.addFunC(100, 'loop6400', loopN, 6400);
			//bcL.addFunC(100, 'loop12800', loopN, 12800);
			//bcL.addFunC(100, 'loop25600', loopN, 25600);
			//bcL.addFunC(200, 'loop25600-2', loopN, 25600);
			//bcL.addFunC(400, 'loop25600-4', loopN, 25600);
			//bcL.addFunC(800, 'loop25600-8', loopN, 25600);
			//bcL.addFunC(1600, 'loop25600-16', loopN, 25600);
			//bcL.addFunC(100, 'loop1024000', loopN, 1024000);
			//bcL.addFunC(100, 'loop2048000', loopN, 2048000);
			//*
			bench.add(bcL);
			/*/
			benchRunner.run(bcL);
			//*/
			
			var bcP:BenchmarkCase = new BenchmarkCase('TestPower');
			//bcP.addFun('power1', power, 1);
			//bcP.addFun('power2', power, 2);
			//bcP.addFun('power3', power, 3);
			//bcP.addFun('power4', power, 4);
			//bcP.addFun('power5', power, 5);
			//bcP.addFun('power5.3', power, 5, 3);
			//bcP.addFun('power5.5', power, 5, 5);
			//bcP.addFun('power5.8', power, 5, 8);
			//bcP.addFun('power6.1', power, 6);
			//bcP.addFun('power6.11', power, 6, 1.1);
			//bcP.addFun('power6.12', power, 6, 1.2);
			//bcP.addFun('power6.13', power, 6, 1.3);
			//bcP.addFun('power6.14', power, 6, 1.4);
			//bcP.addFun('power6.15', power, 6, 1.5);
			//bcP.addFun('power6.2', power, 6, 2);
			//bcP.addFun('power6.3', power, 6, 3);
			//bcP.addFun('power6.4', power, 6, 4);
			//bcP.addFun('power6.5', power, 6, 5);
			//bcP.addFun('power7', power, 7, 1);
			//bcP.addFun('power7.5', power, 7, 5);
			//*
			bench.add(bcP);
			/*/
			benchRunner.run(bcP);
			//*/
			
			benchRunner.run(bench);
		}
		
		protected function divide(x:Number, y:Number):Number {
			return x / y;
		}
		
		protected function loopN(n:uint):String {
			var n2:Number = 0;
			for (var i:int = n; i >= 0; --i)
				n2 += i;
			return n + ': ' + n2;
		}
		
		protected function power(x:uint, y:Number = 1):Number {
			var count:int = y * Math.pow(10, x);
			for (var i:int = 0; i < count; ++i) {
				var num:Number = Math.pow(10, x);
			}
			return count;
		}
		
		protected function testFunUtil():void {
			var reciprocal:Function = Lambda.prepend1(divide, 1);
			trace(reciprocal(10)); // 0.1
			var halve:Function = Lambda.append1(divide, 2);
			trace(halve(10)); // 5
			var f1:Function = Lambda.compose(Math.cos, Math.sin);
			var f2:Function = Lambda.compose(Math.sqrt, Math.tan, Math.cos, Math.sin);
			var num:Number = 0.31337;
			trace(f1(num), Math.cos(Math.sin(num)));
			trace(f2(num), Math.sqrt(Math.tan(Math.cos(Math.sin(num)))));
		}
		
		protected function testArrayLengthPerformance():void {
			//var dq:Deque = new Deque();
			//for (var i:int = 0; i < 150; ++i)
				//dq.addLast(i);
			//_startTime = getTimer();
			//var k:Number = 0;
			//for (i = 0; i < 100000; ++i) {
				//k += dq.length2;
			//}
			//trace(getTimer() - _startTime, k);
			//return;
			
			//var arr:Array = [];
			//for (var i:int = 0; i < 150; ++i)
				//arr[i] = i;
			//_startTime = getTimer();
			//var k:Number = 0;
			//for (i = 0; i < 100000; ++i) {
				//k += arr.length;
			//}
			//trace(getTimer() - _startTime, k);
		}
		
		protected function testFormatter():void {
			//StrUtil.formatter.exponentWidth = 3;
			//var obj:Object = {src:'source', dst:'destination'};
			//var arr:Array = [10, 20, 30, obj];
			//var varFormat:String = '{0:{1.fill}{1.align}16} {1!J}\n';
			//StrUtil.printf('obj=%j!s arr=%j!s', obj, arr);
			//StrUtil.tformat(varFormat, 'left', {fill:'^', align:'<'});
			//StrUtil.tformat(varFormat, 'center', {fill:'_', align:'^'});
			//StrUtil.tformat(varFormat, 'right', {fill:'-', align:'>'});
			//StrUtil.tformat('#2={0[2]} #1={0[1]} #0={0[0]} src={0[3].src} dst={0[3].dst}.', arr);
			//StrUtil.tformat('tformat: From {0.src} to {0.dst}.', obj);
			//StrUtil.printf('printf: From %0.src$s to %0.dst$s.', obj);
			//StrUtil.tformat('From {0} to {1}.', 'source', 'destination');
			//StrUtil.tformat('From {} to {}.  {{ {{{{{}}}}} }} {} }}{{', 'source', 'destination', 't1', 't2', 't3', 't4');
			//StrUtil.printf('From %s to %s. %%s % s %%%%s %s', 'source', 'destination', 't1', 't2', 't3', 't4');
			//StrUtil.printf('From %s to %s in %.^20d days.', 'source', 'destination', int.MAX_VALUE);
			//StrUtil.printf('%-#013,.5g', MathExt.DOUBLE_EPSILON);
			//StrUtil.tformat('{:0e}', 1235912350);
			//StrUtil.tformat('{:0e}', 1235);
			//StrUtil.tformat('{:0e}', 1.235);
			//StrUtil.tformat('{:0E}', -NaN);
			//StrUtil.tformat('{:0f}', 1235912350);
			//StrUtil.tformat('{:0f}', 1235);
			//StrUtil.tformat('{:0f}', 1.235);
			//StrUtil.tformat('{:0f}', 194);
			//StrUtil.tformat('{:0F}', 194);
			//StrUtil.tformat('{:0F}', -Infinity);
			//StrUtil.tformat('{:0g}', 1235912350);
			//StrUtil.tformat('{:0g}', 1235);
			//StrUtil.tformat('{:0g}', 1.235);
			//StrUtil.tformat('{:0g}', 194);
			//StrUtil.tformat('{:0G}', 194);
			//StrUtil.tformat('{:0G}', -Infinity);
			//StrUtil.tformat('{:.2p}', 0.1375);
			//StrUtil.tformat('{:.g}', 1.5);
			//StrUtil.tformat('{:.g}', 1.4);
			//StrUtil.tformat('{:018_.4f}', -50302327.5019);
			//StrUtil.tformat('{:018*.4f}', 50302327.5019);
			//StrUtil.tformat('{:,g}', -50302327.5019);
			//StrUtil.tformat('{:_e}', -50302327.5019);
			//StrUtil.tformat('{:047,b}', 0xffffffff);
			//StrUtil.tformat('{:020,x}', 0xfefdfcfb);
			//StrUtil.tformat('{:021,X}', 0xdeadbeef);
			//StrUtil.tformat('{:g}', 503.00000019), StrUtil.format('{:#g}', 503.00000019);
			//StrUtil.tformat('{:e}', 503.00000019), StrUtil.format('{:#e}', 503.00000019);
			//StrUtil.tformat('{:f}', 503.00000019), StrUtil.format('{:#f}', 503.00000019);
			//StrUtil.tformat('{:p}', 503.00000019), StrUtil.format('{:#p}', 503.00000019);
			//trace('');
			//StrUtil.tformat('{:.g}', 1), StrUtil.format('{:#.g}', 1);
			//StrUtil.tformat('{:.e}', 1), StrUtil.format('{:#.e}', 1);
			//StrUtil.tformat('{:.f}', 1), StrUtil.format('{:#.f}', 1);
			//StrUtil.tformat('{:.p}', 1), StrUtil.format('{:#.p}', 1);
			//trace('');
			//StrUtil.tformat('{}', uint.MAX_VALUE);
			//StrUtil.tformat('{:.6}', 1.00001), StrUtil.format('{:#.6}', 1.00001);
			//StrUtil.tformat('{:.5}', 1.00001), StrUtil.format('{:#.5}', 1.00001);
			//StrUtil.tformat('{:.1}', 1), StrUtil.format('{:.2}', 1);
			//StrUtil.tformat('{:010,.4g}', 12.45678);
			//StrUtil.tformat('{:010,.4g}', 123.45678);
			//StrUtil.tformat('{:010,.4g}', 1234.45678);
			//StrUtil.tformat('{:010,.4g}', 12345.45678);
			//StrUtil.tformat('{:010,.4g}', 123456.45678);
			//StrUtil.tformat('{:010,.4g}', 1234567.45678);
			//StrUtil.tformat('{:015,.4g}', 1234567.45678);
			//StrUtil.tformat('{:016,.4g}', 1234567.45678);
			//StrUtil.tformat('{:017,.4g}', 1234567.45678);
			//StrUtil.tformat('{:018,.4g}', 1234567.45678);
			//StrUtil.tformat('{:019,.4g}', 1234567.45678);
			//var str:String = StrUtil.format('{!j}', {a:[1, 2, 3], b:"test"});
			//StrUtil.tformat('{0!eu}\n{1!du}\n{0!ec}\n{2!dc}', str, encodeURI(str), encodeURIComponent(str));
			//StrUtil.tformat('{0!eu}\n{0!ec}', 'https://docs.python.org/3/library/string.html#format-string-syntax?user=index aaa');
			//StrUtil.tformat('{0!u}\n{0!l}', 'mixedCaseString SomethingOrWhatNot');
			//StrUtil.tformat('{:25,a}', 1.6125);
			//StrUtil.tformat('{:025*.6g}', 1234567.45678);
			//StrUtil.tformat('{:025*.6g}', 1237.45678);
			//StrUtil.tformat('{:025*.6e}', 1237.45678);
			//StrUtil.formatter.exponentWidth = 2;
			//StrUtil.tformat('{:.6g}', 1234567.45678);
			//StrUtil.formatter.exponentWidth = 3;
			//StrUtil.tformat('{:.6g}', 1234567.45678);
			//StrUtil.tformat('{:.4g}', 12.45678);
			//StrUtil.tformat('{:024_.4f}', 12.45678);
			//StrUtil.tformat('{:015,u}', 0xffffffff);
			//StrUtil.tformat('{:047,b}', 0xffffffff);
			//StrUtil.tformat('{:025,o}', 0xfefdfcfb);
			//StrUtil.tformat('{:019,x}', 0xfefdfcfb);
			//StrUtil.tformat('{:020,X}', 0xdeadbeef);
			//StrUtil.tformat('{:015,d}', int.MIN_VALUE);
			//StrUtil.tformat('{:016,d}', int.MIN_VALUE);
			//StrUtil.tformat('{:017,d}', int.MIN_VALUE);
			//StrUtil.tformat('{:018*i}', int.MIN_VALUE / 2);
			//StrUtil.tformat('{: =20_.6f}', -1238.3479);
			//StrUtil.tformat('{}', -1238.3479);
			//StrUtil.tformat('{}', 0.1);
			//StrUtil.tformat('{}', 0.101);
			//StrUtil.tformat('{}', 0.00012345);
			//StrUtil.tformat('{}', 0.000012345);
			//StrUtil.tformat('{}', 0.0000012345);
			//StrUtil.tformat('{}', 0.00000012345);
			//StrUtil.tformat('{0} {0:.20}', 0.000000012345);
			//StrUtil.tformat('{0} {0:.20}', NumUtil.prior(0.000000012345));
			//StrUtil.tformat('{0} {0:.20}', NumUtil.next(0.000000012345));
			//StrUtil.tformat('{: =15,.2f}', -12345678.357);
			//StrUtil.tformat('aaa {} bbb {} ccc', '___');
			//StrUtil.tformat('{1}', 'aaa');
			//StrUtil.tformat('{0.x.name}', {x: {name: 'WORKS'}});
			//StrUtil.tformat('{0:{1}s} {2:{3}s}', 'aaa', 6, 'bbb');
		}
		
		protected function assertEqualsUInt(a:uint, b:uint, base:int = 16):void {
			trace(((a == b) ? 'PASS' : 'FAIL') + ':', a.toString(base), '==', b.toString(base));
		}
		
		protected function assertEqualsString(a:String, b:String):void {
			trace(((a == b) ? 'PASS' : 'FAIL') + ':', a, '==', b);
		}
		
		protected function testSimpleBigInt():void {
			var bigInt:SimpleBigInt = new SimpleBigInt(4);
			bigInt.insertHighBits(0x80000000, 32);
			trace(bigInt);
			assertEqualsUInt(bigInt.mul16(10), 5);
			bigInt.insertHighBits(0x40000000, 31);
			trace(bigInt);
			assertEqualsUInt(bigInt.mul16(10), 5);
			bigInt.insertHighBits(0x12345678, 40);
			trace(bigInt);
			assertEqualsUInt(bigInt.getWordAt(0), 0x00123456);
			assertEqualsUInt(bigInt.getWordAt(1), 0x78000000);
			assertEqualsUInt(bigInt.getWordAt(2), 0);
			assertEqualsUInt(bigInt.getWordAt(3), 0);
			assertEqualsUInt(bigInt.mul16(16), 0);
			assertEqualsUInt(bigInt.getWordAt(0), 0x01234567);
			assertEqualsUInt(bigInt.getWordAt(1), 0x80000000);
		}
		
		protected function testNumUtilNumber(num:Number, expect:String = ''):void {
			/*const fractionDigits:int = 15;
			trace(num.toExponential(fractionDigits), num.toString());
			var dblStr:String = NumUtil.toExponential(num, fractionDigits);
			if (expect)
				assertEqualsString(dblStr, expect);
			else
				trace(dblStr);
			trace('');*/
			/*const fractionDigits:int = 10;
			var str1:String = NumUtil.toFixed(num, fractionDigits);
			var str2:String = num.toFixed(fractionDigits);
			trace(str1, str2, num.toString(), (str1 == str2) ? '' : 'FAIL');*/
			//trace('');
			/*const fractionDigits:int = 5;
			var str1:String = NumUtil.toExponential(num, fractionDigits);
			var str2:String = num.toExponential(fractionDigits);
			trace(str1, str2, num.toString(), (str1 == str2) ? '' : 'FAIL');*/
			//trace('');
			/*const precision:int = 2;
			var str1:String = NumUtil.toGeneral(num, precision);
			trace(str1, num.toString());*/
			//trace('');
			const precision:int = 16;
			var bigDec:SimpleBigDecimal = NumUtil.toBigDecimal(num, precision);
			trace(bigDec, num.toString());
		}
		
		protected function printNPN(num:Number):void {
			var str:String = StrUtil.format('{:.16e}', num);
			if (str.search(/\. 0* 5 0* [6-9] \d e .* $/x) != -1)
				trace(str);
		}
		
		protected function printNumAndNeighbors(num:Number, str:String = ''):void {
			num *= -1;
			if (str)
				str = '-' + str;
			var parseF:Number = parseFloat(str ? str : num.toExponential(16));
			var parseN:Number = NumUtil.parseNumber(str ? str : num.toExponential(16));
			StrUtil.tformat(' prior={:.20e}', NumUtil.prior(num));
			StrUtil.tformat('number={:.20e}\nparseF={:.20e} dist={} ULPs\nparseN={:.20e} dist={} ULPs', num, parseF, NumUtil.distance(parseF, num), parseN, NumUtil.distance(parseN, num));
			StrUtil.tformat('  next={:.20e}', NumUtil.next(num));
			trace('');
			//StrUtil.tformat('number={:.20e}\npars.p={:.20e}\nparsed={:.20e} dist={} ULPs\npars.n={:.20e}\n', num, NumUtil.prior(parsed), parsed, NumUtil.distance(parsed, num), NumUtil.next(parsed));
		}
		
		protected function testParseND(str:String):void {
			var dbl:Double = new Double(str);
			trace(str ? str : '""', parseFloat(str).toExponential(20), NumUtil.toExponential(dbl.number, 20));
			trace(dbl + '\n');
		}
		
		protected function testNumUtil():void {
			//var numbers:Array = ['', ' ', '-', '0', '-0', '.', '-.', '.0', 'e-1', 'e0', 'e1', 'e', '-e', 'e308', 'e309', '-e308', '-e309', '1e400', '-1e400', '1e-400', '-1e-400', '0e400', '.e400', '-0e400', '-.e400', '-e400',
				//'nan', 'NaN', 'infinity', 'Infinity', '+infinity', '+Infinity', '-infinity', '-Infinity', 'inf', 'Inf', '+inf', '+Inf', '-inf', '-Inf'];
			//for each (var str:String in numbers)
				//testParseND(str);
			//return;
			//
			//trace(NumUtil.toExponential(0));
			//trace(NumUtil.toFixed(0));
			//trace(NumUtil.toGeneral(0));
			//return;
			
			//printNumAndNeighbors(1e-300, '1e-300');
			//printNumAndNeighbors(1e-50, '1e-50');
			//printNumAndNeighbors(1e-10, '1e-10');
			//printNumAndNeighbors(1e-1, '1e-1');
			//printNumAndNeighbors(1, '1');
			//printNumAndNeighbors(1e10, '1e10');
			//printNumAndNeighbors(1.50000000000000011102, '1.50000000000000011102');
			//printNumAndNeighbors(9.50000000000000088818, '9.50000000000000088818');
			//printNumAndNeighbors(2.5);
			//printNumAndNeighbors(3.5);
			//printNumAndNeighbors(4.5);
			//printNumAndNeighbors(5.5);
			//printNumAndNeighbors(6.5);
			//printNumAndNeighbors(7.5);
			//printNumAndNeighbors(8.5);
			//printNumAndNeighbors(9.5);
			//printNumAndNeighbors(1e50, '1e50');
			//printNumAndNeighbors(1e300, '1e300');
			//printNumAndNeighbors(0.1, '0.1');
			//printNumAndNeighbors(9.5e-305, '9.5e-305');
			//printNumAndNeighbors(9.5e+250, '9.5e+250');
			//printNumAndNeighbors(9.00000000000004e-235, '9.00000000000004e-235');
			//printNumAndNeighbors(9.00000000000005e-235, '9.00000000000005e-235');
			//printNumAndNeighbors(9.00000000000004e+307, '9.00000000000004e+307');
			//printNumAndNeighbors(9.00000000000005e+303, '9.00000000000005e+303');
			//return;
			//StrUtil.tformat('{:.20e}\n{:.20e}\n{}\n', 9.0000000000000483e+37, parseFloat('9.0000000000000483e+37'), 9.0000000000000483e+37 == parseFloat('9.0000000000000483e+37'));
			//StrUtil.tformat('{:.20e}\n{:.20e}\n{:.20e}\n', NumUtil.prior(9.0000000000000483e+37), 9.0000000000000483e+37, NumUtil.next(9.0000000000000483e+37));
			//StrUtil.tformat('{:.20e}\n{:.20e}\n{}\n', 8.90000000000005e+307, parseFloat('8.90000000000005e+307'), 8.90000000000005e+307 == parseFloat('8.90000000000005e+307'));
			//StrUtil.tformat('{:.20e}\n{:.20e}\n{}\n', 9.10000000000005e+37, parseFloat('9.10000000000005e+37'), 9.10000000000005e+37 == parseFloat('9.10000000000005e+37'));
			//return;
			//for (var exp:int = -307; exp <= 307; ++exp)
				//for (var dig1:int = 1; dig1 <= 9; ++dig1)
					//for (var numZeros:int = 0; numZeros <= 0; ++numZeros)
						//printNPN(parseFloat(dig1 + '.' + StrUtil.repeat('0', numZeros) + '5e' + exp));
			//var dbl:Double = new Double(9.900000000006748e+300);
			//trace(dbl.prior().toExponential(CHECK_PRECISION - 1));
			//trace(dbl.next().toExponential(CHECK_PRECISION - 1));
			//trace(dbl.next().toExponential(CHECK_PRECISION - 1));
			//return;
			//checkDoubleDigits();
			//x = 2.80030000000071791e-306;
			//trace(x.toExponential(20), x.toExponential(17), NumUtil.toExponential(x, 17));
			//x = 2.8003000000007176e-306;
			//trace(x.toExponential(20), x.toExponential(17), NumUtil.toExponential(x, 17));
			//return;
			//var x:Number = MathExt.DOUBLE_MIN;
			//var count:int = 30;
			//while (count--) {
				//StrUtil.tformat('{:.2}', x);
				//x = NumUtil.next(x);
			//}
			//return;
			//StrUtil.tformat('{}', NumUtil.prior(0.000000012345));
			//trace(NumUtil.prior(0.000000012345), MathExt.digits(NumUtil.prior(0.000000012345)));
			//StrUtil.tformat('{}', 0.000000012345);
			//trace(0.000000012345, MathExt.digits(0.000000012345));
			//StrUtil.tformat('{}', NumUtil.next(0.000000012345));
			//trace(NumUtil.next(0.000000012345), MathExt.digits(NumUtil.next(0.000000012345)));
			//trace(0.0000012345, Number(0.0000012345).toExponential(17), MathExt.digits(0.0000012345), StrUtil.format('{:.4e}', 0.0000012345));
			//trace(new Double('1.25e10').hexFloat);
			//trace(new Double('1.25e10').hexString);
			//trace(new Double('1.25e10').bitString);
			//trace(NumUtil.parseHexFloat('1.74876e8p+33'));
			//trace(NumUtil.parseHexString('42074876e8000000'));
			//trace(NumUtil.parseBitString('0100001000000111010010000111011011101000000000000000000000000000'));
			//return;
			testNumUtilNumber(-1*0);
			testNumUtilNumber(0, '0');
			testNumUtilNumber(MathExt.DOUBLE_MIN);
			testNumUtilNumber(MathExt.DOUBLE_MIN * 2);
			testNumUtilNumber(MathExt.DOUBLE_MIN * 4);
			testNumUtilNumber(MathExt.DOUBLE_MIN * 8);
			testNumUtilNumber(MathExt.DOUBLE_MIN * 16);
			testNumUtilNumber(MathExt.DOUBLE_MIN * 32);
			testNumUtilNumber(MathExt.DOUBLE_MIN * 64);
			testNumUtilNumber(MathExt.DOUBLE_MIN * 128);
			testNumUtilNumber(MathExt.DOUBLE_MIN * 256);
			testNumUtilNumber(MathExt.DOUBLE_MIN * 512);
			testNumUtilNumber(MathExt.DOUBLE_MIN * 1024);
			testNumUtilNumber(MathExt.DOUBLE_MIN * 2048);
			testNumUtilNumber(MathExt.DOUBLE_MIN * 4096);
			testNumUtilNumber(MathExt.DOUBLE_MIN * 8192);
			testNumUtilNumber(MathExt.DOUBLE_MIN * 16384);
			testNumUtilNumber(MathExt.DOUBLE_MIN * 32768);
			testNumUtilNumber(MathExt.DOUBLE_MIN * 65536);
			testNumUtilNumber(MathExt.DOUBLE_MIN * 131072);
			testNumUtilNumber(MathExt.DOUBLE_MIN * 262144);
			testNumUtilNumber(MathExt.DOUBLE_MIN * 524288);
			testNumUtilNumber(MathExt.DOUBLE_MIN * 1048576);
			testNumUtilNumber(2.0, '2');
			testNumUtilNumber(.5, '0.5');
			testNumUtilNumber(.75, '0.75');
			testNumUtilNumber(.25, '0.25');
			testNumUtilNumber(.125, '0.125');
			testNumUtilNumber(.0625, '0.0625');
			testNumUtilNumber(Infinity);
			testNumUtilNumber(-Infinity);
			testNumUtilNumber(NaN);
			testNumUtilNumber(0.0123);
			testNumUtilNumber(0.123);
			testNumUtilNumber(0.5);
			testNumUtilNumber(1.23);
			testNumUtilNumber(12.3);
			testNumUtilNumber(123);
			testNumUtilNumber(123400000);
			testNumUtilNumber(0.00000012345);
			testNumUtilNumber(0.00000123455);
			testNumUtilNumber(0.00000123456);
			testNumUtilNumber(MathExt.DOUBLE_MIN);
			testNumUtilNumber(MathExt.DOUBLE_MIN_NORMAL);
			testNumUtilNumber(MathExt.DOUBLE_MAX);
			testNumUtilNumber(Double.MIN_NORMAL.prior().number);
			testNumUtilNumber(1234.5);
			testNumUtilNumber(0.00012345);
			testNumUtilNumber(1);
			testNumUtilNumber(0);
			testNumUtilNumber(123456789012345);
			testNumUtilNumber(12345678901234);
			testNumUtilNumber(1234567890123);
			testNumUtilNumber(123456789012);
			testNumUtilNumber(12345678901);
			testNumUtilNumber(1234567890);
			testNumUtilNumber(123456789);
			testNumUtilNumber(12345678);
			testNumUtilNumber(1234567);
			testNumUtilNumber(123456);
			testNumUtilNumber(12345);
			testNumUtilNumber(1234.5);
			testNumUtilNumber(123.45);
			testNumUtilNumber(12.345);
			testNumUtilNumber(1.2345);
			testNumUtilNumber(0.99999999999999);
			testNumUtilNumber(0.89999999999999);
			testNumUtilNumber(0.79999999999999);
			testNumUtilNumber(0.69999999999999);
			testNumUtilNumber(0.59999999999999);
			testNumUtilNumber(0.49999999999999);
			testNumUtilNumber(0.39999999999999);
			testNumUtilNumber(0.29999999999999);
			testNumUtilNumber(0.19999999999999);
			testNumUtilNumber(0.12345);
			testNumUtilNumber(0.099999999999999);
			testNumUtilNumber(0.012345);
			testNumUtilNumber(0.0012345);
			testNumUtilNumber(0.00012345);
			testNumUtilNumber(0.000012345);
			testNumUtilNumber(0.0000012345);
			testNumUtilNumber(0.00000012345);
			testNumUtilNumber(0.000000012345);
			testNumUtilNumber(0.0000000012345);
			testNumUtilNumber(0.00000000012345);
			testNumUtilNumber(0.01);
			testNumUtilNumber(-0.01);
			
			//testNumUtilNumber(0.0000012345); // Should double round
			
			var startTime:int;
			var i:int, j:int;
			
			/*for (j = 0; j < 3; ++j) {
				startTime = getTimer();
				for (i = 0; i < 1024; ++i)
					NumUtil.toDigits(MathExt.DOUBLE_MIN * Math.pow(2, i));
				trace('took', getTimer() - startTime, 'ms');
			}
			trace('');
			trace(MathExt.DOUBLE_MIN.toExponential(20));
			trace(NumUtil.toDigits(MathExt.DOUBLE_MIN));*/
			
			/*for (j = 0; j < 3; ++j) {
				startTime = getTimer();
				for (i = 0; i < 2048; ++i)
					NumUtil.toDigits(MathExt.DOUBLE_MAX / Math.pow(2, i / 2));
				trace('took', getTimer() - startTime, 'ms');
			}
			trace('');
			trace(MathExt.DOUBLE_MAX.toExponential(20));
			trace(NumUtil.toDigits(MathExt.DOUBLE_MAX));*/
			
			//testNumUtilNumber(0.1);
			//testNumUtilNumber(0.00000123455);
			//testNumUtilNumber(0.00000123456);
			//testNumUtilNumber(1.23454999999999981419);
			//testNumUtilNumber(NumUtil.next(1.23454999999999981419).number);
			//trace(Number(0.12355).toPrecision(4)); // FAIL !
			//trace(Number(-0.12355).toPrecision(4)); // FAIL !
			//testNumUtilNumber(MathExt.DOUBLE_MAX / 1024);
			//testNumUtilNumber(MathExt.DOUBLE_MIN * 1024);
			//testNumUtilNumber(1234567890123454999e10);
			//0.00000123455)); // FAIL! rounding error
			//trace(nf1.format(0.00000123456)); // FAIL! rounding error
			//trace(nf1.format(0.000000000123456));
			//trace(testRound(0.00000123455, 11, RoundingMode.HALF_AWAY_FROM_ZERO));
			//trace(testRound(0.00000123455, 10, RoundingMode.HALF_AWAY_FROM_ZERO));
			//trace(testRound(0.00000123455, 9, RoundingMode.HALF_AWAY_FROM_ZERO));
			//trace(testRound(0.00000123455, 8, RoundingMode.HALF_AWAY_FROM_ZERO));
			//trace('');
			//trace(testRound(0.00000123455, 11, RoundingMode.TOWARDS_ZERO));
			//trace(testRound(0.00000123455, 10, RoundingMode.TOWARDS_ZERO));
			//trace(testRound(0.00000123455, 9, RoundingMode.TOWARDS_ZERO));
			//trace(testRound(0.00000123455, 8, RoundingMode.TOWARDS_ZERO));
			//trace('');
			//trace(testRound(1.23454999999999981419, 5, RoundingMode.HALF_ODD));
			//trace(testRound(1.23454999999999981419, 10, RoundingMode.HALF_ODD));
			//trace(testRound(1.23454999999999981419, 11, RoundingMode.HALF_ODD));
			//trace(testRound(1.23454999999999981419, 12, RoundingMode.HALF_ODD));
			//trace(testRound(1.23454999999999981419, 13, RoundingMode.HALF_ODD));
			//trace(testRound(1.23454999999999981419, 14, RoundingMode.HALF_ODD));
			//trace(testRound(1.23454999999999981419, 15, RoundingMode.HALF_ODD));
			//trace(testRound(1.23454999999999981419, 16, RoundingMode.HALF_ODD));
			
			//trace('Smallest non-zero subnormal number.');
			//testNumUtilNumber(MathExt.DOUBLE_MIN);
			//trace('Largest subnormal number, one below smallest normal number.');
			//testNumUtilNumber(Double.prior(MathExt.DOUBLE_MIN_NORMAL).number);
			//trace('Smallest normal number.');
			//testNumUtilNumber(MathExt.DOUBLE_MIN_NORMAL);
			//trace('Largest representable power of two (2^' + Double.EXPONENT_MAX + ').');
			//testNumUtilNumber(Math.pow(2, Double.EXPONENT_MAX));
			//trace('Largest representable number.');
			//testNumUtilNumber(MathExt.DOUBLE_MAX);
			/*var intPart:SimpleBigInt = new SimpleBigInt(3);
			trace(intPart);
			intPart.insertHighBits(-1, 32);
			intPart.insertHighBits(-1, 64);
			intPart.insertHighBits(-1, 96);
			trace(intPart);
			intPart.insertHighBits(0, 32, true);
			intPart.insertHighBits(0, 64, true);
			intPart.insertHighBits(0, 96, true);
			trace(intPart);
			intPart.insertLowBits(0xdb, 63);
			intPart.insertLowBits(0xdb, 96);
			intPart.insertLowBits(0xdb, 88);
			intPart.insertLowBits(0xdb << 24, 3);
			intPart.insertHighBits(0xdb, 5);
			trace(intPart);
			trace('');
			var bv:BitVector = new BitVector(96);
			trace(bv.toString(true));
			bv.insertHighBits(-1, 32);
			bv.insertHighBits(-1, 64);
			bv.insertHighBits(-1, 96);
			trace(bv.toString(true));
			bv.insertHighBits(0, 32, true);
			bv.insertHighBits(0, 64, true);
			bv.insertHighBits(0, 96, true);
			trace(bv.toString(true));
			bv.insertLowBits(0xdb, 63);
			bv.insertLowBits(0xdb, 96);
			bv.insertLowBits(0xdb, 88);
			bv.insertLowBits(0xdb << 24, 3);
			bv.insertHighBits(0xdb, 5);
			trace(bv.toString(true));*/
		}
		
		protected static const CHECK_NUMBER:Number = 5.5e300;
		protected static const CHECK_NUMBER_STEP:Number = 0.1;
		protected static const CHECK_PRECISION:int = 16;
		protected static const NUMBERS_PER_ITER:int = 1000;
		protected static const MAX_RUN_SECONDS:int = 60;
		protected var _dbl:Double;
		protected var _lastDigit:int;
		protected var _startTime:int;
		protected var _numbersFoundIter:uint;
		protected var _numbersFoundTotal:uint;
		protected var _numbersTotal:uint;
		
		protected function checkDoubleDigits():void {
			_dbl = new Double(CHECK_NUMBER).advance(-NUMBERS_PER_ITER);
			_startTime = getTimer();
			var timer:Timer = new Timer(50);
			timer.addEventListener(TimerEvent.TIMER, checkDoubleDigitsIter);
			timer.start();
			//stage.addEventListener(MouseEvent.RIGHT_CLICK, function (e:MouseEvent):void { timer.stop(); });
		}
		
		protected function checkDoubleDigitsIter(e:TimerEvent):void {
			var elapsed:Number = (getTimer() - _startTime) / 1000;
			_statusText.text = StrUtil.format('{1:0{0}.2f} s elapsed, num={2:.16e} hexF={8} binExp={7}, @ {3:5.1f} num/s, {4}/{5} ({6:p}) imprecise', MAX_RUN_SECONDS.toString().length + 3, elapsed, _dbl.number,
				elapsed ? (_numbersTotal / elapsed) : 0, _numbersFoundTotal, _numbersTotal, _numbersTotal ? (_numbersFoundTotal / _numbersTotal) : 0, _dbl.exponent, _dbl.hexFraction);
			/*if (elapsed > MAX_RUN_SECONDS) {
				statusText.text += '   FINISHED';
				(e.currentTarget as Timer).removeEventListener(TimerEvent.TIMER, checkDoubleDigitsIter);
				return;
			}*/
			
			_numbersFoundIter = 0;
			_lastDigit = _getLastDigit();
			_dbl.next();
			var count:int = NUMBERS_PER_ITER;
			while (count--) {
				var digit:int = _getLastDigit();
				if (((_lastDigit == 9) && (digit != 0)) || ((digit - _lastDigit) != 1)) {
					public::trace(_dbl.toExponential(16), Double.prior(_dbl).toExponential(16), digit, _lastDigit);
					//public::trace(_dbl.toExponential(CHECK_PRECISION - 1), Double.prior(_dbl).toExponential(CHECK_PRECISION - 1), digit, _lastDigit);
					++_numbersFoundIter;
				}
				_lastDigit = digit;
				_dbl.next();
			}
			_numbersFoundTotal += _numbersFoundIter;
			//public::trace(StrUtil.format('{:.3e} hexF={} binExp={} {}/{} ({:p}) imprecise', _dbl.number, _dbl.hexFraction, _dbl.exponent, _numbersFoundIter, NUMBERS_PER_ITER, _numbersFoundIter / NUMBERS_PER_ITER));
			_dbl.number += CHECK_NUMBER_STEP * Math.pow(10, Math.floor(Math.log(_dbl.number) * Math.LOG10E));
			_dbl.advance(-NUMBERS_PER_ITER);
			_numbersTotal += NUMBERS_PER_ITER;
		}
		
		protected function _getLastDigit():int {
			return _dbl.toExponential(CHECK_PRECISION - 1).replace(/^ .* (\d) e [-+] \d* $/x, '$1').charCodeAt(0) - 48;
		}
		
		protected function testObjUtil():void {
			var objA:Object = {1: 'a', 2: {3: 'b', 4: 'd'}, 7: 'U1', 8: {9: 'U2'}};
			var objB:Object = {1: 'A', 2: {3: 'B'}, 5: {6: 'C'}};
			trace('objA                 =', StrUtil.valueToString(objA));
			trace('objB                 =', StrUtil.valueToString(objB));
			trace('clone(objB)          =', StrUtil.valueToString(ObjUtil.clone(objB)));
			trace('deepClone(objB)      =', StrUtil.valueToString(ObjUtil.deepClone(objB)));
			trace('copy(objB, objA)     =', StrUtil.valueToString(ObjUtil.copy(objB, ObjUtil.deepClone(objA))));
			trace('deepCopy(objB, objA) =', StrUtil.valueToString(ObjUtil.deepCopy(objB, ObjUtil.deepClone(objA))));
			trace('objA                 =', StrUtil.valueToString(objA));
			trace('objB                 =', StrUtil.valueToString(objB));
			return;
			
			var x:int = -15;
			trace(x, '\n', StrUtil.valueToBinaryString(x).replace(/(.{8})/g, '$1 ') + '\n', ObjUtil.valueToBitVector(x).bitString.replace(/(.{8})/g, '$1 '));
			var y:Number = 1.2125;
			trace(y, '\n', StrUtil.valueToBinaryString(y).replace(/(.{8})/g, '$1 ') + '\n', ObjUtil.valueToBitVector(y).bitString.replace(/(.{8})/g, '$1 '));
			var a:Array = [1, 2, 3];
			trace(StrUtil.valueToString(a) + '\n', StrUtil.valueToBinaryString(a).replace(/(.{8})/g, '$1 ') + '\n', ObjUtil.valueToBitVector(a).bitString.replace(/(.{8})/g, '$1 '));
		}
		
		protected function testBitVector():void {
			var bitString:String =
				   '11111111111111111111111111111' +
				'10101010101010101010101010101010' +
				'11001100110011001100110011001100' +
				'01010101010101010101010101010101' +
				'00110011001100110011001100110011' +
				'00000000000000000000000000000111';
			var bv:BitVector = new BitVector(bitString);
			trace(('   ' + bitString).replace(/(.{32})/g, '$1\n'));
			trace(bv);
			trace('');
			trace(('   ' + bv.bitString).replace(/(.{32})/g, '$1\n'));
			
			var iterator:IIterator = bv.iterator;
			var str:String = '';
			var lines:Array = [];
			while (iterator.hasNext) {
				str += iterator.getNext();
				if ((str.length % 32) == 0) {
					lines.push(str);
					str = '';
				}
			}
			if (str)
				lines.push(str);
			trace(lines.join('\n'));
			iterator = bv.reverseIterator;
			
			trace('');
			str = '   ';
			lines = [];
			while (iterator.hasNext) {
				str += iterator.getNext();
				if ((str.length % 32) == 0) {
					lines.push(str);
					str = '';
				}
			}
			if (str)
				lines.push(str);
			trace(lines.join('\n'));
			/*bv.toggleAllBits();
			trace(('   ' + bitString).replace(/(.{32})/g, '$1\n'));
			trace(bv);
			trace('');
			trace(('   ' + bv.bitString).replace(/(.{32})/g, '$1\n'));
			bv.clearAllBits();
			trace(('   ' + bv.bitString).replace(/(.{32})/g, '$1\n'));
			bv.setAllBits();
			trace(('   ' + bv.bitString).replace(/(.{32})/g, '$1\n'));
			bv.length = 192;
			trace(bv.bitString.replace(/(.{32})/g, '$1\n'));
			trace(new BitVector(bv.bitString).bitVector.join('').replace(/(.{32})/g, '$1\n'));*/
		}
		
		protected function testObjectIterator():void {
			var obj:Object = {a:1, b:2, c:3, d:0, e: -4, f: -9, t:5, x:Math.PI / 4, y: -1, m:0, Z:'Z', A:'A', B:'B', O:{xxx:'0x0', yyy:'0y0'}};
			trace(StrUtil.objectToString(obj));
			var iterator:ObjectIterator = new ObjectIterator(obj, true);
			while (iterator.hasNext) {
				var name:String = iterator.getNext();
				trace(name + '=' + obj[name]);
				if (obj[name] < 0)
					iterator.remove();
			}
			trace(StrUtil.valueToString(obj));
		}
		
		protected function testArrayIterator():void {
			var a:Array = [1, 2, 3, 4, 5, 6];
			trace(StrUtil.arrayToString(a));
			var iterator:IIterator = new ArrayIterator(a);
			iterator.forEachRemaining(trace);
			trace('');
			while (iterator.hasNext)
				trace(iterator.getNext());
			trace('');
			iterator = new ArrayIterator(a, ArrayIterator.REVERSE);
			iterator.forEachRemaining(trace);
			trace('------');
			trace(iterator.getNext());
			trace(iterator.getNext());
			trace('--');
			iterator.forEachRemaining(trace);
			trace(iterator.getNext(), '*rm');
			iterator.remove();
			trace('---[');
			iterator.forEachRemaining(trace);
			trace('---]');
			trace(iterator.getNext(), '*rm');
			iterator.remove();
			trace(iterator.getNext());
			trace(iterator.getNext());
			trace(StrUtil.arrayToString(a));
		}
		
		protected function testVectorIterator():void {
			var vInt:Vector.<Object> = new <Object>[{x:1}, {y:2}, {z:3}, {a:4}, {b:5}, {c:6}];
			trace(StrUtil.valueToString(vInt));
			var iterator:IIterator = new VectorIterator(vInt);
			while (iterator.hasNext)
				trace(StrUtil.valueToString(iterator.getNext()));
			trace('');
			iterator = new VectorIterator(vInt, VectorIterator.REVERSE);
			while (iterator.hasNext)
				trace(StrUtil.valueToString(iterator.getNext()));
			trace('');
			iterator = new VectorIterator(new <Array>[[1,2,3], [4,5,6], [7,8,9], [10,11,12,13]]);
			while (iterator.hasNext)
				trace(StrUtil.valueToString(iterator.getNext()));
			trace('');
			iterator = new VectorIterator(new <DisplayObject>[this, this.parent]);
			while (iterator.hasNext)
				trace(iterator.getNext());
			trace('');
		}
		
		protected function testDeque():void {
			var ds:Stack = new Stack(1, 2, 3);
			ds.push(4);
			ds.push(5, 6);
			trace(ds);
			var iterator:IIterator = ds.iterator;
			while (iterator.hasNext)
				trace(iterator.getNext());
			trace('');
			var dq:Queue = new Queue(1, 2, 3);
			dq.add(4);
			dq.add(5, 6);
			trace(dq);
			iterator = dq.iterator;
			while (iterator.hasNext)
				trace(iterator.getNext());
			trace('');
			var dd:Deque = new Deque(1, 2, 3);
			dd.addLast(4);
			dd.addFirst(5, 6);
			trace(dd);
			iterator = dd.iterator;
			while (iterator.hasNext)
				trace(iterator.getNext());
			trace('');
			iterator = dd.reverseIterator;
			while (iterator.hasNext)
				trace(iterator.getNext());
		}
		
		protected function testTime():void {
			const iterations:int = 5000;
			var bitString:String =
				   '11111111111111111111111111111' +
				'10101010101010101010101010101010' +
				'11001100110011001100110011001100' +
				'01010101010101010101010101010101' +
				'00110011001100110011001100110011' +
				'00000000000000000000000000000111';
			var bv:BitVector = new BitVector(bitString);
			var result1:String = '';
			var result2:String = '';
			var time:int = getTimer();
			while (getTimer() < time + 250)
				Math.pow(3.77, 2.21);
			
			time = getTimer();
			var count:int = iterations;
			while (count--) {
				var iterator:BitVectorIterator = bv.reverseIterator as BitVectorIterator;
				while (iterator.hasNext)
					iterator.getNext();
			}
			trace('1 took', getTimer() - time, 'ms');
			
			time = getTimer();
			count = iterations;
			while (count--) {
				for (var i:int = bv.length - 1; i >= 0; --i)
					bv.getBitAt(i);
			}
			trace('2 took', getTimer() - time, 'ms');
			
			trace('result is correct', (result1 == result2));
		}
		
		protected function testDoubleNumber(num:Number):void {
			var d:Double = new Double(num);
			var d2:Double = Double.advance(d, 1024);
			trace(d);
			trace(d2.distance(d));
			var c:int = 12;
			while (c--) {
				d.advance(-1);
				d2.advance(1);
				trace(d2);
				trace(d2.distance(d));
			}
		}
		
		protected function testDouble():void {
			//var hfd:Double = new Double();
			//hfd.hexFloat = new Double('1.25e10').hexFloat;
			//trace(hfd);
			//return;
			/*var pos:Double = new Double(3);
			var one:Double = new Double(1);
			var neg:Double = new Double(-2);
			trace('p=' + pos.number, 'n=' + neg.number, 'o=' + one.number);
			trace('p-o=' + pos.distance(one));
			trace('o-p=' + one.distance(pos));
			trace('n-o=' + neg.distance(one));
			trace('o-n=' + one.distance(neg));
			trace('p-n=' + pos.distance(neg));
			trace('n-p=' + neg.distance(pos));
			trace('p-0=' + pos.distance(Double.ZERO));
			trace('p--0=' + pos.distance(Double.NEG_ZERO));
			trace('0-p=' + Double.ZERO.distance(pos));
			trace('-0-p=' + Double.NEG_ZERO.distance(pos));
			trace('n-0=' + neg.distance(Double.ZERO));
			trace('n--0=' + neg.distance(Double.NEG_ZERO));
			trace('0-n=' + Double.ZERO.distance(neg));
			trace('-0-n=' + Double.NEG_ZERO.distance(neg));*/
			//testDoubleNumber(1324236879.892381008);
			//testDoubleNumber(-1);
			//testDoubleNumber(-NaN);
			//testDoubleNumber(Infinity);
			//testDoubleNumber(-Infinity);
			//testDoubleNumber(0);
			//testDoubleNumber(-MathExt.DOUBLE_MAX);
			//testDoubleNumber(MathExt.DOUBLE_MAX);
			//testDoubleNumber(MathExt.DOUBLE_MAX / 2);
			//testDoubleNumber(MathExt.DOUBLE_MAX / 4);
			//testDoubleNumber(MathExt.DOUBLE_MAX / 16);
			//testDoubleNumber(MathExt.DOUBLE_MAX / 65536);
			//testDoubleNumber(MathExt.DOUBLE_MIN_NORMAL);
			//testDoubleNumber(MathExt.DOUBLE_MIN_NORMAL / 2);
			//testDoubleNumber(MathExt.DOUBLE_MIN_NORMAL / 4);
			//testDoubleNumber(MathExt.DOUBLE_MIN_NORMAL / 16);
			//testDoubleNumber(MathExt.DOUBLE_MIN_NORMAL / 65536);
			//testDoubleNumber(MathExt.DOUBLE_MIN);
			//testDoubleNumber(MathExt.DOUBLE_MIN * 2);
			//testDoubleNumber(MathExt.DOUBLE_MIN * 4);
			//testDoubleNumber(MathExt.DOUBLE_MIN * 16);
			//testDoubleNumber(MathExt.DOUBLE_MIN * 65536);
			//testDoubleNumber(1);
			//testDoubleNumber(0.1);
			//testDoubleNumber(-0.00000123455);
			//testDoubleNumber(123456);
			//testDoubleNumber(1 / 49 * 49 - 1);
		}
		
		protected function testStrUtil():void {
			trace(StrUtil.repeat('0', 3));
			trace(StrUtil.repeat('0', 4));
			trace(StrUtil.repeat('0', 5));
			trace(StrUtil.repeat('0', 10));
			trace(StrUtil.repeat('0', 15));
			trace(StrUtil.repeat('01', 1));
			trace(StrUtil.repeat('01', 3));
			trace(StrUtil.repeat('01', 5));
			trace(StrUtil.repeat('01', 8));
			trace(StrUtil.lpad('12345.67', '0', 6));
			trace(StrUtil.lpad('12345.67', '0', 7));
			trace(StrUtil.lpad('12345.67', '0', 8));
			trace(StrUtil.lpad('12345.67', '0', 9));
			trace(StrUtil.lpad('12345.67', '0', 10));
			trace(StrUtil.lpad('12345.67', '0', 12));
			trace(StrUtil.lpad('12345.67', '0', 15));
			trace(StrUtil.rpad('12345.67', '0', 6));
			trace(StrUtil.rpad('12345.67', '0', 7));
			trace(StrUtil.rpad('12345.67', '0', 8));
			trace(StrUtil.rpad('12345.67', '0', 9));
			trace(StrUtil.rpad('12345.67', '0', 10));
			trace(StrUtil.rpad('12345.67', '0', 12));
			trace(StrUtil.rpad('12345.67', '0', 15));
			trace(StrUtil.trim('000000', '0'));
			trace(StrUtil.trim('0001000', '0'));
			trace(StrUtil.trim('0001', '0'));
			trace(StrUtil.trim('1000', '0'));
			trace(StrUtil.trim('010', '0'));
			trace(StrUtil.trim('000123000', '0'));
			trace(StrUtil.trim('000123', '0'));
			trace(StrUtil.trim('123000', '0'));
			trace(StrUtil.trim('01230', '0'));
			trace(StrUtil.trim('123', '0'));
		}
		
		protected var mathContext:MathContext;
		protected var typeConverter:TypeConverter;
		protected var mathLib:MathLib;
		protected var mathFun:MathFun;
		protected var mathOperator:MathOperator;
		protected var tokenFactory:IMathTokenFactory;
		protected var mathParser:MathParser;
		protected var stringLexer:StringLexer;
		protected var mathTokenLexer:MathTokenLexer;
		protected var mathTokenArrayProvider:MathTokenArrayMathTokenProvider;
		protected var mathTokenVectorProvider:MathTokenVectorMathTokenProvider;
		protected var stringConverter:StringConverter;
		protected var mathTokenConverter:MathTokenConverter;
		
		protected function initMathParser():void {
			if (mathParser)
				return;
			mathContext = new MathContext(RoundingMode.HALF_AWAY_FROM_ZERO, AngleUnit.RADIAN, 2.2e-322, 1e-14, FPMode.MATHIC, 0, {a:1, b:2, c:3, d:0, e: -4, f: -9, t:5, x:Math.PI / 4, y: -1, m:0}, {pi:Math.PI, PI:Math.PI});
			typeConverter = TypeConversionFun.extend(new TypeConverter());
			mathLib = new MathLib(mathContext, typeConverter);
			mathFun = new MathFun(mathContext, mathLib);
			mathOperator = new MathOperator(mathFun);
			tokenFactory = new MathTokenFactory();
			//tokenFactory = new PooledMathTokenFactory();
			stringLexer = new StringLexer();
			//stringLexer.numberAllowSoloRadix = true;
			mathTokenLexer = new MathTokenLexer();
			mathParser = new MathParser(mathContext, mathFun, mathOperator, mathTokenLexer, null, tokenFactory);
			mathTokenArrayProvider = new MathTokenArrayMathTokenProvider();
			mathTokenVectorProvider = new MathTokenVectorMathTokenProvider();
			stringConverter = new StringConverter(mathContext, mathFun, mathOperator, tokenFactory, StringConverter.KEEP_WHITESPACE | StringConverter.USE_ERROR_TOKENS);
			mathTokenConverter = new MathTokenConverter();
		}
		
		protected function testMathParser():void {
			initMathParser();
			//testEval('.');
			//testEval('0.');
			//testEval('.0');
			//testEval('0.0');
			//testEval('.e1');
			//testEval('.0e1');
			//testEval('0.e1');
			//return;
			//var src:Array/*MathToken*/ = new Array();
			//// 123.4e-2
			//src.push(new NumberCharToken('1'), new NumberCharToken('2'), new NumberCharToken('3'), new NumberCharToken('.'), new NumberCharToken('4'), new NumberCharToken('e'), new NumberCharToken('-'), new NumberCharToken('2'));
			//// * (
			//src.push(new OperatorToken(MathToken.OPERATOR, '*'), new MathToken(MathToken.LEFT_PAREN));
			//// - e1
			//src.push(new OperatorToken(MathToken.OPERATOR, '-'), new NumberCharToken('e'), new NumberCharToken('1'));
			//// + 10
			//src.push(new OperatorToken(MathToken.OPERATOR, '+'), new NumberCharToken('1'), new NumberCharToken('0'));
			//// sin(1.5PI)
			//src.push(new OperatorToken(MathToken.FUN_JUXTA, 'sin'), new NumberCharToken('1'), new NumberCharToken('.'), new NumberCharToken('5'), new IdentifierToken(MathToken.CONSTANT, 'PI'));
			//// - max(
			//src.push(new OperatorToken(MathToken.OPERATOR, '-'), new OperatorToken(MathToken.FUN_PAREN, 'max'), new MathToken(MathToken.LEFT_PAREN));
			//// 60, 80
			//src.push(new NumberCharToken('6'), new NumberCharToken('0'), new MathToken(MathToken.LIST_SEP), new DummyToken(' '), new NumberCharToken('8'), new NumberCharToken('0'));
			//// ))
			//src.push(new MathToken(MathToken.RIGHT_PAREN), new MathToken(MathToken.RIGHT_PAREN));
			//testEvalMTL(src);
			//return;
			
			//const ITER_COUNT:int = 1000000;
			//const VAR_NAME:String = 'x';
			//const CONST_NAME:String = 'const';
			//const VAR_VALUE:Number = mathContext.getVariable(VAR_NAME);
			//mathContext.constants = mathContext.variables;
			//for (var n:int = 0; n < 2000; ++n)
				//mathContext.setConstant('n' + n, n);
			//mathContext.setConstant(CONST_NAME, VAR_VALUE);
			//trace(VAR_NAME + '=' + VAR_VALUE);
			//var time:int = getTimer();
			//for (var i:int = 0; i < ITER_COUNT; ++i)
				//mathContext.getVariable(VAR_NAME);
			//trace('1:', getTimer() - time, 'ms', (mathContext.getVariable(VAR_NAME) == VAR_VALUE) ? 'correct' : 'incorrect');
			////trace('1:', getTimer() - time, 'ms', isNaN(mathContext.getVariable(VAR_NAME)) ? 'correct' : 'incorrect');
			//time = getTimer();
			//for (i = 0; i < ITER_COUNT; ++i)
				//mathContext.getConstant(CONST_NAME);
			//trace('2:', getTimer() - time, 'ms', (mathContext.getConstant(CONST_NAME) == VAR_VALUE) ? 'correct' : 'incorrect');
			////trace('2:', getTimer() - time, 'ms', isNaN(mathContext.getConstant(CONST_NAME)) ? 'correct' : 'incorrect');
			//return;
			
			//mathParser.lexer = stringLexer;
			//trace(StrUtil.valueToString(mathContext.variables));
			//testEval('2→a+1+pi+3');
			//trace(StrUtil.valueToString(mathContext.variables));
			//testEval('e*=a+=b=1+3*5→-c');
			//trace(StrUtil.valueToString(mathContext.variables));
			//testEval('max(a=10,b=20,c=15)');
			//trace(StrUtil.valueToString(mathContext.variables));
			//testEval('sin 3xcos 3x * tan 3x - xxx');
			//return;
			//testEval('=10');
			//testEval('a=');
			//testEval('10=10');
			//testEval('min 2');
			//testEval('min(2)');
			//testEval('sin(2, 3)');
			//testEval('random 1');
			//testEval('random(1,2,3,4,5,)');
			//return;
			//testEval('sin 0', false);
			//testEval('sin pi⁄2', false);
			//testEval('sin pi', false);
			//testEval('sin 3pi⁄2', false);
			//testEval('sin 2pi', false);
			//testEval('sin 5pi⁄2', false);
			//testEval('sin -pi⁄2', false);
			//testEval('sin -pi', false);
			//testEval('sin -3pi⁄2', false);
			//testEval('sin -2pi', false);
			//testEval('sin -2pi', false);
			//testEval('cos 0', false);
			//testEval('cos pi⁄2', false);
			//testEval('cos pi', false);
			//testEval('cos 3pi⁄2', false);
			//testEval('cos 2pi', false);
			//testEval('cos 5pi⁄2', false);
			//testEval('cos -pi⁄2', false);
			//testEval('cos -pi', false);
			//testEval('cos -3pi⁄2', false);
			//testEval('cos -2pi', false);
			//testEval('tan 0', false);
			//testEval('tan pi⁄2', false);
			//testEval('tan pi', false);
			//testEval('tan 3pi⁄2', false);
			//testEval('tan 2pi', false);
			//testEval('tan 5pi⁄2', false);
			//testEval('tan -pi⁄2', false);
			//testEval('tan -pi', false);
			//testEval('tan -3pi⁄2', false);
			//testEval('tan -2pi', false);
			//return;
			//testEval('32\\3');
			//testEval('32%3');
			//testEval('1e1+1e');
			//return;
			//testEval('-10*-20+3*(-1-2)');
			//testEval('random() + random()');
			//testEval('random 3 + random 3');
			//var randExpr:MathExpression = mathParser.parse('random()');
			//for (var i:int = 0; i < 20; ++i)
				//trace(randExpr.eval() ? randExpr.value : (randExpr.errorName + ': ' + randExpr.errorMessage));
			//return;
			//testEval('.');
			//testEval('2sin');
			//testEval('2sin0.5236');
			//testEval('1,2');
			//testEval('(1,)');
			//testEval('1+1()');
			//testEval('(sin(10),())');
			//return;
			//testEval('sin k');
			testEval('1 + 2 * (1 + min(1, 3) / max(1, 2) - 1)');
			//testEval('1−−1');
			//testEval('');
			//testEval('10-1+6');
			testEval('2^3^4');
			//testEval('-1-1+-1');
			//testEval('-2^-1');
			//testEval('-1-1+-+-1');
			//testEval('((a+t)*((b+(a+c))^(c+d)))');
			//testEval('-1-1+--+-1');
			testEval('-2e1^2+-1*(300-100)/(5+3+2)++10*f*sin(PI/2)');
			testEval('9.5E250'); // 9.5 * E * 250 - invalid implicit multiplication with 2nd number being a number.
			testEval('9.5e250 / 1.5e250');
			testEval('3 + 4 * 2 / -( 1 - 5 ) ^ 2 ^ 3');
			testEval('2^sin(cos 1)');
			testEval('sin    cos PI  ');
			testEval('2^-3*4');
			testEval('10 + max(1, 2, min(6,4,5), 3)');
			testEval('2ⁿ√3ⁿ√27');
			testEval('3 ⁿ√ sqrt 4 - curt 2 ⁿ√ 4');
			testEval('2²³ - 2^2^3');
			testEval('sin 3xcos 3x * tan 3x');
			//testEval('[[1,2,3],[4,5,6],[7,8,9]]');
			//testEval('1/0');
			testEval('tan (PI/2)');
			testEval('cos (PI/2)');
			testEval('3 + 16 / 2 / 4 ^ 2 ^ 3');
			testEval('sqrt 4 - 2'); // Should be 0
			testEval('randomRange(-100, 100, 3)');
			
			trace('');
			mathParser.valueTransform = function (value:Number):Number {
				if (MathExt.isNaN(value))
					throw new ArithmeticError('Invalid operation.');
				var absValue:Number = Math.abs(value);
				if (absValue >= 1e100)
					throw new RangeError('Out of range.');
				if (absValue <= 1e-100)
					return 0;
				return value; // NumUtil.roundToDigitsPrecision(value, 12);
			}
			var expr:MathExpression = mathParser.parse('0.1/a');
			for (var a:int = 10; a >= 0; --a) {
				mathContext.setVariable('a', new Number(a));
				trace(expr.eval() ? expr.value : (expr.errorName + ': ' + expr.errorMessage));
			}
			mathContext.setVariable('a', new Number(1e200));
			trace(expr.eval() ? expr.value : (expr.errorName + ': ' + expr.errorMessage));
			expr = mathParser.parse('sqr(a)');
			mathContext.setVariable('a', new Number(1e49));
			trace(expr.eval() ? expr.value : (expr.errorName + ': ' + expr.errorMessage));
			mathContext.setVariable('a', new Number(1e50));
			trace(expr.eval() ? expr.value : (expr.errorName + ': ' + expr.errorMessage));
		}
		
		protected function testEvalMTL(source:Array/*MathToken*/):void {
			trace('testEval("' + source.map(MathExpressionConvUtil.mathTokenToString).join(' ') + '")');
			var expr:MathExpression = mathParser.eval(mathTokenArrayProvider.setSource(source));
			if (expr.rpn) {
				var str:String = '';
				for each (var token:MathToken in expr.rpn) {
					if (str)
						str += ' ';
					if (token is ValueToken)
						str += (token as ValueToken).value;
					else if (token is IdentifierToken)
						str += (token as IdentifierToken).name;
					else if (token is OperatorToken)
						str += (token as OperatorToken).operator;
					else
						str += token.typeShortName;
				}
				trace('rpn="' + str + '"');
				trace('rpn=[\n' + expr.rpn.join(',\n') + ']');
			}
			if (expr.hasError) {
				trace(expr.errorName + ': ' + expr.errorMessage);
				if (expr.errorIndex >= 0)
					trace('Error @index=' + expr.errorIndex + ': ' + expr.source.slice(expr.errorIndex).map(MathExpressionConvUtil.mathTokenToString).join(' '));
			}
			else {
				trace('value=' + NumUtil.toGeneral(expr.value, 21));
			}
			trace('');
		}
		
		protected function testEval(source:String, traceRPN:Boolean = true):void {
			source = StrUtil.trim(source).replace(/\s{2,}/g, ' ');
			trace('testEval("' + source + '")');
			mathParser.lexer = stringLexer;
			var expr:MathExpression = mathParser.eval(source);
			
			//stringConverter.convert(source, mathTokenVectorProvider);
			//trace('testEval("' + MathExpressionConvUtil.convertToString(mathTokenVectorProvider, ' ') + '")');
			//mathParser.lexer = mathTokenLexer;
			//var expr:MathExpression = mathParser.eval(mathTokenVectorProvider);
			
			if (traceRPN && expr.rpn) {
				var str:String = '';
				for each (var token:MathToken in expr.rpn) {
					if (str)
						str += ' ';
					str += MathExpressionConvUtil.mathTokenToString(token);
				}
				trace('rpn="' + str + '"');
				trace('rpn=[\n' + expr.rpn.join(',\n') + ']');
			}
			if (expr.hasError) {
				trace(expr.errorName + ': ' + expr.errorMessage);
				if (expr.errorIndex >= 0)
					trace('Error @index=' + expr.errorIndex + ': ' + expr.source.substr(expr.errorIndex));
			}
			else {
				trace('value=' + NumUtil.toGeneral(expr.value, 21));
			}
			trace('');
		}
		
		protected function testMathConverter():void {
			initMathParser();
			testConvert('2^-3*4');
			testConvert('10 + max(1, 2, min(6,4,5), 3)');
			testConvert('2ⁿ√3ⁿ√27');
			testConvert('3 ⁿ√ sqrt 4 - curt 2 ⁿ√ 4');
			testConvert('2²³ - 2^2^3');
			testConvert('sin 3xcos 3x * tan 3x');
			testConvert('sin 1.25e-30 + cos(2.77e+25) - XXX + VVV - mmm // 10.2');
		}
		
		protected function testConvert(source:String):void {
			trace('');
			trace('original ="' + source + '"');
			stringConverter.convert(source, mathTokenVectorProvider);
			if (stringConverter.hasError) {
				for (var j:int = 0; j < stringConverter.numErrors; ++j)
					trace('err' + j, stringConverter.getErrorAt(j), ' "' + stringConverter.source.substr(stringConverter.getErrorAt(j).index, stringConverter.getErrorAt(j).length) + '"');
			}
			for (var i:int = 0; i < mathTokenVectorProvider.length; ++i)
				trace(mathTokenVectorProvider.getTokenAt(i));
			trace('converted="' + mathTokenConverter.convertToString(mathTokenVectorProvider) + '"');
		}
		
		protected function testRound(num:Number, fractionDigits:Number, roundingMode:RoundingMode):String {
			trace('num=' + num.toExponential(20), 'fractionDigits=' + fractionDigits, 'roundingMode=' + roundingMode);
			return 'rnd=' + StrUtil.rtrim(MathExt.roundToDigits(num, fractionDigits, roundingMode).toFixed(20), '0');
		}
		
		protected function assertTrue(value:Boolean):void {
			trace(value);
			trace('');
		}
		
		protected function assertFalse(value:Boolean):void {
			trace(!value);
			trace('');
		}
		
		protected function testMathExt():void {
			//var a:Number = 2e-290;
			//var b:Number = 2.00000000000001e-300;
			//var c:Number = 2e-300;
			//var d:Number = 2e-310;
			//var e:Number = 2e-320;
			//var f:Number = 2e-322;
			//var g:Number = 1e-322;
			//var x:Number = 1.99999999999999e-100;
			//var y:Number = 2e-100;
			//trace(x, y, Math.abs(x - y), Math.max(x, y) * 1e-14, Math.abs(x - y) / Math.max(x, y));
			//trace('a - b =', a - b);
			//trace('a - c =', a - c);
			//trace('a - d =', a - d);
			//trace('b - c =', b - c);
			//trace('b - d =', b - d);
			//trace('c - d =', c - d);
			//trace('d - e =', d - e);
			//trace('d - f =', d - f);
			//trace('d - g =', d - g);
			//trace('e - f =', e - f);
			//trace('e - g =', e - g);
			//trace('f - g =', f - g);
			//var mathFun:MathFun = new MathFun(new MathContext(RoundingMode.HALF_AWAY_FROM_ZERO, AngleUnit.RADIAN, 2.2e-322, 1e-14));
			//var eq:Function = mathFun.nearlyEqual;
			//trace(NumUtil.toExponential(1e294 + 1e280, 20), NumUtil.toExponential(1e294, 20), NumUtil.toExponential((1e294 + 1e280) - 1e+294, 20), NumUtil.toExponential(1e+294 - (1e294 + 1e280), 20));
			//assertTrue(eq(1e294, 1e294 + 1e280));
			//assertTrue(eq(-1e294, -1e294 - 1e280));
			//assertTrue(eq(1e-10 + 1e-24, 1e-10 + 2e-24));
			//assertTrue(eq(1e-10 + 1e-24, 1e-10 + 2e-24));
			//assertTrue(eq(1e-293 + 1e-307, 1e-293));
			//assertTrue(eq(-1e-280 - 1e-294, -1e-280));
			//assertFalse(eq(0.0000000000000001, 0));
			//assertFalse(eq(MathExt.DOUBLE_MIN, MathExt.DOUBLE_MIN * 128));
			//assertTrue(eq(0, 1e-315, 1.1e-14, 1e-315));
			//assertFalse(eq(1e-315, 0, 1.1e-14, 1e-316));
			//return;
			
			trace('DBL_MIN     ' + MathExt.DOUBLE_MIN.toExponential(20));
			trace('DBL_MIN_NOR ' + MathExt.DOUBLE_MIN_NORMAL.toExponential(20));
			trace('DBL_MAX     ' + MathExt.DOUBLE_MAX.toExponential(20));
			trace('DBL_EPSILON ' + MathExt.DOUBLE_EPSILON.toExponential(20));
			trace('DBL_EPS*MNR ' + (MathExt.DOUBLE_MIN_NORMAL * MathExt.DOUBLE_EPSILON).toExponential(20));
			trace('DBL_EPS^2   ' + (MathExt.DOUBLE_EPSILON * MathExt.DOUBLE_EPSILON).toExponential(20));
			trace('');
			
			trace(MathExt.roundToNearest(1.254, 0.01));
			trace(MathExt.roundToNearest(1.255, 0.01));
			trace(MathExt.roundToNearest(1.256, 0.01));
			trace(MathExt.roundToNearest(-1.254, 0.01));
			trace(MathExt.roundToNearest(-1.255, 0.01));
			trace(MathExt.roundToNearest(-1.256, 0.01));
			trace(MathExt.roundToNearest(132.5, 5));
			trace(MathExt.roundToNearest(133, 44.17));
			trace(MathExt.roundToNearest(-132, 44.17));
			trace(MathExt.roundToNearest(-132.5, 5));
			trace(MathExt.roundToNearest(-133, 44.17));
			
			trace(testRound(0.00000123455, 11, RoundingMode.HALF_AWAY_FROM_ZERO));
			trace(testRound(0.00000123455, 10, RoundingMode.HALF_AWAY_FROM_ZERO));
			trace(testRound(0.00000123455, 9, RoundingMode.HALF_AWAY_FROM_ZERO));
			trace(testRound(0.00000123455, 8, RoundingMode.HALF_AWAY_FROM_ZERO));
			trace('');
			trace(testRound(0.00000123455, 11, RoundingMode.TOWARDS_ZERO));
			trace(testRound(0.00000123455, 10, RoundingMode.TOWARDS_ZERO));
			trace(testRound(0.00000123455, 9, RoundingMode.TOWARDS_ZERO));
			trace(testRound(0.00000123455, 8, RoundingMode.TOWARDS_ZERO));
			trace('');
			trace(testRound(1.23454999999999981419, 5, RoundingMode.HALF_ODD));
			trace(testRound(1.23454999999999981419, 10, RoundingMode.HALF_ODD));
			trace(testRound(1.23454999999999981419, 11, RoundingMode.HALF_ODD));
			trace(testRound(1.23454999999999981419, 12, RoundingMode.HALF_ODD));
			trace(testRound(1.23454999999999981419, 13, RoundingMode.HALF_ODD));
			trace(testRound(1.23454999999999981419, 14, RoundingMode.HALF_ODD));
			trace(testRound(1.23454999999999981419, 15, RoundingMode.HALF_ODD));
			trace(testRound(1.23454999999999981419, 16, RoundingMode.HALF_ODD));
			/*var value:Number = 0.000000009955e-8;
			trace(value);
			trace(MathExt.roundToDigits(value, 3));
			trace(MathExt.toExponential(value, 3));
			trace(value.toFixed(18));
			trace(MathExt.toFixed(1, 18));
			trace(MathExt.toFixed(0.000001, 18));
			trace(MathExt.toFixed(value, 19));*/
		}
		
		protected function _rn(value:Number = 0):Number {
			return new Number(value);
		}
		
		protected function _fr(numerator:Number = 0, denominator:Number = 1):Fraction {
			return new Fraction(numerator, denominator);
		}
		
		protected function _cn(re:Number = 0, im:Number = 0):ComplexNumber {
			return new ComplexNumber(re, im);
		}
		
		protected function testMathFun():void {
			var mathLib:MathLib = new MathLib(new MathContext(), new TypeConverter());
			trace(mathLib.arithmeticMath.lcm(_rn(20), _rn(70), _rn(30)));
		}
		
		protected function testNumberFormatter():void {
			//const DOUBLE_MIN:Number = 4.94065645841246544176e-324;
			//trace(DOUBLE_MIN.toString());        // 4.9406564584124654e-324
			//trace(DOUBLE_MIN.toExponential(16)); // 4.9406564584124654e-324
			//trace(DOUBLE_MIN.toExponential(20)); // 4.94065645841246544176e-324
			//const DOUBLE_MIN_NORMAL:Number = 2.22507385850720138309e-308;
			//trace(DOUBLE_MIN_NORMAL.toString());                    // 2.2250738585072014e-308
			//trace(parseFloat(DOUBLE_MIN_NORMAL.toString()));        // 2.2250738585072004e-308
			//trace(DOUBLE_MIN_NORMAL.toExponential(16));             // 2.2250738585072013e-308
			//trace(parseFloat(DOUBLE_MIN_NORMAL.toExponential(16))); // 2.2250738585072e-308
			//trace(DOUBLE_MIN_NORMAL.toExponential(20)); // 2.22507385850720138309e-308
			//const DOUBLE_MAX:Number = 1.79769313486231570814e+308;
			//trace(DOUBLE_MAX.toString());        // 1.79769313486231e+308
			//trace(DOUBLE_MAX.toExponential(14)); // 1.79769313486231e+308
			//trace(DOUBLE_MAX.toExponential(20)); // 1.79769313486231570814e+308
			//const number1:Number = 0.000000123455555555544;
			//trace(number1.toString());        // 1.23455555555544e-7
			//trace(number1.toExponential(14)); // 1.23455555555543e-7
			//trace(number1.toExponential(20)); // 1.23455555555543993039e-7
			//const number2:Number = 0.000000123455555555567;
			//trace(number2.toString());        // 1.23455555555567e-7
			//trace(number2.toExponential(14)); // 1.23455555555566e-7
			//trace(number2.toExponential(20)); // 1.23455555555566995278e-7
			
			//var nf1:NumberFormatter = NumberFormatter.createNormalNumberFormatter(4, 1);
			//trace(nf1.format(0.00001));
			//trace(nf1.format(0.0001));
			//trace(nf1.format(0.001));
			//trace(nf1.format(0.01));
			//trace(nf1.format(0.1));
			//trace(nf1.format(1));
			//trace(nf1.format(0.999));
			//trace(nf1.format(0.9995));
			
			//var nf1:NumberFormatter = NumberFormatter.createNormalNumberFormatter(4);
			//nf1.leadingZero = false;
			//trace(nf1.format(0));
			//trace(nf1.format(0.000012));
			//trace(nf1.format(0.00012));
			//trace(nf1.format(0.0012));
			//trace(nf1.format(0.012));
			//trace(nf1.format(0.12));
			//trace(nf1.format(1.2));
			//trace(nf1.format(12.3));
			//trace(nf1.format(123.4));
			//trace(nf1.format(1234.5));
			//trace('');
			//trace(nf1.format(9999.499999999));
			//trace(nf1.format(9999.5));
			//trace(nf1.format(10000));
			//trace(nf1.format(10005));
			//trace('');
			//nf1.maxDigits = 10;
			//nf1.engineeringReformatter = NumberFormatter.createEngineeringNumberFormatter(nf1.maxDigits);
			//var x:Number = 123.456789357;
			//trace(nf1.format(x));
			//trace(nf1.reformatEngineering(1));
			//trace(nf1.reformatEngineering(1));
			//trace(nf1.reformatEngineering(1));
			//trace(nf1.reformatEngineering(1));
			//trace(nf1.reformatEngineering(-1));
			//trace(nf1.reformatEngineering(-1));
			//trace(nf1.reformatEngineering(-1));
			//trace(nf1.reformatEngineering(-1));
			//trace(nf1.reformatEngineering(-1));
			//trace(nf1.reformatEngineering(-1));
			//x = 0.000357;
			//trace(nf1.format(x));
			//trace(nf1.reformatEngineering(1));
			//trace(nf1.reformatEngineering(1));
			//trace(nf1.reformatEngineering(1));
			//trace(nf1.reformatEngineering(1));
			//trace(nf1.reformatEngineering(-1));
			//trace(nf1.reformatEngineering(-1));
			//trace(nf1.reformatEngineering(-1));
			//trace(nf1.reformatEngineering(-1));
			//trace(nf1.reformatEngineering(-1));
			
			//var nf1:NumberFormatter = NumberFormatter.createNormalNumberFormatter(10, 1e-2);
			//trace(nf1.format(123456789123));
			//trace(nf1.format(12345678915));
			//trace(nf1.format(1234567891));
			//trace(nf1.format(123456789));
			//trace(nf1.format(0.123456));
			//trace(nf1.format(0.0123456));
			//trace(nf1.format(0.000123456));
			//trace(nf1.format(0.00000123455));
			//trace(nf1.format(0.00000123456));
			//trace(nf1.format(0.000000000123456));
			//trace(nf1.format(0));
			//trace(nf1.format(-123456789123));
			//trace(nf1.format(-12345678915));
			//trace(nf1.format(-1234567891));
			//trace(nf1.format(-123456789));
			//trace(nf1.format(-0.123456));
			//trace(nf1.format(-0.0123456));
			//trace(nf1.format(-0.000123456));
			//trace(nf1.format(-0.00000123455));
			//trace(nf1.format(-0.00000123456));
			//trace(nf1.format(-0.000000000123456));
			//trace(nf1.format(-0));
			
			//trace('');
			//var nf2:NumberFormatter = NumberFormatter.createNormalNumberFormatter(10);
			//trace(nf2.format(0));
			//trace(nf2.format(123456789123));
			//trace(nf2.format(12345678912));
			//trace(nf2.format(1234567891));
			//trace(nf2.format(123456789));
			//trace(nf2.format(0.123456));
			//trace(nf2.format(0.0123456));
			//trace(nf2.format(0.000123456));
			//trace(nf2.format(0.00000123456));
			//trace(nf2.format(0.000000000123456)); // FAIL! rounding error
			//trace(nf2.format(0.00000000098765432198765));
			
			//trace('');
			//var nf3:NumberFormatter = NumberFormatter.createFixedNumberFormatter(5, 3);
			//trace(nf3.format(0));
			//trace(nf3.format(99999));
			//trace(nf3.format(100000));
			//trace(nf3.format(100005));
			//nf3.leadingZero = false;
			//trace(nf3.format(0.0123456789));
			//trace(nf3.format(0.123456789));
			//nf3.leadingZero = true;
			//trace(nf3.format(0.0123456789));
			//trace(nf3.format(0.123456789));
			//trace(nf3.format(1.23456789));
			//trace(nf3.format(12.3456789));
			//trace(nf3.format(123.456789));
			//trace(nf3.format(1234.56789));
			//trace('');
			//nf3.fractionDigits = 5;
			//nf3.leadingZero = false;
			//trace(nf3.format(0.0123456789));
			//trace(nf3.format(0.123456789));
			//nf3.leadingZero = true;
			//trace(nf3.format(0.0123456789));
			//trace(nf3.format(0.123456789));
			//trace(nf3.format(1.23456789));
			//trace(nf3.format(12.3456789));
			//trace(nf3.format(123.456789));
			//trace(nf3.format(1234.56789));
			//trace('');
			//nf3.leadingZero = false;
			//trace(nf3.format(0.09999999));
			//trace(nf3.format(0.99999999));
			//trace(nf3.format(9.99999999));
			//trace(nf3.format(99.9999999));
			//trace(nf3.format(999.999999));
			//nf3.leadingZero = false;
			//trace(nf3.format(0.09999999));
			//trace(nf3.format(0.99999999));
			//trace(nf3.format(9.99999999));
			//trace(nf3.format(99.9999999));
			//trace(nf3.format(999.999999));
			//trace(nf3.format(999.999999));
			
			//trace('');
			var nf3:NumberFormatter = NumberFormatter.createFixedNumberFormatter(10, 6);
			trace(nf3.format(0));
			trace(nf3.format(123456789123));
			trace(nf3.format(12345678912));
			trace(nf3.format(9999999999.99999));
			trace(nf3.format(9999999999.49999));
			trace(nf3.format(1234567891));
			trace(nf3.format(123456789));
			trace(nf3.format(0.123456));
			trace(nf3.format(0.0123456));
			trace(nf3.format(0.000123456));
			trace(nf3.format(0.00000123456));
			nf3.engineeringReformatter = NumberFormatter.createEngineeringNumberFormatter(nf3.maxDigits);
			nf3.engineeringReformatter.trailingZeros = true;
			trace('');
			trace(nf3.format(0));
			trace(nf3.format(-0));
			trace('---');
			//trace(nf3.format(0.0000000145456));
			trace(nf3.format(0.9995));
			trace('');
			trace(nf3.reformatEngineering(1));
			trace(nf3.reformatEngineering(1));
			trace(nf3.reformatEngineering(1));
			trace(nf3.reformatEngineering(-1));
			trace(nf3.reformatEngineering(-1));
			trace(nf3.reformatEngineering(-1));
			trace(nf3.reformatEngineering(-1));
			trace(nf3.reformatEngineering(-1));
			trace(nf3.reformatEngineering(-1));
			trace('---');
			trace(nf3.format(500.0));
			trace('');
			trace(nf3.reformatEngineering(-1));
			trace(nf3.reformatEngineering(-1));
			trace(nf3.reformatEngineering(-1));
			trace(nf3.reformatEngineering(-1));
			trace(nf3.reformatEngineering(-1));
			//trace('');
			//trace(nf3.format(0.000000000098765432198765));
			//trace(nf3.reformatEngineering(1));
			//trace(nf3.reformatEngineering(1));
			//trace(nf3.reformatEngineering(-1));
			//trace(nf3.reformatEngineering(-1));
			
			//var nf4:NumberFormatter = NumberFormatter.createScientificNumberFormatter(10);
			//trace(nf4.format(0));
			//trace(NumUtil.toExponential(0));
			//testNF4(nf4, 0.0000012444444445); // Double rounding (to precision=16 first) necessary.
			//testNF4(nf4, 0.0000012344444445);
			//nf4.maxDigits = 14;
			//nf4.fractionDigits = nf4.maxDigits - 1;
			//testNF4(nf4, 1.99999894444445); // Double rounding (to precision=16 first) necessary.
			//testNF4(nf4, 1.99999884444445);
			//trace(nf4.format(-125.3005e20));
			//trace(nf4.format(5000));
			//nf4.trailingZeros = false;
			//trace(nf4.format(-125.3005e20));
			//trace(nf4.format(5000));
			//nf4.trailingZeros = true;
			//trace('');
			//trace(nf4.format(125.300555555));
			//nf4.maxDigits = 3;
			//nf4.engineeringReformatter = NumberFormatter.createEngineeringNumberFormatter(10);
			//trace(nf4.format(3.576981));
			//trace(nf4.reformatEngineering(1));
			//trace(nf4.reformatEngineering(1));
			//trace(nf4.reformatEngineering(1));
			//trace(nf4.reformatEngineering(1));
			//trace(nf4.reformatEngineering(1));
			//trace(nf4.reformatEngineering(-4));
			//trace(nf4.reformatEngineering(-1));
			//trace(nf4.reformatEngineering(-1));
			//trace(nf4.reformatEngineering(-1));
			
			//var nf4:NumberFormatter = NumberFormatter.createScientificNumberFormatter(17);
			//trace(nf4.format(5.7000000000249956e+300));
			//trace(nf4.format(5.7000000000249944e+300));
			//nf4.maxDigits = 12;
			//trace(nf4.format(5.7000000000249956e+300));
			//trace(nf4.format(5.7000000000249944e+300));
			//trace(nf4.format(x), NumUtil.toExponential(x, 16));
			//trace(nf4.format(9.0499999999999961e+301));
			//trace(nf4.format(9.0499999999999942e+301));
			//trace('');
			//nf4.maxDigits = 2;
			//trace(nf4.format(x), NumUtil.toExponential(x, 16));
			//trace(nf4.format(9.0499999999999961e+301));
			//trace(nf4.format(9.0499999999999942e+301));
			// For 6.5e300 ~ 6.49999999999999944907e+300, rounding to 16 digits first results in 6.499999999999999e+300, subsequent rounding to one digit results in 6 (instead of the correct 7).
			//var prec:int = 20;
			//var x:Number = 6.5e300;
			//var xp:Number = NumUtil.prior(x);
			//var xn:Number = NumUtil.next(x);
			//trace(nf4.format(xp), NumUtil.toExponential(xp, prec));
			//trace(nf4.format(x), NumUtil.toExponential(x, prec));
			//trace(nf4.format(xn), NumUtil.toExponential(xn, prec));
			//trace('');
			//nf4.maxDigits = 1;
			//trace(nf4.format(xp), NumUtil.toExponential(xp, prec));
			//trace(nf4.format(x), NumUtil.toExponential(x, prec));
			//trace(nf4.format(xn), NumUtil.toExponential(xn, prec));
			//x = 1.5e54;
			//xp = NumUtil.prior(x);
			//xn = NumUtil.next(x);
			//nf4.maxDigits = 17;
			//trace('');
			//trace(nf4.format(xp), NumUtil.toExponential(xp, prec));
			//trace(nf4.format(x), NumUtil.toExponential(x, prec));
			//trace(nf4.format(xn), NumUtil.toExponential(xn, prec));
			//trace('');
			//nf4.maxDigits = 1;
			//trace(nf4.format(xp), NumUtil.toExponential(xp, prec));
			//trace(nf4.format(x), NumUtil.toExponential(x, prec));
			//trace(nf4.format(xn), NumUtil.toExponential(xn, prec));
			//x = 1.00000000000005e1;
			//xp = NumUtil.prior(x);
			//xn = NumUtil.next(x);
			//nf4.maxDigits = 17;
			//trace('');
			//trace(nf4.format(xp), NumUtil.toExponential(xp, prec));
			//trace(nf4.format(x), NumUtil.toExponential(x, prec));
			//trace(nf4.format(xn), NumUtil.toExponential(xn, prec));
			//trace('');
			//nf4.maxDigits = 14;
			//trace(nf4.format(xp), NumUtil.toExponential(xp, prec));
			//trace(nf4.format(x), NumUtil.toExponential(x, prec));
			//trace(nf4.format(xn), NumUtil.toExponential(xn, prec));
			
			//var nf5:NumberFormatter = NumberFormatter.createEngineeringNumberFormatter(6);
			//trace(nf5.format(0));
			//nf5.leadingZero = false;
			//nf5.groupDigits = true;
			//trace(nf5.format(0));
			//var x:Number = 0.0135795468;
			//trace(nf5.format(x), 'o=' + nf5.engineeringOffset++);
			//trace(nf5.format(x), 'o=' + nf5.engineeringOffset++);
			//trace(nf5.format(x), 'o=' + nf5.engineeringOffset++);
			//nf5.engineeringOffset = -1;
			//trace(nf5.format(x), 'o=' + nf5.engineeringOffset--);
			//trace(nf5.format(x), 'o=' + nf5.engineeringOffset--);
			//trace(nf5.format(x), 'o=' + nf5.engineeringOffset--);
			//trace('');
			//nf5.engineeringOffset = 0;
			//trace(nf5.format(x));
			//trace(nf5.reformatEngineering(1));
			//trace(nf5.reformatEngineering(1));
			//trace(nf5.reformatEngineering(1));
			//trace(nf5.reformatEngineering(1));
			//trace('-');
			//trace(nf5.reformatEngineering(-1));
			//trace(nf5.reformatEngineering(-1));
			//trace(nf5.reformatEngineering(-1));
			//trace(nf5.reformatEngineering(-1));
			//trace('');
			//x = MathExt.DOUBLE_MAX;
			//trace(nf5.format(x));
			//trace(nf5.reformatEngineering(1));
			//trace(nf5.reformatEngineering(1));
			//trace(nf5.reformatEngineering(1));
			//trace(nf5.reformatEngineering(-1));
			//trace(nf5.reformatEngineering(-1));
			//trace(nf5.reformatEngineering(-1));
			//trace(nf5.reformatEngineering(-1));
			//trace(nf5.reformatEngineering(-1));
			//trace('');
			//nf5.engineeringOffset = 1;
			//nf5.maxDigits = 10;
			//nf5.leadingZero = true;
			//x = 12.3456789357;
			//trace(nf5.format(x));
			//trace(nf5.reformatEngineering(1));
			//trace(nf5.reformatEngineering(1));
			//trace(nf5.reformatEngineering(-1));
			//trace(nf5.reformatEngineering(-1));
			//trace(nf5.reformatEngineering(-1));
			//trace(nf5.reformatEngineering(-1));
			//trace(nf5.reformatEngineering(-1));
			//trace(nf5.reformatEngineering(-1));
			//trace(nf5.format(x));
			//trace(nf5.reformatEngineering(-1));
		}
		
		protected function testNF4(nf4:NumberFormatter, num:Number):void {
			StrUtil.printf('%0$<21.16e precision=17\n%0$<21.15e precision=16\n%0$<21.14e precision=15\n%1$21s precision=%2$d (nf)\n', num, nf4.format(num), nf4.fractionDigits + 1);
		}
	}
}
