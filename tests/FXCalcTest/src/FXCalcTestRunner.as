package{
	import asunit.textui.TestRunner;
	import flash.display.Sprite;
	import flash.events.Event;
	
	/**
	 * FXCalc test suite runner.
	 *
	 * @author Gene Pavlovsky
	 */
	public class FXCalcTestRunner extends Sprite {
		public function FXCalcTestRunner() {
			if (stage)
				init();
			else
				addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void {
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			var testRunner:TestRunner = new TestRunner();
			stage.addChild(testRunner);
			testRunner.start(FXCalcTestSuite, null, TestRunner.SHOW_TRACE);
		}
	}
}
