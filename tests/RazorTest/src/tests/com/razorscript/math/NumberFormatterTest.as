package tests.com.razorscript.math {
	import asunit.framework.TestCase;
	import com.razorscript.math.NumberFormatter;
	
	/**
	 * NumberFormatter test case.
	 *
	 * @author Gene Pavlovsky
	 */
	public class NumberFormatterTest extends TestCase {
		public function NumberFormatterTest(significantDigits:uint, testMethod:String=null) {
			super(testMethod);
			this.significantDigits = significantDigits;
		}
		
		override protected function setUp():void {
			nfNorm1 = NumberFormatter.createNormalNumberFormatter(significantDigits, 1e-2);
			nfNorm2 = NumberFormatter.createNormalNumberFormatter(significantDigits);
			nfFix = NumberFormatter.createFixedNumberFormatter(significantDigits, 3);
			nfSci = NumberFormatter.createScientificNumberFormatter(significantDigits);
			nfEng = NumberFormatter.createEngineeringNumberFormatter(significantDigits);
		}
		
		override protected function tearDown():void {
			nfNorm1 = nfNorm2 = nfFix = nfSci = nfEng = null;
		}
		
		public function testInstantiated():void {
			assertTrue(nfNorm1 is NumberFormatter);
			assertTrue(nfNorm2 is NumberFormatter);
			assertTrue(nfFix is NumberFormatter);
			assertTrue(nfSci is NumberFormatter);
			assertTrue(nfEng is NumberFormatter);
		}
		
		public function testNormal1():void {
			/*assertEquals(NumberFormatter.eval('1+2^3^2').value, 513);
			trace(nf.format(123456789123));
			trace(nf.format(12345678912));
			trace(nf.format(1234567891));
			trace(nf.format(123456789));
			trace(nf.format(0.123456));
			trace(nf.format(0.0123456));
			trace(nf.format(0.00123456));
			trace(nf.format(0.000123456));
			trace(nf.format(0.00000123456));
			trace(nf.format(0.000000000123456));
			trace(nf.format(0));*/
		}
		
		public function testNormal2():void {
		}
		
		public function testFixed():void {
		}
		
		public function testScientific():void {
		}
		
		public function testEngineering():void {
		}
		
		protected var significantDigits:uint;
		
		protected var nfNorm1:NumberFormatter;
		protected var nfNorm2:NumberFormatter;
		protected var nfFix:NumberFormatter;
		protected var nfSci:NumberFormatter;
		protected var nfEng:NumberFormatter;
	}
}
