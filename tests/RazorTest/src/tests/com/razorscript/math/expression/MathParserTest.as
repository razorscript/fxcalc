package tests.com.razorscript.math.expression {
	import asunit.framework.TestCase;
	import com.razorscript.math.MathContext;
	import com.razorscript.math.MathExt;
	import com.razorscript.math.errors.ArithmeticError;
	import com.razorscript.math.expression.MathFun;
	import com.razorscript.math.expression.MathOperator;
	import com.razorscript.math.expression.MathParser;
	import com.razorscript.math.expression.lexers.StringLexer;
	import com.razorscript.math.expression.tokens.MathTokenFactory;
	import com.razorscript.math.functions.MathLib;
	import com.razorscript.math.functions.polymorphism.TypeConverter;
	import com.razorscript.math.objects.conversions.TypeConversionFun;
	
	/**
	 * MathParser test case.
	 *
	 * @author Gene Pavlovsky
	 */
	public class MathParserTest extends TestCase {
		public function MathParserTest(testMethod:String=null) {
			super(testMethod);
		}
		
		protected const CONST:Object = {PI: Math.PI, E: Math.E};
		protected const VAR:Object = {a: Math.SQRT2, b: Math.SQRT1_2, c: Math.LN10, d: Math.LN2, e: Math.LOG10E, f: Math.LOG2E, x: 65536, y: .357, m: 125};
		
		override protected function setUp():void {
			mathContext = new MathContext();
			mathContext.constants = CONST;
			mathContext.variables = VAR;
			
			typeConverter = TypeConversionFun.extend(new TypeConverter());
			mathLib = new MathLib(mathContext, typeConverter);
			mathFun = new MathFun(mathContext, mathLib);
			mathOperator = new MathOperator(mathFun);
			valueTransform = function (value:Number):Number {
				if (MathExt.isNaN(value))
					throw new ArithmeticError('Invalid operation.');
				var absValue:Number = Math.abs(value);
				if ((absValue >= 1e100) || (absValue && (absValue <= 1e-100)))
					throw new RangeError('Value (' + value + ') is out of range.');
				return value;
			}
			mathParser = new MathParser(mathContext, mathFun, mathOperator, new StringLexer(), valueTransform, new MathTokenFactory());
		}
		
		override protected function tearDown():void {
			mathContext = null;
			typeConverter = null;
			mathLib = null;
			mathFun = null;
			mathOperator = null;
			valueTransform = null;
			mathParser = null;
		}
		
		public function testInstantiated():void {
			assertTrue(mathParser is MathParser);
		}
		
		public function testEval():void {
			assertTrue(mathParser.eval('1/0').hasError);
			assertTrue(mathParser.eval('(12+34').hasError);
			assertEquals(mathParser.eval('1+2^3^2').value, 513);
		}
		
		protected var mathContext:MathContext;
		protected var typeConverter:TypeConverter;
		protected var mathLib:MathLib;
		protected var mathFun:MathFun;
		protected var mathOperator:MathOperator;
		protected var valueTransform:Function;
		protected var mathParser:MathParser;
	}
}
