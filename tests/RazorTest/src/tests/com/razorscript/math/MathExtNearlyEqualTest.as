package tests.com.razorscript.math {
	import asunit.framework.TestCase;
	import com.razorscript.math.AngleUnit;
	import com.razorscript.math.expression.MathFun;
	import com.razorscript.math.MathContext;
	import com.razorscript.math.MathExt;
	import com.razorscript.math.RoundingMode;
	
	/**
	 * MathExt test case.
	 *
	 * @author Gene Pavlovsky
	 */
	public class MathExtNearlyEqualTest extends TestCase {
		public function MathExtNearlyEqualTest(testMethod:String=null) {
			super(testMethod);
		}
		
		override protected function setUp():void {
		}
		
		override protected function tearDown():void {
		}
		
		public function testInstantiated():void {
		}
		
		protected const ABSOLUTE_ERROR:Number = 2.2e-322;
		protected const RELATIVE_ERROR:Number = 5e-14;
		
		protected const DBL_MIN:Number = MathExt.DOUBLE_MIN; // 4.94065645841246544176e-324
		protected const DBL_MIN_NORMAL:Number = MathExt.DOUBLE_MIN_NORMAL; // 2.22507385850720138309e-308
		protected const DBL_MAX:Number = MathExt.DOUBLE_MAX; // 1.79769313486231570814e+308
		
		/** Really large numbers - generally not problematic. */
		public function testReallyBig():void {
			assertTrue(eq(1e294, 1e294 + 1e280));
			assertTrue(eq(1e294 + 1e280, 1e294));
			assertTrue(eq(1e194, 1e194 + 1e180));
			assertTrue(eq(1e194 + 1e180, 1e194));
			assertTrue(eq(1e94, 1e94 + 1e80));
			assertTrue(eq(1e94 + 1e80, 1e94));
			assertFalse(eq(1e293, 1e293 + 1e280));
			assertFalse(eq(1e293 + 1e280, 1e293));
			assertFalse(eq(1e193, 1e193 + 1e180));
			assertFalse(eq(1e193 + 1e180, 1e193));
			assertFalse(eq(1e93, 1e93 + 1e80));
			assertFalse(eq(1e93 + 1e80, 1e93));
		}
		
		/** Really large negative numbers. */
		public function testReallyBigNeg():void {
			assertTrue(eq(-1e294, -1e294 - 1e280));
			assertTrue(eq(-1e294 - 1e280, -1e294));
			assertTrue(eq(-1e194, -1e194 - 1e180));
			assertTrue(eq(-1e194 - 1e180, -1e194));
			assertTrue(eq(-1e94, -1e94 - 1e80));
			assertTrue(eq(-1e94 - 1e80, -1e94));
			assertFalse(eq(-1e293, -1e293 - 1e280));
			assertFalse(eq(-1e293 - 1e280, -1e293));
			assertFalse(eq(-1e193, -1e193 - 1e180));
			assertFalse(eq(-1e193 - 1e180, -1e193));
			assertFalse(eq(-1e93, -1e93 - 1e80));
			assertFalse(eq(-1e93 - 1e80, -1e93));
		}
		
		/** Regular large numbers - generally not problematic. */
		public function testBig():void {
			assertTrue(eq(1e14, 1e14 + 1));
			assertTrue(eq(1e14 + 1, 1e14));
			assertFalse(eq(1e13, 1e13 + 1));
			assertFalse(eq(1e13 + 1, 1e13));
		}
		
		/** Large negative numbers. */
		public function testBigNeg():void {
			assertTrue(eq(-1e14, -1e14 - 1));
			assertTrue(eq(-1e14 - 1, -1e14));
			assertFalse(eq(-1e13, 1e13 - 1));
			assertFalse(eq(-1e13 - 1, -1e13));
		}
		
		/** Numbers around 1. */
		public function testMid():void {
			assertTrue(eq(1 + 1e-14, 1 + 2e-14));
			assertTrue(eq(1 + 2e-14, 1 + 1e-14));
			assertFalse(eq(1 + 1e-13, 1 + 2e-13));
			assertFalse(eq(1 + 2e-13, 1 + 1e-13));
		}
		
		/** Numbers around -1. */
		public function testMidNeg():void {
			assertTrue(eq(-1 - 1e-14, -1 - 2e-14));
			assertTrue(eq(-1 - 2e-14, -1 - 1e-14));
			assertFalse(eq(-1 - 1e-13, -1 - 2e-13));
			assertFalse(eq(-1 - 2e-13, -1 - 1e-13));
		}
		
		/** Numbers between 1 and 0. */
		public function testSmall():void {
			assertTrue(eq(1e-10 + 1e-24, 1e-10 + 2e-24));
			assertTrue(eq(1e-10 + 2e-24, 1e-10 + 1e-24));
			assertFalse(eq(1e-11 + 1e-24, 1e-11 + 2e-24));
			assertFalse(eq(1e-11 + 2e-24, 1e-11 + 1e-24));
		}
		
		/** Numbers between -1 and 0. */
		public function testSmallNeg():void {
			assertTrue(eq(1e-10 + 1e-24, 1e-10 + 2e-24));
			assertTrue(eq(1e-10 + 2e-24, 1e-10 + 1e-24));
			assertFalse(eq(1e-11 + 1e-24, 1e-11 + 2e-24));
			assertFalse(eq(1e-11 + 2e-24, 1e-11 + 1e-24));
		}
		
		/** Really small normal numbers between 1 and 0. */
		public function testReallySmall():void {
			assertTrue(eq(1e-293 + 1e-307, 1e-293));
			assertTrue(eq(1e-293, 1e-293 + 1e-307));
			assertTrue(eq(1e-180, 1e-180 + 1e-194));
			assertTrue(eq(1e-180 + 1e-194, 1e-180));
			assertTrue(eq(1e-80, 1e-80 + 1e-94));
			assertTrue(eq(1e-80 + 1e-94, 1e-80));
			assertFalse(eq(1e-293 + 1e-306, 1e-293));
			assertFalse(eq(1e-293, 1e-293 + 1e-306));
			assertFalse(eq(1e-180, 1e-180 + 1e-193));
			assertFalse(eq(1e-180 + 1e-193, 1e-180));
			assertFalse(eq(1e-80, 1e-80 + 1e-93));
			assertFalse(eq(1e-80 + 1e-93, 1e-80));
		}
		
		/** Really small negative normal numbers between -1 and 0. */
		public function testReallySmallNeg():void {
			assertTrue(eq(-1e-280 - 1e-294, -1e-280));
			assertTrue(eq(-1e-280, -1e-280 - 1e-294));
			assertTrue(eq(-1e-180, -1e-180 - 1e-194));
			assertTrue(eq(-1e-180 - 1e-194, -1e-180));
			assertTrue(eq(-1e-80, -1e-80 - 1e-94));
			assertTrue(eq(-1e-80 - 1e-94, -1e-80));
			assertFalse(eq(-1e-280 - 1e-293, -1e-280));
			assertFalse(eq(-1e-280, -1e-280 - 1e-293));
			assertFalse(eq(-1e-180, -1e-180 - 1e-193));
			assertFalse(eq(-1e-180 - 1e-193, -1e-180));
			assertFalse(eq(-1e-80, -1e-80 - 1e-93));
			assertFalse(eq(-1e-80 - 1e-93, -1e-80));
		}
		
		/** Comparisons involving zero. */
		public function testZero():void {
			assertTrue(eq(0, 0));
			assertTrue(eq(0, -0));
			assertTrue(eq(-0, -0));
			assertFalse(eq(0.0000000000000001, 0));
			assertFalse(eq(0, 0.0000000000000001));
			assertFalse(eq(-0.0000000000000001, 0));
			assertFalse(eq(0, -0.0000000000000001));
			
			assertTrue(eq(0, 1e-315, 1e-315, 1.1e-14));
			assertTrue(eq(1e-315, 0, 1e-315, 1.1e-14));
			assertFalse(eq(1e-315, 0, 1e-316, 1.1e-14));
			assertFalse(eq(0, 1e-315, 1e-316, 1.1e-14));
			
			assertTrue(eq(0, -1e-315, 1e-315, 1.1e-14));
			assertTrue(eq(-1e-315, 0, 1e-315, 1.1e-14));
			assertFalse(eq(-1e-315, 0, 1e-316, 1.1e-14));
			assertFalse(eq(0, -1e-315, 1e-316, 1.1e-14));
		}
		
		/** Comparisons involving extreme values (overflow potential). */
		public function testExtremeMax():void {
			assertTrue(eq(DBL_MAX, DBL_MAX));
			assertTrue(eq(DBL_MAX, DBL_MAX - 1e294));
			assertTrue(eq(DBL_MAX - 1e294, DBL_MAX));
			assertTrue(eq(-DBL_MAX, -DBL_MAX));
			assertTrue(eq(-DBL_MAX, -DBL_MAX + 1e294));
			assertTrue(eq(-DBL_MAX + 1e294, -DBL_MAX));
			assertFalse(eq(DBL_MAX, -DBL_MAX));
			assertFalse(eq(-DBL_MAX, DBL_MAX));
			assertFalse(eq(DBL_MAX, DBL_MAX / 2));
			assertFalse(eq(DBL_MAX, -DBL_MAX / 2));
			assertFalse(eq(-DBL_MAX, DBL_MAX / 2));
		}
		
		/** Comparisons involving Infinities. */
		public function testInfinities():void {
			assertTrue(eq(Infinity, Infinity));
			assertTrue(eq(-Infinity, -Infinity));
			assertFalse(eq(-Infinity, Infinity));
			assertFalse(eq(Infinity, DBL_MAX));
			assertFalse(eq(-Infinity, -DBL_MAX));
		}
		
		/** Comparisons involving NaN values. */
		public function testNaN():void {
			assertFalse(eq(NaN, NaN));
			assertFalse(eq(NaN, 0));
			assertFalse(eq(-0, NaN));
			assertFalse(eq(NaN, -0));
			assertFalse(eq(0, NaN));
			assertFalse(eq(NaN, Infinity));
			assertFalse(eq(Infinity, NaN));
			assertFalse(eq(NaN, -Infinity));
			assertFalse(eq(-Infinity, NaN));
			assertFalse(eq(NaN, DBL_MAX));
			assertFalse(eq(DBL_MAX, NaN));
			assertFalse(eq(NaN, -DBL_MAX));
			assertFalse(eq(-DBL_MAX, NaN));
			assertFalse(eq(NaN, DBL_MIN));
			assertFalse(eq(DBL_MIN, NaN));
			assertFalse(eq(NaN, -DBL_MIN));
			assertFalse(eq(-DBL_MIN, NaN));
		}
		
		/** Comparisons of numbers on opposite sides of 0. */
		public function testOpposite():void {
			assertFalse(eq(1, -1));
			assertFalse(eq(-1, 1));
			assertFalse(eq(1 + 1e-15, -1));
			assertFalse(eq(-1, 1 + 1e-15));
			assertFalse(eq(-1 - 1e-15, 1));
			assertFalse(eq(1, -1 - 1e-15));
			assertFalse(eq(-1 - 1e-15, 1 + 1e-15));
			assertFalse(eq(1 + 1e-15, -1 - 1e-15));
			assertTrue(eq(10 * DBL_MIN, -10 * DBL_MIN));
			assertTrue(eq(-10 * DBL_MIN, 10 * DBL_MIN));
			assertFalse(eq(1e5 * DBL_MIN, -1e5 * DBL_MIN));
			assertFalse(eq(-1e5 * DBL_MIN, 1e5 * DBL_MIN));
			assertFalse(eq(1e14 * DBL_MIN, -1e14 * DBL_MIN));
			assertFalse(eq(-1e14 * DBL_MIN, 1e14 * DBL_MIN));
		}
		
		/** The really tricky part - comparisons of numbers very close to zero. */
		public function testULP():void {
			assertTrue(eq(DBL_MIN, DBL_MIN));
			assertTrue(eq(DBL_MIN, -DBL_MIN));
			assertTrue(eq(-DBL_MIN, DBL_MIN));
			assertTrue(eq(DBL_MIN, 0));
			assertTrue(eq(0, DBL_MIN));
			assertTrue(eq(-DBL_MIN, 0));
			assertTrue(eq(0, -DBL_MIN));
			assertTrue(eq(DBL_MIN, DBL_MIN * 32));
			assertTrue(eq(DBL_MIN * 32, DBL_MIN));
			assertFalse(eq(DBL_MIN, DBL_MIN * 128));
			assertFalse(eq(DBL_MIN * 128, DBL_MIN));
			assertTrue(eq(-DBL_MIN, -DBL_MIN * 32));
			assertTrue(eq(-DBL_MIN * 32, -DBL_MIN));
			assertFalse(eq(-DBL_MIN, -DBL_MIN * 128));
			assertFalse(eq(-DBL_MIN * 128, -DBL_MIN));
			
			assertFalse(eq(DBL_MIN, 1e-300));
			assertFalse(eq(1e-300, DBL_MIN));
			assertFalse(eq(DBL_MIN, 1e-300));
			assertFalse(eq(-DBL_MIN, 1e-300));
		}
		
		protected function eq(x:Number, y:Number, absoluteError:Number = -1, relativeError:Number = -1):Boolean {
			return MathExt.nearlyEqual(x, y, (absoluteError >= 0) ? absoluteError : ABSOLUTE_ERROR, (relativeError >= 0) ? relativeError : RELATIVE_ERROR);
		}
	}
}
