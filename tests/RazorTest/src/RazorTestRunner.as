package{
	import asunit.textui.TestRunner;
	import flash.display.Sprite;
	import flash.events.Event;
	
	/**
	 * Razor test suite runner.
	 *
	 * @author Gene Pavlovsky
	 */
	public class RazorTestRunner extends Sprite {
		public function RazorTestRunner() {
			if (stage)
				init();
			else
				addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void {
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			var testRunner:TestRunner = new TestRunner();
			stage.addChild(testRunner);
			testRunner.start(RazorTestSuite, null, TestRunner.SHOW_TRACE);
		}
	}
}
