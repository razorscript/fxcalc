package {
	import asunit.framework.TestSuite;
	import tests.com.razorscript.math.expression.MathParserTest;
	import tests.com.razorscript.math.MathExtTest;
	import tests.com.razorscript.math.MathExtNearlyEqualTest;
	import tests.com.razorscript.math.NumberFormatterTest;
	import tests.com.razorscript.utils.StringUtilTest;
	
	/**
	 * Razor library test suite.
	 *
	 * @author Gene Pavlovsky
	 */
	public class RazorTestSuite extends TestSuite {
		public function RazorTestSuite() {
			super();
			
			/** com.razorscript.math */
			addTest(new MathExtTest());
			addTest(new MathExtNearlyEqualTest());
			addTest(new NumberFormatterTest(10));
			addTest(new NumberFormatterTest(12));
			addTest(new NumberFormatterTest(14));
			
			/** com.razorscript.math.expression */
			addTest(new MathParserTest());
			
			/** com.razorscript.utils */
			addTest(new StringUtilTest());
		}
	}
}
