package bench.com.razorscript.math {
	import com.razorscript.math.MathContext;
	import com.razorscript.math.MathExt;
	import com.razorscript.math.NumberFormatter;
	import com.razorscript.math.errors.ArithmeticError;
	import com.razorscript.math.expression.MathExpression;
	import com.razorscript.math.expression.MathFun;
	import com.razorscript.math.expression.MathOperator;
	import com.razorscript.math.expression.MathParser;
	import com.razorscript.math.expression.lexers.StringLexer;
	import com.razorscript.math.expression.tokens.PooledMathTokenFactory;
	import com.razorscript.math.functions.MathLib;
	import com.razorscript.math.functions.polymorphism.TypeConverter;
	import com.razorscript.math.objects.ComplexNumber;
	import com.razorscript.math.objects.Fraction;
	import com.razorscript.math.objects.conversions.TypeConversionFun;
	import com.razorscript.utils.ClassUtil;
	import com.razorscript.utils.NumUtil;
	import com.razorscript.utils.VecUtil;
	
	/**
	 * NumberFormatter benchmark.
	 *
	 * @author Gene Pavlovsky
	 */
	public class NumberFormatterBench extends RazorBenchmarkCaseBase {
		public function NumberFormatterBench() {
			super();
		}
		
		protected const SIGNIFICANT_DIGITS:int = 12;
		protected const NUMBERS:Array = [
			4.94065645841246544176e-324,
			2.22507385850720138309e-308,
			1.79769313486231570814e+308,
			0.00000123455,
			0.000000000123456
		];
		
		override public function initialize():void {
			super.initialize();
			
			nfNorm1 = NumberFormatter.createNormalNumberFormatter(SIGNIFICANT_DIGITS, 1e-2);
			nfNorm2 = NumberFormatter.createNormalNumberFormatter(SIGNIFICANT_DIGITS);
			nfFix = NumberFormatter.createFixedNumberFormatter(SIGNIFICANT_DIGITS, 3);
			nfSci = NumberFormatter.createScientificNumberFormatter(SIGNIFICANT_DIGITS);
			nfEng = NumberFormatter.createEngineeringNumberFormatter(SIGNIFICANT_DIGITS);
			
			for each (var nfName:String in ['nfNorm1', 'nfNorm2', 'nfFix', 'nfSci', 'nfEng'])
				for each (var num:Number in NUMBERS)
					addFun(nfName + '.format(' + num + ')', this[nfName].format, num);
		}
		
		override public function dispose():void {
			super.dispose();
			
			nfNorm1 = nfNorm2 = nfFix = nfSci = nfEng = null;
		}
		
		protected var nfNorm1:NumberFormatter;
		protected var nfNorm2:NumberFormatter;
		protected var nfFix:NumberFormatter;
		protected var nfSci:NumberFormatter;
		protected var nfEng:NumberFormatter;
	}
}
