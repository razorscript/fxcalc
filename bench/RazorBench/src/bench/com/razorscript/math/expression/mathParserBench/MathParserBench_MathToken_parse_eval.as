package bench.com.razorscript.math.expression.mathParserBench {
	import com.razorscript.math.expression.lexers.IMathTokenProvider;
	import com.razorscript.math.expression.lexers.MathTokenVectorMathTokenProvider;
	
	/**
	 * MathParser benchmark.
	 * Tests parsing and evaluation.
	 *
	 * @author Gene Pavlovsky
	 */
	public class MathParserBench_MathToken_parse_eval extends MathParserBenchBase_MathToken {
		public function MathParserBench_MathToken_parse_eval(mathFunClass:Class = null) {
			super(mathFunClass);
		}
		
		override public function initialize():void {
			super.initialize();
			
			for each (var str:String in EXPR) {
				var mathTokenProvider:IMathTokenProvider = new MathTokenVectorMathTokenProvider();
				stringConverter.convert(str, mathTokenProvider);
				addFun('parse(' + str + ').eval()', mathParser.eval, mathTokenProvider);
			}
		}
	}
}
