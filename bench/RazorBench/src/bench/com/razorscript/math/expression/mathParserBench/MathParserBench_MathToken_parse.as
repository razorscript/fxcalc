package bench.com.razorscript.math.expression.mathParserBench {
	import com.razorscript.bench.formatters.BenchmarkFormatOptions;
	import com.razorscript.math.expression.lexers.IMathTokenProvider;
	import com.razorscript.math.expression.lexers.MathTokenVectorMathTokenProvider;
	
	/**
	 * MathParser benchmark.
	 * Tests parsing.
	 *
	 * @author Gene Pavlovsky
	 */
	public class MathParserBench_MathToken_parse extends MathParserBenchBase_MathToken {
		public function MathParserBench_MathToken_parse(mathFunClass:Class = null) {
			super(mathFunClass);
			
			_formatOptions.valueFormatFun = formatMathExpressionStatus;
		}
		
		override public function initialize():void {
			super.initialize();
			
			for each (var str:String in EXPR) {
				var mathTokenProvider:IMathTokenProvider = new MathTokenVectorMathTokenProvider();
				stringConverter.convert(str, mathTokenProvider);
				addFun('parse(' + str + ')', mathParser.parse, mathTokenProvider);
			}
		}
	}
}
