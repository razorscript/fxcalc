package bench.com.razorscript.math.expression.mathParserBench {
	import bench.com.razorscript.math.expression.mathParserBench.MathParserBenchBase;
	import com.razorscript.math.expression.MathExpression;
	
	/**
	 * MathParser benchmark.
	 * Tests evaluation.
	 *
	 * @author Gene Pavlovsky
	 */
	public class MathParserBench_eval extends MathParserBenchBase {
		public function MathParserBench_eval(mathFunClass:Class = null) {
			super(mathFunClass);
		}
		
		override public function initialize():void {
			super.initialize();
			
			for each (var str:String in EXPR) {
				var expr:MathExpression = mathParser.parse(str);
				if (!expr.hasError)
					addFun('eval(' + str + ')', expr.eval);
			}
		}
	}
}
