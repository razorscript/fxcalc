package bench.com.razorscript.math.expression.mathParserBench {
	import com.razorscript.math.expression.converters.StringConverter;
	import com.razorscript.math.expression.lexers.MathTokenLexer;
	import com.razorscript.math.expression.tokens.IMathTokenFactory;
	
	/**
	 * Base class for MathParser benchmarks that use MathTokenLexer.
	 *
	 * @author Gene Pavlovsky
	 */
	public class MathParserBenchBase_MathToken extends MathParserBenchBase {
		public function MathParserBenchBase_MathToken(mathFunClass:Class = null) {
			super(mathFunClass);
		}
		
		override public function initialize():void {
			super.initialize();
			
			var tokenFactory:IMathTokenFactory = mathParser.tokenFactory;
			mathParser.lexer = new MathTokenLexer(mathOperator, tokenFactory);
			stringConverter = new StringConverter(mathContext, mathFun, mathOperator, tokenFactory, StringConverter.KEEP_WHITESPACE | StringConverter.USE_ERROR_TOKENS);
		}
		
		override public function dispose():void {
			super.dispose();
			
			stringConverter = null;
		}
		
		protected var stringConverter:StringConverter;
	}
}
