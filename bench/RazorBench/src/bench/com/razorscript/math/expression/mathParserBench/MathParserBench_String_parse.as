package bench.com.razorscript.math.expression.mathParserBench {
	import com.razorscript.bench.formatters.BenchmarkFormatOptions;
	
	/**
	 * MathParser benchmark.
	 * Tests parsing.
	 *
	 * @author Gene Pavlovsky
	 */
	public class MathParserBench_String_parse extends MathParserBenchBase {
		public function MathParserBench_String_parse(mathFunClass:Class = null) {
			super(mathFunClass);
			
			_formatOptions.valueFormatFun = formatMathExpressionStatus;
		}
		
		override public function initialize():void {
			super.initialize();
			
			for each (var str:String in EXPR)
				addFun('parse(' + str + ')', mathParser.parse, str);
		}
	}
}
