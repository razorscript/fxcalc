package bench.com.razorscript.math.expression.mathParserBench {
	import com.razorscript.math.MathContext;
	import com.razorscript.math.MathExt;
	import com.razorscript.math.errors.ArithmeticError;
	import com.razorscript.math.expression.MathExpression;
	import com.razorscript.math.expression.MathFun;
	import com.razorscript.math.expression.MathOperator;
	import com.razorscript.math.expression.MathParser;
	import com.razorscript.math.expression.lexers.StringLexer;
	import com.razorscript.math.expression.tokens.PooledMathTokenFactory;
	import com.razorscript.math.functions.MathLib;
	import com.razorscript.math.functions.polymorphism.TypeConverter;
	import com.razorscript.math.objects.ComplexNumber;
	import com.razorscript.math.objects.Fraction;
	import com.razorscript.math.objects.conversions.TypeConversionFun;
	import com.razorscript.utils.ClassUtil;
	import com.razorscript.utils.NumUtil;
	import com.razorscript.utils.VecUtil;
	
	/**
	 * Base class for MathParser benchmarks.
	 *
	 * @author Gene Pavlovsky
	 */
	public class MathParserBenchBase extends RazorBenchmarkCaseBase {
		public function MathParserBenchBase(mathFunClass:Class = null) {
			super();
			
			this.mathFunClass = mathFunClass ? mathFunClass : MathFun;
			_formatOptions.valueFormatFun = formatMathExpression;
		}
		
		override public function get name():String {
			var className:String = ClassUtil.getClassName(mathFunClass);
			return (className == 'MathFun') ?
				super.name.replace(/\. mathParserBench/x, '') :
				super.name.replace(/\. mathParserBench \. MathParserBench ((?: _ [A-Z] [A-Za-z0-9]*)*)/x, '.MathParserBench$1_' + className);
		}
		
		protected const CONST:Object = {PI: Math.PI, E: Math.E};
		protected const VAR:Object = {a: Math.SQRT2, b: Math.SQRT1_2, c: Math.LN10, d: Math.LN2, e: Math.LOG10E, f: Math.LOG2E, x: 65536, y: .357, m: 125};
		protected const EXPR:Array = [
			'PI',
			'2²³ - 2 ^ 2 ^ 3',
			'3 + 16 / 2 / 4 ^ 2 ^ 3',
			'3 + 4 * 2 / -( 1 - 5 ) ^ 2 ^ 3',
			'3ⁿ√ sqrt 4 - curt 2ⁿ√ 4',
			'1 + 2 * (1 + min(1, 3) / max(1, 2) - 1)',
			'sin 3x cos 3x tan 3x - sin 3x * cos 3x * tan 3x',
			'((a + b) * ((c + (e + f)) / (y + f ^ m)))',
			'-2e1 ^ 2 + -1 * (300 - 100) / (5 + 3 + 2) + 10 * E * sin(PI/2)',
			'(sqrt 4 - 2 + sqrt 4 - 2 + sqrt 4 - 2 + sqrt 4 - 2 + sqrt 4 - 2 + sqrt 4 - 2 + sqrt 4 - 2) * (2 ^ 64)',
		];
		
		override public function initialize():void {
			super.initialize();
			
			mathContext = new MathContext();
			mathContext.constants = CONST;
			mathContext.variables = VAR;
			
			typeConverter = TypeConversionFun.extend(new TypeConverter());
			mathLib = new MathLib(mathContext, typeConverter);
			mathFun = new mathFunClass(mathContext, mathLib);
			mathOperator = new MathOperator(mathFun);
			valueTransform = function (value:Number):Number {
				if (MathExt.isNaN(value))
					throw new ArithmeticError('Invalid operation.');
				var absValue:Number = Math.abs(value);
				if ((absValue >= 1e100) || (absValue && (absValue <= 1e-100)))
					throw new RangeError('Value (' + value + ') is out of range.');
				return value;
			}
			mathParser = new MathParser(mathContext, mathFun, mathOperator, new StringLexer(), valueTransform, new PooledMathTokenFactory());
			EXPR.sort(VecUtil.compareStringsByLengthAsc);
			//EXPR.push.apply(null, EXPR.slice().reverse());
		}
		
		override public function dispose():void {
			super.dispose();
			
			mathContext = null;
			typeConverter = null;
			mathLib = null;
			mathFun = null;
			mathOperator = null;
			valueTransform = null;
			mathParser = null;
		}
		
		protected function formatMathExpressionStatus(expr:MathExpression):String {
			return expr.hasError ? (expr.errorName + ' @' + expr.errorIndex) : '';
		}
		
		protected function formatMathExpression(expr:MathExpression):String {
			return expr.hasError ? (expr.errorName + ' @' + expr.errorIndex) : NumUtil.toGeneral(expr.value, MathExt.DOUBLE_UNIQ_DIGITS);
		}
		
		protected var mathFunClass:Class;
		protected var mathContext:MathContext;
		protected var typeConverter:TypeConverter;
		protected var mathLib:MathLib;
		protected var mathFun:MathFun;
		protected var mathOperator:MathOperator;
		protected var valueTransform:Function;
		protected var mathParser:MathParser;
	}
}
