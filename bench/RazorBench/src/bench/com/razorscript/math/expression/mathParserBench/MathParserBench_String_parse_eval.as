package bench.com.razorscript.math.expression.mathParserBench {
	
	/**
	 * MathParser benchmark.
	 * Tests parsing and evaluation.
	 *
	 * @author Gene Pavlovsky
	 */
	public class MathParserBench_String_parse_eval extends MathParserBenchBase {
		public function MathParserBench_String_parse_eval(mathFunClass:Class = null) {
			super(mathFunClass);
		}
		
		override public function initialize():void {
			super.initialize();
			
			for each (var str:String in EXPR)
				addFun('parse(' + str + ').eval()', mathParser.eval, str);
		}
	}
}
