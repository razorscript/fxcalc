package {
	import bench.com.razorscript.math.NumberFormatterBench;
	import bench.com.razorscript.math.expression.mathParserBench.MathParserBench_MathToken_parse;
	import bench.com.razorscript.math.expression.mathParserBench.MathParserBench_MathToken_parse_eval;
	import bench.com.razorscript.math.expression.mathParserBench.MathParserBench_String_parse;
	import bench.com.razorscript.math.expression.mathParserBench.MathParserBench_String_parse_eval;
	import bench.com.razorscript.math.expression.mathParserBench.MathParserBench_eval;
	import com.razorscript.bench.Benchmark;
	import com.razorscript.bench.formatters.BenchmarkTableFormatter;
	import com.razorscript.math.expression.RealMathFun;
	
	/**
	 * Razor benchmark.
	 *
	 * @author Gene Pavlovsky
	 */
	public class RazorBenchmark extends Benchmark {
		public function RazorBenchmark() {
			super();
			
			_formatOptions = new BenchmarkTableFormatter().formatOptions;
			CONFIG::debug {
				_itemDuration = 50;
			}
			CONFIG::release {
				_itemDuration = 5000;
			}
			
			/** com.razorscript.math */
			add(new NumberFormatterBench());
			
			/** com.razorscript.math.expression */
			add(new MathParserBench_String_parse());
			add(new MathParserBench_String_parse_eval());
			add(new MathParserBench_String_parse_eval(RealMathFun));
			add(new MathParserBench_MathToken_parse());
			add(new MathParserBench_MathToken_parse_eval());
			add(new MathParserBench_MathToken_parse_eval(RealMathFun));
			add(new MathParserBench_eval());
			add(new MathParserBench_eval(RealMathFun));
		}
	}
}
