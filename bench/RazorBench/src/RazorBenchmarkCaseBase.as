package {
	import com.razorscript.bench.BenchmarkCase;
	import com.razorscript.bench.BenchmarkCaseItem;
	
	/**
	 * Base class for Razor benchmark cases.
	 *
	 * @author Gene Pavlovsky
	 */
	public class RazorBenchmarkCaseBase extends BenchmarkCase {
		public function RazorBenchmarkCaseBase(name:String = null, items:Vector.<BenchmarkCaseItem> = null) {
			super(name, items);
		}
		
		override public function get name():String {
			return super.name.replace(/^bench\./, '').replace('::', '.');
		}
	}
}
