package {
	import com.razorscript.bench.ui.AIRBenchmarkConsole;
	
	[SWF(width="1350", height="690", backgroundColor="#002B36")]
	
	/**
	 * Razor benchmark console.
	 *
	 * @author Gene Pavlovsky
	 */
	public class RazorBenchMain extends AIRBenchmarkConsole {
		public function RazorBenchMain() {
			super();
		}
		
		override protected function run():void {
			benchRunner.run(new RazorBenchmark());
		}
	}
}
