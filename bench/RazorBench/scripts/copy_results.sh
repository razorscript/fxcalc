#!/bin/sh
bench_name=RazorBench
dir_name="../../tmp/bench/$bench_name"

cd "$(dirname "$0")/.."
mkdir -p "$dir_name" 2>/dev/null
dir_name="$dir_name/$(date +'%Y%m%d_%H%M%S')"
echo Copying results to: $(realpath $dir_name)
cp -a results "$dir_name"
echo Done.
