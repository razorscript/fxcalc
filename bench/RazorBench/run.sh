#!/bin/sh
swf='bin/RazorBench.swf'

cd "$(dirname "$0")"
mtime=$(stat -c %Y "$swf")
./build.sh build
echo
if test "$(stat -c %Y "$swf")" != "$mtime"; then
	echo -n "Cooling down for 5 seconds... "
	sleep 5
	echo -e '\n'
fi

echo -n "Running benchmark... "
cmd /c 'bat\RunApp-unattend.bat'
if test $? -ne 0; then
	echo failed
	exit 1
fi
echo done
echo
./scripts/copy_results.sh
