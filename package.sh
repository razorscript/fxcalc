#!/bin/sh
#
# Unless up to date, builds the app for the specified target using build.sh.
# Packages the app for the specified target.
#

default='android'
mobile='android'

android='android-captive-runtime'
ios='ios-app-store'

build_opts=
pkg_opts=

cd "$(dirname "$0")"
. ./scripts/share/app.sh

usage() {
	echo "Usage: $(basename "$0") [option]* [target]"
	echo
	echo "Default target: $default"
	echo
	echo "Package targets:"
	echo
	echo -e "  mobile                        Package the mobile app ($mobile)."
	echo -e "    android                     Package the Android app ($android)."
	echo -e "      [-captive-runtime]        Package with a captive runtime."
	echo -e "    ios                         Package the iOS app ($ios)."
	echo -e "      -app-store                Package for ad hoc distribution."
	echo -e "      -ad-hoc                   Package for App Store distribution."
	echo -e "      -interpreter              Use interpreter (deploys faster, runs slower)."
	echo -e "      -compiler                 Use compiler (deploys slower, runs faster)."
	echo
	echo "Options:"
	echo
	echo -e "  --release                     Use release build configuration."
	echo -e "  --debug                       Use debug build configuration, connect to debugger."
	echo -e "  --fd                          Use an existing build from FlashDevelop or another IDE, implies --debug."
	echo -e "  --help                        Display this help and exit."
	exit 0
}

while test $# -gt 0; do
	case $1 in
		--release|--debug)
			build_opts="${build_opts} -Dapp.build=${1#--}"
			pkg_opts="${pkg_opts} $1"
		;;
		--fd)
			skip_build=1
			pkg_opts="${pkg_opts} $1"
		;;
		--help|help)
			usage
		;;
		-*)
			die "Unknown option: \"$1\""
		;;
		*)
			target=$1
		;;
	esac
	
	shift
done
[[ ! $target ]] && target=$default

while true; do
	ref=${target//-/_}
	[[ ${!ref} ]] || break
	target=${!ref}
done

[[ $skip_build ]] || ./build.sh $build_opts "build-${target%%-[-a-z]*}" || die "Unknown package target: \"$target\""
echo -e "\npackage-${target}:\n"
eval exec ./scripts/package-app.sh $pkg_opts \"$target\"
